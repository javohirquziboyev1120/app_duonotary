package ai.ecma.duoserver.payload;

import lombok.Data;

import java.util.UUID;

@Data
public class CustomDiscountTariffDto {
    private UUID id;
    private String clientEmail;
    private CustomDiscountTariffClient customDiscountTariffClient;
    private Double percent;
    private Integer count;
    private Integer usingCount;
    private boolean unlimited;
    private boolean active;
}
