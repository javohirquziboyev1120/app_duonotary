package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResBlog {
    private UUID id;
    private String title;
    private String text;
    private boolean featured;
    private UUID attachmentId;
    private UUID userPhotoId;
    private String date;
    private String owner;
    private String url;
    private Integer categoryId;
    private String categoryName;
}
