package ai.ecma.duoserver.payload;

import lombok.Data;

import java.util.UUID;

@Data
public class ResFirstOrderDiscountTariff {
    private UUID id;
    private Double minPercent;
    private Double maxPercent;
    private Double percent;
    private ZipCodeDto zipCodeDto;
    private boolean active;
    private boolean online;
}
