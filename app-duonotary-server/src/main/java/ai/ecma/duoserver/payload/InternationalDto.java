package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.Country;
import ai.ecma.duoserver.entity.DocumentType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InternationalDto {
    private  UUID id;

    private boolean embassy;

    private boolean someOneElse;

    private DocumentType documentType;

    private String requesterFirstName;

    private String requesterLastName;

    private String requesterPhoneNumber;

    private String requesterEmail;

    private Integer numberDocument;

    private String pickUpAddress;

    private Country country;

    private OrderDto orderDto;
}
