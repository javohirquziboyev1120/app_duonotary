package ai.ecma.duoserver.payload;

import java.sql.Timestamp;

public interface AdminDashboardCalendarDto {
    String getSubServiceId();

    String getSubServiceName();

    String getClientFullName();

    String getClientPhoneNumber();

    String getAgentFullName();

    Timestamp getFromTime();

    Timestamp getTillTime();
}
