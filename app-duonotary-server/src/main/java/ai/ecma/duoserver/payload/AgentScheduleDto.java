package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AgentScheduleDto {
    private UUID id;
    private Integer weekDayId;
    private boolean dayOff;//1, Monday
    private WeekDaysDto weekDay;
    private List<HourDto> hourList;//[{fromTime:"09:00",tillTime:"12:00"},{fromTime:"15:00",tillTime:"20:00"}]
    private List<Integer> weekDayIds;

    public AgentScheduleDto(UUID id, Integer weekDayId, boolean dayOff, WeekDaysDto weekDay, List<HourDto> hourList) {
        this.id = id;
        this.weekDayId = weekDayId;
        this.dayOff = dayOff;
        this.weekDay = weekDay;
        this.hourList = hourList;
    }

    public AgentScheduleDto(WeekDaysDto weekDay, List<HourDto> hourList, boolean dayOff) {
        this.weekDay = weekDay;
        this.hourList = hourList;
        this.dayOff = dayOff;
    }
}