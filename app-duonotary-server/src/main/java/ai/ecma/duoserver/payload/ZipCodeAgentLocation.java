package ai.ecma.duoserver.payload;

import lombok.Data;

@Data
public class ZipCodeAgentLocation {
    private String lastTime;
    private String address;
}
