package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HourOffDto {
    private UUID id;
    private Timestamp fromHourOff;
    private Timestamp tillHourOff;
}
