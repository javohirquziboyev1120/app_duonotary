package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.Order;
import ai.ecma.duoserver.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SharingDiscountDto {

    private UUID id;

    private User clientId;

    private Double percent;

    private Double leftover;

    private Order orderId;
}
