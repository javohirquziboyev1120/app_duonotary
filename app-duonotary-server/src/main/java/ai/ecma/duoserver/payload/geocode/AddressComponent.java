package ai.ecma.duoserver.payload.geocode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedList;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressComponent {
    String long_name;
    String short_name;
    LinkedList<String> types;
}
