package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PermissionDto2 {
    private Integer id;
    private String perName;

    private String generalName;
    private List<PermissionDto2> perList;

    public PermissionDto2(Integer id, String generalName, List<PermissionDto2> perList) {
        this.id = id;
        this.generalName = generalName;
        this.perList = perList;
    }
    public PermissionDto2( String generalName, List<PermissionDto2> perList) {
        this.generalName = generalName;
        this.perList = perList;
    }

    public PermissionDto2(Integer id, String perName) {
        this.id = id;
        this.perName = perName;
    }
}
