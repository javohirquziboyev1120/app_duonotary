package ai.ecma.duoserver.payload.geocode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedList;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {
    LinkedList<AddressComponent> address_components;
    String formatted_address;
    Geometry geometry;
    String place_id;
    PlusCode plus_code;
    LinkedList<String> types;
}
