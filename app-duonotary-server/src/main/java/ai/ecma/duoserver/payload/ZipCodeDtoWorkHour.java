package ai.ecma.duoserver.payload;

import lombok.Data;

@Data
public class ZipCodeDtoWorkHour {
    private String name;
    private String startTime;
    private String endTime;
}
