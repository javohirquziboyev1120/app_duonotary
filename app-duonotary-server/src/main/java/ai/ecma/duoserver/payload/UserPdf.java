package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserPdf {
    private UUID id;

    private Integer num;

    private String firstName;//ISMI

    private String lastName;//FAMILYASI

    private String phoneNumber;//TELEFON RAQAMI. BUNDAN USERNAME SIFATIDA FOYDLANS HAM BO'LADI

    private String email;//XAT YUBORISH UCHUN EMAIL. BUNDAN USERNAME SIFATIDA FOYDLANS HAM BO'LADI

    private Integer ordersCount;//XAT YUBORISH UCHUN EMAIL. BUNDAN USERNAME SIFATIDA FOYDLANS HAM BO'LADI

    private String active;//AGENT UCHUN. AGAR FALSE BO'LSA ZAKAZ OLOLMAYDI VA ZAKAZ BILAN ISHLOLMAYDI

    private String online;//AGENT UCHUN. APPLICATION DAGI HOLATI

    private String  roleName;//USERNING ROLELARI

    private String onlineAgent;

    private String enabled;
}
