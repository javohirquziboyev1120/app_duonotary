package ai.ecma.duoserver.payload;

import lombok.Data;

import java.util.UUID;

@Data
public class SharingDiscountTariffDto {
    private UUID id;
    private Double maxPercent;
    private Double minPercent;
    private Double percent;
    private boolean active;
    private boolean online;
    private ZipCodeDto zipCodeDto;
}
