package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ClientMainPage {

    private String upcomingCount;
    private List<OrderDto> upcoming;

    private String inProgressCount;
    private List<OrderDto> inProgress;

    private String completedCount;
    private List<OrderDto> completed;

    private String rejectedCount;
    private List<OrderDto> rejected;

    private List<CanceledOrderDto> canceledOrder;
}
