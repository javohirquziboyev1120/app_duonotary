package ai.ecma.duoserver.payload;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class ReqFirstOrderDiscountTariff {
    private boolean allZipCode;
    private boolean active;
    private boolean online;
    private Double percent;
    private List<UUID> stateIds;
    private List<UUID> countyIds;
    private List<UUID> zipCodeIds;
}
