package ai.ecma.duoserver.payload;

import lombok.Data;

import java.sql.Timestamp;
import java.util.UUID;

@Data
public class ResClientOrder {
    private UUID id;
    private Timestamp createdAt;
    private String agentFullName;
    private Double orderAmount;
}
