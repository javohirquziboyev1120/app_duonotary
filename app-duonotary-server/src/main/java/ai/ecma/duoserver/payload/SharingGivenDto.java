package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SharingGivenDto {
    private String clientFullName;
    private String invitedClientFullName;
    private Double percent;
    private Double sum;
    private Double leftover;
    private Date date;

    public SharingGivenDto(String clientFullName, String invitedClientFullName, Double percent, Double sum) {
        this.clientFullName = clientFullName;
        this.invitedClientFullName = invitedClientFullName;
        this.percent = percent;
        this.sum = sum;
    }

    public SharingGivenDto(String clientFullName, String invitedClientFullName, Double percent, Double sum, Double leftover) {
        this.clientFullName = clientFullName;
        this.invitedClientFullName = invitedClientFullName;
        this.percent = percent;
        this.sum = sum;
        this.leftover = leftover;
    }
}
