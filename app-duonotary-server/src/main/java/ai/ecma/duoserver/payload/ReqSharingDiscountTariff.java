package ai.ecma.duoserver.payload;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class ReqSharingDiscountTariff {
    private boolean allZipCode;
    private boolean active;
    private List<UUID> stateIds;
    private List<UUID> countyIds;
    private List<UUID> zipCodeIds;
    private double percent;
    private boolean online;
}