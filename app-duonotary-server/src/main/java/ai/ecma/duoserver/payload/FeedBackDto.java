package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.enums.OperationEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FeedBackDto {

    private UUID id;

    private OrderDto orderDto;

    private boolean agent;

    private Integer rate;

    private String description;

    private String answer;

    private boolean seen;

    private OperationEnum operationEnum;
}
