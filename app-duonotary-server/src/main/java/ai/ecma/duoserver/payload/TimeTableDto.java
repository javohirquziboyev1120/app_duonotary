package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimeTableDto {
    private UUID id;
    private String fromTime;
    private String tillTime;

    private UserDto userDto;
    private OrderDto orderDto;
    private boolean tempBooked;
    private UUID tempId;
    private Boolean online;
    private Timestamp createdAt;
}
