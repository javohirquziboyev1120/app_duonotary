package ai.ecma.duoserver.payload.geocode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedList;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Geocode {
    PlusCode plus_code;
    LinkedList<Result> results;
    String status;
}
