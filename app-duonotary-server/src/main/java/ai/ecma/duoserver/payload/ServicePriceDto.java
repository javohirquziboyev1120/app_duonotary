package ai.ecma.duoserver.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)    //BU ANNATITION SERVERDAN KELAYOPTGAN NULL MALUMOTLRNI BERIB YUBORMAYDI!!!
public class ServicePriceDto {

    private UUID id;

    private UUID serviceId;

    private String subServiceName;

    private String mainServiceName;

    private Double price;

    private Boolean active;

    private Boolean serviceActive;

    private Integer chargeMinute;

    private Double chargePercent;

    private boolean all;

    private List<UUID> statesId = new ArrayList<>();

    private List<UUID> countiesId = new ArrayList<>();

    private List<UUID> zipCodesId = new ArrayList<>();

    private ServiceDto serviceDto;

    private ZipCodeDto zipCodeDto;

    private double minPrice;

    private double maxPrice;

    private Boolean online;

    private String subServiceDescription;


    public ServicePriceDto(String mainServiceName, String subServiceName, double minPrice, double maxPrice, Boolean online, UUID serviceId, Boolean serviceActive, String subServiceDescription) {
        this.mainServiceName = mainServiceName;
        this.subServiceName = subServiceName;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.online = online;
        this.serviceId = serviceId;
        this.serviceActive = serviceActive;
        this.subServiceDescription = subServiceDescription;
    }

    public ServicePriceDto(String mainServiceName, String subServiceName, Boolean online) {
        this.mainServiceName = mainServiceName;
        this.subServiceName = subServiceName;
        this.online = online;
    }

    public ServicePriceDto(UUID id, String subServiceName, Double price, String subServiceDescription) {
        this.id = id;
        this.subServiceName = subServiceName;
        this.price = price;
        this.subServiceDescription = subServiceDescription;
    }

    public ServicePriceDto(UUID id, Double price, boolean active, Integer chargeMinute, Double chargePercent, ServiceDto serviceDto, ZipCodeDto zipCodeDto) {
        this.id = id;
        this.price = price;
        this.active = active;
        this.chargeMinute = chargeMinute;
        this.chargePercent = chargePercent;
        this.serviceDto = serviceDto;
        this.zipCodeDto = zipCodeDto;
    }
}
