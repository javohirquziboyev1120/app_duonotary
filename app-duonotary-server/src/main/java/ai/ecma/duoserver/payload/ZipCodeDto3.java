package ai.ecma.duoserver.payload;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ZipCodeDto3 {
    private String code;
    private String countyName;
    private String stateName;
    private List<String> zipCodes;

    public ZipCodeDto3(String stateName, List<String> zipCodes) {
        this.stateName = stateName;
        this.zipCodes = zipCodes;
    }

    public ZipCodeDto3(String code, String countyName, String stateName) {
        this.code = code;
        this.countyName = countyName;
        this.stateName = stateName;
    }
}
