package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.enums.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CertificateDto {
    private UUID id;
    private UUID attachmentId;
    private UserDto userDto;
    private StateDto stateDto;
    private Date issueDate;
    private Date expireDate;
    private StatusEnum statusEnum;
    private boolean expired;
}
