package ai.ecma.duoserver.payload.enums;

public enum PaymentType {
    ALL, YEAR, MONTH, WEEK, DAY
}
