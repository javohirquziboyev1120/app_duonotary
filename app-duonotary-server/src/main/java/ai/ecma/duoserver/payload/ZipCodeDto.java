package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.TimezoneName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ZipCodeDto {

    private UUID id;

    private String code;

    private CountyDto countyDto;

    private UUID countyId;

    private String city;

    private boolean active;

    private TimezoneName timezoneName;

    private Integer timezoneNameId;

    public ZipCodeDto(String code, String city) {
        this.code = code;
        this.city = city;
    }

    public ZipCodeDto(UUID id, String code, CountyDto countyDto, String city, boolean active) {
        this.id = id;
        this.code = code;
        this.countyDto = countyDto;
        this.city = city;
        this.active = active;
    }

    public ZipCodeDto(UUID id, String code, CountyDto countyDto, String city, boolean active, TimezoneName timezoneName) {
        this.id = id;
        this.code = code;
        this.countyDto = countyDto;
        this.city = city;
        this.active = active;
        this.timezoneName = timezoneName;
    }

    public ZipCodeDto(UUID id) {
        this.id = id;
    }
}
