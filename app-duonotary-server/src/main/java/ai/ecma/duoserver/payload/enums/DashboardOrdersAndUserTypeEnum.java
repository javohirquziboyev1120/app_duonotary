package ai.ecma.duoserver.payload.enums;

public enum DashboardOrdersAndUserTypeEnum {
    USER,
    ONLINE,
    IN_PERSON,
    MOBILE
}
