package ai.ecma.duoserver.payload;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ReqLogin {
    @NotBlank
    private String username;
    @NotBlank
    private String password;
    private boolean newUser;
}
