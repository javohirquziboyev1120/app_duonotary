package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.criteria.Order;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResSharingDiscount {
    private double leftover;
    private double percent;
    private Order order;
    private User clients;
    private User shareClinet;
    private double allLeftover;

    public ResSharingDiscount(User clients, double allLeftover) {
        this.clients = clients;
        this.allLeftover = allLeftover;
    }
}
