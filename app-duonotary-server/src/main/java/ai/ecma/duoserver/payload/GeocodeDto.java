package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.ZipCode;
import ai.ecma.duoserver.payload.geocode.AddressComponent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GeocodeDto {
    String address;
    Double lat;
    Double lng;
    AddressComponent addressComponent;
    ZipCode zipCode;

    public GeocodeDto(String address, Double lat, Double lng, AddressComponent addressComponent) {
        this.address = address;
        this.lat = lat;
        this.lng = lng;
        this.addressComponent = addressComponent;
    }
}
