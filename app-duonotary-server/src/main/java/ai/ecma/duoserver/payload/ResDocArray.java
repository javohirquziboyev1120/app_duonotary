package ai.ecma.duoserver.payload;

import lombok.Data;

@Data
public class ResDocArray {

    private String name;

    private String docVerifyId;
}
