package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.PayType;
import ai.ecma.duoserver.entity.enums.PayStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentDto {
    private UUID id;
    private UUID orderId;//
    private Double paySum;//
    private UUID payTypeId;//
    private PayType payType;
    private String clientId;//
    private String agentId;//
    private String chargeId;//
    private UserDto client;
    private UserDto agent;
    private OrderDto order;
    private String payTypeName;
    private PayStatus payStatus;
    private Timestamp date;//

    public PaymentDto(UUID orderId, Double paySum, UUID payTypeId, String clientId) {
        this.orderId = orderId;
        this.paySum = paySum;
        this.payTypeId = payTypeId;
        this.clientId = clientId;
    }

    public PaymentDto(Double paySum, UserDto client, UserDto agent, OrderDto order, String payTypeName) {
        this.paySum = paySum;
        this.client = client;
        this.agent = agent;
        this.order = order;
        this.payTypeName = payTypeName;
    }

    public PaymentDto(UUID orderId, Double paySum, UUID payTypeId, String clientId, UserDto client, UserDto agent, OrderDto order, String payTypeName) {
        this.orderId = orderId;
        this.paySum = paySum;
        this.payTypeId = payTypeId;
        this.clientId = clientId;
        this.client = client;
        this.agent = agent;
        this.order = order;
        this.payTypeName = payTypeName;
    }

    public PaymentDto(UserDto client, UserDto agent, OrderDto order, Timestamp date, Double paySum, PayType payType, String chargeId, String payTypeName, UUID id) {
        this.client = client;
        this.agent = agent;
        this.order = order;
        this.date = date;
        this.paySum = paySum;
        this.payType = payType;
        this.chargeId = chargeId;
        this.payTypeName = payTypeName;
        this.id = id;
    }
}
