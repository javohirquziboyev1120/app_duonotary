package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.Order;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiscountExpenseDto {
    private UUID id;

    private Order order;

    private Double amountDto;

    private UUID discountId;

    private boolean sharingId;
}
