package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiscountPercentDto {

    private UUID id;

    private double percent;

    private ZipCodeDto zipCodeDto;

    private boolean all;

    private List<UUID> statesId = new ArrayList<>();

    private List<UUID> countiesId = new ArrayList<>();

    private List<UUID> zipCodesId = new ArrayList<>();

    private double minPercent;

    private double maxPercent;

    private boolean active;

    private boolean online;

}
