package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PriceDtoForOrder {
    private double amount;
    private double sharingDiscount;
    private double loyaltyDiscount;
    private double percent;
}
