package ai.ecma.duoserver.payload.order;

import ai.ecma.duoserver.entity.ServicePrice;
import ai.ecma.duoserver.payload.ZipCodeDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResAddress {
    private ZipCodeDto zipCode;
    private List<ServicePrice> servicePrice;
}
