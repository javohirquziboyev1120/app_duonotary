package ai.ecma.duoserver.payload;

import lombok.Data;

import java.util.UUID;

@Data
public class CustomDiscountTariffClient {
    private UUID id;
    private UUID photoId;
    private String email;
    private String phoneNumber;
    private String fullName;
}
