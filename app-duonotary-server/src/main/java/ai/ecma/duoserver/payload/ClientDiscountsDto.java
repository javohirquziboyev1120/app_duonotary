package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientDiscountsDto {

    private int number;
    private UUID discountId;
    private String discountName;
    private Double amount;
    private Double allAmount;
    private double needAmount;
    private boolean needed;
    private double discountPercent;


    public ClientDiscountsDto(UUID discountId, String discountName, Double amount, Double allAmount, Double needAmount, boolean needed) {
        this.discountId = discountId;
        this.discountName = discountName;
        this.amount = amount;
        this.allAmount = allAmount;
        this.needAmount = needAmount;
        this.needed = needed;
    }

    public ClientDiscountsDto(UUID discountId, String discountName, Double amount, Double allAmount, Double needAmount, boolean needed, double discountPercent) {
        this.discountId = discountId;
        this.discountName = discountName;
        this.amount = amount;
        this.allAmount = allAmount;
        this.needAmount = needAmount;
        this.needed = needed;
        this.discountPercent = discountPercent;
    }

    public ClientDiscountsDto(String discountName, Double amount, Double allAmount, Double needAmount, boolean needed) {
        this.discountName = discountName;
        this.amount = amount;
        this.allAmount = allAmount;
        this.needAmount = needAmount;
        this.needed = needed;
    }

    public ClientDiscountsDto(String discountName, Double amount, Double allAmount, Double needAmount, boolean needed, double discountPercent) {
        this.discountName = discountName;
        this.amount = amount;
        this.allAmount = allAmount;
        this.needAmount = needAmount;
        this.needed = needed;
        this.discountPercent = discountPercent;
    }

    public ClientDiscountsDto(UUID discountId, String discountName, Double amount) {
        this.discountId = discountId;
        this.discountName = discountName;
        this.amount = amount;
    }

    public ClientDiscountsDto(String discountName, Double amount) {
        this.discountName = discountName;
        this.amount = amount;
    }
}
