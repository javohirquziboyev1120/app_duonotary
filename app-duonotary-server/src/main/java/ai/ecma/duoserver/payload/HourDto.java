package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HourDto {
    private UUID id;
    private String fromTime;
    private String tillTime;
    private Double percent;

    public HourDto(String fromTime, String tillTime) {
        this.fromTime = fromTime;
        this.tillTime = tillTime;
    }

    public HourDto(UUID id, String fromTime, String tillTime) {
        this.id = id;
        this.fromTime = fromTime;
        this.tillTime = tillTime;
    }

    public HourDto(String fromTime, String tillTime, Double percent) {
        this.fromTime = fromTime;
        this.tillTime = tillTime;
        this.percent = percent;
    }
}
