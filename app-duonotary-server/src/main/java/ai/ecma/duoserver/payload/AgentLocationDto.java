package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AgentLocationDto {
    @ManyToOne(optional = false)
    private UserDto agent;

    private Float lat;//AGENT UCHUN. QAYSI KENGLIKDA EKANLIGI

    private Float lon;//AGENT UCHUN. QAYERDA UZUNLIKDA EKANLIGI

    private Timestamp createdAt;

    private String zipCode;

    private String fullLocation;

}
