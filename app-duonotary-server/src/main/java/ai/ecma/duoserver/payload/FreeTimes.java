package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FreeTimes {

    private LocalTime time;
    private LocalTime end;
    private boolean enable;
    private UUID agentId;
    private double percent;

    public FreeTimes(LocalTime time, LocalTime end, UUID agentId) {
        this.time = time;
        this.end = end;
        this.agentId = agentId;
    }
    public FreeTimes(LocalTime time, LocalTime end) {
        this.time = time;
        this.end = end;
    }
    public FreeTimes(LocalTime time, LocalTime end, double percent) {
        this.time = time;
        this.end = end;
        this.percent = percent;
    }

    public FreeTimes(LocalTime time, boolean enable) {
        this.time = time;
        this.enable = enable;
    }

    public FreeTimes(LocalTime time, boolean enable, UUID agentId) {
        this.time = time;
        this.enable = enable;
        this.agentId = agentId;
    }
}
