package ai.ecma.duoserver.payload.geocode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlusCode {
    String compound_code;
    String global_code;
}
