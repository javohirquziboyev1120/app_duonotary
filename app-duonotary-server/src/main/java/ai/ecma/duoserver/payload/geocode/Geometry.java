package ai.ecma.duoserver.payload.geocode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Geometry {
    Location location;
    String location_type;
    Viewport viewport;
}
