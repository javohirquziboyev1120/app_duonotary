package ai.ecma.duoserver.payload;

import lombok.Data;

import java.util.UUID;

@Data
public class ResCustomers {
    private UUID id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private UUID photoId;
}
