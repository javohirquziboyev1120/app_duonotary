package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PricingDto {
    private UUID id;

    private UUID serviceId;

    private UUID stateId;

    private ServicePriceDto servicePriceDto;

    private Double price;

    private Double newPrice;

    private boolean active;

    private Integer fromCount;

    private Integer tillCount;

    private Integer everyCount;

    private boolean all;

    private List<UUID> statesId = new ArrayList<>();

    private double minPrice;

    private double maxPrice;

    private Integer minFromCount;

    private Integer maxFromCount;

    private Integer minTillCount;

    private Integer maxTillCount;

    private Integer minEveryCount;

    private Integer maxEveryCount;

    private Integer newFromCount;

    private Integer newTillCount;

    private Integer newEveryCount;

    private String subServiceName;

    private String stateName;

    public PricingDto(double minPrice, double maxPrice, Integer minFromCount, Integer maxTillCount, Integer minEveryCount, String stateName, UUID stateId) {
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.minFromCount = minFromCount;
        this.maxTillCount = maxTillCount;
        this.minEveryCount = minEveryCount;
        this.stateName = stateName;
        this.stateId = stateId;
    }


    public PricingDto(UUID id, ServicePriceDto servicePriceDto, Double price, boolean active, Integer fromCount, Integer tillCount, Integer everyCount) {
        this.id = id;
        this.servicePriceDto = servicePriceDto;
        this.price = price;
        this.active = active;
        this.fromCount = fromCount;
        this.tillCount = tillCount;
        this.everyCount = everyCount;
    }

    public PricingDto(double price, int fromCount, int tillCount, int everyCount, String subServiceName, boolean active) {
        this.price = price;
        this.fromCount = fromCount;
        this.tillCount = tillCount;
        this.everyCount = everyCount;
        this.subServiceName = subServiceName;
        this.active=active;
    }
}
