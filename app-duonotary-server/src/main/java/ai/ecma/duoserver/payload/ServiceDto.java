package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.MainService;
import ai.ecma.duoserver.entity.SubService;
import ai.ecma.duoserver.entity.enums.ServiceEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServiceDto {

    private UUID id;

    private UUID mainServiceId;

    private MainServiceDto mainServiceDto;

    private UUID subServiceId;

    private SubServiceDto subServiceDto;

    private Integer initialCount;

    private Integer initialSpendingTime;

    private Integer everyCount;

    private Integer everySpendingTime;

    private boolean dynamic;

    private boolean active;

    private List<UUID> mainServices = new ArrayList<>();

    private String name;

    private boolean defaultInput;

    private String description;

    private ServiceEnum serviceEnum;

    public ServiceDto(UUID id, MainServiceDto mainServiceDto, SubServiceDto subServiceDto,
                      Integer initialCount, Integer initialSpendingTime, Integer everyCount,
                      Integer everySpendingTime, boolean dynamic, boolean active) {
        this.id = id;
        this.mainServiceDto = mainServiceDto;
        this.subServiceDto = subServiceDto;
        this.initialCount = initialCount;
        this.initialSpendingTime = initialSpendingTime;
        this.everyCount = everyCount;
        this.everySpendingTime = everySpendingTime;
        this.dynamic = dynamic;
        this.active = active;
    }

    public ServiceDto(UUID mainServiceId, UUID subServiceId,
                      Integer initialCount, Integer initialSpendingTime,
                      Integer everyCount, Integer everySpendingTime,
                      boolean dynamic, boolean active) {
        this.mainServiceId = mainServiceId;
        this.subServiceId = subServiceId;
        this.initialCount = initialCount;
        this.initialSpendingTime = initialSpendingTime;
        this.everyCount = everyCount;
        this.everySpendingTime = everySpendingTime;
        this.dynamic = dynamic;
        this.active = active;
    }


}
