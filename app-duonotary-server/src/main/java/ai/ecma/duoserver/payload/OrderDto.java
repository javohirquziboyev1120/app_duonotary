package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.Attachment;
import ai.ecma.duoserver.entity.TimeTable;
import ai.ecma.duoserver.entity.enums.OrderFeedback;
import ai.ecma.duoserver.entity.enums.OrderStatus;
import ai.ecma.duoserver.entity.enums.RegisterBy;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {
    private UUID id; //orderning id si tahrirlash uchun

    private OrderFeedback feedbackStatus;

    private String address;//faqat in person uchun ok

    private UUID zipCodeId;//faqat in person uchun ok

    private UUID clientId; //ok

    private String lat;//faqat in person uchun

    private String lng;//faqat in person uchun

    @NotNull
    private UUID servicePriceId;//servicening narxi va service ma'lumotlari shundan olinadi ok

    @NotNull
    private UUID payTypeId;//online(in-person yoki online uchun), cash(in-person uchun)

    @NotNull
    private UUID agentId;// ok

    private Double amount;//buyurtmaning chegirmalar hisobga olinmagandagi sof narxi dollarda

    private Integer countDocument;//document soni //ok

    private List<OrderAdditionalServiceDto> orderAdditionalServiceDtoList = new ArrayList<>();//qo'shimcha xizmatlar(guvohnoma, list)
    private List<UUID> orderAdditionalService = new ArrayList<>();//qo'shimcha xizmatlar(guvohnoma, list)

    private String checkNumber;//pul to'langanda to'lov roqami

    private String serialNumber;//GENERATE QILINADIGAN RAQAM CLIENT UCHUN

    private List<UUID> docAttachmentId = new ArrayList<>();//orderdagi documentlarning idisi

    private String docVerifyId;//online uchun, orderga pul to'langandan keyin, agent sayt orqali docverifyga
    // kirganda orderga tegishli hujjatlarni docverifyga jo'natib docverifydan hujjatning
    // yoki paketning idisini olish uchun
    private boolean packet;//docverifyda faqat bitta dokumentmi yoki paketmi(ko'p dokumentlar)

    private boolean newUser; //ok

    private String username; //ok

    private String password; //ok

    private double discountAmount;//$ qancha miqdorda discountidan foydalanishi

    private UUID timeTableId;

    private UserDto client;
    private UserDto agent;
    private ServicePriceDto servicePrice;
    private ZipCodeDto zipCode;
    private PayTypeDto payType;
    private List<Attachment> documents;
    private Timestamp created_at;
    private OrderStatus status;
    private TimeTableDto timeTableDto;
    private String titleDocument;
    private InternationalDto internationalDto;
    private RealEstateDto realEstateDto;
    private boolean payed = false;
    private boolean stripe = false;
    private List<ResUploadFile> resUploadFiles;
    private List<ClientDiscountsDto> discountsDtos;
    private RegisterBy registerBy;

    public OrderDto(UUID id, UserDto client, UserDto agent, double amount) {
        this.id = id;
        this.client = client;
        this.agent = agent;
        this.amount = amount;
    }

    public OrderDto(String address, ZipCodeDto zipCode, String lat, String lng, PayTypeDto payType, Double amount,
                    Integer countDocument, String checkNumber, List<Attachment> documents, String docVerifyId, boolean packet,
                    UUID id, Timestamp createdAt, UserDto client, UserDto agent, ServicePriceDto servicePrice, OrderStatus status, TimeTableDto timeTableDto) {
        this.address = address;
        this.zipCode = zipCode;
        this.lat = lat;
        this.lng = lng;
        this.payType = payType;
        this.amount = amount;
        this.countDocument = countDocument;
        this.checkNumber = checkNumber;
        this.documents = documents;
        this.docVerifyId = docVerifyId;
        this.packet = packet;
        this.id = id;
        this.created_at = createdAt;
        this.client = client;
        this.agent = agent;
        this.servicePrice = servicePrice;
        this.status = status;
        this.timeTableDto = timeTableDto;
    }
}
