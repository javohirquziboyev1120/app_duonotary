package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimeTableForOrderDto {//bu dtoda klient buyurtma berayotganda chiqadigan vaqtlar
    private String date;//bu kun nomi keladi. mn: 23 Decemeber 2019
    private List<FreeTimes> hourDtoList; //o'sha kundagi soatlarning listi
}
