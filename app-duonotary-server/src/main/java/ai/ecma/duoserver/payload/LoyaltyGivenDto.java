package ai.ecma.duoserver.payload;

import lombok.Data;

@Data
public class LoyaltyGivenDto {
    private String clientFullName;
    private Double percent;
    private Double sum;
}
