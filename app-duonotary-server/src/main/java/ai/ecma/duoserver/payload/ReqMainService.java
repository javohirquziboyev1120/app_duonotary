package ai.ecma.duoserver.payload;

import lombok.Data;

@Data
public class ReqMainService {
    private String name;

    private boolean active;

    private String fromTime;

    private String tillTime;

    private String description;
}
