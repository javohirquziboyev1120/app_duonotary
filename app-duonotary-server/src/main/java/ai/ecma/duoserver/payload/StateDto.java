package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StateDto {

    private UUID id;

    private String name;

    public StateDto(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    private List<ZipCodeDto> zipCodeDtoList;
}
