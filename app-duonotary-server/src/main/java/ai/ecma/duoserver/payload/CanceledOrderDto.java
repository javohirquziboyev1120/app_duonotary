package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CanceledOrderDto {
    private UUID id;
    private UserDto fromAgent;
    private UserDto toAgent;
    private OrderDto order;
    private Boolean accept;
}
