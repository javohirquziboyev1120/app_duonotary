package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResTimeTable {
    UUID id;
    UUID agentId;
    Timestamp fromTime;
    Timestamp tillTime;
    boolean online;
    Timestamp createdAt;
}
