package ai.ecma.duoserver.payload;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.validation.constraints.Email;
import java.util.List;
import java.util.UUID;

@Data
public class AdminDto {
    private String firstName;
    private String lastName;
    private String password;
    private String prePassword;

    @NotNull
    private String phoneNumber;

    @Email
    private String email;



}
