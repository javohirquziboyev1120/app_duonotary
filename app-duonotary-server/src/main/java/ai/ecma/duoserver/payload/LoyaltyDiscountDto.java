package ai.ecma.duoserver.payload;

import lombok.Data;

@Data
public class LoyaltyDiscountDto {

    private Integer id;
    private Integer month;
    private Double percent;
    private boolean active;
    private Double minSum;
}
