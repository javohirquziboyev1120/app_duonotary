package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.Order;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderAdditionalServiceDto {
    private UUID id;
    private OrderDto orderDto;
    private AdditionalServicePriceDto additionalServicePriceDto;
    private Integer count; //asosan frontend dan olinadi.
    private String description; //asosan frontend dan olinadi.
    private Double currentPrice;
    private Order order;//orderni saqlaganimizdan so`ng keladi.
    private UUID additionalServicePriceId; //asosan frontend dan olinadi.

    public OrderAdditionalServiceDto(UUID id, OrderDto orderDto, AdditionalServicePriceDto additionalServicePriceDto, Integer count, String description, Double currentPrice) {
        this.id = id;
        this.orderDto = orderDto;
        this.additionalServicePriceDto = additionalServicePriceDto;
        this.count = count;
        this.description = description;
        this.currentPrice = currentPrice;
    }

    public OrderAdditionalServiceDto(Integer count, String description, Double currentPrice, Order order, UUID additionalServicePriceId) {
        this.count = count;
        this.description = description;
        this.currentPrice = currentPrice;
        this.order = order;
        this.additionalServicePriceId = additionalServicePriceId;
    }
}