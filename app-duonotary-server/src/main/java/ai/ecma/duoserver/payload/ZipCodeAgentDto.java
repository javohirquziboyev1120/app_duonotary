package ai.ecma.duoserver.payload;

import lombok.Data;

import java.util.UUID;

@Data
public class ZipCodeAgentDto {
    private UUID id;
    private String imgUrl;
    private String email;
    private String phoneNumber;
    private String fullName;
    private ZipCodeAgentLocation location;
    private String onlineTime;
    private Integer ordersCount;
}
