package ai.ecma.duoserver.payload;

public interface DashboardOrderCashDto {
    String getAllTime();

    String getToday();

    String getWeekly();

    String getMonthly();
}
