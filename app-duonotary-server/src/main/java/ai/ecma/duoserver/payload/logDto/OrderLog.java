package ai.ecma.duoserver.payload.logDto;

import ai.ecma.duoserver.entity.History;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderLog {

    private UUID id;

    private String createdAt;

    private String updatedAt;

    private String number;

    private String client;

    private String agent;

    private String amount;



    private List<Object> objects;

}
