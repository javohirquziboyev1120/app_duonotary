package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.AdditionalServicePrice;

import java.util.UUID;

public class ReqOrderAdditionalServiec {
    private UUID orderId;
    private AdditionalServicePrice additionalServicePrice;
    private Integer count;
    private String description;
    private Double currentPrice;
}
