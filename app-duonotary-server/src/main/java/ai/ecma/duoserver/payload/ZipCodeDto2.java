package ai.ecma.duoserver.payload;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class ZipCodeDto2 {
    private UUID id;

    private String code;

    private CountyDto countyDto;

    private UUID countyId;

    private String city;

    private boolean active;

    private List<ZipCodeDtoWorkHour> workHours;

    private String discount;

    private List<ZipCodeAgentDto> agents;

}
