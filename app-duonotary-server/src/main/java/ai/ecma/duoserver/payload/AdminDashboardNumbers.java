package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.payload.enums.DashboardOrdersAndUserTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdminDashboardNumbers {
    private DashboardOrdersAndUserTypeEnum type;
    private Integer today;
    private Integer all;
}
