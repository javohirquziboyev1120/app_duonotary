package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResTimeTable2 {
    private UUID id;
    private Timestamp fromTime;
    private Timestamp tillTime;

    private UserDto userDto;
    private OrderDto orderDto;
    private boolean tempBooked;
    private UUID tempId;
    private Boolean online;
    private Timestamp createdAt;
}
