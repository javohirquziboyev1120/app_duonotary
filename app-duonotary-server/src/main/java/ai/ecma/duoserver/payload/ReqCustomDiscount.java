package ai.ecma.duoserver.payload;

import lombok.Data;

import java.util.UUID;

@Data
public class ReqCustomDiscount {
    private UUID id;
    private UUID orderId;
    private Double percent;
    private Double sum;
    private String description;
}
