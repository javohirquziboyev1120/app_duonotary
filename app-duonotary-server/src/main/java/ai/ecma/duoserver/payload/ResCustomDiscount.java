package ai.ecma.duoserver.payload;

import lombok.Data;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Data
public class ResCustomDiscount {
    private UUID id;
    private UUID clientId;
    private List<ResClientOrder> orderList;
     private UUID orderId;
    private Timestamp createdAt;
    private String clientFullName;
    private UUID clientPhotoId;
    private Double percent;
    private Double sum;
    private String description;
}
