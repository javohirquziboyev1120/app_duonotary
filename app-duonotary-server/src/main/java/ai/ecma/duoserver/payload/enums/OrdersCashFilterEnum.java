package ai.ecma.duoserver.payload.enums;

public enum OrdersCashFilterEnum {
    TODAY,
    WEEK,
    MONTH
}
