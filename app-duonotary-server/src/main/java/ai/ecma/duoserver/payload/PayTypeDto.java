package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PayTypeDto {

    private UUID id;

    private String name;

    private boolean active;

    private boolean online;

    private String description;
}
