package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.enums.WeekDayEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WeekDaysDto {
    private Integer id;
    private WeekDayEnum day;
    private Integer orderNumber;

    public WeekDaysDto(WeekDayEnum day, Integer orderNumber) {
        this.day = day;
        this.orderNumber = orderNumber;
    }
}
