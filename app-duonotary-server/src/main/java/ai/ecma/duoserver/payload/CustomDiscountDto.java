package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomDiscountDto {
    private UUID id;

    private OrderDto orderDto;

    private Double percent;

    private Double amount;

    private String description;
}
