package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.enums.RealEstateEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RealEstateDto {
    private UUID id;

    private String clientName;

    private String clientPhone;

    private String clientAddress;

    private String clientEmail;

    private RealEstateEnum realEstateEnum;

    private OrderDto orderDto;
}
