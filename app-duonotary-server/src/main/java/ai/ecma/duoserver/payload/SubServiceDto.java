package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.enums.ServiceEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubServiceDto {

    private UUID id;

    private String name;

    private boolean defaultInput;

    private boolean active;

    private String description;

    private ServiceEnum serviceEnum;
}
