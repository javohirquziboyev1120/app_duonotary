package ai.ecma.duoserver.payload;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class ResDiscountExpense {
    private String clientFullName;
    private Timestamp createdAt;
    private Double sum;
}
