package ai.ecma.duoserver.payload;

import lombok.Data;

import javax.persistence.Column;
import java.util.Date;
import java.util.UUID;

@Data
public class ReqHoliday {
    private UUID id;
    private String name;
    private UUID mainServiceId;
    private boolean active;
    private String description;
    private Date date;
}
