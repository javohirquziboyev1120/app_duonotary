package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OutOfServiceDto {
    private UUID id;
    private String email;
    private String zipCode;
    private boolean sent;
}
