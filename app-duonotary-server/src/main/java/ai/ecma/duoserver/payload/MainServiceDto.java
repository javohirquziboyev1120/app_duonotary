package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MainServiceDto {

    private UUID id;

    private String name;

    private String fromTime;//ISH VAQTINING BOSHLANISHI

    private String tillTime;//ISH VAQTINING TUGASHI

    private boolean online;

    public MainServiceDto(UUID id, String name, String fromTime, String tillTime) {
        this.id = id;
        this.name = name;
        this.fromTime = fromTime;
        this.tillTime = tillTime;
    }
}
