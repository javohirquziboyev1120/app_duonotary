package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResPayment {
    private Boolean amountOnline;
    private Boolean amountCash;
    private Boolean amountDiscount;
    private Integer payCount;
}
