package ai.ecma.duoserver.payload;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private UUID id;
    private String firstName;
    private String lastName;

    private String password;

    private String prePassword;

    private String oldPassword;

    @NotNull
    private String phoneNumber;

    @Email
    private String email;

    private UUID sharingUserId;

    private UUID photoId;

    private List<CertificateDto> certificateDtoList = new ArrayList<>();

    private Timestamp updatedAt;

    private Boolean onlineAgent;

    private boolean active;
    private AgentLocationDto agentLocationDto;
    private int countOrder;
    private Object agentOnlineHour;
    private String firebaseToken;

    public UserDto(UUID id, String firstName, String lastName, String phoneNumber, @Email String email, UUID photoId, List<CertificateDto> certificateDtoList, List<CertificateDto> passportDtoList, List<UUID> countiesId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.photoId = photoId;
        this.certificateDtoList = certificateDtoList;
        this.passportDtoList = passportDtoList;
        this.countiesId = countiesId;
    }

    public UserDto(UUID id, String firstName, String lastName, String phoneNumber, @Email String email, UUID photoId, List<CertificateDto> certificateDtoList, List<CertificateDto> passportDtoList, Timestamp updatedAt, boolean active, AgentLocationDto agentLocationDto, int countOrder, Boolean onlineAgent, Object onlineHour) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.photoId = photoId;
        this.certificateDtoList = certificateDtoList;
        this.passportDtoList = passportDtoList;
        this.updatedAt = updatedAt;
        this.active = active;
        this.agentLocationDto = agentLocationDto;
        this.countOrder = countOrder;
        this.onlineAgent = onlineAgent;
        this.agentOnlineHour = onlineHour;

    }

    public UserDto(UUID id, String firstName, String lastName, String phoneNumber, @Email String email, UUID photoId, Timestamp updatedAt, boolean active, AgentLocationDto agentLocationDto, int countOrder, Boolean onlineAgent, Object onlineHour) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.photoId = photoId;
        this.updatedAt = updatedAt;
        this.active = active;
        this.agentLocationDto = agentLocationDto;
        this.countOrder = countOrder;
        this.onlineAgent = onlineAgent;
        this.agentOnlineHour = onlineHour;
    }


    private List<CertificateDto> passportDtoList;

    private List<UUID> statesId = new ArrayList<>();
    private List<UUID> countiesId = new ArrayList<>();
    private List<UUID> zipCodesId = new ArrayList<>();

    private List<Integer> permissionsId = new ArrayList<>();

    public UserDto(String firstName, String lastName, String phoneNumber, @Email String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public UserDto(UUID id, String firstName, String lastName, String phoneNumber, @Email String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public UserDto(UUID id, String firstName, String lastName, String phoneNumber, @Email String email, Boolean onlineAgent) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.onlineAgent = onlineAgent;
    }

    public UserDto(UUID id, String firstName, String lastName, String phoneNumber, @Email String email, UUID photoId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.photoId = photoId;
    }
}
