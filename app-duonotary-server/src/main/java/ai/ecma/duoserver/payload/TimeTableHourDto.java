package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.enums.HourTypeEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TimeTableHourDto {//bu dtoda client orderga buyurtma qilayotganda tanlash uchun soatlarni bervorishda ishlatamiz
    private String hour;
    private boolean enabled;//agar vaqt bo'sh bo'lsa true, agar band bo'lsa false

    private String fromTime;
    private String tillTime;
    private HourTypeEnum hourTypeEnum;

    public TimeTableHourDto(String hour, boolean enabled) {
        this.hour = hour;
        this.enabled = enabled;
    }
}
