package ai.ecma.duoserver.payload;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PermissionDto {
    private String permissionName;
    private Integer id;
    private String authority;
    private String perName;
    private String generalName;
    private boolean active;

    public PermissionDto(Integer id, String perName) {
        this.id = id;
        this.perName = perName;
    }
}
