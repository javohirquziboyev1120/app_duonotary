package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.*;
import ai.ecma.duoserver.entity.enums.OrderPayStatusEnum;
import ai.ecma.duoserver.entity.enums.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderPdf {
    private Integer num;

    private String address;//Ma'lumot in-persondan adress olinadi

    private String zipCode;//IN-PERSON da clientning zip-codi

    private String clientFullName;//IN-PERSON da clientning zip-codi

    private String clientEmail;//IN-PERSON da clientning zip-codi

    private String  service;

    private String payType;//TO`LOV TURI YA`NI IN-PERSON VA ONLINEDAN BIRINI

    private String  agentFullName;

    private String  agentEmail;

    private Double amount; //BUYURTMANING CHEGIRMALARSIZ SUMMASI

    private double amountDiscount;//BARCHA TURDAGI CHEGIRMALARNING DOLLARDAGI MIQDORI

    private String checkNumber;//STRIPENING CHEK RAQAMI

    private String serialNumber;//GENERATE QILINADIGAN RAQAM CLIENT UCHUN

    private String orderStatus;//BUYURTMA HOLATI

    private Integer countDocument;//MIJOSNING NATARIUS QILMOQCHI BO`LGAN HUJJATLAR SONI

    private String active; //delete qilinganlari false

    private String orderPayStatusEnum;

    private String titleDocument;
}
