package ai.ecma.duoserver.payload;

import ai.ecma.duoserver.entity.enums.HourTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AgentWorkScheduleDto {
    private UUID hourOffId;
    private Timestamp fromTime;
    private Timestamp tillTime;
    private HourTypeEnum hourTypeEnum;
}
