package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdditionalServicePriceDto {
    private UUID id;

    private UUID additionalServiceId;

    private AdditionalServiceDto additionalServiceDto;

    private UUID serviceId;

    private ServicePriceDto servicePriceDto;

    private Double price;

    private boolean active;
    private Boolean serviceActive;

    private boolean all;

    public AdditionalServicePriceDto(UUID serviceId, double minPrice, double maxPrice, String subServiceName, String additionalServiceName, String mainServiceName, Boolean serviceActive) {
        this.serviceId = serviceId;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.subServiceName = subServiceName;
        this.additionalServiceName = additionalServiceName;
        this.mainServiceName = mainServiceName;
        this.serviceActive = serviceActive;
    }

    private double minPrice;

    private double maxPrice;

    private String subServiceName;

    private String additionalServiceName;

    private String mainServiceName;

    private List<UUID> statesId;

    private List<UUID> countiesId;

    private List<UUID> zipCodesId;

    public AdditionalServicePriceDto(UUID id, AdditionalServiceDto additionalServiceDto, ServicePriceDto servicePriceDto, Double price, boolean active) {
        this.id = id;
        this.servicePriceDto = servicePriceDto;
        this.additionalServiceDto = additionalServiceDto;
        this.price = price;
        this.active = active;
    }
}
