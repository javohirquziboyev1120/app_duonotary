package ai.ecma.duoserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MainServiceDto2 {

    private UUID id;

    private boolean online;

    private boolean active;

    private String name;

    private Double percent;

    private String fromTime;//ISH VAQTINING BOSHLANISHI

    private String tillTime;//ISH VAQTINING TUGASHI

    private List<HourDto> mainServiceHours;//[{fromTime:"09:00",tillTime:"12:00"},{fromTime:"15:00",tillTime:"20:00"}]

}
