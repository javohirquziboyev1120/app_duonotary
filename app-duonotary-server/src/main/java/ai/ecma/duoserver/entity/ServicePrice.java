package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
//@Table(uniqueConstraints = {//JAVOHIR
//        @UniqueConstraint(columnNames = {"service_id", "zip_code_id"})})
public class ServicePrice extends AbsEntity {
    //Servislarni zip kod bo'yicha narxi, qancha vaqtdan oldin zakasni bekor qilsa qancha
    // foiz pul yechishi.

    @ManyToOne(optional = false)
    private Service service;// sub service va main service birgalikda qushilgan service keladi

    @ManyToOne
    private ZipCode zipCode;// zipCode id si

    @Column(nullable = false)
    private Double price;// service va zipCode ga qarab belgilanadigan qiymat

    private Boolean active;// ishlayotgani yoki ishlamayotgani

    private Integer chargeMinute;// qancha vaqtdan oldin zakasni bekor qilinishi

    private Double chargePercent;// charneMinutega qarab pricening malum bir foizini yechib olish

}
