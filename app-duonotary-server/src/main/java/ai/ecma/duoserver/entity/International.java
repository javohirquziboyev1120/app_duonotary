package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class International extends AbsEntity {//SHOHRUX
    //BU XALQARO XUJJATLARNI TASDIQLASH UCHUN TO'LDIRILADIGAN MA'LUMOTLAR

    private boolean embassy;//XALQARO KONVENSIYAGA QO'SHILMAGAN DAVLAT UCHUN XUJJATMI

    private boolean someOneElse;

    @ManyToOne
    private DocumentType documentType;

    private String requesterFirstName;

    private String requesterLastName;

    private String requesterPhoneNumber;

    private String requesterEmail;

    private Integer numberDocument;

    private String pickUpAddress;

    @ManyToOne(optional = false)
    private Country country;

    @OneToOne(optional = false)
    private Order order;
}
