package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.component.HistoryListener;
import ai.ecma.duoserver.entity.template.AbsNameEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class County extends AbsNameEntity {
    // AQSH Shtatni ichidagi tumanlar
    @ManyToOne
    private State state;
}
