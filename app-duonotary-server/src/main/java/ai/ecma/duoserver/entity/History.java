package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.config.JpaConverterJson;
import ai.ecma.duoserver.entity.enums.OperationEnum;
import ai.ecma.duoserver.entity.template.AbsEntity;
import ai.ecma.duoserver.entity.template.AbsEntityForOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class History extends AbsEntityForOrder {//SIROJIDDIN

//BU CLASS HAR BIR TABLELARDAGI O'ZGARISHLARNI SHU TABLEGA YOZIB BORADI!!!

    private String tableName;//O'ZGARISH BULGAN TABLE NOMINI QAYD QILIB BORADI!!!

    private UUID objectId;//O'ZGARISH  BULAYOTGAN OBJECTNI ID SI!!

    @Enumerated(EnumType.STRING)
    private OperationEnum operationEnum;// QANAQA O'ZGARISH BULAYOTGANINI QAYD ETADIGAN ENUM!!!

    @Convert(converter = JpaConverterJson.class)
    @Column(columnDefinition = "text")
    private Object object; //O'ZGARISH BULAYOTGAN OBJECTNI O'ZI




}
