package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CanceledOrder extends AbsEntity {

    @ManyToOne(optional = false)
    User fromAgent;
    @ManyToOne(optional = false)
    User toAgent;
    @ManyToOne(optional = false)
    Order order;
    Boolean accept;
}
