package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Hour extends AbsEntity {
    @Column(nullable = false)
    private String fromTime;//18:30

    @Column(nullable = false)//09:30 , 09:00
    private String tillTime;

    @ManyToOne(fetch = FetchType.LAZY)
    private AgentSchedule agentSchedule;

}