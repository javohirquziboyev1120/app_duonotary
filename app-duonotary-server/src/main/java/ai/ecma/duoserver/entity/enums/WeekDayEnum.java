package ai.ecma.duoserver.entity.enums;

public enum WeekDayEnum {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
}
