package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.enums.ServiceEnum;
import ai.ecma.duoserver.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.awt.*;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class SubService extends AbsNameEntity {
    //BU TABLE NOTARY, REAL ESTATE, INTERNATIONAL SHU KABI SERVICELAR UCHUN. MAIN SERVICEGA DAXLI YO'Q

    private boolean defaultInput = false;//BU FILD ONLINEDA sub-service bo`lmaganligi uchun ushbu field true bo`lgan subServiceni servicega o`rnatamiz

    @Enumerated(EnumType.STRING)
    private ServiceEnum serviceEnum;



}
