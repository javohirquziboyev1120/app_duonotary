package ai.ecma.duoserver.entity.enums;

public enum OrderFeedback {
    BOTH,
    AGENT,
    CLIENT,
    NONE
}
