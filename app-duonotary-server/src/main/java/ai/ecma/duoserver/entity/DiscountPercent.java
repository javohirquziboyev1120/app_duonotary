package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor

@Table(name="discountPercent",  uniqueConstraints={
        @UniqueConstraint(columnNames={"zip_code_id", "percent"}),
//        @UniqueConstraint(columnNames={"zip_code_id", "online"})
})
public class DiscountPercent extends AbsEntity {//ABUROZZAQ

    @Column(nullable = false)
    private Double percent;

    @ManyToOne
    private ZipCode zipCode;

    private boolean active;

    private boolean online;
}
