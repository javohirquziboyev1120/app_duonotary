package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
 @Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"week_day_id", "user_id"})})
public class AgentSchedule extends AbsEntity {
    @ManyToOne(optional = false)
    private WeekDay weekDay;//sunday

    @ManyToOne(optional = false)
    private User user;//agent

    private boolean dayOff;//dam olish kunimi yoki yo'qmi
}
