package ai.ecma.duoserver.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class TimeBooked {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private Integer bookedDuration; // BU AGENTNI BOOKED QLISH VAQTI YANI BOOKED QLNSA SHUNCHA VAQTDA BOOKED YECHILADI
}
