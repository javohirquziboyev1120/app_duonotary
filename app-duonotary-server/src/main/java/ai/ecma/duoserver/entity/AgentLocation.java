package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntityForOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;


@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class AgentLocation extends AbsEntityForOrder {
    //BU TABLE AGENTNI QAYR=ERDA YURGANLIGI HAQIDAGI MA'LUMOTLAR

    @ManyToOne(optional = false)
    private User agent;

    private String zipCode;

    private String fullLocation;

    private Float lat;//AGENT UCHUN. QAYSI KENGLIKDA EKANLIGI

    private Float lon;//AGENT UCHUN. QAYERDA UZUNLIKDA EKANLIGI


}
