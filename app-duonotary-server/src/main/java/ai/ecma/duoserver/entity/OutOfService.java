package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"zipCode", "email"})
})
public class OutOfService extends AbsEntity {
    //ZIP CODE LAR BO'YICHA BIZNI XIZMATIMIZGA QIZIQQANDA
    // USHBU ZIP CODE DA BIZ XIZMAT KO'RSTAMASAK, O'SHA MIJOZNING EMAILINI SO'RAYMIZ.
    // VA BIZDA SHU ZIP CODE OCHILSA ULARA EMAIL DAN XABAR JO'NATAMIZ

    @Column(nullable = false)
    private String zipCode;

    @Column(nullable = false)
    private String email;

    private boolean sent;//xabar jo'natlgan
}
