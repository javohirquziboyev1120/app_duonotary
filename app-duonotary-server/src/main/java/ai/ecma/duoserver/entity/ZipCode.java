package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ZipCode extends AbsEntity {//JAVOHIR MANNONOV
// Bu table tumanlar uchun Zip cod lar
    @Column(nullable = false,unique = true)
    private String code;
    
    @ManyToOne(optional = false)
    private County county;

    @Column (nullable = false)
    private String city;

    private boolean active=true;

    @ManyToOne(optional = false)
    private TimezoneName timezone;
}
