package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import ai.ecma.duoserver.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"main_service_id", "date","name"})})
public class Holiday extends AbsEntity {
    // bu table bayram kunlarini qayt qiladi
    @ManyToOne
    private MainService mainService;

    @Column(nullable = false)
    private String name;

    private boolean active;

    @Column(columnDefinition = "text")
    private String description;

    @Column(nullable = false)
    private Date date;


    public Holiday(UUID id, MainService mainService, String name, boolean active, String description, Date date) {
        this.setId(id);
        this.mainService = mainService;
        this.name = name;
        this.active = active;
        this.description = description;
        this.date = date;
    }

}
