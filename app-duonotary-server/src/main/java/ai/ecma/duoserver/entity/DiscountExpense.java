package ai.ecma.duoserver.entity;


import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiscountExpense extends AbsEntity {

    @ManyToOne
    private Order order;//Discount ishlatilayotgan order

    private Double amount;//discountning dollardagi miqdori

    private UUID discountId;//sharing yoki loyalt discountning idsi

    private boolean sharingId = false;//sharing id mi yoki loyalty id ekanligini bilish uchun boolean
    private boolean loyaltyId = false;//sharing id mi yoki loyalty id ekanligini bilish uchun boolean
    private boolean customId = false;//sharing id mi yoki loyalty id ekanligini bilish uchun boolean
    private boolean firstOrder = false;//sharing id mi yoki loyalty id ekanligini bilish uchun boolean

    public DiscountExpense(Double amount, UUID discountId, boolean sharingId) {
        this.amount = amount;
        this.discountId = discountId;
        this.sharingId = sharingId;
    }

    public DiscountExpense(Order order, double amount, UUID discountId, boolean sharingId, boolean loyaltyId, boolean customId) {
        this.order = order;
        this.amount = amount;
        this.discountId = discountId;
        this.sharingId = sharingId;
        this.loyaltyId = loyaltyId;
        this.customId = customId;
    }

    public DiscountExpense(Order order, double amount, UUID discountId, boolean sharingId, boolean loyaltyId, boolean customId, boolean firstOrder) {
        this.order = order;
        this.amount = amount;
        this.discountId = discountId;
        this.sharingId = sharingId;
        this.loyaltyId = loyaltyId;
        this.customId = customId;
        this.firstOrder = firstOrder;
    }
}
