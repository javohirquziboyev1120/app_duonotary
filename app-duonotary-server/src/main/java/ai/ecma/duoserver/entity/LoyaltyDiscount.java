package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class LoyaltyDiscount extends AbsEntity {//SHOHRUX

    @ManyToOne(optional = false)
    private User user;

    @ElementCollection
    private List<UUID> orders;


    @Column(nullable = false)
    private Double percent;

    @Column(nullable = false)
    private Double amount;

    @Column(nullable = false)
    private Double leftover;


}
