package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class PayType  extends AbsNameEntity {

    private boolean online;
}
