package ai.ecma.duoserver.entity.enums;

public enum OrderPayStatusEnum {
    PAID,
    UNPAID
}
