package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.enums.PayStatus;
import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.function.Supplier;

@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Payment extends AbsEntity implements Supplier<Payment> {
    @ManyToOne
    private Order order;

    private Double paySum;

    @Enumerated(EnumType.STRING)
    private PayStatus payStatus;

    private String checkNumber;

    @ManyToOne(optional = false)
    private PayType payType;

    @Column(nullable = false)
    private String clientId;

    private String chargeId;

    private Timestamp date = new Timestamp(new Date().getTime());

    @Override
    public Payment get() {
        return new Payment();
    }

    public Payment(Order order, Double paySum, PayStatus payStatus, String checkNumber, PayType payType, String clientId, String chargeId) {
        this.order = order;
        this.paySum = paySum;
        this.payStatus = payStatus;
        this.checkNumber = checkNumber;
        this.payType = payType;
        this.clientId = clientId;
        this.chargeId = chargeId;
    }
}
