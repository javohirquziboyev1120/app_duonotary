package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CustomDiscount extends AbsEntity {//ISLAM

    //HAR BIR MIJOZ UCHUN ALOHIDA RAVISHDA BERILADIGAN CHEGIRMALAR.
    // MISOL UCHUN SISTEMA TOMONIDAN XATO BO'LGANDA MIJOZNING
    // BUYURTMASINING ASL SUMMASIGA CHEGIRMA BERILADI

    @OneToOne(optional = false)
    private Order order;

    @Column(nullable = false)
    private Double percent;

    @Column(nullable = false)
    private Double amount;

    @Column(columnDefinition = "text")
    private String description;

}
