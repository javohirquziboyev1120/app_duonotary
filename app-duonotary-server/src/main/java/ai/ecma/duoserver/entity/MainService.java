package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@javax.persistence.Embeddable
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class MainService extends AbsNameEntity {
    //BU TABLE PROJECT UCHUN INPERSON, ONLINE LARNI QAYD ETADI

    private String fromTime;//ISH VAQTINING BOSHLANISHI

    private String tillTime;//ISH VAQTINING TUGASHI

    private boolean online;//BU ONLINE YOKI IN-PRESON EKNALIGINI BILISH UCHUN

    private Integer orderNumber;//BU NICHINCHI BULIB KURINISHI ONLINE OR IN-PERSON


}
