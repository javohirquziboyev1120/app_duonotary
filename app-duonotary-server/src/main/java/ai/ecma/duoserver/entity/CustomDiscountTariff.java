package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CustomDiscountTariff extends AbsEntity {//ISLAM

    //Bu table da admin yoki super admin tomonidan alohida mijozga atab berilgan chegirmalar yozildi.
    // Masalan bir mijozga sizga kelasi 5 ta buyurtmangizga har biri uchun 20 % chegirma beramiz desa  shu tablega yoziladi va
    // ishlatilganlari soni ham shu yerda qayd etiladi .

    @ManyToOne(optional = false)
    private User customer;

    @Column(nullable = false)
    private Double percent;

    private Integer discountGivenOrderCount;
    private int usingDiscountGivingOrderCount;

    private boolean unlimited;
    private boolean active;
}
