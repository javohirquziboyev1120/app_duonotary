package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.enums.OrderPayStatusEnum;
import ai.ecma.duoserver.entity.enums.OrderStatus;
import ai.ecma.duoserver.entity.enums.RegisterBy;
import ai.ecma.duoserver.entity.template.AbsEntityForOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity(name = "orders")
@AllArgsConstructor
@NoArgsConstructor
public class Order extends AbsEntityForOrder {//SIROJIDDIN

    private String address;//Ma'lumot in-persondan adress olinadi

    @ManyToOne
    private ZipCode zipCode;//IN-PERSON da clientning zip-codi

    @ManyToOne(optional = false)
    private User client;   // BUYURTMA BERUVCHI MIJOZ

    private String lat;//IN-PERSON CLIENTNING MAP ADDRESS

    private String lng;//IN-PERSON CLIENTNING MAP ADDRESS

    @ManyToOne(optional = false)
    private ServicePrice servicePrice;//clientning ikkala zakaz holatida xizmat turini qayd etadi(online/in-person)

    @ManyToOne(optional = false)
    private PayType payType;//TO`LOV TURI YA`NI IN-PERSON VA ONLINEDAN BIRINI

    @ManyToOne(optional = false)
    private User agent; //BUYURTMA BILAN ISHLOVCHI HODIM

    @Column(nullable = false)
    private Double amount; //BUYURTMANING CHEGIRMALARSIZ SUMMASI

    private double amountDiscount;//BARCHA TURDAGI CHEGIRMALARNING DOLLARDAGI MIQDORI

    private String checkNumber;//STRIPENING CHEK RAQAMI

    @Column(nullable = false, unique = true)
    private String serialNumber;//GENERATE QILINADIGAN RAQAM CLIENT UCHUN

    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;//BUYURTMA HOLATI

    @Column(nullable = false)
    private Integer countDocument;//MIJOSNING NATARIUS QILMOQCHI BO`LGAN HUJJATLAR SONI

    @OneToMany
    private List<Attachment> documents;//HUJJATLARI

    @OneToMany
    private List<Attachment> filesFromDocVerify;//Bu yerda DocVerify dan yuklab olingan tayyor bolgan filelarni joylashtiramiz.

    private String docVerifyId; //DOC VERIFY TIZIMIDAGI YUKLANADIGAN HUJJATNING IDSI

    private boolean packet; //DOC VERIFY TIZIMIDAGI YUKLANADIGAN HUJJATNING TURI PACKET BO`LSA SHU FIELD TRUE BO`LADI VA DOC-VERIFYIDDA PACKET_ID

    private boolean active = true; //delete qilinganlari false

    private OrderPayStatusEnum orderPayStatusEnum;

    //Bu Field orqali stripedan olingan  charge Id saqlanadi, future Charge Id
    private String chargeId;

    private String titleDocument;

    private Integer editCount = 0;

    @Enumerated(EnumType.STRING)
    private RegisterBy registerBy;

    public Order(String address, ZipCode zipCode, User client, String lat, String lng, ServicePrice servicePrice, PayType payType,
                 User agent, Double amount, Integer countDocument,
                 String checkNumber, List<Attachment> documents, OrderStatus orderStatus, String serialNumber) {
        this.address = address;
        this.zipCode = zipCode;
        this.client = client;
        this.lat = lat;
        this.lng = lng;
        this.servicePrice = servicePrice;
        this.payType = payType;
        this.agent = agent;
        this.amount = amount;
        this.countDocument = countDocument;
        this.checkNumber = checkNumber;
        this.documents = documents;
        this.orderStatus = orderStatus;
        this.serialNumber = serialNumber;
    }

    public Order(User client, ServicePrice servicePrice, User agent, Double amount, Integer countDocument,
                 String checkNumber, List<Attachment> documents, OrderStatus orderStatus, String serialNumber, PayType payType) {
        this.client = client;
        this.servicePrice = servicePrice;
        this.agent = agent;
        this.amount = amount;
        this.countDocument = countDocument;
        this.checkNumber = checkNumber;
        this.documents = documents;
        this.orderStatus = orderStatus;
        this.serialNumber = serialNumber;
        this.payType = payType;
    }

}
