package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.component.HistoryListener;
import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(HistoryListener.class)
public class FeedBack extends AbsEntity {//ISLAM
    //KLIENT VA ROLE_AGENT ZAKAZ BO'YICHA O'ZLARINING BAHOLARINI BERADI

    @ManyToOne(optional = false)
    private Order order;

    private boolean agent;

    @Column(nullable = false)
    private Integer rate;

    @Column(columnDefinition = "text")
    private String description;

    @Column(columnDefinition = "text")
    private String answer;

    private boolean seen=false;

    public FeedBack(Order order, boolean agent, Integer rate, String description, boolean seen) {
        this.order = order;
        this.agent = agent;
        this.rate = rate;
        this.description = description;
        this.seen = seen;
    }
}
