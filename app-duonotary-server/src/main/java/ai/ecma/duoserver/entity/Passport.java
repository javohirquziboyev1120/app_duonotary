package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.enums.StatusEnum;
import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;


@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Passport extends AbsEntity {//ISLAM
    @ManyToOne(optional = false)
    private User user;

    @OneToOne(optional = false)
    private Attachment attachment;

    @Column(nullable = false)
    private Date issueDate;

    @Column(nullable = false)
    private Date expireDate;

    @Enumerated(EnumType.STRING)
    private StatusEnum statusEnum;//hujjatni admin tasdiqlaganligi holati

    private boolean expired;//muddati o'tganmi?


}
