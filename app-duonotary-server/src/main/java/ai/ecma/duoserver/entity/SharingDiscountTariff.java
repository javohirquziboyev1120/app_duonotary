package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GeneratorType;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class SharingDiscountTariff extends AbsEntity {

    // BU TABLEDA USER SHARE QILISH ORQALI KELGAN USERLAR UCHUN BERILADIGA CHEGIRMALAR
    private Double percent;
    private  boolean active;

    @OneToOne(fetch = FetchType.LAZY)
    private ZipCode zipCode;

    private boolean online;
}
