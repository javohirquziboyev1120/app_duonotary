package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class FirstOrderDiscountTariff extends AbsEntity {
    private boolean active;
    private Double percent;

    @OneToOne(fetch = FetchType.LAZY)
    private ZipCode zipCode;

    private boolean online;
}
