package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class SharingDiscount extends AbsEntity {//JAVOHIR MANNONOV
    // userga discount berilishi yani saytga boshqalarni taklif qilganligi uchun unga malum miqdorda discount ajralishini yoziladigan table

    @ManyToOne(optional = false)
    private User client;//clientni id isi keladi

    @Column(nullable = false)
    private Double percent;// nechi foiz skidka berishimiz

    @Column(nullable = false)
    private Double leftover;// $ da berilgan skidkani qanchasi qogani

    private Double amount; // $ da qancha skidka berilgani

    @OneToOne
    private Order order;// qaysi ordergan olganligi

}
