package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(uniqueConstraints = {//DIOR
        @UniqueConstraint(columnNames = {"service_price_id", "additional_service_id"})})
public class AdditionalServicePrice extends AbsEntity {
    //Qo'shimcha xizmatlar va ularning narxlari, qaysi ServicePrice kirishi

    @ManyToOne(optional = false)
    private ServicePrice servicePrice; //QAYSI SERVICE PRICEGA KIRISHI

    @ManyToOne(optional = false)
    private AdditionalService additionalService; //QAYSI ADDITIONAL SERVICEGA TEGISHLIGI

    @Column(nullable = false)
    private Double price; //QO'SHIMCHA XIZMAT NARXI

    private boolean active; //AMALDA YOKI YOQLIGI
}
