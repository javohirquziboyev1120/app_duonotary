package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class OrderAdditionalService extends AbsEntity {//SIROJIDDIN

    // BU TABLE BITA ORDER UCHUN QO'SHMCHA XIZMATLAR
    // MISOL: (GUVOH CHAQIRISH, QOSHIMCHA HUJJAT OBKELISH)

    @ManyToOne(optional = false)
    private Order order;

    @ManyToOne(optional = false)
    private AdditionalServicePrice additionalServicePrice;

    @Column(nullable = false)
    private Integer count;

    private String description;

    @Column(nullable = false)
    private Double currentPrice;
}
