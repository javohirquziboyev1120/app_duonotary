package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.enums.RealEstateEnum;
import ai.ecma.duoserver.entity.template.AbsEntity;
import ai.ecma.duoserver.utils.Time;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import java.sql.Timestamp;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class RealEstate extends AbsEntity {
    //BU UY JOY OLDI-SOTDI SHARTNOMALARI QILISH
    // UCHUN TO'LDIRILADIGAN MA'LUMOTLAR

    private String clientName;

    private String clientPhone;

    private String clientEmail;

    private String clientAddress;

    @Enumerated(EnumType.STRING)
    private RealEstateEnum realEstateEnum;
    @OneToOne
    private Order order;
}
