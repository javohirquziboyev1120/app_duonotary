package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Pricing extends AbsEntity {//JAVOHIR
    //USHBU TABLE HAR BIR HUJJAT SONIGA QARAB NARX BELGILASH UCHUN ISHLATILADI.
    // AGAR EVERYCOUNT NULL BO'LMASA HAR BIR HUJJAT UCHUN SHU NARX.
    //TILLCOUNT NULL BO'LSA FROMCOUNT DAN BOSHLAB HAR BIR HUJJAT UCHUN SHU NARX

    @ManyToOne(optional = false)
    private ServicePrice servicePrice;// bu narx qaysi servicePRice ga tegishligi

    @Column(nullable = false)
    private Double price;// documentlarning soniga qarab belgilanadigan qiymat

    private boolean active;// ishlayotgani yoki ishlamayotgani

    private Integer fromCount;// documentlarning boshlang'ich sonini shunday

    private Integer tillCount;// documentlarning tugash sonini shungacha

    private Integer everyCount;// har bir document uchun\

    @ManyToOne
    private State state;

}
