package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"fromTime", "main_service_id"}),
        @UniqueConstraint(columnNames = {"tillTime", "main_service_id"})})
public class MainServiceWorkTime extends AbsEntity {
    // Bu table main serviceni ishlash vaqti, ish vaqtidan tashqarida % qoshiladi ;
    @ManyToOne
    private MainService mainService;

    private String fromTime;

    private String tillTime;

    private double percent;

    private boolean active;

}
