package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.component.HistoryListener;
import ai.ecma.duoserver.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(HistoryListener.class)
@Entity
public class Country extends AbsNameEntity {

    private boolean embassy;
}





