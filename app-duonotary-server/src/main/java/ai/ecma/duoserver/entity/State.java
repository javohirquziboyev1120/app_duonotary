package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsNameEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Embeddable;
import javax.persistence.Entity;


@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class State extends AbsNameEntity {
    // AQSH shtatlari
}
