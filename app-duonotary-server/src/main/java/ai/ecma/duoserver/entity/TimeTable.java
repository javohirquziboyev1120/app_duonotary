package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimeTable extends AbsEntity {//ABDUROZZAQ
    // BU CLASS ENDI AGENT BAND QILINSAGINA YOZILADI

    @ManyToOne(optional = false)
    private User agent;

    @Column(nullable = false)
    private Timestamp fromTime; // BUYURTMANING BOSHLANISH VAQT //10:00

    @Column(nullable = false)
    private Timestamp tillTime; // BUYURTMANING TUGASH VAQT //12:00

    @ManyToOne
    private Order order; // QAYSI O'RDER UCHUN BAND QLINGAN

    private boolean tempBooked; // VAQTINCHA BAND QLINDIMI ?

    private UUID tempId;//Ro'yhatdan o'tmasdan buyurtma berayotgan bo'lsa, vaqtincha klientni aniqlab olish uchun
                        //Ro`yhatdan o`tgan bo`lsa shu clientni id uchun
    private boolean online;

    private boolean active=true;

    private Boolean remainder;

    private Integer gmt=0;

    public TimeTable(User agent, Timestamp fromTime, Timestamp tillTime, Order order, boolean tempBooked, UUID tempId, boolean online) {
        this.agent = agent;
        this.fromTime = fromTime;
        this.tillTime = tillTime;
        this.order = order;
        this.tempBooked = tempBooked;
        this.tempId = tempId;
        this.online = online;
    }
}
