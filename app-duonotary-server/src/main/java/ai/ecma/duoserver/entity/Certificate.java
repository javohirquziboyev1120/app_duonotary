package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.enums.StatusEnum;
import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Certificate extends AbsEntity {
    @ManyToOne(optional = false)
    private User user;//agent

    @OneToOne(optional = false)
    private Attachment attachment;//fayli

    @Column(nullable = false)
    private Date issueDate;//berilgan vaqti

    @Column(nullable = false)
    private Date expireDate;//amal qilish muddati

    private boolean expired;//muddati o'tganmi?

    @ManyToOne(optional = false)
    private State state;//qaysi tuman uchun

    @Enumerated(EnumType.STRING)
    private StatusEnum statusEnum;//hujjatni admin tasdiqlaganligi holati


}
