package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.enums.EmailStatus;
import ai.ecma.duoserver.entity.template.AbsEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.javers.core.metamodel.annotation.DiffIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "users")
public class User extends AbsEntity implements UserDetails {
    @Column(nullable = false)
    private String firstName;//ISMI

    @Column(nullable = false)
    private String lastName;//FAMILYASI

    @Column(nullable = false)
    private String phoneNumber;//TELEFON RAQAMI. BUNDAN USERNAME SIFATIDA FOYDLANS HAM BO'LADI

    @Column(nullable = false)
    private String email;//XAT YUBORISH UCHUN EMAIL. BUNDAN USERNAME SIFATIDA FOYDLANS HAM BO'LADI

    private String changingEmail;

    @JsonIgnore
    @Column(nullable = false)
    @DiffIgnore
    private String password;//PAROLI

    private String emailCode;//USERGA EMAILGA  TASDIQLASHI UCHUN uuid GENERATSIYA QILGAN VA YUBORGAN KODIMIZ.

    private boolean active;//AGENT UCHUN. AGAR FALSE BO'LSA ZAKAZ OLOLMAYDI VA ZAKAZ BILAN ISHLOLMAYDI

    private boolean online;//AGENT UCHUN. APPLICATION DAGI HOLATI

    @DiffIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_role",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private Set<Role> roles;//USERNING ROLELARI

    @DiffIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_permission",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "permission_id")})
    private Set<Permission> permissions;//USERNING HUQUQLARI

    @OneToOne
    private Attachment photo;//USERNING AVATAR PHOTOSI

    @ManyToOne
    private User sharingUser;//USERNI KIM SISTEMAGA TAKLIF ETGANI

    private Boolean onlineAgent;

    private String stripeCustomerId;//

    @Enumerated(EnumType.STRING)
    private EmailStatus emailStatus;

    @DiffIgnore
    private boolean isAccountNonExpired = true;
    @DiffIgnore
    private boolean isAccountNonLocked = true;
    @DiffIgnore
    private boolean isCredentialsNonExpired = true;
    private boolean enabled = false;
    private Integer gmt = 0;
    private String firebaseToken;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> grantedAuthorityList = new HashSet<>();
        if (permissions != null)
            grantedAuthorityList.addAll(permissions);
        grantedAuthorityList.addAll(roles);
        return grantedAuthorityList;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return isAccountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isAccountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return isCredentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public User(String firstName, String lastName, String phoneNumber, String email,
                String password, String emailCode, Set<Role> roles, User sharingUser, boolean enabled, Attachment photo) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.password = password;
        this.emailCode = emailCode;
        this.roles = roles;
        this.sharingUser = sharingUser;
        this.enabled = enabled;
        this.photo = photo;
    }

    public User(String firstName, String lastName, String phoneNumber, String email,
                String password, String emailCode, Set<Role> roles, User sharingUser, boolean enabled, Set<Permission> permissions, EmailStatus emailStatus) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.password = password;
        this.emailCode = emailCode;
        this.roles = roles;
        this.sharingUser = sharingUser;
        this.enabled = enabled;
        this.permissions = permissions;
        this.emailStatus = emailStatus;
    }

    @Override
    public String getPassword() {
        return password;
    }


}
