package ai.ecma.duoserver.entity.enums;

public enum  PayStatus {
    PAYED, BACKOFF, STRIPE, SCHEDULE
}
