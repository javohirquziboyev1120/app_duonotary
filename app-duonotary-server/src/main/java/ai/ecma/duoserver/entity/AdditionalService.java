package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsNameEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class AdditionalService extends AbsNameEntity {
    //QO'SHIMCHA XIZMATLAR UCHUN. MISOL UCHUN GUVOH, RANGLI QOG'OZ PECHAT QILISH VA H.K
}
