package ai.ecma.duoserver.entity.enums;


public enum OperationEnum {
    INSERT, UPDATE, DELETE, GET
}
