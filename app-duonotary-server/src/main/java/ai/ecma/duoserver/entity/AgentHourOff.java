package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class AgentHourOff extends AbsEntity {
    // AGENT DAM OLISH VAQTLARI XOXLAGAN PAYTIDA BELGILAYDI FAQAT BUYURTMA OLNMAGAN BO'LSA
    // MASALAN AGENT 11:00:00 AM DAN 12:30:00 GACHA DAM OLMOQCHI BO'LDI VA SHU VAQTLARNI BELGILAYDI QACHONKI SHU VAQTLARDA BUYUTMA YOZILMAGAN BO'LSA
    @ManyToOne(optional = false)
    private User agent;
    @Column(nullable = false)
    private Timestamp fromHourOff; // DAN
    @Column(nullable = false)
    private Timestamp tillHourOff; // GACHA
}
