package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class LoyaltyDiscountTariff{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    // BU TABLEDA SODIQLIK VA FAOLLIK  UCHUN BERILADIGAN OYLIK YILLIK BONUSLARNING TARIFLARI


    private Integer month;//AGAR MONTH NULL BO'LSA HAR BIR SAVDODAN SKITKA BERILADIGAN BOLADI AKS HOLDA TARIFFGA QARAB SKITKA BELGILANADI

    private Double percent;//SKITKANING FOIZDAGI QIYMATI

    private boolean active;//SKITKANING BOR YOGLIGI

    private Double border;// SIKIDKA BERISH MINIMAL QIYMATI

}
