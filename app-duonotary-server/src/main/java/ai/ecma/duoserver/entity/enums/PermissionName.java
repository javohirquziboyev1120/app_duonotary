package ai.ecma.duoserver.entity.enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum PermissionName {

    /**
     * START DATA REST PERMISSION ENUM
     */

    //COUNTRY ENTITY {started:Abdurozzaq}
    DELETE_COUNTRY("Delete country", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage country"),
    SAVE_COUNTRY("Add and edit country", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage country"),
    //{end:Abdurozzaq}

    //PAY TYPE ENTITY
    DELETE_PAY_TYPE("Delete pay type", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage pay type"),
    SAVE_PAY_TYPE("Add and edit pay type", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage pay type"),

    //STATE ENTITY
    DELETE_STATE("Delete state", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage state"),
    SAVE_STATE("Add and edit state", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage state"),

    //DISCOUNT PERCENT ENTITY
    DELETE_DISCOUNT_PERCENT("Delete discount percent", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage discount percent"),
    SAVE_DISCOUNT_PERCENT("Add and edit discount percent", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage discount percent"),

    //LOYALTY DISCOUNT TARIFF ENTITY
    DELETE_LOYALTY_DISCOUNT_TARIFF("Delete loyalty discount tariff", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage loyalty discount tariff"),
    SAVE_LOYALTY_DISCOUNT_TARIFF("Add and edit loyalty discount tariff", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage loyalty discount tariff"),

    //MAIN SERVICE ENTITY
    DELETE_MAIN_SERVICE("Delete main service", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage main service"),
    SAVE_MAIN_SERVICE("Add and edit main service", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage main service"),

    //SUB SERVICE ENTITY
    DELETE_SUB_SERVICE("Delete sub service", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage sub service"),
    SAVE_SUB_SERVICE("Add and edit sub service", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage sub service"),

    //WEEK DAY ENTITY
    DELETE_WEEK_DAY("Delete week days", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage week day"),
    SAVE_WEEK_DAY("Add and edit sub week days", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage week day"),

    //TIME BOOKED ENTITY
    DELETE_TIME_BOOKED("Delete time booked", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage time booked"),
    SAVE_TIME_BOOKED("Add and edit sub time booked", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage time booked"),

    //TIME DURATION ENTITY
    DELETE_TIME_DURATION("Delete time duration", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage time duration"),
    SAVE_TIME_DURATION("Add and edit time duration", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage time duration"),

    //SHARING DISCOUNT TARIFF ENTITY
    DELETE_SHARING_DISCOUNT_TARIFF("Delete sharing discount tariff", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage sharing discount tariff"),
    SAVE_SHARING_DISCOUNT_TARIFF("Add and edit sharing discount tariff", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage sharing discount tariff"),

    //ADDITIONAL SERVICE ENTITY
    DELETE_ADDITIONAL_SERVICE("Delete additional service", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage additional service"),
    SAVE_ADDITIONAL_SERVICE("Add and edit additional service", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage additional service"),

    //COUNTY ENTITY
    DELETE_COUNTY("Delete county", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage county"),
    SAVE_COUNTY("Add and edit county", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage county"),

    /**
     * FINISH DATA REST PERSMISSION ENUM
     */

//    VERIFY_EMAIL(Arrays.asList(RoleName.ROLE_USER, RoleName.ROLE_AGENT, RoleName.ROLE_ADMIN)),
//    GET_STATE(Arrays.asList(RoleName.ROLE_USER, RoleName.ROLE_AGENT, RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN)),
//    //    GET_COUNTY, GET_PAY_TYPE,
////    GET_COUNTRY_LIST, GET_STATE_LIST, GET_COUNTY_LIST, GET_PAY_TYPE_LIST,
////    ADD_COUNTRY, ADD_STATE, ADD_COUNTY, ADD_PAY_TYPE,
////    EDIT_COUNTRY, EDIT_STATE, EDIT_COUNTY, EDIT_PAY_TYPE,
////    DELETE_COUNTRY, DELETE_STATE, DELETE_COUNTY, DELETE_PAY_TYPE,
////    REGISTER_ROLE_USER,
////    VERIFY_EMAIL,
////    LOGIN

    //SERVICE ENTITY {started:Diyorbek}
    ADD_SERVICE("Add Service", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage service"),
    EDIT_SERVICE("Edit Service", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage service"),
    CHANGE_ACTIVE_SERVICE("Change active Service", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage service"),
    CHANGE_DYNAMIC_SERVICE("Change dynamic Service", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage service"),
    GET_SERVICE("Get Service", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage service"),
    //{end:Diyorbek}

    //ADDITIONAL_SERVICE_PRICE ENTITY {started:Diyorbek}
    SAVE_ADDITIONAL_SERVICE_PRICE("Add additional Service Price", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage additional service price"),
    EDIT_ADDITIONAL_SERVICE_PRICE("Edit additional Service Price", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage additional service price"),
    GET_ADDITIONAL_SERVICE_PRICE("Get additional Service Price", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage additional service price"),
    ACTIVE_ADDITIONAL_SERVICE_PRICE("Change active additional Service Price", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage additional service price"),
    DELETE_ADDITIONAL_SERVICE_PRICE("Delete additional Service Price", Collections.singletonList(RoleName.ROLE_SUPER_ADMIN), "Manage additional service price"),
    //{end:Diyorbek}

    //{STARTED:imirsaburov}

    FOR_AGENT_AND_USER_FEEDBACK("FeedBack actions for  agent and  user", Arrays.asList(RoleName.ROLE_AGENT, RoleName.ROLE_USER, RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage feedback"),
    FOR_ADMIN_FEEDBACK("FeedBack actions for  admin", Arrays.asList(RoleName.ROLE_SUPER_ADMIN, RoleName.ROLE_ADMIN), "Manage feedback"),

    FOR_AGENT_AGENT_ACTION("Agent actions for agent", Arrays.asList(RoleName.ROLE_AGENT, RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage agent"),
    FOR_ADMIN_AGENT_ACTION("Agent actions for Admin", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage agent"),

    PDF_EXCEL("file download actions for admin", Arrays.asList(RoleName.ROLE_SUPER_ADMIN, RoleName.ROLE_ADMIN), "Manage File"),


    ADD_CUSTOM_DISCOUNT("Add custom discount", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage discount"),
    GET_ONE_CUSTOM_DISCOUNT("Get custom discount", Arrays.asList(RoleName.ROLE_USER, RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage discount"),

    ADD_FEED_BACK("Add feed Back", Arrays.asList(RoleName.ROLE_USER, RoleName.ROLE_AGENT)),
    SEEN_FEED_BACK("Seen feed backs", Arrays.asList(RoleName.ROLE_SUPER_ADMIN, RoleName.ROLE_ADMIN), "Manage feedback"),


    //{ended:imirsaburov}
    //PRICING ENTITY {STARTED:JAVOHIR}
    ADD_PRICING("Add pricing", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage pricing"),
    EDIT_PRICING("Edit pricing", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage pricing"),
    GET_PRICING("Get pricing", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN, RoleName.ROLE_AGENT, RoleName.ROLE_USER), "Manage pricing"),
    //{end:JAVOHIR}
    //SERVICE PRICE ENTITY {STARTED:JAVOHIR}
    ADD_SERVICE_PRICE("Add servicePrice", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage service price"),
    EDIT_SERVICE_PRICE("Edit servicePrice", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage service price"),
    GET_SERVICE_PRICE("Get servicePrice", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN, RoleName.ROLE_AGENT, RoleName.ROLE_USER), "Manage service price"),
    //{end:JAVOHIR}

    //ORDER ENTITY {started: Sirojiddin}
    SAVE_ORDER("Add ORDER", Arrays.asList(RoleName.ROLE_USER, RoleName.ROLE_AGENT, RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage order"),
    UPDATE_ORDER_STATUS("Edit order status", Arrays.asList(RoleName.ROLE_AGENT, RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage order"),
    DELETE_ORDER("Delete order", Arrays.asList(RoleName.ROLE_USER, RoleName.ROLE_AGENT, RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage order"),
    GET_ORDER_LIST("Get orders list", Arrays.asList(RoleName.ROLE_USER, RoleName.ROLE_AGENT, RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage order"),
    GET_ORDER_LIST_FOR_ADMIN("Get orders for admin list", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage order"),
    GET_ORDER("Get order", Arrays.asList(RoleName.ROLE_USER, RoleName.ROLE_AGENT, RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage order"),

    //HISTORY ENTITY
    GET_HISTORY("Get history", Arrays.asList(RoleName.ROLE_SUPER_ADMIN), "Get history"),
    //{ended: Sirojiddin}

    UPLOAD_FILE_ATTACHMENT("Attachment upload file", Arrays.asList(RoleName.ROLE_USER, RoleName.ROLE_AGENT, RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage file attachment"),
    GET_FILE_ATTACHMENT("Attachment get file", Arrays.asList(RoleName.ROLE_AGENT, RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage file attachment"),

    ADD_ZIP_CODE("Add zip code", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage zip code"),
    EDIT_ZIP_CODE("Edit zip code", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage zip code"),
    DELETE_ZIP_CODE("Delete zip code", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage zip code"),

    SAVE_HOLIDAY("Add and edit holiday", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage holiday"),
    DELETE_HOLIDAY("Delete holiday", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage holiday"),

    SAVE_MAIN_SERVICE_WORK_TIME_PERCENT("Add and edit after hours work", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage after hours work"),
    DELETE_MAIN_SERVICE_WORK_TIME_PERCENT("Delete after hours work", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage after hours work"),

    ADD_BLOG_SERVICE("Add blog", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage blog"),
    EDIT_BLOG_SERVICE("Edit blog", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage blog"),
    DELETE_BLOG_SERVICE("Delete blog", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage blog"),

    ADD_TERMS_SERVICE("Add terms", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage terms"),
    EDIT_TERMS_SERVICE("Edit terms", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage terms"),
    DELETE_TERMS_SERVICE("Delete terms", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage terms"),
    GET_PAYMENT_LIST("Get payments", Arrays.asList(RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Payment info"),
    COMPLETE_ORDER("Complete order", Arrays.asList(RoleName.ROLE_AGENT, RoleName.ROLE_ADMIN, RoleName.ROLE_SUPER_ADMIN), "Manage order");

    public List<RoleName> roleNames;
    public String name;
    public String generalName;

    PermissionName(String name, List<RoleName> roleNames, String generalName) {
        this.roleNames = roleNames;
        this.name = name;
        this.generalName = generalName;
    }

    PermissionName(String name, List<RoleName> roleNames) {
        this.roleNames = roleNames;
        this.name = name;
    }
}
