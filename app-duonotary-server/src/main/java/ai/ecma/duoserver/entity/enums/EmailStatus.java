package ai.ecma.duoserver.entity.enums;

public enum EmailStatus {
    ERROR,
    SENT
}
