package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.enums.WeekDayEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class WeekDay {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private WeekDayEnum day; // KUN NOMI

    @Column(nullable = false)
    private Integer orderNumber; // KUN TARTIBI

    public WeekDay(WeekDayEnum day, Integer orderNumber) {
        this.day = day;
        this.orderNumber = orderNumber;
    }
}
