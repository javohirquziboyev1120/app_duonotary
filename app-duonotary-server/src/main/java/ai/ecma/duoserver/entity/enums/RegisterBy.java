package ai.ecma.duoserver.entity.enums;

public enum RegisterBy {
    WEB, IOS, ANDROID
}
