package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"main_service_id", "sub_service_id"})})
public class Service extends AbsEntity {//DIYOR
    //Main Service va Sub service bn yig'ilgan joy. Har bir service uchun ketadigan vaqt.
    // Service da qo'shimcha vaqtlar hisoblab borish uchun dynamik ishlatamiz

    @ManyToOne(optional = false)
    private MainService mainService;//online yoki in-person

    @ManyToOne(optional = false)
    private SubService subService;

    @Column(nullable = false)
    private Integer initialCount;//boshlang'ich soni, misol: 5 ta hujjat

    @Column(nullable = false)
    private Integer initialSpendingTime;//Boshlang'ich vaqt. Misol 1 soat

    private Integer everyCount;//1 ta hujjat

    private Integer everySpendingTime;//15 minut

    private boolean dynamic;//dinamik true bo'ladi yuqoridagi holatda

    private boolean active;

}
