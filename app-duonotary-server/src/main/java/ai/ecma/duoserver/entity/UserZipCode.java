package ai.ecma.duoserver.entity;

import ai.ecma.duoserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class UserZipCode extends AbsEntity {
    //BU TABLE AGENT VA ADMINNI BIROR-BIR ZIP CODE GA BIRIKTIRISH UCHUN ISHLATILADI.
    //AGENT SHU TABLE DA ACTIVE TRUE BO'LGAN ZIP CODE LAR UCHUN XIZMAT QILADI
    //ADMIN SHU TABLE DA ACTIVE TRUE BO'LGAN ZIP CODE LAR VA ULARDAGI HODISALRNI BOSHQARADI


    @ManyToOne(optional = false)
    private ZipCode zipCode;//ZIP CODE ID SI

    @ManyToOne(optional = false)
    private User user;//AGENT YOKI ADMIN BO'LISHI MUMKIN

    private boolean active = true;//AGENT UCHUN USHBU ZIP CODE FAOL YOKI NOFAOLLIGI

    private boolean enable=false;

    public UserZipCode(ZipCode zipCode, User user) {
        this.zipCode = zipCode;
        this.user = user;
    }
}
