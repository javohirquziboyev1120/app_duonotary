package ai.ecma.duoserver.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Time;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class TimeDuration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private Integer durationTime; // BU VAQT FRONTDA BO'SH SOATLAR ORALIG'INI BELGILAYDI// 30 minut
}
