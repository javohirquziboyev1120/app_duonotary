package ai.ecma.duoserver.entity.enums;

public enum HourTypeEnum {
    FREE, BREAK, BOOKED
}
