package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.FeedBackDto;
import ai.ecma.duoserver.repository.FeedBackRepository;
import ai.ecma.duoserver.security.CurrentUser;
import ai.ecma.duoserver.service.FeedBackService;
import ai.ecma.duoserver.utils.AppConstants;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping("/api/feedBack")
public class FeedBackController {
    @Autowired
    FeedBackService feedBackService;
    @Autowired
    FeedBackRepository feedBackRepository;

    /**
     * Bu methodda feedback qo'shiladi feedback mijoz va agent ni order haqida fikri boladi
     * Faqat Agent va Userga Mumkun
     *
     * @param feedBackDto feedBackni ma'lumotlari
     * @return ApiResponse
     */
//    @PreAuthorize("hasAuthority('FOR_AGENT_AND_USER_FEEDBACK')")
    @PostMapping
    public HttpEntity<?> addFeedBack(@RequestBody FeedBackDto feedBackDto, @CurrentUser User user) {
        ApiResponse apiResponse = feedBackService.addFeedBack(feedBackDto,user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    /**
     * Bu methodda feedback edit qilinadi feedback mijoz va agent ni order haqida fikri boladi
     * Faqat Agent Va userga Mumkun
     *
     * @param feedBackDto feedBackni ma'lumotlari
     * @return ApiResponse
     */
//    @PreAuthorize("hasAuthority('FOR_AGENT_AND_USER_FEEDBACK')")
    @PutMapping
    public HttpEntity<?> editFeedBack(@RequestBody FeedBackDto feedBackDto) {
        ApiResponse apiResponse = feedBackService.editFeedBack(feedBackDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }

    /**
     * Admin notificationda feedBack korganda Bu yerga isSeen true qilgani keladi
     *
     * @param id Bunda feedBackni idsi keladi
     * @return ApiResponse
     */
//    @PreAuthorize("hasAuthority('FOR_ADMIN_FEEDBACK')")
    @PutMapping("/seen/{id}")
    protected HttpEntity<?> seenAdminUnSeenFeedBack(@PathVariable UUID id) {
        ApiResponse apiResponse = feedBackService.seenAdminUnSeenFeedBack(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }

    /**
     * Bunda bitta feedBack hohlagan userga berib yuboriladi
     *
     * @param id
     * @return feedBackDto
     */
    @GetMapping("/{id}")
    protected HttpEntity<?> getById(@PathVariable UUID id) {
        return ResponseEntity.ok(feedBackService.getFeedBack(feedBackRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getFeedBack", "id", id))));
    }

    /**
     * Bu methoda admini notificationga yangi yoki edit qilingan feedBacklar boradi
     *
     * @return List<FeedBackDto> feedBacklarni listi qaytadi
     */
//    @PreAuthorize("hasAuthority('FOR_ADMIN_FEEDBACK')")
    @GetMapping("/forAdminNavigation")
    protected HttpEntity<?> forAdminNavigation() {
        return ResponseEntity.ok(feedBackService.forAdminNavigation());
    }

    /**
     * Bu methoda hamma feedBacklar qaytriladi
     *
     * @param page
     * @param size
     * @return ResPageable
     */
//    @PreAuthorize("hasAuthority('FOR_ADMIN_FEEDBACK')")
    @GetMapping
    protected HttpEntity<?> getList(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                    @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
                                    @RequestParam(value = "isAgent", defaultValue = "true") boolean isAgent) {
        return ResponseEntity.ok(feedBackService.getFeedBackList(size, page, isAgent));
    }

    /**
     * Bu admin feedback yuborgan userga javob yubboradi ,   feedbackni seen=true  boladi
     *
     * @param feedBackDto
     * @return ApiResponse
     */
//    @PreAuthorize("hasAuthority('FOR_ADMIN_FEEDBACK')")
    @PostMapping("/answer")
    protected HttpEntity<?> answerFeedBack(@RequestBody FeedBackDto feedBackDto) {
        ApiResponse apiResponse = feedBackService.answerFeedBack(feedBackDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 202 : 409).body(apiResponse);
    }

    /**
     * user id orqali feedbacklarni olish current user role ni tekishirish uchun kerak
     *
     * @param user
     * @return list
     */
//    @PreAuthorize("hasAuthority('FOR_AGENT_AND_USER_FEEDBACK')")
    @GetMapping("/byUserId")
    protected HttpEntity<?> getByUserId(@CurrentUser User user,
                                        @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                        @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(feedBackService.getFeedBacksByUserId(user, page, size));
    }

    //    @PreAuthorize("hasAuthority('FOR_AGENT_AND_USER_FEEDBACK')")
    @GetMapping("/maxRate")
    protected HttpEntity<?> getByMaxRate(@CurrentUser User user,
                                         @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                         @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(feedBackService.getAgentFeedBacksMaxRate(user, page, size));
    }
    @GetMapping("/mobile")
    public HttpEntity<?> getFeedbackMobile(
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
            @RequestParam(value = "date",required = false) Date date,
            @RequestParam(value = "client",required = false) boolean client,
            @CurrentUser User user){
        return ResponseEntity.ok(feedBackService.getFeedBackMobile(user,date,client,page,size));
    }
}
