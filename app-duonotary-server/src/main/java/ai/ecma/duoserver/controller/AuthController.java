package ai.ecma.duoserver.controller;


import ai.ecma.duoserver.component.NotificationService;
import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.JwtToken;
import ai.ecma.duoserver.payload.ReqLogin;
import ai.ecma.duoserver.payload.UserDto;
import ai.ecma.duoserver.repository.UserRepository;
import ai.ecma.duoserver.security.CurrentUser;
import ai.ecma.duoserver.security.JwtTokenProvider;
import ai.ecma.duoserver.service.AuthService;
import ai.ecma.duoserver.service.CheckRole;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private AuthService authService;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtTokenProvider jwtTokenProvider;
    @Autowired
    CheckRole checkRole;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;


    /**
     * userlarni registratsiyadan o'tkazish uchun
     *
     * @param request UserDto da userning ma'lumotlari keladi
     * @return ro'yxatdan o'tsa true qaytaramiz
     */
    @PostMapping("/registerUser")
    private HttpEntity<?> registerUser(@Valid @RequestBody UserDto request) {
        ApiResponse apiResponse = authService.registerUser(request);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @PatchMapping("/exist")
    private HttpEntity<?> existEmailAndPhone(@RequestBody UserDto userDto) {
        return ResponseEntity.ok(authService.checkEmailAndPhoneNumber(userDto));
    }


    /**
     * emailni tasdiqlash uchun
     *
     * @param check
     * @param email
     * @param code
     * @return
     */
    @GetMapping("/verifyEmail")
    private HttpEntity<?> verifyEmail(@RequestParam boolean check,
                                      @RequestParam String email,
                                      @RequestParam String code,
                                      @RequestParam boolean changing) {
        ApiResponse apiResponse = authService.verifyEmail(check, code, email, changing);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    /**
     * login qilish uchun. Agar hammasi ok bo'lsa unga token qaytaramiz
     *
     * @param reqLogIn
     * @return
     */
    @PostMapping("/login")
    private HttpEntity<?> logIn(@Valid @RequestBody ReqLogin reqLogIn) {
        try {
            Authentication authentication = authenticationManager.authenticate(new
                    UsernamePasswordAuthenticationToken(reqLogIn.getUsername(), reqLogIn.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            User user = (User) authentication.getPrincipal();
            if (checkRole.isAgent(user.getRoles()) && !user.isActive()) {
                return ResponseEntity.status(401).body(new ApiResponse("You have not yet been confirmed by admin", false));
            }
            if (reqLogIn.isNewUser()) {
                return ResponseEntity.ok(checkUserNotEnabled(reqLogIn));
            }
            String token = jwtTokenProvider.generateToken(user);
            return ResponseEntity.ok(new JwtToken(token));
        } catch (Exception e) {
            return ResponseEntity.status(401).body(new ApiResponse("username or password  error", false));
        }
    }

    @PostMapping("/checkToken")
    private HttpEntity<?> checkUserFirebaseToken(@CurrentUser User user, @RequestBody String firebaseToken) {
        try {
            if (user.getFirebaseToken().equals(firebaseToken)) {
                return ResponseEntity.ok("Success");
            } else {
                return ResponseEntity.ok("New token set");
            }
        } catch (Exception e) {
            return ResponseEntity.status(401).body(new ApiResponse("User cannot find!", false));
        }
    }

    /**
     * agent ro'yxatdan o'tishi
     *
     * @param userDto
     * @return
     */
    @PostMapping("/registerAgent")
    private HttpEntity<?> registerAgent(@Valid @RequestBody UserDto userDto) throws MessagingException, IOException, TemplateException {
        ApiResponse response = authService.registerAgent(userDto);
        return ResponseEntity.status(response.isSuccess() ? 201 : 409).body(response);
    }

    @PutMapping("/forgotPassword")
    private HttpEntity<?> forgotPassword(@RequestParam(value = "username") String username) {
        ApiResponse response = authService.forgotPassword(username);
        return ResponseEntity.status(response.isSuccess() ? 201 : 409).body(response);
    }

    @PutMapping("/confirmVerification")
    private HttpEntity<?> confirmVerification(@RequestParam(value = "code") String code,
                                              @RequestParam(value = "username") String username) {
        ApiResponse response = authService.confirmVerification(code, username);
        return ResponseEntity.status(response.isSuccess() ? 201 : 409).body(response);
    }

    @PutMapping("/resetPassword")
    private HttpEntity<?> resetPassword(@RequestParam(value = "password") String password,
                                        @RequestParam(value = "prePassword") String prePassword,
                                        @RequestParam(value = "emailCode") String emailCode) {
        ApiResponse response = authService.resetPassword(password, prePassword, emailCode);
        return ResponseEntity.status(response.isSuccess() ? 201 : 409).body(response);
    }

    public boolean checkUserNotEnabled(ReqLogin reqLogin) {
        if (reqLogin.isNewUser()) {
            Optional<User> optionalUser = userRepository.findByEmailAndEnabledFalse(reqLogin.getUsername());
            if (optionalUser.isPresent()) {
                return passwordEncoder.matches(reqLogin.getPassword(), optionalUser.get().getPassword());
            }
        }
        return false;
    }
}
