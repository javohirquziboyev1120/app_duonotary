package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.payload.AdditionalServicePriceDto;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.repository.AdditionalServicePriceRepository;
import ai.ecma.duoserver.service.AdditionalServicePriceService;
import ai.ecma.duoserver.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/additionalServicePrice")
public class AdditionalServicePriceController {
    @Autowired
    AdditionalServicePriceService additionalServicePrice;

    @Autowired
    AdditionalServicePriceRepository addServicePriceRepository;

    /**
     * AdditionalServerPrice saqlash uchun
     *
     * @param addServicePriceDto
     * @return apiResponse
     */
    @PreAuthorize("hasAuthority('SAVE_ADDITIONAL_SERVICE_PRICE')")
    @PostMapping
    public HttpEntity<?> addAdditionalServicePrice(@RequestBody AdditionalServicePriceDto addServicePriceDto) {
        ApiResponse apiResponse = additionalServicePrice.addAndEditAdditionalServicePrice(addServicePriceDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @PreAuthorize("hasAuthority('SAVE_ADDITIONAL_SERVICE_PRICE')")
    @PutMapping("/editAdditionalServicePrice")
    public HttpEntity<?> editAdditionalServicePrice(@RequestBody AdditionalServicePriceDto addServicePriceDto) {
        ApiResponse apiResponse = additionalServicePrice.addAndEditAdditionalServicePrice(addServicePriceDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(apiResponse);
    }


//    @GetMapping("/{id}")
//    public HttpEntity<?> getAdditionalServicePrice(@PathVariable UUID id) {
//        AdditionalServicePrice addServicePrice = addServicePriceRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getAdditionalServicePrice"));
//        return ResponseEntity.ok(additionalServicePrice.getAddServicePrice(addServicePrice));
//    }

    /**
     * AdditionalServerPrice page olish uchun
     *
     * @param page page
     * @param size size
     * @return page size
     */
    @PreAuthorize("hasAuthority('GET_ADDITIONAL_SERVICE_PRICE')")
    @GetMapping("/getPage")
    public HttpEntity<?> getAdditionalServicePricePage(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
                                                       @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return ResponseEntity.ok(additionalServicePrice.getAdditionalServicePricePage(page, size));
    }
    @PreAuthorize("hasAnyAuthority('GET_ADDITIONAL_SERVICE_PRICE')")
    @GetMapping("/dashboard")
    public HttpEntity<?> getAdditionalServicePriceDashboard() {
        return ResponseEntity.ok(additionalServicePrice.getAdditionalServicePriceDashboard());
    }

    /**
     * AdditionalServerPrice page User uchun olish uchun
     *
     * @param page page
     * @param size size
     * @return page size
     */
    @GetMapping("/getForUser")
    public HttpEntity<?> getAdditionalServicePriceListForUser(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
                                                              @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return ResponseEntity.ok(additionalServicePrice.getAdditionalServicePricePageForUser(page, size));
    }

    /**
     * AdditionalServerPrice o'chirish
     *
     * @param id id
     * @return apiResponse
     */
    @PreAuthorize("hasAuthority('DELETE_ADDITIONAL_SERVICE_PRICE')")
    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteAdditionalServicePrice(@PathVariable UUID id) {
        ApiResponse apiResponse = additionalServicePrice.deleteAddServicePrice(id);
        return ResponseEntity.ok(apiResponse);
    }

    /**
     * AdditionalServerPrice activeni almashtirish uchun
     *
     * @param id     id
     * @param active activeligi
     * @return ApiResponse
     */
    @PreAuthorize("hasAuthority('ACTIVE_ADDITIONAL_SERVICE_PRICE')")
    @PutMapping("/changeActive/{id}")
    public HttpEntity<?> changeActiveAdditionalServicePrice(@PathVariable UUID id, boolean active) {
        ApiResponse apiResponse = additionalServicePrice.setActiveAddServicePrice(id, active);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }


    /**
     * klient zakaz qoldirmoqchi bo'lganda service price id ni jo'natsa,
     * biz unga tegishli additional service price larni jo'natamiz
     * @param servicePriceId
     * @return
     */
    @GetMapping("/byServicePriceId/{servicePriceId}")
    public HttpEntity<?> getAdditionalServicePricesByServicePriceId(@PathVariable UUID servicePriceId) {
        return ResponseEntity.ok(new ApiResponse(additionalServicePrice.getAdditionalServicePricesDto(addServicePriceRepository.findAllByServicePriceId(servicePriceId))));
    }

}
