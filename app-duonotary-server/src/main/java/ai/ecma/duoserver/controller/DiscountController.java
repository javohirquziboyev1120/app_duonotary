package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.payload.*;
import ai.ecma.duoserver.service.DiscountService;
import ai.ecma.duoserver.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/discount")
public class DiscountController {

    @Autowired
    DiscountService discountService;

    @PostMapping("/saveDiscountPercent")
    public HttpEntity<?> saveDiscountPercent(@RequestBody DiscountPercentDto discountPercentDto) {
        ApiResponse apiResponse = discountService.addAndEditDiscountPercent(discountPercentDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? apiResponse.getMessage().equals("Saved") ? 201 : 200 : 409).body(apiResponse);
    }

    @PutMapping("/editDiscountPercent/{id}")
    public HttpEntity<?> editDiscountPercent(@RequestBody DiscountPercentDto discountPercentDto, @PathVariable UUID id) {
        ApiResponse apiResponse = discountService.editDiscountPercent(discountPercentDto, id);
        return ResponseEntity.status(apiResponse.isSuccess() ? apiResponse.getMessage().equals("Saved") ? 201 : 200 : 409).body(apiResponse);
    }

    @GetMapping("/sharingDiscountTariff")
    public HttpEntity<?> getSharingDiscountTariff(@RequestParam(value = "search", defaultValue = "all") String search) {
        return ResponseEntity.ok(discountService.getSharingDiscountTariff(search));
    }

    @GetMapping("/changeActiveStatusSharingDiscountTariff")
    public HttpEntity<?> changeActiveStatusSharingDiscountTariff(@RequestParam boolean active, @RequestParam boolean online) {
        return ResponseEntity.ok(discountService.changeActiveStatusSharingDiscountTariff(active, online));
    }

    @PostMapping("/saveOrEditSharingDiscountTariff")
    public HttpEntity<?> saveOrEditSharingDiscountTariff(@RequestBody ReqSharingDiscountTariff reqSharingDiscountTariff) {
        ApiResponse apiResponse = discountService.saveOrEditSharingDiscountTariff(reqSharingDiscountTariff);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/changePercentSharingDiscountTariff")
    public HttpEntity<?> changePercentSharingDiscountTariff(@RequestParam Double sharingPercent) {
        return ResponseEntity.ok(discountService.changePercentSharingDiscountTariff(sharingPercent));
    }

    @PostMapping("/saveOrEditLoyaltyDiscountTariff")
    public HttpEntity<?> saveOrEditLoyaltyDiscountTariff(@RequestBody LoyaltyDiscountDto loyaltyDiscountDto) {
        ApiResponse apiResponse = discountService.saveOrEditLoyaltyDiscountTariff(loyaltyDiscountDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? apiResponse.getMessage().equals("Saved") ? 201 : 200 : 409).body(apiResponse);
    }


    @GetMapping("/getAllLoyaltyDiscountTariff")
    public HttpEntity<?> getAllLoyaltyDiscountTariff() {
        return ResponseEntity.ok(discountService.getAllLoyaltyDiscountTariff());
    }

    @GetMapping("/changeLoyaltyDiscountTariffActive")
    public HttpEntity<?> changeLoyaltyDiscountTariffActive(@RequestParam Integer id) {
        ApiResponse apiResponse = discountService.changeLoyaltyDiscountTariffActive(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/removeLoyaltiDiscountTariff")
    public HttpEntity<?> removeLoyaltiDiscountTariff(@RequestParam Integer id) {
        ApiResponse apiResponse = discountService.removeLoyaltiDiscountTariff(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/getAllSharingDiscountGiven")
    public HttpEntity<?> getAllSharingDiscountGiven(@RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                                    @RequestParam(name = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) short size) {
        return ResponseEntity.ok(discountService.getAllSharingDiscountGiven(page, size));
    }

    @GetMapping("/getAllLoyaltyDiscountGiven")
    public HttpEntity<?> getAllLoyaltyDiscountGiven(@RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                                    @RequestParam(name = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) short size) {
        return ResponseEntity.ok(discountService.getAllLoyaltyDiscountGiven(page, size));
    }

    @GetMapping("/getAllDiscountExpense")
    public HttpEntity<?> getAllDiscountExpense(@RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                               @RequestParam(name = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) short size) {
        return ResponseEntity.ok(discountService.getAllDiscountExpense(page, size));
    }

    //===========
    @GetMapping("/getAllCustomDiscount")
    public HttpEntity<?> getAllCustomDiscount(@RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                              @RequestParam(name = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) short size) {
        return ResponseEntity.ok(discountService.getAllCustomDiscount(page, size));
    }

    @GetMapping("/getAllCustomersBySearch")
    public HttpEntity<?> getAllCustomersBySearch(@RequestParam(value = "search", defaultValue = "all") String search) {
        return ResponseEntity.ok(discountService.getAllCustomersBySearch(search));
    }

    @GetMapping("/getAllOrdersByClientId")
    public HttpEntity<?> getAllOrdersByClientId(@RequestParam(value = "id") UUID id) {
        return ResponseEntity.ok(discountService.getAllOrdersByClientId(id));
    }

    @PostMapping("/saveOrEditCustomDiscount")
    public HttpEntity<?> saveOrEditCustomDiscount(@RequestBody ReqCustomDiscount reqCustomDiscount) {
        ApiResponse apiResponse = discountService.saveOrEditCustomDiscount(reqCustomDiscount);
        return ResponseEntity.status(apiResponse.isSuccess() ? apiResponse.getMessage().equals("Saved") ? 201 : 200 : 409).body(apiResponse);
    }

    @GetMapping("/removeCustomOrder")
    public HttpEntity<?> removeCustomOrder(@RequestParam(value = "id") UUID id) {
        ApiResponse apiResponse = discountService.removeCustomOrder(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }


    //   Custom Discount Tariff

    @PostMapping("/saveOrEditCustomDiscountTariff")
    public HttpEntity<?> saveOrEditCustomDiscountTariff(@RequestBody CustomDiscountTariffDto customDiscountTariffDto) {
        ApiResponse apiResponse = discountService.saveOrEditCustomDiscountTariff(customDiscountTariffDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? apiResponse.getMessage().equals("Saved") ? 201 : 200 : 409).body(apiResponse);
    }

    @GetMapping("/changeCustomDiscountTariffActiveStatus")
    public HttpEntity<?> changeCustomDiscountTariffActiveStatus(@RequestParam UUID id) {
        ApiResponse apiResponse = discountService.changeCustomDiscountTariffActiveStatus(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? apiResponse.getMessage().equals("Saved") ? 201 : 200 : 409).body(apiResponse);
    }

    @GetMapping("/getCustomDiscountTariffs")
    public HttpEntity<?> getCustomDiscountTariffs(@RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                                  @RequestParam(name = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) short size) {
        return ResponseEntity.ok(discountService.getCustomDiscountTariffs(page, size));
    }

    @GetMapping("/removeCustomDiscountTariff")
    public HttpEntity<?> removeCustomDiscountTariff(@RequestParam(value = "id") UUID id) {
        ApiResponse apiResponse = discountService.removeCustomDiscountTariff(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/getBySearch")
    public HttpEntity<?> getBySearch(@RequestParam String search) {
        return ResponseEntity.ok(discountService.getBySearch(search));
    }

    @GetMapping("/getBySearchAgent")
    public HttpEntity<?> getBySearchAgent(@RequestParam String search) {
        return ResponseEntity.ok(discountService.getBySearchAgent(search));
    }

    //==================
    // First order discount tariff
    @PostMapping("/saveOrEditFirstOrderDiscountTariff")
    public HttpEntity<?> saveOrEditFirstOrderDiscountTariff(@RequestBody ReqFirstOrderDiscountTariff reqFirstOrderDiscountTariff) {
        ApiResponse apiResponse = discountService.saveOrEditFirstOrderDiscountTariff(reqFirstOrderDiscountTariff);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/getResFirstDiscountTariff")
    public HttpEntity<?> getResFirstDiscountTariff(@RequestParam(value = "search", defaultValue = "all") String search) {
        return ResponseEntity.ok(discountService.getResFirstDiscountTariff(search));
    }

    @GetMapping("/changeActiveStatusFirstOrderDiscountTariff")
    public HttpEntity<?> changeActiveStatusFirstOrderDiscountTariff(@RequestParam boolean active, @RequestParam boolean online) {
        return ResponseEntity.ok(discountService.changeActiveStatusFirstOrderDiscountTariff(active, online));
    }
}
