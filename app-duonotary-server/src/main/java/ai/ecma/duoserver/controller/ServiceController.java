package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.entity.MainService;
import ai.ecma.duoserver.entity.Service;
import ai.ecma.duoserver.entity.SubService;
import ai.ecma.duoserver.entity.enums.StatusEnum;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.ServiceDto;
import ai.ecma.duoserver.repository.*;
import ai.ecma.duoserver.service.ServiceService;
import ai.ecma.duoserver.utils.AppConstants;
import ai.ecma.duoserver.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/service")
public class ServiceController {
    @Autowired
    ServiceService service;

    @Autowired
    ServiceRepository serviceRepository;
    @Autowired
    SubServiceRepository subServiceRepository;
    @Autowired
    AdditionalServiceRepository additionalServiceRepository;
    @Autowired
    DocumentTypeRepository documentTypeRepository;
    @Autowired
    DiscountPercentRepository discountPercentRepository;

    /**
     * service qoshish uchun
     *
     * @param serviceDto
     * @return apiResponse
     */
    @PreAuthorize("hasAnyAuthority('ADD_SERVICE')")
    @PostMapping
    public HttpEntity<?> addService(@RequestBody ServiceDto serviceDto) {
        ApiResponse apiResponse = service.addService(serviceDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @PreAuthorize("hasAnyAuthority('ADD_SERVICE')")
    @PostMapping("/firstService")
    public HttpEntity<?> firstService() {
        ApiResponse apiResponse = service.firstService();
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    /**
     * service tahrirlash uchun
     *
     * @param serviceDto
     * @return apiResponse
     */
    @PreAuthorize("hasAnyAuthority('EDIT_SERVICE')")
    @PutMapping("/editService")
    public HttpEntity<?> editService(@RequestBody ServiceDto serviceDto) {
        ApiResponse apiResponse = service.editService(serviceDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PutMapping("/editSubService")
    public HttpEntity<?> editSubService(@RequestBody ServiceDto serviceDto) {
        Optional<SubService> byId = subServiceRepository.findById(serviceDto.getSubServiceDto().getId());
        if (byId.isPresent()) {
            SubService subService = byId.get();
            subService.setDefaultInput(serviceDto.isDefaultInput());
            subServiceRepository.save(subService);
            ApiResponse apiResponse = new ApiResponse("Edit Service", true);
            return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(apiResponse);
        }
        ApiResponse apiResponse = new ApiResponse("Edit Error Service", false);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(apiResponse);
    }

    /**
     * serviceni bitasini olish uchun
     *
     * @param id servicening idsi
     * @return
     */
    @GetMapping("/{id}")
    public HttpEntity<?> getService(@PathVariable UUID id) {
        Service service = serviceRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getService"));
        return ResponseEntity.ok(this.service.getService(service));
    }

    /**
     * Servicelarni page qaytaradi
     *
     * @param page page
     * @param size size
     * @return page and size
     */
    @PreAuthorize("hasAnyAuthority('GET_SERVICE')")
    @GetMapping("/getPage")
    public HttpEntity<?> getServicePage(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
                                        @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return ResponseEntity.ok(service.getServicesPage(page, size));
    }
    @PreAuthorize("hasAnyAuthority('GET_SERVICE')")
    @GetMapping("/getServiceList")
    public HttpEntity<?> getServiceList() {
        return ResponseEntity.ok(serviceRepository.findAll());
    }

    /**
     * Servicelarni page User uchun
     *
     * @param page page
     * @param size size
     * @return page and size
     */
    @GetMapping("/getUser")
    public HttpEntity<?> getServicePageForUser(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
                                               @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return ResponseEntity.ok(service.getServicesPageForUser(page, size));
    }

    /**
     * serviceni activveni ozgartirish uchun
     *
     * @return apiResponse
     */
    @PreAuthorize("hasAnyAuthority('CHANGE_ACTIVE_SERVICE')")
    @PutMapping("/changeActive/{serviceId}")
    public HttpEntity<?> serviceActive(@PathVariable UUID serviceId) {
        ApiResponse apiResponse = service.serviceActive(serviceId);
        return ResponseEntity.ok(apiResponse);
    }

    @PreAuthorize("hasAnyAuthority('ADD_SERVICE')")
    @GetMapping("/getService")
    public HttpEntity<?> getServiceByServicePrice() {
        return ResponseEntity.ok(service.getExistServiceList());
    }


    /**
     * dynamicni activveni ozgartirish uchun
     *
     * @return apiResponse
     */
    @PreAuthorize("hasAnyAuthority('CHANGE_DYNAMIC_SERVICE')")
    @PutMapping("/isDynamic")
    public HttpEntity<?> serviceDynamic(@RequestBody ServiceDto serviceDto) {
        ApiResponse apiResponse = service.serviceDynamic(serviceDto);
        return ResponseEntity.ok(apiResponse);
    }

    @DeleteMapping("/delete")
    public HttpEntity<?> deleteService(@RequestParam(value = "serviceId") UUID serviceId,
                                       @RequestParam(value = "subServiceId") UUID subServiceId) {
        ApiResponse apiResponse = service.deleteService(serviceId, subServiceId);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/subServicePageable")
    public HttpEntity<?> getPageSubService(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
                                           @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return ResponseEntity.ok(subServiceRepository.findAll(CommonUtils.getPageable(page, size)));
    }

    @GetMapping("/additionalServicePageable")
    public HttpEntity<?> getPageAdditionalService(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
                                                  @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return ResponseEntity.ok(additionalServiceRepository.findAll(CommonUtils.getPageable(page, size)));
    }

    @GetMapping("/documentTypePageable")
    public HttpEntity<?> getPageDocumentType(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
                                             @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return ResponseEntity.ok(documentTypeRepository.findAll(CommonUtils.getPageable(page, size)));
    }

    @GetMapping("/discountPercentPageable")
    public HttpEntity<?> getPageDiscountPercent(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
                                                @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return ResponseEntity.ok(discountPercentRepository.findAll(CommonUtils.getPageable(page, size)));
    }

}
