package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.entity.ZipCode;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.*;
import ai.ecma.duoserver.repository.CountryRepository;
import ai.ecma.duoserver.repository.ZipCodeRepository;
import ai.ecma.duoserver.service.ZipCodeService;
import ai.ecma.duoserver.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/zipCode")
public class ZipCodeController {
    @Autowired
    ZipCodeService zipCodeService;
    @Autowired
    ZipCodeRepository zipCodeRepository;

    @Autowired
    CountryRepository countryRepository;

    /**
     * ZipCodelarni qoshish uchun method
     *
     * @param zipCodeDto zipcode malumotalari
     * @return apiResponse
     */
    @PreAuthorize("hasAuthority('ADD_ZIP_CODE')")
    @PostMapping
    public HttpEntity<?> addZipCode(@RequestBody ZipCodeDto zipCodeDto) {
        ApiResponse apiResponse = zipCodeService.addZipCode(zipCodeDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @PreAuthorize("hasAuthority('ADD_ZIP_CODE')")
    @PostMapping("/saveOrEditZipCode")
    public HttpEntity<?> saveOrEditZipCode(@RequestBody ZipCodeDto zipCodeDto) {
        ApiResponse apiResponse = zipCodeService.saveOrEditZipCode(zipCodeDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    /**
     * ZipCodelarni edit uchun method
     *
     * @param zipCodeDto zipcode malumotalari
     * @return apiResponse
     */
    @PreAuthorize("hasAuthority('ADD_ZIP_CODE')")
    @PutMapping
    public HttpEntity<?> editZipCode(@RequestBody ZipCodeDto zipCodeDto) {
        ApiResponse apiResponse = zipCodeService.editZipcode(zipCodeDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    /**
     * ZipCodelarni activeni o'zgartirish uchun method
     *
     * @param id zipcode id
     * @return apiResponse
     */
    @PreAuthorize("hasAuthority('ADD_ZIP_CODE')")
    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteZipCode(@PathVariable UUID id) {
        ApiResponse apiResponse = zipCodeService.changeActive(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.NO_CONTENT : HttpStatus.CONFLICT).body(apiResponse);

    }

    /**
     * Bir dona zipcodeni olish uchun
     *
     * @param id zipcodeId
     * @return dto
     */
    @GetMapping("/{id}")
    public HttpEntity<?> getZipCode(@PathVariable UUID id) {
        return ResponseEntity.ok(zipCodeService.getZipCode(zipCodeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getZipCode", "id", id))));

    }

    /**
     * page va size ga qarab zipCodelarni qaytaradigan method
     *
     * @param page page raqami
     * @param size nechata keraklgi
     * @return resPageable
     */
    @GetMapping
    public HttpEntity<?> getAll(@RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                @RequestParam(name = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) short size) {
        return ResponseEntity.ok(zipCodeService.getZipCodes(page, size));
    }


    /**
     * Api responseda true qaytsa shu zip code da ishlaydi
     *
     * @param zipCode
     * @return
     */
    @GetMapping("/checkZipCode")
    public HttpEntity<?> checkZipCode(@RequestParam String zipCode) {
        ApiResponse apiResponse = zipCodeService.checkZipCode(zipCode);
        return ResponseEntity.ok(apiResponse);
    }

    @GetMapping("/changeActiveOfZipCode")
    public HttpEntity<?> changeActiveOfZipCode(@RequestParam UUID id) {
        ApiResponse apiResponse = zipCodeService.changeActiveOfZipCode(id);
        return ResponseEntity.ok(apiResponse);
    }

    /**
     * Api responseda true qaytsa shu zip code da ishlaydi
     *
     * @param code
     * @return
     */
    @GetMapping("/byCode")
    public HttpEntity<?> getZipCode(@RequestParam(value = "code", required = false) String code) {
        List<ZipCode> byCodeAndActiveTrue = zipCodeRepository.getByCodeAndActiveTrue(code + "%");
        return ResponseEntity.ok(new ApiResponse(true, byCodeAndActiveTrue));
    }

    /**
     * Service ishlamaydigan zipCodeni qo'shish
     *
     * @param outOfServiceDto dto
     * @return apiResponse
     */

    @PostMapping("/outOfService")
    public HttpEntity<?> addOutOfService(@RequestBody OutOfServiceDto outOfServiceDto) {
        ApiResponse apiResponse = zipCodeService.addOutOfService(outOfServiceDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping("/getAllCounties")
    public HttpEntity<?> getAllCounties(@RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                        @RequestParam(name = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) short size) {
        return ResponseEntity.ok(zipCodeService.getCounties(page, size));
    }

    @GetMapping("/getAllStatesByPageable")
    public HttpEntity<?> getAllStates(@RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                      @RequestParam(name = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) short size) {
        return ResponseEntity.ok(zipCodeService.getStates(page, size));
    }

    @GetMapping("/allStates")
    public HttpEntity<?> allStates() {
        return ResponseEntity.ok(zipCodeService.allStates());
    }

    @GetMapping("/allStates2")
    public HttpEntity<?> allStates2() {
        return ResponseEntity.ok(zipCodeService.allStates2());
    }

    @GetMapping("/deleteZipCode")
    public HttpEntity<?> deleteZipCodeById(@RequestParam UUID id) {
        ApiResponse apiResponse = zipCodeService.deleteZipCode(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/deleteCounty")
    public HttpEntity<?> deleteCounty(@RequestParam UUID id) {
        ApiResponse apiResponse = zipCodeService.deleteCounty(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/deleteState")
    public HttpEntity<?> deleteState(@RequestParam UUID id) {
        ApiResponse apiResponse = zipCodeService.deleteState(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/deleteAgentByZipCode")
    public HttpEntity<?> deleteAgentByZipCode(@RequestParam UUID zcId, @RequestParam UUID agId) {
        ApiResponse apiResponse = zipCodeService.deleteAgentByZipCode(zcId, agId);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/addAgentToZipCode")
    public HttpEntity<?> addAgentToZipCode(@RequestParam UUID zcId, @RequestParam UUID agId) {
        ApiResponse apiResponse = zipCodeService.addAgentToZipCode(zcId, agId);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/getCountyBySelectedState")
    public HttpEntity<?> getCountyBySelectedState(@RequestParam UUID id) {
        return ResponseEntity.ok(zipCodeService.getCountyBySelectedState(id));
    }

    @GetMapping("/getZipCodeBySelectedCounty")
    public HttpEntity<?> getZipCodeBySelectedCounty(@RequestParam(value = "id") UUID id){
        return ResponseEntity.ok(zipCodeService.getZipCodeBySelectedCounty(id));
    }

    @PostMapping("/saveOrEditState")
    public HttpEntity<?> saveOrEditState(@RequestBody StateDto stateDto) {
        ApiResponse apiResponse = zipCodeService.saveOrEditState(stateDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @PostMapping("/saveOrEditCounty")
    public HttpEntity<?> saveOrEditCounty(@RequestBody CountyDto countyDto) {
        ApiResponse apiResponse = zipCodeService.saveOrEditCounty(countyDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/getCurrentZipCode2")
    public HttpEntity<?> getCurrentZipCode2(@RequestParam UUID id) {

        return ResponseEntity.ok(zipCodeService.getCurrentZipCode2(id));
    }

    @GetMapping("/getAgentsByZipCode")
    public HttpEntity<?> getAgentsByZipCode(@RequestParam UUID zcId) {

        return ResponseEntity.ok(zipCodeService.getAgentsByZipCode(zcId));
    }

    @GetMapping("/getAdminZipCode/{id}")
    public HttpEntity<?> getAdminZipCode(@PathVariable UUID id) {
        return ResponseEntity.ok(zipCodeService.getAdminZipCode(id));
    }

}