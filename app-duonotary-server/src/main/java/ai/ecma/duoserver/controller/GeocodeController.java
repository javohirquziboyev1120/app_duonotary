package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.entity.ZipCode;
import ai.ecma.duoserver.payload.GeocodeDto;
import ai.ecma.duoserver.payload.geocode.AddressComponent;
import ai.ecma.duoserver.payload.geocode.Geocode;
import ai.ecma.duoserver.payload.geocode.Result;
import ai.ecma.duoserver.repository.ZipCodeRepository;
import com.google.gson.Gson;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/geocode")
public class GeocodeController {

    final
    ZipCodeRepository zipCodeRepository;

    public GeocodeController(ZipCodeRepository zipCodeRepository) {
        this.zipCodeRepository = zipCodeRepository;
    }

    /**
     * This method return geocode by latitude and longitude (address, lat, lng and postal code)
     */
    @GetMapping("/byLocation")
    public HttpEntity<?> getAddressFromLatLng(@RequestParam(value = "lat") String lat,
                                              @RequestParam(value = "lng") String lng) throws IOException {
        return ResponseEntity.ok(makeGeocode("latlng", lat + ',' + lng));
    }

    /**
     * This method return geocode by address (address, lat, lng and postal code)
     */
    @GetMapping("/byAddress")
    public HttpEntity<?> getAddressFromAddress(@RequestParam(value = "address") String address) throws IOException {
        return ResponseEntity.ok(makeGeocode("address", address));
    }

    @GetMapping("/code")
    public HttpEntity<?> getAddressFromZipCode(@RequestParam(value = "code") String code) throws IOException {
        return ResponseEntity.ok(makeGeocode("components", "postal_code:" + code + "|country:USA"));
    }

    public List<GeocodeDto> makeGeocode(String type, String value) throws IOException {
        OkHttpClient client = new OkHttpClient();
        String key = "AIzaSyChk6yLhGVd7j2vkaLYIYXKwizvfyHPKhc";
        Request request = new Request.Builder()
                .url("https://maps.googleapis.com/maps/api/geocode/json?" + type + "=" + value + "&key=" + key)
                .get()
                .build();
        ResponseBody responseBody = client.newCall(request).execute().body();
        Gson gson = new Gson();
        Geocode geocode = gson.fromJson(responseBody.string(), Geocode.class);
        if (geocode.getStatus().toUpperCase().equals("OK"))
            return geocode.getResults().stream().map(this::makeGeoCodeDto).collect(Collectors.toList());
        return new ArrayList<>();
    }

    private AddressComponent getAddressComponent(List<AddressComponent> list) {
        for (AddressComponent addressComponent : list) {
            LinkedList<String> types = addressComponent.getTypes();
            for (String type : types) {
                if (type.equals("postal_code"))
                    return addressComponent;
            }
        }
        return new AddressComponent();
    }

    private GeocodeDto makeGeoCodeDto(Result result) {
        return new GeocodeDto(
                result.getFormatted_address(),
                result.getGeometry().getLocation().getLat(),
                result.getGeometry().getLocation().getLng(),
                getAddressComponent(result.getAddress_components()),
                zipCodeRepository.findByCodeAndActiveTrue(getAddressComponent(result.getAddress_components()).getLong_name()).orElse(new ZipCode()));
    }
}
