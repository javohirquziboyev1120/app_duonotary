package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.TimeTableDto;
import ai.ecma.duoserver.payload.TimeTableForOrderDto;
import ai.ecma.duoserver.repository.TimeTableRepository;
import ai.ecma.duoserver.service.TimeTableService;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping("/api/timeTable")
public class TimeTableController {
    final
    TimeTableService timeTableService;
    final
    TimeTableRepository timeTableRepository;

    public TimeTableController(TimeTableService timeTableService, TimeTableRepository timeTableRepository) {
        this.timeTableService = timeTableService;
        this.timeTableRepository = timeTableRepository;
    }

    @GetMapping("/timeTableForOrder")
    public HttpEntity<?> getTimeTableForOrder(@RequestParam(value = "servicePriceId") String servicePriceId,
                                              @RequestParam(value = "zipCodeId") String zipCodeId,
                                              @RequestParam(value = "countDocument") Integer countDocument,
                                              @RequestParam(value = "online") boolean online,
                                              @RequestParam(value = "date") String date) throws ParseException {
        Date day = new SimpleDateFormat("yyyy-MM-dd").parse(date);
        TimeTableForOrderDto timeTableForOrder = timeTableService.getTimeTableForOrder(UUID.fromString(servicePriceId), countDocument, day, online, zipCodeId);
        return ResponseEntity.ok(timeTableForOrder);
    }
    /*
    Time tableni saqlab qaytaradigan metod. Bundan orderni olish jarayonida, Date/Time bosqichidan o`tayotganda foydalaniladi.
     */
    @PostMapping
    public HttpEntity<?> addTimeTable(@RequestBody TimeTableDto timeTableDto){
        ApiResponse apiResponse = timeTableService.addTimeTable(timeTableDto);
        return ResponseEntity.status(201).body(apiResponse);
    }
}
