package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.security.CurrentUser;
import ai.ecma.duoserver.service.CheckRole;
import ai.ecma.duoserver.service.QrCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.util.Base64;
import java.util.UUID;

@RestController
@RequestMapping("/api/qrcode")
public class QrCodeController {
    @Autowired
    QrCodeService qrCodeService;
    @Autowired
    CheckRole checkRole;

    @GetMapping
    public HttpEntity<?> getQrCode(@CurrentUser User user) throws IOException {
        if (checkRole.isUser(user.getRoles()) || checkRole.isAgent(user.getRoles())) {
            if (qrCodeService.makeQrCodeForShareClient(user.getId())) {
////                BufferedImage bImage = ImageIO.read(new File("QrCodeForShareClient.png"));
////                ByteArrayOutputStream bos = new ByteArrayOutputStream();
////                ImageIO.write(bImage, "png", bos);
//                InputStream input = new FileInputStream("QrCodeForShareClient.png");
////                byte[] data = bos.toByteArray();
//
//                return ResponseEntity.ok(new InputStreamResource(input));
//
                File file = new File("QrCodeForShareClient.png");

                String encodeImage = Base64.getEncoder().withoutPadding().encodeToString(Files.readAllBytes(file.toPath()));
                ApiResponse apiResponse = new ApiResponse("This is QrCode", true, encodeImage);
                return ResponseEntity.status(201).body(apiResponse);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

}
