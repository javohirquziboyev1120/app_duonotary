package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.entity.Pricing;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.PricingDto;
import ai.ecma.duoserver.repository.PricingRepository;
import ai.ecma.duoserver.service.PricingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/pricing")
public class PricingController {
    @Autowired
    PricingService pricingService;
    @Autowired
    PricingRepository pricingRepository;

    /**
     * Pricing ni qo'shish
     *
     * @param pricingDto PricingDto da pricing ma'lumotlari keladi
     * @return ro'yxatdan o'tsa true qaytaramiz
     * @currnetUser d object buladi
     */
    @PreAuthorize("hasAnyAuthority('ADD_PRICING')")
    @PostMapping
    public HttpEntity<?> addPricing(@RequestBody PricingDto pricingDto) {
        ApiResponse apiResponse = pricingService.addPricing(pricingDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    /**
     * pricing ni edit qilish
     *
     * @param pricingDto PricingDto da pricing ma'lumotlari keladi
     * @return ro'yxatdan o'tsa true qaytaramiz
     * @Pathda uzgartirilayotgan pricing id isi ketadi
     * @currnetUser d object buladi
     */
    @PreAuthorize("hasAnyAuthority('EDIT_PRICING')")
    @PutMapping("/editPrice")
    public HttpEntity<?> editPrice(@RequestBody PricingDto pricingDto) {
        if (pricingDto.getStateId()!=null){
            ApiResponse apiResponse = pricingService.editPricing(pricingDto);
            return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
        }
        ApiResponse apiResponse = pricingService.editPricingStateNull(pricingDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
    }

    /**
     * pricing ni malum bir id sini buyicha olib kelishi
     *
     * @path da suralayotgan pricing id keladi
     */
    @PreAuthorize("hasAnyAuthority('GET_PRICING')")
    @GetMapping("/{id}")
    public HttpEntity<?> getPricing(@PathVariable UUID id) {
        Pricing pricing = pricingRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getPricing"));
        return ResponseEntity.ok(pricing);
    }

    /**
     * pricing ni pageable qilib yuborish
     */
//    @PreAuthorize("hasAnyAuthority('GET_PRICING')")
    @GetMapping("/page")
    public HttpEntity<?> searchPricing(@RequestParam(value = "page", defaultValue = "0") Integer page,
                                       @RequestParam(value = "size", defaultValue = "10") Integer size) {
        return ResponseEntity.ok(pricingService.getPricingPage(page, size));
    }

    @GetMapping("/dashboard")
    public HttpEntity<?> getDashboardPricing() {
        return ResponseEntity.ok(pricingService.getPricingDashboard());
    }

    @GetMapping("/stateByPricing")
    public HttpEntity<?> getPricingByState(@RequestParam(value = "stateId", required = false) String stateId) {
        if (!stateId.equals("null"))
            return ResponseEntity.ok(pricingService.getPricingByState(UUID.fromString(stateId)));
        else
            return ResponseEntity.ok(pricingService.getPricingByStateISNULL());
    }

    @GetMapping("/byServicePrice")
    public HttpEntity<?> getPricingByServicePrice(@RequestParam(value = "servicePriceId") String servicePriceId) {
        return ResponseEntity.ok(pricingRepository.findAllByServicePrice_IdAndActiveTrue(UUID.fromString(servicePriceId)));
    }

    @GetMapping("/getStateByPricing")
    public HttpEntity<?> getStateByPricing(@RequestParam(value = "serviceId") UUID serviceId) {
        return ResponseEntity.ok(pricingService.getStateByPricing(serviceId));
    }

    @DeleteMapping("/deletePricing")
    public HttpEntity<?> deletePricing(@RequestParam(value = "fromCount") Integer fromCount,
                                       @RequestParam(value = "tillCount") Integer tillCount,
                                       @RequestParam(value = "everyCount") Integer everyCount,
                                       @RequestParam(value = "stateId") String stateId,
                                       @RequestParam(value = "price") double price,
                                       @RequestParam(value = "subServiceName") String subServiceName){
        if (!stateId.equals("null"))
            return ResponseEntity.ok(pricingService.deletePricing(fromCount,tillCount, everyCount,UUID.fromString(stateId),price,subServiceName));
        else
            return ResponseEntity.ok(pricingService.getDeletePricingByStateISNULL(fromCount,tillCount,price,everyCount));
    }
}
