package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.entity.State;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.repository.StateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/mobile")
public class MobileController {
    @Autowired
    StateRepository stateRepository;

    @PreAuthorize("hasAnyAuthority('ROLE_AGENT','ROLE_USER')")
    @GetMapping("/state")
    public HttpEntity<?> getStateList(){
        try{
            return ResponseEntity.ok(new ApiResponse(true,stateRepository.findAll()));
        }catch (Exception e){
            return ResponseEntity.ok(new ApiResponse("error",false));
        }
    }
}
