package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.entity.Holiday;
import ai.ecma.duoserver.entity.Permission;
import ai.ecma.duoserver.entity.Role;
import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.entity.enums.PermissionName;
import ai.ecma.duoserver.entity.enums.RoleName;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.*;
import ai.ecma.duoserver.repository.*;
import ai.ecma.duoserver.repository.UserRepository;
import ai.ecma.duoserver.security.CurrentUser;
import ai.ecma.duoserver.service.CheckRole;
import ai.ecma.duoserver.service.DiscountService;
import ai.ecma.duoserver.service.UserService;
import ai.ecma.duoserver.utils.AppConstants;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;
    @Autowired
    PermissionRepository permissionRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    UserZipCodeRepository userZipCodeRepository;
    @Autowired
    CheckRole checkRole;
    @Autowired
    DiscountService discountService;
    @Autowired
    HolidayRepository holidayRepository;
    @Autowired
    MainServiceRepository mainServiceRepository;
    @Autowired
    TimeTableRepository timeTableRepository;

    /**
     * adminni ro'yxatga olish uchun. NU ishni super admin qila oladi
     *
     * @param userDto
     * @return
     * @throws MessagingException
     * @throws IOException
     * @throws TemplateException
     */

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @PostMapping("/addAdmin")
    public HttpEntity<?> addAdmin(@RequestBody UserDto userDto) throws MessagingException, IOException, TemplateException {
        ApiResponse response = userService.addAdmin(userDto);
        return ResponseEntity.status(response.isSuccess() ? 201 : 409).body(response);
    }


    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @GetMapping("/getUserTotalCount")
    public int getUserTotalCount(@RequestParam(value = "role", defaultValue = "ROLE_ADMIN") String role) {
        return userRepository.getUserTotalCount(role);
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @GetMapping("/getAdminList")
    public HttpEntity<?> getAdminList(@CurrentUser User user,
                                      @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                      @RequestParam(value = "size", defaultValue = "20") int size) {
        List<User> adminList = userRepository.getUserListWithPageable("ROLE_ADMIN", size, size * page);
        return ResponseEntity.ok(adminList);
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @GetMapping("/getAgentList")
    public HttpEntity<?> getAgentList(@CurrentUser User user,
                                      @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                      @RequestParam(value = "size", defaultValue = "20") int size) {
        List<User> adminList = userRepository.getUserListWithPageable("ROLE_AGENT", size, size * page);
        return ResponseEntity.ok(adminList);
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @GetMapping("/getClientList")
    public HttpEntity<?> getClientList(@CurrentUser User user,
                                       @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                       @RequestParam(value = "size", defaultValue = "20") int size) {
        List<User> adminList = userRepository.getUserListWithPageable("ROLE_USER", size, size * page);
        return ResponseEntity.ok(adminList);
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @GetMapping("/getPermissionsForAdmin")
    public HttpEntity<?> getPermissionsForAdmin(@CurrentUser User user) {
        List<Permission> role_admin = permissionRepository.findAllByRoleName("ROLE_ADMIN");
        List<PermissionDto> permissionDtos = new ArrayList<>();
        for (Permission permission : role_admin) {
            permissionDtos.add(new PermissionDto(permission.getAuthority(), permission.getId(),
                    permission.getAuthority(), permission.getPermissionName().name, permission.getPermissionName().generalName, false));
        }
        return ResponseEntity.ok(permissionDtos);
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @GetMapping("/getNewPermissionsForAdmin")
    public HttpEntity<?> getNewPermissionsForAdmin(@CurrentUser User user) {
        List<Permission> role_admin = permissionRepository.findAllByRoleName("ROLE_ADMIN");
        Map<String, PermissionDto2> permissionDto2Map = new TreeMap<>();
        for (Permission permission : role_admin) {
            if (permissionDto2Map.containsKey(permission.getPermissionName().generalName)) {
                PermissionDto2 permissionDto2 = permissionDto2Map.get(permission.getPermissionName().generalName);


                List<PermissionDto2> perList1 = new ArrayList<>();
                perList1.add(new PermissionDto2(permission.getId(), permission.getPermissionName().name));
                perList1.addAll(permissionDto2.getPerList());

                permissionDto2.setPerList(perList1);
                permissionDto2.setGeneralName(permission.getPermissionName().generalName);
                permissionDto2Map.put(permissionDto2.getGeneralName(), permissionDto2);
            } else {
                permissionDto2Map.put(permission.getPermissionName().generalName,
                        new PermissionDto2(permission.getPermissionName().generalName,
                                Collections.singletonList(new PermissionDto2(permission.getId(), permission.getPermissionName().name)))
                );
            }
        }
        int i = 1000;
        List<PermissionDto2> resPerList = new LinkedList<>();
        for (String s : permissionDto2Map.keySet()) {
            resPerList.add(new PermissionDto2(i, s, permissionDto2Map.get(s).getPerList()));
            i++;
        }
        return ResponseEntity.ok(resPerList);
    }


    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @PostMapping("/editAdminPermission")
    public HttpEntity<?> editAdminPermission(@RequestBody UserDto userDto) throws MessagingException, IOException, TemplateException {
        ApiResponse response = userService.editAdminPermission(userDto);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }

    /**
     * adminning huquqlarini o'zgartirish uchun
     *
     * @param userDto
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @PostMapping("/editAdminZipCode")
    public HttpEntity<?> editAdminZipCode(@RequestBody UserDto userDto) throws MessagingException, IOException, TemplateException {
        ApiResponse response = userService.editAdminZipCode(userDto);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }

    /**
     * adminning zip code larini o'zgartirish uchun
     *
     * @param adminId
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @PostMapping("/changeEnabledAdmin/{adminId}")
    public HttpEntity<?> changeEnabledAdmin(@PathVariable UUID adminId) {
        ApiResponse response = userService.changeEnabledUser(adminId, true);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }


    /**
     * o'zini edit qilish
     *
     * @param userDto
     * @param user
     * @return
     */
    @PutMapping("/edit")
    public HttpEntity<?> editUser(@RequestBody UserDto userDto, @CurrentUser User user) throws MessagingException, IOException, TemplateException {
        ApiResponse response = userService.editUser(userDto, user);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }

    /**
     * parolni almashtirish uchun
     *
     * @param userDto
     * @param user
     * @return
     */
    @PutMapping("/editPassword")
    public HttpEntity<?> editPassword(@RequestBody UserDto userDto, @CurrentUser User user) {
        ApiResponse response = userService.editPassword(userDto, user);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }

    @GetMapping("/getUser/{id}")
    public HttpEntity<?> getUser(@CurrentUser User user, @PathVariable UUID id) {
        if (checkRole.isROLE_SUPER_ADMIN(user.getRoles())) {
            return ResponseEntity.ok(userRepository.findById(id));
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }

    /**
     * user o'zini olish uchun
     *
     * @param user
     * @return
     */
    @GetMapping("/me")
    public HttpEntity<?> getUserMe(@CurrentUser User user) {
        return ResponseEntity.ok(user);
    }

    @GetMapping("/{userId}")
    public HttpEntity<?> getUserById(@PathVariable UUID userId) {
        User user = userRepository.findById(userId).orElseGet(null);
        return ResponseEntity.ok(user == null ? AppConstants.ID_NOT_FOUND : userService.getUser(user));
    }


    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @DeleteMapping("/deleteAdmin/{id}")
    public HttpEntity<?> deleteAdmin(@PathVariable UUID id, @CurrentUser User user) {
        userZipCodeRepository.deleteAllByUserId(id);
        userRepository.deleteById(id);
        return ResponseEntity.ok(new ApiResponse("Successfully deleted!", true));
    }

    @GetMapping("/getUsersByZipCode/{id}")
    public HttpEntity<?> getUsersByZipCode(@PathVariable UUID id) {
        return ResponseEntity.ok(userService.getUsersByZipCode(id));
    }

    @GetMapping("/getCustomers")
    public HttpEntity<?> getCustomers(@RequestParam(value = "page") int page,
                                      @RequestParam(value = "size") int size) {
        return ResponseEntity.ok(userService.getCustomers(page, size));
    }

    @GetMapping("/search")
    public HttpEntity<?> searchUserByEmail(@RequestParam(value = "email") String email) {
        return ResponseEntity.ok(discountService.getAllCustomersBySearch(email));
    }

    @PostMapping("/saveHolidays")
    public HttpEntity<?> saveHolidays(@RequestBody List<ReqHoliday> holidays) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        List<Holiday> holidayList = new ArrayList<>();
        for (ReqHoliday holiday : holidays) {
            if (!holiday.isActive()) {
                holidayList.add(new Holiday(
                        holiday.getId(),
                        mainServiceRepository.findById(holiday.getMainServiceId()).orElseThrow(() -> (new ResourceNotFoundException("getMainService", "getMainService", null))),
                        holiday.getName(),
                        holiday.isActive(),
                        holiday.getDescription(),
                        holiday.getDate()
                ));
            } else {
                int orderCountForHoliday = timeTableRepository.getOrderCountForHoliday(simpleDateFormat.format(holiday.getDate()));
                if (orderCountForHoliday == 0) {
                    holidayList.add(new Holiday(
                            holiday.getId(),
                            mainServiceRepository.findById(holiday.getMainServiceId()).orElseThrow(() -> (new ResourceNotFoundException("getMainService", "getMainService", null))),
                            holiday.getName(),
                            holiday.isActive(),
                            holiday.getDescription(),
                            holiday.getDate()
                    ));
                }
            }
        }
        holidayRepository.saveAll(holidayList);
        return ResponseEntity.ok("Successfully saved!");
    }

    @GetMapping("/orderDay/{holDate}")
    public HttpEntity<?> isOrderDay(@PathVariable String holDate) {
        return ResponseEntity.ok(timeTableRepository.getOrderCountForHoliday(holDate));
    }

    @PostMapping("/removePhoto/{id}")
    public HttpEntity<?> removePhoto(@PathVariable UUID id) {
        ApiResponse response = userService.removePhoto(id);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }
    @PostMapping("/addPhoto/{id}/{attach}")
    public HttpEntity<?> addPhoto(@PathVariable UUID id,@PathVariable UUID attach) {
        ApiResponse response = userService.addPhoto(id,attach);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @PostMapping("/customer/edit")
    public HttpEntity<?> editCustomerName(@RequestBody UserDto userDto){
        return ResponseEntity.ok(userService.editCustomerName(userDto));
    }
    @PutMapping("/changeOnlineAndOffline")
    public HttpEntity<?> changeOnlineAndOffline(@CurrentUser User user){
        return ResponseEntity.ok(userService.changeOnlineAndOffline(user));
    }
    @PreAuthorize("hasAnyAuthority('FOR_ADMIN_AGENT_ACTION')")
    @GetMapping("/customer/search")
    public HttpEntity<?> getAgentsFilter(
            @RequestParam(value = "search", required = false) String search,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size
            ) {
        return ResponseEntity.ok(userService.getUsersBySearch(search+'%',page,size));
    }
}