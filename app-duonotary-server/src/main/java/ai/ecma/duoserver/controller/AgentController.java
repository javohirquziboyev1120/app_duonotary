package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.entity.enums.StatusEnum;
import ai.ecma.duoserver.entity.enums.TimeEnum;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.CertificateDto;
import ai.ecma.duoserver.payload.UserDto;
import ai.ecma.duoserver.repository.CertificateRepository;
import ai.ecma.duoserver.repository.PassportRepository;
import ai.ecma.duoserver.repository.RoleRepository;
import ai.ecma.duoserver.repository.UserRepository;
import ai.ecma.duoserver.security.CurrentUser;
import ai.ecma.duoserver.service.AgentService;
import ai.ecma.duoserver.service.CheckUser;
import ai.ecma.duoserver.service.UserService;
import ai.ecma.duoserver.utils.AppConstants;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping("/api/agent")
public class AgentController {
    @Autowired
    AgentService agentService;
    @Autowired
    PassportRepository passportRepository;
    @Autowired
    CertificateRepository certificateRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    UserService userService;
    @Autowired
    CheckUser checkUser;

    /**
     * Admin agentni edit qiladigan method faqat adminga
     *
     * @param userDto agentni malumotlari
     * @return ApiResponse
     */
    @PreAuthorize("hasAnyAuthority('FOR_ADMIN_AGENT_ACTION')")
    @PutMapping("/editAgent")
    public HttpEntity<?> editAgent(@RequestBody UserDto userDto) {
        ApiResponse apiResponse = agentService.editAgent(userDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    /**
     * agent o'zni edit qiladigan method
     *
     * @param userDto agentni malumotlari
     * @return ApiResponse
     */
    @PreAuthorize("hasAnyAuthority('FOR_AGENT_AGENT_ACTION')")
    @PutMapping("/editAgentOwn")
    public HttpEntity<?> editAgentOwn(@RequestBody UserDto userDto,@CurrentUser User user) {
        ApiResponse apiResponse = agentService.editAgentOwn(userDto,user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    /**
     * Agent Passportni add qilish uchun method
     *
     * @param certificateDto malumot
     * @param user           current user
     * @return ApiResponse object=null
     */
    @PreAuthorize("hasAnyAuthority('FOR_AGENT_AGENT_ACTION')")
    @PostMapping("/passport")
    public HttpEntity<?> addPassport(@RequestBody CertificateDto certificateDto, @CurrentUser User user) {
        ApiResponse apiResponse = agentService.addPassport(certificateDto, user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    /**
     * Agent Passportni edit qilish uchun method
     *
     * @param certificateDto malumot
     * @param user           current user
     * @return ApiResponse object=null
     */
    @PreAuthorize("hasAnyAuthority('FOR_AGENT_AGENT_ACTION')")
    @PutMapping("/passport")
    public HttpEntity<?> editPassport(@RequestBody CertificateDto certificateDto, @CurrentUser User user) {
        ApiResponse apiResponse = agentService.editPassport(certificateDto, user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    /**
     * Agent Certificate edit qilish uchun method
     *
     * @param certificateDto malumot
     * @param user           current  user
     * @return ApiResponse object=null
     */
    @PreAuthorize("hasAnyAuthority('FOR_AGENT_AGENT_ACTION')")
    @PutMapping("/certificate")
    public HttpEntity<?> editCertificate(@RequestBody CertificateDto certificateDto, @CurrentUser User user) {
        ApiResponse apiResponse = agentService.editCertificate(certificateDto, user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    /**
     * Agent Certificate add qilish uchun method
     *
     * @param certificateDtos malumot, array keladi
     * @param user            current  user
     * @return ApiResponse object=null
     */
    @PreAuthorize("hasAnyAuthority('FOR_AGENT_AGENT_ACTION')")
    @PostMapping("/certificates")
    public HttpEntity<?> addCertificates(@RequestBody List<CertificateDto> certificateDtos,
                                         @CurrentUser User user) {
        ApiResponse apiResponse = agentService.addCertificates(certificateDtos, user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    /**
     * Agent id orqali  passportlarni olish
     *
     * @param userId agent id
     * @return CertificateDto
     */
    @PreAuthorize("hasAnyAuthority('FOR_AGENT_AGENT_ACTION')")
    @GetMapping("/passport/byUserId/{userId}")
    public HttpEntity<?> getPassportsByUserId(@PathVariable UUID userId, @CurrentUser User user) {
        return ResponseEntity.ok(checkUser.isAdmin(user) ? agentService.getPassportsByUser(userId) : agentService.getPassportsByUser(user.getId()));
    }

    /**
     * Certificate id orqali bitta certificateni Olish
     *
     * @param passportId passport id
     * @return CertificateDto
     */
    @PreAuthorize("hasAnyAuthority('FOR_AGENT_AGENT_ACTION')")
    @GetMapping("/passport/{passportId}")
    public HttpEntity<?> getPassport(@PathVariable UUID passportId) {
        return ResponseEntity.ok(
                agentService.getPassport(passportRepository.findById(passportId).orElseThrow(() -> new ResourceNotFoundException("getPassport", "id", passportId))));
    }

    /**
     * Certificate id orqali bitta certificateni Olish
     *
     * @param certificateId id
     * @return CertificateDto
     */
    @PreAuthorize("hasAnyAuthority('FOR_AGENT_AGENT_ACTION')")
    @GetMapping("/certificate/{certificateId}")
    public HttpEntity<?> getCertificateById(@PathVariable UUID certificateId) {
        return ResponseEntity.ok(agentService.getCertificate(certificateRepository.findById(certificateId).orElseThrow(() -> new ResourceNotFoundException("getCertificate", "id", certificateId))));
    }

    /**
     * Agent Id orqali barcha certificatelarni olish
     *
     * @param userId id
     * @return List<CertificateDto>
     */
    @PreAuthorize("hasAnyAuthority('FOR_AGENT_AGENT_ACTION')")
    @GetMapping("/certificate/byUserId/{userId}")
    public HttpEntity<?> getCertificatesByUserId(@PathVariable UUID userId, @CurrentUser User user) {
        return ResponseEntity.ok(checkUser.isAdmin(user) ? agentService.getCertificatesByUserId(userId) : agentService.getCertificatesByUserId(user.getId()));
    }

    /**
     * Documentni status enumni o'zgartiruvchi method
     *
     * @param documentId id
     * @param statusEnum status
     * @return apiResponse
     */
    @PreAuthorize("hasAnyAuthority('FOR_ADMIN_AGENT_ACTION')")
    @PutMapping("/status")
    public HttpEntity<?> editStatus(@RequestParam(value = "documentId") UUID documentId,
                                    @RequestParam(value = "statusEnum") StatusEnum statusEnum,
                                    @RequestParam(value = "description") String description) throws MessagingException, IOException, TemplateException {
        ApiResponse apiResponse = agentService.editStatus(documentId, statusEnum, description);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    /**
     * Statusga qarab passportlarni olib keladi
     *
     * @param status status
     * @return List<dto
     */
    @PreAuthorize("hasAnyAuthority('FOR_AGENT_AGENT_ACTION')")
    @GetMapping("/passport/status/{status}")
    public HttpEntity<?> getByStatusPassports(@PathVariable StatusEnum status) {
        return ResponseEntity.ok(agentService.getByStatusPassports(status));
    }

    /**
     * Statusga qarab certificatelarni olib keladi
     *
     * @param status status
     * @return list<dto
     */
    @PreAuthorize("hasAnyAuthority('FOR_AGENT_AGENT_ACTION')")
    @GetMapping("/certificate/status/{status}")
    public HttpEntity<?> getByStatusCertificates(@PathVariable StatusEnum status) {
        return ResponseEntity.ok(agentService.getByStatusCertificates(status));
    }

    /**
     * Agentni onlineni o'zgartirish
     *
     * @param userId   agent id
     * @param aBoolean qaysiga uzgartirish
     * @return apiResponse
     */
    @PreAuthorize("hasAnyAuthority('FOR_ADMIN_AGENT_ACTION')")
    @PutMapping("/online")
    public HttpEntity<?> changeAgentOnline(@RequestParam(value = "userId") UUID userId,
                                           @RequestParam(value = "type") Boolean aBoolean) {
        ApiResponse apiResponse = agentService.changeAgentOnline(userId, aBoolean);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    /**
     * Agentni Admin active yoki deactive qiladi
     * states bu zipcodlarni userZipCode tableda true qilamiz!
     *
     * @param userId    id
     * @param stateIdes satates
     * @return ApiResponse
     */
    @PreAuthorize("hasAnyAuthority('FOR_ADMIN_AGENT_ACTION')")
    @PutMapping("/changeActiveAgent/{userId}")
    public HttpEntity<?> activateOrDeactivate(@PathVariable UUID userId, @RequestBody List<UUID> stateIdes) throws MessagingException, IOException, TemplateException {
        ApiResponse apiResponse = agentService.activateOrDeactivate(userId, stateIdes);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    /**
     * Agentlarni listini olish
     *
     * @param page page
     * @param size size
     * @return Pageable
     */
    @PreAuthorize("hasAnyAuthority('FOR_ADMIN_AGENT_ACTION')")
    @GetMapping("/getAgents")
    public HttpEntity<?> getAgentsForAdmin(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                           @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(agentService.getAgents(page, size));
    }

    /**
     * Bitta agentni olish
     *
     * @param id agent id
     * @return UserDto
     */
    @PreAuthorize("hasAnyAuthority('FOR_AGENT_AGENT_ACTION')")
    @GetMapping("/{id}")
    public HttpEntity<?> getAgent(@PathVariable UUID id) {
        User user = userRepository.findById(id).orElseGet(null);
        return ResponseEntity.ok(user == null ? AppConstants.ID_NOT_FOUND : agentService.getAgent(user));
    }

    /**
     * Agentni onlineligini  uzgartirish
     *
     * @param userId      id
     * @param onlineAgent boolean
     * @return apiresponse
     */
    @PreAuthorize("hasAnyAuthority('FOR_ADMIN_AGENT_ACTION')")
    @PutMapping("/changeOnline/{userId}")
    public HttpEntity<?> changeOnlineAgent(@PathVariable UUID userId, @RequestParam(value = "onlineAgent") Boolean onlineAgent) {
        ApiResponse apiResponse = agentService.changeOnlineAgent(userId, onlineAgent);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    /**
     * Agent filter qilib listni olish
     *
     * @param zipCode zipcode
     * @param date    vaqt
     * @return list
     */
    @PreAuthorize("hasAnyAuthority('FOR_ADMIN_AGENT_ACTION')")
    @GetMapping("/getAgents/filter")
    public HttpEntity<?> getAgentsFilter(
            @RequestParam(value = "zipCode", required = false) String zipCode,
            @RequestParam(value = "date", required = false) Date date,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(agentService.getAgentsFilter(zipCode, date,page,size));
    }

    /**
     * Agent filter qilib listni olish
     *
     * @param search text
     * @return list
     */
    @PreAuthorize("hasAnyAuthority('FOR_ADMIN_AGENT_ACTION')")
    @GetMapping("/getAgents/search")
    public HttpEntity<?> getAgentsFilter(
            @RequestParam(value = "search", required = false) String search,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size
            ) {
        return ResponseEntity.ok(agentService.getAgentsBySearch(search+"%",page,size));
    }
    @PreAuthorize("hasAnyAuthority('FOR_ADMIN_AGENT_ACTION')")
    @PutMapping("/userZipCode/enable/{id}")
    public HttpEntity<?> changeEnableUserZipcode(@PathVariable UUID id){
        ApiResponse apiResponse=agentService.changeEnableUserZipCode(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }
    @PreAuthorize("hasAnyAuthority('FOR_AGENT_AGENT_ACTION')")
    @PutMapping("/userZipCode/active/{id}")
    public HttpEntity<?> changeActiveUserZipcode(@PathVariable UUID id){
        ApiResponse apiResponse=agentService.changeActiveUserZipCode(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }
    @PreAuthorize("hasAnyAuthority('FOR_AGENT_AGENT_ACTION')")
    @PutMapping("/userZipCode")
    public HttpEntity<?> addUserZipcode(@RequestBody UUID []zipcode,@CurrentUser User user){
        ApiResponse apiResponse=agentService.addUserZipCode(Arrays.asList(zipcode),user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }
    @PreAuthorize("hasAnyAuthority('FOR_AGENT_AGENT_ACTION')")
    @PutMapping("/userZipCode/{id}")
    public HttpEntity<?> addUserZipcodeForAdmin(@PathVariable UUID id,@RequestBody UUID []zipcode){
        User user = userRepository.findByIdAndEnabledTrue(id).orElse(null);
        ApiResponse apiResponse=new ApiResponse("error",false);
        if (user!=null){
            apiResponse=agentService.addUserZipCode(Arrays.asList(zipcode),user);
        }
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }
    @PreAuthorize("hasAnyAuthority('FOR_AGENT_AGENT_ACTION')")
    @GetMapping("/userZipCode")
    public HttpEntity<?> getUserZipcode(@CurrentUser User user){
        return ResponseEntity.ok(agentService.getUserZipCodeList(user));
    }
    @PreAuthorize("hasAnyAuthority('FOR_AGENT_AGENT_ACTION')")
    @GetMapping("/zipCode")
    public HttpEntity<?> getUserZipcodeForAdd(@CurrentUser User user){ return ResponseEntity.ok(agentService.getUserZipCodeListForAdd(user));
    }
    @PreAuthorize("hasAnyAuthority('FOR_AGENT_AGENT_ACTION')")
    @GetMapping("/zipCode/{id}")
    public HttpEntity<?> getUserZipcodeForAddForAdmin(@PathVariable UUID id){
        User user = userRepository.findByIdAndEnabledTrue(id).orElseGet(null);
        return ResponseEntity.ok(user==null?new ArrayList<>() :agentService.getUserZipCodeListForAdd(user));
    }
    @PreAuthorize("hasAnyAuthority('FOR_ADMIN_AGENT_ACTION')")
    @GetMapping("/userZipCode/{id}")
    public HttpEntity<?> getUserZipcode(@PathVariable UUID id){
        return ResponseEntity.ok(agentService.getUserZipCodeList(userRepository.getOne(id)));
    }

    @GetMapping("/certificate/zipCode")
    public HttpEntity<?> getCertificateByZipCode(@RequestParam("userId") UUID userId,@RequestParam("zipCode")UUID zipCode){
        return ResponseEntity.ok(agentService.getCertificateByZipCode(userId,zipCode));
    }
    @GetMapping("/report")
    public HttpEntity<?> getReport(@CurrentUser User user, @RequestParam(value = "time",required = false)TimeEnum time){
        return ResponseEntity.ok(agentService.getReportForAgent(user,time));
    }


}
