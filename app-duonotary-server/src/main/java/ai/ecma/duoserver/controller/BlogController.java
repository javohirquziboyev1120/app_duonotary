package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.entity.TermsCondition;
import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.payload.*;
import ai.ecma.duoserver.repository.BlogRepository;
import ai.ecma.duoserver.security.CurrentUser;
import ai.ecma.duoserver.service.BlogService;
import ai.ecma.duoserver.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/blog")
public class BlogController {

    @Autowired
    BlogService blogService;
    @Autowired
    BlogRepository blogRepository;

    @PreAuthorize("hasAuthority('ADD_BLOG_SERVICE')")
    @PostMapping
    public HttpEntity<?> addBlog(
            @CurrentUser User user,
            @RequestBody ReqBlog request
    ) {
        ApiResponse apiResponse = blogService.addBlog(user, request, false);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PreAuthorize("hasAuthority('EDIT_BLOG_SERVICE')")
    @PutMapping("/edit/{blogId}")
    public HttpEntity<?> editBlog(
            @PathVariable UUID blogId,
            @CurrentUser User user,
            @RequestBody ReqBlog request
    ) {
        ApiResponse apiResponse = blogService.editBlog(user, request, blogId, false);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PreAuthorize("hasAuthority('ADD_BLOG_SERVICE')")
    @PostMapping("/dynamic")
    public HttpEntity<?> addDynamicBlog(
            @CurrentUser User user,
            @RequestBody ReqBlog request
    ) {
        ApiResponse apiResponse = blogService.addBlog(user, request, true);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PreAuthorize("hasAuthority('EDIT_BLOG_SERVICE')")
    @PutMapping("/edit/dynamic/{blogId}")
    public HttpEntity<?> editDynamicBlog(
            @PathVariable UUID blogId,
            @CurrentUser User user,
            @RequestBody ReqBlog request
    ) {
        ApiResponse apiResponse = blogService.editBlog(user, request, blogId, true);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @GetMapping("/{blogId}")
    public HttpEntity<?> getBlog(
            @PathVariable UUID blogId
    ) {
        ResBlog resBlog = blogService.getResBlog(blogId);
        return ResponseEntity.ok(resBlog);
    }

    @GetMapping("/all")
    public HttpEntity<?> getAllBlogs(
            @RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(name = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
        ResPageable resBlog = blogService.getAll(page, size, false);
        return ResponseEntity.ok(resBlog);
    }

    @GetMapping("/dynamic/all")
    public HttpEntity<?> getAllDynamicBlogs(
            @RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(name = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
        ResPageable resBlog = blogService.getAll(page, size, true);
        return ResponseEntity.ok(resBlog);
    }

    @GetMapping("/featured")
    public HttpEntity<?> getAllFeaturedBlogs() {
        return ResponseEntity.ok(blogRepository.findAllByFeatured(true));
    }

    @GetMapping("/byCategory/{id}")
    public HttpEntity<?> getAllFeaturedBlogs(@PathVariable Integer id) {
        return ResponseEntity.ok(blogRepository.findAllByCategoryId(id));
    }

    @GetMapping
    public HttpEntity<?> getAllUrlBlogs(@RequestParam(value = "url") String url) {
        return ResponseEntity.ok(blogRepository.findByUrl(url));
    }

    @PreAuthorize("hasAuthority('DELETE_BLOG_SERVICE')")
    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteBlog(@PathVariable UUID id) {
        ApiResponse apiResponse = blogService.deleteBlog(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @PreAuthorize("hasAuthority('ADD_TERMS_SERVICE')")
    @PostMapping("/terms")
    public HttpEntity<?> addTerms(
            @CurrentUser User user,
            @RequestBody ReqBlog request
    ) {
        ApiResponse apiResponse = blogService.addTerms(user, request, false);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PreAuthorize("hasAuthority('EDIT_TERMS_SERVICE')")
    @PutMapping("/terms/edit/{termsId}")
    public HttpEntity<?> addTerms(
            @PathVariable UUID termsId,
            @CurrentUser User user,
            @RequestBody ReqBlog request
    ) {
        ApiResponse apiResponse = blogService.editTerms(user, request, termsId, true);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PreAuthorize("hasAuthority('DELETE_TERMS_SERVICE')")
    @DeleteMapping("/terms/{id}")
    public HttpEntity<?> deleteTerms(@PathVariable UUID id) {
        ApiResponse apiResponse = blogService.deleteTerms(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }


    @GetMapping("/terms/{terms}")
    public HttpEntity<?> getTerms(
            @PathVariable String terms
    ) {
        TermsCondition resBlog = blogService.getTerms(terms);
        return ResponseEntity.ok(resBlog);
    }

    //FAQ

    @PostMapping("/faq/add")
    public HttpEntity<?> addFaq(
            @CurrentUser User user,
            @RequestBody ReqFaq request
    ) {
        ApiResponse apiResponse = blogService.addFaq(user, request);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PutMapping("/faq/edit/{id}")
    public HttpEntity<?> editFaq(
            @CurrentUser User user,
            @PathVariable Integer id,
            @RequestBody ReqFaq request
    ) {
        ApiResponse apiResponse = blogService.editFaq(user, id, request);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @DeleteMapping("/faq/delete/{id}")
    public HttpEntity<?> deleteFaq(
            @CurrentUser User user,
            @PathVariable Integer id
    ) {
        ApiResponse apiResponse = blogService.deleteFaq(user, id);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(apiResponse);
    }

    @GetMapping("/faq/get/all")
    public HttpEntity<?> getAllFaq() {
        ApiResponse allFaq = blogService.getAllFaq();
        return ResponseEntity.ok(allFaq);
    }

    @GetMapping("/faq/get")
    public HttpEntity<?> getFaq(
            @RequestParam(name = "role") String role
    ) {
        ApiResponse allFaqByRole = blogService.getAllFaqByRole(role);
        return ResponseEntity.ok(allFaqByRole);
    }
}
