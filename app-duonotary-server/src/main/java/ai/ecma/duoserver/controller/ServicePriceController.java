package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.ResPageable;
import ai.ecma.duoserver.payload.ServicePriceDto;
import ai.ecma.duoserver.repository.ServicePriceRepository;
import ai.ecma.duoserver.service.ServicePriceService;
import ai.ecma.duoserver.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/servicePrice")
public class ServicePriceController {
    @Autowired
    ServicePriceService servicePriceService;
    @Autowired
    ServicePriceRepository servicePriceRepository;

    /**
     * servicePRice ni qo'shish
     *
     * @param servicePriceDto ServicePricedDto da ServicePrice ma'lumotlari keladi
     * @return ro'yxatdan o'tsa true qaytaramiz
     * @currnetUser da user object buladi
     */
    @PreAuthorize("hasAnyAuthority('ADD_SERVICE_PRICE')")
    @PostMapping
    public HttpEntity<?> addServicePrice(@RequestBody ServicePriceDto servicePriceDto) {
        ApiResponse apiResponse = servicePriceService.addOrEditServicePrice(servicePriceDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    /**
     * servicePRice ni edit qilish
     *
     * @param servicePriceDto ServicePricedDto da ServicePrice ma'lumotlari keladi
     * @return ro'yxatdan o'tsa true qaytaramiz
     * @Pathda uzgartirilayotgan servicePrice id isi ketadi
     * @currnetUser da user object buladi
     */
    @PreAuthorize("hasAnyAuthority('EDIT_SERVICE_PRICE')")
    @PutMapping("/editServicePrice")
    public HttpEntity<?> editServicePrice(
            @RequestBody ServicePriceDto servicePriceDto) {
        ApiResponse apiResponse = servicePriceService.addOrEditServicePrice(servicePriceDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(apiResponse);
    }

    /**
     * servicePRice ni malum bir id sini buyicha olib kelishi
     *
     * @path da suralayotgan servicePrice id keladi
     */
    @PreAuthorize("hasAnyAuthority('GET_SERVICE_PRICE')")
    @GetMapping("/{id}")
    public HttpEntity<?> getServicePrice(@PathVariable UUID id) {
        return ResponseEntity.ok(servicePriceRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getServicePricing")));
    }

    @PreAuthorize("hasAnyAuthority('GET_SERVICE_PRICE')")
    @GetMapping("/page")
    protected HttpEntity<?> getServicePricePage(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                                @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(servicePriceService.getServicePricePage(size, page));
    }

    /**
     * servicePRice ni get qilish yani main service va sub serviceni service ichidan get qilib servicePRicedagi
     * shu servicega tegishli eng qiymat va eng arzon narxlarni olib chiqishi
     */
    @PreAuthorize("hasAnyAuthority('GET_SERVICE_PRICE')")
    @GetMapping("/dashboard")
    public HttpEntity<?> getServicePriceDashboard() {
        return ResponseEntity.ok(servicePriceService.getServicePriceDashboard());
    }

    /**
     * servicePRice ni ichidasi servicelarga tegishli zipCodelar qaysi stateda ekanligini va usha statelar
     * ruyxatini olib keladi
     *
     * @param serviceId isi keladi biz shu id buyicha statelarni topamiz
     */
    @PreAuthorize("hasAnyAuthority('GET_SERVICE_PRICE')")
    @GetMapping("/byService/{serviceId}")
    public HttpEntity<?> getStatesByServiceId(@PathVariable UUID serviceId) {
        return ResponseEntity.ok(servicePriceService.getStateByServiceId(serviceId));
    }

    /**
     * servicePRice ni ichidasi servicelarga tegishli zipCodelar qaysi countyda ekanligini va usha countylarni
     * ruyxatini olib keladi
     */
    @PreAuthorize("hasAnyAuthority('GET_SERVICE_PRICE')")
    @GetMapping("/byState")
    public HttpEntity<?> getCountiesByStateAndServiceId(
//            @RequestParam(name = "serviceId") UUID serviceId,
            @RequestParam(name = "stateId") UUID stateId) {
        return ResponseEntity.ok(servicePriceService.getCountiesByStateAndServiceId(stateId));
    }

    /**
     * servicePRice ni ichidasi servicelarga tegishli countylar ichidagi zipCodelar
     * ruyxatini olib keladi
     */
    @PreAuthorize("hasAnyAuthority('GET_SERVICE_PRICE')")
    @GetMapping("/byCounty")
    public HttpEntity<?> getZipCodesByServiceIdAndCountyId(
//            @RequestParam(name = "serviceId") UUID serviceId,
            @RequestParam(name = "countyId") UUID countyId) {
        return ResponseEntity.ok(servicePriceService.getZipCodesByServiceIdAndCountyId(countyId));
    }

    @GetMapping("/byZipCode")
    public HttpEntity<?> getServicePriceByZipCodeAndMainServiceId(@RequestParam(name = "mainService") UUID mainServiceId,
                                                                  @RequestParam(name = "zipCode") String zipCode) {
        return ResponseEntity.ok(servicePriceService.getServicePriceByZipCodeAndMainServiceId(mainServiceId, zipCode));
    }

    @GetMapping("/byZipCodeId")
    public HttpEntity<?> getServicePrisesByZipCodeId(@RequestParam(name = "zipCodeId") String zipCodeId) {
        return ResponseEntity.ok(servicePriceRepository.findAllByZipCodeAndActiveAndInPerson(UUID.fromString(zipCodeId)));
    }

    @GetMapping("/online")
    public HttpEntity<?> getAllServicePriceForOnline(){
        return ResponseEntity.ok(servicePriceRepository.findAllByService_MainService_Online(true));
    }



}
