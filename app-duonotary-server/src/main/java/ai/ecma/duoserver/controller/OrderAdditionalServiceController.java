package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.OrderAdditionalServiceDto;
import ai.ecma.duoserver.security.CurrentUser;
import ai.ecma.duoserver.service.OrderAdditionalServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/orderAdditionalService")
public class OrderAdditionalServiceController {

    @Autowired
    OrderAdditionalServiceService additionalServiceService;

    @GetMapping
    public HttpEntity<?> getOrderAdditionalServicePageable(@RequestParam(value = "page", defaultValue = "0") int page,
                                                           @RequestParam(value = "size", defaultValue = "10") int size,
                                                           @RequestParam(value = "clientOrAgentId") UUID clientOrAgentId,
                                                           @CurrentUser User user) {

        return ResponseEntity.ok(additionalServiceService.getOrderAdditionalServicePageable(page, size, clientOrAgentId, user));
    }

    @PutMapping
    public HttpEntity<?> updateOrderAdditionalService(OrderAdditionalServiceDto orderAdditionalServiceDto) {
        ApiResponse apiResponse = additionalServiceService.addOrderAdditionService(orderAdditionalServiceDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }
}
