package ai.ecma.duoserver.controller;


import ai.ecma.duoserver.entity.MainServiceWorkTime;
import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.repository.CountyRepository;
import ai.ecma.duoserver.repository.MainServiceWorkTimeRepository;
import ai.ecma.duoserver.security.CurrentUser;
import ai.ecma.duoserver.service.CheckRole;
import ai.ecma.duoserver.utils.AppConstants;
import org.javers.core.Changes;
import org.javers.core.Javers;
import org.javers.core.json.JsonConverter;
import org.javers.core.metamodel.object.CdoSnapshot;
import org.javers.repository.jql.JqlQuery;
import org.javers.repository.jql.QueryBuilder;
import org.javers.shadow.Shadow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

@RestController
@RequestMapping(value = "/api/audit")
public class AuditController {
    private static final Logger logger = LoggerFactory.getLogger(AuditController.class);

    private final Javers javers;

    @Autowired
    CountyRepository countyRepository;
    @Autowired
    CheckRole checkRole;
    @Autowired
    MainServiceWorkTimeRepository mainServiceWorkTimeRepository;

    @Autowired
    public AuditController(Javers javers) {
        this.javers = javers;
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @GetMapping("/getItemByAuthor/{id}")
    public String getItemsByAuthor(@PathVariable UUID id,
                                   @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                   @RequestParam(value = "size", defaultValue = "20") int size) {
        QueryBuilder jqlQuery = QueryBuilder.anyDomainObject().withNewObjectChanges();
        List<CdoSnapshot> changes = new ArrayList(javers.findSnapshots(jqlQuery.byAuthor(id.toString()).skip(page * size).limit(size).build()));
        changes.sort((o1, o2) -> -1 * o1.getCommitMetadata().getCommitDate().compareTo(o2.getCommitMetadata().getCommitDate()));
        JsonConverter jsonConverter = javers.getJsonConverter();
        return jsonConverter.toJson(changes);
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @GetMapping("/getTotalElementsCount")
    public HttpEntity<?> getTotalElementsCount(
            @RequestParam(name = "tableName", defaultValue = "") String tableName,
            @RequestParam(name = "id", defaultValue = "") String id,
            @RequestParam(name = "admin", defaultValue = "false") boolean admin
    ) {
        if (tableName.isEmpty()) {
            if (id == null || id.equals("")) {
                return ResponseEntity.ok(countyRepository.getTotalElementsCount());
            } else if (admin) {
                Integer totalElementCountForAdminItem = countyRepository.getTotalElementCountForAdminItem(id);
                return ResponseEntity.ok(totalElementCountForAdminItem);
            } else {
                return ResponseEntity.ok(countyRepository.getTotalElementCountForItem("\"" + id + "\""));
            }
        } else {
            return ResponseEntity.ok(countyRepository.getTotalElementsCountByTableName(tableName));
        }
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @GetMapping("/getAuditTableList")
    public HttpEntity<?> getAuditTableList(@CurrentUser User user) {
        if (checkRole.isROLE_SUPER_ADMIN(user.getRoles())) {
            return ResponseEntity.ok(countyRepository.getAuditTableList());
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping("/{tableName}")//bu matn qaytaradi
    public String getPersonChanges(@PathVariable String tableName) {
        Class<?> theClass = null;
        try {
            theClass = Class.forName(("ai.ecma.duoserver.entity." + tableName));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        QueryBuilder jqlQuery = QueryBuilder.byClass(theClass);
        Changes changes = javers.findChanges(jqlQuery.build());
        return "<pre>" + changes.prettyPrint() + "</pre>";
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping("/tableItemChanges")//bu matn qaytaradi
    public String getTableItemChanges(@RequestParam(name = "tableName") String tableName,
                                      @RequestParam(name = "tableItemId", defaultValue = "") String tableItemId) {
        Class<?> theClass = null;
        try {
            theClass = Class.forName(("ai.ecma.duoserver.entity." + tableName));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        QueryBuilder jqlQuery = QueryBuilder.byInstanceId(tableItemId, theClass)
                .withNewObjectChanges();
        Changes changes = javers.findChanges(jqlQuery.build());
        return "<pre>" + changes.prettyPrint() + "</pre>";
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping("/tableOneItemChanges/{id}")//bu matn qaytaradi
    public String getTableOneItemChanges(@PathVariable UUID id,
                                         @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                         @RequestParam(value = "size", defaultValue = "20") int size) {
        String tableNameForItem = countyRepository.getTableNameForItem("\"" + id.toString() + "\"");
        Class<?> theClass = null;
        try {
            theClass = Class.forName(("ai.ecma.duoserver.entity." + tableNameForItem));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        QueryBuilder jqlQuery = QueryBuilder.byInstanceId(id, theClass);
        List<CdoSnapshot> changes = new ArrayList(javers.findSnapshots(jqlQuery.skip(page * size).limit(size).build()));
        changes.sort((o1, o2) -> -1 * o1.getCommitMetadata().getCommitDate().compareTo(o2.getCommitMetadata().getCommitDate()));
        JsonConverter jsonConverter = javers.getJsonConverter();
        return jsonConverter.toJson(changes);
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping("/{tableName}/snapshots")
    //tableName bo'yicha bu json qaytaradi(faqat o'zgargan fieldlarning nomini)
    public String getTableSnapshots(@PathVariable String tableName,
                                    @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                    @RequestParam(value = "size", defaultValue = "20") int size) {
        Class<?> theClass = null;
        try {
            theClass = Class.forName(("ai.ecma.duoserver.entity." + tableName));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        QueryBuilder jqlQuery = QueryBuilder.byClass(theClass);
        List<CdoSnapshot> changes = new ArrayList(javers.findSnapshots(jqlQuery.skip(page * size).limit(size).build()));
        changes.sort((o1, o2) -> -1 * o1.getCommitMetadata().getCommitDate().compareTo(o2.getCommitMetadata().getCommitDate()));
        JsonConverter jsonConverter = javers.getJsonConverter();
        return jsonConverter.toJson(changes);
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping("/snapshots")//barcha tabellar bo'yicha  json qaytaradi(faqat o'zgargan fieldlarning nomini)
    public String getAllSnapshots(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                  @RequestParam(value = "size", defaultValue = "20") int size, @CurrentUser User user) {
        QueryBuilder jqlQuery = QueryBuilder.anyDomainObject().withNewObjectChanges();
        List<CdoSnapshot> changes = new ArrayList(javers.findSnapshots(jqlQuery.skip(page * size).limit(size).build()));
        changes.sort((o1, o2) -> -1 * o1.getCommitMetadata().getCommitDate().compareTo(o2.getCommitMetadata().getCommitDate()));
        JsonConverter jsonConverter = javers.getJsonConverter();
        return jsonConverter.toJson(changes);
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping("/changes")
//berilgan tabledagi obyektning o'zgarishini qaytaradi(oldingi holati va hozirigi holatini)
    public String getTableSnapshots(@RequestParam(name = "tableName") String tableName,
                                    @RequestParam(name = "tableItemId", defaultValue = "") String tableItemId,
                                    @RequestParam(name = "commitId", defaultValue = "0") String commitId) {
        Class<?> theClass = null;
        try {
            theClass = Class.forName("ai.ecma.duoserver.entity." + tableName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        QueryBuilder jqlQuery = QueryBuilder.byInstanceId(tableItemId, theClass);
        Changes changes = null;
        if (Double.parseDouble(commitId) != 0) {
            changes = javers.findChanges(jqlQuery.withCommitId(BigDecimal.valueOf(Double.parseDouble(commitId))).build());
        } else {
            changes = javers.findChanges(jqlQuery.build());

        }
        JsonConverter jsonConverter = javers.getJsonConverter();
        return jsonConverter.toJson(changes);
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @GetMapping("/workTime/{id}/shadows")
    public String getStoreShadows(@PathVariable UUID id) {
        MainServiceWorkTime mainServiceWorkTime = mainServiceWorkTimeRepository.findById(id).get();
        JqlQuery jqlQuery = QueryBuilder.byInstance(mainServiceWorkTime)
                .withChildValueObjects().build();
        List<Shadow<MainServiceWorkTime>> shadows = javers.findShadows(jqlQuery);
        return javers.getJsonConverter().toJson(shadows.get(0));
    }

}
