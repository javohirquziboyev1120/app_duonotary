package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.service.AdminDashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/api/dashboard/")
public class AdminDashboardController {
    @Autowired
    AdminDashboardService adminDashboardService;

    @GetMapping("events")
    public HttpEntity<?> getUsersAndOrdersCount() {
        ApiResponse apiResponse = adminDashboardService.getUsersAndOrdersCount();
        return ResponseEntity.ok(apiResponse);
    }

    @GetMapping("orders")
    public HttpEntity<?> getOrdersSum() {
        ApiResponse apiResponse = adminDashboardService.getOrdersSum();
        return ResponseEntity.ok(apiResponse);
    }

    @GetMapping("filter")
    public HttpEntity<?> getOrdersByFilter(
            @RequestParam(value = "stateId", defaultValue = "") String stateId,
            @RequestParam(value = "countyId", defaultValue = "") String countyId,
            @RequestParam(value = "zipCodeId", defaultValue = "") String zipCodeId,
            @RequestParam(value = "date", defaultValue = "") String date) {
        ApiResponse apiResponse = adminDashboardService.getOrdersByFilter(stateId, countyId, zipCodeId, date);
        return ResponseEntity.ok(apiResponse);
    }
}
