package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.CustomDiscountDto;
import ai.ecma.duoserver.repository.CustomDiscountRepository;
import ai.ecma.duoserver.service.CustomDiscountService;
import ai.ecma.duoserver.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/customDiscount")
public class CustomDiscountController {
    @Autowired
    CustomDiscountService customDiscountService;
    @Autowired
    CustomDiscountRepository customDiscountRepository;

    /**
     * CustomDiscount ni qo'shish uchun method
     * @param dto
     * @return ApiResponse object=null
     */
    @PreAuthorize("hasAuthority('ADD_CUSTOM_DISCOUNT')")
    @PostMapping
    protected HttpEntity<?> addCustomDiscount(@RequestBody CustomDiscountDto dto){
        ApiResponse apiResponse=customDiscountService.addCustomDiscount(dto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    /**
     * CustomDiscount ni tahrirlash uchun method
     * @param dto
     * @return ApiResponse object=null
     */
    @PreAuthorize("hasAuthority('ADD_CUSTOM_DISCOUNT')")
    @PutMapping
    protected HttpEntity<?> editCustomDiscount(@RequestBody CustomDiscountDto dto){
        ApiResponse apiResponse=customDiscountService.editCustomDiscount(dto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    /**
     * CustomDiscountlarni barchasini qaytaradi faqat Pageable qilib
     * @param page
     * @param size
     * @return ResPageable
     */
    @PreAuthorize("hasAuthority('ADD_CUSTOM_DISCOUNT')")
    @GetMapping
    protected HttpEntity<?> getCustomDiscountList(@RequestParam(value = "page",defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                                  @RequestParam(value = "size",defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size){
        return ResponseEntity.ok(customDiscountService.getCustomDiscountList(page,size));
    }

    /**
     * Bir dona CustomDiscount ni qaytarish uchun method
     * @param id CustomDiscountId
     * @return CustomDiscountDto
     */
    @PreAuthorize("hasAuthority('GET_ONE_CUSTOM_DISCOUNT')")
    @GetMapping("/{id}")
    protected HttpEntity<?> getCustomDiscountById(@PathVariable UUID id){
        return ResponseEntity.ok(customDiscountService.getCustomDiscount(customDiscountRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getCustomDiscount","id",id))));
    }

    /**
     * Bir Clientni barcha CustomDiscountlarni qaytarish
     * @param id UserId
     * @return List<CustomDiscount>
     */
    @PreAuthorize("hasAuthority('ADD_CUSTOM_DISCOUNT')")
    @GetMapping("/byUserId/{id}")
    protected HttpEntity<?> getCustomDiscountsByUser(@PathVariable UUID id){
        return ResponseEntity.ok(customDiscountService.getAllByUserId(id));
    }
}
