package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.entity.ServicePrice;
import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.entity.enums.OrderStatus;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.OrderDto;
import ai.ecma.duoserver.payload.UserDto;
import ai.ecma.duoserver.repository.ServicePriceRepository;
import ai.ecma.duoserver.repository.UserRepository;
import ai.ecma.duoserver.security.CurrentUser;
import ai.ecma.duoserver.service.CheckRole;
import ai.ecma.duoserver.service.DiscountService;
import ai.ecma.duoserver.service.OrderService;
import ai.ecma.duoserver.utils.AppConstants;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/order")
public class OrderController {
    final
    DiscountService discountService;
    final
    ServicePriceRepository servicePriceRepository;
    final
    UserRepository userRepository;
    final
    OrderService orderService;
    final
    CheckRole checkRole;


    public OrderController(OrderService orderService, UserRepository userRepository, CheckRole checkRole, DiscountService discountService, ServicePriceRepository servicePriceRepository) {
        this.orderService = orderService;
        this.userRepository = userRepository;
        this.checkRole = checkRole;
        this.discountService = discountService;
        this.servicePriceRepository = servicePriceRepository;
    }

    /**
     * buyurtmani ro`yxatga olish va tahrirlash
     */
    @PostMapping
    public HttpEntity<?> addOrder(@RequestBody OrderDto orderDto,
                                  @CurrentUser User user,
                                  @RequestParam(value = "id", required = false) String id) {
        if (orderDto.getId() == null) {
            if (orderDto.getClientId() == null && !orderDto.isNewUser())
                orderDto.setClientId(user.getId());
        }
        ApiResponse apiResponse = orderService.addOrder(orderDto, id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    /**
     * buyurtmani tahrirlash faqat customer tomonidan
     */
    @PutMapping
    public HttpEntity<?> editOrderByCustomer(@RequestBody OrderDto orderDto, @CurrentUser User user) {
        ApiResponse apiResponse = orderService.editOrder(orderDto, user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    /**
     * order id orqali 1 ta orderni olish
     */
    @PreAuthorize("hasAuthority('GET_ORDER')")
    @GetMapping("/{id}")
    public HttpEntity<?> getOrder(@PathVariable UUID id) {
        return ResponseEntity.ok(orderService.getOne(id));
    }

    /**
     * userId yoki current user orqali orderlarni pageable qilib olish
     **/
    @PreAuthorize("hasAuthority('GET_ORDER_LIST')")
    @GetMapping
    public HttpEntity<?> getOrderPageable(@RequestParam(value = "page", defaultValue = "0") int page,
                                          @RequestParam(value = "size", defaultValue = "10") int size,
                                          @RequestParam(value = "userId", required = false) UUID userId,
                                          @CurrentUser User user) {
        return ResponseEntity.ok(orderService.getPageable(page, size, userId, user));
    }


    //    @PreAuthorize("hasAuthority('GET_ORDER_LIST_FOR_ADMIN')")
    @GetMapping("/getOrderList")
    public HttpEntity<?> getOrderPageable(@RequestParam(value = "page", defaultValue = "0") int page,
                                          @RequestParam(value = "size", defaultValue = "10") int size) {
        return ResponseEntity.ok(orderService.getOrderList(page, size));
    }


    /**
     * Complete the order
     */
    @PreAuthorize("hasAnyAuthority('COMPLETE_ORDER','ROLE_SUPER_ADMIN','ROLE_AGENT')")
    @PutMapping("/complete/{id}")
    public HttpEntity<?> orderComplete(@PathVariable UUID id, @CurrentUser User user) {
        try {
            ApiResponse apiResponse = orderService.orderCompleter(id, user);
            return ResponseEntity.ok(apiResponse);
        } catch (ResourceNotFoundException e) {
            throw new ResourceNotFoundException("getOrder", "id", id);
        }
    }

    /**
     * Calculate the amount of money to be deducted for order cancellation
     */
    @GetMapping("/cancel/{id}")
    public HttpEntity<?> orderCancelStepOne(@PathVariable UUID id, @CurrentUser User user) {
        try {
            ApiResponse apiResponse = orderService.calculateOrderToCancel(id, user);
            return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
        } catch (ResourceNotFoundException e) {
            throw new ResourceNotFoundException("getOrder", "id", id);
        }
    }

    /**
     * Cancel the order
     */
    @PutMapping("/cancel/{id}")
    public HttpEntity<?> orderCancelStepTwo(@PathVariable UUID id, @CurrentUser User user) {
        try {
            ApiResponse apiResponse = orderService.orderCanceler(id, user);
            return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
        } catch (ResourceNotFoundException e) {
            throw new ResourceNotFoundException("getOrder", "id", id);
        }
    }

    /**
     * buyurtmani o`chirish
     */
    @PreAuthorize("hasAuthority('DELETE_ORDER')")
    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteOrder(@PathVariable UUID id) {
        ApiResponse apiResponse = orderService.deleteOrder(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @PostMapping("/amounts")
    public HttpEntity<?> getClientDiscounts(@RequestBody OrderDto orderDto, @CurrentUser User user) {
        if (!orderDto.isNewUser())
            orderDto.setClientId(user.getId());
        return ResponseEntity.status(200).body(orderService.getAllClientDiscount(orderDto));
    }

    @PostMapping("/orderAmount")
    public HttpEntity<?> getOrderAmount(@RequestBody OrderDto order) {
        ServicePrice servicePrice = servicePriceRepository.findById(order.getServicePriceId()).orElseThrow(() -> new ResourceNotFoundException("service price", "", ""));
        Double orderAmount = orderService.amountOrderWithoutDiscount(servicePrice, order);
        return ResponseEntity.ok(orderAmount);
    }

    //    @PreAuthorize("hasAuthority('GET_ORDER')")
    @GetMapping("/byUserId/{id}")
    public HttpEntity<?> getOrderByUserId(@PathVariable UUID id, @CurrentUser User user, @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                          @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        User userById = userRepository.getOne(id);
        return ResponseEntity.ok(checkRole.isAdmin(user.getRoles()) ? checkRole.isAgent(userById.getRoles()) ? orderService.getAllByAgentId(page, size, userById) : orderService.getAllByUserId(page, size, userById) : checkRole.isAgent(user.getRoles()) ? orderService.getAllByAgentId(page, size, user) : orderService.getAllByUserId(page, size, user));
    }

    @GetMapping("/byUserId")
    public HttpEntity<?> getOrderByUser(@CurrentUser User user, @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                        @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        User userById = userRepository.getOne(user.getId());
        return ResponseEntity.ok(checkRole.isAdmin(user.getRoles()) ? checkRole.isAgent(userById.getRoles()) ? orderService.getAllByAgentId(page, size, userById) : orderService.getAllByUserId(page, size, userById) : checkRole.isAgent(user.getRoles()) ? orderService.getAllByAgentId(page, size, user) : orderService.getAllByUserId(page, size, user));
    }

    /**
     * If the customer selects now option of th online order, the System will return to him the id of the current free agent
     */
    @GetMapping("/getOnlineAgentForNow")
    public HttpEntity<?> getFreeAgentIdNow(@RequestParam(value = "tempId") UUID tempId) {
        return ResponseEntity.ok(orderService.getFreeAgentIdNow(tempId));
    }

    @GetMapping("/getService")
    public HttpEntity<?> getServiceBySubService() {
        return ResponseEntity.ok(orderService.getServiceForNow());
    }

    @GetMapping("/client/mainPage")
    public HttpEntity<?> getOrdersByClient(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                           @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
                                           @CurrentUser User user) {
        return ResponseEntity.ok(orderService.getOrdersByClient(page, size, user.getId(), 3));
    }

    @GetMapping("/client/mainPage/{userId}")
    public HttpEntity<?> getOrdersByClient(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                           @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
                                           @CurrentUser User user,
                                           @PathVariable UUID userId) {
        if (checkRole.isAdmin(user.getRoles()) || checkRole.isROLE_SUPER_ADMIN(user.getRoles()))
            return ResponseEntity.ok(orderService.getOrdersByClient(page, size, userId, 3));
        return ResponseEntity.ok("");
    }

    @GetMapping("/admin/order")
    public HttpEntity<?> getOrdersGroupByAgent(@RequestParam(value = "id") UUID id,
                                               @RequestParam(value = "online") boolean online) {
        ApiResponse response = orderService.getOrdersGroupByAgent(id, online);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }

    //todo PERMISHSHIN QOYISH KK
    @GetMapping("/admin/agent/{id}")
    public HttpEntity<?> getOrdersByAgent(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                          @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
                                          @PathVariable UUID id, @CurrentUser User user) {
        UUID clientId = user.getId();
        if (id != null) {
            clientId = id;
        }
        return ResponseEntity.ok(orderService.getOrdersByClient(page, size, clientId, 4));
    } //todo PERMISHSHIN QOYISH KK

    @GetMapping("/admin/agent")
    public HttpEntity<?> getOrdersByAgent2(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                           @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
                                           @CurrentUser User user) {
        return ResponseEntity.ok(orderService.getOrdersByClient(page, size, user.getId(), 4));
    }

    @GetMapping("/searchAgent")
    public HttpEntity<?> findFreeAgentsByEmailAndTime(@RequestParam(value = "online") boolean online,
                                                      @RequestParam(value = "zipCodeId") String zipCodeId,
                                                      @RequestParam(value = "start") String start,
                                                      @RequestParam(value = "end") String end) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
            Timestamp fromTime = new java.sql.Timestamp(dateFormat.parse(start).getTime());
            Timestamp endTime = new java.sql.Timestamp(dateFormat.parse(end).getTime());
            List<UserDto> list;
            if (!zipCodeId.equals("null"))
                list = orderService.findAgentToAdmin(online, UUID.fromString(zipCodeId), fromTime, endTime);
            else list = orderService.findAgentToAdmin(online, null, fromTime, endTime);
            return ResponseEntity.ok(list);
        } catch (Exception e) {
            return ResponseEntity.ok("");
        }

    }

    @GetMapping("/customers")
    public HttpEntity<?> getCustomersWithOrder(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                               @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(orderService.getCustomer(page, size));
    }


    // Bu Kontrollerda Online ishlaydigan Agent Order ni  Tayyor bolgan documentlarni DocverifyId sini yoki PackageId sini yuklaydi
//    @PreAuthorize("hasAuthority('ADD_DOCVERIFY_ID_OR_PACKAGE_ID_FROM_DOCVERIFY')")
    //Bu method orderni Statusini NEW dan In_PROGRESSGA ham o`tkizadi
    @GetMapping("/changeStatus")
    public HttpEntity<?> changeStatus(@RequestParam(value = "id") UUID id,
                                      @RequestParam(value = "docId", required = false) String docId,
                                      @RequestParam(value = "isPackage", required = false) boolean isPackage) {
        return ResponseEntity.ok(orderService.changeStatus(id, docId, isPackage));
    }

    //    @PreAuthorize("hasAuthority('ROLE_AGENT')")
    @GetMapping("/changeAgent")
    public HttpEntity<?> changeAgent(@RequestParam(value = "sender") boolean sender,
                                     @RequestParam(value = "accept", required = false) boolean accept,
                                     @RequestParam(value = "id", required = false) UUID id,
                                     @RequestParam(value = "orderId", required = false) UUID orderId,
                                     @CurrentUser User user) {
        return ResponseEntity.ok(orderService.changeAgent(user.getId(), orderId, sender, accept, id));
    }

    @PostMapping("/document")
    public HttpEntity<?> orderDocument(
            @RequestParam(value = "order") UUID order,
            @RequestParam(value = "attach") UUID attach
    ) {
        return ResponseEntity.ok(orderService.addDocument(order, attach));
    }

    @DeleteMapping("/document")
    public HttpEntity<?> deleteDocument(
            @RequestParam(value = "order") UUID order,
            @RequestParam(value = "attach") UUID attach) {
        return ResponseEntity.ok(orderService.deleteDocument(order, attach));
    }

    @GetMapping("/search")
    public HttpEntity<?> searchOrderBySerialNumber(@RequestParam String serialNumber) {
        return ResponseEntity.ok(orderService.searchOrderBySerialNumber(serialNumber));
    }

    @PreAuthorize("hasAnyAuthority('FOR_ADMIN_AGENT_ACTION')")
    @GetMapping("/getOrder/filter")
    public HttpEntity<?> getOrdersFilter(
            @RequestParam(value = "zipCode", required = false) String zipCode,
            @RequestParam(value = "date", required = false) Date date,
            @RequestParam(value = "status", required = false) String status) {
        return ResponseEntity.ok(orderService.getOrdersFilter(zipCode, date, status));
    }

    @PreAuthorize("hasAnyAuthority('FOR_ADMIN_AGENT_ACTION')")
    @GetMapping("/getOrders/search")
    public HttpEntity<?> getOrdersSearch(
            @RequestParam(value = "search", required = false) String search,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(orderService.getOrdersBySearch(search + "%", page, size));
    }

    //    @PreAuthorize("hasAnyAuthority('FOR_ADMIN_AGENT_ACTION')")
    @GetMapping("/getOrders/search/byClient")
    public HttpEntity<?> getOrdersSearchForClient(
            @CurrentUser User user,
            @RequestParam(value = "search", required = false) String search,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size ) {
        return ResponseEntity.ok(orderService.getOrdersBySearchForClient(search + "%", page,size, user));
    }
    @GetMapping("/getOrders/search/byAgent")
    public HttpEntity<?> getOrdersSearchForAgent(
            @CurrentUser User user,
            @RequestParam(value = "search", required = false) String search,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(orderService.getOrdersBySearchForAgent(search + "%",page,size, user));
    }

    //////////////////////////////////// ------------------------------------- //////////////////////////////////////

    @GetMapping("/zipCode")
    public HttpEntity<?> findZipCodeAndServicePrices(@RequestParam(value = "code") String code) {
        return ResponseEntity.ok(orderService.findZipCodeAndServicePrice(code));
    }

//    @PutMapping("/editDiscount")
//    public HttpEntity<?> editOrderDiscountByCustomer(@RequestBody OrderDto orderDto) {
//        if (!orderDto.getDiscountsDtos().isEmpty())
//        ApiResponse apiResponse = orderService.editOrderDiscount(orderDto);
//        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
//    }


    @GetMapping("/mobile/mainPage")
    public HttpEntity<?> getOrdersForMobile(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
                                            @RequestParam(value = "status", defaultValue = "NEW") OrderStatus orderStatus,
                                            @CurrentUser User user) {
        return ResponseEntity.ok(orderService.getOrderListByStatus(orderStatus, user, page, size));
    }
}