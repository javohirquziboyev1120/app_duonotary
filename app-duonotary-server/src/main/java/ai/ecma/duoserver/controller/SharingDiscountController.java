package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.payload.AgentResetDiscountDto;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.security.CurrentUser;
import ai.ecma.duoserver.service.CheckRole;
import ai.ecma.duoserver.service.SharingAndLoyaltyDiscountService;
import ai.ecma.duoserver.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/sharingDiscount")
public class SharingDiscountController {
    @Autowired
    SharingAndLoyaltyDiscountService sharingAndLoyaltyDiscountService;
    @Autowired
    CheckRole checkRole;

    @GetMapping
    public HttpEntity<?> getSharingDiscountForClient(@CurrentUser User user) {
        return ResponseEntity.status(201).body(new ApiResponse("This is sharing discount", true, sharingAndLoyaltyDiscountService.getUserSharingDiscount(user)));
    }

    @GetMapping("/list")
    public HttpEntity<?> getSharingDiscountList(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
                                                @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size, @CurrentUser User user) {
        return ResponseEntity.ok(sharingAndLoyaltyDiscountService.getSharingDiscountPage(page, size, (checkRole.isAdmin(user.getRoles()) ? null : user)));

    }

    /*
     * Admin tomonidan agentga sharing discount bonus puli berilganida, agentni bonuslarini o'chirish uchun
     * */
    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @PostMapping("/resetDiscount")
    public HttpEntity<?> resetAgentDiscount(@RequestBody AgentResetDiscountDto agentResetDiscountDto) {
        ApiResponse apiResponse = sharingAndLoyaltyDiscountService.resetAgentDiscount(agentResetDiscountDto.getEmail(), agentResetDiscountDto.getAmount());
        return ResponseEntity.ok(apiResponse);
    }
}
