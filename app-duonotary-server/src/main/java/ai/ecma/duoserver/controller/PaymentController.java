package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.PaymentDto;
import ai.ecma.duoserver.service.PaymentService;
import com.stripe.exception.StripeException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/payment")
public class PaymentController {

    final
    PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    //Bu method orqali tolov ni darhol pul yeshib olish uchun front ga clientSecretKey yasab beradigan method
    @GetMapping("/getChargeOnTime")
    public HttpEntity<?> getClientSecterKetForStripeChargeByOrderId(@RequestParam UUID id) {
        String clentSecretKey = paymentService.getChangeOnTime(id);
        return ResponseEntity.status(clentSecretKey != null ? 200 : 409).body(clentSecretKey);
    }

    //Bu Method orqali orderId va stripe_Token_Id kelsa Future  Charge amalga oshiriladi
    @GetMapping("/getFutureCharge")
    public ApiResponse getFutureCharge(@RequestParam UUID orderId,
                                       @RequestParam String tokenId) {
        return paymentService.getFutureCharge(orderId, tokenId);
    }

    //Bu Method orqali orderId va stripe_Token_Id kelsa Future  Charge amalga oshiriladi
    @GetMapping("/getTestFutureCharge")
    public HttpEntity<?> getTestFutureCharge(@RequestParam UUID orderId) throws StripeException {
        return ResponseEntity.ok(paymentService.testFuturePayment(orderId));
    }

    @GetMapping("/config")
    public HttpEntity<?> configPayment(@RequestParam UUID orderId,
                                       @RequestParam String secretId){
        return ResponseEntity.ok(paymentService.configFuturePayment(secretId, orderId));
    }

    @GetMapping("/success")
    public HttpEntity<?> setPayed(@RequestParam(value = "id") UUID id, @RequestParam(value = "checkId") String checkId) {
        return ResponseEntity.ok(paymentService.setPayStatus(id, checkId));
    }

    //    @PreAuthorize("hasAnyAuthority('GET_PAYMENT_LIST')") todo sleshdan chiqarib create always qilish kk
    @GetMapping
    public HttpEntity<?> getInfoPayment(@RequestParam(value = "page", defaultValue = "0") int page,
                                        @RequestParam(value = "size", defaultValue = "10") int size) {
        return ResponseEntity.ok(paymentService.getPaymentInfo(page, size));
    }

    @GetMapping("/admin")
    public HttpEntity<?> getPayments(@RequestParam(value = "page", defaultValue = "0") int page,
                                     @RequestParam(value = "size", defaultValue = "10") int size) {
        return ResponseEntity.ok(paymentService.getPayments(page, size));
    }

    @PostMapping
    public HttpEntity<?> addOrEditPaymentAdmin(@RequestBody PaymentDto paymentDto) {
        return ResponseEntity.ok(paymentService.addOrEditPaymentAdmin(paymentDto));
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deletePayment(@PathVariable UUID id) {
        return ResponseEntity.ok(paymentService.deletePayment(id));
    }

}
