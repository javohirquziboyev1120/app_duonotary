package ai.ecma.duoserver.controller;

import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.payload.AgentScheduleDto;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.repository.AgentScheduleRepository;
import ai.ecma.duoserver.security.CurrentUser;
import ai.ecma.duoserver.service.AgentScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/agentSchedule")
public class AgentScheduleController {
    @Autowired
    AgentScheduleService agentScheduleService;

    @Autowired
    AgentScheduleRepository agentScheduleRepository;

    //    @PreAuthorize("hasAuthority('ADD_AGENT_SCHEDULE')")
    @PostMapping
        public HttpEntity<?> addOrEditSchedule(@CurrentUser User user,
                                           @RequestBody AgentScheduleDto agentScheduleDto) {
        ApiResponse apiResponse = agentScheduleService.addOrEditSchedule(user, agentScheduleDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    //    @PreAuthorize("hasAuthority('GET_AGENT_SCHEDULE')")
    @GetMapping("/byAgent")
    public HttpEntity<?> getSchedule(@CurrentUser User user) {
        List<AgentScheduleDto> schedule = agentScheduleService.getAllScheduleByAgent(user.getId(), user);
        return ResponseEntity.ok(schedule);
    }

    //    @PreAuthorize("hasAuthority('GET_AGENT_SCHEDULE')")
    @GetMapping("/workDay/{agentId}")
    public HttpEntity<?> getAgentWorkTimes(@PathVariable UUID agentId) {
        return ResponseEntity.ok(agentScheduleService.getAgentWorkTimes(agentId));
    }

    //    @PreAuthorize("hasAuthority('GET_AGENT_SCHEDULE')")
    @GetMapping("/daylySchedule")
    public HttpEntity<?> getAgentDaylySchedule(@CurrentUser User user, @RequestParam(name = "date", defaultValue = "") Date date) {
        try {
            return ResponseEntity.ok(agentScheduleService.getAgentDailySchedule(user.getId(), date));
        } catch (Exception e) {
            return ResponseEntity.ok(new ApiResponse("You cannot do it", false));
        }
    }

    //    @PreAuthorize("hasAuthority('DELETE_AGENT_SCHEDULE')")
    @DeleteMapping("/{agentScheduleId}")
    public HttpEntity<?> deleteAgentScheduleById(@PathVariable String agentScheduleId) {
        try {
            ApiResponse response = agentScheduleService.deleteAgentScheduleById(UUID.fromString(agentScheduleId));
            return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
        } catch (Exception e) {
            return ResponseEntity.ok(new ApiResponse("You cannot do it", false));
        }
    }

    //    @PreAuthorize("hasAuthority('EDIT_AGENT_SCHEDULE')")
    @PatchMapping("/agentDayOff")
    public HttpEntity<?> addAgentOff(@RequestParam(name = "agentScheduleId") UUID agentScheduleId,
                                     @RequestParam(name = "dayOff") boolean dayOff) {
        ApiResponse apiResponse = agentScheduleService.addAgentDayOff(agentScheduleId, dayOff);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    //    @PreAuthorize("hasAuthority('EDIT_AGENT_SCHEDULE')")
    @PatchMapping("/agentHourOff")
    public HttpEntity<?> addAgentOff(@CurrentUser User user,
                                     @RequestParam(name = "from") Timestamp from,
                                     @RequestParam(name = "till") String till) {
        ApiResponse apiResponse = agentScheduleService.addAgentHourOff(user, from, till);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }
}
