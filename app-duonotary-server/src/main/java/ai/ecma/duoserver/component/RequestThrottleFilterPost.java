package ai.ecma.duoserver.component;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

//@Component
public class RequestThrottleFilterPost implements Filter {

    private int MAX_REQUESTS_PER_MINUTE = 20;//or whatever you want it to be

    private LoadingCache<String, Integer> requestCountsPerIpAddress;

    public RequestThrottleFilterPost() {
        super();
        requestCountsPerIpAddress = CacheBuilder.newBuilder().
                expireAfterWrite(86000, TimeUnit.MINUTES).build(new CacheLoader<String, Integer>() {
            public Integer load(String key) {
                return 0;
            }
        });
    }

    private boolean isMaximumRequestsPerSecondExceeded(List<String> clientIpAddress) {
        int requests = 0;
        try {
            requests = requestCountsPerIpAddress.get(clientIpAddress.get(1));
            if (requests > MAX_REQUESTS_PER_MINUTE) {
                requestCountsPerIpAddress.put(clientIpAddress.get(1), requests);
                return true;
            }
        } catch (ExecutionException e) {
            requests = 0;
        }
        switch (clientIpAddress.get(0)) {
            case "GET":
                break;
            case "PUT":
                requests++;
                break;
            case "POST":
                requests++;
                break;
            case "DELETE":
                requests++;
                break;
            case "PATCH":
                requests++;
                break;
        }
        requestCountsPerIpAddress.put(clientIpAddress.get(1), requests);
        return false;
    }

    public List<String> getClientIP(HttpServletRequest request) {
        List<String> xfHeader = new ArrayList<>(Collections.singletonList(request.getHeader("X-Forwarded-For")));
        if (xfHeader.get(0) == null) {
            String method = request.getMethod();
            switch (method) {
                case "GET":
                    xfHeader.add(0, "GET");
                    break;
                case "PUT":
                    xfHeader.add(0, "PUT");
                    break;
                case "POST":
                    xfHeader.add(0, "POST");
                    break;
                case "DELETE":
                    xfHeader.add(0, "DELETE");
                    break;
                case "PATCH":
                    xfHeader.add(0, "PATCH");
                    break;
            }
            xfHeader.add(1, request.getRemoteAddr());
            return xfHeader;
        }
        String[] split = xfHeader.get(0).split(",");
        xfHeader.add(1, split[0]);
        return xfHeader;// voor als ie achter een proxy zit
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        if (isMaximumRequestsPerSecondExceeded(getClientIP((HttpServletRequest) servletRequest))) {
            httpServletResponse.setStatus(HttpStatus.TOO_MANY_REQUESTS.value());
            httpServletResponse.getWriter().write("Too many requests");
            return;
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    @Override
    public void destroy() {
    }
}
