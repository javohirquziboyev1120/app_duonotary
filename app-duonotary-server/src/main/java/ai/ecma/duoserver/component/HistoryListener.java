package ai.ecma.duoserver.component;

import ai.ecma.duoserver.entity.History;
import ai.ecma.duoserver.entity.enums.OperationEnum;
import ai.ecma.duoserver.entity.template.AbsEntity;
import ai.ecma.duoserver.repository.HistoryRepository;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

@Component
public class HistoryListener {

    // BU COMPONENT HAR BIR TABLELARDAGI O'ZGARISHLARNI ALOHIDA BOSHQA BIR TABLEGA YOZIB BORISH UCHUN YOZILDI!!!

    final
    HistoryRepository historyRepository;

    public HistoryListener(@Lazy HistoryRepository historyRepository) {
        this.historyRepository = historyRepository;
    }

    //BU METHOD TABLEGA INSERT BULGANDA QAYD QILIB BORADI
    @PrePersist
    public void insert(Object object) { saveLogDuo(object, OperationEnum.INSERT);
    }

    //BU METHOD TABLEGA UPDATE BULGANDA QAYD QILIB BORADI
    @PreUpdate
    public void update(Object object) {
        saveLogDuo(object, OperationEnum.UPDATE);
    }

    //BU METHOD TABLEGA DELETE BULGANDA QAYD QILIB BORADI
    @PreRemove
    public void remove(Object object) {
        saveLogDuo(object, OperationEnum.DELETE);
    }

    public void saveLogDuo(Object object, OperationEnum operationEnum) {
        String table = object.toString();
        AbsEntity absEntity = (AbsEntity) object;
        historyRepository.save(new History(
                table.substring(0, table.indexOf("(")),
                absEntity.getId(),
                operationEnum,
                object
        ));
    }
}
