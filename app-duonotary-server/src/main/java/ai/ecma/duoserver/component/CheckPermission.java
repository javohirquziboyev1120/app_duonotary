package ai.ecma.duoserver.component;

import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.entity.enums.PermissionName;
import ai.ecma.duoserver.repository.PermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component("CheckPermission")
public class CheckPermission {
    @Autowired
    PermissionRepository permissionRepository;


    public boolean checkPermission(UserDetails principal, String permissionName) {
        User user = (User) principal;
        return user.getPermissions().stream().anyMatch(permission ->
                permission.getPermissionName().equals(PermissionName.valueOf(permissionName)));
    }
}
