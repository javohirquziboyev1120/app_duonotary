package ai.ecma.duoserver.component;

import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.security.CurrentUser;
import com.google.firebase.messaging.*;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class NotificationService {
    private static final SimpleDateFormat simpleDate = new SimpleDateFormat("dd.MM.yyyy HH:mm");


    /**
     * TADBIRGA QATNASHADIGANLAR UCHUN TADBIR HAQIDA ESLATISH UCHUN YUBORISH
     * REMIND
     *
     * @param
     * @throws FirebaseMessagingException
     */
    public void sendNotifyLoginUser(@CurrentUser User user) throws FirebaseMessagingException {
        Message message = Message.builder()
                .setNotification(Notification.builder()
//                        .setTitle("REMIND")
                        .setBody("Duo Token").build())
                .putData("click_action", "FLUTTER_NOTIFICATION_CLICK")
//                .putData("type", "REMIND")
//                .putData("fullName", "Sirojiddin")
                .setToken(user.getFirebaseToken())
                .build();
        FirebaseMessaging.getInstance().send(message);
    }

    /**
     * YANGI TADBIR QO'SHILGANDA XABAR YUBORISH
     * NEW
     *
     * @throws FirebaseMessagingException
     */
//    public void sendNotifyNewEvent() throws FirebaseMessagingException {
//        List<String> firebaseTokenList = new ArrayList<>();
//        MulticastMessage message = MulticastMessage.builder()
//                .setNotification(Notification.builder()
//                        .setTitle("NEW EVENT")
//                        .setBody("New event").build())
//                .putData("click_action", "FLUTTER_NOTIFICATION_CLICK")
//                .putData("type", "IXTIYORIY")
//                .putData("key", "value")
//                .putData("qalay", "yomon")
//                .addAllTokens(firebaseTokenList)
//                .build();
//        FirebaseMessaging.getInstance().sendMulticast(message);
//    }
//
//    public String getMessage(String status) {
//        String message = "Tizimda o'zgarish";
//        switch (status) {
//            case "NEW":
//                message = "Yangi buyurtma";
//                break;
//            case "DELETE":
//                message = "Buyurtma sizdan yechildi";
//                break;
//            case "READY":
//                message = "Buyurtmangiz tayyor";
//                break;
//            case "ADD":
//                message = "Sizga buyurtma taqildi";
//                break;
//            case "ADDED":
//                message = "Buyurtmalar ro'yxatida o'zgarish";
//                break;
//            case "MESSAGE":
//                message = "Sizga xabar keldi";
//                break;
//        }
//        return message;
//    }


}
