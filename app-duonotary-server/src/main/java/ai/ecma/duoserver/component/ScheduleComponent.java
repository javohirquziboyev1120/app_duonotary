package ai.ecma.duoserver.component;

import ai.ecma.duoserver.entity.TimeTable;
import ai.ecma.duoserver.repository.HistoryRepository;
import ai.ecma.duoserver.repository.TimeTableRepository;
import ai.ecma.duoserver.service.AgentService;
import ai.ecma.duoserver.service.AttachmentService;
import ai.ecma.duoserver.service.MailSenderService;
import freemarker.template.TemplateException;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@EnableScheduling
@Component
public class ScheduleComponent {

    final
    HistoryRepository historyRepository;
    final
    AgentService agentService;
    final
    TimeTableRepository timeTableRepository;
    final
    MailSenderService mailSenderService;
    final
    AttachmentService attachmentService;

    public ScheduleComponent(HistoryRepository historyRepository, AgentService agentService, TimeTableRepository timeTableRepository, MailSenderService mailSenderService,   AttachmentService attachmentService) {
        this.historyRepository = historyRepository;
        this.agentService = agentService;
        this.timeTableRepository = timeTableRepository;
        this.mailSenderService = mailSenderService;
        this.attachmentService = attachmentService;
    }

    @Scheduled(cron = "0 0 8 * * *")
    public void setExpired() {
        attachmentService.deletePdfFiles();
        agentService.setExpired();
    }

    @Scheduled(fixedDelay = 1000 * 60)
    public void sendEmail() {
        mailSenderService.sentErrorUsers();
    }
    //Bu metod time_table dan keraksiz row larni o`chirib turadi
    @Scheduled(cron = "*/60 * * * * *")
    public void cleanTimeTable() {
        timeTableRepository.cleanTimeTable(new Timestamp(System.currentTimeMillis()));
    }

    @Scheduled(cron = "*/60 * * * * *")
    public void meetingRemainder() throws MessagingException, IOException, TemplateException {
        List<TimeTable> allToRemainder = timeTableRepository.findAllToRemainderDay(new Timestamp(System.currentTimeMillis()));
        List<TimeTable> toSave = new ArrayList<>();
        for (TimeTable timeTable : allToRemainder) {
            timeTable.setRemainder(false);
            toSave.add(timeTable);
            mailSenderService.remainderEveryDay(timeTable);
        }
        timeTableRepository.saveAll(toSave);
        List<TimeTable> allToRemainder2 = timeTableRepository.findAllToRemainderHour(new Timestamp(System.currentTimeMillis()));
        List<TimeTable> toSave2 = new ArrayList<>();
        for (TimeTable timeTable : allToRemainder2) {
            timeTable.setRemainder(true);
            toSave2.add(timeTable);
            mailSenderService.remainderEveryDay(timeTable);
        }
        timeTableRepository.saveAll(toSave2);
    }
}
