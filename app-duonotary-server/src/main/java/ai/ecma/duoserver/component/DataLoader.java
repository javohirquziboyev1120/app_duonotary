package ai.ecma.duoserver.component;

import ai.ecma.duoserver.entity.*;
import ai.ecma.duoserver.entity.enums.*;
import ai.ecma.duoserver.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static ai.ecma.duoserver.entity.enums.TermsEnum.*;

@Component
public class DataLoader implements CommandLineRunner {
    final
    UserRepository userRepository;
    final
    RoleRepository roleRepository;
    final
    PasswordEncoder passwordEncoder;
    final PermissionRepository permissionRepository;
    @Autowired
    PermissionRoleRepository permissionRoleRepository;
    @Autowired
    DiscountPercentRepository discountPercentRepository;
    @Autowired
    TermsConditionRepository termsConditionRepository;
    @Autowired
    SubServiceRepository subServiceRepository;
    @Value("${spring.datasource.initialization-mode}")
    private String initMode;

    public DataLoader(UserRepository userRepository, RoleRepository roleRepository,
                      @Lazy PasswordEncoder passwordEncoder, PermissionRepository permissionRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.permissionRepository = permissionRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        if (initMode.equals("always")) {

            List<Role> roles = roleRepository.findAll();

            List<PermissionRole> permissionRoles = new ArrayList<>();

            for (PermissionName permissionName : PermissionName.values()) {

                Permission savedPermission = permissionRepository.save(
                        new Permission(permissionName));

                for (RoleName roleName : permissionName.roleNames) {
                    permissionRoles.add(new PermissionRole(
                            getRoleByRoleName(roles, roleName), savedPermission));
                }
            }
            permissionRoleRepository.saveAll(permissionRoles);
            userRepository.save(
                    new User(
                            "SuperAdmin",
                            "SuperAdminov",
                            "1234567",
                            "superadmin@gmail.com",
                            passwordEncoder.encode("root123"),
                            "admin123",
                            new HashSet<>(roleRepository.findAllByRoleName(RoleName.ROLE_SUPER_ADMIN)),
                            null,
                            true,
                            new HashSet<>(permissionRepository.findAllByRoleName(RoleName.ROLE_SUPER_ADMIN.name()))
                            , EmailStatus.SENT)
            );

            termsConditionRepository.saveAll(
                    new ArrayList<>(Arrays.asList(
                            new TermsCondition("Hello!! Enter text", NOTIFICATIONSETTING),
                            new TermsCondition("Hello!! Enter text", PRIVACYPOLISY),
                            new TermsCondition("Hello!! Enter text", TERMSOFUSE)
                    ))
            );




//            userRepository.save(
//                    new User(
//                            "Admin",
//                            "Adminov",
//                            "321",
//                            "admin@gmail.com",
//                            passwordEncoder.encode("321"),
//                            "admin123",
//                            new HashSet<>(roleRepository.findAllByRoleName(RoleName.ROLE_ADMIN)),
//                            null,
//                            true,
//                            new HashSet<>(permissionRepository.findAllByRoleName(RoleName.ROLE_ADMIN.name()))
//                    )
//            );
//            userRepository.save(
//                    new User(
//                            "Agent",
//                            "Agentov",
//                            "456",
//                            "agent@gmail.com",
//                            passwordEncoder.encode("456"),
//                            "admin123",
//                            new HashSet<>(roleRepository.findAllByRoleName(RoleName.ROLE_AGENT)),
//                            null,
//                            true,
//                            new HashSet<>(permissionRepository.findAllByRoleName(RoleName.ROLE_AGENT.name()))
//                    )
//            );
//        } else if (initMode.equals("never")) {
//            List<Permission> permissionList = permissionRepository.findAll();
//            List<PermissionName> permissionNames = Arrays.asList(PermissionName.values());
//            List<PermissionName> notSavedPermission = permissionNames.stream().filter(permissionName -> !isDbSaved(permissionName, permissionList)).collect(Collectors.toList());
//            List<Role> roles = roleRepository.findAll();
//
//            List<PermissionRole> permissionRoles = new ArrayList<>();
//
//            for (PermissionName permissionName : notSavedPermission) {
//
//                Permission savedPermission = permissionRepository.save(
//                        new Permission(permissionName));
//
//                for (RoleName roleName : permissionName.roleNames) {
//                    permissionRoles.add(new PermissionRole(
//                            getRoleByRoleName(roles, roleName), savedPermission));
//                }
//            }
//            permissionRoleRepository.saveAll(permissionRoles);
//        }

        }
//        userRepository.save(
//                new User(
//                        "baba ",
//                        "baba",
//                        "321",
//                        "baba@gmail.com",
//                        passwordEncoder.encode("321"),
//                        "admin123",
//                        new HashSet<>(roleRepository.findAllByRoleName(RoleName.ROLE_USER)),
//                        null,
//                        true,
//                        new HashSet<>(permissionRepository.findAllByRoleName(RoleName.ROLE_USER.name()))
//                )
//        );
    }

    private Role getRoleByRoleName(List<Role> roles, RoleName roleName) {
        for (Role role : roles) {
            if (role.getRoleName().equals(roleName))
                return role;
        }
        return null;
    }

    private boolean isDbSaved(PermissionName permissionName, List<Permission> permissions) {
        return permissions.stream().anyMatch(permission -> permission.getPermissionName().equals(permissionName));
    }

}
