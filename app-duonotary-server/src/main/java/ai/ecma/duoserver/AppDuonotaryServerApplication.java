package ai.ecma.duoserver;


import ai.ecma.duoserver.config.InitConfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppDuonotaryServerApplication {
    public static void main(String[] args) {
        if (InitConfig.isStart())
            SpringApplication.run(AppDuonotaryServerApplication.class, args);
        else System.out.println("Malumotlar o'chmadi");
    }
}
