package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.AdditionalService;
import org.springframework.data.rest.core.config.Projection;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.UUID;
@Projection(name = "customAdditionalService", types = AdditionalService.class)
public interface CustomAdditionalService {

    UUID getId();

    String getName();

    Boolean getActive();

    String getDescription();
}
