package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.LoyaltyDiscountTariff;
import ai.ecma.duoserver.entity.SharingDiscountTariff;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customSharingDiscountTariff", types = SharingDiscountTariff.class)
public interface CustomSharingDiscountTariff {
    UUID getId();

    Double getPercent();

    boolean isActive();


}
