package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.Category;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "category", types = Category.class)
public interface CustomCategory {
    Integer getId();

    String getName();
}
