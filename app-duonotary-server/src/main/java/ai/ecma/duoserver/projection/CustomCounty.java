package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.County;
import ai.ecma.duoserver.entity.State;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customCounty", types = County.class)
public interface CustomCounty extends CustomAbsName {

    State getState();

    @Value("#{target.state.id}")
    UUID getStateId();

}
