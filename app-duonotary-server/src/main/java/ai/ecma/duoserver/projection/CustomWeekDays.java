package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.WeekDay;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "customWeekDays", types = WeekDay.class)
public interface CustomWeekDays {
    Integer getId();

    String getDay();

    Integer getOrderNumber();

}
