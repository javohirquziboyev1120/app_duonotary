package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.Attachment;
import ai.ecma.duoserver.entity.Partner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "partner", types = Partner.class)
public interface CustomPartner extends CustomAbsName {
    Attachment getAttachment();

    @Value("#{target.attachment.id}")
    UUID getAttachmentId();
}
