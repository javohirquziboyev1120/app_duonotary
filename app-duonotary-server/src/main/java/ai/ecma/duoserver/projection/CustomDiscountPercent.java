package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.DiscountPercent;
import ai.ecma.duoserver.entity.TimeDuration;
import ai.ecma.duoserver.entity.ZipCode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customDiscountPercent", types = DiscountPercent.class)
public interface CustomDiscountPercent {
    UUID getId();

    Double getPercent();

    @Value("#{target.zipCode!=null?  target.zipCode.id:''}")
    UUID getZipCodeId();

    ZipCode getZipCode();

    boolean getActive();

    boolean getOnline();
}