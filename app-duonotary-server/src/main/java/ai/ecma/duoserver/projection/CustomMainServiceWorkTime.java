package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.MainService;
import ai.ecma.duoserver.entity.MainServiceWorkTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customMainServiceWorkTime", types = MainServiceWorkTime.class)
public interface CustomMainServiceWorkTime {

    MainService getMainService();

    @Value("#{target.mainService.id}")
    UUID getMainServiceId();

    String getFromTime();

    String getTillTime();

    Boolean getActive();

    Double getPercent();

    UUID getId();

}
