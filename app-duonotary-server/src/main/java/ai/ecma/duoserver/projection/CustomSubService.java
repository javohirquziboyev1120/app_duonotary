package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.SubService;
import ai.ecma.duoserver.entity.enums.ServiceEnum;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customSubService", types = SubService.class)
public interface CustomSubService {
    UUID getId();

    String getName();

    boolean isActive();

    String getDescription();

    boolean isDefaultInput();

    ServiceEnum getServiceEnum();


}
