package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.State;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "customState", types = State.class)
public interface CustomState extends CustomAbsName {
}
