package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.DocumentType;
import ai.ecma.duoserver.entity.SubService;
import ai.ecma.duoserver.entity.enums.ServiceEnum;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customDocumentType", types = DocumentType.class)
public interface CustomDocumentType {
    UUID getId();

    String getName();

    boolean isActive();

    String getDescription();

    boolean isFbi();



}
