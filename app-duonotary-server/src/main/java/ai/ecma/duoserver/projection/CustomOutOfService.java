package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.OutOfService;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customOutOfService", types = OutOfService.class)
public interface CustomOutOfService {

    UUID getId();

    String getZipCode();

    String getEmail();

    Boolean getSent();

}
