package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.Country;
import ai.ecma.duoserver.entity.MainService;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customMainService", types = MainService.class)
public interface CustomMainService extends CustomAbsName {

    String getFromTime();

    String getTillTime();

    Boolean getOnline();

    Integer getOrderNumber();

    UUID getId();

}
