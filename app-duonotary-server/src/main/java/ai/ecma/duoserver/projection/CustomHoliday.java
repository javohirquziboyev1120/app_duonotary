package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.County;
import ai.ecma.duoserver.entity.Holiday;
import ai.ecma.duoserver.entity.MainService;
import ai.ecma.duoserver.entity.State;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.Date;
import java.util.UUID;

@Projection(name = "customHoliday", types = Holiday.class)
public interface CustomHoliday extends CustomAbsName {

    Date getDate();

    MainService getMainService();

    @Value("#{target.mainService.id}")
    UUID getMainServiceId();

}
