package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.*;
import org.springframework.data.rest.core.config.Projection;

import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.util.UUID;

@Projection(name = "customInternational", types = International.class)
public interface CustomInternational {
    UUID getId();

     boolean getEmbassy();

     DocumentType getDocumentType();

     String getFirstName();
     String getPickUpAddress();

     String getLastName();

     String getPhoneNumber();

     String getEmail();

     Integer getNumberDocument();

     String getMessage();

     Country getCountry();

     Order getOrder();

}
