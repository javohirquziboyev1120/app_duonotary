package ai.ecma.duoserver.projection;

import java.util.UUID;

public interface CustomAbsName {

    UUID getId();

    String getName();

    Boolean getActive();

    String getDescription();
}
