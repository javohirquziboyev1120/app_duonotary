package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.TimeDuration;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "customTimeDuration", types = TimeDuration.class)
public interface CustomTimeDuration {
    Integer getId();

    Integer getDurationTime();
}