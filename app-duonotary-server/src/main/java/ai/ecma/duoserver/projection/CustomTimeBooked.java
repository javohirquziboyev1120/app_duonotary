package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.TimeBooked;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "customTimeBooked", types = TimeBooked.class)
public interface CustomTimeBooked {
    Integer getId();

    Integer getBookedDuration();
}