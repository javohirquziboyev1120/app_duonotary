package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.Country;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customCountry", types = Country.class)
public interface CustomCountry {
    UUID getId();

    String getName();

    boolean isActive();

    Boolean getEmbassy();

}
