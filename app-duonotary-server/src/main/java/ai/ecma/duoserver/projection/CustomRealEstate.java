package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.Order;
import ai.ecma.duoserver.entity.RealEstate;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customRealEstate", types = RealEstate.class)
public interface CustomRealEstate{
    UUID getId();

    String getRequester();//agentstva nomi

    String getRequesterPhone();//agentsva tel raqami

    String getRequesterEmail();//agentsva emaili

    String getRequesterAddress();//agentsva MANZILI

    String getClientName();

    String getClientPhone();

    String getClientAddress();

    String getMessage();

    Order getOrder();

}
