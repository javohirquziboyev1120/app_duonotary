package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.PayType;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customPayType", types = PayType.class)
public interface CustomPayType {
    UUID getId();

    String getName();

    String getDescription();

    boolean isActive();

    boolean isOnline();
}
