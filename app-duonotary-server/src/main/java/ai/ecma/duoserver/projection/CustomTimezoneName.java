package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.TimezoneName;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "customTimezoneName", types = TimezoneName.class)
public interface CustomTimezoneName{

    Integer getId();

    String getName();
}
