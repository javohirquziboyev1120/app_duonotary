package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.LoyaltyDiscountTariff;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customLoyaltyDiscountTariff", types = LoyaltyDiscountTariff.class)
public interface CustomLoyaltyDiscountTariff {
    UUID getId();

    Integer getMounth();

    Double getPercent();

    boolean isActive();

}
