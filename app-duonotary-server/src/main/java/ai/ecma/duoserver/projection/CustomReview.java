package ai.ecma.duoserver.projection;

import ai.ecma.duoserver.entity.Review;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "review", types = Review.class)
public interface CustomReview {
    UUID getId();

    String getFullName();

    String getCompanyName();

//    String getTitle();

    String getDescription();

    @Value("#{target.photo.id}")
    UUID getPhotoId();
}
