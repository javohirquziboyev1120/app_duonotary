package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.*;
import ai.ecma.duoserver.entity.enums.EmailStatus;
import ai.ecma.duoserver.entity.enums.OperationEnum;
import ai.ecma.duoserver.entity.enums.StatusEnum;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.CertificateDto;
import ai.ecma.duoserver.payload.UserDto;
import ai.ecma.duoserver.repository.AttachmentContentRepository;
import ai.ecma.duoserver.repository.OrderRepository;
import ai.ecma.duoserver.repository.TimeTableRepository;
import ai.ecma.duoserver.repository.UserRepository;
import ai.ecma.duoserver.utils.AppConstants;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class MailSenderService {
    @Autowired
    private JavaMailSender sender;

    @Autowired
    private Configuration config;

    @Autowired
    TimeTableRepository timeTableRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    /**
     * user accountini tasdiqlash uchun emailiga xabar yuboriladi. "changing" true bo'lsa email edit qilinayotgan bo'ladi
     *
     * @param code
     * @param userDto
     * @param changing
     * @throws MessagingException
     * @throws IOException
     * @throws TemplateException
     */
    public void sendEmailForVerification(UUID code, UserDto userDto, Boolean changing) throws MessagingException, IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
        model.put("email", userDto.getEmail());
        model.put("changing", changing.toString());
        model.put("code", code);
        model.put("localhost", AppConstants.CLIENT_URl);
        model.put("fullName", userDto.getFirstName() + " " + userDto.getLastName());
        sendEmailWithHtml(userDto.getEmail(), model, "email-template.ftl", "Welcome to DuoNotary");
    }

    public void sendAddedNewZipCode(String email, OutOfService outOfService) throws MessagingException, IOException, TemplateException {
        Map<String, Object> model = new HashMap<>();
//        model.put("email", email);
//        model.put("zipCode", outOfService.getZipCode());
        model.put("fullName", "Mr/Mrs");
        sendEmailWithHtml(email, model, "addNewZipCode.ftl", "DuoNotary arrived in your area!");
    }

    public void sendEmailWithHtml(String email, Map<String, Object> model, String htmlFile, String subject) throws MessagingException, IOException, TemplateException {
        MimeMessage message = sender.createMimeMessage();
        // set mediaType
        MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());
        // add attachment
//			helper.addAttachment("logo.png", new ClassPathResource("logo.png"));
        Template t = config.getTemplate(htmlFile);
        String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
        helper.setTo(email);
        helper.setText(html, true);
//        helper.setSubject("\uD83D\uDCDB noreply");
        helper.setSubject(subject);
        sender.send(message);
    }

    public void sendAgentOnStatusChanged(CertificateDto certificateDto, String description) throws MessagingException, IOException, TemplateException {
        Map<String, Object> toHtml = new HashMap<>();
        toHtml.put("userFullName", certificateDto.getUserDto().getFirstName() + " " + certificateDto.getUserDto().getLastName());

        if (certificateDto.getStatusEnum() == StatusEnum.RECEIVED) {
            toHtml.put("description", description);
            toHtml.put("text", "<span\n" +
                    "                                      style=\"font-size: 16px; color: #000000\"\n" +
                    "                                    >\n" +
                    "                                      <b> Congratulations!</b> We have reviewed\n" +
                    "                                      your application and submitted documents,\n" +
                    "                                      and excited to inform you that your\n" +
                    "                                      account is activated!\n" +
                    "                                      <br />\n" +
                    "                                      <br />\n" +
                    "                                      Please go ahead and sign in to your\n" +
                    "                                      account at this\n" +
                    "                                      <i><a href=\"" + AppConstants.CLIENT_URl + "\">link</a> </i>and\n" +
                    "                                      make sure to download our Duonotary Agent\n" +
                    "                                      App from the application market.\n" +
                    "                                      <br />\n" +
                    "                                      <br />\n" +
                    "                                      If you have any questions or concerns\n" +
                    "                                      please do not hesitate to send us an email\n" +
                    "                                      at info@duonotary.com\n" +
                    "                                    </span>");
            sendEmailWithHtml(certificateDto.getUserDto().getEmail(), toHtml, "send-status.ftl", "Account Activated");
        }
        if (certificateDto.getStatusEnum() == StatusEnum.REJECTED) {
            toHtml.put("text", "We appreciate your interest in becoming a DuoNotary Agent. Unfortunately, we can not approve\n" +
                    "your account at this moment. Feel free to contact us via email to get more information about our\n" +
                    "decision.");
            toHtml.put("description", description);
            sendEmailWithHtml(certificateDto.getUserDto().getEmail(), toHtml, "send-status.ftl", "Application Rejected");
        }

    }

    public void sendAgentChangeActive(User user, boolean bool) throws MessagingException, IOException, TemplateException {
        Map<String, Object> toHtml = new HashMap<>();
        toHtml.put("userFullName", user.getFirstName() + " " + user.getLastName());
        toHtml.put("enabled", bool ? "activated" : "deactivated");
        sendEmailWithHtml(user.getEmail(), toHtml, "agentActiveChange.ftl", bool ? "Account Activated" : "Account Deactivated");
    }

    public void sendClientForgotPassword(String fullname, String email, String code) throws MessagingException, IOException, TemplateException {
        Map<String, Object> toHtml = new HashMap<>();
        toHtml.put("fullName", fullname);
        toHtml.put("code", code);
        sendEmailWithHtml(email, toHtml, "forgotPassword.ftl", "Reset Password");
    }

    public void sendFeedBackForAdmin(FeedBack feedBack, OperationEnum operationEnum) throws MessagingException, IOException, TemplateException {
        String feedBacker = feedBack.isAgent() ? (feedBack.getOrder().getAgent().getFirstName() + " " + feedBack.getOrder().getAgent().getLastName()) : (feedBack.getOrder().getClient().getFirstName() + " " + feedBack.getOrder().getClient().getLastName());

        Map<String, Object> toHtml = new HashMap<>();
        toHtml.put("fullName", "Chief");
        toHtml.put("feedBacker", feedBacker);
        toHtml.put("checkNumber", feedBack.getOrder().getSerialNumber());
        toHtml.put("rate", feedBack.getRate());
        toHtml.put("description", feedBack.getDescription());
//        toHtml.put("type", operationEnum.name());
        sendEmailWithHtml(feedBack.isAgent() ? feedBack.getOrder().getAgent().getEmail() : feedBack.getOrder().getClient().getEmail(), toHtml, "feedback.ftl", "Feedback Received from " + feedBacker);
    }

    public void sendAnswerFeedBack(String fullName, String answer, String email) throws MessagingException, IOException, TemplateException {
        Map<String, Object> toHtml = new HashMap<>();
        toHtml.put("fullName", fullName);
        toHtml.put("answer", answer);
        sendEmailWithHtml(email, toHtml, "feedBackAnswer.ftl", "Thank you for your Feedback!");
    }

    public void sendClientCustomDiscount(CustomDiscount customDiscount, String type) throws MessagingException, IOException, TemplateException {
        Map<String, Object> toHtml = new HashMap<>();
        toHtml.put("userFullName", customDiscount.getOrder().getClient().getFirstName() + " " + customDiscount.getOrder().getClient().getLastName());
        toHtml.put("orderController", AppConstants.CLIENT_URl + "/api/order/" + customDiscount.getOrder().getId());
        toHtml.put("percent", customDiscount.getPercent());
        toHtml.put("amount", customDiscount.getAmount());
        toHtml.put("description", customDiscount.getDescription());
        toHtml.put("type", type);
        sendEmailWithHtml(customDiscount.getOrder().getClient().getEmail(), toHtml, "customDiscount.ftl", "Discount");
    }

    public void sendOrderInfoToAgent(Order order, TimeTable timeTable, boolean agent) throws MessagingException, IOException, TemplateException {
        SimpleDateFormat date = new SimpleDateFormat("MM-dd-yyyy");
        SimpleDateFormat time = new SimpleDateFormat("hh.mm aa");
        String dateFormat = date.format(timeTable.getFromTime());
        String timeFormat = time.format(timeTable.getFromTime());
        String service = order.getServicePrice().getService().getMainService().getName() + " " + order.getServicePrice().getService().getSubService().getName();
        String documentName = "";
        for (Attachment document : order.getDocuments()) {
            documentName = document.getName();
            break;
        }
        Map<String, Object> toHtml = new HashMap<>();
        if (agent) {
            toHtml.put("fullName", order.getAgent().getFirstName() + " " + order.getAgent().getLastName());
        } else {
            toHtml.put("fullName", order.getClient().getFirstName() + " " + order.getClient().getLastName());
        }
        toHtml.put("service", service);
        toHtml.put("documentName", documentName);
        toHtml.put("date", dateFormat);
        toHtml.put("time", timeFormat);
        toHtml.put("location", timeTable.isOnline() ? "online" : order.getAddress());
        if (agent)
            sendEmailWithHtml(order.getAgent().getEmail(), toHtml, "addOrderNotifyToAgent.ftl", "Order Confirmation");
        else
            sendEmailWithHtml(order.getClient().getEmail(), toHtml, "addOrderNotifyToAgent.ftl", "Order Confirmation");
    }

    public void editOrder(Order order, User user, boolean agent) throws MessagingException, IOException, TemplateException {
        Map<String, Object> toHtml = new HashMap<>();
        if (agent) {
            toHtml.put("fullName", order.getAgent().getFirstName() + " " + order.getAgent().getLastName());
        } else {
            toHtml.put("fullName", order.getClient().getFirstName() + " " + order.getClient().getLastName());
        }
        sendEmailWithHtml(order.getAgent().getEmail(), toHtml, "agentEditInfo.ftl", "Your order update");
    }


    //  Bu method orqali client online notarizatsiya qilganidan keyin pulini tolaganida uning emailiga DocVerifydan olingan va bizani bazada saqlayotgan filelarni jonatiadigan method
    public boolean sendDocVerifyFileByOrderId(UUID orderId) {
        try {
            Order order = orderRepository.findById(orderId).orElseThrow(() -> new ResourceNotFoundException("order", "id", orderId));
            List<Attachment> attachmentList = order.getFilesFromDocVerify();
            if (attachmentList.size() > 0) {
                MimeMessage mimeMessage = sender.createMimeMessage();
                MimeMessageHelper helper;
                try {
                    helper = new MimeMessageHelper(mimeMessage, true);
                    helper.setTo(order.getClient().getEmail());
                    helper.setSubject("From Duo Notary ");
                    helper.setText("Dear " + order.getClient().getFirstName() + " " + order.getClient().getLastName() + " we send you your ready documents");
                    for (int i = 0; i < attachmentList.size(); i++) {
                        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachmentId(attachmentList.get(i).getId());
                        ByteArrayDataSource file = new ByteArrayDataSource(attachmentContent.getContent(), "application/octet-stream");
                        helper.addAttachment((i + 1) + " file from Duo Notary ", file);
                    }
                    sender.send(mimeMessage);
                    return true;
                } catch (MessagingException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            return false;
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
        return false;
    }

    public void sendAgentEditYourInfo(User user) throws MessagingException, IOException, TemplateException {
        Map<String, Object> toHtml = new HashMap<>();
        toHtml.put("fullName", user.getFirstName() + " " + user.getLastName());
        sendEmailWithHtml(user.getEmail(), toHtml, "agentEditInfo.ftl", "Admin Update");
    }

    public void sendAgentEditZipCodeArea(User user, String activated, String zipcode) throws MessagingException, IOException, TemplateException {
        Map<String, Object> toHtml = new HashMap<>();
        toHtml.put("userFullName", user.getFirstName() + " " + user.getLastName());
        sendEmailWithHtml(user.getEmail(), toHtml, "agentEditZipCodeArea.ftl", "Admin Update");
    }

    public void sentErrorUsers() {
        for (User user : userRepository.findAllByEmailStatus(EmailStatus.ERROR)) {
            try {
                sendEmailForVerification(UUID.fromString(user.getEmailCode()), userService.getUser(user), false);
                user.setEmailStatus(EmailStatus.SENT);
                userRepository.save(user);
            } catch (MessagingException | IOException | TemplateException e) {
                System.out.println(EmailStatus.ERROR);
                user.setEmailStatus(EmailStatus.ERROR);
                userRepository.save(user);
            }

        }
    }

    public void remainderEveryDay(TimeTable timeTable) throws MessagingException, IOException, TemplateException {
        SimpleDateFormat date = new SimpleDateFormat("MM-dd-yyyy");
        SimpleDateFormat time = new SimpleDateFormat("hh.mm aa");
        String dateFormat = date.format(timeTable.getFromTime());
        String timeFormat = time.format(timeTable.getFromTime());
        String service = timeTable.getOrder().getServicePrice().getService().getMainService().getName() + " " + timeTable.getOrder().getServicePrice().getService().getSubService().getName();
        String documentName = "";
        for (Attachment document : timeTable.getOrder().getDocuments()) {
            documentName = document.getName();
            break;
        }

        Map<String, Object> toHtml = new HashMap<>();
        toHtml.put("fullName", timeTable.getOrder().getClient().getFirstName() + " " + timeTable.getOrder().getClient().getLastName());
        toHtml.put("service", service);
        toHtml.put("who", "Agent");
        toHtml.put("documentName", documentName);
        toHtml.put("date", dateFormat);
        toHtml.put("time", timeFormat);
        toHtml.put("location", timeTable.isOnline() ? "online" : timeTable.getOrder().getAddress());
        toHtml.put("agent", timeTable.getAgent().getFirstName() + " " + timeTable.getAgent().getFirstName());
        sendEmailWithHtml(timeTable.getOrder().getClient().getEmail(), toHtml, "meeting.ftl", " Appointment Reminder");


        Map<String, Object> toHtml2 = new HashMap<>();
        toHtml2.put("fullName", timeTable.getOrder().getAgent().getFirstName() + " " + timeTable.getOrder().getAgent().getLastName());
        toHtml2.put("service", service);
        toHtml2.put("who", "Client");
        toHtml2.put("documentName", documentName);
        toHtml2.put("date", dateFormat);
        toHtml2.put("time", timeFormat);
        toHtml2.put("location", timeTable.isOnline() ? "online" : timeTable.getOrder().getAddress());
        toHtml2.put("agent", timeTable.getOrder().getClient().getFirstName() + " " + timeTable.getOrder().getClient().getFirstName());
        sendEmailWithHtml(timeTable.getOrder().getAgent().getEmail(), toHtml2, "meeting.ftl", " Appointment Reminder");
    }
}
