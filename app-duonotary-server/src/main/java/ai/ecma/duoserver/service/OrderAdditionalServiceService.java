package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.AdditionalServicePrice;
import ai.ecma.duoserver.entity.OrderAdditionalService;
import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.AdditionalServicePriceDto;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.OrderAdditionalServiceDto;
import ai.ecma.duoserver.payload.ResPageable;
import ai.ecma.duoserver.repository.AdditionalServicePriceRepository;
import ai.ecma.duoserver.repository.OrderAdditionalServiceRepository;
import ai.ecma.duoserver.repository.OrderRepository;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class OrderAdditionalServiceService {
    final
    OrderAdditionalServiceRepository orderAddSerRep;

    final
    OrderRepository orderRepository;

    final
    AdditionalServicePriceRepository additionalServicePriceRepository;

    final
    OrderService orderService;

    final
    AdditionalServicePriceService additionalServicePriceService;

    final
    CheckRole checkRole;

    public OrderAdditionalServiceService(OrderAdditionalServiceRepository orderAddSerRep, OrderRepository orderRepository, AdditionalServicePriceRepository additionalServicePriceRepository, @Lazy OrderService orderService, AdditionalServicePriceService additionalServicePriceService, CheckRole checkRole) {
        this.orderAddSerRep = orderAddSerRep;
        this.orderRepository = orderRepository;
        this.additionalServicePriceRepository = additionalServicePriceRepository;
        this.orderService = orderService;
        this.additionalServicePriceService = additionalServicePriceService;
        this.checkRole = checkRole;
    }

    public ApiResponse addOrderAdditionService(OrderAdditionalServiceDto orderAddSerDto) {
        try {
            Optional<AdditionalServicePrice> optionalAdditionalServicePrice = additionalServicePriceRepository.findById(orderAddSerDto.getAdditionalServicePriceId());
            if (!optionalAdditionalServicePrice.isPresent())
                return new ApiResponse("Additional Service Price not fount", false);
            OrderAdditionalService toSaveOrdAddSer = new OrderAdditionalService();
            toSaveOrdAddSer.setAdditionalServicePrice(optionalAdditionalServicePrice.get());
            toSaveOrdAddSer.setCount(orderAddSerDto.getCount());
            toSaveOrdAddSer.setCurrentPrice(optionalAdditionalServicePrice.get().getPrice()*orderAddSerDto.getCount());
            toSaveOrdAddSer.setDescription(orderAddSerDto.getDescription());
            toSaveOrdAddSer.setOrder(orderAddSerDto.getOrder());
            if (orderAddSerDto.getId() != null) {
                toSaveOrdAddSer.setId(orderAddSerDto.getId());
            }
            orderAddSerRep.save(toSaveOrdAddSer);
            return new ApiResponse("Ok", true);
        } catch (Exception e) {
            return new ApiResponse();
        }
    }

    public ApiResponse getOrderAdditionalService(UUID id, User user) {
        OrderAdditionalService orderAdditionalService = orderAddSerRep.findById(id).orElseThrow(() -> new ResourceNotFoundException("OrderAdditionalService", "getAdditionalService", id));
        if (user.getId().equals(orderAdditionalService.getOrder().getClient().getId()) && user.getId().equals(orderAdditionalService.getOrder().getAgent().getId())) {
            return new ApiResponse("Ok", true, makeOrderAddSerDto(orderAdditionalService));
        }
        if (checkRole.isAdmin(user.getRoles()) || checkRole.isROLE_SUPER_ADMIN(user.getRoles())) {
            return new ApiResponse("Ok", true, makeOrderAddSerDto(orderAdditionalService));
        }
        return new ApiResponse("You can not get", false);
    }

    public ApiResponse deleteOrderAddSer(UUID id) {
        try {
            orderAddSerRep.deleteById(id);
            return new ApiResponse("Deleted", true);
        } catch (Exception e) {
            return new ApiResponse("System error", false);
        }
    }

    public ResPageable getOrderAdditionalServicePageable(int page, int size, UUID userId, User currentUser) {
        if (checkRole.isROLE_SUPER_ADMIN(currentUser.getRoles()) || checkRole.isAdmin(currentUser.getRoles()) || userId.equals(currentUser.getId())) {
            Page<OrderAdditionalService> allByOrderAgentId = orderAddSerRep.findAllByOrderClient_IdInOrOrderAgent_IdIn(userId, PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt"))));
            return new ResPageable(page, size, allByOrderAgentId.getTotalPages(), allByOrderAgentId.getTotalElements(), makeOrderAddSerDtoList(allByOrderAgentId.getContent()));
        }
        return null;
    }

    public OrderAdditionalServiceDto makeOrderAddSerDto(OrderAdditionalService oAs) {
        return new OrderAdditionalServiceDto(
                oAs.getId(),
                null,
                additionalServicePriceService.getAdditionalServicePrice(oAs.getAdditionalServicePrice()),
                oAs.getCount(),
                oAs.getDescription(),
                oAs.getCurrentPrice()
        );
    }

    public List<OrderAdditionalServiceDto> makeOrderAddSerDtoList(List<OrderAdditionalService> oAs) {
        List<OrderAdditionalServiceDto> response = new ArrayList<>();
        oAs.forEach(additionalService -> response.add(makeOrderAddSerDto(additionalService)));
        return response;
    }

}
