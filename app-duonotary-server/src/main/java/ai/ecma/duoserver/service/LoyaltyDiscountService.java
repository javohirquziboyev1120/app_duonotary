package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.LdtOrder;
import ai.ecma.duoserver.entity.LoyaltyDiscount;
import ai.ecma.duoserver.entity.LoyaltyDiscountTariff;
import ai.ecma.duoserver.entity.Order;
import ai.ecma.duoserver.entity.enums.OrderStatus;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.*;

@Service
public class LoyaltyDiscountService {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    LoyaltyDiscountTariffRepository loyaltyDiscountTariffRepository;
    @Autowired
    LoyaltyDiscountRepository loyaltyDiscountRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    LdtOrderRepository ldtOrderRepository;

    /**
     * bu method har bir order completed bolgandan keyin loyaltyDiscount beradi
     *
     * @param order
     */
    public void addLoyaltyDiscount(Order order) {
        try {
            LoyaltyDiscountTariff tariff = loyaltyDiscountTariffRepository.selectFirst();
            Optional<LoyaltyDiscount> optionalLoyaltyDiscount = loyaltyDiscountRepository.findByUser(order.getClient());
            if (order.getAmount() >= tariff.getBorder()) {
                if (optionalLoyaltyDiscount.isPresent()) {
                    LoyaltyDiscount loyaltyDiscount = optionalLoyaltyDiscount.get();
                    if (order.getOrderStatus().equals("COMPLETED")) {
                        Double amount = (order.getAmount() / 100) * tariff.getPercent();
                        List<UUID> orders = loyaltyDiscount.getOrders();
                        orders.add(order.getId());
                        loyaltyDiscount.setOrders(orders);
                        loyaltyDiscount.setAmount(amount);
                        loyaltyDiscount.setPercent(tariff.getPercent());
                        loyaltyDiscount.setLeftover(amount);
                        loyaltyDiscountRepository.save(loyaltyDiscount);

                    }
                } else {
                    List<UUID> orders = new ArrayList<>();
                    orders.add(order.getId());
                    LoyaltyDiscount loyaltyDiscount = new LoyaltyDiscount(
                            order.getClient(),
                            orders,
                            tariff.getPercent(),
                            (order.getAmount() / 100) * tariff.getPercent(),
                            (order.getAmount() / 100) * tariff.getPercent()
                    );
                    loyaltyDiscountRepository.save(loyaltyDiscount);
                }
            }
        } catch (Exception e) {
        }
    }

    /**
     * bu metod loyaltyDiscountning oylik tarifi 1 oylik 3 oylik skidka berishi
     *
     * @param order
     */
    public void addLoyaltyDiscountTariffMonth(Order order) {
        try {
            LoyaltyDiscountTariff tariff = loyaltyDiscountTariffRepository.findByMonthNotNull().orElseThrow(() -> new ResourceNotFoundException("getLoyaltyDiscountTariff"));

            Timestamp dateTo = new Timestamp((new Date()).getTime());
            Timestamp dateFrom = new Timestamp((new Date()).getTime());
//            dateFrom.setMonth(dateFrom.getMonth()-tariff.getMonth;
            List<Order> atMonth = orderRepository.findAllByClientIdAndCreatedAtAndOrderStatus(order.getClient().getId(), dateFrom, dateTo, OrderStatus.COMPLETED);

            LoyaltyDiscount getLoyaltyDiscount = loyaltyDiscountRepository.findByUser(order.getClient()).orElseThrow(() -> new ResourceNotFoundException("getLoyaltyDiscount"));
            Double amount = 0.0;
            for (Order order1 : atMonth) {
                amount += order1.getAmount();
            }
            if (amount >= tariff.getBorder()) {
                amount = (amount / 100) * tariff.getPercent();
                LoyaltyDiscount loyaltyDiscount = new LoyaltyDiscount(
                        order.getClient(),
                        getLoyaltyDiscount.getOrders(),
                        tariff.getPercent(),
                        amount,
                        amount
                );
                loyaltyDiscountRepository.save(loyaltyDiscount);
            }
        } catch (Exception e) {
        }
    }

    /**
     * bu metod serverdan chaqirilgan payt loyalty discount bor yoqligini tekshiradi agar bosa update bo'lmasa add qiladi
     *
     * @param order
     */
    public void updateLoyaltyDiscount(Order order) {
        Optional<LoyaltyDiscount> byUser = loyaltyDiscountRepository.findByUser(order.getClient());
        if (byUser.isPresent()) {
            LoyaltyDiscountTariff tariff = loyaltyDiscountTariffRepository.selectFirst();
            Double discount = (order.getAmount()) / 100 * tariff.getPercent();
            LoyaltyDiscount loyaltyDiscount = new LoyaltyDiscount();
            loyaltyDiscount.setAmount(loyaltyDiscount.getAmount() - discount);
            loyaltyDiscount.setLeftover(loyaltyDiscount.getAmount() - discount);
            loyaltyDiscount.setPercent(loyaltyDiscount.getPercent());
            loyaltyDiscountRepository.save(loyaltyDiscount);
        } else {
            addLoyaltyDiscount(order);
        }
    }
    /*
     * order amountdiscountiga loyaltydiscountni ozlashtirish ucun ishlatiladi
     * */
    public ApiResponse spendLoyaltyDiscount(Order order) {
        Optional<LoyaltyDiscount> optionalUser = loyaltyDiscountRepository.findByUser(order.getClient());
        if (optionalUser.isPresent()) {
            LoyaltyDiscount loyaltyDiscount = optionalUser.get();
            Optional<Order> optionalOrder = orderRepository.findById(order.getId());
            if (optionalOrder.isPresent()) {
                Order savedOrder = optionalOrder.get();
                if (savedOrder.getOrderStatus().name().equals("DRAFT")) {
                    savedOrder.setAmountDiscount(loyaltyDiscount.getAmount());
                    loyaltyDiscount.setLeftover(loyaltyDiscount.getAmount() - loyaltyDiscount.getLeftover());
                    loyaltyDiscountRepository.save(loyaltyDiscount);
                    orderRepository.save(savedOrder);
                    return new ApiResponse("Success!", true);
                }
                return new ApiResponse("not Draft", false);
            }
            return new ApiResponse("order not found!", false);
        }
        return new ApiResponse("user not found!", false);
    }

    @Scheduled(cron="0 0 0 1 * ?")
    public void setLoyalty() {
        List<LoyaltyDiscountTariff> allTarif = loyaltyDiscountTariffRepository.findAllByActive(true);

        if (!allTarif.isEmpty()) {
            Timestamp now = new Timestamp((new Date()).getTime());

            for (LoyaltyDiscountTariff loyaltyDiscountTariff : allTarif) {

                Timestamp fromTime = getFromTime(loyaltyDiscountTariff.getMonth());
                List<String> allClients = orderRepository.findAllClientsUUID(now, fromTime);

                double amount = 0.0;
                List<UUID> ordersId = new ArrayList<>();

                for (String clientId : allClients) {
                    List<Object[]> objects = orderRepository.checkDiscount(now, fromTime, UUID.fromString(clientId), loyaltyDiscountTariff.getId());

                    for (Object[] object : objects) {
                        amount += Double.parseDouble(object[0].toString());
                        ordersId.add(UUID.fromString(object[1].toString()));
                    }

                    if (amount >= loyaltyDiscountTariff.getBorder()) {
                        makeLoyaltyDiscount(ordersId, UUID.fromString(clientId), loyaltyDiscountTariff.getPercent(), amount, loyaltyDiscountTariff.getId());
                    }
                }
            }
        }
    }

    public void makeLoyaltyDiscount(List<UUID> ordersId, UUID clientId, double percent, double amount, Integer loyaltyDiscountTariffId) {
        try {
            double amount1 = (amount / 100) * percent;
            DecimalFormat decimalFormat = new DecimalFormat("#.##");

            LoyaltyDiscount newLoyaltyDiscount = new LoyaltyDiscount();
            newLoyaltyDiscount.setUser(userRepository.findById(clientId).orElseThrow(() -> new ResourceNotFoundException("getUSer")));
            newLoyaltyDiscount.setOrders(ordersId);
            newLoyaltyDiscount.setPercent(percent);
            newLoyaltyDiscount.setAmount(Double.valueOf(decimalFormat.format(amount1)));
            newLoyaltyDiscount.setLeftover(Double.valueOf(decimalFormat.format(amount1)));

            LoyaltyDiscount savedLoyaltyDiscount = loyaltyDiscountRepository.save(newLoyaltyDiscount);

            addLdtOrder(savedLoyaltyDiscount, loyaltyDiscountTariffId, ordersId);

        } catch (Exception e) {

        }
    }

    public void addLdtOrder(LoyaltyDiscount savedLoyaltyDiscount, Integer loyaltyDiscountTariffId, List<UUID> ordersId) {
        if (!ordersId.isEmpty()) {
            LdtOrder ldtOrder = new LdtOrder();
            ldtOrder.setLoyaltyDiscount(savedLoyaltyDiscount);
            ldtOrder.setLoyaltyDiscountTariff(loyaltyDiscountTariffRepository.findById(loyaltyDiscountTariffId).orElseThrow(() -> new ResourceNotFoundException("getLoyaltyDiscountTariffId")));

            for (UUID uuid : ordersId) {
                ldtOrder.setOrder(orderRepository.findById(uuid).orElseThrow(() -> new ResourceNotFoundException("getOrder")));
                ldtOrderRepository.save(ldtOrder);
            }
        }

    }

    public Timestamp getFromTime(int monthNumber) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, (-monthNumber));
        return new Timestamp(calendar.getTime().getTime());
    }
}
