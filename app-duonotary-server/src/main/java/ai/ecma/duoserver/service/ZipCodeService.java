package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.*;
import ai.ecma.duoserver.entity.enums.RoleName;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.*;
import ai.ecma.duoserver.repository.*;
import ai.ecma.duoserver.utils.AppConstants;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ZipCodeService {
    final
    CountyRepository countyRepository;
    final
    ZipCodeRepository zipCodeRepository;
    final
    OutOfServiceRepository outOfServiceRepository;
    final
    MailSenderService mailSenderService;
    final
    UserRepository userRepository;
    final
    StateRepository stateRepository;
    final
    MainServiceRepository mainServiceRepository;
    final
    OrderRepository orderRepository;
    final
    UserZipCodeRepository userZipCodeRepository;
    final
    TimezoneNameRepository timezoneNameRepository;

    public ZipCodeService(CountyRepository countyRepository, ZipCodeRepository zipCodeRepository, OutOfServiceRepository outOfServiceRepository, MailSenderService mailSenderService, UserRepository userRepository, StateRepository stateRepository, MainServiceRepository mainServiceRepository, OrderRepository orderRepository, UserZipCodeRepository userZipCodeRepository, TimezoneNameRepository timezoneNameRepository) {
        this.countyRepository = countyRepository;
        this.zipCodeRepository = zipCodeRepository;
        this.outOfServiceRepository = outOfServiceRepository;
        this.mailSenderService = mailSenderService;
        this.userRepository = userRepository;
        this.stateRepository = stateRepository;
        this.mainServiceRepository = mainServiceRepository;
        this.orderRepository = orderRepository;
        this.userZipCodeRepository = userZipCodeRepository;
        this.timezoneNameRepository = timezoneNameRepository;
    }

    /**
     * ZipCode code bazada yoqligini tekshiradigan method
     *
     * @param code code
     * @return boolean
     */
    public boolean checkZipCodeCode(String code) {
        return zipCodeRepository.existsByCode(code);
    }

    /**
     * ZipcodeDto dan Zipcodegaga O'gartiruvchi method
     *
     * @param zipCode    zipcode
     * @param zipCodeDto dto
     * @return ZipCode
     */
    public ZipCode makeZipCode(ZipCode zipCode, ZipCodeDto zipCodeDto) {
        if (zipCodeDto.getId() != null) {
            zipCode.setActive(zipCode.isActive());
        } else {
            zipCode.setActive(true);
        }
        zipCode.setCity(zipCodeDto.getCity());
        zipCode.setCode(zipCodeDto.getCode());
        zipCode.setCounty(countyRepository.findById(zipCodeDto.getCountyId() == null
                ? zipCodeDto.getCountyDto().getId() : zipCodeDto.getCountyId()).orElseThrow(() ->
                new ResourceNotFoundException("getCounty", "countyId", zipCodeDto.getCountyDto().getId())));
        return zipCode;

    }

    /**
     * ZipCodelarni qoshish uchun method
     *
     * @param zipCodeDto zipcode malumotalari
     * @return apiResponse
     */
    public ApiResponse addZipCode(ZipCodeDto zipCodeDto) {
        try {
            if (checkZipCodeCode(zipCodeDto.getCode()))
                return new ApiResponse(AppConstants.ALREADY_EXIST, false);
            Optional<TimezoneName> optionalTimezoneName = timezoneNameRepository.findById(zipCodeDto.getTimezoneNameId());
            if (!optionalTimezoneName.isPresent())
                return new ApiResponse(AppConstants.MISSING_FIELD, false);
            sendEmailForOutOfService(zipCodeDto.getCode());
            ZipCode zipCode = new ZipCode();
            zipCode.setTimezone(optionalTimezoneName.get());
            zipCodeRepository.save(makeZipCode(zipCode, zipCodeDto));
            return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
        } catch (Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }
    }

    public ApiResponse saveOrEditZipCode(ZipCodeDto zipCodeDto) {
        try {

            ZipCode zipCode = new ZipCode();
            if (zipCodeDto.getId() != null) {
                zipCode = zipCodeRepository.findById(zipCodeDto.getId()).orElseThrow(() -> new ResourceNotFoundException("", "", ""));
            } else {
                if (checkZipCodeCode(zipCodeDto.getCode())) {
                    return new ApiResponse(AppConstants.ALREADY_EXIST, false);
                }
                sendEmailForOutOfService(zipCodeDto.getCode());
            }
            return getApiResponse(zipCodeDto, zipCode);
        } catch (Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }
    }

    private ApiResponse getApiResponse(ZipCodeDto zipCodeDto, ZipCode zipCode) {
        if (zipCodeDto.getTimezoneNameId() != null) {
            Optional<TimezoneName> optionalTimezoneName = timezoneNameRepository.findById(zipCodeDto.getTimezoneNameId());
            if (!optionalTimezoneName.isPresent())
                return new ApiResponse(AppConstants.MISSING_FIELD, false);
            zipCode.setTimezone(optionalTimezoneName.get());
        }
        zipCodeRepository.save(makeZipCode(zipCode, zipCodeDto));
        return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
    }

    /**
     * ZipCodelarni edit uchun method
     *
     * @param zipCodeDto zipcode malumotalari
     * @return apiResponse
     */
    public ApiResponse editZipcode(ZipCodeDto zipCodeDto) {
        try {
            ZipCode zipCode = zipCodeRepository.findById(zipCodeDto.getId()).orElseThrow(() -> new ResourceNotFoundException("", "", ""));
            if (!zipCodeDto.getCode().equals(zipCode.getCode())) {
                if (checkZipCodeCode(zipCodeDto.getCode()))
                    return new ApiResponse(AppConstants.ALREADY_EXIST, false);
            }
            return getApiResponse(zipCodeDto, zipCode);
        } catch (Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }

    }

    /**
     * ZipCodelarni activeni o'zgartirish uchun method
     *
     * @param id zipcode id
     * @return apiResponse
     */
    public ApiResponse changeActive(UUID id) {
        try {
            Optional<ZipCode> zipCodeOptional = zipCodeRepository.findById(id);
            if (zipCodeOptional.isPresent()) {
                zipCodeOptional.get().setActive(!zipCodeOptional.get().isActive());
                zipCodeRepository.save(zipCodeOptional.get());
                return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
            }
            return new ApiResponse(AppConstants.ID_NOT_FOUND, false);
        } catch (Exception e) {
            return new ApiResponse("Tizimda xatolik", false);
        }
    }

    /**
     * Zipcodedan ZipcodeDtoga o'tkazuvchi method
     *
     * @param zipCode zipcode
     * @return dto
     */
    public ZipCodeDto getZipCode(ZipCode zipCode) {
        if (zipCode != null)
            return new ZipCodeDto(
                    zipCode.getId(),
                    zipCode.getCode(),
                    getCounty(zipCode.getCounty()),
                    zipCode.getCity(),
                    zipCode.isActive(),
                    zipCode.getTimezone()
            );
        return new ZipCodeDto();
    }

    public CountyDto getCounty(County county) {
        return new CountyDto(county.getId(), county.getName(), getState(county.getState()));
    }

    public StateDto getState(State state) {
        return new StateDto(state.getId(), state.getName());
    }

    /**
     * page va size ga qarab zipCodelarni qaytaradigan method
     *
     * @param page page raqami
     * @param size nechata keraklgi
     * @return resPageable
     */
    public ResPageable getZipCodes(int page, int size) {
        Page<ZipCode> all = zipCodeRepository.findAll(PageRequest.of(page, size));
        return new ResPageable(page, size, all.getTotalPages(), all.getTotalElements(),
                all.getContent().stream().map(this::getZipCode).collect(Collectors.toList()));
    }

    public ResPageable getCounties(int page, int size) {
        Page<County> all = countyRepository.findAll(PageRequest.of(page, size));
        return new ResPageable(page, size, all.getTotalPages(), all.getTotalElements(),
                all.getContent().stream().map(this::getCounty).collect(Collectors.toList()));
    }

    public ResPageable getStates(int page, int size) {
        Page<State> all = stateRepository.findAll(PageRequest.of(page, size));
        return new ResPageable(page, size, all.getTotalPages(), all.getTotalElements(),
                all.getContent().stream().map(this::getState).collect(Collectors.toList()));
    }

    public List<State> allStates() {
        return stateRepository.findAll();
    }

    public List<StateDto> allStates2() {
        List<StateDto> stateDtoList = new ArrayList<>();
        List<State> all = stateRepository.findAll();
        for (State state : all) {
            StateDto stateDto = new StateDto();
            stateDto.setId(state.getId());
            stateDto.setName(state.getName());
            stateDto.setZipCodeDtoList(zipCodeRepository.findAllByCountyStateId(state.getId()).stream().map(this::getZipCode).collect(Collectors.toList()));
            stateDtoList.add(stateDto);
        }
        return stateDtoList;
    }


    public List<ZipCode> getZipCodeByDtoList(List<UUID> statesId, List<UUID> countiesId, List<UUID> zipCodesId) {
        if (!statesId.isEmpty())
            return zipCodeRepository.getZipCodeByStatesId(statesId);
        else if (!countiesId.isEmpty())
            return zipCodeRepository.getZipCodeByCountiesId(countiesId);
        else return zipCodeRepository.getZipCodeByZipCodesId(zipCodesId);
    }

    public List<ZipCode> getZipCodeByDtoListForServicePrice(List<UUID> statesId, List<UUID> countiesId, List<UUID> zipCodesId, UUID serviceId) {
        if (!statesId.isEmpty())
            return zipCodeRepository.getZipCodeByStatesIdForEditServicePrice(statesId, serviceId);
        else if (!countiesId.isEmpty())
            return zipCodeRepository.getZipCodeByCountiesIdForEditServicePrice(countiesId, serviceId);
        else return zipCodeRepository.getZipCodeByZipCodesIdForEditServicePrice(zipCodesId, serviceId);
    }

    /**
     * Api responseda true qaytsa shu zip code da ishlaydi
     *
     * @param zipCode
     * @return
     */
    public ApiResponse checkZipCode(String zipCode) {
        return new ApiResponse("Ok", zipCodeRepository.existsByCodeAndActiveTrue(zipCode));
    }

    /**
     * Service ishlamaydigan zipCodeni qo'shish
     *
     * @param outOfServiceDto dto
     * @return apiResponse
     */
    public ApiResponse addOutOfService(OutOfServiceDto outOfServiceDto) {
        try {
            if (checkZipCodeCode(outOfServiceDto.getZipCode())) {
                return new ApiResponse(AppConstants.ALREADY_EXIST, false);
            }
            if (outOfServiceRepository.existsByEmail(outOfServiceDto.getEmail())) {
                return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
            }
            outOfServiceRepository.save(new OutOfService(outOfServiceDto.getZipCode(), outOfServiceDto.getEmail(), false));
            return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
        } catch (Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }
    }

    public void sendEmailForOutOfService(String zipCode) throws MessagingException, IOException, TemplateException {
        Thread thread = new Thread(() -> {
            for (OutOfService outOfService : outOfServiceRepository.findAllByZipCodeAndSent(zipCode, false)) {
                try {
                    mailSenderService.sendAddedNewZipCode(outOfService.getEmail(), outOfService);
                } catch (MessagingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TemplateException e) {
                    e.printStackTrace();
                }
                outOfService.setSent(true);
                outOfServiceRepository.save(outOfService);
            }

        });
//        for (OutOfService outOfService : outOfServiceRepository.findAllByZipCodeAndSent(zipCode, false)) {
//            mailSenderService.sendAddedNewZipCode(outOfService.getEmail(), outOfService);
//            outOfService.setSent(true);
//            outOfServiceRepository.save(outOfService);
//        }
        thread.start();
    }

    public ApiResponse deleteZipCode(UUID id) {
        try {
            zipCodeRepository.deleteById(id);
            return new ApiResponse("Ok", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse deleteCounty(UUID id) {
        try {
            countyRepository.deleteById(id);
            return new ApiResponse("Ok", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse deleteState(UUID id) {
        try {
            stateRepository.deleteById(id);
            return new ApiResponse("Ok", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse saveOrEditState(StateDto stateDto) {
        try {
            State state = new State();
            if (stateDto.getId() != null) {
                state = stateRepository.findById(stateDto.getId()).orElseThrow(() -> new ResourceNotFoundException("state", "id", stateDto.getId()));
            } else {
                state.setActive(true);
            }
            state.setName(stateDto.getName());
            stateRepository.save(state);
            return new ApiResponse("Ok", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse saveOrEditCounty(CountyDto countyDto) {
        try {
            County county = new County();
            if (countyDto.getId() != null) {
                county = countyRepository.findById(countyDto.getId()).orElseThrow(() -> new ResourceNotFoundException("county", "id", countyDto.getId()));
            } else {
                county.setActive(true);
            }
            county.setName(countyDto.getName());
            if (countyDto.getStateDto().getId() != null) {
                county.setState(stateRepository.findById(countyDto.getStateDto().getId()).orElseThrow(() -> new ResourceNotFoundException("state", "id", countyDto.getStateDto().getId())));
            }

            countyRepository.save(county);
            return new ApiResponse("Ok", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }


    public List<CountyDto> getCountyBySelectedState(UUID id) {
        return countyRepository.findAllByStateId(id).stream().map(this::getCounty).collect(Collectors.toList());
    }

    public List<ZipCodeDto> getZipCodeBySelectedCounty(UUID id) {
        return zipCodeRepository.findAllByCountyId(id).stream().map(this::getZipCode).collect(Collectors.toList());
    }

    public ApiResponse changeActiveOfZipCode(UUID id) {
        try {
            ZipCode zipCode = zipCodeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("", "", id));
            if (!zipCode.isActive()) {
                sendEmailForOutOfService(zipCode.getCode());
            }
            zipCode.setActive(!zipCode.isActive());
            zipCodeRepository.save(zipCode);
            return new ApiResponse("Ok", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ZipCodeDto2 getCurrentZipCode2(UUID id) {
        try {
            ZipCodeDto2 zipCodeDto2 = new ZipCodeDto2();
            ZipCode zipCode = zipCodeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("", "", id));
            zipCodeDto2.setId(zipCode.getId());
            zipCodeDto2.setActive(zipCode.isActive());
            zipCodeDto2.setCode(zipCode.getCode());
            zipCodeDto2.setCity(zipCode.getCity());
            zipCodeDto2.setCountyDto(getCounty(zipCode.getCounty()));
            zipCodeDto2.setCountyId(zipCode.getCounty().getId());
            List<ZipCodeDtoWorkHour> zipCodeDtoWorkHourList = new ArrayList<>();
//            List<MainService> allMainService = mainServiceRepository.findAll();
//            for (MainService mainService : allMainService) {
//                ZipCodeDtoWorkHour zipCodeDtoWorkHour = new ZipCodeDtoWorkHour();
//                zipCodeDtoWorkHour.setName(mainService.getName());
//                zipCodeDtoWorkHour.setStartTime(mainService.getFromTime());
//                zipCodeDtoWorkHour.setEndTime(mainService.getTillTime());
//                zipCodeDtoWorkHourList.add(zipCodeDtoWorkHour);
//            }
            zipCodeDto2.setWorkHours(zipCodeDtoWorkHourList);
            List<ZipCodeAgentDto> zipCodeAgentDtoList = new ArrayList<>();
            List<User> agentListByZipCode = userRepository.findAllAgentByZipCode(zipCode.getId(), RoleName.ROLE_AGENT.name());
            for (User agent : agentListByZipCode) {
                ZipCodeAgentDto zipCodeAgentDto = new ZipCodeAgentDto();
                zipCodeAgentDto.setId(agent.getId());
                if (agent.getPhoto() != null) {
                    zipCodeAgentDto.setImgUrl("http://localhost:/api/attachment/" + agent.getPhoto().getId());
                }
                zipCodeAgentDto.setFullName(agent.getFirstName() + " " + agent.getLastName());
                zipCodeAgentDto.setOrdersCount(orderRepository.countAllByAgentIdAndActiveIsTrue(agent.getId()));
                zipCodeAgentDtoList.add(zipCodeAgentDto);
            }
            zipCodeDto2.setAgents(zipCodeAgentDtoList);
            return zipCodeDto2;
        } catch (Exception e) {
            return null;
        }
    }

    public ApiResponse deleteAgentByZipCode(UUID zcId, UUID agId) {
        try {
            User user = userRepository.findById(agId).orElseThrow(() -> new ResourceNotFoundException("", "", agId));
            ZipCode zipCode = zipCodeRepository.findById(zcId).orElseThrow(() -> new ResourceNotFoundException("", "", zcId));
            userZipCodeRepository.deleteByZipCodeIdAndUserId(zcId, agId);
            mailSenderService.sendAgentEditZipCodeArea(user, "deactivated", zipCode.getCode());
            return new ApiResponse("Ok", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public List<ZipCode> getAdminZipCode(UUID id) {

//        Map<String, List<String>> admin = new TreeMap<>();
//
//        for (ZipCodeDto3 zipCodeDto3 : adminZipCode) {
//            if (admin.containsKey(zipCodeDto3.getStateName())) {
//                List<String> zipCodes = admin.get(zipCodeDto3.getStateName());
//                zipCodes.add(zipCodeDto3.getCode());
//                admin.put(zipCodeDto3.getStateName(), zipCodes);
//            } else {
//                admin.put(zipCodeDto3.getStateName(), Collections.singletonList(zipCodeDto3.getCode()));
//            }
//        }
//        adminZipCode = new ArrayList<>();
//        for (String s : admin.keySet()) {
//            adminZipCode.add(
//                    new ZipCodeDto3(s, admin.get(s)));
//        }
        return zipCodeRepository.getAdminZipCode(id);

    }

    public List<ZipCodeAgentDto> getAgentsByZipCode(UUID zcId) {
        List<ZipCodeAgentDto> zipCodeAgentDtoList = new ArrayList<>();
        List<User> agentList = userRepository.getlAllAgentByCertificate(zcId);
        for (User agent : agentList) {
            ZipCodeAgentDto zipCodeAgentDto = new ZipCodeAgentDto();
            zipCodeAgentDto.setId(agent.getId());
            zipCodeAgentDto.setFullName(agent.getFirstName() + " " + agent.getLastName());
            zipCodeAgentDto.setEmail(agent.getEmail());
            zipCodeAgentDto.setPhoneNumber(agent.getPhoneNumber());
            if (agent.getPhoto() != null) {
                zipCodeAgentDto.setImgUrl("http://localhost:/api/attachment/" + agent.getPhoto().getId());
            }
            zipCodeAgentDtoList.add(zipCodeAgentDto);
        }
        return zipCodeAgentDtoList;
    }

    public ApiResponse addAgentToZipCode(UUID zcId, UUID agId) {
        try {
            User user = userRepository.findById(agId).orElseThrow(() -> new ResourceNotFoundException("", "", agId));
            ZipCode zipCode = zipCodeRepository.findById(zcId).orElseThrow(() -> new ResourceNotFoundException("", "", zcId));
            UserZipCode userZipCode = new UserZipCode();
            userZipCode.setActive(false);
            userZipCode.setEnable(true);
            userZipCode.setUser(user);
            userZipCode.setZipCode(zipCode);
            userZipCodeRepository.save(userZipCode);
            mailSenderService.sendAgentEditZipCodeArea(user, "activated", zipCode.getCode());
            return new ApiResponse("Ok", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }
}


