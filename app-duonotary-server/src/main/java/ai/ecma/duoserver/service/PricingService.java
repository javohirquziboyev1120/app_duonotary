package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.Pricing;
import ai.ecma.duoserver.entity.ServicePrice;
import ai.ecma.duoserver.entity.State;
import ai.ecma.duoserver.entity.ZipCode;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.PricingDto;
import ai.ecma.duoserver.payload.ResPageable;
import ai.ecma.duoserver.repository.*;
import ai.ecma.duoserver.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PricingService {
    @Autowired
    ServicePriceRepository servicePriceRepository;
    @Autowired
    PricingRepository pricingRepository;
    @Autowired
    ServicePriceService servicePriceService;
    @Autowired
    MainServiceRepository mainServiceRepository;
    @Autowired
    SubServiceRepository subServiceRepository;
    @Autowired
    ServiceRepository serviceRepository;
    @Autowired
    ZipCodeRepository zipCodeRepository;
    @Autowired
    ZipCodeService zipCodeService;
    @Autowired
    StateRepository stateRepository;
    @Autowired
    CountyRepository countyRepository;

    public List<ZipCode> getZipCodesForPricing(PricingDto dto) {
        List<ZipCode> selectedZipcodes = new ArrayList<>();
        if (dto.isAll()) {
            selectedZipcodes = zipCodeRepository.findAll();
        } else if (!dto.getStatesId().isEmpty()) {
            selectedZipcodes = zipCodeService.getZipCodeByDtoList(dto.getStatesId(), null, null);
        } else {
            return null;
        }
        return selectedZipcodes;
    }

    public ApiResponse addPricing(PricingDto dto) {
        try {
            Optional<ai.ecma.duoserver.entity.Service> serviceOptional = serviceRepository.findById(dto.getServiceId());
            ai.ecma.duoserver.entity.Service service = serviceOptional.get();
            if (!service.getMainService().isOnline()) {
                if (dto.getFromCount() != null) {
                    Integer count = pricingRepository.findByServiceIdAndFromCount(dto.getServiceId(), dto.getFromCount(), dto.getTillCount(), dto.getStatesId());
                    if (count > 0) {
                        return new ApiResponse("Such count pricing is pre-existing ", false);
                    }
                    List<ZipCode> zipCodesApi = getZipCodesForPricing(dto);
                    if (zipCodesApi.size() < 0)
                        return new ApiResponse(AppConstants.ID_NOT_FOUND, false, zipCodesApi);
                    List<Pricing> pricing = new ArrayList<>();
                    for (ZipCode zipCode : zipCodesApi) {
                        State state1 = stateRepository.findByZipCode(zipCode.getId());
                        for (ServicePrice servicePrice : servicePriceRepository.findAllByServiceAndZipCode(serviceRepository.findById(dto.getServiceId()).get(), zipCode)) {
                            pricing.add(makePricing(dto, new Pricing(), servicePrice, state1));
                        }
                    }
                    pricingRepository.saveAll(pricing);
                    return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
                } else if (dto.getEveryCount() != null) {
                    List<State> stateList = stateRepository.findAllById(dto.getStatesId());
                    for (State state : stateList) {
                        List<Pricing> everyCountBetween = pricingRepository.findByServiceIdAndStateIdAndEveryCountBetween(dto.getServiceId(), state.getId(), dto.getEveryCount());
                        List<Pricing> everyCount = pricingRepository.findByServiceIdAndStateIdAndEveryCount(dto.getServiceId(), state.getId(), dto.getEveryCount());
                        List<Pricing> everyCountIsNotNull = pricingRepository.getByServiceIdAndStateIdAndEveryCount(dto.getServiceId(), state.getId(), dto.getStatesId());
                        if (everyCountIsNotNull.size() > 0) {
                            if (everyCount.size() > 0 || everyCountBetween.size() > 0) {
                                return new ApiResponse("Such count pricing is pre-existing ", false);
                            }
                            return new ApiResponse("This every count already exist", false);
                        }
                    }
                    List<ZipCode> zipCodesApi = getZipCodesForPricing(dto);
                    if (zipCodesApi.size() < 0)
                        return new ApiResponse(AppConstants.ID_NOT_FOUND, false, zipCodesApi);
                    List<Pricing> pricing = new ArrayList<>();
                    for (ZipCode zipCode : zipCodesApi) {
                        State state1 = stateRepository.findByZipCode(zipCode.getId());
                        for (ServicePrice servicePrice : servicePriceRepository.findAllByServiceAndZipCode(serviceRepository.findById(dto.getServiceId()).get(), zipCode)) {
                            pricing.add(makePricing(dto, new Pricing(), servicePrice, state1));
                        }
                    }
                    pricingRepository.saveAll(pricing);
                    return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
                }
            }
            Optional<ServicePrice> byServiceId = servicePriceRepository.findByServiceId(dto.getServiceId());
            if (byServiceId.isPresent()) {
                ServicePrice servicePrice1 = byServiceId.get();
                if (dto.getFromCount() != null && dto.getFromCount() > 0 && dto.getTillCount() != null && dto.getTillCount() > 0) {
                    List<Pricing> pricingList = pricingRepository.getAllPricingStateIsNullAndSubServiceName2(dto.getFromCount(), dto.getTillCount(), servicePrice1.getService().getSubService().getName());
                    if (pricingList.size() > 0)
                        return new ApiResponse(AppConstants.ALREADY_EXIST, false);
                    pricingList = pricingRepository.getAllPricingStateIsNullAndSubServiceName(dto.getFromCount(), dto.getTillCount(), servicePrice1.getService().getSubService().getName());
                    if (pricingList != null) {
                        for (Pricing pricing : pricingList) {
                            if (
                                    (dto.getFromCount() >= pricing.getFromCount() && dto.getFromCount() <= pricing.getTillCount()) ||
                                            (dto.getTillCount() >= pricing.getFromCount() && dto.getTillCount() <= pricing.getTillCount()) ||
                                            (dto.getFromCount() <= pricing.getFromCount() && dto.getFromCount() >= pricing.getTillCount()) ||
                                            (dto.getFromCount() <= pricing.getTillCount() && dto.getTillCount() >= pricing.getTillCount()) ||
                                            dto.getFromCount() >= dto.getTillCount()
                            ) {
                                return new ApiResponse("This pricing already exist ", false);
                            }
                        }
                    }
                    Pricing pricing = makePricing(dto, new Pricing(), servicePrice1, null);
                    pricingRepository.save(pricing);
                    return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
                } else if (dto.getEveryCount() != null) {
                    List<Pricing> pricingList = pricingRepository.findAllByStateIsNullAndEveryCountIsNotNull();
                    if (pricingList.size() > 0)
                        return new ApiResponse(AppConstants.ALREADY_EXIST, false);
                    Pricing pricing = makePricing(dto, new Pricing(), servicePrice1, null);
                    pricingRepository.save(pricing);
                    return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
                }
                return new ApiResponse(AppConstants.ALREADY_EXIST, false);
            }
            return new ApiResponse(AppConstants.ID_NOT_FOUND, false);
        } catch (Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }
    }


    /**
     * PRICING NI EDIT QILISH
     *
     * @param
     * @param
     * @return
     */
    public ApiResponse editPricing(PricingDto pricingDto) {
        try {
            List<Pricing> stateAndSubServiceName = new ArrayList<>();
            if (pricingDto.getEveryCount() != null && pricingDto.getEveryCount() != 0) {
                stateAndSubServiceName = pricingRepository.getAllPricingStateAndSubServiceName2(pricingDto.getStateId(), pricingDto.getEveryCount(), pricingDto.getSubServiceName());
            } else {
                stateAndSubServiceName = pricingRepository.getAllPricingStateAndSubServiceName(pricingDto.getStateId(), pricingDto.getFromCount(), pricingDto.getTillCount(), pricingDto.getSubServiceName());

            }
            if (pricingDto.getNewEveryCount() == null) {
                List<Pricing> antiStateAndSubServiceName = new ArrayList<>();
                if (pricingDto.getEveryCount() == null || pricingDto.getEveryCount() == 0) {
                    antiStateAndSubServiceName = pricingRepository.getAntiAllPricingStateAndSubServiceName(pricingDto.getStateId(), pricingDto.getFromCount(), pricingDto.getTillCount(), pricingDto.getSubServiceName());
                } else {
                    antiStateAndSubServiceName = pricingRepository.getAllPricingStateAndSubServiceName(pricingDto.getStateId(), pricingDto.getNewFromCount(), pricingDto.getNewTillCount(), pricingDto.getSubServiceName());
                }
                for (Pricing pricing : antiStateAndSubServiceName) {
                    if (
                            (pricingDto.getNewFromCount() >= pricing.getFromCount() && pricingDto.getNewFromCount() <= pricing.getTillCount()) ||
                                    (pricingDto.getNewTillCount() >= pricing.getFromCount() && pricingDto.getNewTillCount() <= pricing.getTillCount()) ||
                                    (pricingDto.getNewFromCount() <= pricing.getFromCount() && pricingDto.getNewFromCount() >= pricing.getTillCount()) ||
                                    (pricingDto.getNewFromCount() <= pricing.getTillCount() && pricingDto.getNewTillCount() >= pricing.getTillCount()) ||
                                    pricingDto.getNewFromCount() >= pricingDto.getNewTillCount()
                    ) {
                        return new ApiResponse("This pricing already exist ", false);
                    }
                }

            } else {
                if (pricingDto.getEveryCount() != null && pricingDto.getEveryCount() != 0) {
                    List<Pricing> checkEveryCount = pricingRepository.getAntiAllPricingStateAndSubServiceNameBetween(pricingDto.getStateId(), pricingDto.getNewEveryCount(), pricingDto.getSubServiceName());
                    if (checkEveryCount.size() > 0) {
                        return new ApiResponse("This pricing already exist ", false);
                    } else {
                        List<Pricing> antiStateAndSubServiceName = pricingRepository.getAntiAllPricingStateAndSubServiceName2(pricingDto.getStateId(), pricingDto.getEveryCount(), pricingDto.getSubServiceName());
                        if (antiStateAndSubServiceName.size() > 0) {
                            return new ApiResponse("This pricing already exist ", false);
                        }
                    }
                }
            }
            for (Pricing pricing : stateAndSubServiceName) {
                pricing.setFromCount(pricingDto.getNewFromCount());
                pricing.setTillCount(pricingDto.getNewTillCount());
                pricing.setEveryCount(pricingDto.getNewEveryCount());
            }
            pricingRepository.saveAll(stateAndSubServiceName);
            return new ApiResponse("Successfully edit price", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse editPricingStateNull(PricingDto pricingDto) {
        try {
            List<Pricing> stateAndSubServiceName = new ArrayList<>();
            if (pricingDto.getEveryCount() != null && pricingDto.getEveryCount() != 0) {
                stateAndSubServiceName = pricingRepository.getAllPricingStateIsNullAndSubServiceName(pricingDto.getEveryCount(), pricingDto.getSubServiceName());

            } else {
                stateAndSubServiceName = pricingRepository.getAllPricingStateIsNullAndSubServiceName2(pricingDto.getFromCount(), pricingDto.getTillCount(), pricingDto.getSubServiceName());

            }
            if (pricingDto.getNewEveryCount() == null) {
                List<Pricing> antiStateAndSubServiceName = new ArrayList<>();
                if (pricingDto.getEveryCount() == null || pricingDto.getEveryCount() == 0) {
                    antiStateAndSubServiceName = pricingRepository.getAllPricingStateIsNullAndSubServiceName(pricingDto.getFromCount(), pricingDto.getTillCount(), pricingDto.getSubServiceName());
                } else {
                    antiStateAndSubServiceName = pricingRepository.getAllPricingStateIsNullAndSubServiceName(pricingDto.getNewFromCount(), pricingDto.getNewTillCount(), pricingDto.getSubServiceName());
                }
                for (Pricing pricing : antiStateAndSubServiceName) {
                    if (
                            (pricingDto.getNewFromCount() >= pricing.getFromCount() && pricingDto.getNewFromCount() <= pricing.getTillCount()) ||
                                    (pricingDto.getNewTillCount() >= pricing.getFromCount() && pricingDto.getNewTillCount() <= pricing.getTillCount()) ||
                                    (pricingDto.getNewFromCount() <= pricing.getFromCount() && pricingDto.getNewFromCount() >= pricing.getTillCount()) ||
                                    (pricingDto.getNewFromCount() <= pricing.getTillCount() && pricingDto.getNewTillCount() >= pricing.getTillCount()) ||
                                    pricingDto.getNewFromCount() >= pricingDto.getNewTillCount()
                    ) {
                        return new ApiResponse("This pricing already exist ", false);
                    }
                }

            } else {
                if (pricingDto.getEveryCount() != null && pricingDto.getEveryCount() != 0) {
                    List<Pricing> checkEveryCount = pricingRepository.getAntiAllPricingStateIsNullAndSubServiceNameBetween(pricingDto.getNewEveryCount(), pricingDto.getSubServiceName());
                    if (checkEveryCount.size() > 0) {
                        return new ApiResponse("This pricing already exist ", false);
                    } else {
                        List<Pricing> antiStateAndSubServiceName = pricingRepository.getAntiAllPricingStateIsNullAndSubServiceName2(pricingDto.getEveryCount(), pricingDto.getSubServiceName());
                        if (antiStateAndSubServiceName.size() > 0) {
                            return new ApiResponse("This pricing already exist ", false);
                        }
                    }
                }
            }
            for (Pricing pricing : stateAndSubServiceName) {
                pricing.setFromCount(pricingDto.getNewFromCount());
                pricing.setTillCount(pricingDto.getNewTillCount());
                pricing.setEveryCount(pricingDto.getNewEveryCount());
            }
            pricingRepository.saveAll(stateAndSubServiceName);
            return new ApiResponse("Successfully edit price", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
//        try {
//            Integer count1 = pricingRepository.getByStateIsNullAndFromCount(pricingDto.getNewFromCount());
//            if (count1 > 0) {
//                return new ApiResponse("Such count pricing is pre-existing ", false);
//            }
//            List<Pricing> count = pricingRepository.findAllByStateIsNullAndFromCountAndTillCount(pricingDto.getFromCount(), pricingDto.getTillCount());
//            if (count.size() <= 0) {
//                return new ApiResponse("Such count pricing is pre-existing ", false);
//            }
//            List<Pricing> pricingList = pricingRepository.findAllByFromCountAndTillCountAndStateIdNullAndPrice(pricingDto.getFromCount(), pricingDto.getTillCount(), pricingDto.getPrice(), pricingDto.getSubServiceName());
//            List<Pricing> pricing = new ArrayList<>();
//            for (Pricing pricings : pricingList) {
//                pricing.add(editMakePricing(pricingDto, pricings, pricings.getServicePrice(), pricings.getState()));
//            }
//            pricingRepository.saveAll(pricing);
//            return new ApiResponse("Successfully edit price", true);
//        } catch (Exception e) {
//            return new ApiResponse("Error", false);
//        }
    }

    public ApiResponse deletePricing(Integer fromCount, Integer tillCount, Integer everyCount, UUID stateId, double price, String subServiceName) {
        try {
            if (fromCount != null && fromCount != 0) {
                List<Pricing> pricingList = pricingRepository.findAllByFromCountAndTillCountAndStateIdAndPrice(fromCount, tillCount, stateId, price, subServiceName);
                pricingRepository.deleteAll(pricingList);
                return new ApiResponse("Successfully delete price", true);
            } else {
                List<Pricing> pricingList = pricingRepository.findAllByEveryCountAndStateIdAndPrice(everyCount, stateId, price, subServiceName);
                pricingRepository.deleteAll(pricingList);
                return new ApiResponse("Successfully delete price", true);
            }
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse getDeletePricingByStateISNULL(Integer fromCount, Integer tillCount, double price, Integer everCount) {

            if (fromCount != null && fromCount != 0) {
                List<Pricing> stateIsNull = pricingRepository.findAllByFromCountAndTillCountAndPriceAndStateIsNull(fromCount, tillCount, price);
                pricingRepository.deleteAll(stateIsNull);
                return new ApiResponse("Successfully delete price", true);
            } else {
                List<Pricing> pricingList = pricingRepository.findAllByEveryCountAndPriceAndStateIsNull(everCount, price);
//                for (Pricing pricing : pricingList) {
//                    pricingRepository.deleteById(pricing.getId());
//                }
                pricingRepository.deleteAll(pricingList);
                return new ApiResponse("Successfully delete price", true);
            }
    }

    public PricingDto getPricing(Pricing pricing) {
        return new PricingDto(
                pricing.getId(),
                servicePriceService.getServicePricePage(pricing.getServicePrice()),
                pricing.getPrice(),
                pricing.isActive(),
                pricing.getFromCount(),
                pricing.getTillCount(),
                pricing.getEveryCount()
        );
    }

    public ApiResponse getPricingPage(Integer page, Integer size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Pricing> pricingPage = pricingRepository.findAllBy(pageable);
            return new ApiResponse(new ResPageable(
                    page,
                    size,
                    pricingPage.getTotalPages(),
                    pricingPage.getTotalElements(),
                    pricingPage.getContent().stream().map(this::getPricing).collect(Collectors.toList())));
        } catch (IllegalArgumentException e) {
            return new ApiResponse("Error", false);
        }
    }

    public PricingDto getPricingDashboard(Object[] objects) {
        return new PricingDto(
                objects[3] != null ? Double.parseDouble(objects[3].toString()) : 0d,// minPrice
                objects[4] != null ? Double.parseDouble(objects[4].toString()) : 0d,//maxPrice
                Integer.parseInt(objects[0] != null ? objects[0].toString() : "0"),// minFromCount
                Integer.parseInt(objects[2] != null ? objects[2].toString() : "0"),// mixTillCount
                Integer.parseInt(objects[1] != null ? objects[1].toString() : "0"), //minEveryCount
                objects[5].toString(),//state name
                objects.length > 6 && objects[6] != null ? UUID.fromString(objects[6].toString()) : null//state id
        );
    }

//    public List<PricingDto> getPricingDashboard() {
////        List<PricingDto> pricingList2 = pricingRepository.getPricingForDashboard2();
//        List<PricingDto> pricingList = pricingRepository.getPricingForDashboard()
//                .stream()
//                .map(this::getPricingDashboard)
//                .collect(Collectors.toList()
//                );
//        Object[] byStateIsNull = pricingRepository.findByStateIsNULL();
//        pricingList.add(getPricingDashboard((Object[]) byStateIsNull[0]));
//        return pricingList;
//
////        Pageable pageable = PageRequest.of(page, size);
////        Page<Object[]> pricingForDashboard = pricingRepository.getPricingForDashboard(pageable);
////        new ResPageable(
////                page,
////                size,
////                pricingForDashboard.getTotalPages(),
////                pricingForDashboard.getTotalElements(),
////                pricingForDashboard.getContent().stream().map(this::getPricingDashboard).collect(Collectors.toList()));
////        Page<Object[]> byStateIsNull = pricingRepository.findByStateIsNULL(pageable);
////        ResPageable resPageable = new ResPageable(
////                page,
////                size,
////                byStateIsNull.getTotalPages(),
////                byStateIsNull.getTotalElements(),
////                getPricingDashboard((Object[]) byStateIsNull[0])
////
////        );
//
//    }


    public List<PricingDto> getPricingDashboard() {
        List<PricingDto> pricingList = pricingRepository.getPricingForDashboard()
                .stream()
                .map(this::getPricingDashboard)
                .collect(Collectors.toList()
                );
        Object[] byStateIsNull = pricingRepository.findByStateIsNULL();
        if(byStateIsNull.length>0) {
            pricingList.add(getPricingDashboard((Object[]) byStateIsNull[0]));
            return pricingList;
        }
        return pricingList;
    }
    public PricingDto getPricingByState(Object[] objects) {
        return new PricingDto(
                objects[1] != null ? Double.parseDouble(objects[1].toString()) : 0d,// price
                Integer.parseInt(objects[0] != null ? objects[0].toString() : "0"),// FromCount
                Integer.parseInt(objects[2] != null ? objects[2].toString() : "0"),// TillCount
                Integer.parseInt(objects[3] != null ? objects[3].toString() : "0"), //EveryCount
                objects[5].toString(),//subServiceName
                Boolean.parseBoolean(objects[4].toString())//pricingActive
        );
    }

    public List<PricingDto> getPricingByState(UUID stateId) {
        List<PricingDto> pricingList = pricingRepository.getPricingForState(stateId)
                .stream()
                .map(this::getPricingByState)
                .collect(Collectors.toList()
                );
        return pricingList;
    }

    public List<PricingDto> getPricingByStateISNULL() {
        List<PricingDto> pricingList = pricingRepository.getPricingForStateIsNull()
                .stream()
                .map(this::getPricingByState)
                .collect(Collectors.toList()
                );
        return pricingList;
    }

    /**
     * BU FUNCTION PRICE YASAB OLISH UCHUN
     *
     * @param pricingDto
     * @return
     */
    public Pricing makePricing(PricingDto pricingDto, Pricing pricing, ServicePrice servicePrice, State state) {
        pricing.setServicePrice(servicePrice);
        pricing.setPrice(pricingDto.getPrice());
        pricing.setActive(pricingDto.isActive());
        pricing.setFromCount((pricingDto.getFromCount() == null) ? null : pricingDto.getFromCount());
        pricing.setTillCount((pricingDto.getTillCount()) == null ? null : pricingDto.getTillCount());
        pricing.setEveryCount((pricingDto.getEveryCount()) == null ? null : pricingDto.getEveryCount());
        pricing.setState(state);
        return pricing;
    }
    public List<State> getStateByPricing(UUID serviceId) {
        return stateRepository.findAllByServiceId(serviceId);
    }

}