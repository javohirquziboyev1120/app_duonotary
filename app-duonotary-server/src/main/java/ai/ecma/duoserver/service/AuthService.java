package ai.ecma.duoserver.service;

import ai.ecma.duoserver.component.NotificationService;
import ai.ecma.duoserver.entity.Order;
import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.entity.UserZipCode;
import ai.ecma.duoserver.entity.ZipCode;
import ai.ecma.duoserver.entity.enums.EmailStatus;
import ai.ecma.duoserver.entity.enums.OrderStatus;
import ai.ecma.duoserver.entity.enums.RoleName;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.CertificateDto;
import ai.ecma.duoserver.payload.UserDto;
import ai.ecma.duoserver.repository.*;
import ai.ecma.duoserver.utils.AppConstants;
import ai.ecma.duoserver.utils.CommonUtils;
import freemarker.template.TemplateException;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.*;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    JavaMailSender javaMailSender;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    MailSenderService mailSender;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    AgentService agentService;
    @Autowired
    PermissionRepository permissionRepository;
    @Autowired
    UserZipCodeRepository userZipCodeRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    TwilioService twilioService;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    NotificationService notificationService;

    public UUID generateEmailCode() {
        return UUID.randomUUID();
    }

    public ApiResponse registerUser(UserDto userDto) {
        try {
            userDto.setEmail(userDto.getEmail().toLowerCase());
            ApiResponse checkPassword = checkPassword(userDto);
            if (!checkPassword.isSuccess())
                return checkPassword;
            ApiResponse checkEmailAndPhoneNumber = checkEmailAndPhoneNumber(userDto);
            if (!checkEmailAndPhoneNumber.isSuccess())
                return checkEmailAndPhoneNumber;
            UUID emailCode = generateEmailCode();
            User user = makeUser(userDto, false, emailCode, 2);
            user.setEmailStatus(EmailStatus.ERROR);
            user.setPermissions(new HashSet<>(permissionRepository.findAllByRoleName(RoleName.ROLE_USER.name())));
            if (userDto.getFirebaseToken() != null && !userDto.getFirebaseToken().isEmpty())
                user.setFirebaseToken(userDto.getFirebaseToken());
            User save = userRepository.save(user);
            new Thread(() -> {
                try {
                    mailSender.sendEmailForVerification(emailCode, userDto, false);
                    save.setEmailStatus(EmailStatus.SENT);
                    userRepository.save(save);
                } catch (Exception e) {
                    save.setEmailStatus(EmailStatus.ERROR);
                    userRepository.save(save);
                }
            }).start();
            return new ApiResponse("Congratulation you are successfully registrated. please check your email address.", true);
        } catch (Exception e) {
            return new ApiResponse("Failed to save user", false);
        }
    }

    /**
     * BU METHOD ROLE_AGENT RO'YXATDAN O'TADI
     *
     * @param userDto
     * @return
     */
    public ApiResponse registerAgent(UserDto userDto) throws MessagingException, IOException, TemplateException {
        userDto.setEmail(userDto.getEmail().toLowerCase());
        ApiResponse checkPassword = checkPassword(userDto);
        if (!checkPassword.isSuccess())
            return checkPassword;
        ApiResponse checkEmailAndPhoneNumber = checkEmailAndPhoneNumber(userDto);//userning passwordlarini mosligini, email va phoneNumberning o'xshashi yo'qligini
        if (!checkEmailAndPhoneNumber.isSuccess())
            return checkEmailAndPhoneNumber;
        ApiResponse notValidPassportAndCertificate = notValidPassportAndCertificate(userDto);//ID KARTA VA CERTIFICATE LARNI XATOLIGINI TEKSHIRIB BERADI
        if (notValidPassportAndCertificate.isSuccess())
            return notValidPassportAndCertificate;
        UUID emailCode = UUID.randomUUID();
        User user = makeUser(userDto, null, emailCode, 1);
        user.setActive(false);
        user.setOnlineAgent(false);
        user.setPermissions(new HashSet<>(permissionRepository.findAllByRoleName(RoleName.ROLE_AGENT.toString())));
        User savedUser = userRepository.save(user);
        agentService.addPassport(userDto.getPassportDtoList().get(0), savedUser);
        agentService.addCertificates(userDto.getCertificateDtoList(), savedUser);
//        userDto.getCertificateDtoList().forEach(certificateDto -> {
//            List<ZipCode> zipCodes = agentService.getZipCodes(certificateDto.getStateDto().getId());
//            zipCodes.forEach(zipCode -> userZipCodeRepository.save(new UserZipCode(zipCode, savedUser, false)));
//        });//agentga userZipCode tableda zip codlar biriktildi.

        new Thread(() -> {
            try {
                mailSender.sendEmailForVerification(emailCode, userDto, false);
                savedUser.setEmailStatus(EmailStatus.SENT);
                userRepository.save(savedUser);
            } catch (Exception e) {
                savedUser.setEmailStatus(EmailStatus.ERROR);
                userRepository.save(savedUser);
            }
        }).start();
        return new ApiResponse("Successfully registered. Verify your email. After wait activate admin", true);
    }


    /**
     * user emailni tasdiqlasa enabled ni true qilamiz. User sistemaga kira olishi uchun. Yoki o'chirsa o'chirib yuboramiz.
     *
     * @param check
     * @param emailCode
     * @param email
     * @return
     */
    public ApiResponse verifyEmail(Boolean check, String emailCode, String email, boolean changing) {
        try {
            Optional<User> optionalUser = changing ?
                    userRepository.findByEmailCodeAndChangingEmail(emailCode, email) :
                    userRepository.findByEmailCodeAndEmail(emailCode, email);
            if (optionalUser.isPresent()) {
                User user = optionalUser.get();
                if (check) {
                    user.setEnabled(true);
                    user.setEmailCode(null);
                    if (changing) {
                        user.setEmail(user.getChangingEmail());
                        user.setChangingEmail(null);
                    }
                    userRepository.save(user);
                    List<Order> clientOrders = orderRepository.findAllByClient_IdAndOrderStatusAndActive(user.getId(), OrderStatus.DRAFT, true);
                    if (!clientOrders.isEmpty()) {
                        for (Order clientOrder : clientOrders) {
                            clientOrder.setOrderStatus(OrderStatus.NEW);
                            orderRepository.save(clientOrder);
                        }
                    }
                    if (user.getFirebaseToken() != null && !user.getFirebaseToken().isEmpty())
                        notificationService.sendNotifyLoginUser(user);
                    return new ApiResponse("User enabled!", true);
                } else {
                    if (!changing)
                        userRepository.delete(user);
                    else {
                        user.setChangingEmail(null);
                        user.setEmailCode(null);
                    }
                }
                return new ApiResponse("User deleted!", true);
            }
            return new ApiResponse("User not found!", false);
        } catch (Exception i) {
            return new ApiResponse("Email code exception", false);
        }


    }


    /**
     * userni tayyor hola yasab berish uchun
     *
     * @param userDto
     * @param admin
     * @param emailCode
     * @return
     */
    public User makeUser(UserDto userDto, Boolean admin, UUID emailCode, int type) {
        return new User(
                userDto.getFirstName(),
                userDto.getLastName(),
                userDto.getPhoneNumber(),
                userDto.getEmail(),
                passwordEncoder.encode(userDto.getPassword()),
                String.valueOf(emailCode),
                roleRepository.findAllByRoleName(admin == null ? RoleName.ROLE_AGENT :
                        admin ? RoleName.ROLE_ADMIN
                                : RoleName.ROLE_USER
                ),
                userDto.getSharingUserId() == null ? null : userRepository.findById(userDto.getSharingUserId()).orElse(null),
                admin != null && admin,
                userDto.getPhotoId() == null ? null : attachmentRepository.findById(userDto.getPhotoId()).orElse(null)
        );
    }

    /**
     * id card va certificate larni ishonchli emasligini tekshirish uchun
     *
     * @param userDto
     * @return
     */
    public ApiResponse notValidPassportAndCertificate(UserDto userDto) {
        ApiResponse passportExpired = CommonUtils.agentPassportNotValidDate(userDto.getPassportDtoList().get(0));
        if (passportExpired.isSuccess())
            return passportExpired;
        ApiResponse passportNotValidDate = CommonUtils.agentPassportNotValidDate(userDto.getPassportDtoList().get(0));
        if (passportNotValidDate.isSuccess())
            return passportNotValidDate;
        if (userDto.getCertificateDtoList().size() > 0) {
            for (CertificateDto certificateDto : userDto.getCertificateDtoList()) {
                ApiResponse certificateExpired = CommonUtils.agentCertificateExpired(certificateDto);
                if (certificateExpired.isSuccess())
                    return certificateExpired;
                ApiResponse certificateNotValidDate = CommonUtils.agentCertificateExpired(certificateDto);
                if (certificateNotValidDate.isSuccess())
                    return certificateNotValidDate;
            }
        }
        return new ApiResponse("Ok", false);
    }


    /**
     * password va phone numberni tekshirish
     *
     * @param userDto
     * @return
     */
    public ApiResponse checkEmailAndPhoneNumber(UserDto userDto) {
        if (userRepository.existsByEmail(userDto.getEmail().toLowerCase()))
            return new ApiResponse("Email is already exist", false);
        if (userRepository.existsByPhoneNumber(userDto.getPhoneNumber()))
            return new ApiResponse("This phone number is already exist", false);
        return new ApiResponse("", true);
    }

    /**
     * PAROLNI TEKSHIRADIGAN METHOD
     *
     * @param userDto
     * @return
     */
    public ApiResponse checkPassword(UserDto userDto) {
        if (userDto.getPassword().length() < 6 || userDto.getPassword().length() > 16)
            return new ApiResponse("Password size between 6 and 16 character!", false);
        if (!(userDto.getPassword().equals(userDto.getPrePassword())))
            return new ApiResponse("Password and prepassword is not equals!", false);
        return new ApiResponse("", true);
    }

    /**
     * ctelefon raqam va emailni o'zgartirayotganda tekshirish uchun
     *
     * @param userDto
     * @return
     */
    public ApiResponse checkEmailAndPhoneNumberForEdit(UserDto userDto, UUID userId) {
        if (userRepository.existsByEmailAndIdNot(userDto.getEmail().toLowerCase(), (userDto.getId() != null ? userDto.getId() : userId)))
            return new ApiResponse("Email is already exist", false);
        if (userRepository.existsByPhoneNumberAndIdNot(userDto.getPhoneNumber(), (userDto.getId() != null ? userDto.getId() : userId)))
            return new ApiResponse("This phone number is already exist", false);
        return new ApiResponse("", true);
    }

    public UserDetails getUserById(UUID id) {
        return userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getUser"));
    }

    @Override
    public UserDetails loadUserByUsername(String userName) {
        return userRepository.findByEmailOrPhoneNumber(userName.toLowerCase(), userName).orElseThrow(() -> new UsernameNotFoundException("get user name"));
    }

    public String trimPhoneNumber(String username) {
        return "+" + username.substring(1);
    }

    @SneakyThrows
    public ApiResponse forgotPassword(String username) {
        boolean isEmail = username.contains("@");
        if (!isEmail) username = trimPhoneNumber(username);
        Optional<User> userOptional = userRepository.findByEmailOrPhoneNumber(username, username);
        if (!userOptional.isPresent()) {
            return new ApiResponse(isEmail ? "This email is not exist" : "This phone number is not exist", false);
        }
        String code = generateEmailCode().toString();
        userOptional.get().setEmailCode(code);
        userRepository.save(userOptional.get());
        if (isEmail) new Thread(() -> {
            try {
                mailSender.sendClientForgotPassword(userOptional.get().getFirstName() + " " + userOptional.get().getLastName(), userOptional.get().getEmail(), AppConstants.CLIENT_URl + "/resetPassword/" + code);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
        else
            twilioService.sendMessage(userOptional.get().getPhoneNumber(), AppConstants.CLIENT_URl + "/resetPassword/" + code);
        return new ApiResponse(isEmail ? "Verification code send your email" : "Verification code send your phone Number", true);
    }

    public Integer generateRandom() {
        return new Random().nextInt((999999 - 100000) + 1) + 100000;
    }

    public ApiResponse confirmVerification(String code, String username) {
        if (!username.contains("@")) username = trimPhoneNumber(username);
        Optional<User> userOptional = userRepository.findByEmailOrPhoneNumber(username, username);
        if (!userOptional.isPresent()) {
            return new ApiResponse("No users found", false);
        }
        if (!userOptional.get().getEmailCode().equals(code)) {
            return new ApiResponse("Verification code is not valid", false);
        }
        UUID emailCode = generateEmailCode();
        userOptional.get().setEmailCode(emailCode.toString());
        userRepository.save(userOptional.get());
        return new ApiResponse("success", true, emailCode);

    }

    public ApiResponse resetPassword(String password, String prePassword, String emailCode) {
        if (!password.equals(prePassword)) {
            return new ApiResponse("Password is not matches", false);
        }
        Optional<User> userOptional = userRepository.findByEmailCode(emailCode);
        if (!userOptional.isPresent()) {
            return new ApiResponse("No users found", false);
        }
        userOptional.get().setPassword(passwordEncoder.encode(password));
        userOptional.get().setEmailCode(null);
        userRepository.save(userOptional.get());
        return new ApiResponse("success", true);
    }
}