package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.Hour;
import ai.ecma.duoserver.entity.MainService;
import ai.ecma.duoserver.entity.MainServiceWorkTime;
import ai.ecma.duoserver.entity.SubService;
import ai.ecma.duoserver.payload.*;
import ai.ecma.duoserver.repository.HourRepository;
import ai.ecma.duoserver.repository.MainServiceRepository;
import ai.ecma.duoserver.repository.MainServiceWorkTimeRepository;
import com.google.inject.internal.cglib.proxy.$NoOp;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class MainServiceService {
    @Autowired
    MainServiceRepository mainServiceRepository;
    @Autowired
    MainServiceWorkTimeRepository mainServiceWorkTimeRepository;
    @Autowired
    HourRepository hourRepository;

    public ApiResponse saveMainService(MainServiceDto2 mainServiceDto2) {
        List<HourDto> mainServiceHours = mainServiceDto2.getMainServiceHours();
        for (int i = 0; i < mainServiceHours.size(); i++) {
            LocalTime fromTime = LocalTime.parse(mainServiceHours.get(i).getFromTime());
            LocalTime tillTime;
            if (mainServiceHours.get(i).getTillTime().equals("24:00")) {
                tillTime = LocalTime.parse("23:59");
            } else {
                tillTime = LocalTime.parse(mainServiceHours.get(i).getTillTime());
            }
            for (int j = i + 1; j < mainServiceHours.size(); j++) {
                LocalTime a = LocalTime.parse(mainServiceHours.get(j).getFromTime());
                LocalTime b;
                if (mainServiceHours.get(j).getTillTime().equals("24:00")) {
                    b = LocalTime.parse("23:59");
                } else {
                    b = LocalTime.parse(mainServiceHours.get(j).getTillTime());
                }
                if (fromTime.isBefore(b) && tillTime.isAfter(a)) {
                    return new ApiResponse("Error times, Please try again!", false);
                }
            }
            if (fromTime.isAfter(tillTime))
                return new ApiResponse("Times wrong", false);

            MainServiceWorkTime mainServiceWorkTime = new MainServiceWorkTime();
            mainServiceWorkTime.setActive(mainServiceDto2.isActive());
            mainServiceWorkTime.setMainService(makeMainService(mainServiceDto2));
            mainServiceWorkTime.setPercent(mainServiceDto2.getPercent());
            mainServiceWorkTime.setTillTime(tillTime.toString());
            mainServiceWorkTime.setFromTime(fromTime.toString());
            mainServiceWorkTimeRepository.save(mainServiceWorkTime);

        }
        return new ApiResponse("Save Main Service", true);
    }


    public MainService makeMainService(MainServiceDto2 mainServiceDto2) {
        Optional<MainService> byName = mainServiceRepository.findByName(mainServiceDto2.getName());
        if (byName.isPresent()) {
            MainService mainService = byName.get();
            mainService.setName(mainServiceDto2.getName());
            mainService.setOnline(mainServiceDto2.isOnline());
            mainService.setActive(mainServiceDto2.isActive());
            List<HourDto> mainServiceHours = mainServiceDto2.getMainServiceHours();
            mainService.setFromTime(mainServiceDto2.getFromTime());
            mainService.setTillTime(mainServiceDto2.getTillTime());
            return mainServiceRepository.save(mainService);
        }
        MainService mainService = new MainService();
        mainService.setName(mainServiceDto2.getName());
        mainService.setOnline(mainServiceDto2.isOnline());
        mainService.setActive(mainServiceDto2.isActive());
        List<HourDto> mainServiceHours = mainServiceDto2.getMainServiceHours();
        mainService.setFromTime(mainServiceDto2.getFromTime());
        mainService.setTillTime(mainServiceDto2.getTillTime());
        return mainServiceRepository.save(mainService);


    }

}
