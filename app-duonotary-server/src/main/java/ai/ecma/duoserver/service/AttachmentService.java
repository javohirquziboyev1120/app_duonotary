package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.Attachment;
import ai.ecma.duoserver.entity.AttachmentContent;
import ai.ecma.duoserver.entity.enums.AttachmentType;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.repository.AttachmentContentRepository;
import ai.ecma.duoserver.repository.AttachmentRepository;
import ai.ecma.duoserver.utils.AppConstants;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@Service
public class AttachmentService {
    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;


    /**
     * BU RASMNI YUKLASH UCHUN
     *
     * @param request
     * @return
     */
    public UUID uploadFile(MultipartHttpServletRequest request) {
        try {
            Iterator<String> fileNames = request.getFileNames();
            while (fileNames.hasNext()) {
                MultipartFile file = request.getFile(fileNames.next());
                assert file != null;
                Attachment attachment = new Attachment(
                        file.getOriginalFilename(),
                        file.getSize(),
                        file.getContentType() != null ? file.getContentType() : "unknown",
                        AttachmentType.SIMPLE
                );
                Attachment savedAttachment = attachmentRepository.save(attachment);

                AttachmentContent attachmentContent = new AttachmentContent(
                        savedAttachment,
                        file.getBytes());
                attachmentContentRepository.save(attachmentContent);
                return savedAttachment.getId();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * BU PRICING YARATISH UCHUN
     *
     * @param id
     * @return
     */
    public HttpEntity<?> getFile(UUID id) {
        Attachment attachment = attachmentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getAttachment"));
        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachmentId(id);
        return ResponseEntity.ok().contentType(MediaType.valueOf(attachment.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; fileName=\"" + attachment.getName() + "\"")
                .body(attachmentContent.getContent());
    }

    public void getOrderFile(UUID id, HttpServletResponse response) throws IOException {
        Attachment attachment = attachmentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getAttachment"));
        response.setContentType(attachment.getContentType());
        response.getOutputStream().write(attachmentContentRepository.findByAttachmentId(id).getContent());
        response.getOutputStream().flush();
    }

    /**
     * BU PRICING YARATISH UCHUN
     *
     * @param id
     * @return
     */
    public HttpEntity<?> getFilePDF(UUID id, boolean download) {
        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachmentId(id);
        if (download) {
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_PDF)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\" codelari.pdf")
                    .body(Base64.decodeBase64(attachmentContent.getContent()));
        } else {
            HttpHeaders header = new HttpHeaders();
            header.setContentType(MediaType.APPLICATION_PDF);
            return new ResponseEntity<>(Base64.decodeBase64(attachmentContent.getContent()), header, HttpStatus.OK);
        }
    }

    public void deleteAttachment(UUID id) {
        attachmentContentRepository.deleteByAttachment(id);
        attachmentRepository.delete(id);
    }

    public void deleteAllAttachment(Collection<UUID> uuidList) {
        uuidList.forEach(this::deleteAttachment);
    }

    public void deletePdfFiles() {
        deleteAllAttachment(attachmentRepository.findAllByAttachmentType(AttachmentType.RENDER.name()));
    }
}
