package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.*;
import ai.ecma.duoserver.entity.enums.AttachmentType;
import ai.ecma.duoserver.entity.enums.RoleName;
import ai.ecma.duoserver.payload.OrderPdf;
import ai.ecma.duoserver.payload.UserPdf;
import ai.ecma.duoserver.repository.*;
import lombok.Value;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import sun.misc.Resource;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

@Service
public class PdfService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    @Autowired
    RoleRepository roleRepository;
    @Autowired
    OrderRepository orderRepository;

    @Autowired
    PaymentRepository paymentRepository;

    final private String userJrxmlPath="classpath:templates/UserList.jrxml";
    final private String orderJrxmlPath="classpath:templates/OrderList.jrxml";




    public HttpEntity<?> getAgentListPdf() throws IOException, JRException {
        return generatePdfFromJasper(generateDate("AgentList"), userJrxmlPath, getUserList(userRepository.getUsersForPdf(RoleName.ROLE_AGENT.name())));
    }

    public HttpEntity<?> getCustomerListPdf() throws IOException, JRException {
        return generatePdfFromJasper(generateDate("CustomerList"), userJrxmlPath, getUserList(userRepository.getUsersForPdf(RoleName.ROLE_USER.name())));
    }

    public HttpEntity<?> getOrderListPdf() throws IOException, JRException {
        return generatePdfFromJasper(generateDate("OrderList"), orderJrxmlPath, getOrderList(orderRepository.findAll()));
    }

    public HttpEntity<?> getOrderListPdfByAgent(UUID userId ) throws IOException, JRException {
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isPresent())
            return generatePdfFromJasper(generateDate(userOptional.get().getFirstName() + " " + userOptional.get().getLastName() + " " + "OrderList"), orderJrxmlPath, getOrderList(orderRepository.findAllByAgent(userOptional.get())));
        return null;
    }

    /**
     * Bu universal PDF yaratadigan method
     *
     * @param filePath         jrxml file qayerdaligi ,
     * @param objects          List<Object> object hohlagan narsa bolishi mumkun example : User,Order,
     * @param downloadFileName download bolgandagi fileni nomi,
     */
    public HttpEntity<?> generatePdfFromJasper(String downloadFileName, String filePath, Object objects) throws IOException, JRException {
        String file = ResourceUtils.getFile(filePath).getAbsolutePath();
        JasperReport jasperReport = JasperCompileManager.compileReport(file);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap<>(), new JRBeanCollectionDataSource((Collection<?>) objects));
        byte[] bytes = JasperExportManager.exportReportToPdf(jasperPrint);
        assert bytes != null;
        String s = Base64.encodeBase64String(bytes);
        Attachment attachment = new Attachment(
                downloadFileName + ".pdf",
                s.length(),
                "application/pdf",
                AttachmentType.RENDER
        );
        Attachment savedAttachment = attachmentRepository.save(attachment);
        AttachmentContent attachmentContent = new AttachmentContent(savedAttachment, bytes);
        attachmentContentRepository.save(attachmentContent);
        return ResponseEntity.ok(savedAttachment.getId());
    }

    public String generateDate(String name) {
        return name + " " + new Timestamp(System.currentTimeMillis()).toString().substring(0, 11) + ".pdf";
    }
    public UserPdf getUser(User user, Integer i) {
        return new UserPdf(
                user.getId(),
                i,
                user.getFirstName(),
                user.getLastName(),
                user.getPhoneNumber(),
                user.getEmail(),
                orderRepository.countUserOrders(user.getId()),
                user.isActive() ? "Active" : "DisActive",
                user.isOnline() ? "Online" : "Offline",
                new ArrayList<>(user.getRoles()).get(0).getRoleName().name(),
                getOnlineAgent(user.getOnlineAgent()),
                user.isEnabled() ? "Enabled" : "Disabled"
        );
    }

    public OrderPdf getOrder(Order order, Integer i) {
        Optional<Payment> pay = paymentRepository.findByOrder(order);

        return new OrderPdf(
                i,
                order.getAddress(),
                order.getZipCode().getCode(),
                order.getClient().getFirstName() + " " + order.getClient().getLastName(),
                order.getClient().getEmail(),
                order.getServicePrice().getService().getSubService().getName(),
                order.getPayType().getName(),
                order.getAgent().getFirstName() + " " + order.getAgent().getLastName(),
                order.getAgent().getEmail(),
                order.getAmount(),
                order.getAmountDiscount(),
                order.getCheckNumber(),
                order.getSerialNumber(),
                order.getOrderStatus().name(),
                order.getCountDocument(),
                order.isActive() ? "Active" : "DisActive",
                pay.map(payment -> payment.getPayStatus().name()).orElse("NONE"),
                order.getTitleDocument()
        );
    }

    public List<UserPdf> getUserList(List<User> userList) {
        List<UserPdf> list = new ArrayList<>();
        for (int i = 0; i < userList.size(); i++) {
            list.add(getUser(userList.get(i), 1 + i));
        }
        return list;
    }

    public List<OrderPdf> getOrderList(List<Order> orderList) {
        List<OrderPdf> list = new ArrayList<>();
        for (int i = 0; i < orderList.size(); i++) {
            list.add(getOrder(orderList.get(i), 1 + i));
        }
        return list;
    }
    public String getOnlineAgent(Boolean b) {
        if (b == null) return "Client";
        if (b) return "Online Agent";
        return "Offline Agent";
    }

}
