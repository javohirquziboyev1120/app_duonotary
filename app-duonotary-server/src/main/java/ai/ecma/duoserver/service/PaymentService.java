package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.Order;
import ai.ecma.duoserver.entity.PayType;
import ai.ecma.duoserver.entity.Payment;
import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.entity.enums.OrderStatus;
import ai.ecma.duoserver.entity.enums.PayStatus;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.*;
import ai.ecma.duoserver.repository.OrderRepository;
import ai.ecma.duoserver.repository.PayTypeRepository;
import ai.ecma.duoserver.repository.PaymentRepository;
import ai.ecma.duoserver.repository.UserRepository;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.model.PaymentIntent;
import com.stripe.param.ChargeCreateParams;
import com.stripe.param.PaymentIntentCaptureParams;
import com.stripe.param.PaymentIntentCreateParams;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PaymentService {
    @Value("${stripe.keys.secret}")
    private String secretKey;

    final
    PayTypeRepository payTypeRepository;

    final
    PaymentRepository paymentRepository;

    final
    UserRepository userRepository;

    final
    PasswordEncoder passwordEncoder;

    final
    OrderRepository orderRepository;

    final
    MailSenderService mailSenderService;

    public PaymentService(PaymentRepository paymentRepository, PayTypeRepository payTypeRepository, UserRepository userRepository, PasswordEncoder passwordEncoder, OrderRepository orderRepository, MailSenderService mailSenderService) {
        this.paymentRepository = paymentRepository;
        this.payTypeRepository = payTypeRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.orderRepository = orderRepository;
        this.mailSenderService = mailSenderService;
    }

    //Bu method orqali tolov ni darhol pul yeshib olish front ga clientSecretKey yasab beradigan method
    public String getChangeOnTime(UUID id) {
        Optional<Order> optionalOrder = orderRepository.findById(id);
        if (optionalOrder.isPresent()) {
            Order order = optionalOrder.get();
            Stripe.apiKey = secretKey;

            PaymentIntentCreateParams createParams = new PaymentIntentCreateParams.Builder()
                    .setCurrency("usd")
                    .setAmount((long) ((order.getAmount() - order.getAmountDiscount()) * 100))
                    .build();
            try {
                PaymentIntent paymentIntent = PaymentIntent.create(createParams);
                return paymentIntent.getClientSecret();
            } catch (StripeException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    //Bu Method orqali orderId va stripe_Token_Id kelsa Future  Charge amalga oshiriladi
    public ApiResponse getFutureCharge(UUID orderId, String tokenId) {
        try {
            Order order = orderRepository.findById(orderId).orElseThrow(() -> new ResourceNotFoundException("order", "id", orderId));
            ChargeCreateParams params = ChargeCreateParams.builder()
                    .setAmount((long) ((order.getAmount() - order.getAmountDiscount()) * 100))
                    .setCurrency("usd")
                    .setDescription("Example charge")
                    .setSource(tokenId)
                    .setCapture(false)
                    .build();

            Charge charge = Charge.create(params);

            Payment payment = new Payment();
            payment.setOrder(order);
            payment.setPaySum(order.getAmount() - order.getAmountDiscount());
            payment.setPayType(order.getPayType());
            if (order.getOrderStatus().equals(OrderStatus.COMPLETED)) {
                PaymentIntent intent = PaymentIntent.retrieve(charge.getId());
                PaymentIntentCaptureParams charger =
                        PaymentIntentCaptureParams.builder()
                                .setAmountToCapture((long) (order.getAmount() - order.getAmountDiscount()) * 100)
                                .build();
                PaymentIntent capture = intent.capture(charger);
                payment.setPayStatus(PayStatus.PAYED);
                getCaptureByOrderId(order.getId());
                payment.setCheckNumber(capture.getId());
            } else {
                payment.setPayStatus(PayStatus.STRIPE);
                payment.setChargeId(charge.getId());
            }
            paymentRepository.save(payment);
            return new ApiResponse("Ok", true, charge.getId());
        } catch (StripeException e) {
            return new ApiResponse("Error", false, null);
        }
    }

    //Bu Method orqali order complate boganda orderId berib yuborilsa shu order uchun future charge qilingan bold=sa pul yechib olinadi.
    public ApiResponse getCaptureByOrderId(UUID orderId) {
        try {
            Stripe.apiKey = secretKey;
            Order order = orderRepository.findById(orderId).orElseThrow(() -> new ResourceNotFoundException("order", "id", orderId));
            Optional<Payment> optionalPayment = paymentRepository.findByOrder(order);
            if (optionalPayment.isPresent()) {
                Payment payment = optionalPayment.get();
                if (payment.getChargeId() != null && payment.getPayStatus().equals(PayStatus.STRIPE)) {
                    PaymentIntent intent = PaymentIntent.retrieve(order.getChargeId());

                    PaymentIntentCaptureParams params =
                            PaymentIntentCaptureParams.builder()
                                    .setAmountToCapture((long) (order.getAmount() - order.getAmountDiscount()) * 100)
                                    .build();
                    PaymentIntent capture = intent.capture(params);
                    payment.setCheckNumber(capture.getId());
                    payment.setPayStatus(PayStatus.PAYED);
                    paymentRepository.save(payment);
                    return new ApiResponse("Ok", true);
                }
            }
            return new ApiResponse("Error", false, null);
        } catch (Exception e) {
            return new ApiResponse("Error", false, null);
        }
    }

    public ApiResponse setPayStatus(UUID id, String checkId) {
        try {
            Order order = orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getOrder", "id", id));
            Optional<Payment> optionalPayment = paymentRepository.findByOrder_Id(id);
            Payment payment;
            if (optionalPayment.isPresent()) {
                payment = optionalPayment.get();
                payment.setPayStatus(PayStatus.PAYED);
            } else {
                payment = new Payment();
                payment.setOrder(order);
                payment.setPayStatus(PayStatus.PAYED);
                payment.setPayType(order.getPayType());
                payment.setPaySum(order.getAmount() - order.getAmountDiscount());
                payment.setClientId(order.getClient().getId().toString());
            }
            payment.setCheckNumber(checkId);
            paymentRepository.save(payment);
            mailSenderService.sendDocVerifyFileByOrderId(id);
            return new ApiResponse("Success", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public String testFuturePayment(UUID orderId) throws StripeException {
        Stripe.apiKey = secretKey;

        Order order = orderRepository.findById(orderId).orElseThrow(() -> new ResourceNotFoundException("", "Order", orderId));

        PaymentIntentCreateParams params =
                PaymentIntentCreateParams.builder()
                        .setAmount((long) ((order.getAmount() - order.getAmountDiscount()) * 100))
                        .setCurrency("usd")
                        .addPaymentMethodType("card")
                        .setCaptureMethod(PaymentIntentCreateParams.CaptureMethod.MANUAL)
                        .build();

        PaymentIntent paymentIntent = PaymentIntent.create(params);

        order.setChargeId(paymentIntent.getId());
        Optional<Payment> optionalPayment = paymentRepository.findByOrder(order);
        Payment payment;
        if (optionalPayment.isPresent()) {
            payment = optionalPayment.get();
            payment.setChargeId(paymentIntent.getId());
            payment.setPayStatus(PayStatus.SCHEDULE);
        } else {
            payment = new Payment(
                    order, order.getAmount() - order.getAmountDiscount(), PayStatus.SCHEDULE, "", order.getPayType(), order.getClient().getId().toString(), order.getChargeId()
            );
        }
        paymentRepository.save(payment);
        orderRepository.save(order);
        return paymentIntent.getClientSecret();
    }

    public ApiResponse configFuturePayment(String paymentIntendId, UUID orderId){
        try {
            Optional<Payment> paymentOptional = paymentRepository.findByChargeIdAndOrder_Id(paymentIntendId, orderId);
            if (paymentOptional.isPresent()){
                Payment payment = paymentOptional.get();
                payment.setPayStatus(PayStatus.STRIPE);
                paymentRepository.save(payment);
                return new ApiResponse("Ok", true);
            }else{
                return new ApiResponse("Error", false);
            }
        }catch (Exception e){
            return new ApiResponse("Payment not found", false);
        }
    }

    public ApiResponse cancelPaymentIntent(UUID orderId) {
        try {
            Stripe.apiKey = secretKey;
            Order order = orderRepository.findById(orderId).orElseThrow(() -> new ResourceNotFoundException("order", "id", orderId));
            Optional<Payment> optionalPayment = paymentRepository.findByOrder(order);
            if (optionalPayment.isPresent()) {
                Payment payment = optionalPayment.get();
                if (payment.getChargeId() != null && payment.getPayStatus().equals(PayStatus.STRIPE)) {
                    PaymentIntent paymentIntent =
                            PaymentIntent.retrieve(
                                    payment.getChargeId()
                            );

                    PaymentIntent cancel = paymentIntent.cancel();

                    payment.setPayStatus(PayStatus.BACKOFF);
                    payment.setChargeId(cancel.getId());
                    paymentRepository.save(payment);
                    return new ApiResponse("Ok", true);
                }
            }
            return new ApiResponse("Error", false, null);
        } catch (Exception e) {
            return new ApiResponse("Error", false, null);
        }
    }

    public boolean cancelOrder(Payment payment, double amount) throws StripeException {
        try {
            Stripe.apiKey = secretKey;
            if (payment.getChargeId() != null && payment.getPayStatus().equals(PayStatus.STRIPE)) {
                if (amount>0) {
                    PaymentIntent intent = PaymentIntent.retrieve(payment.getChargeId());

                    PaymentIntentCaptureParams params =
                            PaymentIntentCaptureParams.builder()
                                    .setAmountToCapture((long) (amount) * 100)
                                    .build();
                    intent.capture(params);
                    return true;
                }else{
                    PaymentIntent paymentIntent =
                            PaymentIntent.retrieve(
                                    payment.getChargeId()
                            );

                    PaymentIntent cancel = paymentIntent.cancel();

                    payment.setPayStatus(PayStatus.BACKOFF);
                    payment.setChargeId(cancel.getId());
                    paymentRepository.save(payment);
                }
            }
            return false;
        }catch (Exception e){return false;}

    }

    public ResPageable getPaymentInfo(int page, int size) {
        try {
            Page<Payment> payments = paymentRepository.findAllByPayStatus(PayStatus.PAYED, PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt"))));
            List<PaymentDto> collect = payments.getContent().stream().map(this::makePayment).collect(Collectors.toList());
            return new ResPageable(page, size, payments.getTotalPages(), payments.getTotalElements(), collect);
        } catch (Exception e) {
            return new ResPageable(page, size, 0, (long) 0, new ArrayList<>());
        }
    }

    public ResPageable getPayments(int page, int size) {
        try {
            Page<Payment> payments = paymentRepository.findAll(PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt"))));
            List<PaymentDto> collect = payments.getContent().stream().map(this::makePaymentAdmin).collect(Collectors.toList());
            return new ResPageable(page, size, payments.getTotalPages(), payments.getTotalElements(), collect);
        } catch (Exception e) {
            return new ResPageable(page, size, 0, (long) 0, new ArrayList<>());
        }
    }

    public PaymentDto makePayment(Payment payment) {
        return new PaymentDto(
                payment.getPaySum(),
                makeUserDto(payment.getOrder().getClient()),
                makeUserDto(payment.getOrder().getAgent()),
                makeOrderDto(payment.getOrder(), 1),
                payment.getPayType().getName()
        );
    }

    public PaymentDto makePaymentAdmin(Payment payment) {
        return new PaymentDto(
                makeUserDto(payment.getOrder().getClient()),
                makeUserDto(payment.getOrder().getAgent()),
                makeOrderDto(payment.getOrder(), 2),
                payment.getDate(),
                payment.getPaySum(),
                payment.getPayType(),
                payment.getChargeId(),
                payment.getPayStatus().name(),
                payment.getId()
        );
    }

    private UserDto makeUserDto(User user) {
        return new UserDto(user.getId(), user.getFirstName(), user.getLastName(), user.getPhoneNumber(), user.getEmail());
    }

    private OrderDto makeOrderDto(Order order, int type) {
        OrderDto orderDto = new OrderDto();
        orderDto.setSerialNumber(order.getSerialNumber());
        orderDto.setDiscountAmount(order.getAmountDiscount());
        orderDto.setAmount(order.getAmount());
        if (type == 2) orderDto.setId(order.getId());
        return orderDto;
    }

    public ApiResponse addOrEditPaymentAdmin(PaymentDto paymentDto) {
        try {
            Payment payment = new Payment();
            if (paymentDto.getId() != null)
                payment = paymentRepository.findById(paymentDto.getId()).orElseGet(Payment::new);
            Optional<Order> orderOptional = orderRepository.findById(paymentDto.getOrderId());
            if (orderOptional.isPresent()) {
                Order order = orderOptional.get();
                if (payment.getId() == null) payment = paymentRepository.findByOrder(order).orElseGet(Payment::new);
                Optional<PayType> payTypeOptional = payTypeRepository.findById(paymentDto.getPayTypeId());
                if (payTypeOptional.isPresent()) {
                    PayType payType = payTypeOptional.get();
                    Optional<User> optionalUser = userRepository.findById(order.getClient().getId());
                    if (optionalUser.isPresent()) {
                        User client = optionalUser.get();
                        if (paymentDto.getPaySum() > 0) {
                            payment.setOrder(order);
                            payment.setPaySum(paymentDto.getPaySum());
                            payment.setPayStatus(paymentDto.getPayStatus());
                            payment.setCheckNumber(order.getCheckNumber() != null ? order.getCheckNumber() : "");
                            payment.setPayType(payType);
                            payment.setClientId(client.getId().toString());
                            payment.setChargeId(paymentDto.getChargeId());
                            payment.setDate(paymentDto.getDate());
                            paymentRepository.save(payment);
                            return new ApiResponse("Payment saved", true);
                        } else {
                            return new ApiResponse("Payment sum is 0", false);
                        }
                    } else {
                        return new ApiResponse("Client not found", false);
                    }
                } else {
                    return new ApiResponse("Pay type not found", false);
                }
            } else {
                return new ApiResponse("Order not found", false);
            }
        } catch (
                Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse deletePayment(UUID id) {
        try {
            paymentRepository.deleteById(id);
            return new ApiResponse("Payment deleted", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }
}
