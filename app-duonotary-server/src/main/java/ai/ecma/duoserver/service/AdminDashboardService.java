package ai.ecma.duoserver.service;

import ai.ecma.duoserver.payload.AdminDashboardCalendarDto;
import ai.ecma.duoserver.payload.AdminDashboardNumbers;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.DashboardOrderCashDto;
import ai.ecma.duoserver.payload.enums.DashboardOrdersAndUserTypeEnum;
import ai.ecma.duoserver.repository.OrderRepository;
import ai.ecma.duoserver.repository.PaymentRepository;
import ai.ecma.duoserver.repository.TimeTableRepository;
import ai.ecma.duoserver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class AdminDashboardService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    TimeTableRepository timeTableRepository;

    public ApiResponse getUsersAndOrdersCount() {
        try {
            List<AdminDashboardNumbers> resList = new ArrayList<>();

            String stringToday = getStartTimeToday(1, null);
            Timestamp today = new Timestamp(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(stringToday).getTime());

            AdminDashboardNumbers users = new AdminDashboardNumbers(
                    DashboardOrdersAndUserTypeEnum.USER,
                    userRepository.getRegisteredUsersToday(today),
                    userRepository.findAllByEnabledTrue().size());

            AdminDashboardNumbers online = new AdminDashboardNumbers(
                    DashboardOrdersAndUserTypeEnum.ONLINE,
                    orderRepository.getOrdersToday(today, true),
                    orderRepository.getOrdersAllTime(true));


            AdminDashboardNumbers offline = new AdminDashboardNumbers(
                    DashboardOrdersAndUserTypeEnum.IN_PERSON,
                    orderRepository.getOrdersToday(today, false),
                    orderRepository.getOrdersAllTime(false));

            AdminDashboardNumbers mobile = new AdminDashboardNumbers(
                    DashboardOrdersAndUserTypeEnum.MOBILE,
                    orderRepository.getAllCountByCreatedAtLessThanAndRegisterByMobile(today),
                    orderRepository.getAllByRegisterByMobile());

            resList.add(users);
            resList.add(online);
            resList.add(offline);
            resList.add(mobile);

            return new ApiResponse("Success", true, resList);
        } catch (Exception e) {
            return new ApiResponse("Error", false, null);
        }
    }

    public ApiResponse getOrdersSum() {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            DashboardOrderCashDto caches = paymentRepository.getOrdersCashBySelectedDate(new Timestamp(simpleDateFormat.parse(getStartTimeToday(1, null)).getTime()), new Timestamp(simpleDateFormat.parse(getStartDateThisWeek()).getTime()), new Timestamp(simpleDateFormat.parse(getStartDateThisMonth()).getTime()));
            return new ApiResponse("Success", true, caches);
        } catch (Exception e) {
            return new ApiResponse("Error", false, null);
        }
    }

    public ApiResponse getOrdersByFilter(String stateId, String countyId, String zipCodeId, String sDate) {
        try {
            Date date;
            if (sDate.equals("")) date = new Date();
            else date = new SimpleDateFormat("yyyy-MM-dd").parse(sDate);
            List<AdminDashboardCalendarDto> forDashboard = timeTableRepository.getOrdersByZipCodeOrCountyOrStateAndDateForDashboard(stateId, countyId, zipCodeId, date);
            return new ApiResponse("Orders info", true, forDashboard);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse("Error", false, null);
        }
    }

    private String getStartDateThisMonth() {
        Calendar month = Calendar.getInstance();
        month.add(Calendar.MONTH, -1);
        String[] split = new Timestamp(month.getTime().getTime()).toString().split(" ");
        return split[0] + " 00:00:00";
    }

    private String getStartDateThisWeek() {
        Calendar week = Calendar.getInstance();
        week.add(Calendar.DATE, -7);
        String[] split = new Timestamp(week.getTime().getTime()).toString().split(" ");
        return split[0] + " 00:00:00";
    }

    private String getStartTimeToday(int type, Timestamp timestamp) {
        String[] strings;
        if (type == 1) strings = new Timestamp(new Date().getTime()).toString().split(" ");
        else strings = timestamp.toString().split(" ");
        return strings[0] + " 00:00:00";
    }

//    private UUID getRandomUUID(String id) {
//        return id.equals("null") ? null : UUID.fromString(id);
//    }
}
