package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.*;
import ai.ecma.duoserver.entity.enums.TermsEnum;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.*;
import ai.ecma.duoserver.repository.*;
import ai.ecma.duoserver.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class BlogService {
    @Autowired
    CheckUser checkUser;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    BlogRepository blogRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    TermsConditionRepository termsConditionRepository;
    @Autowired
    FaqRepository faqRepository;

    public ApiResponse addBlog(User user, ReqBlog request, boolean dynamic) {
        Blog newBlog = new Blog();
        return addOrEditBlog(user, newBlog, request, false, dynamic);
    }

    public ApiResponse editBlog(User user, ReqBlog request, UUID blogId, boolean dynamic) {
        Blog foundedBlog = blogRepository.findById(blogId).orElseThrow(() -> new ResourceNotFoundException("getBlog", "", blogId));
        return addOrEditBlog(user, foundedBlog, request, true, dynamic);
    }

    public ApiResponse addOrEditBlog(User user, Blog blog, ReqBlog request, boolean edit, boolean dynamic) {
        try {

            if (!edit) {
                String blogUrl = makeUrl(request.getTitle());
                blog.setUrl(blogUrl);
            }
            blog.setDynamic(dynamic);
            blog.setCategory(request.getCategoryId() == null ? blog.getCategory() : categoryRepository.findById(request.getCategoryId()).orElseThrow(() -> new ResourceNotFoundException("getCategory", "", request.getCategoryId())));
            blog.setAttachment(request.getAttachmentId() == null ? blog.getAttachment() : attachmentRepository.findById(request.getAttachmentId()).orElseThrow(() -> new ResourceNotFoundException("getAttachment", "", request.getAttachmentId())));
            blog.setText(request.getText());
            blog.setCategory(request.getCategoryId() == null ? null : categoryRepository.findById(request.getCategoryId()).orElseThrow(() -> new ResourceNotFoundException("getCategory", "", request.getCategoryId())));
            if (edit) {
                blog.setAttachment(request.getAttachmentId() == null ? blog.getAttachment() : attachmentRepository.findById(request.getAttachmentId()).orElseThrow(() -> new ResourceNotFoundException("get Attachment", "", request.getAttachmentId())));
            } else {
                blog.setAttachment(attachmentRepository.findById(request.getAttachmentId()).orElseThrow(() -> new ResourceNotFoundException("get Attachment", "", request.getAttachmentId())));
            }
            blog.setText(addClassName(request.getText()));
            blog.setTitle(request.getTitle());
            blog.setFeatured(request.isFeatured());
            blogRepository.save(blog);
            return new ApiResponse("Successfully " + (edit ? "edited" : "saved"), true);

        } catch (Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }
    }

    public ResBlog getResBlog(UUID blogId) {
        Blog blog = blogRepository.byId(blogId).orElseThrow(() -> new ResourceNotFoundException("getBlog", "", blogId));
        return getResBlog(blog);
    }

    public ResBlog getResBlog(Blog blog) {
        DateFormat dateFormat = new SimpleDateFormat("MMMM dd,yyyy");
        String date = dateFormat.format(blog.getCreatedAt());
        User user = userRepository.findById(blog.getCreatedBy()).orElseThrow(() -> new ResourceNotFoundException("getUser", "", blog.getCreatedBy()));
        String owner = user.getFirstName() + " " + user.getLastName();
        Attachment userPhoto = user.getPhoto();

        return new ResBlog(
                blog.getId(),
                blog.getTitle(),
                blog.getText(),
                blog.isFeatured(),
                blog.getAttachment() != null ? blog.getAttachment().getId() : null,
                userPhoto != null ? userPhoto.getId() : null,
                date,
                owner,
                blog.getUrl(),
                blog.getCategory() != null ? blog.getCategory().getId() : null,
                blog.getCategory() != null ? blog.getCategory().getName() : null
        );
    }

    public ResPageable getAll(Integer page, Integer size, boolean dynamic) {
        if (dynamic) {
            Page<Blog> allByDynamic = blogRepository.findAllByDynamic(true, PageRequest.of(page, size));
            return getResPageable(page, size, allByDynamic);
        } else {
            Page<Blog> allByDynamic = blogRepository.findAllByDynamic(false, PageRequest.of(page, size));
            return getResPageable(page, size, allByDynamic);
        }
    }

    public ResPageable getResPageable(Integer page, Integer size, Page<Blog> allBlogs) {
        List<ResBlog> resBlogList = new ArrayList<>();
        for (Blog blog : allBlogs) {
            resBlogList.add(getResBlog(blog));
        }
        return new ResPageable(
                page,
                size,
                allBlogs.getTotalPages(),
                allBlogs.getTotalElements(),
                resBlogList
        );
    }

    public String makeUrl(String title) {
        String replacedText = title.replace("?", "");
        String replacedText2 = replacedText.replace("&", "");
        String url = replacedText2.replace(" ", "-").toLowerCase();


        int step = 1;
        String newUrl = "";

        while (step > 0) {
            newUrl = url + "-0" + step;
            if (blogRepository.existsByUrl(newUrl)) {
                step++;
            } else {
                step = 0;
            }
        }
        return newUrl;
    }

    public String addClassName(String str) {
        return str.replace("<img ", "<img class=\"img-width\"");
    }

    public ApiResponse deleteBlog(UUID id) {
        try {
            blogRepository.deleteById(id);
            return new ApiResponse("Deleted", true);
        } catch (Exception e) {
            return new ApiResponse("Error in removal", false);
        }
    }

    public ApiResponse addTerms(User user, ReqBlog request, boolean edit) {
        TermsCondition termsCondition = new TermsCondition();
        return addOrEditTerms(user, termsCondition, request, edit);
    }

    public ApiResponse editTerms(User user, ReqBlog request, UUID termsId, boolean edit) {
        TermsCondition termsCondition = termsConditionRepository.findById(termsId).orElseThrow(() -> new ResourceNotFoundException("getTerms", "", termsId));
        return addOrEditTerms(user, termsCondition, request, edit);
    }

    public ApiResponse addOrEditTerms(User user, TermsCondition termsCondition, ReqBlog request, boolean edit) {
        try {
            termsCondition.setText(addClassName(request.getText()));
            if (!edit) {
                TermsEnum termsEnum = null;
                switch (request.getTermsEnum()) {
                    case "PRIVACYPOLISY":
                        termsEnum = TermsEnum.PRIVACYPOLISY;
                        break;
                    case "TERMSOFUSE":
                        termsEnum = TermsEnum.TERMSOFUSE;
                        break;
                    case "NOTIFICATIONSETTING":
                        termsEnum = TermsEnum.NOTIFICATIONSETTING;
                        break;
                    default:
                        return new ApiResponse("Error terms type", false);
                }
                if (!termsConditionRepository.existsByTermsEnum(termsEnum)) {
                    termsCondition.setTermsEnum(termsEnum);
                } else {
                    return new ApiResponse("Error terms type enum", false);
                }
            }
            termsConditionRepository.save(termsCondition);
            return new ApiResponse("Successfully " + (edit ? "edited" : "saved"), true);
        } catch (Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }
    }

    public ApiResponse deleteTerms(UUID id) {
        try {
            termsConditionRepository.deleteById(id);
            return new ApiResponse("Deleted", true);
        } catch (Exception e) {
            return new ApiResponse("Error in removal", false);
        }
    }

    public TermsCondition getTerms(String termsEnum) {
        TermsCondition termsCondition = new TermsCondition();
        switch (termsEnum) {
            case "PRIVACYPOLISY":
                termsCondition = termsConditionRepository.findByTermsEnum(TermsEnum.PRIVACYPOLISY).orElseThrow(() -> new ResourceNotFoundException("getTerms", "", termsEnum));
                break;
            case "TERMSOFUSE":
                termsCondition = termsConditionRepository.findByTermsEnum(TermsEnum.TERMSOFUSE).orElseThrow(() -> new ResourceNotFoundException("getTerms", "", termsEnum));
                break;
            case "NOTIFICATIONSETTING":
                termsCondition = termsConditionRepository.findByTermsEnum(TermsEnum.NOTIFICATIONSETTING).orElseThrow(() -> new ResourceNotFoundException("getTerms", "", termsEnum));
                break;
            default:
        }
        return termsCondition;
    }

    public ApiResponse addFaq(User user, ReqFaq request) {
        if (checkUser.isSuperAdmin(user)) {
            Faq newFaq = new Faq();
            newFaq.setHeader(request.getHeader());
            newFaq.setText(request.getText());
            if (request.getRole().equals("AGENT")) {
                newFaq.setRole("AGENT");
            } else if (request.getRole().equals("CLIENT")) {
                newFaq.setRole("CLIENT");
            }
            faqRepository.save(newFaq);
            return new ApiResponse("Successfully add FAQ", true);
        }
        return new ApiResponse("Error", false);
    }

    public ApiResponse editFaq(User user, Integer id, ReqFaq request) {
        if (checkUser.isSuperAdmin(user)) {
            Faq newFaq = faqRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getFaq", "id", id));
            newFaq.setText(request.getText());
            newFaq.setHeader(request.getHeader());
            if (request.getRole().equals("AGENT")) {
                newFaq.setRole("AGENT");
            } else if (request.getRole().equals("CLIENT")) {
                newFaq.setRole("CLIENT");
            }
            faqRepository.save(newFaq);
            return new ApiResponse("Successfully edit FAQ", true);
        }
        return new ApiResponse("Error", false);
    }

    public ApiResponse getAllFaqByRole(String role) {
        if (role.equals("AGENT")) {
            List<Faq> agent = faqRepository.findAllByRole("AGENT");
            return new ApiResponse("Successfully edit FAQ", true, agent);
        } else if (role.equals("CLIENT")) {
            List<Faq> client = faqRepository.findAllByRole("CLIENT");
            return new ApiResponse("Successfully edit FAQ", true, client);
        }
        return new ApiResponse("Error", false);
    }

    public ApiResponse getAllFaq() {
        List<Faq> all = faqRepository.findAll();
        return new ApiResponse("Success", true, all);
    }

    public ApiResponse deleteFaq(User user, Integer id) {
        try {
            if (checkUser.isSuperAdmin(user)) {
                faqRepository.deleteById(id);
                return new ApiResponse("Successfully deleted", true);
            }
            return new ApiResponse("Error", false);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }
}
