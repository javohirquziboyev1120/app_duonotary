package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.*;
import ai.ecma.duoserver.entity.enums.HourTypeEnum;
import ai.ecma.duoserver.entity.enums.WeekDayEnum;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.*;
import ai.ecma.duoserver.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AgentScheduleService {
    @Autowired
    AgentScheduleRepository agentScheduleRepository;

    @Autowired
    HourRepository hourRepository;

    @Autowired
    WeekDaysRepository weekDaysRepository;

    @Autowired
    AgentHourOffRepository agentHourOffRepository;

    @Autowired
    TimeDurationRepository timeDurationRepository;

    @Autowired
    TimeTableRepository timeTableRepository;

    @Autowired
    TimeBookedRepository timeBookedRepository;

    public ApiResponse addOrEditSchedule(User user, AgentScheduleDto agentScheduleDto) {
        try {
            List<HourDto> hourList = agentScheduleDto.getHourList();
            for (int i = 0; i < hourList.size(); i++) {
                LocalTime fromTime = LocalTime.parse(hourList.get(i).getFromTime());
                LocalTime tillTime;
                if (hourList.get(i).getTillTime().equals("24:00")) {
                    tillTime = LocalTime.parse("23:59");
                } else {
                    tillTime = LocalTime.parse(hourList.get(i).getTillTime());
                }
                for (int j = i + 1; j < hourList.size(); j++) {
                    LocalTime a = LocalTime.parse(hourList.get(j).getFromTime());
                    LocalTime b;
                    if (hourList.get(j).getTillTime().equals("24:00")) {
                        b = LocalTime.parse("23:59");
                    } else {
                        b = LocalTime.parse(hourList.get(j).getTillTime());
                    }
                    if (fromTime.isBefore(b) && tillTime.isAfter(a)) {
                        return new ApiResponse("Error times, Please try again!", false);
                    }
                }
                if (fromTime.isAfter(tillTime))
                    return new ApiResponse("Times wrong", false);
            }
            Set<Hour> hoursSet = new HashSet<>();
            for (Integer weekDayId : agentScheduleDto.getWeekDayIds()) {
                AgentSchedule agentSchedule = agentScheduleRepository.findByUserAndWeekDay_Id(user, weekDayId).orElseGet(AgentSchedule::new);

                agentSchedule.setWeekDay(weekDaysRepository.findById(weekDayId).orElseThrow(() -> new ResourceNotFoundException("id", "weekdays", null)));
                agentSchedule.setUser(user);
                agentSchedule.setDayOff(agentScheduleDto.isDayOff());
                AgentSchedule savedAgentSchedule = agentScheduleRepository.save(agentSchedule);
                for (HourDto hourDto : hourList) {
                    hourRepository.deleteAllByAgentSchedule_Id(savedAgentSchedule.getId());
                    hoursSet.add(new Hour(
                            hourDto.getFromTime() + ":00",
                            hourDto.getTillTime() + ":00",
                            savedAgentSchedule));
                }
                hourRepository.saveAll(hoursSet);
            }
            return new ApiResponse("Your daily schedule saved !", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public List<AgentScheduleDto> getAllScheduleByAgent(UUID userId, User user) {
        List<WeekDay> weekdays = weekDaysRepository.getWeekdays(userId != null ? userId : user.getId());
        for (WeekDay weekday : weekdays) {
            agentScheduleRepository.save(new AgentSchedule(weekday, user, true));
        }
        return agentScheduleRepository.findAllByUserIdOrderByWeekDay_OrderNumber(userId != null ? userId : user.getId())
                .stream().map(this::getAgentScheduleDto).collect(Collectors.toList());
    }

    public ApiResponse deleteAgentScheduleById(UUID agentScheduleId) {
        try {
            hourRepository.deleteAllByAgentSchedule_Id(agentScheduleId);
            return new ApiResponse("Successfully deleted", true);
        } catch (Exception e) {
            return new ApiResponse("Error in deleting", false);
        }
    }

    private AgentScheduleDto getAgentScheduleDto(AgentSchedule agentSchedule) {
        return new AgentScheduleDto(
                agentSchedule.getId(),
                agentSchedule.getWeekDay().getId(),
                agentSchedule.isDayOff(),
                getWeekDay(agentSchedule),
                getHourDtoList(agentSchedule.getId()));
    }

    public WeekDaysDto getWeekDay(AgentSchedule agentSchedule) {
        return new WeekDaysDto(
                agentSchedule.getWeekDay().getId(),
                agentSchedule.getWeekDay().getDay(),
                agentSchedule.getWeekDay().getOrderNumber());
    }

    private HourDto getHourDto(Hour hour) {
        return new HourDto(
                hour.getId(),
                hour.getFromTime().substring(0, 5),
                hour.getTillTime().substring(0, 5));
    }

    public ApiResponse addAgentDayOff(UUID agentScheduleId, boolean isOff) {
        try {
            Optional<AgentSchedule> optionalAgentSchedule = agentScheduleRepository.findById(agentScheduleId);
            if (!optionalAgentSchedule.isPresent()) return new ApiResponse("This day not found");

            AgentSchedule agentSchedule = optionalAgentSchedule.get();
            agentSchedule.setDayOff(isOff);
            agentScheduleRepository.save(agentSchedule);
            return new ApiResponse("Saved", true);
        } catch (
                Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse addAgentHourOff(User user, Timestamp from, String till) {
        try {
            String[] strings = from.toString().split(" ");
            String sFormat = strings[0] + " " + till;
            Timestamp tillTimestamp = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(sFormat).getTime());

            Integer timeDuration = timeDurationRepository.getLast().getDurationTime();
            long d = (tillTimestamp.getTime() - from.getTime()) / (60 * 1000);
            if (!(d > 0 && d <= timeDuration)) {
                tillTimestamp = new Timestamp(from.getTime() + new Time(0, timeDuration + 360, 0).getTime());
            }

            Optional<AgentHourOff> agentHourOffOptional = agentHourOffRepository.findByFromHourOffEqualsAndTillHourOffEquals(from, tillTimestamp);
            if (agentHourOffOptional.isPresent()) {
                agentHourOffRepository.delete(agentHourOffOptional.get());
                return new ApiResponse("This time activated!", true);
            }

            List<TimeTable> timeTable = timeTableRepository.getTimeTableByAgentAndTimes(user.getId(), from, tillTimestamp);
            if (timeTable.size() > 0)
                return new ApiResponse("You have an order during this time period", false);
            else {
                agentHourOffRepository.save(new AgentHourOff(user, from, tillTimestamp));
                return new ApiResponse("Saved", true);
            }
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public List<AgentScheduleDto> getAgentWorkTimes(UUID agentId) {
        try {
            List<AgentSchedule> list = agentScheduleRepository.findAllByUserIdOrderByWeekDay_OrderNumber(agentId);
            return list.stream().map(
                    agentSchedule -> new AgentScheduleDto(
                            getWeekDay(agentSchedule),
                            getHourDtoList(agentSchedule.getId()),
                            agentSchedule.isDayOff())).collect(Collectors.toList());
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public List<TimeTableHourDto> getAgentDailySchedule(UUID agentId, Date date) {
        try {
            WeekDayEnum dayEnum = WeekDayEnum.valueOf(new SimpleDateFormat("EEEE").format(date).toUpperCase());

            List<TimeTable> timeTableListDaily = timeTableRepository.getTimeTableByAgentIdAndSelectedDate(agentId, date);

            AgentSchedule agentSchedule = agentScheduleRepository.findByUserIdAndWeekDay_Day(agentId, dayEnum);

            if (agentSchedule.isDayOff() && timeTableListDaily.isEmpty()) return new ArrayList<>();

            List<AgentHourOff> agentHourOffListDaily = agentHourOffRepository.getAgentHourOffByAgentIdAndSelectedDate(agentId, date);

            Set<Hour> hourList = hourRepository.findAllByAgentSchedule_Id(agentSchedule.getId());

            Integer timeDuration = timeDurationRepository.getLast().getDurationTime();

            LocalTime durationTime = LocalTime.parse(new Time(0, timeDuration, 0).toString());

//            TreeSet<TimeTableHourDto> response = new TreeSet<>();
            List<TimeTableHourDto> response = new ArrayList<>();

            for (TimeTable timeTable : timeTableListDaily) {
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                response.add(new TimeTableHourDto(null, false, sdf.format(timeTable.getFromTime()), sdf.format(timeTable.getTillTime()), HourTypeEnum.BOOKED));
//                for (TimeDto timeDto : generateResponseTime(sdf.format(new Date(timeTable.getFromTime().getTime())), sdf.format(new Date(timeTable.getTillTime().getTime())), durationTime)) {
//                }
            }

            for (AgentHourOff agentHourOff : agentHourOffListDaily) {
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                for (TimeDto timeDto : generateResponseTime(
                        sdf.format(new Date(agentHourOff.getFromHourOff().getTime())),
                        sdf.format(new Date(agentHourOff.getTillHourOff().getTime())),
                        durationTime)) {
                    response.add(new TimeTableHourDto(null, false,
                            timeDto.getFrom(),
                            timeDto.getTill(),
                            HourTypeEnum.BREAK)
                    );
                }
            }

            for (Hour hour : hourList) {
                for (TimeDto timeDto : generateResponseTime(hour.getFromTime(), hour.getTillTime(), durationTime)) {
                    response.add(new TimeTableHourDto(null, false,
                            timeDto.getFrom(),
                            timeDto.getTill(),
                            HourTypeEnum.FREE)
                    );
                }
            }

            return response;

        } catch (Exception e) {
            return null;
        }
    }

    public List<HourDto> getHourDtoList(UUID agentScheduleId) {
        return hourRepository.findAllByAgentSchedule_Id(agentScheduleId).stream().map(this::getHourDto).collect(Collectors.toList());
    }

    private List<TimeDto> generateResponseTime(String fromTime, String tillTime, LocalTime duration) {

        LocalTime from = LocalTime.parse(fromTime);
        LocalTime till = LocalTime.parse(tillTime);

        List<TimeDto> resList = new ArrayList<>();

        LocalTime res = till.minusHours(from.getHour()).minusMinutes(from.getMinute());

        int count = 1;

        boolean hourHave = duration.getHour() > 0;

        int minute = duration.getMinute();
        int hour = duration.getHour();

        while (count > 0) {

            if (hourHave && res.getHour() >= hour) {
                res = res.withHour(res.getHour() - hour);
                if (res.getMinute() >= minute || res.getHour() >= hour) {
                    res = res.minusMinutes(minute);
                } else {
                    if (res.getMinute() > 0) {
                        from = from.plusHours(hour);
                        resList.add(new TimeDto(from.toString(), from.plusMinutes(res.getMinute()).toString()));
                    }
                    break;
                }
                resList.add(new TimeDto(from.toString(), from.plusHours(hour).plusMinutes(minute).toString()));
                from = from.plusHours(hour).plusMinutes(minute);
                continue;
            }

            if (res.getHour() >= hour && res.getMinute() >= minute) {
                res = res.minusMinutes(minute);
                resList.add(new TimeDto(from.toString(), from.plusMinutes(minute).toString()));
                from = from.plusMinutes(minute);
            } else if (hour < 1 && res.getHour() > hour || res.getMinute() > minute && minute > 0) {
                res = res.minusMinutes(minute);
                resList.add(new TimeDto(from.toString(), from.plusMinutes(minute).toString()));
                from = from.plusMinutes(minute);
            } else {
                if (res.getMinute() > 0) {
                    resList.add(new TimeDto(from.toString(), from.plusMinutes(res.getMinute()).toString()));
                }
                break;
            }
        }
        return resList;
    }

}
