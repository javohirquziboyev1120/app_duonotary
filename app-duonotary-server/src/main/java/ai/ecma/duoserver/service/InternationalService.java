package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.International;
import ai.ecma.duoserver.entity.Order;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.InternationalDto;
import ai.ecma.duoserver.repository.CountryRepository;
import ai.ecma.duoserver.repository.DocumentTypeRepository;
import ai.ecma.duoserver.repository.InternationalRepository;
import ai.ecma.duoserver.repository.OrderRepository;
import ai.ecma.duoserver.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InternationalService {
    @Autowired
    InternationalRepository internationalRepository;
    @Autowired
    CountryRepository countryRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    DocumentTypeRepository documentTypeRepository;

    public ApiResponse addInternational(InternationalDto internationalDto, Order order) {
        try {
            International international = new International();
            Optional<International> byOrder = internationalRepository.findByOrder(order);
            if (byOrder.isPresent()) international = byOrder.get();
            international.setOrder(order);
            internationalRepository.save(makeInternational(internationalDto, international));
            return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
        } catch (Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }
    }


    public ApiResponse editInternational(InternationalDto internationalDto) {
        try {
            Optional<International> byId = internationalRepository.findById(internationalDto.getId());
            if (!byId.isPresent()) return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
            internationalRepository.save(makeInternational(internationalDto, byId.get()));
            return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
        } catch (Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }
    }


    public International makeInternational(InternationalDto internationalDto, International international) {
        if (internationalDto.getDocumentType() != null && internationalDto.getDocumentType().getId() != null)
            international.setDocumentType(documentTypeRepository.findById(internationalDto.getDocumentType().getId()).orElseThrow(ResourceNotFoundException::new));
        if (internationalDto.isSomeOneElse()) {
            international.setRequesterFirstName(internationalDto.getRequesterFirstName());
            international.setRequesterLastName(internationalDto.getRequesterLastName());
            international.setRequesterPhoneNumber(internationalDto.getRequesterPhoneNumber());
            international.setRequesterEmail(internationalDto.getRequesterEmail());
        } else {
            international.setRequesterFirstName(null);
            international.setRequesterLastName(null);
            international.setRequesterPhoneNumber(null);
            international.setRequesterEmail(null);
        }
        international.setEmbassy(internationalDto.isEmbassy());
        international.setPickUpAddress(internationalDto.getPickUpAddress());
        international.setCountry(countryRepository.findById(internationalDto.getCountry().getId()).orElseThrow(ResourceNotFoundException::new));
        international.setNumberDocument(internationalDto.getNumberDocument());
        international.setSomeOneElse(internationalDto.isSomeOneElse());
        return international;
    }

    public InternationalDto getInternational(International international) {
        return new InternationalDto(
                international.getId(),
                international.isEmbassy(),
                international.isSomeOneElse(),
                international.getDocumentType(),
                international.getRequesterFirstName(),
                international.getRequesterLastName(),
                international.getRequesterPhoneNumber(),
                international.getRequesterEmail(),
                international.getNumberDocument(),
                international.getPickUpAddress(),
                international.getCountry(),
                null
        );
    }
}
