package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.*;
import ai.ecma.duoserver.entity.enums.OrderStatus;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.ResPageable;
import ai.ecma.duoserver.payload.SharingDiscountDto;
import ai.ecma.duoserver.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class SharingAndLoyaltyDiscountService {
    @Autowired
    SharingDiscountRepository sharingDiscountRepository;
    @Autowired
    SharingDiscountTariffRepository sharingDiscountTariffRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    DiscountExpenseRepository discountExpenseRepository;
    @Autowired
    LoyaltyDiscountRepository loyaltyDiscountRepository;
    @Autowired
    CheckRole checkRole;
    @Autowired
    ServicePriceRepository servicePriceRepository;

    /**
     * Klient order uchun to'lov qilish jarayonida chegirmadan foydalanishi uchun chiqariladigan jami chegirmasi
     *
     * @param client
     * @return
     */
    public double getDiscountAmountSpendForOrder(User client) {
        Double sharingDiscountLeftover = sharingDiscountRepository.getUserSharingDiscountLeftover(client.getId());
        Double userLoyaltyDiscountLeftover = loyaltyDiscountRepository.getUserLoyaltyDiscountLeftover(client.getId());
        return sharingDiscountLeftover + userLoyaltyDiscountLeftover;
    }

    /**
     * sharing discountni qo'shish uchun orderni completed bo'lganda chaqirilib qo'yiladigan method
     *
     * @param order
     * @return boolean
     */
    public boolean addSharingDiscount(Order order) {
        if (order.getOrderStatus().equals(OrderStatus.COMPLETED)) {
            if (order.getClient().getSharingUser() != null) {
                if (checkRole.isAgent(order.getClient().getSharingUser().getRoles())) {
                    sharingDiscountRepository.save(new SharingDiscount(
                            order.getClient().getSharingUser(),
                            0.0,
                            10.0,
                            10.0,
                            order
                    ));
                } else {
                    double sharingDiscountTariff = 0.0;
                    double getSharingDiscountTariff = 0.0;
                    if (order.getServicePrice().getService().getMainService().isOnline()) {
                        Optional<SharingDiscountTariff> sharingDiscountTariffOptional = sharingDiscountTariffRepository.findByOnlineIsTrueAndActive(true);
                        if (sharingDiscountTariffOptional.isPresent()) {
                            getSharingDiscountTariff = sharingDiscountTariffOptional.get().getPercent();
                            sharingDiscountTariff = ((order.getAmount() / 100) * sharingDiscountTariffOptional.get().getPercent());
                        }
                    } else {
                        Optional<SharingDiscountTariff> sharingDiscountTariffOptional = sharingDiscountTariffRepository.findByZipCodeAndActive(order.getZipCode(), true);
                        if (sharingDiscountTariffOptional.isPresent()) {
                            getSharingDiscountTariff = sharingDiscountTariffOptional.get().getPercent();
                            sharingDiscountTariff = ((order.getAmount() / 100) * sharingDiscountTariffOptional.get().getPercent());
                        }
                    }
                    sharingDiscountRepository.save(new SharingDiscount(
                            order.getClient().getSharingUser(),
                            getSharingDiscountTariff,
                            sharingDiscountTariff,
                            sharingDiscountTariff,
                            order
                    ));
                }
                order.getClient().setSharingUser(null);
                userRepository.save(order.getClient());
                return true;
            }
        }
        return false;
    }

    /**
     * Agar complete bo'lgan order bekor qilinsa, order bekor qilinayotgan methodga chaqirish uchun sharing discountni bekor qilish uchun mehtod
     *
     * @param id
     * @return
     */
    public boolean cancelSharingDiscount(UUID id) {
        Optional<SharingDiscount> optionalSharingDiscount = sharingDiscountRepository.findById(id);
        if (optionalSharingDiscount.isPresent()) {
            SharingDiscount sharingDiscount = optionalSharingDiscount.get();
            boolean isUsed = discountExpenseRepository.existsByDiscountIdAndSharingId(sharingDiscount.getId(), true);
            if (!isUsed) {
                sharingDiscountRepository.deleteById(sharingDiscount.getId());
                return true;
            }
        }
        return false;
    }


    /**
     * userning sharing discounti, foydalanish uchun yetadimi yo'qmi tekshirish methodi
     *
     * @param user
     * @param leftover
     * @return
     */
    public boolean isReach(User user, Double leftover) {
        Double allLeftover = sharingDiscountRepository.getUserSharingDiscountLeftover(user.getId());
        return allLeftover >= leftover;
    }

    /**
     * userning sharing discountlarini dollarda umumiy qiymatini olish uchun
     *
     * @param user
     * @return
     */

    public double getUserSharingDiscount(User user) {
        return sharingDiscountRepository.getUserSharingDiscountLeftover(user.getId());
    }


    /**
     * user discountdan foydalanmoqchi bo'lganida, orderni to'lov qismida foydalaniladigan method
     *
     * @param client
     * @param needDiscountAmount
     * @return
     */
    public boolean useDiscount(User client, double needDiscountAmount) {
        double clientDiscountAmount = this.getDiscountAmountSpendForOrder(client);//clientning umumiy discountni
        if (clientDiscountAmount >= needDiscountAmount) {
            List<Object[]> discountObjectList = sharingDiscountRepository.getClientDiscount(client.getId());
            double needLeftover = clientDiscountAmount;
            for (Object[] discountObj : discountObjectList) {//id, date_time, user_id, amount,leftover, bool
                double ourLeftover = Double.parseDouble(discountObj[4].toString());
                String isLoyalty = discountObj[5].toString();
                UUID id = UUID.fromString(discountObj[0].toString());
                if (ourLeftover < needLeftover) {
                    needLeftover = needLeftover - ourLeftover;
                    if (isLoyalty.equals("true")) {
                        loyaltyDiscountRepository.updateLoyaltyDiscount(0.0, id);
                    } else {
                        sharingDiscountRepository.updateSharingDiscount(0.0, id);
                    }
                } else {
                    ourLeftover = ourLeftover - needLeftover;
                    if (isLoyalty.equals("true")) {
                        loyaltyDiscountRepository.updateLoyaltyDiscount(ourLeftover, id);
                    } else {
                        sharingDiscountRepository.updateSharingDiscount(ourLeftover, id);
                    }
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * sharing discountlar haqida ma'lumot olish uchun
     *
     * @param page
     * @param size
     * @param user
     * @return
     */
    public ResPageable getSharingDiscountPage(int page, int size, User user) {
        Page<SharingDiscount> sharingDiscountPage;
        if (user != null) {//user null bo'lmasa, user uchun berilayotgan bo'ladi ma'lumot
            sharingDiscountPage = sharingDiscountRepository.findAllByClient_Id(user.getId(), PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt"))));
        } else {//user null bo'lsa admin uchun berilayotgan bo'ladi ma'lumot
            sharingDiscountPage = sharingDiscountRepository.findAll(PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt"))));
        }
        return new ResPageable(page, size, sharingDiscountPage.getTotalPages(),
                sharingDiscountPage.getTotalElements(),
                sharingDiscountPage.getContent());
    }

    /**
     * Admin uchun bitta sharing discountni olish uchun
     *
     * @param id
     * @param user
     * @return
     */
    public SharingDiscount getSharingDiscount(UUID id, User user) {
        Optional<SharingDiscount> sharingDiscount = Optional.of(new SharingDiscount());
        if (user != null) {
            sharingDiscount = sharingDiscountRepository.findByClientAndId(user, id);
        } else {
            sharingDiscount = sharingDiscountRepository.findById(id);
        }
        return sharingDiscount.orElseGet(SharingDiscount::new);
    }

    public SharingDiscount makeShare(SharingDiscountDto sharingDiscountDto) {
        return new SharingDiscount(
//                sharingDiscountDto.getClientId(),
//                sharingDiscountDto.getPercent(),
//                sharingDiscountDto.getLeftover(),
//                sharingDiscountDto.getOrderId()
        );
    }

    public ai.ecma.duoserver.payload.ApiResponse resetAgentDiscount(String email, double amount) {
        List<SharingDiscount> agentDiscounts = sharingDiscountRepository.getSharingDiscountByAgent(email);
        double needAmount = amount;
        if (agentDiscounts.size() > 0) {
            double need = 0.0;
            for (SharingDiscount agentDiscount : agentDiscounts) {
                need += agentDiscount.getLeftover();
            }
            if (need >= needAmount) {
                for (SharingDiscount agentDiscount : agentDiscounts) {
                    if (agentDiscount.getLeftover() >= needAmount) {
                        agentDiscount.setLeftover(agentDiscount.getLeftover() - needAmount);
                        sharingDiscountRepository.save(agentDiscount);
                        discountExpenseRepository.save(new DiscountExpense(needAmount, agentDiscount.getId(), true));
                        needAmount = 0.0;
                        return new ApiResponse("Successfully!", true);
                    } else {
                        agentDiscount.setLeftover(0.0);
                        sharingDiscountRepository.save(agentDiscount);
                        discountExpenseRepository.save(new DiscountExpense(agentDiscount.getLeftover(), agentDiscount.getId(), true));
                        needAmount = needAmount - agentDiscount.getLeftover();
                    }
                }
                return new ApiResponse("Successfully!", true);
            }
            return new ApiResponse("Discount amount not enough!", false);
        } else {
            return new ApiResponse("This agent cannot find discount!", false);
        }

    }


}
