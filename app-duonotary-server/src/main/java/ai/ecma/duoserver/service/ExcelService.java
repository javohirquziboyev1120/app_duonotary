package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.Attachment;
import ai.ecma.duoserver.entity.AttachmentContent;
import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.entity.enums.AttachmentType;
import ai.ecma.duoserver.entity.enums.RoleName;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.OrderPdf;
import ai.ecma.duoserver.payload.UserPdf;
import ai.ecma.duoserver.repository.*;
import ai.ecma.duoserver.utils.AppConstants;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

@Service
public class ExcelService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PdfService pdfService;
    @Autowired
    OutOfServiceRepository outOfServiceRepository;
    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;


    String[] userColumns = {"#", "First Name", "Last Name", "Email", "PhoneNumber", "IsActive", "Count Order", "Online", "Role", "Online Agent", "Enabled"};
    String[] orderColumns = {"#", "Client Full Name", "Client Email", "Agent Full Name", "Agent Email", "Address", "Zipcode", "Amount", "Amount Discount", "Pay type", "Service", "Check Number", "Serial Number", "Status", "Count Document", "Active", "Pay Status", "Title Document"};

    public List<Map<Integer, String>> makeUser(List<UserPdf> userPdfList) {
        List<Map<Integer, String>> object = new ArrayList<>();
        int rowNum = 1;
        for (UserPdf agent : userPdfList) {
            Map<Integer, String> forObject = new HashMap<>();
            forObject.put(0, String.valueOf(rowNum++));
            forObject.put(1, agent.getFirstName());
            forObject.put(2, agent.getLastName());
            forObject.put(3, agent.getEmail());
            forObject.put(4, agent.getPhoneNumber());
            forObject.put(5, agent.getActive());
            forObject.put(6, ""+orderRepository.countUserOrders(agent.getId()));
            forObject.put(7, agent.getOnline());
            forObject.put(8, agent.getRoleName());
            forObject.put(9,agent.getOnlineAgent());
            forObject.put(10, agent.getEnabled() );
            object.add(forObject);
        }
        return object;
    }

    public List<Map<Integer, String>> makeOrder(List<OrderPdf> orderPdfList) {
        List<Map<Integer, String>> object = new ArrayList<>();
        int rowNum = 1;
        for (OrderPdf order : orderPdfList) {
            Map<Integer, String> forObject = new HashMap<>();
            forObject.put(0, String.valueOf(rowNum++));
            forObject.put(1, order.getClientFullName());
            forObject.put(2, order.getClientEmail());
            forObject.put(3, order.getAgentFullName());
            forObject.put(4, order.getAgentEmail());
            forObject.put(5, order.getAddress());
            forObject.put(6, order.getZipCode());
            forObject.put(7, order.getAmount().toString());
            forObject.put(8, "" + order.getAmountDiscount());
            forObject.put(9, order.getPayType());
            forObject.put(10, order.getService());
            forObject.put(11, order.getCheckNumber());
            forObject.put(12, order.getSerialNumber());
            forObject.put(13, order.getOrderStatus());
            forObject.put(14, "" + order.getCountDocument());
            forObject.put(15, "" + order.getActive());
            forObject.put(16, order.getOrderPayStatusEnum());
            forObject.put(17, order.getTitleDocument());
            object.add(forObject);
        }
        return object;
    }

    public HttpEntity<?> downloadAgentList() {
        return generateExcel(userColumns, makeUser(pdfService.getUserList(userRepository.getUsersForPdf(RoleName.ROLE_AGENT.name()))), generateDate("AgentList"), generateDate(" AgentList"));
    }
    public HttpEntity<?> downloadCustomerList() {
        return generateExcel(userColumns, makeUser(pdfService.getUserList(userRepository.getUsersForPdf(RoleName.ROLE_USER.name()))), generateDate("CustomerList"), generateDate(" CustomerList"));
    }

    public HttpEntity<?> downloadOrderListByAgent(UUID id) {
        Optional<User> byId = userRepository.findById(id);
        if(!byId.isPresent())return ResponseEntity.status(409).body(new ApiResponse(AppConstants.ID_NOT_FOUND,false));
        return generateExcel(orderColumns, makeOrder(pdfService.getOrderList(orderRepository.findAllByAgent(byId.get()))), byId.get().getFirstName()+" "+byId.get().getLastName()+" Order List", generateDate(byId.get().getFirstName()+" "+byId.get().getLastName()+" OrderList"));
    }

    public HttpEntity<?> downloadOrderList() {
        return generateExcel(orderColumns, makeOrder(pdfService.getOrderList(orderRepository.findAll())), generateDate(" Order List"), generateDate(" OrderList"));
    }



    /**
     * Bu universal excel yaratadigan method
     *
     * @param columns          bunga columnlarni nomi,
     * @param object           objectni map ga o'rab <key = column number, value= yozilishi kerak bolgan narsani String tipida>,
     * @param listName         excelda list ni nomi,
     * @param downloadFileName download bolgandagi fileni nomi,
     * @return CertificateDto
     */
    private HttpEntity<?> generateExcel(String[] columns, List<Map<Integer, String>> object, String listName, String downloadFileName) {
        Workbook workbook = new XSSFWorkbook();
        CreationHelper creationHelper = workbook.getCreationHelper(); //workbookni xamma formatlarini qo'llab quvvatlaydi
        Sheet sheet = workbook.createSheet(listName);
        Font font = workbook.createFont();
        font.setBold(true);
        font.setColor(IndexedColors.BLUE.getIndex());
        font.setFontHeightInPoints((short) 16);
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(font);
        Row row = sheet.createRow(0);
        for (int i = 0; i < columns.length; i++) {
            Cell cell = row.createCell(i);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(columns[i]);
        }
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(creationHelper.createDataFormat().getFormat("dd-MM-yyyy"));
        int rowNum = 1;
        for (Map<Integer, String> map : object) {
            Row row1 = sheet.createRow(rowNum++);
            map.forEach((integer, value) -> {
                row1.createCell(integer).setCellValue(value);
            });
        }
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        String encode=null;
        try {
            workbook.write(byteArrayOutputStream);
            byteArrayOutputStream.close();
            encode= Base64.encodeBase64String(byteArrayOutputStream.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert encode != null;
        Attachment attachment = new Attachment(
                downloadFileName+".xlsx",
                encode.length(),
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                AttachmentType.RENDER
        );
        Attachment savedAttachment = attachmentRepository.save(attachment);

        AttachmentContent attachmentContent = new AttachmentContent(savedAttachment, byteArrayOutputStream.toByteArray());
        attachmentContentRepository.save(attachmentContent);
        return ResponseEntity.ok(savedAttachment.getId());
    }
    public String generateDate(String name){
            return name+" "+new Timestamp(System.currentTimeMillis()).toString().substring(0,11);
    }

}
