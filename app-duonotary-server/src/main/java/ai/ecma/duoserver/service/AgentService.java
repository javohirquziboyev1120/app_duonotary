package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.*;
import ai.ecma.duoserver.entity.enums.OrderStatus;
import ai.ecma.duoserver.entity.enums.RoleName;
import ai.ecma.duoserver.entity.enums.StatusEnum;
import ai.ecma.duoserver.entity.enums.TimeEnum;
import ai.ecma.duoserver.entity.template.AbsEntity;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.*;
import ai.ecma.duoserver.repository.*;
import ai.ecma.duoserver.utils.AppConstants;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AgentService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    AgentOnlineOfflineTimeRepository agentOnlineOfflineTimeRepository;
    @Autowired
    CertificateRepository certificateRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    StateRepository stateRepository;
    @Autowired
    PassportRepository passportRepository;
    @Autowired
    ZipCodeRepository zipCodeRepository;
    @Autowired
    UserZipCodeRepository userZipCodeRepository;
    @Autowired
    MailSenderService mailSenderService;
    @Autowired
    UserService userService;
    @Autowired
    AgentLocationRepository agentLocationRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    TimeTableRepository timeTableRepository;
    @Autowired
    AuthService authService;


    /**
     * Agent Certificate add qilish uchun method
     *
     * @param certificateDtos malumot
     * @param user            current  user
     * @return ApiResponse object=null
     */
    public ApiResponse addCertificates(List<CertificateDto> certificateDtos, User user) {
        try {
            Optional<User> userOptional = userRepository.findById(certificateDtos.get(0).getUserDto() == null ? user.getId() : certificateDtos.get(0).getUserDto().getId());
            if (!userOptional.isPresent()) return new ApiResponse("User not found", false);
            boolean isExist = false;
            for (CertificateDto certificateDto : certificateDtos) {
                if (certificateRepository.existsByUserAndState_IdAndExpired(userOptional.get(), certificateDto.getStateDto().getId(), false))
                    isExist = true;
            }
            if (isExist) return new ApiResponse("You had a certificate in this state", false);
            certificateRepository.saveAll(certificateDtos.stream().map(certificateDto -> makeCertificate(certificateDto, user, new Certificate())).collect(Collectors.toList()));
            return new ApiResponse("saved all Certificates", true);
        } catch (Exception e) {
            return new ApiResponse("an error occurred during storage", false);
        }
    }

    /**
     * Passport ni saqlash uchun method
     *
     * @param certificateDto certificateDto
     * @param user           user
     * @return ApiResponse object=null
     */
    public ApiResponse addPassport(CertificateDto certificateDto, User user) {
        try {
//            if (passportRepository.existsByExpiredAndUser_Id(false, certificateDto.getUserDto() == null ? user.getId() : certificateDto.getUserDto().getId()))
//                return new ApiResponse("You had a  passport, it is not expired", false);
            passportRepository.save(makePassport(certificateDto, user, new Passport()));
            return new ApiResponse("saved", true);
        } catch (Exception e) {
            return new ApiResponse("an error occurred during storage", false);
        }
    }

    /**
     * CertificateDtodan Certificatega o'tkazadigan method
     *
     * @param certificateDto dto
     * @param user           user
     * @return Certificate
     */
    public Certificate makeCertificate(CertificateDto certificateDto, User user, Certificate certificate) {
        if (user.getRoles().stream().anyMatch(role -> role.getRoleName().equals(RoleName.ROLE_AGENT))) {
            certificate.setUser(user);
            certificate.setStatusEnum(StatusEnum.PENDING);
        } else {
            certificate.setUser(userRepository.findById(certificateDto.getUserDto().getId()).orElseThrow(() -> new ResourceNotFoundException("getUser", "UserId", certificateDto.getUserDto().getId())));
            certificate.setStatusEnum(certificateDto.getStatusEnum());
        }
        certificate.setAttachment(certificateDto.getAttachmentId() == null ? certificate.getAttachment() : attachmentRepository.findById(certificateDto.getAttachmentId()).orElseThrow(() -> new ResourceNotFoundException("getCertificate", "id", certificateDto.getAttachmentId())));
        certificate.setIssueDate(certificateDto.getIssueDate());
        certificate.setExpireDate(certificateDto.getExpireDate());
        certificate.setState(stateRepository.findById(certificateDto.getStateDto().getId()).orElseThrow(() -> new ResourceNotFoundException("getCounty", "id", certificateDto.getStateDto().getId())));
        certificate.setExpired(false);
        return certificate;
    }

    /**
     * Passport ni Edit uchun method
     *
     * @param certificateDto dto
     * @param user           user
     * @return ApiResponse object=null
     */
    public ApiResponse editPassport(CertificateDto certificateDto, User user) {
        try {
            Optional<Passport> passportOptional = passportRepository.findById(certificateDto.getId());
            if (passportOptional.isPresent()) {
                passportRepository.save(makePassport(certificateDto, user, passportOptional.get()));
                return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
            }
            return new ApiResponse(AppConstants.ID_NOT_FOUND, false);
        } catch (Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }
    }

    /**
     * Passport ni Edit uchun method
     *
     * @param certificateDto dto
     * @param user           user
     * @return ApiResponse object=null
     */
    public ApiResponse editCertificate(CertificateDto certificateDto, User user) {
        try {
            Optional<Certificate> certificateOptional = certificateRepository.findById(certificateDto.getId());
            if (certificateOptional.isPresent()) {
                certificateRepository.save(makeCertificate(certificateDto, user, certificateOptional.get()));
                return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
            }
            return new ApiResponse(AppConstants.ID_NOT_FOUND, false);
        } catch (Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }
    }

    /**
     * CertificateDtodan Passportga o'tkazadigan method
     *
     * @param certificateDto dto
     * @param user           user
     * @return Passport
     */
    public Passport makePassport(CertificateDto certificateDto, User user, Passport passport) {
        if (user.getRoles().stream().anyMatch(role -> role.getRoleName().equals(RoleName.ROLE_AGENT)) || user.getId() == null) {
            passport.setUser(user);
            passport.setStatusEnum(StatusEnum.PENDING);
        } else {
            passport.setUser(userRepository.findById(certificateDto.getUserDto().getId()).orElseThrow(() -> new ResourceNotFoundException("getUser", "UserId", certificateDto.getUserDto().getId())));
            passport.setStatusEnum(certificateDto.getStatusEnum());
        }
        passport.setAttachment(certificateDto.getAttachmentId() == null ? passport.getAttachment() : attachmentRepository.findById(certificateDto.getAttachmentId()).orElseThrow(() -> new ResourceNotFoundException("getCertificate", "id", certificateDto.getAttachmentId())));
        passport.setIssueDate(certificateDto.getIssueDate());
        passport.setExpireDate(certificateDto.getExpireDate());
        passport.setExpired(false);
        return passport;
    }

    /**
     * Agent Id orqali agentni barcha certificatelarni olish uchun method
     *
     * @param userId userId
     * @return List<CertificateDto>
     */
    public List<CertificateDto> getCertificatesByUserId(UUID userId) {
        return certificateRepository.findAllByUser_IdOrderByCreatedAtDesc(userId).stream().map(this::getCertificate).collect(Collectors.toList());
    }

    /**
     * Documentni status enumni o'zgartiruvchi method
     *
     * @param documentId id qarab passport mi certificate mi farqi yoq
     * @param statusEnum status
     * @return apiResponse
     */
    public ApiResponse editStatus(UUID documentId, StatusEnum statusEnum, String description) throws MessagingException, IOException, TemplateException {
        if (statusEnum.equals(StatusEnum.REJECTED) && description.length() < 5)
            return new ApiResponse("description is Null", false);
        Optional<Passport> passportOptional = passportRepository.findById(documentId);
        if (!passportOptional.isPresent()) {
            Optional<Certificate> certificateOptional = certificateRepository.findById(documentId);
            if (certificateOptional.isPresent()) {
                Certificate certificate = certificateOptional.get();
                certificate.setStatusEnum(statusEnum);
                if (statusEnum.equals(StatusEnum.RECEIVED))
                    changeActiveUserZipcode(true, certificate);
                if (statusEnum.equals(StatusEnum.REJECTED) || statusEnum.equals(StatusEnum.EXPIRED))
                    changeActiveUserZipcode(false, certificate);
                new Thread(() -> {
                    try {
                        mailSenderService.sendAgentOnStatusChanged(getCertificate(certificateRepository.save(certificate)), description);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }).start();
                return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
            }
            return new ApiResponse(AppConstants.USER_SELECTED_NONE, false);
        }
        passportOptional.get().setStatusEnum(statusEnum);

        new Thread(() -> {
            try {
                mailSenderService.sendAgentOnStatusChanged(getPassport(passportRepository.save(passportOptional.get())), description);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
        return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
    }

    /**
     * Certificate ni CertificateDto ga o'rgiruvchi method
     *
     * @param certificate certificate
     * @return CertirficateDto dto
     */
    public CertificateDto getCertificate(Certificate certificate) {
        return new CertificateDto(
                certificate.getId(),
                certificate.getAttachment().getId(),
                userService.getUser(certificate.getUser()),
                new StateDto(certificate.getState().getId(), certificate.getState().getName()),
                certificate.getIssueDate(),
                certificate.getExpireDate(),
                certificate.getStatusEnum(),
                certificate.isExpired()
        );
    }


    /**
     * Passport ni CertificateDto ga o'rgiruvchi method
     *
     * @param passport passport
     * @return CertirficateDto CertirficateDto
     */
    public CertificateDto getPassport(Passport passport) {
        return new CertificateDto(
                passport.getId(),
                passport.getAttachment() == null ? null : passport.getAttachment().getId(),
                userService.getUser(passport.getUser()),
                null,
                passport.getIssueDate(),
                passport.getExpireDate(),
                passport.getStatusEnum(),
                passport.isExpired()
        );
    }

    /**
     * Agentning sertificatedagi state_id bo`yicha zipCode tabledan mos zipCodelarni listini olib beradi;
     *
     * @param stateId stateId
     * @return List<zipCode
     */
    public List<ZipCode> getZipCodes(UUID stateId) {
        return zipCodeRepository.findAllByStateAndActive(stateId);
    }

    /**
     * Statusga qarab passportlarni olib keladi
     *
     * @param status status
     * @return List<dto
     */
    public List<CertificateDto> getByStatusPassports(StatusEnum status) {
        return passportRepository.findAllByStatusEnum(status).stream().map(this::getPassport).collect(Collectors.toList());
    }

    /**
     * Statusga qarab certificatelar olib keladi
     *
     * @param status status
     * @return List<dto
     */
    public List<CertificateDto> getByStatusCertificates(StatusEnum status) {
        return certificateRepository.findAllByStatusEnum(status).stream().map(this::getCertificate).collect(Collectors.toList());
    }

    /**
     * Agent id orqali  passportlarni olish
     *
     * @param userId userId
     * @return CertificateDto
     */
    public List<CertificateDto> getPassportsByUser(UUID userId) {
        return passportRepository.findAllByUser_IdOrderByCreatedAtDesc(userId).stream().map(this::getPassport).collect(Collectors.toList());
    }

    /**
     * Agentni onlineni o'zgartirish
     *
     * @param userId   agent id
     * @param aBoolean qaysiga uzgartirish
     * @return apiResponse
     */
    public ApiResponse changeAgentOnline(UUID userId, Boolean aBoolean) {
        try {
            Optional<User> userOptional = userRepository.findById(userId);
            if (userOptional.isPresent()) {
                userOptional.get().setOnlineAgent(aBoolean);
                userRepository.save(userOptional.get());
                return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
            }
            return new ApiResponse(AppConstants.ID_NOT_FOUND, false);
        } catch (Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }
    }

    /**
     * Agentni Admin active yoki deactive qiladi
     * states bu zipcodlarni userZipCode tableda true qilamiz!
     *
     * @param agentId  id
     * @param statesId satates
     * @return ApiResponse
     */
    public ApiResponse activateOrDeactivate(UUID agentId, List<UUID> statesId) throws MessagingException, IOException, TemplateException {
        Optional<User> optionalUser = userRepository.findById(agentId);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            if (user.getRoles().stream().noneMatch(role -> role.getRoleName().equals(RoleName.ROLE_AGENT)))
                return new ApiResponse("This user not agent", false);
            user.setActive(!user.isActive());
            userRepository.save(user);
            if (!user.isActive()) {
                setUserZipCode(user);
                new Thread(() -> {
                    try {
                        mailSenderService.sendAgentChangeActive(user, !user.isActive());
                    } catch (MessagingException | IOException | TemplateException e) {
                        System.err.println(e.getMessage());
                        ;
                    }
                }).start();
            }

            return new ApiResponse(AppConstants.SUCCESS_EDITED, true);
        }
        return new ApiResponse("User not found", false);
    }

    public void setUserZipCode(User agent) {
        for (UserZipCode userZipCode : userZipCodeRepository.findAllByUser_id(agent.getId())) {
            userZipCode.setEnable(false);
            userZipCode.setActive(false);
            userZipCodeRepository.save(userZipCode);
        }
    }

    public void setExpired() {
        List<Certificate> certificates = new ArrayList<>();
        for (Certificate certificate : certificateRepository.findAllByExpiredForCertificate()) {
            certificate.setExpired(true);
            certificate.setStatusEnum(StatusEnum.EXPIRED);
            certificates.add(certificate);
        }
        certificateRepository.saveAll(certificates);
        List<Passport> passports = new ArrayList<>();
        for (Passport passport : passportRepository.findAllByExpiredForPassport()) {
            passport.setExpired(true);
            passport.setStatusEnum(StatusEnum.EXPIRED);
            passports.add(passport);
        }
        passportRepository.saveAll(passports);
    }

    public UserDto getAgent(User user) {
        List<CertificateDto> certificatesByUserId = getCertificatesByUserId(user.getId());
        return new UserDto(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getPhoneNumber(),
                user.getEmail(),
                user.getPhoto() == null ? null : user.getPhoto().getId(),
                certificatesByUserId,
                getPassportsByUser(user.getId()),
                user.getUpdatedAt(),
                user.isActive(),
                getAgentLocation(agentLocationRepository.findByAgentIdAndCreatedAt(user.getId()).orElse(new AgentLocation())),
                orderRepository.countUserOrders(user.getId()),
                user.getOnlineAgent(),
                agentOnlineOfflineTimeRepository.getOnlineAgentHour(user.getId())
        );
    }

    public UserDto getAgentForList(User user) {
        return new UserDto(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getPhoneNumber(),
                user.getEmail(),
                user.getPhoto() == null ? null : user.getPhoto().getId(),
                user.getUpdatedAt(),
                user.isActive(),
                getAgentLocation(agentLocationRepository.findByAgentIdAndCreatedAt(user.getId()).orElse(new AgentLocation())),
                orderRepository.countUserOrders(user.getId()),
                user.getOnlineAgent(),
                agentOnlineOfflineTimeRepository.getOnlineAgentHour(user.getId())
        );
    }

    /**
     * Admin agentni edit qiladigan method faqat adminga
     *
     * @param userDto agentni malumotlari
     * @return ApiResponse
     */
    public ApiResponse editAgent(UserDto userDto) {
        try {
            Optional<User> agentOptional = userRepository.findById(userDto.getId());
            if (!agentOptional.isPresent()) return new ApiResponse(AppConstants.ID_NOT_FOUND, false);
            User save = userRepository.save(makeAgent(agentOptional.get(), userDto));
            new Thread(() -> {
                try {
                    mailSenderService.sendAgentEditYourInfo(save);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
            return new ApiResponse(AppConstants.SUCCESS_EDITED, true);
        } catch (Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }

    }

    /**
     * agent o'zni edit qiladigan method
     *
     * @param userDto agentni malumotlari
     * @return ApiResponse
     */

    public ApiResponse editAgentOwn(UserDto userDto, User user) {
        try {
            ApiResponse apiResponse = authService.checkEmailAndPhoneNumberForEdit(userDto, user.getId());
            if (!apiResponse.isSuccess()) return apiResponse;
            user.setEmail(userDto.getEmail());
            user.setPhoneNumber(userDto.getPhoneNumber());
            userRepository.save(user);
            return new ApiResponse(AppConstants.SUCCESS_EDITED, true);
        } catch (Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }
    }

    public User makeAgent(User user, UserDto userDto) {
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setOnlineAgent(userDto.getOnlineAgent());
        user.setPhoto(userDto.getPhotoId() == null ? null : attachmentRepository.findById(userDto.getPhotoId()).orElse(null));
        return user;
    }

    public AgentLocationDto getAgentLocation(AgentLocation agentLocation) {
        return new AgentLocationDto(
                null,
                agentLocation.getLat(),
                agentLocation.getLon(),
                agentLocation.getCreatedAt(),
                agentLocation.getZipCode(),
                agentLocation.getFullLocation()
        );
    }

    /**
     * Agentni onlineligini  uzgartirish
     *
     * @param agentId id
     * @param online  boolean
     * @return apiresponse
     */
    public ApiResponse changeOnlineAgent(UUID agentId, Boolean online) {
        try {
            Optional<User> agentOptional = userRepository.findById(agentId);
            if (!agentOptional.isPresent()) return new ApiResponse(AppConstants.ID_NOT_FOUND, false);
            agentOptional.get().setOnlineAgent(online);
            userRepository.save(agentOptional.get());
            return new ApiResponse(AppConstants.SUCCESS_EDITED, true);
        } catch (Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }

    }

    /**
     * userzipcodeni aktive  ni uzgarturuvch metod
     *
     * @param isActive
     * @param certificate
     */
    public void changeActiveUserZipcode(boolean isActive, Certificate certificate) {
        for (UserZipCode userZipCode : userZipCodeRepository.getAllByUserAndState(certificate.getUser().getId(), certificate.getState().getId())) {
            userZipCode.setEnable(isActive);
            userZipCode.setActive(false);
            userZipCodeRepository.save(userZipCode);
        }
    }

    /**
     * Agentlarni listini olish
     *
     * @param page page
     * @param size size
     * @return Pageable
     */
    public ResPageable getAgents(int page, int size) {
        Page<User> pages = userRepository.findAllByEnabledAndRolesIn(true, roleRepository.findAllByRoleName(RoleName.ROLE_AGENT), PageRequest.of(page, size, Sort.Direction.DESC, "createdAt"));
        return new ResPageable(page, size, pages.getTotalPages(), pages.getTotalElements(),
                pages.getContent().stream().map(this::getAgentForList).collect(Collectors.toList()));
    }

    /**
     * Agent filter qilib listni olish
     *
     * @param zipCode zipcode
     * @param date    vaqt
     * @return list
     */
    public Object getAgentsFilter(String zipCode, Date date, int page, int size) {
        ResPageable resPageable = new ResPageable();
        resPageable.setPage(page);
        resPageable.setSize(size);
        if (zipCode != null && date != null) {
            resPageable.setObject(userRepository.getAgentsByZipCodeAndOrderDate(zipCode, date, page, size).stream().map(this::getAgent).collect(Collectors.toList()));
            Long lon = userRepository.countAgentsByZipCodeAndOrderDate(zipCode, date);
            resPageable.setTotalElements(lon);
            resPageable.setTotalPages((int) (lon / size));
            return resPageable;
        } else if (zipCode != null) {
            resPageable.setObject(userRepository.getAgentsByZipCode(zipCode, page, size).stream().map(this::getAgent).collect(Collectors.toList()));
            Long lon = userRepository.countAgentsByZipCode(zipCode);
            resPageable.setTotalElements(lon);
            resPageable.setTotalPages((int) (lon / size));
            return resPageable;
        } else if (date != null) {
            resPageable.setObject(userRepository.getAgentsByOrderDate(date, page, size).stream().map(this::getAgent).collect(Collectors.toList()));
            Long lon = userRepository.countAgentsByOrderDate(date);
            resPageable.setTotalElements(lon);
            resPageable.setTotalPages((int) (lon / size));
            return resPageable;
        }
        return getAgents(Integer.parseInt(AppConstants.DEFAULT_PAGE_NUMBER), Integer.parseInt(AppConstants.DEFAULT_PAGE_NUMBER));
    }

    /**
     * Agent filter qilib listni olish
     *
     * @param search text
     * @return list
     */
    public Object getAgentsBySearch(String search, int page, int size) {
        Long aLong = userRepository.countAllCustomersBySearch(search, RoleName.ROLE_AGENT.name());
        return new ResPageable(
                page, size, (int) (aLong / size), aLong, userRepository.getAllCustomersBySearch(search.toLowerCase(), RoleName.ROLE_AGENT.name(), page, size).stream().map(this::getAgentForList).collect(Collectors.toList()));
    }

    public ApiResponse changeEnableUserZipCode(UUID id) {
        Optional<UserZipCode> byId = userZipCodeRepository.findById(id);
        if (!byId.isPresent()) return new ApiResponse(AppConstants.ID_NOT_FOUND, false);
        byId.get().setEnable(!byId.get().isEnable());
        byId.get().setActive(false);
        userZipCodeRepository.save(byId.get());
        return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
    }

    public ApiResponse changeActiveUserZipCode(UUID id) {
        Optional<UserZipCode> byId = userZipCodeRepository.findById(id);
        if (!byId.isPresent()) return new ApiResponse(AppConstants.ID_NOT_FOUND, false);
        if (!byId.get().isEnable()) return new ApiResponse("First the administrator must give permission", false);
        byId.get().setActive(!byId.get().isActive());
        userZipCodeRepository.save(byId.get());
        return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
    }

    public ApiResponse addUserZipCode(List<UUID> zipcodeId, User user) {
        List<UserZipCode> allByUser_id = userZipCodeRepository.findAllByUser_id(user.getId());
        zipcodeId.forEach(uuid -> {
            if (allByUser_id.stream().anyMatch(userZipCode -> userZipCode.getZipCode().getId() == uuid))
                zipcodeId.remove(uuid);
        });
        zipCodeRepository.findAllByIdIn(zipcodeId).forEach(zipCode -> {
            userZipCodeRepository.save(new UserZipCode(zipCode, user, false, false));
        });
        return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
    }

    public List<UserZipCode> getUserZipCodeList(User user) {
        return userZipCodeRepository.findAllByUser_id(user.getId()).stream().map(this::getUserZipCode).collect(Collectors.toList());
    }

    public List<ZipCode> getUserZipCodeListForAdd(User user) {
        List<UUID> states = new ArrayList<>();
        certificateRepository.findAllByUser_IdOrderByCreatedAtDesc(user.getId()).forEach(certificate -> {
            if (certificate.getStatusEnum() == StatusEnum.RECEIVED) states.add(certificate.getState().getId());
        });
        List<ZipCode> zipcodes = new ArrayList<>();
        List<ZipCode> allByStateIdesIn = zipCodeRepository.findAllByStateIdesIn(states);
        List<UserZipCode> allByUser_id = userZipCodeRepository.findAllByUser_id(user.getId());
        for (ZipCode zipCode : allByStateIdesIn) {
            if (allByUser_id.stream().noneMatch(userZipCode -> zipCode.equals(userZipCode.getZipCode()))) {
                zipcodes.add(zipCode);
            }
        }
        return zipcodes;
    }

    public UserZipCode getUserZipCode(UserZipCode userZipCode) {
        if (userZipCode == null && userZipCode.getId() == null) return new UserZipCode();
        userZipCode.setUser(null);
        return userZipCode;
    }

    public CertificateDto getCertificateByZipCode(UUID userId, UUID zipCode) {
        return getCertificate(certificateRepository.getByZipCodeAndUser(userId, zipCode));
    }

    public Object getReportForAgent(User user, TimeEnum timeEnum) {
        Date fromTime = new Date(System.currentTimeMillis());
        Date tillTime = new Date(System.currentTimeMillis());
        Calendar calendar = new GregorianCalendar(tillTime.getYear(), tillTime.getMonth(), tillTime.getDate());
        if (timeEnum != null && timeEnum == TimeEnum.WEEK) {
            fromTime.setDate(tillTime.getDate() - 7);
        } else if (timeEnum != null && timeEnum == TimeEnum.MONTH) {
            fromTime.setDate(tillTime.getDate() - calendar.getActualMaximum(Calendar.DATE));
            fromTime.getDate();
        }
        List<Object[]> reportForAgent = userRepository.getReportForAgent(user.getId(), OrderStatus.COMPLETED.name(), fromTime, tillTime);
        return new ReportDto(
                reportForAgent.get(0)[0],
                reportForAgent.get(0)[1],
                reportForAgent.get(0)[2],
                reportForAgent.get(0)[3]
        );
    }
}
