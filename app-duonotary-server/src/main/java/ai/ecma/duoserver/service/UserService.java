package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.*;
import ai.ecma.duoserver.entity.enums.RoleName;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.ResPageable;
import ai.ecma.duoserver.payload.UserDto;
import ai.ecma.duoserver.repository.*;
import ai.ecma.duoserver.utils.AppConstants;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    AuthService authService;
    @Autowired
    PermissionRepository permissionRepository;
    @Autowired
    MailSenderService mailSenderService;
    @Autowired
    UserZipCodeRepository userZipCodeRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ZipCodeRepository zipCodeRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    ZipCodeService zipCodeService;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    AgentOnlineOfflineTimeRepository agentOnlineOfflineTimeRepository;
    @Autowired
    AttachmentService attachmentService;


    /**
     * ADMINNI REGISTRATSIYA QILISH UCHUN ISHLATILADI
     *
     * @param userDto
     * @return
     * @throws MessagingException
     * @throws IOException
     * @throws TemplateException
     */
    public ApiResponse addAdmin(UserDto userDto) throws MessagingException, IOException, TemplateException {

        //EMAIL APHONE NUMBERLARNI TEKSHIRISH
        userDto.setEmail(userDto.getEmail().toLowerCase());
        ApiResponse checkEmailAndPhoneNumber = authService.checkEmailAndPhoneNumber(userDto);
        if (!checkEmailAndPhoneNumber.isSuccess())
            return checkEmailAndPhoneNumber;

        UUID emailCode = UUID.randomUUID();

        //DEFAULT PAROL
        userDto.setPassword(AppConstants.DEFAULT_PASSWORD);
        //AMDIN SIFATIDA USER YASAB OLISH
        User admin = authService.makeUser(userDto, true, emailCode, 1);

        //ADMINGA SUPER ADMIN BELGILAGAN HUQUQLARNI BIRIKTIRISH
        admin.setPermissions(new HashSet<>(permissionRepository.findAllById(userDto.getPermissionsId())));

        if (userDto.getStatesId().isEmpty() && userDto.getCountiesId().isEmpty() && userDto.getZipCodesId().isEmpty())
            return new ApiResponse("Error!", false);
        //ADMINNI SAQLAB, BITTA O'ZGARUVCHIGA OLISH
        User saveAdmin = userRepository.save(admin);

        //USER STO DAN KELGAN ZIP CODE ID ORQALI USERZIPCODE TABLEGA SAQLASH
        userZipCodeRepository.saveAll(makeUserZipCodeList(userDto, saveAdmin));

        //user EMAIL GA XAT YUBORISH
//        mailSenderService.sendEmailForVerification(emailCode, userDto, false);
        return new ApiResponse("Congratulation successfully registrated. Admin have to check email address.", true);
    }

    /**
     * user ma'lumotlarini o'zgartrish
     *
     * @param userDto
     * @param user
     * @return
     * @throws MessagingException
     * @throws IOException
     * @throws TemplateException
     */
    public ApiResponse editUser(UserDto userDto, User user) throws MessagingException, IOException, TemplateException {
        try {
            userDto.setEmail(userDto.getEmail().toLowerCase());
            ApiResponse checkEmailAndPhoneNumberForEdit = authService.checkEmailAndPhoneNumberForEdit(userDto, user.getId());
            if (!checkEmailAndPhoneNumberForEdit.isSuccess())
                return checkEmailAndPhoneNumberForEdit;

            boolean emailChanging = !user.getEmail().equals(userDto.getEmail());

            user.setFirstName(userDto.getFirstName());
            user.setLastName(userDto.getLastName());
            user.setPhoneNumber(userDto.getPhoneNumber());

            if (emailChanging && userDto.getId().equals(user.getId())) {
                user.setChangingEmail(userDto.getEmail());
                UUID emailCode = UUID.randomUUID();
                user.setEmailCode(emailCode.toString());
                mailSenderService.sendEmailForVerification(emailCode, userDto, true);
            } else if (emailChanging) {
                user.setEmail(userDto.getEmail());
            }
            // agar admin boshqa userni ro'yhatdan o'tkazayotgan bo'lsa, o'zining akkauntini o'zgartirib qo'ymasligi uchun
            if (userDto.getId() != null && !user.getId().equals(userDto.getId())) {
                user.setId(userDto.getId());
            }
            //user agar photosi bo'lsa o'rnatamiz. Aks holda null ni qo'yamiz
            if (userDto.getPhotoId() != null)
                user.setPhoto(attachmentRepository.findById(userDto.getPhotoId()).orElseThrow(() -> new ResourceNotFoundException("getPhoto", "id", userDto.getPhotoId())));
            userRepository.save(user);
            return new ApiResponse("Successfully edited!", true);
        } catch (Exception e) {
            return new ApiResponse("Error in edited!", false);
        }
    }

    /**
     * parolni o'zgartirish
     *
     * @param userDto
     * @param user
     * @return
     */
    public ApiResponse editPassword(UserDto userDto, User user) {
        ApiResponse checkPassword = authService.checkPassword(userDto);
        if (!checkPassword.isSuccess())
            return checkPassword;
        ApiResponse checkOldPasswordEqual = checkOldPasswordEqual(userDto, user);
        if (!checkOldPasswordEqual.isSuccess())
            return checkOldPasswordEqual;
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userRepository.save(user);
        return new ApiResponse("Successfuly edited", true);
    }

    /**
     * user bazadagi paroli va old passwordni solishitirish uchun
     *
     * @param userDto
     * @param user
     * @return
     */
    public ApiResponse checkOldPasswordEqual(UserDto userDto, User user) {
        boolean matches = passwordEncoder.matches(userDto.getOldPassword(), user.getPassword());
        return new ApiResponse((matches ? "" : "Old password wrong"), matches);
    }

    /**
     * userlarni enabled o'zgartirish. Agar "admin" true bo'lsa o'zgarayotgan user admin bo'lmasa bu yo'lga kirishga ruxsat yo'q
     *
     * @param userId
     * @param admin
     * @return
     */
    public ApiResponse changeEnabledUser(UUID userId, boolean admin) {
        Optional<User> optionalUser = userRepository.findById(userId);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            if (admin && user.getRoles().stream().noneMatch(role -> role.getRoleName().equals(RoleName.ROLE_ADMIN)))
                return new ApiResponse("You have no access this path", false);
            user.setEnabled(!user.isEnabled());
            userRepository.save(user);
            return new ApiResponse("Successfully " + (user.isEnabled() ? "activated" : "deactivated"), true);
        }
        return new ApiResponse("User not found", false);
    }

    /**
     * adminning huquqlarini o'zgartirish uchun
     *
     * @param userDto
     * @return
     */
    public ApiResponse editAdminPermission(UserDto userDto) {
        Optional<User> optionalUser = userRepository.findById(userDto.getId());
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setPermissions(new HashSet<>(permissionRepository.findAllById(userDto.getPermissionsId())));
            userRepository.save(user);
            return new ApiResponse("Successfully edited", true);
        }
        return new ApiResponse("User not found", false);
    }

    /**
     * adminning zip code larini o'zgartirish uchun
     *
     * @param userDto
     * @return
     */
    public ApiResponse editAdminZipCode(UserDto userDto) {
        Optional<User> optionalUser = userRepository.findById(userDto.getId());
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            userZipCodeRepository.deleteAllByUserId(userDto.getId());
            userZipCodeRepository.saveAll(makeUserZipCodeList(userDto, user));
            return new ApiResponse("Successfully edited", true);
        }
        return new ApiResponse("User not found", false);
    }

    /**
     * userDto asosida userzipcode listini yasab berish uchun
     *
     * @param userDto
     * @param user
     * @return
     */
    public List<UserZipCode> makeUserZipCodeList(UserDto userDto, User user) {
        List<ZipCode> zipCodeByDtoList = zipCodeService.getZipCodeByDtoList(userDto.getStatesId(), userDto.getCountiesId(), userDto.getZipCodesId());
        return zipCodeByDtoList.stream().map(zipCode -> makeUserZipCode(zipCode, user)).collect(Collectors.toList());
    }

    /**
     * userDto asosida userzipcode objectini yasab berish uchun
     *
     * @param zipCode
     * @param user
     * @return
     */
    public UserZipCode makeUserZipCode(ZipCode zipCode, User user) {
        return new UserZipCode(
                zipCode,
                user
        );
    }

    public UserDto getUser(User user) {
        return new UserDto(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getPhoneNumber(),
                user.getEmail(),
                user.getPhoto() == null ? null : user.getPhoto().getId());
    }

    public List<UserDto> getUsersByZipCode(UUID id) {
        return userRepository.findAllAgentByZipCode(id, RoleName.ROLE_AGENT.name()).stream().map(this::getUser).collect(Collectors.toList());
    }

    public ResPageable getCustomers(int page, int size) {
        Page<User> pages = userRepository.findAllByEnabledAndRolesIn(true, roleRepository.findAllByRoleName(RoleName.ROLE_USER), PageRequest.of(page, size));
        return new ResPageable(page, size, pages.getTotalPages(), pages.getTotalElements(),
                pages.getContent().stream().map(this::getUser).collect(Collectors.toList()));
    }

    public ApiResponse removePhoto(UUID id) {
        try {
            Optional<User> byId = userRepository.findById(id);
            if (!byId.isPresent()) return new ApiResponse(AppConstants.ID_NOT_FOUND, false);
            attachmentService.deleteAttachment(byId.get().getPhoto().getId());
            byId.get().setPhoto(null);
            userRepository.save(byId.get());
            return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
        } catch (Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }
    }

    public ApiResponse addPhoto(UUID id, UUID attach) {
        try {
            Optional<User> byId = userRepository.findById(id);
            if (!byId.isPresent()) return new ApiResponse(AppConstants.ID_NOT_FOUND, false);
            Optional<Attachment> attachmentOptional = attachmentRepository.findById(attach);
            if (!attachmentOptional.isPresent()) return new ApiResponse(AppConstants.ID_NOT_FOUND, false);
            byId.get().setPhoto(attachmentOptional.get());
            userRepository.save(byId.get());
            return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
        } catch (Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }
    }

    public ApiResponse editCustomerName(UserDto userDto) {
        try {
            User user = userRepository.findById(userDto.getId()).orElseThrow(() -> new ResourceNotFoundException("getUser", "id", userDto.getId()));
            user.setFirstName(userDto.getFirstName());
            user.setLastName(userDto.getLastName());
            userRepository.save(user);
            return new ApiResponse("Success", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse changeOnlineAndOffline(User user) {
        agentOnlineOfflineTimeRepository.save(
                new AgentOnlineOfflineTime(
                        user,
                        !user.isOnline()));
        userRepository.changeOnlineAndOffline(user.getId());
        return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
    }

    public Object getUsersBySearch(String search,int page,int size) {
        return userRepository.findAllCustomerWithSearch(search.toLowerCase(), "ROLE_USER",page,size);
    }
}
