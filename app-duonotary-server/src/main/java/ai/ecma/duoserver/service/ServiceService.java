package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.MainService;
import ai.ecma.duoserver.entity.Service;
import ai.ecma.duoserver.entity.ServicePrice;
import ai.ecma.duoserver.entity.SubService;
import ai.ecma.duoserver.entity.enums.ServiceEnum;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.*;
import ai.ecma.duoserver.repository.MainServiceRepository;
import ai.ecma.duoserver.repository.ServicePriceRepository;
import ai.ecma.duoserver.repository.ServiceRepository;
import ai.ecma.duoserver.repository.SubServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
public class ServiceService {
    @Autowired
    ServiceRepository serviceRepository;
    @Autowired
    ServicePriceRepository servicePriceRepository;
    @Autowired
    MainServiceRepository mainServiceRepository;

    @Autowired
    SubServiceRepository subServiceRepository;

    @Autowired
    CheckUser checkUser;

    public List<ServiceDto> getExistServiceList() {
        List<ServicePrice> serviceIn = servicePriceRepository.findAllByServiceIn(serviceRepository.findAll());
        List<Optional<Service>> optionalList = serviceIn.stream().map(servicePrice -> serviceRepository.findById(servicePrice.getService().getId())).collect(Collectors.toList());
        return optionalList.stream().map(service -> getService(service.get())).collect(Collectors.toList());
    }

    public List<Service> getServiceList(){
        return serviceRepository.findAll();
    }

    public ApiResponse deleteService(UUID serviceId, UUID subServiceId) {
        try {
            serviceRepository.deleteById(serviceId);
            List<Service> allBySubServiceId = serviceRepository.findAllBySubService_Id(subServiceId);
            if (allBySubServiceId.size() < 1)
                subServiceRepository.deleteById(subServiceId);
            return new ApiResponse("Deleted", true);
        } catch (Exception e) {
            return new ApiResponse("Error in removal", false);
        }
    }

    /**
     * SERVICELARNI PAGE QAYTARISH
     *
     * @param page
     * @param size
     * @return
     */
    public ApiResponse getServicesPage(Integer page, Integer size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Service> servicePage = serviceRepository.findAll(pageable);
            return new ApiResponse(true, new ResPageable(page, size, servicePage.getTotalPages(), servicePage.getTotalElements(), servicePage.getContent().stream().map(this::getService).collect(Collectors.toList())));
        } catch (IllegalArgumentException e) {
            return new ApiResponse("Error", false);
        }
    }

    /**
     * Servicelar pageni Userlarga berish uchun
     *
     * @param page
     * @param size
     * @return
     */
    public ApiResponse getServicesPageForUser(Integer page, Integer size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Service> servicePage = serviceRepository.findAllByActive(true, pageable);
            return new ApiResponse(true, new ResPageable(page, size, servicePage.getTotalPages(), servicePage.getTotalElements(), servicePage.getContent().stream().map(this::getService).collect(Collectors.toList())));
        } catch (IllegalArgumentException e) {
            return new ApiResponse("Error", false);
        }
    }

    /**
     * Serviceni activeni almashtiish uchun
     *
     * @return
     */
    public ApiResponse serviceActive(UUID serviceId) {
        try {
            Optional<Service> priceOptional = serviceRepository.findById(serviceId);
            if (priceOptional.isPresent()) {
                Service service = priceOptional.get();
                service.setActive(!service.isActive());
                serviceRepository.save(service);
                return new ApiResponse("Successfully edit active  service price", true);
            }
            return new ApiResponse("service price not found", false);

        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    /**
     * Serviceni dynamicligini o'zgartirish uchun
     *
     * @return
     */
    public ApiResponse serviceDynamic(ServiceDto serviceDto) {
        try {
            Service service = serviceRepository.findById(serviceDto.getId()).orElseThrow(() -> new ResourceNotFoundException("getService", "id", serviceDto.getId()));
            service.setDynamic(serviceDto.isDynamic());
            serviceRepository.save(service);
            return new ApiResponse("Done", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    /**
     * Serviceni bittasi olish uchun
     *
     * @param service
     * @return
     */
    public ServiceDto getService(Service service) {
        return new ServiceDto(
                service.getId(),
                getMainService(service.getMainService()),
                getSubServiceDto(service.getSubService()),
                service.getInitialCount(),
                service.getInitialSpendingTime(),
                service.getEveryCount(),
                service.getEverySpendingTime(),
                service.isDynamic(),
                service.isActive()
        );
    }

    /**
     * Main serviceni 1 tasi olish uchun
     *
     * @param mainService
     * @return
     */
    public MainServiceDto getMainService(MainService mainService) {
        return new MainServiceDto(
                mainService.getId(),
                mainService.getName(),
                mainService.getFromTime(),
                mainService.getTillTime(),
                mainService.isOnline()
        );
    }

    /**
     * subserviceni 1 tasi olish uchun
     *
     * @param subService
     * @return
     */
    public SubServiceDto getSubServiceDto(SubService subService) {
        return new SubServiceDto(
                subService.getId(),
                subService.getName(),
                subService.isDefaultInput(),
                subService.isActive(),
                subService.getDescription(),
                subService.getServiceEnum()
        );
    }

    public ApiResponse firstService() {
        Optional<Service> bySubService_defaultInput = serviceRepository.findBySubService_DefaultInput(true);
        if (!bySubService_defaultInput.isPresent()) {
            Optional<MainService> mainServiceOptional = mainServiceRepository.findByActiveAndOnline(true, true);
            if (mainServiceOptional.isPresent()) {
                MainService mainService = mainServiceOptional.get();
                SubService subService1 = firstSubService();
                Optional<SubService> byDefaultInput = subServiceRepository.findByDefaultInput(true);
                if (byDefaultInput.isPresent()) {
                    SubService subService = byDefaultInput.get();
                    Service service = new Service();
                    service.setMainService(mainService);
                    service.setSubService(subService);
                    service.setInitialCount(3);
                    service.setInitialSpendingTime(10);
                    service.setEveryCount(1);
                    service.setEverySpendingTime(3);
                    service.setDynamic(true);
                    service.setActive(true);
                    serviceRepository.save(service);
                    return new ApiResponse("Successfully added", true);
                }
                return new ApiResponse("Not found Sub Service added", false);
            }
            return new ApiResponse("Not found Main Service added", false);
        }
        return new ApiResponse("This Sevice already exist", false);
    }


    /**
     * BU FUNCTION SERVICE YASAB OLISH UCHUN
     *
     * @param serviceDto
     * @return
     */
    public ApiResponse addService(ServiceDto serviceDto) {
        SubService subService = makeSubService(serviceDto);
        List<UUID> mainServices = serviceDto.getMainServices();
        for (UUID mainService : mainServices) {
            Service service = new Service();
            service.setMainService(mainServiceRepository.findById(mainService).orElseThrow(() -> new ResourceNotFoundException("getMainService", "id", serviceDto.getMainServiceId())));
            service.setSubService(subService);
            service.setInitialCount(serviceDto.getInitialCount());
            service.setInitialSpendingTime(serviceDto.getInitialSpendingTime());
            service.setEveryCount(serviceDto.getEveryCount());
            service.setEverySpendingTime(serviceDto.getEverySpendingTime());
            service.setDynamic(serviceDto.isDynamic());
            service.setActive(serviceDto.isActive());
            serviceRepository.save(service);
        }
        return new ApiResponse("Successfully added", true);
    }

    public SubService firstSubService() {
        SubService subService = new SubService();
        subService.setName("Online");
        subService.setDefaultInput(true);
        subService.setDescription("Online service");
        subService.setActive(true);
        subService.setServiceEnum(ServiceEnum.OTHERS);
        return subServiceRepository.save(subService);
    }

    public SubService makeSubService(ServiceDto serviceDto) {
        Optional<SubService> optionalSubService = subServiceRepository.findByNameEqualsIgnoreCase(serviceDto.getName());
        if (optionalSubService.isPresent()) {
            SubService subService1 = optionalSubService.get();
            subService1.setName(serviceDto.getName());
            subService1.setServiceEnum(serviceDto.getServiceEnum());
            subService1.setActive(serviceDto.isActive());
            subService1.setDefaultInput(serviceDto.isDefaultInput());
            subService1.setDescription(serviceDto.getDescription());
            return subServiceRepository.save(subService1);
        } else if (serviceDto.getSubServiceId() != null || (serviceDto.getSubServiceDto()!=null)&& (serviceDto.getSubServiceDto().getId() != null)) {
            Optional<SubService> byId = subServiceRepository.findById(serviceDto.getSubServiceId() != null ? serviceDto.getSubServiceId() : serviceDto.getSubServiceDto().getId());
            if (byId.isPresent()) {
                SubService subService = byId.get();
                subService.setDescription(serviceDto.getDescription());
                return subServiceRepository.save(subService);
            }
        } else {
            SubService subService = new SubService();
            subService.setName(serviceDto.getName());
            subService.setServiceEnum(serviceDto.getServiceEnum());
            subService.setActive(serviceDto.isActive());
            subService.setDefaultInput(serviceDto.isDefaultInput());
            subService.setDescription(serviceDto.getDescription());
            return subServiceRepository.save(subService);
        }
        return subServiceRepository.save(new SubService());
    }

    public Service makeService(ServiceDto serviceDto, Service service) {
        service.setMainService(mainServiceRepository.findById(serviceDto.getMainServiceId() != null ? serviceDto.getMainServiceId() : serviceDto.getMainServiceDto().getId()).orElseThrow(() -> new ResourceNotFoundException("getMainService", "id", serviceDto.getMainServiceId())));
        service.setSubService(makeSubService(serviceDto));
        service.setInitialCount(serviceDto.getInitialCount());
        service.setInitialSpendingTime(serviceDto.getInitialSpendingTime());
        service.setEveryCount(serviceDto.getEveryCount());
        service.setEverySpendingTime(serviceDto.getEverySpendingTime());
        service.setDynamic(serviceDto.isDynamic());
        service.setActive(serviceDto.isActive());
        return service;
    }

    public ApiResponse editService(ServiceDto serviceDto) {
        try {
            Optional<Service> optional = serviceRepository.findById(serviceDto.getId());
            if (optional.isPresent()) {
                serviceRepository.save(makeService(serviceDto, optional.get()));
                return new ApiResponse("Successfully edited", true);
            }
            return new ApiResponse("This service is not found", false);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }
}

