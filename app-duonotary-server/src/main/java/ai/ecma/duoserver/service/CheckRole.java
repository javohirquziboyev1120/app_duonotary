package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.Role;
import ai.ecma.duoserver.repository.RoleRepository;
import org.springframework.stereotype.Service;

import java.util.Set;

import static ai.ecma.duoserver.entity.enums.RoleName.*;

@Service
public class CheckRole {
    final
    RoleRepository roleRepository;

    public CheckRole(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public boolean isROLE_SUPER_ADMIN(Set<Role> roles) {
        for (Role role : roles) {
            if (role.getRoleName() == ROLE_SUPER_ADMIN) {
                return true;
            }
        }
        return false;
    }

    public boolean isAdmin(Set<Role> roles) {
        for (Role role : roles) {
            if (role.getRoleName() == ROLE_ADMIN||role.getRoleName()==ROLE_SUPER_ADMIN) {
                return true;
            }
        }
        return false;
    }

    public boolean isAgent(Set<Role> roles) {
        for (Role role : roles) {
            if (role.getRoleName() == ROLE_AGENT) {
                return true;
            }
        }
        return false;
    }

    public boolean isUser(Set<Role> roles) {
        for (Role role : roles) {
            if (role.getRoleName() == ROLE_USER) {
                return true;
            }
        }
        return false;
    }

    public boolean isNone(Set<Role> roles) {
        for (Role role : roles) {
            if (role.getRoleName() == NONE) {
                return true;
            }
        }
        return false;
    }
}
