package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.FeedBack;
import ai.ecma.duoserver.entity.Order;
import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.entity.enums.OperationEnum;
import ai.ecma.duoserver.entity.enums.OrderStatus;
import ai.ecma.duoserver.entity.enums.RoleName;
import ai.ecma.duoserver.payload.*;
import ai.ecma.duoserver.repository.FeedBackRepository;
import ai.ecma.duoserver.repository.HistoryRepository;
import ai.ecma.duoserver.repository.OrderRepository;
import ai.ecma.duoserver.repository.UserRepository;
import ai.ecma.duoserver.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class FeedBackService {
    @Autowired
    FeedBackRepository feedBackRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    MailSenderService mailSenderService;
    @Autowired
    OrderService orderService;
    @Autowired
    HistoryRepository historyRepository;
    @Autowired
    CheckRole checkRole;

    /**
     * Bu methodda feedback qo'shiladi feedback mijoz va agent ni order haqida fikri boladi
     * Faqat Agent va Userga Mumkun
     *
     * @param feedBackDto feedBackni ma'lumotlari
     * @return ApiResponse object=Null
     */
    public ApiResponse addFeedBack(FeedBackDto feedBackDto, User user) {
        try {
            Optional<Order> optionalOrder = orderRepository.findById(feedBackDto.getOrderDto().getId());
            if (!optionalOrder.isPresent() || optionalOrder.get().getOrderStatus() == OrderStatus.DRAFT || optionalOrder.get().getOrderStatus() == OrderStatus.IN_PROGRESS || optionalOrder.get().getOrderStatus() == OrderStatus.NEW)
                return new ApiResponse("This operation is not valid", false);
            ApiResponse apiResponse = checkFeedBack(user, optionalOrder.get());
            if (!apiResponse.isSuccess()) return apiResponse;
            FeedBack feedBack = makeFeedBack(feedBackDto, new FeedBack());
            feedBack.setOrder(optionalOrder.get());
            if (feedBackDto.getRate() < AppConstants.NORMAl_RATE) {
                new Thread(() -> {
                    try {
                        mailSenderService.sendFeedBackForAdmin(feedBack, feedBackDto.getOperationEnum());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }).start();
            }
            feedBackRepository.save(feedBack);
            return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
        } catch (Exception e) {
            return new ApiResponse("an error occurred during storage", false);
        }
    }

    public ApiResponse checkFeedBack(User user, Order order) {
        boolean agent = feedBackRepository.existsByAgentTrueAndOrder(order);
        boolean client = feedBackRepository.existsByAgentFalseAndOrder(order);
        boolean isClient = user.getRoles().stream().anyMatch(role -> role.getRoleName() == RoleName.ROLE_USER);
        boolean isAgent = user.getRoles().stream().anyMatch(role -> role.getRoleName() == RoleName.ROLE_AGENT);
        if(isAgent&&client){
            order.setOrderStatus(OrderStatus.CLOSED);
            orderRepository.save(order);
        }
        else if(isClient&&agent){
            order.setOrderStatus(OrderStatus.CLOSED);
            orderRepository.save(order);
        }
        if (isAgent && agent) {
            return new ApiResponse("You have already feedback on this order", false);
        } else if (isClient && client) {
            return new ApiResponse("You have already feedback on this order", false);
        }
        return new ApiResponse("", true);
    }

    /**
     * FeedBackDto to FeedBack
     *
     * @param feedBackDto
     * @param feedBack
     * @return FeedBack
     */
    public FeedBack makeFeedBack(FeedBackDto feedBackDto, FeedBack feedBack) {
        feedBack.setSeen(feedBackDto.isSeen());
        feedBack.setAgent(feedBackDto.isAgent());
        feedBack.setRate(feedBackDto.getRate());
        feedBack.setDescription(feedBackDto.getDescription());
        return feedBack;
    }

    /**
     * Bu methodda feedback tahrirlandi feedback mijoz va agent ni order haqida fikri boladi
     * Faqat Agent va Userga Mumkun
     *
     * @param feedBackDto feedBackni ma'lumotlari
     * @return ApiResponse object=Null
     */
    public ApiResponse editFeedBack(FeedBackDto feedBackDto) {
        try {
            Optional<FeedBack> byId = feedBackRepository.findById(feedBackDto.getId());
            if (byId.isPresent()) {
                if (byId.get().isSeen()) {
                    return new ApiResponse("You could't edit,This feedback have been already  answered", false);
                }
                FeedBack feedBack = makeFeedBack(feedBackDto, byId.get());
                feedBack.setSeen(false);
                if (feedBack.getRate() < AppConstants.NORMAl_RATE)
                    new Thread(() -> {
                        try {
                            mailSenderService.sendFeedBackForAdmin(feedBack, feedBackDto.getOperationEnum());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }).start();
                feedBackRepository.save(feedBack);
                return new ApiResponse(AppConstants.SUCCESS_EDITED, true);
            }
            return new ApiResponse(AppConstants.ID_NOT_FOUND, false);
        } catch (Exception e) {
            return new ApiResponse("an error occurred during storage", false);
        }
    }

    /**
     * Admin notificationda feedBack korganda Bu yerga isSeen true qilgani keladi
     *
     * @param id Bunda feedBackni idsi keladi
     * @return ApiResponse
     */
    public ApiResponse seenAdminUnSeenFeedBack(UUID id) {
        try {
            Optional<FeedBack> byId = feedBackRepository.findById(id);
            byId.ifPresent(feedBack -> {
                feedBack.setSeen(true);
                feedBackRepository.save(feedBack);
            });
            return byId.isPresent() ? new ApiResponse(AppConstants.SUCCESS_MESSAGE, true) : new ApiResponse("an error occurred during storage", false);
        } catch (Exception e) {
            return new ApiResponse("an error occurred during storage", false);
        }
    }

    /**
     * FeedBack to FeedBackDto
     *
     * @param feedBack
     * @return
     */
    public FeedBackDto getFeedBack(FeedBack feedBack) {
        return new FeedBackDto(
                feedBack.getId(),
                orderService.makeOrderDtoForFeedBack(feedBack.getOrder()),
                feedBack.isAgent(),
                feedBack.getRate(),
                feedBack.getDescription(),
                feedBack.getAnswer(),
                feedBack.isSeen(),
                OperationEnum.GET
        );
    }

    /**
     * Bu methoda admini notificationga yangi yoki edit qilingan feedBacklar boradi
     *
     * @return List<FeedBackDto> feedBacklarni listi qaytadi
     */
    public List<FeedBackDto> forAdminNavigation() {
        return feedBackRepository.findAllBySeen(false).stream().map(this::getFeedBack).collect(Collectors.toList());
    }

    /**
     * Bu methoda hamma feedBacklar qaytriladi
     *
     * @param page
     * @param size
     * @return ResPageable
     */
    public ResPageable getFeedBackList(int size, int page, boolean isAgent) {
        Page<FeedBack> list = feedBackRepository.findAllByAgent(isAgent, PageRequest.of(page, size, Sort.by(AppConstants.ASC_OR_DECK, "seen")));
        return new ResPageable(page, size, list.getTotalPages(), list.getTotalElements(), list.getContent().stream().map(
                this::getFeedBack
        ).collect(Collectors.toList()));
    }

    /**
     * Bu admin feedback yuborgan userga javob yubboradi ,   feedbackni seen=true  boladi
     *
     * @param feedBackDto
     * @return ApiResponse
     */
    public ApiResponse answerFeedBack(FeedBackDto feedBackDto) {
        Optional<FeedBack> feedBackOptional = feedBackRepository.findById(feedBackDto.getId());
        if (!feedBackOptional.isPresent()) {
            return new ApiResponse(AppConstants.ID_NOT_FOUND, false);
        }
        User user = feedBackOptional.get().isAgent() ? feedBackOptional.get().getOrder().getAgent() : feedBackOptional.get().getOrder().getClient();
        new Thread(() -> {
            try {
                mailSenderService.sendAnswerFeedBack(user.getLastName() + " " + user.getFirstName(), feedBackDto.getDescription(), user.getEmail());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
        feedBackOptional.get().setSeen(true);
        feedBackOptional.get().setAnswer(feedBackDto.getAnswer());
        feedBackRepository.save(feedBackOptional.get());
        return new ApiResponse("The message was sent success0fully", true);
    }

    /**
     * user id orqali feedbacklarni olish current user role ni tekishirish uchun kerak
     *
     * @param user
     * @return list
     */
    public ResPageable getFeedBacksByUserId(User user, int page, int size) {
        Page<FeedBack> list = null;
        if (checkRole.isAgent(user.getRoles()))
            list = feedBackRepository.findAllByAgentAndOrder_Agent(true, user, PageRequest.of(page, size));
        else
            list = feedBackRepository.findAllByAgentAndOrder_Client(false, user, PageRequest.of(page, size));

        return new ResPageable(page, size, list.getTotalPages(), list.getTotalElements(),
                list.getContent().stream().map(this::getFeedBack).collect(Collectors.toList())
        );

    }

    public ResPageable getAgentFeedBacksMaxRate(User user, int page, int size) {
        Page<FeedBack> orderAgent = feedBackRepository.findAllByAgentAndRateGreaterThanAndOrder_Agent(false, AppConstants.NORMAl_RATE, user, PageRequest.of(page, size));
        return new ResPageable(page, size, orderAgent.getTotalPages(), orderAgent.getTotalElements(),
                orderAgent.getContent().stream().map(this::getFeedBack).collect(Collectors.toList())
        );
    }

    public ApiResponse getFeedBackMobile(User user, Date date, boolean client, int page, int size) {
        try {
            ResPageable resPageable = new ResPageable(page, size, null, null, null);
            if (!client) {
                if (date == null) {
                    List<FeedBackSortDto> feedBackSortDtos = new ArrayList<>();
                    for (Date createdAt : feedBackRepository.getAllDateByCreatedBy(user.getId(), page, size)) {
                        ArrayList<FeedBackDto> dtos = new ArrayList<>();
                        int i = 0;
                        for (FeedBack feedBack : feedBackRepository.findAllByCreatedAtLimit(user.getId(), createdAt)) {
                            if (i < 3) dtos.add(getFeedBack(feedBack));
                            i++;
                        }
                        feedBackSortDtos.add(new FeedBackSortDto(createdAt, dtos));
                    }
                    resPageable.setObject(feedBackSortDtos);
                } else {
                    resPageable.setObject(feedBackRepository.findAllByCreatedAtAndDate(user.getId(), date).stream().map(this::getFeedBack).collect(Collectors.toList()));
                }
            } else {
                if (date == null) {
                    List<FeedBackSortDto> feedBackSortDtos = new ArrayList<>();
                    for (Date createdAt : feedBackRepository.getClientsFeedbackByAgent(user.getId(), page, size, 4)) {
                        ArrayList<FeedBackDto> dtos = new ArrayList<>();
                        int i = 0;
                        for (FeedBack feedBack : feedBackRepository.getClientsFeedbackByDateLimit(user.getId(), createdAt, 4)) {
                            if (i < 3) dtos.add(getFeedBack(feedBack));
                            i++;
                        }
                        feedBackSortDtos.add(new FeedBackSortDto(createdAt, dtos));
                    }
                    resPageable.setObject(feedBackSortDtos);
                } else {
                    resPageable.setObject(feedBackRepository.getClientsFeedbackByDate(user.getId(), date, 4).stream().map(this::getFeedBack).collect(Collectors.toList()));
                }
            }
            return new ApiResponse("Success", true, resPageable);
        } catch (Exception e) {
            return new ApiResponse("Serverda hatolik", false);
        }

    }
}


