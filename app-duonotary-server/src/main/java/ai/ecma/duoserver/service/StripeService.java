package ai.ecma.duoserver.service;

import ai.ecma.duoserver.repository.OrderRepository;
import ai.ecma.duoserver.repository.PaymentRepository;
import com.stripe.Stripe;
import org.springframework.stereotype.Service;

@Service
public class StripeService {
    final
    UserService userService;

    final OrderRepository orderRepository;
    final PaymentRepository paymentRepository;

    private static final String secretKey = "sk_test_51H7X5PAggaXTCRgQh41DKwgQ4L6xF6WaKzKqXCczqSyiTpDVT2tq6KRxw08OonllGuJPqy3EEmk8SFYy2m3ERinP00O8f7M1Ws";

    public StripeService(UserService userService, OrderRepository orderRepository, PaymentRepository paymentRepository) {
        this.orderRepository = orderRepository;
        this.paymentRepository = paymentRepository;
        Stripe.apiKey = secretKey;
        this.userService = userService;
    }

//    public String createCustomer(User user) {
//        Map<String, Object> customerParams = new HashMap<>();
//        customerParams.put("name",
//                user.getFirstName() + " " + user.getLastName());
//        customerParams.put("email", user.getEmail());
//        String id = null;
//
//        try {
//            // Create customer
//            Customer stripeCustomer = Customer.create(customerParams);
//            id = stripeCustomer.getId();
//            System.out.println(stripeCustomer);
//        } catch (CardException e) {
//            // Transaction failure
//        } catch (RateLimitException e) {
//            // Too many requests made to the API too quickly
//        } catch (InvalidRequestException e) {
//            // Invalid parameters were supplied to Stripe's API
//        } catch (AuthenticationException e) {
//            // Authentication with Stripe's API failed (wrong API key?)
//        }
////        catch (APIConnectionException e) {
////            // Network communication with Stripe failed
////        }
//        catch (StripeException e) {
//            // Generic error
//        } catch (Exception e) {
//            // Something else happened unrelated to Stripe
//        }
//        return id;
//    }
//
//    public void chargeCreditCard(Order order) {
//        // Stripe requires the charge amount to be in cents
//        int chargeAmountCents = (order.getAmount().intValue()) * 100;//TODO ORDERNING HAQIQIY SUMMASINI CHIQARISH KERAK
//
//        User user = order.getClient();
//
//        Map<String, Object> chargeParams = new HashMap<>();
//        chargeParams.put("amount", chargeAmountCents);
//        chargeParams.put("currency", "usd");
//        chargeParams.put("description", "Monthly Charges");
//        chargeParams.put("customer", user.getStripeCustomerId());
//
//        try {
//            // Submit charge to credit card
//            Charge charge = Charge.create(chargeParams);
//            System.out.println(charge);
//        } catch (CardException e) {
//            // Transaction was declined
//            System.out.println("Status is: " + e.getCode());
//            System.out.println("Message is: " + e.getMessage());
//        } catch (RateLimitException e) {
//            // Too many requests made to the API too quickly
//        } catch (InvalidRequestException e) {
//            // Invalid parameters were supplied to Stripe's API
//        } catch (AuthenticationException e) {
//            // Authentication with Stripe's API failed (wrong API key?)
//        }
//        //catch (APIConnectionException e) {
//        // Network communication with Stripe failed
//        //}
//        catch (StripeException e) {
//            // Generic error
//        } catch (Exception e) {
//            // Something else happened unrelated to Stripe
//        }
//    }
//
//    public void testStrip() {
//        User user = new User();
//
//        user.setEmail("sirojiddin1466@gmail.com");
//        user.setFirstName("Sirojiddin");
//        user.setLastName("Egamqulov");
//
//// Create Stripe customer
//        String id = createCustomer(user);
//
//// Assign ID to user
//        user.setStripeCustomerId(id);
//        user.setPassword("0211");
//        user.setPhoneNumber("+999");
//
//// Save user to database
//        User saveUser = new User();
//
//        Order order = new Order();
//        order.setAmount(10.50);
//        order.setClient(saveUser);
//
//        chargeCreditCard(order);
//    }
//
//
//    //Bu Method orqali orderId va stripe_Token_Id kelsa Future  Charge amalga oshiriladi
//    public ApiResponse getFutureCharge(UUID orderId, String tokenId) {
//        try {
//            Order order = orderRepository.findById(orderId).orElseThrow(() -> new ResourceNotFoundException("order", "id", orderId));
//            ChargeCreateParams params = ChargeCreateParams.builder()
//                    .setAmount((long)(order.getAmount()*100))
//                    .setCurrency("usd")
//                    .setDescription("Example charge")
//                    .setSource(tokenId)
//                    .setCapture(false)
//                    .build();
//
//            Charge charge = Charge.create(params);
//
//            Payment payment = new Payment();
//            payment.setOrder(order);
//            payment.setPaySum(order.getAmount());
//            payment.setPayType(order.getPayType());
////            payment.setPayStatus();
//            payment.setChargeId(charge.getId());
//            paymentRepository.save(payment);
//            return new ApiResponse("Ok", true, charge.getId());
//        } catch (StripeException e) {
//            return new ApiResponse("Error", false, null);
//        }
//    }
//
//    //Bu Method orqali order complate boganda orderId berib yuborilsa shu order uchun future charge qilingan bold=sa pul yechib olinadi.
//    public ApiResponse getCaptureByOrderId(UUID orderId){
//        try {
//            Order order = orderRepository.findById(orderId).orElseThrow(() -> new ResourceNotFoundException("order", "id", orderId));
//            Optional<Payment> optionalPayment = paymentRepository.findByOrder(order);
//            if (optionalPayment.isPresent()){
//                Payment payment = optionalPayment.get();
//                if (payment.getChargeId()!=null){
//                    Charge charge = Charge.retrieve(payment.getChargeId());
//                    charge.capture();
//                    return new ApiResponse("Ok", true);
//                }
//            }
//            return new ApiResponse("Error", false, null);
//        } catch (Exception e) {
//            return new ApiResponse("Error", false, null);
//        }
//    }
}