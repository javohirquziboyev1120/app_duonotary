package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.History;
import ai.ecma.duoserver.entity.HistoryOrder;
import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.entity.enums.OperationEnum;
import ai.ecma.duoserver.payload.ResHistory;
import ai.ecma.duoserver.payload.ResPageable;
import ai.ecma.duoserver.repository.HistoryOrderRepository;
import ai.ecma.duoserver.repository.HistoryRepository;
import ai.ecma.duoserver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class HistoryService {
    final
    UserRepository userRepository;
    final
    HistoryRepository historyRepository;
    final
    HistoryOrderRepository historyOrderRepository;

    public HistoryService(HistoryRepository historyRepository, UserRepository userRepository, HistoryOrderRepository historyOrderRepository) {
        this.historyRepository = historyRepository;
        this.userRepository = userRepository;
        this.historyOrderRepository = historyOrderRepository;
    }

    public ResPageable getHistory(int page, int size, String tableName, UUID objectId, OperationEnum operation) {

        historyRepository.updateHistoryObjectIsNull();
        if (objectId == null && tableName == null && operation == null) {
            Page<History> allByTableName = historyRepository.findAll(PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt"))));
            return makeResPageable(allByTableName);
        }
        if (tableName == null && objectId == null) {
            Page<History> allByTableName = historyRepository.findAllByOperationEnumIgnoreCase(operation, PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt"))));
            return makeResPageable(allByTableName);
        }
        if (tableName != null && operation != null && objectId == null) {
            Page<History> allByTableName = historyRepository.findAllByTableNameIgnoreCaseAndOperationEnumIgnoreCase(tableName, operation, PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt"))));
            return makeResPageable(allByTableName);
        }
        if (objectId != null && operation != null && tableName == null) {
            Page<History> allByTableName = historyRepository.findAllByObjectIdAndOperationEnumIgnoreCase(objectId, operation, PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt"))));
            return makeResPageable(allByTableName);
        }
        if (tableName != null && objectId == null) {
            Page<History> allByTableName = historyRepository.findAllByTableNameIgnoreCase(tableName, PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt"))));
            return makeResPageable(allByTableName);
        }
        if (tableName == null) {
            Page<History> allByTableName = historyRepository.findAllByObjectId(objectId, PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt"))));
            return makeResPageable(allByTableName);
        }
        return null;
    }

    public ResPageable getHistoryOrder(int page, int size, UUID objectId, OperationEnum operation) {

        historyOrderRepository.updateHistoryOrderObjectIsNull();
        if (objectId == null && operation == null) {
            Page<HistoryOrder> all = historyOrderRepository.findAll(PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt"))));
            return makeResPageable2(all);
        }
        if (objectId != null && operation != null) {
            Page<HistoryOrder> all = historyOrderRepository.findAllByObjectIdAndOperationEnumIgnoreCase(objectId, operation, PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt"))));
            return makeResPageable2(all);
        }
        if (objectId != null) {
            Page<HistoryOrder> all = historyOrderRepository.findAllByObjectId(objectId, PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt"))));
            return makeResPageable2(all);
        }
        return null;
    }

    private ResPageable makeResPageable(Page<History> allByTableName) {
        List<History> content = allByTableName.getContent();
        List<ResHistory> newContent = new ArrayList<>();
        for (History history : content) {
            User user = new User();
            user.setFirstName("User");
            user.setFirstName("Delete");
            newContent.add(
                    new ResHistory(
                            history.getTableName(),
                            history.getObjectId(),
                            history.getOperationEnum(),
                            history.getObject(),
                            history.getObjectId(),
                            history.getCreatedAt(),
                            history.getUpdatedAt(),
                            history.getCreatedBy(),
                            history.getUpdatedBy(),
                            userRepository.findById(history.getCreatedBy()).orElse(user)));
        }
        return new ResPageable(allByTableName.getNumber(), allByTableName.getSize(), allByTableName.getTotalPages(), allByTableName.getTotalElements(), newContent);
    }

    private ResPageable makeResPageable2(Page<HistoryOrder> allForOrder) {
        List<HistoryOrder> content = allForOrder.getContent();
        List<ResHistory> newContent = new ArrayList<>();
        for (HistoryOrder history : content) {
            User user = new User();
            user.setFirstName("User");
            user.setFirstName("Delete");
            newContent.add(
                    new ResHistory(
                            history.getObjectId(),
                            history.getOperationEnum(),
                            history.getObject(),
                            history.getObjectId(),
                            history.getCreatedAt(),
                            history.getUpdatedAt(),
                            history.getCreatedBy(),
                            history.getUpdatedBy(),
                            userRepository.findById(history.getCreatedBy()).orElse(user)));
        }
        return new ResPageable(allForOrder.getNumber(), allForOrder.getSize(), allForOrder.getTotalPages(), allForOrder.getTotalElements(), newContent);
    }

    public Object makeObject(){

        return  new Object();
    }
}
