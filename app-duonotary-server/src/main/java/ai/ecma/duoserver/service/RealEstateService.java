package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.Order;
import ai.ecma.duoserver.entity.RealEstate;
import ai.ecma.duoserver.entity.enums.RealEstateEnum;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.RealEstateDto;
import ai.ecma.duoserver.repository.OrderRepository;
import ai.ecma.duoserver.repository.RealEstateRepository;
import ai.ecma.duoserver.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RealEstateService {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    RealEstateRepository realEstateRepository;
    public ApiResponse addRealEstate(RealEstateDto realEstateDto, Order order){
        try{
            RealEstate realEstate = new RealEstate();
            Optional<RealEstate> byOrder = realEstateRepository.findByOrder(order);
            if (byOrder.isPresent())realEstate=byOrder.get();
            realEstate.setOrder(order);
            realEstateRepository.save(makeRealEstate(realEstateDto,realEstate));
            return new ApiResponse(AppConstants.SUCCESS_MESSAGE,true);
        }catch (Exception e){return new ApiResponse(AppConstants.CATCH_MESSAGE,false);}
    }
    public ApiResponse editRealEstate(RealEstateDto realEstateDto){
        try{
            Optional<RealEstate> repository = realEstateRepository.findById(realEstateDto.getId());
            if (!repository.isPresent())return new ApiResponse(AppConstants.CATCH_MESSAGE,false);
            realEstateRepository.save(makeRealEstate(realEstateDto,repository.get()));
            return new ApiResponse(AppConstants.SUCCESS_MESSAGE,true);
        }catch (Exception e){return new ApiResponse(AppConstants.CATCH_MESSAGE,false);}
    }
    public RealEstate makeRealEstate(RealEstateDto realEstateDto,RealEstate realEstate){
        if (realEstateDto.getRealEstateEnum()== RealEstateEnum.HOME){
            realEstate.setClientName(null);
            realEstate.setClientPhone(null);
            realEstate.setClientEmail(null);
            realEstate.setClientAddress(null);
        }else {
            realEstate.setClientName(realEstateDto.getClientName());
            realEstate.setClientPhone(realEstateDto.getClientPhone());
            realEstate.setClientEmail(realEstateDto.getClientEmail());
            realEstate.setClientAddress(realEstateDto.getClientAddress());
        }
        realEstate.setRealEstateEnum(realEstateDto.getRealEstateEnum());
        return realEstate;
    }
    public RealEstateDto getRealEstate(RealEstate realEstate){
        if (realEstate.getRealEstateEnum()==RealEstateEnum.HOME)
            return new RealEstateDto(realEstate.getId(),null,null,null,null,realEstate.getRealEstateEnum(),null);
        return new RealEstateDto(
                realEstate.getId(),
                realEstate.getClientName(),
                realEstate.getClientPhone(),
                realEstate.getClientEmail(),
                realEstate.getClientAddress(),
                realEstate.getRealEstateEnum(),
                null
        );
    }
}
