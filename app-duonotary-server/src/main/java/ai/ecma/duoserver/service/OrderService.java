package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.*;
import ai.ecma.duoserver.entity.enums.*;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.*;
import ai.ecma.duoserver.payload.order.ResAddress;
import ai.ecma.duoserver.repository.*;
import ai.ecma.duoserver.utils.AppConstants;
import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class OrderService {

    final
    FeedBackRepository feedBackRepository;

    final
    CustomDiscountTariffRepository customDiscountTariffRepository;

    final
    RealEstateRepository realEstateRepository;
    final
    InternationalRepository internationalRepository;
    final
    AttachmentService attachmentService;
    final
    InternationalService internationalService;
    final
    RealEstateService realEstateService;

    final
    UserService userService;

    final
    ServicePriceService servicePriceService;

    final
    CertificateRepository certificateRepository;

    final
    MailSenderService mailSenderService;

    final
    SharingAndLoyaltyDiscountService sharingDiscountService;

    final
    LoyaltyDiscountService loyaltyDiscountService;

    final
    LoyaltyDiscountRepository loyaltyDiscountRepository;

    final
    SharingDiscountRepository sharingDiscountRepository;

    final
    PaymentRepository paymentRepository;

    final
    UserRepository userRepository;

    final
    ServicePriceRepository servicePriceRepository;

    final
    PayTypeRepository payTypeRepository;

    final
    AttachmentRepository attachmentRepository;

    final
    ZipCodeRepository zipCodeRepository;

    final
    OrderRepository orderRepository;

    final
    CheckRole checkRole;

    final
    OrderAdditionalServiceService orderAdditionalServiceService;

    final
    PricingRepository pricingRepository;

    final
    DiscountPercentRepository discountPercentRepository;

    final
    OrderAdditionalServiceRepository orderAdditionalServiceRepository;

    final
    AdditionalServicePriceRepository additionalServicePriceRepository;

    final
    PasswordEncoder passwordEncoder;

    final
    TimeTableRepository timeTableRepository;

    final
    ZipCodeService zipCodeService;

    final
    AttachmentContentRepository attachmentContentRepository;

    final
    AgentScheduleRepository agentScheduleRepository;

    final
    TimeTableService timeTableService;

    final
    RoleRepository roleRepository;

    final
    PaymentService paymentService;
    final
    CanceledOrderRepository canceledOrderRepository;
    final
    DiscountService discountService;
    final
    MainServiceWorkTimeRepository mainServiceWorkTimeRepository;
    final
    UserZipCodeRepository userZipCodeRepository;

    public OrderService(UserService userService, ServicePriceService servicePriceService, OrderRepository orderRepository, UserRepository userRepository, ServicePriceRepository servicePriceRepository, PayTypeRepository payTypeRepository, AttachmentRepository attachmentRepository,
                        ZipCodeRepository zipCodeRepository, CheckRole checkRole, OrderAdditionalServiceService orderAdditionalServiceService, OrderAdditionalServiceRepository orderAdditionalServiceRepository,
                        PricingRepository pricingRepository, AdditionalServicePriceRepository additionalServicePriceRepository,
                        PaymentRepository paymentRepository, LoyaltyDiscountRepository loyaltyDiscountRepository,
                        SharingDiscountRepository sharingDiscountRepository, LoyaltyDiscountService loyaltyDiscountService,
                        SharingAndLoyaltyDiscountService sharingDiscountService, MailSenderService mailSenderService, CertificateRepository certificateRepository,
                        DiscountPercentRepository discountPercentRepository, PasswordEncoder passwordEncoder, TimeTableRepository timeTableRepository, ZipCodeService zipCodeService,
                        AttachmentContentRepository attachmentContentRepository, AgentScheduleRepository agentScheduleRepository, TimeTableService timeTableService, RoleRepository roleRepository,
                        PaymentService paymentService, InternationalService internationalService, RealEstateService realEstateService, CanceledOrderRepository canceledOrderRepository, DiscountService discountService,
                        RealEstateRepository realEstateRepository, InternationalRepository internationalRepository, MainServiceWorkTimeRepository mainServiceWorkTimeRepository,
                        CustomDiscountTariffRepository customDiscountTariffRepository, FirstOrderDiscountTariffRepository firstOrderDiscountTariffRepository,
                        DiscountExpenseRepository discountExpenseRepository,
                        AttachmentService attachmentService,
                        UserZipCodeRepository userZipCodeRepository, FeedBackRepository feedBackRepository) {
        this.userService = userService;
        this.servicePriceService = servicePriceService;
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
        this.servicePriceRepository = servicePriceRepository;
        this.payTypeRepository = payTypeRepository;
        this.attachmentRepository = attachmentRepository;
        this.zipCodeRepository = zipCodeRepository;
        this.checkRole = checkRole;
        this.orderAdditionalServiceService = orderAdditionalServiceService;
        this.orderAdditionalServiceRepository = orderAdditionalServiceRepository;
        this.pricingRepository = pricingRepository;
        this.additionalServicePriceRepository = additionalServicePriceRepository;
        this.paymentRepository = paymentRepository;
        this.loyaltyDiscountRepository = loyaltyDiscountRepository;
        this.sharingDiscountRepository = sharingDiscountRepository;
        this.loyaltyDiscountService = loyaltyDiscountService;
        this.sharingDiscountService = sharingDiscountService;
        this.mailSenderService = mailSenderService;
        this.certificateRepository = certificateRepository;
        this.discountPercentRepository = discountPercentRepository;
        this.passwordEncoder = passwordEncoder;
        this.timeTableRepository = timeTableRepository;
        this.zipCodeService = zipCodeService;
        this.attachmentContentRepository = attachmentContentRepository;
        this.agentScheduleRepository = agentScheduleRepository;
        this.timeTableService = timeTableService;
        this.roleRepository = roleRepository;
        this.paymentService = paymentService;
        this.internationalService = internationalService;
        this.realEstateService = realEstateService;
        this.canceledOrderRepository = canceledOrderRepository;
        this.discountService = discountService;
        this.realEstateRepository = realEstateRepository;
        this.internationalRepository = internationalRepository;
        this.mainServiceWorkTimeRepository = mainServiceWorkTimeRepository;
        this.customDiscountTariffRepository = customDiscountTariffRepository;
        this.firstOrderDiscountTariffRepository = firstOrderDiscountTariffRepository;
        this.discountExpenseRepository = discountExpenseRepository;
        this.attachmentService = attachmentService;
        this.userZipCodeRepository = userZipCodeRepository;
        this.feedBackRepository = feedBackRepository;
    }

    /**
     * buyurtmani saqlash va tahrirlash uchun
     */
    public ApiResponse addOrder(OrderDto orderDto, String id) {
        try {
            boolean admin = false;
            if (id != null) {
                Optional<User> byId = userRepository.findById(UUID.fromString(id));
                if (byId.isPresent()) {
                    User user = byId.get();
                    admin = checkRole.isROLE_SUPER_ADMIN(user.getRoles());
                    if (!admin) {
                        admin = checkRole.isAdmin(user.getRoles());
                    }
                }
            }
            Optional<TimeTable> timeTableOptional;
            if (orderDto.getId() == null) {
                timeTableOptional = timeTableRepository.findByIdAndOrderIsNull(orderDto.getTimeTableId());
            } else {
                timeTableOptional = timeTableRepository.findById(orderDto.getTimeTableId());
            }
            if (!timeTableOptional.isPresent())
                return new ApiResponse("This time already booked", false);
            Optional<ServicePrice> optionalServicePrice = servicePriceRepository.findById(orderDto.getServicePriceId());
            if (optionalServicePrice.isPresent()) {
                ServicePrice servicePrice = optionalServicePrice.get();
                TimeTable timeTable = timeTableOptional.get();
                if (servicePrice.getActive()) {
                    Order order = new Order();
                    if (orderDto.getId() != null) {
                        order = orderRepository.findById(orderDto.getId()).orElseThrow(() -> new ResourceNotFoundException("getOrder", "id", orderDto.getId()));
                    }
                    if (orderDto.isNewUser()) {
                        final Optional<User> newUser = userRepository.findByEmailOrPhoneNumberAndPassword(orderDto.getUsername().toLowerCase(), orderDto.getUsername().toLowerCase(), passwordEncoder.encode(orderDto.getPassword()));
                        if (newUser.isPresent()) {
                            order.setClient(newUser.get());
                        }
                    } else {
                        order.setClient(userRepository.findById(orderDto.getClientId()).orElseThrow(() -> new ResourceNotFoundException("Client", "getClient", orderDto.getClientId())));
                    }

                    order.setServicePrice(servicePrice);
                    ZipCode zipCode = new ZipCode();
                    if (!servicePrice.getService().getMainService().isOnline()) {
                        if (orderDto.getAddress() == null)
                            return new ApiResponse("Address must be empty", false);
                        order.setAddress(orderDto.getAddress());
                        order.setLat(orderDto.getLat());
                        order.setLng(orderDto.getLng());
                        if (orderDto.getZipCodeId() != null) {
                            Optional<ZipCode> optionalZipCode = zipCodeRepository.findByIdAndActiveTrue(orderDto.getZipCodeId());
                            if (!optionalZipCode.isPresent())
                                return new ApiResponse("This zip code not active", false);
                            zipCode = optionalZipCode.get();
                            if (!userZipCodeRepository.existsByZipCode_idAndUser_Id(zipCode.getId(), orderDto.getAgentId())) {
                                return new ApiResponse("Agent has not got permission to this zip code", false);
                            }
                            order.setZipCode(zipCode);
                        }
                        Optional<PayType> byId = payTypeRepository.findById(orderDto.getPayTypeId());
                        if (!byId.isPresent())
                            return new ApiResponse("PayType not found", false);
                        order.setPayType(byId.get());
                    } else {
                        Optional<PayType> byOnlineTrue = payTypeRepository.findByOnlineTrue();
                        if (byOnlineTrue.isPresent()) {
                            order.setPayType(byOnlineTrue.get());
                        } else {
                            new ApiResponse("Online PayType yo`q", false);
                        }
                    }
                    Optional<User> agentOptional = userRepository.findByIdAndActiveTrue(orderDto.getAgentId());
                    if (agentOptional.isPresent()) {
                        User agent = agentOptional.get();
                        if (!checkRole.isAgent(agent.getRoles())) {
                            return new ApiResponse("Agent not fount", false);
                        }
                        if (!servicePrice.getService().getMainService().isOnline()) {
                            List<Certificate> allByUser_idAndExpiredFalse = certificateRepository.findAllByUser_IdAndExpiredFalse(agent.getId());
                            if (zipCode.getCounty() == null && !servicePrice.getService().getMainService().isOnline())
                                return new ApiResponse("Please enter the zip code", false);
                            boolean equal = false;
                            for (Certificate certificate : allByUser_idAndExpiredFalse) {
                                if (certificate.getState().getId() == zipCode.getCounty().getState().getId()) {
                                    equal = true;
                                    break;
                                }
                            }
                            if (!equal)
                                return new ApiResponse("Your zip code is not active", false);
                        }
                        order.setAgent(agent);
                        timeTable.setAgent(agent);
                    } else {
                        return new ApiResponse("Agent not fount", false);
                    }

                    order.setTitleDocument(orderDto.getTitleDocument());

                    order.setCountDocument(orderDto.getCountDocument());

                    ServiceEnum serviceEnum = order.getServicePrice().getService().getSubService().getServiceEnum();
                    boolean isDocument = serviceEnum == ServiceEnum.INTERNATIONAL || serviceEnum == ServiceEnum.REAL_ESTATE || order.getServicePrice().getService().getMainService().isOnline();

                    if (isDocument && !orderDto.getDocAttachmentId().isEmpty()) {
                        if (orderRepository.existDocument(orderDto.getDocAttachmentId()) > 0)
                            return new ApiResponse("Something wrong in time table", false);
                        else
                            order.setDocuments(attachmentRepository.findAllById(orderDto.getDocAttachmentId()));
                    }
                    double amount = amountOrderWithoutDiscount(order.getServicePrice(), orderDto);
                    if (admin) {
                        order.setAmount(orderDto.getAmount());
                        order.setAmountDiscount(orderDto.getDiscountAmount());
                        order.setOrderStatus(orderDto.getStatus());
                    } else {
                        order.setAmount(amount);
                        ApiResponse apiResponse = checkDiscount(orderDto, order, amount);
                        if (!apiResponse.isSuccess())
                            return apiResponse;
                        if (orderDto.getDiscountsDtos() != null && !orderDto.getDiscountsDtos().isEmpty()) {
                            order.setAmountDiscount(orderDto.getDiscountAmount());
                        }
                        order.setOrderStatus(orderDto.isNewUser() ? OrderStatus.DRAFT : OrderStatus.NEW);
                    }
                    if (order.getId() == null) {
                        order.setSerialNumber(generateNumber(""));
                        order.setCheckNumber(orderDto.getCheckNumber());
                        order.setRegisterBy(orderDto.getRegisterBy());
                    }
                    Order savedOrder = orderRepository.save(order);
                    try {
                        timeTable.setOrder(savedOrder);
                        timeTableRepository.save(timeTable);
                    } catch (Exception e) {
                        orderRepository.delete(savedOrder);
                        return new ApiResponse("Something wrong in time table", false);
                    }
                    new Thread(() -> {
                        try {
                            mailSenderService.sendOrderInfoToAgent(savedOrder, timeTable, true);
                            mailSenderService.sendOrderInfoToAgent(savedOrder, timeTable, false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }).start();

                    if (orderDto.getDiscountsDtos() != null && !orderDto.getDiscountsDtos().isEmpty() && !clientUserDiscounts(orderDto, orderDto.getDiscountsDtos(), savedOrder)) {
                        savedOrder.setAmountDiscount(0);
                        orderRepository.save(savedOrder);
                    }
                    if (!setInternationalAndRealEstate(savedOrder, orderDto).isSuccess()) {
                        orderRepository.delete(savedOrder);
                        return new ApiResponse("Process has not successfully completed", false);
                    }

                    try {
                        if (!orderDto.getOrderAdditionalServiceDtoList().isEmpty()) {
                            for (OrderAdditionalServiceDto item : orderDto.getOrderAdditionalServiceDtoList()) {
                                item.setOrder(savedOrder);
                                orderAdditionalServiceService.addOrderAdditionService(item);
                            }
                        }
                        return new ApiResponse(orderDto.getId() == null ? "Successfully saved" : "Successfully edited", true, makeOrderDto(savedOrder));
                    } catch (Exception e) {
                        orderRepository.delete(savedOrder);
                        return new ApiResponse("Process has not successfully completed", false);
                    }
                }
                return new ApiResponse("This service not working this zip code", false);
            }
            return new ApiResponse("This service not active. Please try again later", false);
        } catch (
                Exception e) {
            return new ApiResponse("Sorry could not save", false);
        }

    }

    public String generateNumber(String item) {
        item = item + new Random().nextInt(10) + 1;
        if (item.length() < 7) {
            item = generateNumber(item);
        }
        if (orderRepository.existsBySerialNumber(item)) {
            item = "";
            generateNumber(item);
        }
        return item;
    }


    public ApiResponse editOrder(OrderDto orderDto, User requester) {
        try {
            Order order = orderRepository.findById(orderDto.getId()).orElseThrow(() -> new ResourceNotFoundException("getOrder", "id", orderDto.getId()));
            if (!order.getOrderStatus().equals(OrderStatus.NEW))
                return new ApiResponse("Your order already " + order.getOrderStatus().name(), false);
            if (order.getEditCount() > 1 && orderDto.getDiscountsDtos().isEmpty())
                return new ApiResponse("Sorry. You have already taken full advantage of the editing feature. You can cancel only", false);
            boolean online = order.getServicePrice().getService().getMainService().isOnline();
            TimeTable timeTable = timeTableRepository.findById(orderDto.getTimeTableId()).orElseThrow(() -> new ResourceNotFoundException("getTimeTable", "id", orderDto.getTimeTableId()));
            User agent = timeTable.getAgent();
            order.setAgent(agent);                                                                  //setAgent

            ServicePrice servicePrice = servicePriceRepository.findByIdAndActiveTrue(orderDto.getServicePriceId()).orElseThrow(() -> new ResourceNotFoundException("getServicePrice", "id", orderDto.getServicePriceId()));
            Double newAmount = amountOrderWithoutDiscount(servicePrice, orderDto);
            if (!orderDto.getDocAttachmentId().isEmpty())
                order.setDocuments(attachmentRepository.findAllById(orderDto.getDocAttachmentId())); //setDocuments

            if (orderDto.getCountDocument() == 0 || orderDto.getCountDocument() == null)
                return new ApiResponse("Please, fill all information", false);

            ApiResponse checkDiscount = checkDiscount(orderDto, order, newAmount);
            if (checkDiscount.isSuccess())
                if (orderDto.getDiscountsDtos() != null && !orderDto.getDiscountsDtos().isEmpty()) {
                    order.setAmountDiscount(orderDto.getDiscountAmount());
                }

            order.setCountDocument(orderDto.getCountDocument());                            //setCountDocument
            order.setServicePrice(servicePrice);                                            //setServicePrice
            double oldAmount = order.getAmount();
            order.setAmount(newAmount);                                                     //setAmount
            order.setAmountDiscount(orderDto.getDiscountAmount());                          //setDiscountAmount
            boolean zipCodeChange = false;
            if (online) {
                if (orderDto.getTitleDocument() == null)
                    return new ApiResponse("Please, fill all information", false);
                order.setTitleDocument(orderDto.getTitleDocument());                        //setTitleDocument
                order.setPayType(payTypeRepository.findByOnlineTrue().orElseThrow(() -> new ResourceNotFoundException("getPayType", "online", true)));
            } else {
                if (!newAmount.equals(oldAmount)) {
                    List<Payment> paymentList = paymentRepository.findAllByOrder_IdAndPayStatus(order.getId(), PayStatus.STRIPE);
                    if (!paymentList.isEmpty())
                        paymentService.cancelPaymentIntent(order.getId());
                }

                if (orderDto.getZipCodeId() == null)
                    new ApiResponse("Zip code not found", false);
                zipCodeChange = !order.getZipCode().getId().equals(orderDto.getZipCodeId());
                if (!setInternationalAndRealEstate(order, orderDto).isSuccess()) {
                    return new ApiResponse("Process has not successfully completed", false);
                }
                ZipCode zipCode = zipCodeRepository.findByIdAndActiveTrue(orderDto.getZipCodeId()).orElseThrow(() -> new ResourceNotFoundException("getZipCode", "id", orderDto.getZipCodeId()));
                if (!order.getServicePrice().getZipCode().equals(zipCode))
                    return new ApiResponse("Sorry, the service not working this zip code", false);
                if (!certificateRepository.existsByUserAndState_IdAndExpired(agent, zipCode.getCounty().getState().getId(), false))
                    return new ApiResponse("Something wrong with agent", false);

                order.setZipCode(zipCode);                                                  //setZipCode
                order.setLat(orderDto.getLat());                                            //setLat
                order.setLng(orderDto.getLng());                                            //setLng
                order.setAddress(orderDto.getAddress());                                    //setAddress
            }
            if (orderDto.getDiscountsDtos().isEmpty())
                order.setEditCount(order.getEditCount() + 1);                                   //setEditCount
            Order save = orderRepository.save(order);
            try {
                timeTableRepository.deleteAllByOrder(save);
            } catch (Exception e) {
                return new ApiResponse("Cannot edit", false);
            }
            try {
                timeTable.setOrder(save);
                timeTableRepository.save(timeTable);
            } catch (Exception e) {
                timeTableRepository.delete(timeTable);
                return new ApiResponse("Cannot edit", false);
            }
            new Thread(() -> {
                try {
                    mailSenderService.editOrder(save, requester, true);
                    mailSenderService.editOrder(save, requester, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();

            if ((!newAmount.equals(oldAmount) || zipCodeChange) && orderDto.getDiscountsDtos().isEmpty())
                clientUserDiscountsDelete(save);
            if (orderDto.getDiscountsDtos() != null && !orderDto.getDiscountsDtos().isEmpty() && !clientUserDiscounts(orderDto, orderDto.getDiscountsDtos(), order)) {
                order.setAmountDiscount(0);
            } else {
                order.setAmountDiscount(orderDto.getDiscountAmount());//setDiscountAmount
            }
            orderRepository.save(order);
            return new ApiResponse("Successfully edited", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse editOrderDiscount(OrderDto orderDto) {
        try {
            Order order = orderRepository.findById(orderDto.getId()).orElseThrow(() -> new ResourceNotFoundException("getOrder", "id", orderDto.getId()));
            if (!order.getOrderStatus().equals(OrderStatus.NEW))
                return new ApiResponse("Your order already " + order.getOrderStatus().name(), false);
            if (orderDto.getDiscountsDtos() != null && !orderDto.getDiscountsDtos().isEmpty() && !clientUserDiscounts(orderDto, orderDto.getDiscountsDtos(), order)) {
                order.setAmountDiscount(0);
            } else {
                order.setAmountDiscount(orderDto.getDiscountAmount());//setDiscountAmount
            }
            orderRepository.save(order);
            return new ApiResponse("Successfully edited", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    private ApiResponse checkDiscount(OrderDto orderDto, Order order, Double amount) {
        if (orderDto.getDiscountsDtos() != null && !orderDto.getDiscountsDtos().isEmpty() && !clientUserEnoughDiscounts(orderDto, orderDto.getDiscountsDtos())) {
            return new ApiResponse("Error! Your discounts!", false);
        }
        Double percent;
        if (servicePriceRepository.existsByIdAndService_MainService_OnlineIsTrue(orderDto.getServicePriceId())) {
            percent = discountPercentRepository.findByOnlineIsTrueAndActiveIsTrue()
                    .orElse(new DiscountPercent(0d, null, false, false)).getPercent();
        } else {
            percent = discountPercentRepository.findByZipCode_IdAndActiveIsTrue(orderDto.getZipCodeId())
                    .orElse(new DiscountPercent(0d, null, false, false)).getPercent();
        }
        if (orderDto.getDiscountAmount() > 0 && percent == 0.0) {
            return new ApiResponse("You cannot use discounts", false);
        }
        if ((order.getAmountDiscount() / amount) * 100 > percent)
            return new ApiResponse("You cannot use more than " + percent + "% off the discount.", false);
        return new ApiResponse("ok", true);
    }

    public ApiResponse setInternationalAndRealEstate(Order order, OrderDto orderDto) {
        if (order.getServicePrice().getService().getSubService().getServiceEnum() == ServiceEnum.REAL_ESTATE) {
            if (orderDto.getRealEstateDto() == null || orderDto.getRealEstateDto().getId() == null)
                return realEstateService.addRealEstate(orderDto.getRealEstateDto(), order);
            else return realEstateService.editRealEstate(orderDto.getRealEstateDto());
        } else if (order.getServicePrice().getService().getSubService().getServiceEnum() == ServiceEnum.INTERNATIONAL) {
            if (orderDto.getInternationalDto() == null || orderDto.getInternationalDto().getId() == null)
                return internationalService.addInternational(orderDto.getInternationalDto(), order);
            else return internationalService.editInternational(orderDto.getInternationalDto());

        } else {
            realEstateRepository.deleteByOrder(order.getId());
            internationalRepository.deleteByOrder(order.getId());
        }
        return new ApiResponse(true, "this service is not international or real estate");
    }

    /**
     * 1 ta buyurtmani olish
     */
    public ApiResponse getOne(UUID id) {
        Order order = orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Order", "getOrder", id));
        return new ApiResponse("ok", true, makeOrderDto(order));
    }

    public ResPageable getAllByUserId(int page, int size, User user) {
        Page<Order> orderPage = orderRepository.findAllByClientAndActiveIsTrue(user, PageRequest.of(page, size));
        return new ResPageable(page, size, orderPage.getTotalPages(), orderPage.getTotalElements(),
                orderPage.getContent().stream().map(this::makeOrderDto).collect(Collectors.toList()));
    }

    public ResPageable getAllByAgentId(int page, int size, User user) {
        Page<Order> orderPage = orderRepository.findAllByAgentAndActiveIsTrue(user, PageRequest.of(page, size));
        return new ResPageable(page, size, orderPage.getTotalPages(), orderPage.getTotalElements(),
                orderPage.getContent().stream().map(this::makeOrderDto).collect(Collectors.toList()));
    }

    public OrderDto getOrder(Order order) {
        return new OrderDto(
                order.getAddress(),
                zipCodeService.getZipCode(order.getZipCode()),
                order.getLat(),
                order.getLng(),
                getPayType(order.getPayType()),
                order.getAmount(),
                order.getCountDocument(),
                order.getCheckNumber(),
                order.getDocuments(),
                order.getDocVerifyId(),
                order.isPacket(),
                order.getId(),
                order.getCreatedAt(),
                this.userService.getUser(order.getClient()),
                this.userService.getUser(order.getAgent()),
                this.servicePriceService.getServicePriceForOrder(order.getServicePrice()),
                order.getOrderStatus(),
                timeTableService.getTimeTableDto(timeTableRepository.findByOrder(order).orElse(new TimeTable()))
        );
    }

    public PayTypeDto getPayType(PayType payType) {
        return new PayTypeDto(
                payType.getId(),
                payType.getName(),
                payType.isActive(),
                payType.isOnline(),
                payType.getDescription()
        );
    }

    /**
     * buyurtmalarni userId yoki current user orqali pageable qilib olish
     */
    public ResPageable getPageable(int page, int size, UUID userId, User user) {
        if (checkRole.isAgent(user.getRoles())) {
            Page<Order> allByAgent = orderRepository.findAllByAgent_IdAndActiveIsTrue(user.getId(), PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt"))));
            return new ResPageable(page, size, allByAgent.getTotalPages(), allByAgent.getTotalElements(), makeOrderList(allByAgent.getContent()));
        }
        if (checkRole.isUser(user.getRoles())) {
            Page<Order> allByAgent = orderRepository.findAllByClient_IdAndActiveIsTrue(user.getId(), PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt"))));
            return new ResPageable(page, size, allByAgent.getTotalPages(), allByAgent.getTotalElements(), makeOrderList(allByAgent.getContent()));
        }
        Page<Order> allOrder = orderRepository.findAllByClient_IdOrAgent_IdAndActiveIsTrue(userId, userId, PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt"))));
        return new ResPageable(page, size, allOrder.getTotalPages(), allOrder.getTotalElements(), makeOrderList(allOrder.getContent()));
    }

    public ResPageable getOrderList(int page, int size) {
        Page<Order> orderPage = orderRepository.findAllByActiveIsTrue(PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt"))));
        return new ResPageable(page, size, orderPage.getTotalPages(), orderPage.getTotalElements(), makeOrderList(orderPage.getContent()));
    }

    /**
     * To complete the order
     */
    public ApiResponse orderCompleter(UUID id, User currentUser) {
        try {
            Order order = orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Order", "getOrder", id));
            if (order.getPayType().isOnline()) {
                List<Payment> payments = paymentRepository.findAllByOrder_IdAndPayStatus(order.getId(), PayStatus.STRIPE);
                if (!payments.isEmpty()) {
                    for (Payment payment : payments) {
                        if (payment.getPayStatus().equals(PayStatus.STRIPE)) {
                            if (order.getChargeId() != null) {
                                paymentService.getCaptureByOrderId(order.getId());
                            }
                        }
                    }
                } else {
                    return new ApiResponse("In order to complete this order, client should make a payment.", false);
                }
            } else {
                Payment payment = new Payment(
                        order,
                        order.getAmount() - order.getAmountDiscount(),
                        PayStatus.PAYED,
                        "CASH",
                        order.getPayType(),
                        order.getClient().getId().toString(),
                        order.getChargeId()
                );
                paymentRepository.save(payment);
            }
            if (order.getOrderStatus().equals(OrderStatus.COMPLETED))
                new ApiResponse("Order already COMPLETED", true);
            if (!order.getAgent().getId().equals(currentUser.getId()) && !checkRole.isAdmin(currentUser.getRoles()))
                return new ApiResponse("Yuo can not do it!", false);
            order.setOrderStatus(OrderStatus.COMPLETED);
            order = orderRepository.save(order);
            sharingDiscountService.addSharingDiscount(order);

            return new ApiResponse("Success", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    /**
     * Calculate amount of money to cancel the order
     */
    public ApiResponse calculateOrderToCancel(UUID id, User currentUser) {
        try {
            Order order = orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Order", "getOrder", id));
            if (!order.getClient().getId().equals(currentUser.getId()))
                return new ApiResponse("Yuo can not do it!", false);
            if (order.getServicePrice().getChargeMinute() != null && order.getServicePrice().getChargePercent() != null
                    && order.getServicePrice().getChargeMinute() > 0 && order.getServicePrice().getChargePercent() > 0) {
                boolean bigFromChargeMinute = timeTableRepository.isBigFromChargeMinute(order.getId(), new Timestamp(System.currentTimeMillis() + order.getServicePrice().getChargeMinute() * 60 * 1000), new Timestamp(System.currentTimeMillis()));
                if (!bigFromChargeMinute) {
                    double amount = (order.getAmount() - order.getAmountDiscount()) / 100 * order.getServicePrice().getChargePercent();
                    return new ApiResponse(true, Double.parseDouble(formatDecimal(amount)));
                }
                return new ApiResponse(true, 0d);
            }
            return new ApiResponse(true, 0d);

        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    /**
     * To cancel the order
     */
    public ApiResponse orderCanceler(UUID id, User currentUser) {
        try {
            Order order = orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Order", "getOrder", id));
            TimeTable timeTable = timeTableRepository.findByOrder(order).orElseThrow(() -> new ResourceNotFoundException("getTimeTable", "orderId", order));
            if (timeTable.getFromTime().before(new Timestamp(System.currentTimeMillis()))) {
                return new ApiResponse("You can not cancel", false);
            }
            Optional<Payment> byOrder = paymentRepository.findByOrder(order);
            if (order.getClient().getId().equals(currentUser.getId())) {
                if (byOrder.isPresent()) {
                    Payment payment = byOrder.get();
                    if (payment.getPayStatus().equals(PayStatus.STRIPE)) {
                        if (paymentService.cancelOrder(payment, (double) calculateOrderToCancel(id, currentUser).getObject())) {
                            order.setOrderStatus(OrderStatus.CANCELLED);
                            payment.setPayStatus(PayStatus.BACKOFF);
                            paymentRepository.save(payment);
                            orderRepository.save(order);
                            return new ApiResponse("Order is successfully canceled", true);
                        } else {
                            return new ApiResponse("Something wrong", false);
                        }
                    }
                }
                order.setOrderStatus(OrderStatus.CANCELLED);
                orderRepository.save(order);
                return new ApiResponse("Order is successfully canceled", true);
            } else if (checkRole.isAdmin(currentUser.getRoles())) {
                if (byOrder.isPresent()) {
                    Payment payment = byOrder.get();
                    if (payment.getPayStatus().equals(PayStatus.STRIPE) || payment.getPayStatus().equals(PayStatus.PAYED)) {
                        ApiResponse apiResponse = paymentService.cancelPaymentIntent(order.getId());
                        if (apiResponse.isSuccess()) {
                            order.setOrderStatus(OrderStatus.REJECTED);
                            orderRepository.save(order);
                            return new ApiResponse("Order is successfully canceled", true);
                        } else {
                            return new ApiResponse("Order is not successfully canceled", false);
                        }
                    }
                }
                return new ApiResponse("Order is not canceled", false);
            } else {
                return new ApiResponse("You cannot do it", false);
            }
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }


    /**
     * buyurtmani o`chirish
     */
    public ApiResponse deleteOrder(UUID id) {
        Order order = orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Order", "getOrder", id));
        order.setActive(false);
        Optional<TimeTable> byOrder = timeTableRepository.findByOrder(order);
        try {
            byOrder.ifPresent(timeTable -> {
                timeTable.setActive(false);
                timeTableRepository.save(timeTable);
            });
            orderRepository.save(order);
            return new ApiResponse("Successfully deleted", true);
        } catch (Exception error) {
            return new ApiResponse("Server could not delete", false);
        }
    }

    public ApiResponse getAmount(OrderDto orderDto) {
        final Optional<ServicePrice> servicePrice = servicePriceRepository.findByIdAndActiveTrue(orderDto.getServicePriceId());
        if (servicePrice.isPresent()) {
            final Double aDouble = amountOrderWithoutDiscount(servicePrice.get(), orderDto);
            double sharingDiscount = 0;
            double loyaltyDiscount = 0;
            Double percent = discountPercentRepository.findByZipCode_IdAndActiveIsTrue(orderDto.getZipCodeId())
                    .orElse(new DiscountPercent(0d, null, false, false))
                    .getPercent();
            if (orderDto.isNewUser()) {
                Optional<User> optionalUser = userRepository.findByEmailOrPhoneNumberAndPassword(orderDto.getUsername(), orderDto.getUsername(), passwordEncoder.encode(orderDto.getPassword()));
                if (optionalUser.isPresent()) {
                    sharingDiscount = sharingDiscountService.getUserSharingDiscount(optionalUser.get());
                    loyaltyDiscount = loyaltyDiscountRepository.getUserLoyaltyDiscountLeftover(optionalUser.get().getId());
                }
            } else {
                sharingDiscount = sharingDiscountService.getUserSharingDiscount(userRepository.findById(orderDto.getClientId()).orElseThrow(() -> new ResourceNotFoundException("getUser", "id", orderDto.getClientId())));
                loyaltyDiscount = loyaltyDiscountRepository.getUserLoyaltyDiscountLeftover(orderDto.getClientId());
            }
            return new ApiResponse(true, new PriceDtoForOrder(aDouble, sharingDiscount, loyaltyDiscount, percent));
        }
        return new ApiResponse("Service Price not found", false);
    }

    public boolean complete(OrderDto orderDto) {
        if (orderDto.getAddress() == null && orderDto.getZipCodeId() == null && orderDto.getLng() == null && orderDto.getLat() == null) {
            return true;
        } else
            return orderDto.getAddress() != null && orderDto.getZipCodeId() != null && orderDto.getLng() != null && orderDto.getLat() != null;
    }


    /**
     * orderni chegirmalarsiz jami summasi qancha ekanligini hisoblab beradi
     */
    public Double amountOrderWithoutDiscount(ServicePrice servicePrice, OrderDto orderDto) {
        try {
            double percent = 0.0;
            Optional<TimeTable> timeTable = timeTableRepository.findById(orderDto.getTimeTableId());
            if (timeTable.isPresent()) {
                percent = mainServiceWorkTimeRepository.getMainServiceWorkTimePercent(timeTable.get().getFromTime(), servicePrice.getService().getMainService().getId());
            }
            double docPrice = pricingRepository.getPriceByCountDocAndServicePrice(orderDto.getCountDocument(), orderDto.getServicePriceId());
            docPrice = docPrice * orderDto.getCountDocument();
            double additionalServicePriceSum = 0;
            if (orderDto.getOrderAdditionalServiceDtoList().size() > 0) {
                for (OrderAdditionalServiceDto orderAdditionalServiceDto : orderDto.getOrderAdditionalServiceDtoList()) {
                    AdditionalServicePrice additionalServicePrice = additionalServicePriceRepository.findByIdAndActiveTrue(orderAdditionalServiceDto.getAdditionalServicePriceId()).orElseThrow(() -> new ResourceNotFoundException("getAdditionalServicePrice", "id", orderAdditionalServiceDto.getAdditionalServicePriceId()));
                    additionalServicePriceSum = additionalServicePrice.getPrice() * orderAdditionalServiceDto.getCount();
                }
            }

            double allAmount = docPrice + additionalServicePriceSum + servicePrice.getPrice();
            return allAmount + (allAmount / 100 * percent);
        } catch (Exception e) {
            e.printStackTrace();
            return 0d;
        }
    }

    /**
     * orderni orderDto ga o`rash
     */
    public OrderDto makeOrderDto(Order order) {
        OrderDto orderDto = new OrderDto(
                order.getAddress(),
                order.getZipCode() != null ? zipCodeService.getZipCode(order.getZipCode()) : null,
                order.getLat(),
                order.getLng(),
                new PayTypeDto(order.getPayType().getId(), order.getPayType().getName(), order.getPayType().isActive(), order.getPayType().isOnline(), order.getPayType().getDescription()),
                order.getAmount(),
                order.getCountDocument(),
                order.getCheckNumber(),
                order.getDocuments(),
                order.getDocVerifyId(),
                order.isPacket(),
                order.getId(),
                order.getCreatedAt(),
                this.userService.getUser(order.getClient()),
                this.userService.getUser(order.getAgent()),
                this.servicePriceService.getServicePricePage(order.getServicePrice()), order.getOrderStatus(),
                timeTableService.getTimeTableDto(timeTableRepository.findByOrder(order).orElse(new TimeTable())));
        orderDto.setDiscountAmount(order.getAmountDiscount());
        orderDto.setSerialNumber(order.getSerialNumber());
        orderDto.setTitleDocument(order.getTitleDocument());
        orderDto.setOrderAdditionalServiceDtoList(orderAdditionalServiceService.makeOrderAddSerDtoList(orderAdditionalServiceRepository.findAllByOrder(order)));
        Optional<Payment> byOrder = paymentRepository.findByOrder(order);
        orderDto.setResUploadFiles(getResUploadFileListByOrderId(order.getId()));
        if (byOrder.isPresent()) {
            Payment payment = byOrder.get();
            if (payment.getPayStatus().equals(PayStatus.PAYED))
                orderDto.setPayed(true);
            if (payment.getPayStatus().equals(PayStatus.STRIPE))
                orderDto.setStripe(true);
        }
        realEstateRepository.findByOrder(order).ifPresent(estate -> orderDto.setRealEstateDto(realEstateService.getRealEstate(estate)));
        internationalRepository.findByOrder(order).ifPresent(international -> orderDto.setInternationalDto(internationalService.getInternational(international)));
        orderDto.setFeedbackStatus(getFeedBackStatus(order));
        return orderDto;
    }

    public OrderDto makeOrderDtoForFeedBack(Order order) {
        return new OrderDto(order.getId(), userService.getUser(order.getClient()), userService.getUser(order.getAgent()), order.getAmount());
    }

    public OrderFeedback getFeedBackStatus(Order order) {
        final boolean agent = feedBackRepository.existsByAgentTrueAndOrder(order);
        final boolean client = feedBackRepository.existsByAgentFalseAndOrder(order);
        if (agent && client) return OrderFeedback.BOTH;
        else if (agent) return OrderFeedback.AGENT;
        else if (client) return OrderFeedback.CLIENT;
        else return OrderFeedback.NONE;
    }

    /**
     * orderlarni orderDtolarga o`rash
     */
    private List<OrderDto> makeOrderList(List<Order> orders) {
        List<OrderDto> resList = new ArrayList<>();
        orders.forEach(order -> resList.add(makeOrderDto(order)));
        return resList;
    }

    public String getSerialNumberOrder() {
        return RandomStringUtils.random(6, true, true);
    }


    // Bu Kontrollerda Online ishlaydigan Agent Order ni  Tayyor bolgan documentlarni DocverifyId sini yoki PackageId sini yuklaydi
    @Transactional
    public void addDocverifyIdOrPackageIdFromDocverify(UUID orderId, boolean isPackageId, String docId) {
        try {
            Optional<Order> optionalOrder = orderRepository.findById(orderId);
            if (optionalOrder.isPresent()) {
                Order order = optionalOrder.get();
                List<Attachment> docVerifyFileList = new ArrayList<>();
                if (isPackageId) {
                    List<ResDocArray> resDocArrayList = getPacket(docId);
                    if (resDocArrayList != null) {
                        for (ResDocArray resDocArray : resDocArrayList) {
                            String stringContent = getStringContentByDocVerifyId(resDocArray.getDocVerifyId());
                            if (stringContent != null) {
                                Attachment attachment = new Attachment("OrderId : " + orderId, 0, "pdf",AttachmentType.SIMPLE);
                                Attachment savedAttachment = attachmentRepository.save(attachment);
                                AttachmentContent attachmentContent = new AttachmentContent(savedAttachment, stringContent.getBytes(), stringContent);
                                attachmentContentRepository.save(attachmentContent);
                                docVerifyFileList.add(savedAttachment);
                            }
                        }
                    }
                } else {
                    String stringContent = getStringContentByDocVerifyId(docId);
                    if (stringContent != null) {
                        Attachment attachment = new Attachment("OrderId : " + orderId, 0, "pdf",AttachmentType.SIMPLE);
                        Attachment savedAttachment = attachmentRepository.save(attachment);
                        AttachmentContent attachmentContent = new AttachmentContent(savedAttachment, stringContent.getBytes(), stringContent);
                        attachmentContentRepository.save(attachmentContent);
                        docVerifyFileList.add(savedAttachment);
                    }
                }
                if (docVerifyFileList.size() > 0) {
                    order.setFilesFromDocVerify(docVerifyFileList);
                    order.setPacket(isPackageId);
                    order.setDocVerifyId(docId);
                    orderRepository.save(order);
                    new ApiResponse("Ok !!!", true);
                    return;
                }
                new ApiResponse("Error !!! Order not found", false);
                return;
            }
            new ApiResponse("Error !!! Order not found", false);
        } catch (Exception e) {
            new ApiResponse("Error !!!", false);
        }
    }

    public String getStringContentByDocVerifyId(String docId) {
        try {
            URL url = new URL("https://api.docverify.com/V2/dvapi.asmx");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "text/xml;charset=utf-8");
            OutputStream outputStream = connection.getOutputStream();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
            outputStreamWriter.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                    "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                    "  <soap:Body>\n" +
                    "   <GetDocument xmlns=\"https://api.docverify.com/\">\n" +
                    "      <apiKey>upbvpmZu5eevouSJute8XqFlFZf6gE4Z</apiKey>\n" +
                    "      <apiSig>447194D52559259982BE55B7895BD843</apiSig>\n" +
                    "      <DocVerifyID>" + docId + "</DocVerifyID>\n" +
                    "    </GetDocument>\n" +
                    "     </soap:Body>\n" +
                    "</soap:Envelope>");
            outputStreamWriter.flush();
            outputStreamWriter.close();
            outputStream.close();
            connection.connect();

            BufferedInputStream bis = new BufferedInputStream(connection.getInputStream());
            ByteArrayOutputStream buf = new ByteArrayOutputStream();

            int result2 = bis.read();

            while (result2 != -1) {
                buf.write((byte) result2);
                result2 = bis.read();

            }

            String responseBody = buf.toString();

            JSONObject xmlJSONObj = XML.toJSONObject(responseBody);

            return xmlJSONObj
                    .getJSONObject("soap:Envelope")
                    .getJSONObject("soap:Body")
                    .getJSONObject("GetDocumentResponse")
                    .getString("GetDocumentResult");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<ResDocArray> getPacket(String packetId) {
        try {
            URL url = new URL("https://api.docverify.com/V2/dvapi.asmx");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "text/xml;charset=utf-8");
            OutputStream outputStream = connection.getOutputStream();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
            outputStreamWriter.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                    "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                    "  <soap:Body>\n" +
                    "   <GetPacketDocs xmlns=\"https://api.docverify.com/\">\n" +
                    "      <apiKey>upbvpmZu5eevouSJute8XqFlFZf6gE4Z</apiKey>\n" +
                    "      <apiSig>447194D52559259982BE55B7895BD843</apiSig>\n" +
                    "      <PacketID>" + packetId + "</PacketID>\n" +
                    "    </GetPacketDocs>\n" +
                    "     </soap:Body>\n" +
                    "</soap:Envelope>");
            outputStream.flush();
            outputStreamWriter.close();
            outputStream.close();
            connection.connect();

            BufferedInputStream bis = new BufferedInputStream(connection.getInputStream());
            ByteArrayOutputStream buf = new ByteArrayOutputStream();

            int result2 = bis.read();

            while (result2 != -1) {
                buf.write((byte) result2);
                result2 = bis.read();

            }

            String responseBody = buf.toString();

            JSONObject xmlJSONObj = XML.toJSONObject(responseBody);
            System.out.println(xmlJSONObj);

            String array = xmlJSONObj
                    .getJSONObject("soap:Envelope")
                    .getJSONObject("soap:Body")
                    .getJSONObject("GetPacketDocsResponse")
                    .getString("GetPacketDocsResult");
            String[] split = array.substring(1, array.length() - 1).split("},");
            List<ResDocArray> resDocArrays = new ArrayList<>();
            for (String s : split) {
                s = s.endsWith("}") ? s : s + "}";
                JSONObject jsonObject = new JSONObject(s);
                ResDocArray resDocArray = new ResDocArray();
                resDocArray.setName(jsonObject.getString("Name"));
                resDocArray.setDocVerifyId(jsonObject.getString("DocVerifyID"));
                resDocArrays.add(resDocArray);
            }
            return resDocArrays;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    //Bu methodda OrderId boyicha uni docVerifyOrqali tayyor documentlarini ResUploadFile ini olish mumkin
    public List<ResUploadFile> getResUploadFileListByOrderId(UUID orderId) {
        try {
            List<ResUploadFile> resUploadFileList = new ArrayList<>();
            Order order = orderRepository.findById(orderId).orElseThrow(() -> new ResourceNotFoundException("order", "id", orderId));
            List<Attachment> attachmentList = order.getFilesFromDocVerify();
            for (Attachment attachment : attachmentList) {
                resUploadFileList.add(new ResUploadFile(attachment.getId(),
                        attachment.getName(),
                        ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/attachment/pdf/").path(attachment.getId().toString()).toUriString(),
                        attachment.getContentType(),
                        attachment.getSize()));
            }
            return resUploadFileList;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    /**
     * Method return current free agent's id and timeTable's id for online order
     */
    public ApiResponse getFreeAgentIdNow(UUID tempId) {
        try {
            Timestamp startTime = new Timestamp(System.currentTimeMillis());
            Timestamp endTime = new Timestamp(startTime.getTime() + 30 * 60 * 1000);
            Optional<AgentSchedule> freeOnlineAgentNow = agentScheduleRepository.getFreeOnlineAgentNow(startTime, endTime, tempId);
            if (!freeOnlineAgentNow.isPresent())
                return new ApiResponse(false, null);
            Optional<TimeTable> tableOptional = timeTableRepository.findById(tempId);
            if (tableOptional.isPresent()) {
                TimeTable timeTable = tableOptional.get();
                timeTable.setAgent(freeOnlineAgentNow.get().getUser());
                timeTable.setFromTime(startTime);
                timeTable.setTillTime(endTime);
                TimeTable save = timeTableRepository.save(timeTable);
                return new ApiResponse(true, timeTableService.getTimeTableDto(save));
            }
            TimeTable timeTable = timeTableRepository.save(new TimeTable(freeOnlineAgentNow.get().getUser(), startTime, endTime, null, true, tempId, true));
            return new ApiResponse(true, timeTableService.getTimeTableDto(timeTable));
        } catch (Exception e) {
            return new ApiResponse(false, null);
        }
    }

    public ResPageable getOrdersByClient(int page, int size, UUID id, int count) {
        Object object = count == 4 ? orderRepository.getCountOfOrderByOrderStatus4(OrderStatus.NEW.name(), OrderStatus.IN_PROGRESS.name(), OrderStatus.COMPLETED.name(), OrderStatus.REJECTED.name(), id) : orderRepository.getCountOfOrderByOrderStatus3(OrderStatus.NEW.name(), OrderStatus.IN_PROGRESS.name(), OrderStatus.COMPLETED.name(), id);
        Object[] inside = (Object[]) object;
        int all = 0;
        for (int i = 0; i < count; i++) {
            all = Math.max(all, Integer.parseInt(inside[0].toString()));
        }
        if (count == 4)
            return new ResPageable(
                    page,
                    size,
                    all / size + 1,
                    Long.valueOf(String.valueOf(all)),
                    new ClientMainPage(
                            inside[0].toString(),
                            makeOrderList(orderRepository.findAllByAgent_IdAndOrderStatusAndActive(id, OrderStatus.NEW, true, PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt")))).getContent()),
                            inside[1].toString(),
                            makeOrderList(orderRepository.findAllByAgent_IdAndOrderStatusAndActive(id, OrderStatus.IN_PROGRESS, true, PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt")))).getContent()),
                            inside[2].toString(),
                            makeOrderList(orderRepository.findAllByAgent_IdAndOrderStatusAndActive(id, OrderStatus.COMPLETED, true, PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt")))).getContent()),
                            inside[3].toString(),
                            makeOrderList(orderRepository.findAllByAgent_IdAndOrderStatusAndActive(id, OrderStatus.REJECTED, true, PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt")))).getContent()),
                            canceledOrderRepository.findAllByToAgent_IdAndAcceptIsNull(id).stream().map(this::makeCanceledOrder).collect(Collectors.toList())));
        return new ResPageable(
                page,
                size,
                all / size + 1,
                Long.valueOf(String.valueOf(all)),
                new ClientMainPage(
                        inside[0].toString(),
                        makeOrderList(orderRepository.findAllByClient_IdAndOrderStatusAndActive(id, OrderStatus.NEW, true, PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt")))).getContent()),
                        inside[1].toString(),
                        makeOrderList(orderRepository.findAllByClient_IdAndOrderStatusAndActive(id, OrderStatus.IN_PROGRESS, true, PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt")))).getContent()),
                        inside[2].toString(),
                        makeOrderList(orderRepository.findAllByClient_IdAndOrderStatusAndActive(id, OrderStatus.COMPLETED, true, PageRequest.of(page, size, Sort.by(Sort.Order.desc("createdAt")))).getContent()),
                        null,
                        null, null));
    }

    public ApiResponse getOrdersGroupByAgent(UUID adminId, boolean online) {
        try {
            Optional<User> optionalUser = userRepository.findByIdAndEnabledTrue(adminId);
            if (optionalUser.isPresent()) {
                if (!checkRole.isAdmin(optionalUser.get().getRoles()) && !checkRole.isROLE_SUPER_ADMIN(optionalUser.get().getRoles()))
                    return new ApiResponse("You can not get", false);
                List<User> allAgents = userRepository.findAllByEnabledIsTrueAndRolesInAndOnlineAgent(roleRepository.findAllByRoleName(RoleName.ROLE_AGENT), online);
                List<OrderDto> orders = new ArrayList<>();
                for (User agent : allAgents) {
                    Optional<Order> optionalOrder = orderRepository.getOrderByAgent(agent.getId());
                    optionalOrder.ifPresent(order -> orders.add(makeOrderDto(optionalOrder.get())));
                }
                return new ApiResponse(true, orders);
            }
            return new ApiResponse("Error", false);
        } catch (Exception e) {
            return new ApiResponse("Server error", false);
        }
    }

    public ApiResponse getServiceForNow() {
        try {
            Optional<ServicePrice> inputTrue = servicePriceRepository.findByService_SubService_DefaultInputTrue();
            return inputTrue.map(servicePrice -> new ApiResponse(true, servicePrice)).orElseGet(() -> new ApiResponse(false, null));
        } catch (Exception e) {
            return new ApiResponse("DB ni to`ldiring", false);
        }
    }

    public List<UserDto> findAgentToAdmin(boolean online, UUID zipCodeId, Timestamp fromTime, Timestamp endTime) {
        if (new Timestamp(System.currentTimeMillis()).after(fromTime))
            return new ArrayList<>();
        List<User> allAgent = userRepository.getAllAgent(fromTime, endTime, zipCodeId, online);
        List<UserDto> result = new ArrayList<>();
        for (User user : allAgent) {
            result.add(userService.getUser(user));
        }
        return result;
    }

    public Object[] getCustomer(int page, int size) {
        return userRepository.findAllCustomerWithOrder(page, size);
    }

    public ApiResponse changeStatus(UUID id, String docId, boolean isPackage) {
        Order order = orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getOrder", "id", id));
        boolean isInProgress = false;
        if (order.getOrderStatus().equals(OrderStatus.NEW)) {
            order.setOrderStatus(OrderStatus.IN_PROGRESS);
            return new ApiResponse("Success", orderRepository.save(order));
        } else {
            order.setDocVerifyId(docId);
            order.setPacket(isPackage);
            if (order.getOrderStatus().equals(OrderStatus.IN_PROGRESS)) {
                order.setOrderStatus(OrderStatus.COMPLETED);
                isInProgress = true;
            }
            Order save = orderRepository.save(order);
            addDocverifyIdOrPackageIdFromDocverify(save.getId(), isPackage, docId);
            if (isInProgress)
                sharingDiscountService.addSharingDiscount(save);
            return new ApiResponse("ok", true);
        }
    }

    public ApiResponse changeAgent(UUID agentId, UUID orderId, boolean sender, boolean accept, UUID uuid) {
        try {
            Order order = orderRepository.findById(orderId).orElseThrow(() -> new ResourceNotFoundException("getOrder", "id", orderId));
            TimeTable timeTable = timeTableRepository.findByOrder(order).orElseThrow(() -> new ResourceNotFoundException("getTimeTable", "order", order));
            if (sender) {
                List<CanceledOrder> optionalCanceled = canceledOrderRepository.findByOrderAndAcceptIsNullAndFromAgent(order, order.getAgent());
                if (agentId.equals(order.getAgent().getId()) && optionalCanceled.isEmpty()) {
                    List<UserDto> freeAgents = findAgentToAdmin(
                            order.getServicePrice().getService().getMainService().isOnline(),
                            order.getZipCode() != null ? order.getZipCode().getId() : null,
                            timeTable.getFromTime(),
                            timeTable.getTillTime());
                    if (!freeAgents.isEmpty()) {
                        UserDto userDto = freeAgents.get(0);
                        canceledOrderRepository.save(new CanceledOrder(order.getAgent(), userRepository.findById(userDto.getId()).orElseThrow(() -> new ResourceNotFoundException("getUser", "id", userDto.getId())), order, null));
                        return new ApiResponse("You request was successfully send", true);
                    } else {
                        return new ApiResponse("Free agent not found", null);
                    }
                } else {
                    return new ApiResponse("You cannot do it", false);
                }
            } else {
                CanceledOrder canceledOrder = canceledOrderRepository.findById(uuid).orElseThrow(() -> new ResourceNotFoundException("getCanceledOrder", "id", uuid));
                User user = userRepository.findById(agentId).orElseThrow(() -> new ResourceNotFoundException("getUser", "id", agentId));
                if (canceledOrder.getToAgent().equals(user)) {
                    canceledOrder.setAccept(accept);
                    if (accept) {
                        Order toSave = canceledOrder.getOrder();
                        toSave.setAgent(user);
                        TimeTable table = timeTableRepository.findByOrder(toSave).orElseThrow(() -> new ResourceNotFoundException("getTimeTable", "order", toSave));
                        table.setAgent(user);
                        orderRepository.save(toSave);
                        timeTableRepository.save(table);
                    }
                    canceledOrderRepository.save(canceledOrder);
                    return new ApiResponse("ok", true);
                } else {
                    return new ApiResponse("You cannot do it", false);
                }
            }
        } catch (Exception e) {
            return null;
        }
    }

    public CanceledOrderDto makeCanceledOrder(CanceledOrder canceledOrder) {
        return new CanceledOrderDto(
                canceledOrder.getId(),
                userService.getUser(canceledOrder.getFromAgent()),
                userService.getUser(canceledOrder.getToAgent()),
                makeOrderDto(canceledOrder.getOrder()),
                canceledOrder.getAccept()
        );
    }

    //=====================================Adbdurahmonni ishi===========================================//
    final
    FirstOrderDiscountTariffRepository firstOrderDiscountTariffRepository;

    public List<ClientDiscountsDto> getAllClientDiscount(OrderDto order) {
        if (order.getId() != null) {
            Order order1 = orderRepository.findById(order.getId()).orElseThrow(() -> new ResourceNotFoundException("getOrder", "id", order.getId()));
            if (!clientUserDiscountsDelete(order1))
                return new ArrayList<>();
        }
        ServicePrice servicePrice = servicePriceRepository.findById(order.getServicePriceId()).orElseThrow(() -> new ResourceNotFoundException("service price", "", ""));
        Double orderAmount = amountOrderWithoutDiscount(servicePrice, order);
        UUID clientId = order.getClientId();
        if (order.isNewUser()) {
            Optional<User> user = userRepository.findByEmailOrPhoneNumberAndPassword(
                    order.getUsername(), order.getUsername(), passwordEncoder.encode(order.getPassword())
            );
            if (user.isPresent()) {
                clientId = user.get().getId();
            } else {
                return null;
            }
        }
        List<Order> clientOrders = orderRepository.findAllByClient_Id(clientId);
        List<CustomDiscountTariff> customDiscounts = customDiscountTariffRepository.findAllByUserId(clientId);
        List<SharingDiscount> sharingDiscounts = sharingDiscountRepository.findAllByClient_IdAndLeftoverIsNotNull(clientId);
        List<LoyaltyDiscount> loyaltyDiscounts = loyaltyDiscountRepository.getUserLoyaltyDiscount(clientId);
        double allSum;
        List<ClientDiscountsDto> clientDiscountsDtoList = new ArrayList<>();
        Double percent;
        if (servicePriceRepository.existsByIdAndService_MainService_OnlineIsTrue(order.getServicePriceId())) {
            percent = discountPercentRepository.findByOnlineIsTrueAndActiveIsTrue()
                    .orElse(new DiscountPercent(0d, null, false, false)).getPercent();
        } else {
            percent = discountPercentRepository.findByZipCode_IdAndActiveIsTrue(order.getZipCodeId())
                    .orElse(new DiscountPercent(0d, null, false, false)).getPercent();
        }
        if (clientOrders.isEmpty() || (clientOrders.size() == 1 && order.getId() != null && clientOrders.get(0).getId() == order.getId())) {
            if (servicePriceRepository.existsByIdAndService_MainService_OnlineIsTrue(order.getServicePriceId())) {
                Optional<FirstOrderDiscountTariff> byOnlineIsTrueAndActiveIsTrue = firstOrderDiscountTariffRepository.findByOnlineIsTrueAndActiveIsTrue();
                byOnlineIsTrueAndActiveIsTrue.ifPresent(firstOrderDiscountTariff -> clientDiscountsDtoList.add(
                        new ClientDiscountsDto(firstOrderDiscountTariff.getId(), "Discount for first order",
                                Double.parseDouble(formatDecimal((orderAmount / 100) * firstOrderDiscountTariff.getPercent())),
                                orderAmount,
                                Double.parseDouble(formatDecimal((orderAmount / 100) * percent)),
                                (Double.parseDouble(formatDecimal((orderAmount / 100) * percent)) <=
                                        (Double.parseDouble(formatDecimal((orderAmount / 100) * firstOrderDiscountTariff.getPercent()))))
                                , percent
                        )));
            } else {
                Optional<ZipCode> zipCode = zipCodeRepository.findById(order.getZipCodeId());
                if (zipCode.isPresent()) {
                    Optional<FirstOrderDiscountTariff> byZipCodeCodeAndActive = firstOrderDiscountTariffRepository.findByZipCodeAndActive(zipCode.get(), true);
                    byZipCodeCodeAndActive.ifPresent(firstOrderDiscountTariff -> clientDiscountsDtoList.add(
                            new ClientDiscountsDto(firstOrderDiscountTariff.getId(), "Discount for first order",
                                    Double.parseDouble(formatDecimal((orderAmount / 100) * firstOrderDiscountTariff.getPercent())),
                                    orderAmount,
                                    Double.parseDouble(formatDecimal((orderAmount / 100) * percent)),
                                    (Double.parseDouble(formatDecimal((orderAmount / 100) * percent)) <=
                                            (Double.parseDouble(formatDecimal((orderAmount / 100) * firstOrderDiscountTariff.getPercent()))))
                                    , percent
                            )));
                }
            }
        }
        if (customDiscounts != null && customDiscounts.size() > 0) {
            for (CustomDiscountTariff customDiscountTariff : customDiscounts) {
                if (customDiscountTariff.isUnlimited()) {
                    clientDiscountsDtoList.add(
                            new ClientDiscountsDto(customDiscountTariff.getId(), "Custom Discount",
                                    Double.parseDouble(formatDecimal((orderAmount / 100) * customDiscountTariff.getPercent())),
                                    orderAmount,
                                    Double.parseDouble(formatDecimal((orderAmount / 100) * percent)),
                                    ((Double.parseDouble(formatDecimal((orderAmount / 100) * percent))) <=
                                            Double.parseDouble(formatDecimal((orderAmount / 100) * customDiscountTariff.getPercent())))
                                    , percent
                            ));
                    break;
                } else if (!customDiscountTariff.getDiscountGivenOrderCount().equals(customDiscountTariff.getUsingDiscountGivingOrderCount())) {
                    clientDiscountsDtoList.add(
                            new ClientDiscountsDto(customDiscountTariff.getId(), "Custom Discount",
                                    Double.parseDouble(formatDecimal((orderAmount / 100) * customDiscountTariff.getPercent())),
                                    orderAmount,
                                    Double.parseDouble(formatDecimal((orderAmount / 100) * percent)),
                                    ((Double.parseDouble(formatDecimal((orderAmount / 100) * percent))) <=
                                            Double.parseDouble(formatDecimal((orderAmount / 100) * customDiscountTariff.getPercent())))
                                    , percent
                            ));
                    break;
                }
            }
        }
        allSum = 0.0;
        if (sharingDiscounts != null && sharingDiscounts.size() > 0) {
            for (SharingDiscount sharingDiscount : sharingDiscounts) {
                if (sharingDiscount.getLeftover() > 0) {
                    allSum += sharingDiscount.getLeftover();
                }
            }
            if (allSum > 0) {
                clientDiscountsDtoList.add(
                        new ClientDiscountsDto("Sharing discounts",
                                Double.parseDouble(formatDecimal(allSum)),
                                orderAmount,
                                Double.parseDouble(formatDecimal((orderAmount / 100) * percent)),
                                (Double.parseDouble(formatDecimal((orderAmount / 100) * percent)) <= ((allSum))),
                                percent));
            }
        }
        allSum = 0.0;
        if (loyaltyDiscounts != null && loyaltyDiscounts.size() > 0) {
            for (LoyaltyDiscount loyaltyDiscount : loyaltyDiscounts) {
                if (loyaltyDiscount.getLeftover() > 0) {
                    allSum += loyaltyDiscount.getLeftover();
                }
            }
        }
        if (allSum > 0) {
            clientDiscountsDtoList.add(
                    new ClientDiscountsDto("Loyalty discounts",
                            Double.parseDouble(formatDecimal(allSum)),
                            orderAmount,
                            Double.parseDouble(formatDecimal((orderAmount / 100) * percent)),
                            (Double.parseDouble(formatDecimal((orderAmount / 100) * percent)) <= ((allSum))),
                            percent));
        }
        return clientDiscountsDtoList;
    }

    final
    DiscountExpenseRepository discountExpenseRepository;

    public boolean clientUserDiscounts(OrderDto order, List<ClientDiscountsDto> discountsDtos, Order order2) {
        ServicePrice servicePrice = servicePriceRepository.findById(order.getServicePriceId()).orElseThrow(() -> new ResourceNotFoundException("service price", "", ""));
        Double orderAmount = amountOrderWithoutDiscount(servicePrice, order);
        UUID clientId = order.getClientId();
        if (order.isNewUser()) {
            Optional<User> user = userRepository.findByEmailOrPhoneNumberAndPassword(
                    order.getUsername(), order.getUsername(), passwordEncoder.encode(order.getPassword())
            );
            if (user.isPresent()) {
                clientId = user.get().getId();
            } else {
                return false;
            }
        }
        try {
            if (discountsDtos != null && discountsDtos.size() > 0) {
                int counter = 0;
                Double percent;
                if (servicePriceRepository.existsByIdAndService_MainService_OnlineIsTrue(order.getServicePriceId())) {
                    percent = discountPercentRepository.findByOnlineIsTrueAndActiveIsTrue()
                            .orElse(new DiscountPercent(0d, null, false, false)).getPercent();
                } else {
                    percent = discountPercentRepository.findByZipCode_IdAndActiveIsTrue(order.getZipCodeId())
                            .orElse(new DiscountPercent(0d, null, false, false)).getPercent();
                }
                double needDiscount = ((orderAmount / 100) * percent);
                String s = formatDecimal(needDiscount);
                needDiscount = Double.parseDouble(s);
                for (ClientDiscountsDto discountsDto : discountsDtos) {
                    if (needDiscount == 0.0) {
                        return true;
                    }
                    if ((discountsDto.getNumber() == counter) || discountsDtos.size() == 1) {
                        counter++;
                        switch (discountsDto.getDiscountName()) {
                            case "Custom Discount":
                                List<CustomDiscountTariff> customDiscounts = customDiscountTariffRepository.findAllByUserId(clientId);
                                for (CustomDiscountTariff customDiscount : customDiscounts) {
                                    if (customDiscount.getId().equals(discountsDto.getDiscountId())) {
                                        if (!customDiscount.isUnlimited()) {
                                            if (discountsDto.getAmount() >= needDiscount) {
                                                discountExpenseRepository.save(new DiscountExpense(
                                                        order2, needDiscount,
                                                        customDiscount.getId(), false, false, true
                                                ));
                                                needDiscount = 0.0;
                                            } else {
                                                needDiscount -= discountsDto.getAmount();
                                                discountExpenseRepository.save(new DiscountExpense(
                                                        order2, discountsDto.getAmount(),
                                                        customDiscount.getId(), false, false, true
                                                ));
                                            }
                                            customDiscount.setUsingDiscountGivingOrderCount(customDiscount.getUsingDiscountGivingOrderCount() + 1);
                                            customDiscountTariffRepository.save(customDiscount);
                                            break;
                                        } else {
                                            if (discountsDto.getAmount() >= needDiscount) {
                                                discountExpenseRepository.save(new DiscountExpense(
                                                        order2, needDiscount,
                                                        customDiscount.getId(), false, false, true
                                                ));
                                                needDiscount = 0.0;

                                            } else {
                                                needDiscount -= discountsDto.getAmount();
                                                discountExpenseRepository.save(new DiscountExpense(
                                                        order2, discountsDto.getAmount(),
                                                        customDiscount.getId(), false, false, true
                                                ));
                                            }
                                            break;
                                        }
                                    }
                                }
                                break;
                            case "Sharing discounts":
                                List<SharingDiscount> sharingDiscounts = sharingDiscountRepository.findAllByClient_IdAndLeftoverIsNotNull(clientId);
                                for (SharingDiscount sharingDiscount : sharingDiscounts) {
                                    if (sharingDiscount.getLeftover() > 0.00) {
                                        if (sharingDiscount.getLeftover() >= needDiscount) {
                                            sharingDiscount.setLeftover(sharingDiscount.getLeftover() - needDiscount);
                                            sharingDiscountRepository.save(sharingDiscount);
                                            discountExpenseRepository.save(new DiscountExpense(
                                                    order2, needDiscount,
                                                    sharingDiscount.getId(), true, false, false
                                            ));
                                            return true;
                                        } else {
                                            needDiscount -= sharingDiscount.getLeftover();
                                            discountExpenseRepository.save(new DiscountExpense(
                                                    order2, sharingDiscount.getLeftover(),
                                                    sharingDiscount.getId(), true, false, false
                                            ));
                                            sharingDiscount.setLeftover(0.0);
                                        }
                                        sharingDiscountRepository.save(sharingDiscount);
                                    }
                                }
                                break;
                            case "Discount for first order":
                                List<Order> clientOrders = orderRepository.findAllByClient_Id(clientId);
                                if (clientOrders.size() == 1) {
                                    if (servicePriceRepository.existsByIdAndService_MainService_OnlineIsTrue(order.getServicePriceId())) {
                                        Optional<FirstOrderDiscountTariff> firstOrderDiscountTariff = firstOrderDiscountTariffRepository.findByOnlineIsTrueAndActiveIsTrue();
                                        if (firstOrderDiscountTariff.isPresent()) {
                                            discountExpenseRepository.save(new DiscountExpense(
                                                    order2, (discountsDto.isNeeded() ? discountsDto.getNeedAmount() : discountsDto.getAmount()),
                                                    firstOrderDiscountTariff.get().getId(), false, false, false, true
                                            ));
                                        } else {
                                            return false;
                                        }
                                        if (discountsDto.isNeeded()) {
                                            return true;
                                        } else {
                                            if (needDiscount <= discountsDto.getAmount()) {
                                                return true;
                                            } else {
                                                needDiscount -= discountsDto.getAmount();
                                            }
                                        }
                                    } else {
                                        Optional<ZipCode> zipCode = zipCodeRepository.findById(order.getZipCodeId());
                                        if (zipCode.isPresent()) {
                                            Optional<FirstOrderDiscountTariff> firstOrderDiscountTariff = firstOrderDiscountTariffRepository.findByZipCodeAndActive(zipCode.get(), true);
                                            if (firstOrderDiscountTariff.isPresent()) {
                                                discountExpenseRepository.save(new DiscountExpense(
                                                        order2, (discountsDto.isNeeded() ? discountsDto.getNeedAmount() : discountsDto.getAmount()),
                                                        firstOrderDiscountTariff.get().getId(), false, false, false, true
                                                ));
                                            } else {
                                                return false;
                                            }
                                            if (discountsDto.isNeeded()) {
                                                return true;
                                            } else {
                                                if (needDiscount <= discountsDto.getAmount()) {
                                                    return true;
                                                } else {
                                                    needDiscount -= discountsDto.getAmount();
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            case "Loyalty discounts":
                                List<LoyaltyDiscount> loyaltyDiscounts = loyaltyDiscountRepository.getUserLoyaltyDiscount(clientId);
                                for (LoyaltyDiscount loyaltyDiscount : loyaltyDiscounts) {
                                    if (loyaltyDiscount.getLeftover() > 0.00) {
                                        if (loyaltyDiscount.getLeftover() >= needDiscount) {
                                            loyaltyDiscount.setLeftover(loyaltyDiscount.getLeftover() - needDiscount);
                                            loyaltyDiscountRepository.save(loyaltyDiscount);
                                            discountExpenseRepository.save(new DiscountExpense(
                                                    order2, needDiscount,
                                                    loyaltyDiscount.getId(), false, true, false
                                            ));
                                            return true;
                                        } else {
                                            needDiscount -= loyaltyDiscount.getLeftover();
                                            discountExpenseRepository.save(new DiscountExpense(
                                                    order2, loyaltyDiscount.getLeftover(),
                                                    loyaltyDiscount.getId(), false, true, false
                                            ));
                                            loyaltyDiscount.setLeftover(0.0);
                                        }
                                        loyaltyDiscountRepository.save(loyaltyDiscount);
                                    }
                                }
                                break;
                        }
                    }
                }

                double needDiscount2 = ((orderAmount / 100) * percent);
                String s2 = formatDecimal(needDiscount2);
                needDiscount2 = Double.parseDouble(s2);
                return needDiscount != needDiscount2;
            }
            return false;
        } catch (Exception ignored) {
            return !clientUserDiscountsDelete(order2);
        }

    }

    public boolean clientUserDiscountsDelete(Order order) {
        try {
            List<DiscountExpense> allExpenseDiscounts = discountExpenseRepository.findAllByOrder_Id(order.getId());
            for (DiscountExpense discount : allExpenseDiscounts) {
                if (discount.isFirstOrder()) {
                    discountExpenseRepository.deleteById(discount.getId());
                } else if (discount.isCustomId()) {
                    CustomDiscountTariff customDiscountTariff = customDiscountTariffRepository.findById(discount.getDiscountId()).orElseThrow(() -> new ResourceNotFoundException("getCustomDiscount", "", ""));
                    if (!customDiscountTariff.isUnlimited()) {
                        customDiscountTariff.setUsingDiscountGivingOrderCount(customDiscountTariff.getUsingDiscountGivingOrderCount() > 0 ? customDiscountTariff.getUsingDiscountGivingOrderCount() - 1 : 0);
                        customDiscountTariffRepository.save(customDiscountTariff);
                    }
                } else if (discount.isSharingId()) {
                    SharingDiscount getSharingDiscount = sharingDiscountRepository.findById(discount.getId()).orElseThrow(() -> new ResourceNotFoundException("getSharingDiscount", "", ""));
                    getSharingDiscount.setLeftover(getSharingDiscount.getLeftover() > 0 ? getSharingDiscount.getLeftover() + discount.getAmount() : discount.getAmount());
                    sharingDiscountRepository.save(getSharingDiscount);
                } else if (discount.isLoyaltyId()) {
                    LoyaltyDiscount getLoyaltyDiscount = loyaltyDiscountRepository.findById(discount.getId()).orElseThrow(() -> new ResourceNotFoundException("getLoyaltyDiscount", "", ""));
                    getLoyaltyDiscount.setLeftover(getLoyaltyDiscount.getLeftover() > 0 ? getLoyaltyDiscount.getLeftover() + discount.getAmount() : discount.getAmount());
                    loyaltyDiscountRepository.save(getLoyaltyDiscount);
                }
                discountExpenseRepository.deleteById(discount.getId());
            }
            allExpenseDiscounts = discountExpenseRepository.findAllByOrder_Id(order.getId());
            return allExpenseDiscounts == null || allExpenseDiscounts.size() <= 0;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean clientUserEnoughDiscounts(OrderDto order, List<ClientDiscountsDto> discountsDtos) {
        ServicePrice servicePrice = servicePriceRepository.findById(order.getServicePriceId()).orElseThrow(() -> new ResourceNotFoundException("service price", "", ""));
        Double orderAmount = amountOrderWithoutDiscount(servicePrice, order);
        UUID clientId = order.getClientId();
        if (order.isNewUser()) {
            Optional<User> user = userRepository.findByEmailOrPhoneNumberAndPassword(
                    order.getUsername(), order.getUsername(), passwordEncoder.encode(order.getPassword())
            );
            if (user.isPresent()) {
                clientId = user.get().getId();
            } else {
                return false;
            }
        }

        if (discountsDtos != null && discountsDtos.size() > 0) {
            int counter = 0;
            Double percent;
            if (servicePriceRepository.existsByIdAndService_MainService_OnlineIsTrue(order.getServicePriceId())) {
                percent = discountPercentRepository.findByOnlineIsTrueAndActiveIsTrue()
                        .orElse(new DiscountPercent(0d, null, false, false)).getPercent();
            } else {
                percent = discountPercentRepository.findByZipCode_IdAndActiveIsTrue(order.getZipCodeId())
                        .orElse(new DiscountPercent(0d, null, false, false)).getPercent();
            }
            double needDiscount = ((orderAmount / 100) * percent);
            String s = formatDecimal(needDiscount);
            needDiscount = Double.parseDouble(s);
            for (ClientDiscountsDto discountsDto : discountsDtos) {
                if (needDiscount == 0.0) {
                    return true;
                }
                if (discountsDto.getNumber() == counter) {
                    counter++;
                    switch (discountsDto.getDiscountName()) {
                        case "Custom Discount":
                            List<CustomDiscountTariff> customDiscounts = customDiscountTariffRepository.findAllByUserId(clientId);
                            for (CustomDiscountTariff customDiscount : customDiscounts) {
                                if (customDiscount.getId().equals(discountsDto.getDiscountId())) {
                                    if (discountsDto.getAmount() >= needDiscount) {
                                        return true;

                                    } else {
                                        needDiscount -= discountsDto.getAmount();
                                    }
                                    break;
                                }
                            }
                            break;
                        case "Sharing discounts":
                            List<SharingDiscount> sharingDiscounts = sharingDiscountRepository.findAllByClient_IdAndLeftoverIsNotNull(clientId);
                            for (SharingDiscount sharingDiscount : sharingDiscounts) {
                                if (sharingDiscount.getLeftover() > 0) {
                                    if (sharingDiscount.getLeftover() >= needDiscount) {
                                        return true;
                                    } else {
                                        needDiscount -= sharingDiscount.getLeftover();
                                    }
                                }
                            }
                            break;
                        case "Discount for first order":
                            List<Order> clientOrders = orderRepository.findAllByClient_Id(clientId);
                            if (clientOrders.size() == 1) {
                                if (servicePriceRepository.existsByIdAndService_MainService_OnlineIsTrue(order.getServicePriceId())) {
                                    if (discountsDto.isNeeded()) {
                                        return true;
                                    } else {
                                        if (needDiscount <= discountsDto.getAmount()) {
                                            return true;
                                        } else {
                                            needDiscount -= discountsDto.getAmount();
                                        }
                                    }
                                } else {
                                    Optional<ZipCode> zipCode = zipCodeRepository.findById(order.getZipCodeId());
                                    if (zipCode.isPresent()) {
                                        if (discountsDto.isNeeded()) {
                                            needDiscount = 0.0;
                                        } else {
                                            if (needDiscount <= discountsDto.getAmount()) {
                                                return false;
                                            } else {
                                                needDiscount -= discountsDto.getAmount();
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        case "Loyalty discounts":
                            List<LoyaltyDiscount> loyaltyDiscounts = loyaltyDiscountRepository.getUserLoyaltyDiscount(clientId);
                            for (LoyaltyDiscount loyaltyDiscount : loyaltyDiscounts) {
                                if (loyaltyDiscount.getLeftover() > 0) {
                                    if (loyaltyDiscount.getLeftover() >= needDiscount) {
                                        loyaltyDiscountRepository.save(loyaltyDiscount);
                                        return false;
                                    } else {
                                        needDiscount -= loyaltyDiscount.getLeftover();
                                    }
                                }
                            }
                            break;
                    }
                }
            }
            return true;
        }
        return false;
    }

    public String formatDecimal(double number) {
        float epsilon = 0.004f; // 4 tenths of a cent
        if (Math.abs(Math.round(number) - number) < epsilon) {
            return String.format("%10.0f", number); // sdb
        } else {
            return String.format("%10.2f", number); // dj_segfault
        }
    }

    public ApiResponse addDocument(UUID orderId, UUID attach) {
        Optional<Order> order = orderRepository.findById(orderId);
        if (!order.isPresent()) return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        Optional<Attachment> attachment = attachmentRepository.findById(attach);
        if (!attachment.isPresent()) return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        order.get().getDocuments().add(attachment.get());
        Order save = orderRepository.save(order.get());
        return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true, save);
    }

    public ApiResponse deleteDocument(UUID orderId, UUID attach) {
        Optional<Order> order = orderRepository.findById(orderId);
        if (!order.isPresent()) return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        Optional<Attachment> attachment = attachmentRepository.findById(attach);
        if (!attachment.isPresent()) return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        order.get().getDocuments().remove(attachment.get());
        attachmentService.deleteAttachment(attach);
        Order save = orderRepository.save(order.get());
        return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true, save);
    }

    public ApiResponse findZipCodeAndServicePrice(String code) {
        Optional<ZipCode> zipCodeOptional = zipCodeRepository.findByCodeAndActiveTrue(code);
        if (zipCodeOptional.isPresent()) {
            ZipCode zipCode = zipCodeOptional.get();
            List<ServicePrice> servicePrices = servicePriceRepository.findAllByZipCodeAndActiveAndInPerson(zipCode.getId());
            return new ApiResponse("Success", true, new ResAddress(new ZipCodeDto(zipCode.getId()), servicePrices));
        }
        return new ApiResponse("Error", false);
    }

    public ApiResponse searchOrderBySerialNumber(String serialNumber) {
        try {
            List<OrderSearchDto> collect = orderRepository.searchBySerialNumber(serialNumber).stream().map(this::getOrderSearchDto).collect(Collectors.toList());
            return new ApiResponse("Orders list", true, collect);
        } catch (Exception e) {
            return new ApiResponse("Orders not found", false);
        }
    }

    public Object getOrdersFilter(String zipCode, Date date, String status) {
        if (zipCode != null && date != null && status != null) {
            return makeOrderList(orderRepository.getFilterByStatusAndDateAndZipCode(date, status, zipCode));
        } else if (zipCode != null && status == null && date == null) {
            return makeOrderList(orderRepository.getFilterByZipCode(zipCode));
        } else if (date != null && status == null && zipCode == null) {
            return makeOrderList(orderRepository.getFilterByDate(date));
        } else if (date != null && zipCode != null) {
            return makeOrderList(orderRepository.getFilterByDateAndZipCode(date, zipCode));
        } else if (date != null) {
            return makeOrderList(orderRepository.getFilterByStatusAndDate(date, status));
        } else if (zipCode != null) {
            return makeOrderList(orderRepository.getFilterByStatusAndZipCode(status, zipCode));
        } else if (status != null) {
            return makeOrderList(orderRepository.getFilterByStatus(status));
        }
        return getOrderList(Integer.parseInt(AppConstants.DEFAULT_PAGE_NUMBER), Integer.parseInt(AppConstants.DEFAULT_PAGE_NUMBER));
    }

    public Object getOrdersBySearch(String search, int page, int size) {
        Long aLong = orderRepository.countBySearch(search);
        return new ResPageable(
                page, size, (int) (aLong / size), aLong, orderRepository.getBySearch(search.toLowerCase(), page, size).stream().map(this::makeOrderDto).collect(Collectors.toList()));
    }

    public Object getOrdersBySearchForClient(String search,  int page, int size, User user) {
        Long aLong = orderRepository.countBySearchForClient(search, user.getId());
        return new ResPageable(
                page, size, (int) (aLong / size), aLong,
                orderRepository.getBySearchForClient(search.toLowerCase(), user.getId(), page, size)
                        .stream().map(this::makeOrderDto).collect(Collectors.toList()));
    }

    public Object getOrdersBySearchForAgent(String search,  int page, int size, User user) {
        Long aLong = orderRepository.countBySearchForAgentId(search, user.getId());
        return new ResPageable(
                page, size, (int) (aLong / size), aLong,
                orderRepository.getBySearchForAgentId(search.toLowerCase(), user.getId(), page, size)
                        .stream().map(this::makeOrderDto).collect(Collectors.toList()));
    }

    private OrderSearchDto getOrderSearchDto(Object[] obj) {
        return new OrderSearchDto(
                obj[0] != null ? UUID.fromString(obj[0].toString()) : null,
                obj[1] != null ? obj[1].toString() : ""
        );
    }

    public ResPageable getOrderListByStatus(OrderStatus orderStatus, User user, int page, int size) {
        Page<TimeTable> timeTables;
        if (checkRole.isAdmin(user.getRoles()))
            timeTables = timeTableRepository.findAllByOrder_OrderStatusAndOrder_Active(orderStatus, true, PageRequest.of(page, size, Sort.by(Sort.Order.desc("fromTime"))));
        else
            timeTables = timeTableRepository.findAllByOrder_OrderStatusAndOrder_ClientOrOrder_AgentAndOrder_Active(orderStatus, user, user, true, PageRequest.of(page, size, Sort.by(Sort.Order.desc("fromTime"))));

        List<TimeTable> content = timeTables.getContent();
        List<OrderDto> orders = new ArrayList<>();
        for (TimeTable timeTable : content) {
            Order order = timeTable.getOrder();
            OrderDto orderDto = new OrderDto();
            orderDto.setId(timeTable.getOrder().getId());
            orderDto.setClient(this.userService.getUser(order.getClient()));
            orderDto.setAgent(this.userService.getUser(order.getAgent()));
            orderDto.setAmount(order.getAmount());
            orderDto.setDiscountAmount(order.getAmountDiscount());
            orderDto.setAddress(order.getAddress());
            orderDto.setZipCode(zipCodeService.getZipCode(order.getZipCode()));
            orderDto.setLat(order.getLat());
            orderDto.setLng(order.getLng());
            orderDto.setSerialNumber(order.getSerialNumber());
            orderDto.setTitleDocument(order.getTitleDocument());
            orderDto.setOrderAdditionalServiceDtoList(orderAdditionalServiceService.makeOrderAddSerDtoList(orderAdditionalServiceRepository.findAllByOrder(order)));
            Optional<Payment> byOrder = paymentRepository.findByOrder(order);
            orderDto.setResUploadFiles(getResUploadFileListByOrderId(order.getId()));
            if (byOrder.isPresent()) {
                Payment payment = byOrder.get();
                if (payment.getPayStatus().equals(PayStatus.PAYED))
                    orderDto.setPayed(true);
                if (payment.getPayStatus().equals(PayStatus.STRIPE))
                    orderDto.setStripe(true);
            }
            realEstateRepository.findByOrder(order).ifPresent(estate -> orderDto.setRealEstateDto(realEstateService.getRealEstate(estate)));
            internationalRepository.findByOrder(order).ifPresent(international -> orderDto.setInternationalDto(internationalService.getInternational(international)));
            orderDto.setFeedbackStatus(getFeedBackStatus(order));
            orderDto.setTimeTableDto(getTimeTableDto(timeTable));
            orderDto.setServicePrice(
                    new ServicePriceDto(
                            order.getServicePrice().getService().getMainService().getName(),
                            order.getServicePrice().getService().getSubService().getName(),
                            order.getServicePrice().getService().getMainService().isOnline()
                    ));

            orders.add(orderDto);
        }
        return new ResPageable(page, size, timeTables.getTotalPages(), timeTables.getTotalElements(), orders);
    }


    private TimeTableDto getTimeTableDto(TimeTable timeTable) {
        return new TimeTableDto(
                timeTable.getId(),
                timeTable.getFromTime().toString(),
                timeTable.getTillTime().toString(),
                null,
                null,
                timeTable.isTempBooked(),
                timeTable.getTempId(),
                timeTable.isOnline(),
                timeTable.getCreatedAt()
        );
    }
}