package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.CustomDiscount;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.CustomDiscountDto;
import ai.ecma.duoserver.payload.ResPageable;
import ai.ecma.duoserver.repository.CustomDiscountRepository;
import ai.ecma.duoserver.repository.OrderRepository;
import ai.ecma.duoserver.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CustomDiscountService {
    final
    CustomDiscountRepository customDiscountRepository;
    final
    OrderRepository orderRepository;
    final
    MailSenderService mailSenderService;

    public CustomDiscountService(CustomDiscountRepository customDiscountRepository, OrderRepository orderRepository, MailSenderService mailSenderService) {
        this.customDiscountRepository = customDiscountRepository;
        this.orderRepository = orderRepository;
        this.mailSenderService = mailSenderService;
    }

    /**
     * CustomDiscount ni qo'shish uchun method
     *
     * @param dto
     * @return ApiResponse object=null
     */
    public ApiResponse addCustomDiscount(CustomDiscountDto dto) {
        try {
            mailSenderService.sendClientCustomDiscount(customDiscountRepository.save(makeCustomDiscount(dto, new CustomDiscount())), "Added");
            return new ApiResponse("saved", true);
        } catch (Exception e) {
            return new ApiResponse("an error occurred during storage", false);
        }
    }

    /**
     * CustomDiscount ni tahrirlash uchun method
     *
     * @param discountDto
     * @return ApiResponse object=null
     */
    public ApiResponse editCustomDiscount(CustomDiscountDto discountDto) {
        try {
            mailSenderService.sendClientCustomDiscount(customDiscountRepository.save(makeCustomDiscount(discountDto, customDiscountRepository.findById(discountDto.getId()).orElseThrow(() -> new ResourceNotFoundException("getCustomDiscount", "id", discountDto.getId())))), "Edited");
            return new ApiResponse("edited", true);
        } catch (Exception e) {
            return new ApiResponse("an error occurred during storage", false);
        }
    }

    /**
     * CustomDiscountni dtoga o'tqazuchi method
     *
     * @param customDiscount
     * @return CustomDiscountDto
     */
    public CustomDiscountDto getCustomDiscount(CustomDiscount customDiscount) {
        return new CustomDiscountDto(
                customDiscount.getId(),
                null,
//                orderService.makeOrderDto(customDiscount.getOrder()),
                customDiscount.getPercent(),
                customDiscount.getAmount(),
                customDiscount.getDescription()
        );
    }

    /**
     * Bir Clientni barcha CustomDiscountlarni qaytarish
     *
     * @param userId UserId
     * @return List<CustomDiscount>
     */
    public List<CustomDiscountDto> getAllByUserId(UUID userId) {
        return customDiscountRepository.findAllByUserId(userId).stream().map(this::getCustomDiscount).collect(Collectors.toList());
    }

    /**
     * CustomDiscountlarni barchasini qaytaradi faqat Pageable qilib
     *
     * @param page
     * @param size
     * @return ResPageable
     */
    public ResPageable getCustomDiscountList(int page, int size) {
        Page<CustomDiscount> list = customDiscountRepository.findAll(PageRequest.of(page, size, Sort.by(AppConstants.ASC_OR_DECK, AppConstants.COLUMN_NAME_FOR_SORT)));
        return new ResPageable(page, size, list.getTotalPages(), list.getTotalElements(), list.getContent().stream().map(
                this::getCustomDiscount
        ).collect(Collectors.toList()));
    }

    /**
     * CustomDiscountDto to CustomDiscount
     *
     * @param customDiscountDto
     * @param customDiscount
     * @return
     */
    public CustomDiscount makeCustomDiscount(CustomDiscountDto customDiscountDto, CustomDiscount customDiscount) {
        customDiscount.setOrder(orderRepository.findById(customDiscountDto.getOrderDto().getId()).orElseThrow(() -> new ResourceNotFoundException("getOrder", "OrederId", customDiscountDto.getOrderDto().getId())));
        customDiscount.setPercent(customDiscountDto.getPercent());
        customDiscount.setAmount(customDiscountDto.getAmount());
        customDiscount.setDescription(customDiscountDto.getDescription());
        return customDiscount;
    }

}
