package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.*;
import ai.ecma.duoserver.entity.enums.RoleName;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.*;
import ai.ecma.duoserver.repository.*;
import ai.ecma.duoserver.security.CurrentUser;
import ai.ecma.duoserver.utils.AppConstants;
import ai.ecma.duoserver.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DiscountService {

    @Autowired
    SharingDiscountTariffRepository sharingDiscountTariffRepository;

    @Autowired
    LoyaltyDiscountTariffRepository loyaltyDiscountTariffRepository;

    @Autowired
    SharingDiscountRepository sharingDiscountRepository;

    @Autowired
    LoyaltyDiscountRepository loyaltyDiscountRepository;

    @Autowired
    DiscountExpenseRepository discountExpenseRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ZipCodeService zipCodeService;

    @Autowired
    CustomDiscountRepository customDiscountRepository;

    @Autowired
    ZipCodeRepository zipCodeRepository;

    @Autowired
    CountyRepository countyRepository;

    @Autowired
    StateRepository stateRepository;

    @Autowired
    FirstOrderDiscountTariffRepository firstOrderDiscountTariffRepository;

    @Autowired
    CustomDiscountTariffRepository customDiscountTariffRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    ServicePriceRepository servicePriceRepository;
    @Autowired
    DiscountPercentRepository discountPercentRepository;

    public ApiResponse editDiscountPercent(DiscountPercentDto discountPercentDto, UUID id) {
        Optional<DiscountPercent> optionalDiscountPercent = discountPercentRepository.findById(id);
        if (optionalDiscountPercent.isPresent()) {
            DiscountPercent discountPercent = optionalDiscountPercent.get();
            if (discountPercentDto.isOnline()) {
                if (discountPercentRepository.existsByOnlineIsTrueAndZipCodeIsNullAndIdNot(discountPercentDto.getId())) {
                    return new ApiResponse("This online discount percent exist", false);
                } else {
                    return makeDiscountPercent(discountPercentDto, discountPercent);
                }
            } else {
                return makeDiscountPercent(discountPercentDto, discountPercent);
            }
        }
        return new ApiResponse("Not found discount percent");
    }

    public ApiResponse makeDiscountPercent(DiscountPercentDto discountPercentDto, DiscountPercent discountPercent) {
        discountPercent.setPercent(discountPercentDto.getPercent());
//                discountPercent.setZipCode(discountPercentDto.getZipCodeDto() != null ? zipCodeRepository.findById(discountPercentDto.getZipCodeDto().getId()).get() : null);
        discountPercent.setOnline(discountPercentDto.isOnline());
        discountPercent.setActive(discountPercentDto.isActive());
        discountPercentRepository.save(discountPercent);
        return new ApiResponse("Saved Discount percent", true);
    }

    public List<ZipCode> getZipCodesForDiscountPercent(DiscountPercentDto dto) {
        List<ZipCode> selectedZipcodes = new ArrayList<>();
        if (dto.isAll()) {
            selectedZipcodes = zipCodeRepository.findAll();
        } else if (!dto.getStatesId().isEmpty()) {
            selectedZipcodes = zipCodeService.getZipCodeByDtoList(dto.getStatesId(), dto.getCountiesId(), dto.getZipCodesId());
        } else if (!dto.getCountiesId().isEmpty()) {
            selectedZipcodes = zipCodeService.getZipCodeByDtoList(dto.getStatesId(), dto.getCountiesId(), dto.getZipCodesId());
        } else if (!dto.getZipCodesId().isEmpty()) {
            selectedZipcodes = zipCodeService.getZipCodeByDtoList(dto.getStatesId(), dto.getCountiesId(), dto.getZipCodesId());
        }
        return selectedZipcodes;
    }

    public ApiResponse addAndEditDiscountPercent(DiscountPercentDto dto) {
        try {
            if (dto.isOnline()) {
                if (discountPercentRepository.existsByOnlineIsTrueAndZipCodeIsNull()) {
                    return new ApiResponse("This discount exist", false);
                }
                DiscountPercent discountPercent = new DiscountPercent();
                discountPercent.setPercent(dto.getPercent());
//                discountPercent.setZipCode(dto.getZipCodeDto() != null ? zipCodeRepository.findById(dto.getZipCodeDto().getId()).get() : null);
                discountPercent.setOnline(dto.isOnline());
                discountPercent.setActive(dto.isActive());
                discountPercentRepository.save(discountPercent);
                return new ApiResponse("Saved Discount percent", true);
            } else {
                discountPercentRepository.saveAll(makeAllDiscountPercent(dto, getZipCodesForDiscountPercent(dto)));

            }

            return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);

        } catch (
                Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }

    }

    public List<DiscountPercent> makeAllDiscountPercent(DiscountPercentDto discountPercentDto, List<ZipCode> zipCodes) {
        List<DiscountPercent> discountPercents = new ArrayList<>();
        zipCodes.forEach(zipCode -> {
            DiscountPercent discountPercent = new DiscountPercent();
            discountPercent.setPercent(discountPercentDto.getPercent());
            discountPercent.setZipCode(zipCode);
            discountPercent.setOnline(discountPercentDto.isOnline());
            discountPercent.setActive(discountPercentDto.isActive());
            discountPercents.add(discountPercent);
        });
        return discountPercents;
    }


    public ApiResponse getSharingDiscountTariff(String search) {
        List<SharingDiscountTariffDto> sharingDiscountTariffDtoList = new ArrayList<>();
        SharingDiscountTariffDto sharingDiscountTariffDto = new SharingDiscountTariffDto();
        if (search.equals("all")) {
            List<Object[]> objectArr = sharingDiscountTariffRepository.getMAxAndMinPercents();
            for (Object[] objects : objectArr) {
                sharingDiscountTariffDto.setMinPercent(Double.parseDouble(objects[0].toString()));
                sharingDiscountTariffDto.setMaxPercent(Double.parseDouble(objects[1].toString()));
                sharingDiscountTariffDto.setActive(!objects[2].toString().equals("0"));
            }
            Optional<SharingDiscountTariff> optionalSharingDiscountTariff = sharingDiscountTariffRepository.findByOnline(true);
            if (optionalSharingDiscountTariff.isPresent()) {
                SharingDiscountTariff sharingDiscountTariff = optionalSharingDiscountTariff.get();
                SharingDiscountTariffDto onLineSharingDiscountTariff = new SharingDiscountTariffDto();
                onLineSharingDiscountTariff.setId(sharingDiscountTariff.getId());
                onLineSharingDiscountTariff.setActive(sharingDiscountTariff.isActive());
                onLineSharingDiscountTariff.setOnline(true);
                onLineSharingDiscountTariff.setPercent(sharingDiscountTariff.getPercent());
                sharingDiscountTariffDtoList.add(onLineSharingDiscountTariff);
            }

        } else {
            Optional<SharingDiscountTariff> optionalSharingDiscountTariff = sharingDiscountTariffRepository.findByZipCodeCode(search);
            if (optionalSharingDiscountTariff.isPresent()) {
                SharingDiscountTariff sharingDiscountTariff = optionalSharingDiscountTariff.get();
                sharingDiscountTariffDto.setId(sharingDiscountTariff.getId());
                sharingDiscountTariffDto.setActive(sharingDiscountTariff.isActive());
                sharingDiscountTariffDto.setPercent(sharingDiscountTariff.getPercent());
                sharingDiscountTariffDto.setZipCodeDto(zipCodeService.getZipCode(sharingDiscountTariff.getZipCode()));
            } else {
                return new ApiResponse("Error", false);
            }
        }
        sharingDiscountTariffDtoList.add(sharingDiscountTariffDto);
        return new ApiResponse("Ok", true, sharingDiscountTariffDtoList);
    }

    public ApiResponse changeActiveStatusSharingDiscountTariff(boolean active, boolean online) {
        try {
            if (online) {
                Optional<SharingDiscountTariff> optionalSharingDiscountTariff = sharingDiscountTariffRepository.findByOnline(true);
                if (optionalSharingDiscountTariff.isPresent()) {
                    SharingDiscountTariff sharingDiscountTariff = optionalSharingDiscountTariff.get();
                    sharingDiscountTariff.setActive(active);
                    sharingDiscountTariffRepository.save(sharingDiscountTariff);
                }
            } else {
                sharingDiscountTariffRepository.changeActiveSharingDiscountTariff(active);
            }
            return new ApiResponse("Ok", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse changePercentSharingDiscountTariff(Double sharingPercent) {
        List<SharingDiscountTariff> all = sharingDiscountTariffRepository.findAll();
        SharingDiscountTariff sharingDiscountTariff = all.get(0);
        sharingDiscountTariff.setPercent(sharingPercent);
        sharingDiscountTariffRepository.save(sharingDiscountTariff);
        return new ApiResponse("Ok", true);
    }

    public ApiResponse saveOrEditLoyaltyDiscountTariff(LoyaltyDiscountDto loyaltyDiscountDto) {
        try {
            LoyaltyDiscountTariff loyaltyDiscountTariff = new LoyaltyDiscountTariff();
            if (loyaltyDiscountDto.getId() != null) {
                loyaltyDiscountTariff = loyaltyDiscountTariffRepository.findById(loyaltyDiscountDto.getId()).orElseThrow(() -> new ResourceNotFoundException("", "", loyaltyDiscountDto.getId()));
            }
            loyaltyDiscountTariff.setMonth(loyaltyDiscountDto.getMonth());
            loyaltyDiscountTariff.setPercent(loyaltyDiscountDto.getPercent());
            loyaltyDiscountTariff.setBorder(loyaltyDiscountDto.getMinSum());
            loyaltyDiscountTariffRepository.save(loyaltyDiscountTariff);
            return new ApiResponse(loyaltyDiscountDto.getId() != null ? "Edited" : "Saved", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public List<LoyaltyDiscountDto> getAllLoyaltyDiscountTariff() {
        List<LoyaltyDiscountDto> dtoList = new ArrayList<>();
        List<LoyaltyDiscountTariff> loyaltyDiscountTariffList = loyaltyDiscountTariffRepository.findAll();
        for (LoyaltyDiscountTariff loyaltyDiscountTariff : loyaltyDiscountTariffList) {
            LoyaltyDiscountDto dto = new LoyaltyDiscountDto();
            dto.setId(loyaltyDiscountTariff.getId());
            dto.setActive(loyaltyDiscountTariff.isActive());
            dto.setMonth(loyaltyDiscountTariff.getMonth());
            dto.setPercent(loyaltyDiscountTariff.getPercent());
            dto.setMinSum(loyaltyDiscountTariff.getBorder());
            dtoList.add(dto);
        }
        return dtoList;
    }

    public ApiResponse changeLoyaltyDiscountTariffActive(Integer id) {
        try {
            LoyaltyDiscountTariff loyaltyDiscountTariff = loyaltyDiscountTariffRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("", "", id));
            loyaltyDiscountTariff.setActive(!loyaltyDiscountTariff.isActive());
            loyaltyDiscountTariffRepository.save(loyaltyDiscountTariff);
            return new ApiResponse("Ok", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse removeLoyaltiDiscountTariff(Integer id) {
        try {
            loyaltyDiscountTariffRepository.deleteById(id);
            return new ApiResponse("Ok", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ResPageable getAllSharingDiscountGiven(int page, short size) {
        List<SharingGivenDto> sharingGivenDtoList = new ArrayList<>();
        Page<SharingDiscount> all = sharingDiscountRepository.findAll(CommonUtils.getPageable(page, size));
        for (SharingDiscount sharingDiscount : all) {
            SharingGivenDto shGivenDto = new SharingGivenDto();
            shGivenDto.setClientFullName(sharingDiscount.getClient().getFirstName() + " " + sharingDiscount.getClient().getLastName());
            if (sharingDiscount.getOrder() != null) {
                shGivenDto.setInvitedClientFullName(sharingDiscount.getOrder().getClient().getFirstName() + " " + sharingDiscount.getOrder().getClient().getLastName());
            }
            if (sharingDiscount.getLeftover() != null) {
                shGivenDto.setLeftover(sharingDiscount.getLeftover());
            }
            shGivenDto.setDate(sharingDiscount.getCreatedAt());
            shGivenDto.setPercent(sharingDiscount.getPercent());
            shGivenDto.setSum(sharingDiscount.getAmount());
            sharingGivenDtoList.add(shGivenDto);
        }
        return new ResPageable(page, (int) size, all.getTotalPages(), all.getTotalElements(), sharingGivenDtoList);
    }

    public ResPageable getAllLoyaltyDiscountGiven(int page, short size) {
        List<LoyaltyGivenDto> loyaltyGivenDtoList = new ArrayList<>();
        Page<LoyaltyDiscount> all = loyaltyDiscountRepository.findAll(CommonUtils.getPageable(page, size));
        for (LoyaltyDiscount loyaltyDiscount : all) {
            LoyaltyGivenDto loyaltyGivenDto = new LoyaltyGivenDto();
            loyaltyGivenDto.setClientFullName(loyaltyDiscount.getUser().getFirstName() + " " + loyaltyDiscount.getUser().getLastName());
            loyaltyGivenDto.setPercent(loyaltyDiscount.getPercent());
            loyaltyGivenDto.setSum(loyaltyDiscount.getAmount());
            loyaltyGivenDtoList.add(loyaltyGivenDto);
        }
        return new ResPageable(page, (int) size, all.getTotalPages(), all.getTotalElements(), loyaltyGivenDtoList);
    }

    public ResPageable getAllDiscountExpense(int page, short size) {
        List<ResDiscountExpense> resDiscountExpenseList = new ArrayList<>();
        Page<DiscountExpense> all = discountExpenseRepository.findAll(CommonUtils.getPageable(page, size));
        for (DiscountExpense discountExpense : all) {
            ResDiscountExpense res = new ResDiscountExpense();
            if (discountExpense.getOrder() != null)
                res.setClientFullName(discountExpense.getOrder().getClient().getFirstName() + " " + discountExpense.getOrder().getClient().getLastName());
            else {
                Optional<SharingDiscount> sharingDiscount = sharingDiscountRepository.findById(discountExpense.getDiscountId());
                sharingDiscount.ifPresent(discount -> res.setClientFullName(discount.getClient().getFirstName() + " " + discount.getClient().getLastName()));
            }
            res.setCreatedAt(discountExpense.getCreatedAt());
            res.setSum(discountExpense.getAmount());
            resDiscountExpenseList.add(res);
        }
        return new ResPageable(page, (int) size, all.getTotalPages(), all.getTotalElements(), resDiscountExpenseList);
    }

    public List<ResCustomers> getAllCustomersBySearch(String search) {
//        List<ResCustomers> resCustomersList = new ArrayList<>();
//        List<User> clientList = new ArrayList<>();
//        if (search.equals("all")) {
//            clientList = userRepository.getUserList(RoleName.ROLE_USER.name());
//        } else {
//            clientList = userRepository.getAllCustomersBySearch(search, RoleName.ROLE_USER.name());
//        }
//        for (User user : clientList) {
//            ResCustomers resCustomers = new ResCustomers();
//            resCustomers.setId(user.getId());
//            resCustomers.setFirstName(user.getFirstName());
//            resCustomers.setLastName(user.getLastName());
//            resCustomers.setPhoneNumber(user.getPhoneNumber());
//            resCustomers.setEmail(user.getEmail());
//            if (user.getPhoto() != null) {
//                resCustomers.setPhotoId(user.getPhoto().getId());
//            }
//            resCustomersList.add(resCustomers);
//        }
//        return resCustomersList;
        return null;
    }


    public List<ResClientOrder> getAllOrdersByClientId(UUID id) {
        try {
            List<ResClientOrder> resClientOrderList = new ArrayList<>();
            User client = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("", "", id));
            List<Order> orderList = orderRepository.findAllByClient(client);
            for (Order order : orderList) {
                ResClientOrder resClientOrder = new ResClientOrder();
                resClientOrder.setId(order.getId());
                resClientOrder.setAgentFullName(order.getAgent().getFirstName() + " " + order.getAgent().getLastName());
                resClientOrder.setCreatedAt(order.getCreatedAt());
                resClientOrder.setOrderAmount(order.getAmount());
                resClientOrderList.add(resClientOrder);
            }
            return resClientOrderList;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public ApiResponse saveOrEditCustomDiscount(ReqCustomDiscount reqCustomDiscount) {
        try {
            CustomDiscount customDiscount = new CustomDiscount();
            if (reqCustomDiscount.getId() != null) {
                customDiscount = customDiscountRepository.findById(reqCustomDiscount.getId()).orElseThrow(() -> new ResourceNotFoundException("", "", reqCustomDiscount.getId()));
            }
            customDiscount.setOrder(orderRepository.findById(reqCustomDiscount.getOrderId()).orElseThrow(() -> new ResourceNotFoundException("", "", reqCustomDiscount.getOrderId())));
            customDiscount.setAmount(reqCustomDiscount.getSum());
            customDiscount.setPercent(reqCustomDiscount.getPercent());
            customDiscount.setDescription(reqCustomDiscount.getDescription());
            customDiscountRepository.save(customDiscount);
            return new ApiResponse(reqCustomDiscount.getId() != null ? "Edited" : "Saved", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }


    public ApiResponse removeCustomOrder(UUID id) {
        try {
            customDiscountRepository.deleteById(id);
            return new ApiResponse("Ok", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ResPageable getAllCustomDiscount(int page, short size) {

        List<ResCustomDiscount> resCustomDiscountList = new ArrayList<>();

        Page<CustomDiscount> customDiscountPage = customDiscountRepository.findAll(CommonUtils.getPageable(page, size));

        for (CustomDiscount customDiscount : customDiscountPage) {
            ResCustomDiscount resCustomDiscount = new ResCustomDiscount();
            resCustomDiscount.setId(customDiscount.getId());
            resCustomDiscount.setClientId(customDiscount.getOrder().getClient().getId());
            resCustomDiscount.setClientFullName(customDiscount.getOrder().getClient().getFirstName() + " " + customDiscount.getOrder().getClient().getLastName());
            resCustomDiscount.setOrderId(customDiscount.getOrder().getId());
            resCustomDiscount.setCreatedAt(customDiscount.getCreatedAt());
            if (customDiscount.getOrder().getClient().getPhoto() != null) {
                resCustomDiscount.setClientPhotoId(customDiscount.getOrder().getClient().getPhoto().getId());
            }
            resCustomDiscount.setPercent(customDiscount.getPercent());
            resCustomDiscount.setSum(customDiscount.getAmount());
            resCustomDiscount.setDescription(customDiscount.getDescription());
            List<ResClientOrder> resClientOrderList = new ArrayList<>();
            List<Order> orderList = orderRepository.findAllByClient(customDiscount.getOrder().getClient());
            for (Order order : orderList) {
                ResClientOrder resClientOrder = new ResClientOrder();
                resClientOrder.setId(order.getId());
                resClientOrder.setAgentFullName(order.getAgent().getFirstName() + " " + order.getAgent().getLastName());
                resClientOrder.setCreatedAt(order.getCreatedAt());
                resClientOrder.setOrderAmount(order.getAmount());
                resClientOrderList.add(resClientOrder);
            }
            resCustomDiscount.setOrderList(resClientOrderList);
            resCustomDiscountList.add(resCustomDiscount);
        }
        return new ResPageable(page, (int) size, customDiscountPage.getTotalPages(), customDiscountPage.getTotalElements(), resCustomDiscountList);

    }

    @Transactional
    public ApiResponse saveOrEditSharingDiscountTariff(ReqSharingDiscountTariff reqSharingDiscountTariff) {
        try {
            if (reqSharingDiscountTariff.isOnline()) {
                Optional<SharingDiscountTariff> optionalSharingDiscountTariff = sharingDiscountTariffRepository.findByOnline(true);
                if (optionalSharingDiscountTariff.isPresent()) {
                    SharingDiscountTariff sharingDiscountTariff = optionalSharingDiscountTariff.get();
                    sharingDiscountTariff.setActive(reqSharingDiscountTariff.isActive());
                    sharingDiscountTariff.setPercent(reqSharingDiscountTariff.getPercent());
                    sharingDiscountTariffRepository.save(sharingDiscountTariff);
                } else {
                    SharingDiscountTariff sharingDiscountTariff = new SharingDiscountTariff();
                    sharingDiscountTariff.setOnline(true);
                    sharingDiscountTariff.setActive(reqSharingDiscountTariff.isActive());
                    sharingDiscountTariff.setPercent(reqSharingDiscountTariff.getPercent());
                    sharingDiscountTariffRepository.save(sharingDiscountTariff);
                }
            } else {
                List<ZipCode> zipCodeList = new ArrayList<>();
                if (reqSharingDiscountTariff.isAllZipCode()) {
                    zipCodeList = zipCodeRepository.findAll();
                } else {
                    if (reqSharingDiscountTariff.getZipCodeIds() != null) {
                        zipCodeList = zipCodeRepository.findAllById(reqSharingDiscountTariff.getZipCodeIds());
                    }
                    if (reqSharingDiscountTariff.getZipCodeIds() == null && reqSharingDiscountTariff.getCountyIds() != null) {
                        zipCodeList = zipCodeRepository.getZipCodeByCountiesId(reqSharingDiscountTariff.getCountyIds());
                    }
                    if (reqSharingDiscountTariff.getCountyIds() == null && reqSharingDiscountTariff.getStateIds() != null) {
                        zipCodeList = zipCodeRepository.getZipCodeByStatesId(reqSharingDiscountTariff.getStateIds());
                    }

                }
                List<SharingDiscountTariff> sharingDiscountTariffList = sharingDiscountTariffRepository.findAllByOnline(false);
                for (ZipCode zipCode : zipCodeList) {
                    boolean isHave = false;
                    for (SharingDiscountTariff sharingDiscountTariff : sharingDiscountTariffList) {
                        if (sharingDiscountTariff.getZipCode().getCode().equals(zipCode.getCode())) {
                            sharingDiscountTariff.setActive(reqSharingDiscountTariff.isActive());
                            sharingDiscountTariff.setPercent(reqSharingDiscountTariff.getPercent());
                            sharingDiscountTariffRepository.save(sharingDiscountTariff);
                            isHave = true;
                        }
                    }
                    if (!isHave) {
                        SharingDiscountTariff sharingDiscountTariff = new SharingDiscountTariff();
                        sharingDiscountTariff.setPercent(reqSharingDiscountTariff.getPercent());
                        sharingDiscountTariff.setActive(reqSharingDiscountTariff.isActive());
                        sharingDiscountTariff.setZipCode(zipCode);
                        sharingDiscountTariffRepository.save(sharingDiscountTariff);
                    }
                }
            }
            return new ApiResponse("Ok", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse saveOrEditCustomDiscountTariff(CustomDiscountTariffDto dto) {
        try {
            CustomDiscountTariff customDiscountTariff = new CustomDiscountTariff();
            if (dto.getId() != null) {
                customDiscountTariff = customDiscountTariffRepository.findById(dto.getId()).orElseThrow(() -> new ResourceNotFoundException("", "", dto.getId()));
                if (!customDiscountTariff.getCustomer().getEmail().equals(dto.getClientEmail())) {
                    return new ApiResponse("Error", false);
                }
                if (dto.isUnlimited()) {
                    customDiscountTariff.setDiscountGivenOrderCount(null);
                } else {
                    if (customDiscountTariff.getUsingDiscountGivingOrderCount() > dto.getCount()) {
                        return new ApiResponse("Error", false);
                    }
                    customDiscountTariff.setDiscountGivenOrderCount(dto.getCount());
                }
            } else {
                if (!dto.isUnlimited()) {
                    customDiscountTariff.setDiscountGivenOrderCount(dto.getCount());
                }
            }
            customDiscountTariff.setCustomer(userRepository.findByEmail(dto.getClientEmail()));
            customDiscountTariff.setUnlimited(dto.isUnlimited());
            customDiscountTariff.setActive(dto.isActive());
            customDiscountTariff.setPercent(dto.getPercent());
            customDiscountTariffRepository.save(customDiscountTariff);
            return new ApiResponse(dto.getId() != null ? "Edited" : "Saved", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse changeCustomDiscountTariffActiveStatus(UUID id) {
        try {
            CustomDiscountTariff customDiscountTariff = customDiscountTariffRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("", "", id));
            customDiscountTariff.setActive(!customDiscountTariff.isActive());
            customDiscountTariffRepository.save(customDiscountTariff);
            return new ApiResponse(customDiscountTariff.isActive() ? "Activated" : "Blocked", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ResPageable getCustomDiscountTariffs(int page, short size) {
        List<CustomDiscountTariffDto> dtoList = new ArrayList<>();
        Page<CustomDiscountTariff> customDiscountTariffPage = customDiscountTariffRepository.findAll(CommonUtils.getPageable(page, size));
        for (CustomDiscountTariff customDiscountTariff : customDiscountTariffPage) {
            CustomDiscountTariffDto dto = new CustomDiscountTariffDto();
            dto.setId(customDiscountTariff.getId());
            dto.setActive(customDiscountTariff.isActive());
            dto.setUnlimited(customDiscountTariff.isUnlimited());
            dto.setPercent(customDiscountTariff.getPercent());
            dto.setCount(customDiscountTariff.getDiscountGivenOrderCount());
            dto.setUsingCount(customDiscountTariff.getUsingDiscountGivingOrderCount());
            dto.setCustomDiscountTariffClient(getCustomDiscountTariffClient(customDiscountTariff.getCustomer()));
            dtoList.add(dto);
        }
        return new ResPageable(page, (int) size, customDiscountTariffPage.getTotalPages(), customDiscountTariffPage.getTotalElements(), dtoList);
    }

    public ApiResponse removeCustomDiscountTariff(UUID id) {
        try {
            CustomDiscountTariff customDiscountTariff = customDiscountTariffRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("", "", id));
            if (customDiscountTariff.getUsingDiscountGivingOrderCount() > 0) {
                return new ApiResponse("Error", false);
            }
            customDiscountTariffRepository.deleteById(id);
            return new ApiResponse("Successfully deleted", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public List<CustomDiscountTariffClient> getBySearch(String search) {
        if (search.startsWith(" ")) {
            search = "+" + search.replace(" ", "");
        }
        List<User> clients = userRepository.findAllByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCaseOrPhoneNumberContainingOrEmailContainingIgnoreCaseAndEnabled(search, search, search, search, true);
        List<User> clientList = new ArrayList<>();
        for (User client : clients) {
            if (isClient(client.getRoles())) {
                clientList.add(client);
            }
        }
        return clientList.stream().map(this::getCustomDiscountTariffClient).collect(Collectors.toList());
    }

    public List<CustomDiscountTariffClient> getBySearchAgent(String search) {
        if (search.startsWith(" ")) {
            search = "+" + search.replace(" ", "");
        }
        List<User> clients = userRepository.findAllByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCaseOrPhoneNumberContainingOrEmailContainingIgnoreCaseAndEnabled(search, search, search, search, true);
        List<User> clientList = new ArrayList<>();
        for (User client : clients) {
            if (isAgent(client.getRoles())) {
                clientList.add(client);
            }
        }
        return clientList.stream().map(this::getCustomDiscountTariffClient).collect(Collectors.toList());
    }

    public boolean isClient(Set<Role> roles) {
        for (Role role : roles) {
            if (role.getRoleName().equals(RoleName.ROLE_USER)) {
                return true;
            }
        }
        return false;
    }

    public boolean isAgent(Set<Role> roles) {
        for (Role role : roles) {
            if (role.getRoleName().equals(RoleName.ROLE_AGENT)) {
                return true;
            }
        }
        return false;
    }

    public CustomDiscountTariffClient getCustomDiscountTariffClient(User user) {
        CustomDiscountTariffClient client = new CustomDiscountTariffClient();
        client.setId(user.getId());
        client.setFullName(user.getFirstName() + " " + user.getLastName());
        client.setEmail(user.getEmail());
        client.setPhoneNumber(user.getPhoneNumber());
        if (user.getPhoto() != null) {
            client.setPhotoId(user.getPhoto().getId());
        }
        return client;
    }

    // First order discount tariff CRUD
    public ApiResponse saveOrEditFirstOrderDiscountTariff(ReqFirstOrderDiscountTariff reqFirstOrderDiscountTariff) {
        try {
            if (reqFirstOrderDiscountTariff.isOnline()) {
                Optional<FirstOrderDiscountTariff> optionalFirstOrderDiscountTariff = firstOrderDiscountTariffRepository.findByOnline(true);
                FirstOrderDiscountTariff firstOrderDiscountTariff = new FirstOrderDiscountTariff();
                if (optionalFirstOrderDiscountTariff.isPresent()) {
                    firstOrderDiscountTariff = optionalFirstOrderDiscountTariff.get();
                }
                firstOrderDiscountTariff.setOnline(true);
                firstOrderDiscountTariff.setPercent(reqFirstOrderDiscountTariff.getPercent());
                firstOrderDiscountTariff.setActive(reqFirstOrderDiscountTariff.isActive());
                firstOrderDiscountTariffRepository.save(firstOrderDiscountTariff);
            } else {
                List<ZipCode> zipCodeList = new ArrayList<>();
                if (reqFirstOrderDiscountTariff.isAllZipCode()) {
                    zipCodeList = zipCodeRepository.findAll();
                } else {
                    if (reqFirstOrderDiscountTariff.getZipCodeIds() != null) {
                        zipCodeList = zipCodeRepository.findAllById(reqFirstOrderDiscountTariff.getZipCodeIds());
                    }
                    if (reqFirstOrderDiscountTariff.getZipCodeIds() == null && reqFirstOrderDiscountTariff.getCountyIds() != null) {
                        zipCodeList = zipCodeRepository.getZipCodeByCountiesId(reqFirstOrderDiscountTariff.getCountyIds());
                    }
                    if (reqFirstOrderDiscountTariff.getCountyIds() == null && reqFirstOrderDiscountTariff.getStateIds() != null) {
                        zipCodeList = zipCodeRepository.getZipCodeByStatesId(reqFirstOrderDiscountTariff.getStateIds());
                    }
                }
                List<FirstOrderDiscountTariff> firstOrderDiscountTariffList = firstOrderDiscountTariffRepository.findAllByOnline(false);
                for (ZipCode zipCode : zipCodeList) {
                    boolean isHave = false;
                    for (FirstOrderDiscountTariff firstOrderDiscountTariff : firstOrderDiscountTariffList) {
                        if (firstOrderDiscountTariff.getZipCode().getCode().equals(zipCode.getCode())) {
                            firstOrderDiscountTariff.setActive(reqFirstOrderDiscountTariff.isActive());
                            firstOrderDiscountTariff.setPercent(reqFirstOrderDiscountTariff.getPercent());
                            firstOrderDiscountTariffRepository.save(firstOrderDiscountTariff);
                            isHave = true;
                        }
                    }
                    if (!isHave) {
                        FirstOrderDiscountTariff firstOrderDiscountTariff = new FirstOrderDiscountTariff();
                        firstOrderDiscountTariff.setActive(reqFirstOrderDiscountTariff.isActive());
                        firstOrderDiscountTariff.setPercent(reqFirstOrderDiscountTariff.getPercent());
                        firstOrderDiscountTariff.setZipCode(zipCode);
                        firstOrderDiscountTariffRepository.save(firstOrderDiscountTariff);
                    }
                }
            }
            return new ApiResponse("Ok", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse getResFirstDiscountTariff(String search) {
        List<ResFirstOrderDiscountTariff> list = new ArrayList<>();
        ResFirstOrderDiscountTariff resFirstOrderDiscountTariff = new ResFirstOrderDiscountTariff();
        if (search.equals("all")) {
            List<Object[]> objectArr = firstOrderDiscountTariffRepository.getMAxAndMinPercents();
            for (Object[] objects : objectArr) {
                resFirstOrderDiscountTariff.setMinPercent(Double.parseDouble(objects[0].toString()));
                resFirstOrderDiscountTariff.setMaxPercent(Double.parseDouble(objects[1].toString()));
                resFirstOrderDiscountTariff.setActive(!objects[2].toString().equals("0"));
            }
            Optional<FirstOrderDiscountTariff> optionalFirstOrderDiscountTariff = firstOrderDiscountTariffRepository.findByOnline(true);
            if (optionalFirstOrderDiscountTariff.isPresent()) {
                FirstOrderDiscountTariff firstOrderDiscountTariff = optionalFirstOrderDiscountTariff.get();
                ResFirstOrderDiscountTariff resOnline = new ResFirstOrderDiscountTariff();
                resOnline.setId(firstOrderDiscountTariff.getId());
                resOnline.setActive(firstOrderDiscountTariff.isActive());
                resOnline.setOnline(firstOrderDiscountTariff.isOnline());
                resOnline.setPercent(firstOrderDiscountTariff.getPercent());
                list.add(resOnline);
            }

        } else {
            Optional<FirstOrderDiscountTariff> optionalFirstOrderDiscountTariff = firstOrderDiscountTariffRepository.findByZipCodeCode(search);
            if (optionalFirstOrderDiscountTariff.isPresent()) {
                FirstOrderDiscountTariff firstOrderDiscountTariff = optionalFirstOrderDiscountTariff.get();
                resFirstOrderDiscountTariff.setId(firstOrderDiscountTariff.getId());
                resFirstOrderDiscountTariff.setActive(firstOrderDiscountTariff.isActive());
                resFirstOrderDiscountTariff.setPercent(firstOrderDiscountTariff.getPercent());
                resFirstOrderDiscountTariff.setZipCodeDto(zipCodeService.getZipCode(firstOrderDiscountTariff.getZipCode()));
            } else {
                return new ApiResponse("Error", false);
            }
        }
        list.add(resFirstOrderDiscountTariff);
        return new ApiResponse("Ok", true, list);
    }

    public ApiResponse changeActiveStatusFirstOrderDiscountTariff(boolean active, boolean online) {
        try {
            if (online) {
                Optional<FirstOrderDiscountTariff> optional = firstOrderDiscountTariffRepository.findByOnline(true);
                if (optional.isPresent()) {
                    FirstOrderDiscountTariff firstOrderDiscountTariff = optional.get();
                    firstOrderDiscountTariff.setActive(active);
                    firstOrderDiscountTariffRepository.save(firstOrderDiscountTariff);
                }
            } else {
                firstOrderDiscountTariffRepository.changeActiveStatusFirstOrderDiscountTariff(active);
            }
            return new ApiResponse("Ok", true);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public boolean isFirstOrderForClient(@CurrentUser User user) {
        List<Order> clientOrders = orderRepository.findAllByClient(user);
        return clientOrders != null && clientOrders.size() == 1;
    }

}
