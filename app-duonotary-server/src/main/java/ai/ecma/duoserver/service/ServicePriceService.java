package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.*;
import ai.ecma.duoserver.payload.*;
import ai.ecma.duoserver.repository.*;
import ai.ecma.duoserver.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ServicePriceService {
    @Autowired
    ServicePriceRepository servicePriceRepository;
    @Autowired
    ServiceRepository serviceRepository;
    @Autowired
    ZipCodeRepository zipCodeRepository;
    @Autowired
    StateRepository stateRepository;
    @Autowired
    ServiceService serviceService;
    @Autowired
    CountyRepository countyRepository;
    @Autowired
    ZipCodeService zipCodeService;


    //Controllerga chaqiriladigan method, yani saqlangan servicePRice

    public ApiResponse addServicePrice(ServicePriceDto servicePriceDto) {
        try {
            return addOrEditServicePrice(servicePriceDto);
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ResPageable getServicePricePage(int size, int page) {
        Page<ServicePrice> list = servicePriceRepository.findAll(PageRequest.of(page, size, Sort.by(AppConstants.ASC_OR_DECK, AppConstants.COLUMN_NAME_FOR_SORT)));
        return new ResPageable(page, size, list.getTotalPages(), list.getTotalElements(), list.getContent().stream().map(
                this::getServicePricePage
        ).collect(Collectors.toList()));
    }

    //Controllerga chaqiriladigan method, yani uzgatirilgan servicePRice

    //Controllerga chaqiriladigan method,
//servicePRice ni get qilish yani main service va sub serviceni service ichidan get qilib servicePRicedagi
//     *shu servicega tegishli eng qiymat va eng arzon narxlarni qaytarishi
    public List<ServicePriceDto> getServicePriceDashboard() {
        return servicePriceRepository.getServicePriceForDashboard()
                .stream()
                .map(this::getServicePrice)
                .collect(Collectors.toList());
    }

    public ServicePriceDto getServicePrice(Object[] objects) {
        return new ServicePriceDto(
                objects[3].toString(),//main service online or in-persone
                objects[5].toString(),// notary service
                Double.parseDouble(objects[1].toString()),// minPrice
                Double.parseDouble(objects[2].toString()),// maxPrice
                Boolean.parseBoolean(objects[4].toString()),
                UUID.fromString(objects[0].toString()),
                Boolean.parseBoolean(objects[6].toString()),
                objects[7]!=null?objects[7].toString():""
        );
    }

    // ServicePRicedagi bitta servicega tegishli  statelarni qaytaradi

    public List<StateDto> getStateByServiceId(UUID serviceId) {
        return stateRepository.findAllByServiceId(serviceId).stream().map(this::getStateDto).collect(Collectors.toList());
    }


    public StateDto getStateDto(State state) {
        return new StateDto(
                state.getId(),
                state.getName()
        );
    }

    // ServicePRicedagi bitta servicega tegishli va bitta state bilan boglangan countylarni qaytaradi

    public List<CountyDto> getCountiesByStateAndServiceId(UUID stateId) {
        return countyRepository.findAllByServiceIdAndSateId(stateId).stream().map(this::getCountyDto).collect(Collectors.toList());
    }

    public CountyDto getCountyDto(County county) {
        return new CountyDto(
                county.getId(),
                county.getName(),
                getStateDto(county.getState())
        );
    }

    //ServicePRicedagi bitta servicega tegishli bulgan barcha zipcodelarni qaytaradi!!

    public List<ZipCodeDto> getZipCodesByServiceIdAndCountyId(UUID countyId) {
        return zipCodeRepository.findAllByServiceIdAndCountyId(countyId).stream().map(this::getZipCodeDto).collect(Collectors.toList());
    }

    public ZipCodeDto getZipCodeDto(ZipCode zipCode) {
        return new ZipCodeDto(
                zipCode.getId(),
                zipCode.getCode(),
                getCountyDto(zipCode.getCounty()),
                zipCode.getCity(),
                zipCode.isActive()
        );
    }

    /**
     * bu function ServicePriceni yaratib oladi
     *
     * @param servicePriceDto servicePriceDto malumoti
     * @param servicePrice    servicePrice molumoti
     * @param zipCode         da zipCode malumoti
     * @param service         da service malumoti
     * @return yangi servicePRice qaytariladi
     */
    public ServicePrice makeServicePrice(ServicePriceDto servicePriceDto, ServicePrice servicePrice, ZipCode zipCode, ai.ecma.duoserver.entity.Service service) {
        servicePrice.setService(service);
        if (servicePrice.getId() == null)
        servicePrice.setZipCode(zipCode);
        servicePrice.setPrice(servicePriceDto.getPrice());
        servicePrice.setActive(servicePrice.getId() == null || servicePrice.getActive());
        servicePrice.setChargeMinute(servicePriceDto.getChargeMinute());
        servicePrice.setChargePercent(servicePriceDto.getChargePercent());
        return servicePrice;
    }


    /**
     * bu function servicePRiceni quwadi yoki edit qiladi. bu yerda statelar buyicha quwish
     * yoki countylar buyicha quwish yoki edit qilish
     * yoki zipCodelarni barchani bizgarilda quwish yoki edit qilish
     *
     * @param servicePriceDto da ServicePrice malumotlari keladi
     * @return saqlangan servicePRice qaytade
     */
    public ApiResponse addOrEditServicePrice(ServicePriceDto servicePriceDto) {
        ServicePrice servicePrice1 = new ServicePrice();
        if (servicePriceDto.getId() != null) {
            servicePrice1 = servicePriceRepository.findById(servicePriceDto.getId()).get();
        }
        ServicePrice finalServicePrice = servicePrice1;
        Optional<ai.ecma.duoserver.entity.Service> optionalService = serviceRepository.findById(servicePriceDto.getServiceId() == null ? servicePriceDto.getServiceDto().getId() : servicePriceDto.getServiceId());
        Optional<ai.ecma.duoserver.entity.Service> optionalService1 = serviceRepository.findById(servicePriceDto.getServiceId());
        ai.ecma.duoserver.entity.Service service1 = optionalService.get();
        if (!service1.getMainService().isOnline()) {
            if (optionalService.isPresent()) {
//            List<ServicePrice> servicePriceList = servicePriceRepository.findAllByService_Id(servicePriceDto.getServiceId());
                List<ServicePrice> servicePrices = new ArrayList<>();
                ai.ecma.duoserver.entity.Service service = optionalService.get();
                if (servicePriceDto.isAll()) {
                    List<ZipCode> zipCodeByInServicePriceIsAbsent = zipCodeRepository.getZipCodeByServiceIdNot(servicePriceDto.getServiceId());
                    servicePrices = servicePriceRepository.findAllByService_Id(servicePriceDto.getServiceId() == null ? servicePriceDto.getServiceDto().getId() : servicePriceDto.getServiceId()).stream().map(servicePrice -> makeServicePrice(servicePriceDto, servicePrice, null, service)).collect(Collectors.toList());
                    servicePrices.addAll(zipCodeByInServicePriceIsAbsent.stream().map(zipCode -> makeServicePrice(servicePriceDto, servicePriceDto.getId() != null ? finalServicePrice : new ServicePrice(), zipCode, service)).collect(Collectors.toList()));
                } else if (!servicePriceDto.getStatesId().isEmpty()) {
                    List<ZipCode> zipCodeByDtoList = zipCodeService.getZipCodeByDtoListForServicePrice(servicePriceDto.getStatesId(), servicePriceDto.getCountiesId(), servicePriceDto.getZipCodesId(), servicePriceDto.getServiceId());
                    servicePrices = servicePriceRepository.findAllByService_IdAndZipCode_County_State_IdIn(servicePriceDto.getServiceId(), servicePriceDto.getStatesId()).stream().map(servicePrice -> makeServicePrice(servicePriceDto, servicePrice, null, service)).collect(Collectors.toList());
                    servicePrices.addAll(zipCodeByDtoList.stream().map(zipCode -> makeServicePrice(servicePriceDto, servicePriceDto.getId() != null ? finalServicePrice : new ServicePrice(), zipCode, service)).collect(Collectors.toList()));
                } else if (!servicePriceDto.getCountiesId().isEmpty()) {
                    List<ZipCode> zipCodeByDtoList = zipCodeService.getZipCodeByDtoListForServicePrice(servicePriceDto.getStatesId(), servicePriceDto.getCountiesId(), servicePriceDto.getZipCodesId(), servicePriceDto.getServiceId());
                    servicePrices = servicePriceRepository.findAllByService_IdAndZipCode_County_State_IdIn(servicePriceDto.getServiceId(), servicePriceDto.getCountiesId()).stream().map(servicePrice -> makeServicePrice(servicePriceDto, servicePrice, null, service)).collect(Collectors.toList());
                    servicePrices.addAll(zipCodeByDtoList.stream().map(zipCode -> makeServicePrice(servicePriceDto, servicePriceDto.getId() != null ? finalServicePrice : new ServicePrice(), zipCode, service)).collect(Collectors.toList()));
                } else if (!servicePriceDto.getZipCodesId().isEmpty()) {
                    List<ZipCode> zipCodeByDtoList = zipCodeService.getZipCodeByDtoListForServicePrice(servicePriceDto.getStatesId(), servicePriceDto.getCountiesId(), servicePriceDto.getZipCodesId(), servicePriceDto.getServiceId());
                    servicePrices = servicePriceRepository.findAllByServiceIdAndZipCodeIdIn(servicePriceDto.getServiceId(), servicePriceDto.getZipCodesId()).stream().map(servicePrice -> makeServicePrice(servicePriceDto, servicePrice, null, service)).collect(Collectors.toList());
                    servicePrices.addAll(zipCodeByDtoList.stream().map(zipCode -> makeServicePrice(servicePriceDto, servicePriceDto.getId() != null ? finalServicePrice : new ServicePrice(), zipCode, service)).collect(Collectors.toList()));
                } else {
                    return new ApiResponse("Selected nothing", false);
                }
                servicePriceRepository.saveAll(servicePrices);
                return new ApiResponse("Successfully save  service price", true);
            }
            return new ApiResponse("This service price not found ", false);
        }
        Optional<ServicePrice> servicePrice2 = servicePriceRepository.findByServiceId(servicePriceDto.getServiceId());
        if (servicePrice2.isPresent()) {
            ServicePrice servicePrice3 = servicePrice2.get();
            ServicePrice servicePrice = makeServicePrice(servicePriceDto, servicePriceDto.getServiceId() != null ? servicePrice3 : new ServicePrice(), null, service1);
            servicePriceRepository.save(servicePrice);
            return new ApiResponse("Successfully save  service price", true);
        }
        ServicePrice servicePrice = makeServicePrice(servicePriceDto, servicePriceDto.getId() != null ? finalServicePrice : new ServicePrice(), null, service1);
        servicePriceRepository.save(servicePrice);
        return new ApiResponse("Successfully save  service price", true);
    }

    public ServicePriceDto getServicePriceByZipCodeAndMainServiceId(UUID mainServiceId, String zipCode) {
        Optional<ServicePrice> optionalServicePrice = servicePriceRepository.findFirstByActiveTrueAndZipCode_ActiveTrueAndZipCode_CodeAndService_MainServiceId(zipCode, mainServiceId);
        return optionalServicePrice.map(this::getServicePriceForOrder).orElse(new ServicePriceDto());
    }

    public ServicePriceDto getServicePriceForOrder(ServicePrice servicePrice) {
        return new ServicePriceDto(
                servicePrice.getId(),
                servicePrice.getService().getSubService().getName(),
                servicePrice.getPrice(),
                servicePrice.getService().getSubService().getDescription()
        );
    }

    public ServicePriceDto getServicePricePage(ServicePrice servicePrice) {
        ServicePriceDto servicePriceDto = new ServicePriceDto(
                servicePrice.getId(),
                servicePrice.getPrice(),
                servicePrice.getActive(),
                servicePrice.getChargeMinute(),
                servicePrice.getChargePercent(),
                serviceService.getService(servicePrice.getService()),
                zipCodeService.getZipCode(servicePrice.getZipCode()==null?null:servicePrice.getZipCode())
        );
        servicePriceDto.setOnline(servicePrice.getService().getMainService().isOnline());
        return servicePriceDto;
    }


}