package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.*;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.*;
import ai.ecma.duoserver.repository.*;
import ai.ecma.duoserver.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
public class AdditionalServicePriceService {
    @Autowired
    AdditionalServicePriceRepository additionalServicePriceRepository;

    @Autowired
    ServicePriceRepository servicePriceRepository;

    @Autowired
    AdditionalServiceRepository additionalServiceRepository;

    @Autowired
    CheckUser checkUser;

    @Autowired
    ZipCodeRepository zipCodeRepository;
    @Autowired
    ServiceRepository serviceRepository;

    @Autowired
    ServiceService service;

    @Autowired
    ZipCodeService zipCodeService;

    /**
     * List<UUID> ga qarab zipCodelarni qaytaradi
     *
     * @param dto uuid list
     * @return apiResponse object=List<ZipCode>
     */
    public ApiResponse getZipCodesForAdditionalServicePrice(AdditionalServicePriceDto dto) {
        List<ZipCode> selectedZipcodes = new ArrayList<>();
        if (dto.isAll()) {
            selectedZipcodes = zipCodeRepository.findAll();
        } else if (!dto.getStatesId().isEmpty()) {
            selectedZipcodes = zipCodeService.getZipCodeByDtoList(dto.getStatesId(), dto.getCountiesId(), dto.getZipCodesId());
        } else if (!dto.getCountiesId().isEmpty()) {
            selectedZipcodes = zipCodeService.getZipCodeByDtoList(dto.getStatesId(), dto.getCountiesId(), dto.getZipCodesId());
        } else if (!dto.getZipCodesId().isEmpty()) {
            selectedZipcodes = zipCodeService.getZipCodeByDtoList(dto.getStatesId(), dto.getCountiesId(), dto.getZipCodesId());
        } else {
            return new ApiResponse(AppConstants.USER_SELECTED_NONE, false);
        }
        return new ApiResponse(true, selectedZipcodes);
    }

    /**
     * Add qilish va edit qilish uchun method
     *
     * @param dto malunmotlar
     * @return apiRespoinse object = null
     */
    public ApiResponse addAndEditAdditionalServicePrice(AdditionalServicePriceDto dto) {

        try {
            Optional<AdditionalService> additionalService = additionalServiceRepository.findById(dto.getAdditionalServiceId());
            Optional<ai.ecma.duoserver.entity.Service> serviceOptional = serviceRepository.findById(dto.getServiceId());
            ai.ecma.duoserver.entity.Service service = serviceOptional.get();
            if (!service.getMainService().isOnline()) {
                if (!additionalService.isPresent())
                    return new ApiResponse(AppConstants.ID_NOT_FOUND, false);
                ApiResponse zipCodesApi = getZipCodesForAdditionalServicePrice(dto);
                if (!zipCodesApi.isSuccess())
                    return zipCodesApi;
                List<AdditionalServicePrice> additionalServicePrices = new ArrayList<>();
                for (ServicePrice servicePrice : servicePriceRepository.findAllByServiceAndZipCodeIn(serviceRepository.findById(dto.getServiceId()).get(), (List<ZipCode>) zipCodesApi.getObject())) {
                    if (additionalServicePriceRepository.existsByAdditionalServiceAndServicePrice(additionalService.get(), servicePrice))
                        for (AdditionalServicePrice additionalServicePrice : additionalServicePriceRepository.findAllByServicePriceAndAdditionalService(servicePrice, additionalService.get())) {
                            additionalServicePrices.add(makeAdditionalServicePrice(additionalServicePrice, servicePrice, additionalService.get(), dto.getPrice()));
                        }
                    else
                        additionalServicePrices.add(makeAdditionalServicePrice(new AdditionalServicePrice(), servicePrice, additionalService.get(), dto.getPrice()));
                }
                additionalServicePriceRepository.saveAll(additionalServicePrices);
                return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
            }
            Optional<AdditionalService> byId = additionalServiceRepository.findById(dto.getAdditionalServiceId());
            if (byId.isPresent()) {
                AdditionalService additionalService1 = byId.get();
                Optional<ServicePrice> byServiceId = servicePriceRepository.findByServiceId(dto.getServiceId());
                if (byServiceId.isPresent()) {
                    ServicePrice servicePrice1 = byServiceId.get();
                    AdditionalServicePrice byServiceAndAdditionalService = additionalServicePriceRepository.findByServiceAndAdditionalService(dto.getServiceId(), dto.getAdditionalServiceId());
                    if (byServiceAndAdditionalService == null) {
                        AdditionalServicePrice additionalServicePrice = makeAdditionalServicePrice(new AdditionalServicePrice(), servicePrice1, additionalService1, dto.getPrice());
                        additionalServicePriceRepository.save(additionalServicePrice);
                        return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
                    }
                    AdditionalServicePrice byByService = additionalServicePriceRepository.findByService(dto.getServiceId());
                    if (byByService == null) {
                        AdditionalServicePrice additionalServicePrice = makeAdditionalServicePrice(new AdditionalServicePrice(), servicePrice1, additionalService1, dto.getPrice());
                        additionalServicePriceRepository.save(additionalServicePrice);
                        return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
                    }
                    AdditionalServicePrice additionalServicePrice = makeAdditionalServicePrice(byByService, servicePrice1, additionalService1, dto.getPrice());
                    additionalServicePriceRepository.save(additionalServicePrice);
                    return new ApiResponse(AppConstants.SUCCESS_MESSAGE, true);
                }
                return new ApiResponse(AppConstants.ID_NOT_FOUND, false);
            }
            return new ApiResponse(AppConstants.ID_NOT_FOUND, false);
        } catch (Exception e) {
            return new ApiResponse(AppConstants.CATCH_MESSAGE, false);
        }
    }

    /**
     * AdditionalServicePrice yaratish uchun
     *
     * @param additionalServicePrice qowimca xizmat narxi
     * @param servicePrice           service narxi
     * @param additionalService      qanaqa qowimca xizmat
     * @param price                  obshiy narxi
     * @return additionalServicePrice
     */
    public AdditionalServicePrice makeAdditionalServicePrice(AdditionalServicePrice additionalServicePrice, ServicePrice servicePrice, AdditionalService additionalService, Double price) {
        additionalServicePrice.setServicePrice(servicePrice);
        additionalServicePrice.setAdditionalService(additionalService);
        additionalServicePrice.setPrice(price);
        additionalServicePrice.setActive(true);
        return additionalServicePrice;
    }

    /**
     * AdditionalServicePrice olish uchun
     *
     * @param additionalServicePrice
     * @return AdditionalServicePriceDto
     */
    public AdditionalServicePriceDto getAdditionalServicePrice(AdditionalServicePrice additionalServicePrice) {
        if (additionalServicePrice.isActive()) {
            return new AdditionalServicePriceDto(
                    additionalServicePrice.getId(),
                    getAdditionalService(additionalServicePrice.getAdditionalService()),
                    getServicePrice(additionalServicePrice.getServicePrice()),
                    additionalServicePrice.getPrice(),
                    additionalServicePrice.isActive()
            );
        }
        return new AdditionalServicePriceDto();
    }

    public AdditionalServiceDto getAdditionalService(AdditionalService additionalService) {
        return new AdditionalServiceDto(
                additionalService.getName(),
                additionalService.isActive(),
                additionalService.getDescription()
        );
    }

    public ServicePriceDto getServicePrice(ServicePrice servicePrice) {
        return new ServicePriceDto(
                servicePrice.getId(),
                servicePrice.getPrice(),
                servicePrice.getActive(),
                servicePrice.getChargeMinute(),
                servicePrice.getChargePercent(),
                service.getService(servicePrice.getService()),
                zipCodeService.getZipCode(servicePrice.getZipCode())
        );
    }


    public AdditionalServicePriceDto getAdditionalServicePricerDashboard(Object[] objects) {
        return new AdditionalServicePriceDto(
                UUID.fromString(objects[0].toString()),//serviceId
                Double.parseDouble(objects[3].toString()),// minPrice
                Double.parseDouble(objects[4].toString()),// maxPrice
                objects[6].toString(),//  subservice notary service
                objects[2].toString(),//additional service guvoh
                objects[5].toString(),//main service online or in-persone
                Boolean.parseBoolean(objects[7].toString())


        );
    }

    public List<AdditionalServicePriceDto> getAdditionalServicePriceDashboard() {
        return additionalServicePriceRepository.getAdditionalServicePriceForDashboard()
                .stream()
                .map(this::getAdditionalServicePricerDashboard)
                .collect(Collectors.toList());
    }

    /**
     * AdditionalServicePrice Pageni qaytarish uchun
     *
     * @param page page
     * @param size size
     * @return ResPageable
     */
    public ApiResponse getAdditionalServicePricePage(Integer page, Integer size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<AdditionalServicePrice> addServicePricesPage = additionalServicePriceRepository.findAll(pageable);
            return new ApiResponse(true,
                    new ResPageable(page,
                            size,
                            addServicePricesPage.getTotalPages(),
                            addServicePricesPage.getTotalElements(),
                            getAdditionalServicePricesDto(addServicePricesPage.getContent())));
        } catch (IllegalArgumentException e) {
            return new ApiResponse("Error", false);
        }
    }

    /**
     * AdditionalServicePrice Pageni Userga truelarini qaytarish uchun
     *
     * @param page page
     * @param size size
     * @return ResPageable
     */
    public ApiResponse getAdditionalServicePricePageForUser(Integer page, Integer size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<AdditionalServicePrice> addServicePricesPage = additionalServicePriceRepository.findAll(pageable);
            return new ApiResponse(new ResPageable(
                    page,
                    size,
                    addServicePricesPage.getTotalPages(),
                    addServicePricesPage.getTotalElements(),
                    getAdditionalServicePricesDto(addServicePricesPage.getContent())));
        } catch (IllegalArgumentException e) {
            return new ApiResponse("Error", false);
        }
    }

    /**
     * AdditionalServicePrice bazada ochirish uchun
     *
     * @param id Qaysi AdditionalServicePrice idligi
     * @return deleted
     */
    public ApiResponse deleteAddServicePrice(UUID id) {
        try {
            additionalServicePriceRepository.deleteById(id);
            return new ApiResponse("Deleted", true);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse("Error in removal", false);
        }
    }

    /**
     * activeni almashtirih uchun
     *
     * @param id     Qaysi AdditionalServicePrice idligi
     * @param active boolean
     * @return false or true
     */
    public ApiResponse setActiveAddServicePrice(UUID id, boolean active) {
        try {
            AdditionalServicePrice addServicePrice = additionalServicePriceRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getAdditionalServicePrice", "id", id));
            addServicePrice.setActive(active);
            additionalServicePriceRepository.save(addServicePrice);
            return new ApiResponse("OK", true);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse("Error in removal", false);
        }
    }

    public List<AdditionalServicePriceDto> getAdditionalServicePricesDto(List<AdditionalServicePrice> additionalServicePrices) {
        return additionalServicePrices.stream().map(this::getAdditionalServicePrice).collect(Collectors.toList());
    }

}
