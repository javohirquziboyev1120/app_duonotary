package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.*;
import ai.ecma.duoserver.exception.ResourceNotFoundException;
import ai.ecma.duoserver.payload.*;
import ai.ecma.duoserver.repository.*;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

@Service
public class TimeTableService {
    final
    TimeTableRepository timeTableRepository;

    final
    UserRepository userRepository;

    final
    TimeDurationRepository timeDurationRepository;

    final
    ServicePriceRepository servicePriceRepository;

    final
    AgentScheduleRepository agentScheduleRepository;

    final
    AgentHourOffRepository agentHourOffRepository;

    final
    HourRepository hourRepository;

    final
    MainServiceRepository mainServiceRepository;

    final
    UserService userService;

    final
    MainServiceWorkTimeRepository mainServiceWorkTimeRepository;

    public TimeTableService(TimeTableRepository timeTableRepository, UserRepository userRepository, TimeDurationRepository timeDurationRepository, ServicePriceRepository servicePriceRepository, AgentScheduleRepository agentScheduleRepository, AgentHourOffRepository agentHourOffRepository, HourRepository hourRepository, MainServiceRepository mainServiceRepository, UserService userService, MainServiceWorkTimeRepository mainServiceWorkTimeRepository) {
        this.timeTableRepository = timeTableRepository;
        this.userRepository = userRepository;
        this.timeDurationRepository = timeDurationRepository;
        this.servicePriceRepository = servicePriceRepository;
        this.agentScheduleRepository = agentScheduleRepository;
        this.agentHourOffRepository = agentHourOffRepository;
        this.hourRepository = hourRepository;
        this.mainServiceRepository = mainServiceRepository;
        this.userService = userService;
        this.mainServiceWorkTimeRepository = mainServiceWorkTimeRepository;
    }

    private List<FreeTimes> makerTimes(String item) {
        List<FreeTimes> result = new ArrayList<>();
        if (item.length() > 0)
            for (int i = 0; i < item.length(); i++) {
                int end = item.indexOf(';') + i;
                String both;
                if (end > 0) both = item.substring(i, end);
                else both = item.substring(i);
                LocalTime a = LocalTime.parse(both.substring(0, both.indexOf("/")));
                LocalTime b = LocalTime.parse(both.substring(both.indexOf("/")+1));
                result.add(new FreeTimes(a, b));
                if (end<0)
                    break;
                i = end;
            }
        return result;
    }

    public TimeTableForOrderDto getTimeTableForOrder(UUID servicePriceId, Integer countDocument, Date date, boolean onlineAgent, String zipCodeId) {
        try {
            ServicePrice servicePrice = servicePriceRepository.findByIdAndActiveTrue(servicePriceId).orElseThrow(() -> new ResourceNotFoundException("ServicePrice", "id", servicePriceId));
            MainService mainService = servicePrice.getService().getMainService();
            List<MainServiceWorkTime> workOutTimes = mainServiceWorkTimeRepository.findAllByMainServiceAndActiveIsTrue(mainService);
            List<FreeTimes> serviceTime = new ArrayList<>();
            serviceTime.add(new FreeTimes(LocalTime.parse(mainService.getFromTime()), LocalTime.parse(mainService.getTillTime()), 0d));
            for (MainServiceWorkTime workOutTime : workOutTimes) {
                serviceTime.add(new FreeTimes(LocalTime.parse(workOutTime.getFromTime()), LocalTime.parse(workOutTime.getTillTime()), workOutTime.getPercent()));
            }
            List<FreeTimes> collectTimes = new ArrayList<>();
            List<LocalTime> toFindTimes = new ArrayList<>();
            Integer needMinute = calculateGetMinute(countDocument, servicePrice);
            final TimeDuration last = timeDurationRepository.getLast();
            for (FreeTimes freeTimes : serviceTime) {
                LocalTime a = freeTimes.getTime();
                if (new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.now().toString()).equals(date)) {
                    LocalTime now = LocalTime.now();
                    if (now.isAfter(a)) {
                        a = LocalTime.of(now.getHour() + 1, 0);
                    }
                }
                LocalTime b = freeTimes.getEnd();
                if (b.isAfter(a))
                    for (int i = 0; i<b.minusHours(a.getHour()).getHour()*60/last.getDurationTime(); i++){
                    if (b.minusMinutes(needMinute).isAfter(a.plusMinutes(i * last.getDurationTime())) || b.minusMinutes(needMinute).equals(a.plusMinutes(i * last.getDurationTime()))) {
                        toFindTimes.add(a.plusMinutes(i * last.getDurationTime()));
                    } else break;
                }
            }
            Object[] allTimes = agentScheduleRepository.findAllTimes(date, onlineAgent, servicePriceId, zipCodeId);
            for (Object object : allTimes) {
                Object[] inside = (Object[]) object;
                UUID agentId = UUID.fromString(inside[0].toString());
                List<FreeTimes> agentScheduleHours = makerTimes(inside[1].toString());
                List<FreeTimes> bookedTimes = makerTimes(inside[2].toString());
                List<FreeTimes> hourOff = makerTimes(inside[3].toString());
                for (LocalTime start : toFindTimes) {
                    LocalTime end = start.plusMinutes(needMinute);
                    boolean a = false;
                    for (FreeTimes hour : agentScheduleHours) {
                        if ((hour.getTime().isBefore(start) || hour.getTime().equals(start)) && (hour.getEnd().equals(end) || hour.getEnd().isAfter(end))) {
                            a = true;
                            break;
                        }
                    }
                    if (a)
                        for (FreeTimes off : hourOff) {
                            if (start.isBefore(off.getEnd()) && end.isAfter(off.getTime())) {
                                a = false;
                                break;
                            }
                        }
                    if (a)
                        for (FreeTimes booked : bookedTimes) {
                            if (start.isBefore(booked.getEnd()) && end.isAfter(booked.getTime())) {
                                a = false;
                                break;
                            }
                        }
                    if (a)
                        for (FreeTimes service : serviceTime) {
                            if ((service.getTime().isBefore(start) || service.getTime().equals(start)) && (service.getEnd().equals(end) || service.getEnd().isAfter(end))) {
                                collectTimes.add(new FreeTimes(start, end,true, agentId,service.getPercent()));
                                break;
                            }
                        }
                }
                for (FreeTimes collectTime : collectTimes) {
                    toFindTimes.remove(collectTime.getTime());
                }
            }
            for (LocalTime toFindTime : toFindTimes) {
                for (FreeTimes service : serviceTime) {
                    if ((service.getTime().isBefore(toFindTime) || service.getTime().equals(toFindTime)) && (service.getEnd().equals(toFindTime) || service.getEnd().isAfter(toFindTime))) {
                        collectTimes.add(new FreeTimes(toFindTime, toFindTime,false, null,service.getPercent()));
                        break;
                    }
                }
            }
            List<FreeTimes> result = new ArrayList<>();
            for (FreeTimes collectTime : collectTimes) {
                boolean let = true;
                for (int i = 0; i < result.size(); i++) {
                    if (collectTime.getTime().isBefore(result.get(i).getTime())) {
                        result.add(i, collectTime);
                        let = false;
                        break;
                    }
                }
                if (let)
                    result.add(collectTime);
            }
            return new TimeTableForOrderDto(date.toString(), result);
        } catch (Exception e) {
            return new TimeTableForOrderDto();
        }
    }

    public Integer calculateGetMinute(Integer countDocument, ServicePrice servicePrice) {
        ai.ecma.duoserver.entity.Service service = servicePrice.getService();
        Integer initialCount = service.getInitialCount();//2 ta hujjat
        Integer initialSpendingTime = service.getInitialSpendingTime();//55 min
        if (countDocument <= initialCount)//5 hujjat
            return initialSpendingTime;
        if (!service.isDynamic()) {
            if (initialCount == 1) {
                return initialSpendingTime * countDocument;
            }
            int i = countDocument % initialCount;// 5/2 = 2.5 qoldiq 5
            int countDoc = (countDocument / initialCount);//5/2 = 2 butun sondi 2
            if (i > 0) {
                return (countDoc * initialSpendingTime) + initialSpendingTime;
            }
            return countDoc * initialSpendingTime;
        }
        Integer everyCount;
        Integer everySpendingTime;
        everyCount = service.getEveryCount();//2 ta
        everySpendingTime = service.getEverySpendingTime();

        int i = (countDocument - initialCount)//4 hujjat
                % everyCount;
        if (i > 0) {
            return initialSpendingTime +
                    ((everySpendingTime *
                            (countDocument - initialCount) / everyCount) +
                            everySpendingTime);
        }
        return initialSpendingTime + (everySpendingTime * ((countDocument - initialCount) / everyCount));
    }

    public ApiResponse addTimeTable(TimeTableDto timeTableDto) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Date parsedDate = dateFormat.parse(timeTableDto.getFromTime());
            Timestamp fromTime = new java.sql.Timestamp(parsedDate.getTime());
            Timestamp tillTime;
            if (new Timestamp(System.currentTimeMillis()).after(fromTime))
                return new ApiResponse("The time you entered is incorrect", false);
            if (timeTableDto.getOrderDto() != null && timeTableDto.getOrderDto().getServicePriceId() != null) {
                Optional<ServicePrice> optionalServicePrice = servicePriceRepository.findByIdAndActiveTrue(timeTableDto.getOrderDto().getServicePriceId());
                if (optionalServicePrice.isPresent()) {
                    Integer calculateGetMinute = calculateGetMinute(timeTableDto.getOrderDto().getCountDocument(), optionalServicePrice.get());
                    tillTime = new Timestamp(fromTime.getTime() + calculateGetMinute * 60 * 1000);
                } else {
                    return new ApiResponse("Service Price not found", false);
                }
            } else {
                return new ApiResponse("Service Price Id not found", false);
            }
            boolean abc = 0 < agentScheduleRepository.checkTimeFree(timeTableDto.getUserDto().getId(), fromTime, tillTime);
            if (abc || timeTableDto.getId() != null) {
                TimeTable timeTable;
                if (timeTableDto.getId() != null)
                    timeTable = timeTableRepository.findById(timeTableDto.getId()).orElse(new TimeTable());
                else timeTable = new TimeTable();
                if (!abc && !timeTable.getAgent().getId().equals(timeTableDto.getUserDto().getId()))
                    return new ApiResponse("Error", false);
                timeTable.setAgent(userRepository.findByIdAndActiveTrue(timeTableDto.getUserDto().getId()).orElseThrow(() -> new ResourceNotFoundException("getAgent", "id", timeTableDto.getUserDto().getId())));
                timeTable.setFromTime(fromTime);
                timeTable.setTillTime(tillTime);
                timeTable.setOnline(timeTableDto.getOnline());
                if (timeTableDto.getId() != null && timeTable.getOrder() != null)
                    timeTable.getOrder().setAgent(timeTable.getAgent());
                TimeTable save = timeTableRepository.save(timeTable);
                return new ApiResponse("Success", true, new ResTimeTable(save.getId(),save.getAgent().getId(), save.getFromTime(), save.getTillTime(), save.isOnline(), save.getCreatedAt()));
            }
            return new ApiResponse("Sorry this times is busy, please select another", false);
        } catch (Exception e) {
            return new ApiResponse("Sorry count not do", false);
        }
    }

    public TimeTableDto getTimeTableDto(TimeTable timeTable) {
        return new TimeTableDto(
                timeTable.getId(),
                timeTable.getFromTime().toString(),
                timeTable.getTillTime().toString(),
                userService.getUser(timeTable.getAgent()),
                null,
                timeTable.isTempBooked(),
                timeTable.getTempId(),
                timeTable.isOnline(),
                timeTable.getCreatedAt()
        );
    }
}
