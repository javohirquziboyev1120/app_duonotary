package ai.ecma.duoserver.service;

import com.twilio.Twilio;
import com.twilio.type.PhoneNumber;
import com.twilio.rest.api.v2010.account.Message;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class TwilioService {

    @Value("${twilio.account.sid}")
    private String accauntSid;

    @Value("${twilio.auth.token}")
    private String authToken;

    @Value("${twilio.trial.number}")
    private String trialNumber;

    public void sendMessage(String phoneNumber, String code) {
        Twilio.init(accauntSid, authToken);
        phoneNumber = phoneNumber.startsWith("+") ? phoneNumber.replace(" ", "") : "+" + phoneNumber.replace(" ", "");
        if (phoneNumber.length() < 13) {
            return;
        }
       Message
                .creator(new PhoneNumber(phoneNumber), // to
                        new PhoneNumber(trialNumber), // from
                        "your security  code is " + code)
                .create();
    }

}
