package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.Role;
import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.entity.enums.RoleName;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class CheckUser {

    public boolean isSuperAdmin(User user) {
        return checkRole(RoleName.ROLE_SUPER_ADMIN, user);
    }

    public boolean isAdmin(User user) {
        return checkRole(RoleName.ROLE_ADMIN, user);
    }

    public boolean isAgent(User user) {
        return checkRole(RoleName.ROLE_AGENT, user);
    }

    public boolean isUser(User user) {
        return checkRole(RoleName.ROLE_USER, user);
    }

    private boolean checkRole(RoleName roleName, User user){
        Set<Role> roles = user.getRoles();
        for (Role role : roles) {
            if (role.getRoleName().equals(roleName)){
                return true;
            }
        }
        return false;
    }
}
