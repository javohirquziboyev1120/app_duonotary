package ai.ecma.duoserver.service;

import ai.ecma.duoserver.entity.Attachment;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.repository.AttachmentRepository;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.UUID;

@Service
public class QrCodeService {
    @Autowired
    AttachmentRepository attachmentRepository;
    private static final String QR_CODE_IMAGE_PATH = "QrCodeForShareClient.png";

    public boolean makeQrCodeForShareClient(UUID id) {
        try {
            generateQRCodeImage("http://duonotary.com/login?sharingUserId=" + id.toString(), 500, 500, QR_CODE_IMAGE_PATH);
            return true;
        } catch (WriterException | IOException e) {
            return false;
        }
    }

    private static void generateQRCodeImage(String text, int width, int height, String filePath)
            throws WriterException, IOException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);

        Path path = FileSystems.getDefault().getPath(filePath);
        MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);
    }
}
