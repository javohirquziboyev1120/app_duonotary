package ai.ecma.duoserver.utils;


import ai.ecma.duoserver.exception.BadRequestException;
import ai.ecma.duoserver.payload.ApiResponse;
import ai.ecma.duoserver.payload.CertificateDto;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.xml.crypto.Data;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CommonUtils {
    public static void validatePageNumberAndSize(int page, int size) {
        if (page < 0) {
            throw new BadRequestException("Sahifa soni noldan kam bo'lishi mumkin emas.");
        }

        if (size > AppConstants.MAX_PAGE_SIZE * 10) {
            throw new BadRequestException("Sahifa soni " + AppConstants.MAX_PAGE_SIZE + " dan ko'p bo'lishi mumkin emas.");
        }
    }

    public static String changeTimeAmericanStn(Date date) {
        return new SimpleDateFormat("hh:mm a", Locale.US).format(date);
    }

    public static Integer getBetweenFromTimeAndTillTime(String fromTime, String tillTime) {
        Time difference = Time.difference(
                new Time(
                        Integer.parseInt(fromTime.substring(0, fromTime.indexOf(":"))),
                        Integer.parseInt(fromTime.substring(fromTime.indexOf(":") + 1))
                ),
                new Time(
                        Integer.parseInt(tillTime.substring(0, tillTime.indexOf(":"))),
                        Integer.parseInt(tillTime.substring(tillTime.indexOf(":") + 1))
                ));
        return (difference.hours * 60) + difference.minutes;
    }

    public static Pageable getPageable(int page, int size) {
        validatePageNumberAndSize(page, size);
        return PageRequest.of(page, size, Sort.Direction.ASC, "id");
    }

    public static Pageable getPageableForNative(int page, int size) {
        validatePageNumberAndSize(page, size);
        return PageRequest.of(page, size);
    }

    public static Pageable getPageable(int page, int size, Sort.Direction direction, String... field) {
        validatePageNumberAndSize(page, size);
        return PageRequest.of(page, size, direction, field);
    }


    public static Pageable getPageableForNative(int page, int size, Sort.Direction direction, String... field) {
        validatePageNumberAndSize(page, size);
        return PageRequest.of(page, size, direction, field);
    }


    public static String thousandSeparator(Integer a) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();

        symbols.setGroupingSeparator(' ');
        formatter.setDecimalFormatSymbols(symbols);
        return formatter.format(a.longValue());
    }

    public static double decimalFormat(double sum) {
        String pattern = "#.##";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        String format = decimalFormat.format(sum);
        return Double.parseDouble(format);
    }

    public static ApiResponse agentCertificateExpired(CertificateDto certificateDto) {
        return new ApiResponse((certificateDto.getStateDto().getId() == null ? "Id" : "Certificate ") + " expired", certificateDto.getExpireDate().before(new Date()));
    }

    public static ApiResponse agentPassportNotValidDate(CertificateDto certificateDto) {
        return new ApiResponse((certificateDto.getAttachmentId() == null ? "Id" : " ") + " expired", certificateDto.getIssueDate().after(new Date()));
    }
}
