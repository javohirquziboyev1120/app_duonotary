package ai.ecma.duoserver.utils;

public class Time {

    int minutes;
    int hours;

    public Time(int hours, int minutes) {
        this.hours = hours;
        this.minutes = minutes;
    }

//    public static void main(String[] args) {
//
//        // create objects of Time class
//        Time start = new Time(8, 12);
//        Time stop = new Time(12, 34);
//        Time diff;
//
//        // call difference method
//        diff = difference(start, stop);
//
//        System.out.printf("TIME DIFFERENCE: %d:%d:%d - ", start.hours, start.minute);
//        System.out.printf("%d:%d:%d ", stop.hours, stop.minutes);
//        System.out.printf("= %d:%d:%d\n", diff.hours, diff.minutes);
//    }

    public static Time difference(Time start, Time stop)
    {
        Time diff = new Time(0, 0);

        // if start minute is greater
        // convert stop hour into minutes
        // and add minutes to stop minutes
        if(start.minutes > stop.minutes){
            --stop.hours;
            stop.minutes += 60;
        }

        diff.minutes = stop.minutes - start.minutes;
        diff.hours = stop.hours - start.hours;

        // return the difference time
        return(diff);
    }
}