package ai.ecma.duoserver.utils;

import org.springframework.data.domain.Sort;

import java.text.SimpleDateFormat;
import java.util.Locale;

public interface AppConstants {
    String TRY_AGAIN = "Please try again later";
    String DEFAULT_PAGE_NUMBER = "0";
    String DEFAULT_PAGE_SIZE = "10";

    int MAX_PAGE_SIZE = 20;
    String DEFAULT_BEGIN_DATE_DATE = "2019-04-20";
    String DEFAULT_BEGIN_DATE = "2019-04-20 11:39:47.136000";
    String DEFAULT_END_DATE = "2100-04-20 11:39:47.136000";
    // NormalRate agar shundan past baho bolsa admonga habar boradi hech kim tegmasin
    int NORMAl_RATE = 4;
    // column boyicha sort
    String COLUMN_NAME_FOR_SORT = "createdAt";
    Sort.Direction ASC_OR_DECK = Sort.Direction.ASC;

    String DEFAULT_PASSWORD = "duonotary123";
//    String CLIENT_URl = "http://localhost:3000";
    String CLIENT_URl = "http://70.32.24.165";


    //ApiResponse
    String USER_SELECTED_NONE = "Selected nothing";
    String SUCCESS_MESSAGE = "Successfully saved";
    String CATCH_MESSAGE = "Error on Server";
    String ID_NOT_FOUND = "Id not found";
    String ALREADY_EXIST = "Already exist";
    String SUCCESS_EDITED = "Successfully edited";
    String MISSING_FIELD = "Please fill the all input";
}
