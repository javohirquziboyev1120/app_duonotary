package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Holiday;
import ai.ecma.duoserver.projection.CustomHoliday;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RepositoryRestResource(path = "holiday", collectionResourceRel = "list", excerptProjection = CustomHoliday.class)
public interface HolidayRepository extends JpaRepository<Holiday, UUID> {

    @PreAuthorize("hasAnyAuthority('DELETE_HOLIDAY')")
    @Override
    void deleteById(UUID uuid);

    @PreAuthorize("hasAnyAuthority('SAVE_HOLIDAY')")
    @Override
    <S extends Holiday> S save(S s);

    @PreAuthorize("hasAnyAuthority('SAVE_HOLIDAY')")
    @Override
    <S extends Holiday> List<S> saveAll(Iterable<S> iterable);
}
