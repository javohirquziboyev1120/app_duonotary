package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Review;
import ai.ecma.duoserver.projection.CustomReview;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.UUID;

@CrossOrigin
@JaversSpringDataAuditable
@RepositoryRestResource(path = "review", collectionResourceRel = "list", excerptProjection = CustomReview.class)
public interface ReviewRepository extends JpaRepository<Review, UUID> {

}

