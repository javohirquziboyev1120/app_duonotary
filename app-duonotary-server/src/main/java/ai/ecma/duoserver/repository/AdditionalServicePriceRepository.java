package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.AdditionalService;
import ai.ecma.duoserver.entity.AdditionalServicePrice;
import ai.ecma.duoserver.entity.Pricing;
import ai.ecma.duoserver.entity.ServicePrice;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@JaversSpringDataAuditable
public interface AdditionalServicePriceRepository extends JpaRepository<AdditionalServicePrice, UUID> {
    Page<AdditionalServicePrice> findAllByActive(boolean active, Pageable pageable);

    boolean existsByAdditionalServiceAndServicePrice(AdditionalService additionalService, ServicePrice servicePrice);

    @Query(nativeQuery = true,
            value = "select distinct cast(sp.service_id as varchar)as sid , cast(asp.additional_service_id as varchar) as ass,\n" +
                    "                (select name from additional_service where id=asp.additional_service_id) additionalServiceName ,\n" +
                    "                                        (select coalesce(min(sp2.price), 0)from additional_service_price sp2 where sp2.service_price_id in (select sp5.id from service_price sp5 where sp5.service_id=sp.service_id) and sp2.additional_service_id=asp.additional_service_id) minPrice,\n" +
                    "                                        (select coalesce(max(sp3.price), 0)from additional_service_price sp3 where sp3.service_price_id in (select sp6.id from service_price sp6 where sp6.service_id=sp.service_id) and sp3.additional_service_id=asp.additional_service_id) maxPrice,\n" +
                    "                                        (select ms.name  from service s join main_service ms on s.main_service_id = ms.id where s.id=sp.service_id) mainServiceName,\n" +
                    "                                        (select ss.name  from service s2 join sub_service ss on s2.sub_service_id = ss.id where s2.id=sp.service_id) subServiceName,\n" +
                    "                                        (select s3.active  from service s3  where s3.id=sp.service_id) serviceActive\n" +
                    "                                        from additional_service_price asp\n" +
                    "                                            join service_price sp on asp.service_price_id = sp.id order by ass")
    List<Object[]> getAdditionalServicePriceForDashboard();

    List<AdditionalServicePrice> findAllByAdditionalService(AdditionalService additionalService);

    @Query(nativeQuery = true,
            value = "select * from additional_service_price " +
                    "where service_price_id in(select id from service_price " +
                    "where zip_code_id In(select id from zip_code " +
                    "where county_id in(select id from county where state_id in (:statesId))))")
    List<AdditionalServicePrice> findAllByStatesId(@Param("statesId") List<UUID> statesId);

    @Query(nativeQuery = true,
            value = "select * from additional_service_price" +
                    "where service_price_id in(select id from service_price" +
                    "where zip_code_id in (select id from zip_code" +
                    "where county_id in (:countiesId)))")
    List<AdditionalServicePrice> findAllByCountiesId(@Param("countiesId") List<UUID> countiesId);

    @Query(nativeQuery = true,
            value = "select * from additional_service_price" +
                    "where service_price_id in(select id from service_price" +
                    "where zip_code_id in (:zipCodesId) )")
    List<AdditionalServicePrice> findAllByZipCodesId(@Param("zipCodesId") List<UUID> zipCodesId);

    Optional<AdditionalServicePrice> findByIdAndActiveTrue(UUID id);


    List<AdditionalServicePrice> findAllByServicePriceId(UUID servicePrice_id);

    List<AdditionalServicePrice> findAllByServicePriceAndAdditionalService(ServicePrice servicePrice, AdditionalService additionalService);

    @Query(nativeQuery = true,
            value = "select * from additional_service_price where service_price_id in(select id from service_price where service_id=:serviceId)"
    )
    AdditionalServicePrice findByService(UUID serviceId);

    @Query(nativeQuery = true,
            value = "select * from additional_service_price " +
                    "where service_price_id in(select id from service_price where service_id=:serviceId) " +
                    "and additional_service_id=:additionalServiceId"
    )
    AdditionalServicePrice findByServiceAndAdditionalService(UUID serviceId, UUID additionalServiceId);

}
