package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.PayType;
import ai.ecma.duoserver.projection.CustomPayType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@CrossOrigin
@RepositoryRestResource(path = "payType", collectionResourceRel = "list", excerptProjection = CustomPayType.class)
public interface PayTypeRepository extends JpaRepository<PayType, UUID> {

    @PreAuthorize("hasAnyAuthority('DELETE_PAY_TYPE')")
    @Override
    void deleteById(UUID uuid);

    @PreAuthorize("hasAnyAuthority('SAVE_PAY_TYPE')")
    @Override
    <S extends PayType> S save(S s);

    @PreAuthorize("hasAnyAuthority('SAVE_PAY_TYPE')")
    @Override
    <S extends PayType> List<S> saveAll(Iterable<S> iterable);

    Optional<PayType> findByOnlineFalse();

    Optional<PayType> findByOnlineTrue();

}
