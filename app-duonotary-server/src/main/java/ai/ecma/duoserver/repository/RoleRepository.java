package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Role;
import ai.ecma.duoserver.entity.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Collection;
import java.util.Set;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Set<Role> findAllByRoleName(RoleName roleName);

    Set<Role> findAllByRoleNameIn(Collection<RoleName> roleName);

    @PreAuthorize("hasRole('ROLE_ROLE_ADMIN')")
    @Override
    void deleteById(Integer integer);
}
