package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.ZipCode;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@JaversSpringDataAuditable
public interface ZipCodeRepository extends JpaRepository<ZipCode, UUID> {

    @Query(value = "select * from zip_code where  id not in(select zip_code_id from service_price)", nativeQuery = true)
    List<ZipCode> getZipCodeByInServicePriceIsAbsent();

    @Query(value = "select * from zip_code where county_id in(select id from county where state_id in (:statesId)) ", nativeQuery = true)
    List<ZipCode> getZipCodeByStatesId(@Param("statesId") List<UUID> statesId);

    @Query(value = "select * from zip_code where id not in" +
            " (select zip_code_id from service_price where service_id=:serviceId)", nativeQuery = true)
    List<ZipCode> getZipCodeByServiceIdNot(@Param("serviceId") UUID serviceId);

    @Query(value = "select * from zip_code where county_id in(select id from county where state_id in(:statesId)) and id not in" +
            " (select zip_code_id from service_price where service_id=:serviceId)", nativeQuery = true)
    List<ZipCode> getZipCodeByStatesIdForEditServicePrice(@Param("statesId") List<UUID> statesId, @Param("serviceId") UUID serviceId);

    @Query(value = "select * from zip_code where id in (:zipCodesId) and id not in (select zip_code_id from service_price where service_id=:serviceId) ", nativeQuery = true)
    List<ZipCode> getZipCodeByZipCodesIdForEditServicePrice(@Param("zipCodesId") List<UUID> zipCodesId, @Param("serviceId") UUID serviceId);

    @Query(value = "select * from zip_code where county_id in(:countiesId) and id not in (select zip_code_id from service_price where service_id=:serviceId) ", nativeQuery = true)
    List<ZipCode> getZipCodeByCountiesIdForEditServicePrice(@Param("countiesId") List<UUID> countiesId, @Param("serviceId") UUID serviceId);


    //    @Query(value = "select * from zip_code where county_id =ANY (:countiesId)", nativeQuery = true)
    @Query(value = "select * from zip_code where county_id in (:countiesId)", nativeQuery = true)
    List<ZipCode> getZipCodeByCountiesId(@Param("countiesId") List<UUID> countiesId);

    //        @Query(value = "select * from zip_code where id= ANY(ARRAY[:zipCodesId]) ", nativeQuery = true)
    @Query(value = "select * from zip_code where id in (:zipCodesId)", nativeQuery = true)
    List<ZipCode> getZipCodeByZipCodesId(@Param("zipCodesId") List<UUID> zipCodesId);

    @Query(nativeQuery = true,
            value = "select * from zip_code where county_id=:countyId and active=true ")
//                    "and id in" +
//                    "(select zip_code_id from service_price where service_price.active=true and service_id=:serviceId)")
    List<ZipCode> findAllByServiceIdAndCountyId(@Param("countyId") UUID countyId);

    //    @Param("serviceId") UUID serviceId,
    Optional<ZipCode> findByIdAndActiveTrue(UUID id);


    List<ZipCode> findAllByCounty_IdAndActiveTrue(UUID county_id);

    @Query(nativeQuery = true,
            value = "select * from zip_code where county_id in( select id from county where state_id=:stateId ) and active=true")
    List<ZipCode> findAllByStateAndActive(@Param("stateId") UUID stateId);

    @Query(nativeQuery = true,
            value = "select * from zip_code where county_id in( select id from county where state_id=:stateId )")
    List<ZipCode> findAllByStateId(@Param("stateId") UUID stateId);


    @Query(nativeQuery = true,
            value = "select * from zip_code where county_id in( select id from county where state_id in (:stateIdes) )")
    List<ZipCode> findAllByStateIdesIn(@Param("stateIdes") Collection<UUID> stateIdes);


    @Query(nativeQuery = true,
            value = "select (select count(*) from zip_code where code=:code and active and id in\n" +
                    "(select zip_code_id from service_price where active and service_id in\n" +
                    "(select id from service where active and main_service_id=\n" +
                    "(select id from main_service where active and online=false))))>0\n")
    boolean existsByCodeAndActiveTrue(@Param("code") String code);

    @Query(nativeQuery = true,
            value = "select * from zip_code where \"like\"(code, :code) and active and id in\n" +
                    "(select zip_code_id from service_price where active and service_id in\n" +
                    "(select id from service where active and main_service_id=\n" +
                    "(select id from main_service where active and online=false)))\n")
    List<ZipCode> getByCodeAndActiveTrue(@Param("code") String code);

    //
//    @Query(nativeQuery = true,
//            value = "select z.code as code,c.name as county_name, s.name as state_name from zip_code z inner join " +
//                    "county c inner join state s on c.state_id = s.id on z.county_id = c.id " +
//                    "where z.id in(select uzc.zip_code_id from user_zip_code uzc where uzc.user_id in " +
//                    "(select u.id from users u where u.id=:userId))")
//    List<ZipCodeDto3> getAdminZipCode(UUID userId);

    @Query(nativeQuery = true,
            value = "select * from zip_code zc where zc.id in (select uzc.zip_code_id from user_zip_code uzc where uzc.user_id in (select u.id from users u where u.id=:userId)) ")
    List<ZipCode> getAdminZipCode(UUID userId);


    @Query(nativeQuery = true,
            value = "select * from zip_code where id in\n" +
                    "( select zip_code_id from service_price where service_id=:serviceId) and county_id=:countyId ")
    List<ZipCode> getZipCodesByPricing(UUID serviceId, UUID countyId);


    /**
     * yangi zipCodeni code bazada bormi yoqmi tekshisradigan method
     *
     * @param code
     * @return boolean
     */
    boolean existsByCode(String code);

    List<ZipCode> findAllByCountyStateId(UUID county_state_id);

    List<ZipCode> findAllByCounty_IdIn(Collection<UUID> county_id);

    Optional<ZipCode> findByCodeAndActiveTrue(String code);


    List<ZipCode> findAllByCountyId(UUID county_id);

    ZipCode findAllByIdIn(List<ZipCode> zipCodesId);


    List<ZipCode> findAllByIdIn(Collection<UUID> id);
}

