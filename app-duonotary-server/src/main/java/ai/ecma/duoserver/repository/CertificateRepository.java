package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Certificate;
import ai.ecma.duoserver.entity.County;
import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.entity.enums.StatusEnum;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@JaversSpringDataAuditable
public interface CertificateRepository extends JpaRepository<Certificate, UUID> {
    /**
     * User Id orqali barcha Certificatelarni olish
     * @param user_id
     * @return List<Certificate>
     */
    List<Certificate> findAllByUser_IdOrderByCreatedAtDesc(UUID user_id);

    /**
     * Statusga qarab certificatelarni olib keladi
     * @param statusEnum
     * @return
     */
    List<Certificate> findAllByStatusEnum(StatusEnum statusEnum);

    @Query(nativeQuery = true,
    value = "select * from certificate where expire_date<CURRENT_DATE")
    List<Certificate> findAllByExpiredForCertificate();

    @Query(nativeQuery = true,
            value = "select * from certificate where user_id=:userId and state_id in( select state_id from county where id in(select county_id from zip_code where id=:zipCodeId))")
    Certificate getByZipCodeAndUser(@Param("userId")UUID userId,@Param("zipCodeId")UUID zipCodeId);

    List<Certificate> findAllByUser_IdAndExpiredFalse(UUID user_id);

    boolean existsByUserAndState_IdAndExpired(User user, UUID state_id, boolean expired);

}
