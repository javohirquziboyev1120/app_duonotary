package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.County;
import ai.ecma.duoserver.entity.State;
import ai.ecma.duoserver.projection.CustomCountry;
import ai.ecma.duoserver.projection.CustomCounty;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@JaversSpringDataAuditable
@RepositoryRestResource(path = "county", collectionResourceRel = "list", excerptProjection = CustomCounty.class)
public interface CountyRepository extends JpaRepository<County, UUID> {

    @PreAuthorize("hasAnyAuthority('DELETE_COUNTY')")
    @Override
    void deleteById(UUID uuid);

    @PreAuthorize("hasAnyAuthority('SAVE_COUNTY')")
    @Override
    <S extends County> S save(S s);


    // START QUERIES FOR AUDIT TABLES
    @Query(nativeQuery = true,
            value = "select substring(type_name from 26) from jv_global_id where local_id=:id")
    String getTableNameForItem(String id);

    @Query(nativeQuery = true,
            value = "select count(*) from jv_snapshot")
    Integer getTotalElementsCount();

    @Query(nativeQuery = true,
            value = "select count(*) from jv_snapshot where  substring(managed_type from 26)=:tableName")
    Integer getTotalElementsCountByTableName(String tableName);

    @Query(nativeQuery = true, value = "select substring(managed_type from 26) as table_name from jv_snapshot group by managed_type")
    List<String> getAuditTableList();

    @Query(nativeQuery = true, value = "select * from jv_snapshot where global_id_fk = (select global_id_pk from jv_global_id where local_id=:id)")
    List<org.javers.core.metamodel.object.CdoSnapshot> getOneItemAllAudit(String id);

    @Query(nativeQuery = true, value = "select count(*) from jv_snapshot where global_id_fk = (select global_id_pk from jv_global_id where local_id=:id)")
    Integer getTotalElementCountForItem(String id);

    @Query(nativeQuery = true, value = "select count(*) from jv_commit where author=:id")
    Integer getTotalElementCountForAdminItem(String id);

    // FINISH QUERIES FOR AUDIT TABLES

    @Query(nativeQuery = true,
            value = "select * from county where active=true and state_id=:stateId ")
//                    "and id in" +
//                    "(select county_id from zip_code where active=true and id in(select zip_code_id " +
//                    "from service_price sp where sp.active=true and sp.service_id=:serviceId))")
    List<County> findAllByServiceIdAndSateId(@Param("stateId") UUID stateId);

    @Query(nativeQuery = true,
            value = "select * from county where id in\n" +
                    "(select county_id from zip_code where id in\n" +
                    "( select zip_code_id from service_price where service_id=:serviceId)) and state_id=:stateId")
    List<County> findAllByServiceIdAndSate(@Param("stateId") UUID stateId, @Param("serviceId") UUID serviceId);

    List<County> findAllByStateId(UUID state_id);
}
