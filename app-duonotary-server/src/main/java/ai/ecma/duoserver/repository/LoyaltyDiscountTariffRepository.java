package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.LoyaltyDiscountTariff;
import ai.ecma.duoserver.projection.CustomLoyaltyDiscountTariff;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@JaversSpringDataAuditable
@RepositoryRestResource(path = "loyaltyDiscountTariff", collectionResourceRel = "list", excerptProjection = CustomLoyaltyDiscountTariff.class)
public interface LoyaltyDiscountTariffRepository extends JpaRepository<LoyaltyDiscountTariff, Integer> {

    Optional<LoyaltyDiscountTariff> findByMonthNotNull();

    @Query(value = "select * from loyalty_discount_tariff order by created_at limit 1", nativeQuery = true)
    LoyaltyDiscountTariff selectFirst();


    @PreAuthorize("hasAnyAuthority('DELETE_LOYALTY_DISCOUNT_TARIFF')")
    @Override
    void deleteById(Integer id);

    @PreAuthorize("hasAnyAuthority('SAVE_LOYALTY_DISCOUNT_TARIFF')")
    @Override
    <S extends LoyaltyDiscountTariff> S save(S s);

    @PreAuthorize("hasAnyAuthority('SAVE_LOYALTY_DISCOUNT_TARIFF')")
    @Override
    <S extends LoyaltyDiscountTariff> List<S> saveAll(Iterable<S> iterable);

    //    @PreAuthorize("hasAnyAuthority('GET_LOYALTY_DISCOUNT_TARIFF')")
    @Override
    Page<LoyaltyDiscountTariff> findAll(Pageable pageable);

    List<LoyaltyDiscountTariff> findAllByActive(boolean active);
}
