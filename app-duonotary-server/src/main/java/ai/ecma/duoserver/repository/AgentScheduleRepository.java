package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.AgentSchedule;
import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.entity.enums.WeekDayEnum;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
@JaversSpringDataAuditable
public interface AgentScheduleRepository extends JpaRepository<AgentSchedule, UUID> {
    List<AgentSchedule> findAllByUserIdOrderByWeekDay_OrderNumber(UUID user_id);

    List<AgentSchedule> findAllByUserIdAndWeekDay_Day(UUID user_id, WeekDayEnum weekDay_day);

    @Transactional
    @Modifying
    @Query(value = "select agent_schedule asch set day_off=:isDayOff where user_id=:agent and id=:scheduleId", nativeQuery = true)
    public void setAgentDayOff(@Param("agent") UUID agent, @Param("scheduleId") UUID scheduleId, @Param("isDayOff") boolean isDayOff);

    Optional<AgentSchedule> findByUserAndWeekDay_Id(User user, Integer weekDayId);

    @Query(value = "select count(*) from hour where cast(from_time as time)<=cast(:startTime as time) and cast(till_time as time)>=cast(:endTime as time) and\n" +
            "       agent_schedule_id=(select id from agent_schedule where day_off=false and week_day_id=(date_part('DOW', CAST (:startTime AS date))+1) and user_id=:agentId) and\n" +
            "       0=(select count(*) from time_table where from_time<:endTime and till_time>:startTime and agent_id=:agentId) and\n" +
            "       0=(select count(*) from agent_hour_off where agent_id=:agentId and from_hour_off<:endTime and till_hour_off>:startTime)", nativeQuery = true)
    int checkTimeFree(@Param("agentId") UUID agentId, @Param("startTime") Timestamp startTime, @Param("endTime") Timestamp endTime);

    @Query(nativeQuery = true,
            value = "select * from  agent_schedule where day_off=false and week_day_id=date_part('DOW', cast(:date as date))+1 and \n" +
                    "user_id not in (select agent_id from time_table where active=true and cast(from_time as date)=:date) and \n" +
                    "user_id in (select user_id from certificate where state_id in (select state_id from county where id in(select county_id from zip_code where active and id in (select zip_code_id from service_price \n" +
                        "where id=:servicePriceId and active))) or :onlineAgent and expired=false) and user_id in (select id from users where online_agent=:onlineAgent and active)")
    List<AgentSchedule> freeAgents(Date date, UUID servicePriceId, boolean onlineAgent);

    @Query(value = "select * from agent_schedule where day_off=false and week_day_id=(date_part('DOW', cast(:startTime as date))+1) and\n" +
            "id in (select agent_schedule_id from hour where cast(from_time as time)<=cast(:startTime as time) and cast(till_time as time)>=cast(:endTime as time)) and\n" +
            "user_id in (select id from users where online_agent=:onlineAgent and active) and \n" +
            "user_id not in (select agent_id from time_table where from_time<:endTime and till_time>:startTime) and \n" +
            "user_id not in (select agent_id from agent_hour_off where from_hour_off<:endTime and till_hour_off>:startTime) and \n" +
            "user_id in (select user_id from certificate where state_id in (select state_id from county where id in(select county_id from zip_code where active and id in (select zip_code_id from service_price \n" +
            "where id=:servicePriceId and active))) or :onlineAgent and expired=false) limit 1", nativeQuery = true)
    Optional<AgentSchedule> getFreeTime(Date startTime, Date endTime, UUID servicePriceId, boolean onlineAgent);


    AgentSchedule findByUserIdAndWeekDay_Day(UUID agentId, WeekDayEnum dayEnum);

    @Query(value = "select * from agent_schedule where day_off=false and week_day_id=(date_part('DOW', CAST (:startTime AS date))+1) and " +
            "id in (select agent_schedule_id from hour where cast(from_time as time)<=cast(:startTime as time) and cast(till_time as time)>=cast(:endTime as time)) and " +
            "user_id not in (select agent_id from time_table where active=true and till_time>:startTime and from_time<:endTime and (temp_id!=:tempId or order_id!=null)) and " +
            "user_id in (select id from users where online_agent and active and online) and " +
            "user_id not in (select agent_id from agent_hour_off where from_hour_off<:endTime and till_hour_off>:startTime) limit 1", nativeQuery = true)
    Optional<AgentSchedule> getFreeOnlineAgentNow(@Param("startTime") Timestamp startTime, @Param("endTime") Timestamp endTime, UUID tempId);


    @Query(value = "select coalesce(cast(user_id as character varying), ''),\n" +
            "       coalesce((select string_agg(concat(from_time, '/', till_time), ';' order by from_time)\n" +
            "                 from hour\n" +
            "                 where agent_schedule_id = ac.id), '')                    main,\n" +
            "       coalesce(\n" +
            "               (select string_agg(concat(cast(from_time as time), '/', cast(till_time as time)), ';' order by from_time)\n" +
            "                from time_table\n" +
            "                where agent_id = user_id\n" +
            "                  and active\n" +
            "                  and cast(from_time as date) = cast(:date as date)), '') booked,\n" +
            "       coalesce((select string_agg(concat(cast(from_hour_off as time), '/', cast(till_hour_off as time)), ';'\n" +
            "                                   order by from_hour_off)\n" +
            "                 from agent_hour_off\n" +
            "                 where cast(from_hour_off as date) = cast(:date as date)\n" +
            "                   and agent_id = user_id), '')                           ffTime\n" +
            "from agent_schedule ac\n" +
            "where day_off = false\n" +
            "  and week_day_id in (select id from week_day where order_number = date_part('DOW', cast(:date as date)) + 1)\n" +
            "  and (user_id in (select user_id from user_zip_code where cast(zip_code_id as character varying)=:zipCodeId and active) or :onlineAgent)\n" +
            "  and user_id in (select id\n" +
            "                  from users\n" +
            "                  where active\n" +
            "                    and enabled\n" +
            "                    and online_agent = :onlineAgent\n" +
            "                    and id in (select user_id\n" +
            "                               from certificate\n" +
            "                               where expired = false and :onlineAgent\n" +
            "                                  or state_id in (select state_id\n" +
            "                                                  from county\n" +
            "                                                  where id in (select county_id\n" +
            "                                                               from zip_code\n" +
            "                                                               where active\n" +
            "                                                                 and id = (select zip_code_id\n" +
            "                                                                           from service_price\n" +
            "                                                                           where id = :servicePriceId and active)))))\n" +
            "order by (select count(from_time)\n" +
            "          from time_table\n" +
            "          where agent_id = user_id\n" +
            "            and active\n" +
            "            and cast(from_time as date) = cast(:date as date))",nativeQuery = true)
    Object[] findAllTimes(Date date, boolean onlineAgent, UUID servicePriceId, String zipCodeId);
}
