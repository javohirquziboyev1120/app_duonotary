package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Order;
import ai.ecma.duoserver.entity.OrderAdditionalService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface OrderAdditionalServiceRepository extends JpaRepository<OrderAdditionalService, UUID> {
    @Query(value = "select all from order_additional_service where order_id = (select id from orders where agent_Id =: clientId or client_Id =: clientId)", nativeQuery = true)
    Page<OrderAdditionalService> findAllByOrderClient_IdInOrOrderAgent_IdIn(UUID clientId, Pageable pageable);

    List<OrderAdditionalService> findAllByOrder(Order order);
}
