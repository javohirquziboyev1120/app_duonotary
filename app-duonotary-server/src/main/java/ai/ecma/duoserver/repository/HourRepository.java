package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Hour;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface HourRepository extends JpaRepository<Hour, UUID> {
    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "delete from hour where agent_schedule_id=:agentSchedule_id")
    void deleteAllByAgentSchedule_Id(UUID agentSchedule_id);

    @Query(value = "select *  from hour where agent_schedule_id=\n" +
            "(select id from agent_schedule where user_id=:agentId and week_day_id=\n" +
            "(select id from week_day where day=:dayOfWeek))order by from_time", nativeQuery = true)
    List<Hour> getHourByAgentIdAndSelectedDate(@Param("dayOfWeek") String dayOfWeek, @Param("agentId") UUID agentId);

    Set<Hour> findAllByAgentSchedule_Id(UUID agentSchedule_id);
}
