package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Blog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BlogRepository extends JpaRepository<Blog, UUID> {

    boolean existsByUrl(String url);

    @Query(value = "select * from blog where id=:id", nativeQuery = true)
    Optional<Blog> byId(@Param("id") UUID id);

    @Query(nativeQuery = true, value = "SELECT * FROM blog WHERE is_dynamic=:dynamic ORDER BY created_at DESC")
    Page<Blog> findAllByDynamic(@Param("dynamic") boolean dynamic, Pageable pageable);

    List<Blog> findAllByFeatured(boolean featured);

    List<Blog> findAllByCategoryId(Integer id);

    Blog findByUrl(String url);
}
