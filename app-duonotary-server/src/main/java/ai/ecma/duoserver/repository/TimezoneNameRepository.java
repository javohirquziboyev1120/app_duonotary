package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.TimezoneName;
import ai.ecma.duoserver.projection.CustomTimezoneName;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@JaversSpringDataAuditable
@RepositoryRestResource(path = "timezone", collectionResourceRel = "list", excerptProjection = CustomTimezoneName.class)
public interface TimezoneNameRepository extends JpaRepository<TimezoneName, Integer> {

}