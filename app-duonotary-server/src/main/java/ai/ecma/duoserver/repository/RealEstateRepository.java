package ai.ecma.duoserver.repository;


import ai.ecma.duoserver.entity.Order;
import ai.ecma.duoserver.entity.RealEstate;
import ai.ecma.duoserver.projection.CustomRealEstate;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;
@CrossOrigin
@JaversSpringDataAuditable
@RepositoryRestResource(path = "realEstate", collectionResourceRel = "list", excerptProjection = CustomRealEstate.class)
public interface RealEstateRepository extends JpaRepository<RealEstate, UUID> {
    Optional<RealEstate> findByOrder(Order order);

    @Transactional
    @Modifying
    @Query(value = "delete from real_estate where order_id=:id",nativeQuery = true)
    void deleteByOrder(@Param("id")UUID id);
}
