package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.DocumentType;
import ai.ecma.duoserver.entity.SubService;
import ai.ecma.duoserver.projection.CustomDocumentType;
import ai.ecma.duoserver.projection.CustomSubService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.UUID;

@CrossOrigin
@RepositoryRestResource(path = "documentType", collectionResourceRel = "list", excerptProjection = CustomDocumentType.class)
public interface DocumentTypeRepository extends JpaRepository<DocumentType, UUID> {


}
