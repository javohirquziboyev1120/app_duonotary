package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.PermissionRole;
import ai.ecma.duoserver.entity.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RepositoryRestResource(path = "permissionRole", collectionResourceRel = "list")
public interface PermissionRoleRepository extends JpaRepository<PermissionRole, Integer> {

    @RestResource(path = "byRole")
    List<PermissionRole> findAllByRoleId(@Param("roleId") Integer roleId);
}
