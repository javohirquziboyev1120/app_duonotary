package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.AgentHourOff;
import ai.ecma.duoserver.entity.TimeTable;
import ai.ecma.duoserver.entity.User;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
@JaversSpringDataAuditable
public interface AgentHourOffRepository extends JpaRepository<AgentHourOff, UUID> {
    Optional<AgentHourOff> findByFromHourOffEqualsAndTillHourOffEquals(Timestamp fromHourOff, Timestamp tillHourOff);

    @Query(value = "select * from agent_hour_off where agent_id = :agentId and till_hour_off >= date_trunc('day', cast(:selectedDate as date)) and till_hour_off < date_trunc('day', cast(:selectedDate as date) + interval '1day')", nativeQuery = true)
    List<AgentHourOff> getAgentHourOffByAgentIdAndSelectedDate(@Param("agentId") UUID agentId, @Param("selectedDate") Date selectedDate);

    @Query(value = "select * from agent_hour_off where till_hour_off >= date_trunc('day', cast(:selectedDate as date)) and till_hour_off < date_trunc('day', cast(:selectedDate as date) + interval '1day')", nativeQuery = true)
    List<AgentHourOff> getAgentHourOffBySelectedDate(@Param("selectedDate") Date selectedDate);
}
