package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.PayType;
import ai.ecma.duoserver.entity.SharingDiscountTariff;
import ai.ecma.duoserver.entity.ZipCode;
import ai.ecma.duoserver.projection.CustomSharingDiscountTariff;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@CrossOrigin
@JaversSpringDataAuditable
@RepositoryRestResource(path = "sharingDiscountTariff", collectionResourceRel = "list", excerptProjection = CustomSharingDiscountTariff.class)
public interface SharingDiscountTariffRepository extends JpaRepository<SharingDiscountTariff, Integer> {
    Optional<SharingDiscountTariff> findByActive(boolean active);

    Optional<SharingDiscountTariff> findByOnline(boolean online);

    List<SharingDiscountTariff> findAllByOnline(boolean online);

    Optional<SharingDiscountTariff> findByZipCodeAndActive(ZipCode zipCode, boolean active);

    Optional<SharingDiscountTariff> findByOnlineIsTrueAndActive(boolean active);


    @PreAuthorize("hasAnyAuthority('DELETE_SHARING_DISCOUNT_TARIFF')")
    @Override
    void deleteById(Integer id);

    @PreAuthorize("hasAnyAuthority('SAVE_SHARING_DISCOUNT_TARIFF')")
    @Override
    <S extends SharingDiscountTariff> S save(S s);

    Optional<SharingDiscountTariff> findByZipCodeCode(String zipCode_code);

    @Query(nativeQuery = true, value = "select coalesce(min(percent),0) as minPercent,coalesce(max(percent),0) as maxPercent,(select count(*)from sharing_discount_tariff where active=true and online=false) activeCount from sharing_discount_tariff where online=false")
    List<Object[]> getMAxAndMinPercents();

    @Query(nativeQuery = true, value = "update sharing_discount_tariff set active=:active where online=false")
    void changeActiveSharingDiscountTariff(@Param(value = "active") boolean active);
}
