package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.TermsCondition;
import ai.ecma.duoserver.entity.enums.TermsEnum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface TermsConditionRepository extends JpaRepository<TermsCondition, UUID> {

    Optional<TermsCondition> findByTermsEnum(TermsEnum termsEnum);

    boolean existsByTermsEnum(TermsEnum termsEnum);
}
