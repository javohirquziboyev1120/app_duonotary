package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.AdditionalService;
import ai.ecma.duoserver.entity.Country;
import ai.ecma.duoserver.entity.PayType;
import ai.ecma.duoserver.projection.CustomAdditionalService;
import ai.ecma.duoserver.projection.CustomCountry;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@JaversSpringDataAuditable
@RepositoryRestResource(path = "additionalService", collectionResourceRel = "list", excerptProjection = CustomAdditionalService.class)
public interface AdditionalServiceRepository extends JpaRepository<AdditionalService, UUID> {

    @PreAuthorize("hasAnyAuthority('DELETE_ADDITIONAL_SERVICE')")
    @Override
    void deleteById(UUID uuid);

    @PreAuthorize("hasAnyAuthority('SAVE_ADDITIONAL_SERVICE')")
    @Override
    <S extends AdditionalService> List<S> saveAll(Iterable<S> iterable);

    @PreAuthorize("hasAnyAuthority('SAVE_ADDITIONAL_SERVICE')")
    @Override
    <S extends AdditionalService> S save(S s);

}
