package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.AgentLocation;
import ai.ecma.duoserver.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface AgentLocationRepository extends JpaRepository<AgentLocation, UUID> {
   @Query(value="select * from agent_location where agent_id=:agent_id order by created_at desc limit 1 ",nativeQuery=true)
   Optional<AgentLocation> findByAgentIdAndCreatedAt(@Param("agent_id") UUID agent_id);

}
