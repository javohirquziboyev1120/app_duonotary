package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Service;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@CrossOrigin
@JaversSpringDataAuditable
public interface ServiceRepository extends JpaRepository<Service, UUID> {
    Page<Service> findAllByActive(boolean active, Pageable pageable);

    boolean existsByMainServiceIdAndSubServiceId(UUID mainService_id, UUID subService_id);

    List<Service> findByMainServiceIdAndSubServiceId(UUID mainService_id, UUID subService_id);
    List<Service> findAllBySubService_Id(UUID subService_id);
    Optional<Service> findBySubService_DefaultInput(boolean subService_defaultInput);


    @Query(nativeQuery = true,
    value = "select cast(s.id as text) as serviceId,\n" +
            "(select  name from main_service where id in( select main_service_id from service where id=s.id)) mainServiceName,\n" +
            "(select  name from sub_service where id in( select sub_service_id from service where id=s.id)) subServiceId\n" +
            "from service s where id in (select service_id from service_price where zip_code_id in (select id from zip_code where county_id in\n" +
            "(select id from county where state_id=:stateId)))")
    List<Service> getServiceByState(UUID stateId);
}
