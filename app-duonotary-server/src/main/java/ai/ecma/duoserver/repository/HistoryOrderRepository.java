package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.History;
import ai.ecma.duoserver.entity.HistoryOrder;
import ai.ecma.duoserver.entity.enums.OperationEnum;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.UUID;

@CrossOrigin
public interface HistoryOrderRepository extends JpaRepository<HistoryOrder, UUID> {

    Page<HistoryOrder> findAllByObjectId(UUID objectId, Pageable pageable);
    Page<HistoryOrder> findAllByObjectIdAndOperationEnumIgnoreCase(UUID objectId, OperationEnum operation, Pageable pageable);
    Page<HistoryOrder> findAllByOperationEnumIgnoreCase(OperationEnum operation, Pageable pageable);

    @Transactional
    @Modifying
    @Query(value = "update history_order as ld set object_id=cll.objectId from (select id, cast(((cast(object as json)) ->>'id') as uuid) as objectId from history_order where object_id is null)as cll (id,objectId)where cll.id=ld.id", nativeQuery = true)
    public void updateHistoryOrderObjectIsNull();

    @Query(nativeQuery = true,
    value = "select * from history_order where object_id=:objectId order by created_at desc  limit 1")
    History findForAdminNotification(@Param("objectId") UUID objectId);
}
