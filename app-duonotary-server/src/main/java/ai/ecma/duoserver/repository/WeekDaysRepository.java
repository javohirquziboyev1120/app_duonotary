package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.PayType;
import ai.ecma.duoserver.entity.WeekDay;
import ai.ecma.duoserver.projection.CustomWeekDays;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.UUID;

@RepositoryRestResource(path = "weekDay", collectionResourceRel = "list", excerptProjection = CustomWeekDays.class)
@CrossOrigin
public interface WeekDaysRepository extends JpaRepository<WeekDay, Integer> {
    @PreAuthorize("hasAnyAuthority('DELETE_WEEK_DAY')")
    @Override
    void deleteById(Integer id);

    @PreAuthorize("hasAnyAuthority('SAVE_WEEK_DAY')")
    @Override
    <S extends WeekDay> List<S> saveAll(Iterable<S> iterable);

    @PreAuthorize("hasAnyAuthority('SAVE_WEEK_DAY')")
    @Override
    <S extends WeekDay> S save(S s);

    @Query(nativeQuery = true, value = "select * from week_day where id not in (select week_day_id from agent_schedule where user_id=:userId)")
    List<WeekDay> getWeekdays(UUID userId);
}
