package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.MainService;
import ai.ecma.duoserver.projection.CustomMainService;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@CrossOrigin
@JaversSpringDataAuditable
@RepositoryRestResource(path = "mainService", collectionResourceRel = "list", excerptProjection = CustomMainService.class)
public interface MainServiceRepository extends JpaRepository<MainService, UUID> {

    @PreAuthorize("hasAnyAuthority('DELETE_MAIN_SERVICE')")
    @Override
    void deleteById(UUID uuid);

    @PreAuthorize("hasAnyAuthority('SAVE_MAIN_SERVICE')")
    @Override
    <S extends MainService> List<S> saveAll(Iterable<S> iterable);

    @PreAuthorize("hasAnyAuthority('SAVE_MAIN_SERVICE')")
    @Override
    <S extends MainService> S save(S s);

    @Query(nativeQuery = true,
            value = "select cast(cast(hours.ranges as time) as varchar) from (select (concat((extract('hour' from generate_series(\n" +
                    "            cast((CURRENT_DATE + cast((select from_time from main_service where id = :mainServiceId) as time))as timestamp),\n" +
                    "            cast((CURRENT_DATE + cast((select till_time from main_service where id = :mainServiceId) as time))as timestamp),:lastDurationTime))),\n" +
                    "    ':',\n" +
                    "           (extract('minute' from generate_series(\n" +
                    "                   cast((CURRENT_DATE + cast((select from_time from main_service where id = :mainServiceId) as time))as timestamp),\n" +
                    "                   cast((CURRENT_DATE + cast((select till_time from main_service where id = :mainServiceId) as time))as timestamp),\n" +
                    "               :lastDurationTime))))) ranges) as hours")
    List<String> getHoursByMainServiceIdAndLastDuration(@Param("mainServiceId") UUID mainServiceId, @Param("lastDurationTime") String lastDurationTime);

    List<MainService> findByActiveTrueAndOnline(Boolean online);

    Optional<MainService> findByActiveAndOnline(boolean active, boolean online);

    Optional<MainService> findByName(String name);
}
