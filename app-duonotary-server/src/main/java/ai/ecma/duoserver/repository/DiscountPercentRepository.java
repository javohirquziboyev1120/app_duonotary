package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.DiscountPercent;
import ai.ecma.duoserver.entity.ZipCode;
import ai.ecma.duoserver.projection.CustomDiscountPercent;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@JaversSpringDataAuditable
@RepositoryRestResource(path = "discountPercent", collectionResourceRel = "list", excerptProjection = CustomDiscountPercent.class)
@CrossOrigin
public interface DiscountPercentRepository extends JpaRepository<DiscountPercent, UUID> {

    Optional<DiscountPercent> findByZipCode_IdAndActiveIsTrue(UUID zipCode_id);

    Optional<DiscountPercent> findByOnlineIsTrueAndActiveIsTrue();

    @PreAuthorize("hasAnyAuthority('SAVE_DISCOUNT_PERCENT')")
    @Override
    <S extends DiscountPercent> S save(S s);

    @PreAuthorize("hasAnyAuthority('SAVE_DISCOUNT_PERCENT')")
    @Override
    <S extends DiscountPercent> List<S> saveAll(Iterable<S> iterable);

    @PreAuthorize("hasAuthority('DELETE_DISCOUNT_PERCENT')")
    @Override
    void deleteById(UUID uuid);

    @PreAuthorize("hasAuthority('DELETE_DISCOUNT_PERCENT')")
    @Override
    void deleteAll();

    boolean existsByOnline(boolean online);

    boolean existsByOnlineIsTrueAndZipCodeIsNullAndIdNot(UUID id);
    boolean existsByOnlineIsTrueAndZipCodeIsNull();

    boolean existsByPercentAndZipCode(Double percent, ZipCode zipCode);
}
