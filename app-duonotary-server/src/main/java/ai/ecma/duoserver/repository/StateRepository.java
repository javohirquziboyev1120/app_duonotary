package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.State;
import ai.ecma.duoserver.projection.CustomState;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@JaversSpringDataAuditable
@RepositoryRestResource(path = "state", collectionResourceRel = "list", excerptProjection = CustomState.class)
public interface StateRepository extends JpaRepository<State, UUID> {

    @Query(nativeQuery = true,
            value = "select *  from state where active=true and id in(select state_id from county where id in(select county_id" +
                    " from zip_code where id in(select zip_code_id from service_price where service_id=:serviceId)))")
    List<State> findAllByServiceId(@Param("serviceId") UUID serviceId);
    @Query(nativeQuery = true,
            value = "select * from state where id in " +
                    "(select state_id from county where id in (select county_id from zip_code where id=:zipCodeId))")
    State findByZipCode(@Param("zipCodeId") UUID zipCodeId);





    @PreAuthorize("hasAnyAuthority('DELETE_STATE')")
    @Override
    void deleteById(UUID uuid);

    @PreAuthorize("hasAnyAuthority('SAVE_STATE')")
    @Override
    <S extends State> List<S> saveAll(Iterable<S> iterable);

    @PreAuthorize("hasAnyAuthority('SAVE_STATE')")
    @Override
    <S extends State> S save(S s);

}
