package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.MainService;
import ai.ecma.duoserver.entity.PayType;
import ai.ecma.duoserver.entity.SubService;
import ai.ecma.duoserver.projection.CustomMainService;
import ai.ecma.duoserver.projection.CustomSubService;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@CrossOrigin
@JaversSpringDataAuditable
@RepositoryRestResource(path = "subService", collectionResourceRel = "list", excerptProjection = CustomSubService.class)
public interface SubServiceRepository extends JpaRepository<SubService, UUID> {


    @PreAuthorize("hasAnyAuthority('DELETE_SUB_SERVICE')")
    @Override
    void deleteById(UUID uuid);

    @PreAuthorize("hasAnyAuthority('SAVE_SUB_SERVICE')")
    @Override
    <S extends SubService> List<S> saveAll(Iterable<S> iterable);

    @PreAuthorize("hasAnyAuthority('SAVE_SUB_SERVICE')")
    @Override
    <S extends SubService> S save(S s);

    Optional<SubService> findByNameEqualsIgnoreCase(String name);

    Optional<SubService> findByDefaultInput(boolean defaultInput);

}
