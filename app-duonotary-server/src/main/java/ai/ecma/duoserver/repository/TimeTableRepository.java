package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Order;
import ai.ecma.duoserver.entity.TimeTable;
import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.entity.enums.OrderStatus;
import ai.ecma.duoserver.payload.AdminDashboardCalendarDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TimeTableRepository extends JpaRepository<TimeTable, UUID> {
    @Query(value = "select * from time_table where agent_id = :agent_id and from_time <= :fromTime and till_time >= :tillTime", nativeQuery = true)
    List<TimeTable> getTimeTableByAgentAndTimes(@Param("agent_id") UUID agent_id, @Param("fromTime") Timestamp fromTime, @Param("tillTime") Timestamp tillTime);


    @Query(value = "select * from time_table where from_time<cast(:timeDay as timestamp)+interval '1 day 1 hour' and from_time>=:timeDay and remainder is null and order_id is not null", nativeQuery = true)
    List<TimeTable> findAllToRemainderDay(Timestamp timeDay);

    @Query(value = "select * from time_table where from_time<cast(:timeDay as timestamp)+interval '1 hour' and from_time>=:timeDay and remainder is false", nativeQuery = true)
    List<TimeTable> findAllToRemainderHour(Timestamp timeDay);

    @Transactional
    @Modifying
    @Query(value = "delete from time_table where order_id is null and  abs(cast(to_char(cast(:currentTime as timestamptz)-created_at, 'MI') as integer))>(select booked_duration from time_booked limit 1)", nativeQuery = true)
    void cleanTimeTable(@Param("currentTime") Timestamp currentTime);

    @Query(value = "select * from time_table where agent_id = :agentId and till_time >= date_trunc('day', cast(:selectedDate as date)) and till_time < date_trunc('day', cast(:selectedDate as date) + interval '1day')", nativeQuery = true)
    List<TimeTable> getTimeTableByAgentIdAndSelectedDate(@Param("agentId") UUID agentId, @Param("selectedDate") Date selectedDate);

    Optional<TimeTable> findById(UUID id);

    Optional<TimeTable> findByIdAndOrderIsNull(UUID id);

    @Query(value = "select (from_time-:current_time)>=(cast(:charge_minute as timestamp)-cast(:current_time as timestamp)) from time_table where order_id=:order_id", nativeQuery = true)
    boolean isBigFromChargeMinute(UUID order_id, Timestamp charge_minute, Timestamp current_time);

    Optional<TimeTable> findByOrder(Order order);
    @Transactional
    @Modifying
    void deleteAllByOrder(Order order);

    @Query(value = "select count(*) from time_table where to_char(from_time, 'DD-MM-YYYY')=:holiday_date  or to_char(till_time, 'DD-MM-YYYY')=:holiday_date", nativeQuery = true)
    int getOrderCountForHoliday(String holiday_date);

    @Query(nativeQuery = true, value = "select cast(sb.id as varchar)                 as subServiceId,\n" +
            "       sb.name                                as subServiceName,\n" +
            "       concat(c.first_name, ' ', c.last_name) as clientFullName,\n" +
            "       c.phone_number                         as clientPhoneNumber,\n" +
            "       concat(a.first_name, ' ', a.last_name) as agentFullName,\n" +
            "       tt.from_time                           as fromTime,\n" +
            "       tt.till_time                           as tillTime\n" +
            "from time_table tt\n" +
            "         join orders o on o.id = tt.order_id\n" +
            "         join users c on o.client_id = c.id\n" +
            "         join users a on a.id = tt.agent_id\n" +
            "         join service_price sp on o.service_price_id = sp.id\n" +
            "         join service s on sp.service_id = s.id\n" +
            "         join sub_service sb on s.sub_service_id = sb.id\n" +
            "where temp_booked = false\n" +
            "  and case when :zipCodeId = '' then true else cast(o.zip_code_id as varchar) = :zipCodeId end\n" +
            "  and case\n" +
            "          when :countyId = '' then true\n" +
            "          else o.zip_code_id in (select id from zip_code where cast(county_id as varchar) = :countyId) end\n" +
            "  and case\n" +
            "          when :stateId = '' then true\n" +
            "          else o.zip_code_id in (select zc.id\n" +
            "                                 from zip_code as zc\n" +
            "                                          join county co on zc.county_id = co.id\n" +
            "                                          join state s on cast(co.state_id as varchar) = :stateId) end\n" +
            "  and tt.from_time >= date_trunc('day', cast(:selectedDate as date))\n" +
            "  and tt.from_time < date_trunc('day', cast(:selectedDate as date) + INTERVAL '7' day)")
    List<AdminDashboardCalendarDto> getOrdersByZipCodeOrCountyOrStateAndDateForDashboard(@Param("stateId") String stateId, @Param("countyId") String countyId, @Param("zipCodeId") String zipCodeId, @Param("selectedDate") Date selectedDate);

    Page<TimeTable> findAllByOrder_OrderStatusAndOrder_ClientOrOrder_AgentAndOrder_Active(OrderStatus order_orderStatus, User order_client, User order_agent, boolean order_active, Pageable pageable);
    Page<TimeTable> findAllByOrder_OrderStatusAndOrder_Active(OrderStatus order_orderStatus, boolean order_active, Pageable pageable);
}
