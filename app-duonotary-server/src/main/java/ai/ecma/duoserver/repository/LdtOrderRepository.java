package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.LdtOrder;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface LdtOrderRepository extends JpaRepository<LdtOrder, UUID> {
}
