package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.TimeDuration;
import ai.ecma.duoserver.projection.CustomTimeDuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RepositoryRestResource(path = "timeDuration", collectionResourceRel = "list", excerptProjection = CustomTimeDuration.class)
public interface TimeDurationRepository extends JpaRepository<TimeDuration, Integer> {
    @Query(value = "select * from time_duration order by id desc limit 1", nativeQuery = true)
    TimeDuration getLast();

    @Query(value = "select duration_time from time_duration where id=1", nativeQuery = true)
    String getLastDurationTime();

    @PreAuthorize("hasAnyAuthority('DELETE_TIME_DURATION')")
    @Override
    void deleteById(Integer integer);

    @PreAuthorize("hasAnyAuthority('SAVE_TIME_DURATION')")
    @Override
    <S extends TimeDuration> S save(S s);

}
