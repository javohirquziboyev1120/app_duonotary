package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Role;
import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.entity.enums.EmailStatus;
import ai.ecma.duoserver.entity.enums.OrderStatus;
import ai.ecma.duoserver.payload.ReportDto;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.*;

@JaversSpringDataAuditable
@EnableAutoConfiguration(exclude = HibernateJpaAutoConfiguration.class)
public interface UserRepository extends JpaRepository<User, UUID> {

    boolean existsByEmail(String email);

    boolean existsByEmailAndIdNot(String email, UUID id);

    boolean existsByPhoneNumber(String phoneNumber);


    boolean existsByPhoneNumberAndIdNot(String phoneNumber, UUID id);

    Optional<User> findByEmailCodeAndEmail(String emailCode, String email);

    Optional<User> findByEmailCodeAndChangingEmail(String emailCode, String email);

    Optional<User> findByEmailOrPhoneNumber(String email, String phoneNumber);

    Optional<User> findByEmailOrPhoneNumberAndPassword(String email, String phoneNumber, String password);

    Optional<User> findByIdAndActiveTrue(UUID id);

    Optional<User> findByIdAndEnabledTrue(UUID id);

    User findByEmail(String email);

    Optional<User> findByEmailAndEnabledFalse(String email);

    @Query(nativeQuery = true,
            value = "select * from users where id in (select user_id from user_role where role_id in(select id from role where role_name=:role_name))")
    List<User> getUserList(String role_name);

    @Query(nativeQuery = true,
            value = "select count(*) from users where id in (select user_id from user_role where role_id in(select id from role where role_name=:role_name))")
    Integer getUserTotalCount(String role_name);

    @Query(nativeQuery = true,
            value = "select * from users where id in (select user_id from user_role where role_id in(select id from role where role_name=:role_name)) order by created_at limit :currentSize offset :currentPage")
    List<User> getUserListWithPageable(String role_name, int currentSize, int currentPage);

    @Query(nativeQuery = true,
            value = "select * from users where active and enabled and id in\n" +
                    "(select user_id from user_zip_code where active and zip_code_id=\n" +
                    "(select zip_code_id from service_price where active and id=:servicePriceId))")
    List<User> getUserByServicePriceId(@Param("servicePriceId") UUID servicePriceId);

    /*
    BU QUERY BUYURTMA UCHUN TANLANAYOTGAN KUNDA AGENTLARNI OLIB KELISH
     */
    @Query(nativeQuery = true,
            value = "select * from users where id in\n" +
                    "(select distinct user_id\n" +
                    "from agent_schedule where\n" +
                    "(select case when (select count(*) from time_table where agent_id=user_id and\n" +
                    "from_time>=date_trunc('day',cast(:selectedDate as date)) and\n" +
                    "from_time<date_trunc('day',cast(:selectedDate as date)+interval '1'day))<1 then\n" +
                    "(select day_off from agent_schedule where user_id=user_id and week_day_id=\n" +
                    "(select id from week_day where day=:dayOfWeek))=false else true end)) and active and enabled and\n" +
                    "    (select zip_code_id from service_price where id=:servicePriceId)in\n" +
                    "(select zip_code_id from user_zip_code where user_id=users.id and active)")
    List<User> getAgentByServicePriceIdAndDayOfWeekAndSelectedDate(@Param("servicePriceId") UUID servicePriceId, @Param("dayOfWeek") String dayOfWeek, @Param("selectedDate") Date selectedDate);

    @Query(nativeQuery = true, value = "select *\n" +
            "from users\n" +
            "where enabled = true\n" +
            "  and id in (select user_id from user_role where role_id = (select id from role where role_name =:role))")
    List<User> getUsersForPdf(@Param("role") String role);

    Page<User> findAllByEnabledAndRolesIn(boolean enabled, Collection<Role> roles, Pageable pageable);

    List<User> findAllByEnabledIsTrueAndRolesInAndOnlineAgent(Set<Role> roles, Boolean onlineAgent);

    Optional<User> findByEmailCode(String emailCode);

    @Query(nativeQuery = true,
            value = "select * from users where id in(select user_id from user_zip_code where zip_code_id=:code) and id in (select user_id from user_role where role_id=(select id from role where role_name=:role))")
    List<User> findAllAgentByZipCode(@Param(value = "code") UUID code, @Param(value = "role") String role);

    @Query(nativeQuery = true, value = "select * from users where enabled and (LOWER(first_name) like :search or LOWER(last_name) like :search or phone_number like :search or LOWER(email) like concat(:search, '%')) and id in (select user_id from user_role where role_id in(select id from role where role_name=:role_name)) limit :size offset (:page*:size)")
    List<User> getAllCustomersBySearch(@Param(value = "search") String search, @Param(value = "role_name") String role_name,@Param(value = "page") int page,@Param(value = "size") int size);

@Query(nativeQuery = true, value = "select count(*) from users where enabled and (LOWER(first_name) like :search or LOWER(last_name) like :search or phone_number like :search or LOWER(email) like concat(:search, '%')) and id in (select user_id from user_role where role_id in(select id from role where role_name=:role_name))")
    Long countAllCustomersBySearch(@Param(value = "search") String search, @Param(value = "role_name") String role_name);

    @Query(nativeQuery = true, value = "select u.* from users u where u.id not in (select user_id from user_zip_code where zip_code_id = :zcId) and u.id in (select user_id from user_role where role_id in (select id from role where role_name = 'ROLE_AGENT')) and u.id in (select user_id from certificate where state_id in(select state_id from county where id in (select county_id from zip_code where id= :zcId)) and status_enum='RECEIVED')")
    List<User> getlAllAgentByCertificate(@Param(value = "zcId") UUID zcId);

    @Query(value = "select * from users where active and online_agent=:online and enabled and id in(select user_id from agent_schedule where day_off=false and week_day_id=date_part('DOW', cast(:start as date))+1 and\n" +
            "user_id not in (select agent_id from time_table where active and from_time<:end and till_time>:start) and\n" +
            "user_id not in (select agent_id from agent_hour_off where from_hour_off<:end and till_hour_off>:start) and\n" +
            "id in (select agent_schedule_id from hour where cast(from_time as time)<=cast(:start as time) and cast(till_time as time)>=cast(:end as time)) and\n" +
            "user_id in (select user_id from certificate where state_id=(select state_id from county where id in(select county_id from zip_code where active and id=:zipCodeId)) or :online and expired=false))", nativeQuery = true)
    List<User> getAllAgent(Timestamp start, Timestamp end, UUID zipCodeId, boolean online);

    @Query(value = "select cast(json_build_object('id',id, 'first_name',first_name, 'last_name', last_name, 'email', email, 'phone_number', phone_number, 'photo_id', photo_id,\n" +
            "'order',(select json_build_object('id',id, 'amount',amount, 'amount_discount',amount_discount, 'count_document',count_document, 'address',address, 'title_document',title_document,'order_number', serial_number,\n" +
            "        'time_table', (select json_build_object('id',id, 'from_time',from_time, 'till_time',till_time, 'online',t.online ) from time_table t where active and order_id=o.id),\n" +
            "        'agent',(select json_build_object('id',id, 'first_name',first_name, 'last_name', last_name, 'email', email, 'phone_number', phone_number, 'photo_id', photo_id) from users where id=agent_id)) from orders o where active and client_id=users.id order by o.created_at desc  limit 1)," +
            "'totalElements', (select count(*) from users where id in (select user_id from user_role where role_id in (select id from role where role_name='ROLE_USER')))) as text) customer " +
            "from users where id in (select user_id from user_role where role_id in (select id from role where role_name='ROLE_USER')) OFFSET (:size*:page) ROWS FETCH NEXT :size ROWS ONLY", nativeQuery = true)
    Object[] findAllCustomerWithOrder(@Param("page") int page, @Param("size") int size);

    @Query(value = "select cast(json_build_object('id',id, 'first_name',first_name, 'last_name', last_name, 'email', email, 'phone_number', phone_number, 'photo_id', photo_id,\n" +
            "'order',(select json_build_object('id',id, 'amount',amount, 'amount_discount',amount_discount, 'count_document',count_document, 'address',address, 'title_document',title_document,'order_number', serial_number,\n" +
            "        'time_table', (select json_build_object('id',id, 'from_time',from_time, 'till_time',till_time, 'online',t.online ) from time_table t where active and order_id=o.id),\n" +
            "        'agent',(select json_build_object('id',id, 'first_name',first_name, 'last_name', last_name, 'email', email, 'phone_number', phone_number, 'photo_id', photo_id) from users where id=agent_id)) from orders o where active and client_id=users.id order by o.created_at desc  limit 1)," +
            "'totalElements', (select count(*) from users where id in (select user_id from user_role where role_id in (select id from role where role_name=:role_name)))) as text) customer " +
            "from users where (LOWER(first_name) like :search or LOWER(last_name) like :search or phone_number like :search or LOWER(email) like concat(:search, '%')) and id in (select user_id from user_role where role_id in(select id from role where role_name=:role_name)) OFFSET (:size*:page) ROWS FETCH NEXT :size ROWS ONLY", nativeQuery = true)
    Object[] findAllCustomerWithSearch(@Param(value = "search") String search, @Param(value = "role_name") String role_name,@Param(value = "page") int page,@Param(value = "size") int size);


    List<User> findAllByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCaseOrPhoneNumberContainingOrEmailContainingIgnoreCaseAndEnabled(String firstName, String lastName, String phoneNumber, String email, boolean enabled);

    @Query(nativeQuery = true, value = "select * from users where enabled and id in" +
            " (select user_id from user_zip_code " +
            "where zip_code_id in(select id from zip_code where code=:zipCode )) " +
            "and id in (select user_id from user_role where role_id in " +
            "(select id from role where role_name ='ROLE_AGENT' )) " +
            "and id in(select agent_id from orders where id in " +
            "(select order_id from  time_table where date(from_time)=:date)) limit :size offset (:page*:size)")
    List<User> getAgentsByZipCodeAndOrderDate(@Param("zipCode") String zipCode, @Param("date") Date date,@Param("page") int page,@Param("size") int size);

    @Query(nativeQuery = true, value = "select count(*) from users where enabled and id in" +
            " (select user_id from user_zip_code " +
            "where zip_code_id in(select id from zip_code where code=:zipCode )) " +
            "and id in (select user_id from user_role where role_id in " +
            "(select id from role where role_name ='ROLE_AGENT' )) " +
            "and id in(select agent_id from orders where id in " +
            "(select order_id from  time_table where date(from_time)=:date))")
    Long countAgentsByZipCodeAndOrderDate(@Param("zipCode") String zipCode, @Param("date") Date date);

    @Query(nativeQuery = true, value = "select * from users where enabled and id in " +
            "(select user_id from user_zip_code where zip_code_id in" +
            "(select id from zip_code where code=:zipCode )) and id in" +
            " (select user_id from user_role where role_id in " +
            "(select id from role where role_name ='ROLE_AGENT' )) limit :size offset  (:page*:size) ")
    List<User> getAgentsByZipCode(@Param("zipCode") String zipCode,@Param("page") int page,@Param("size") int size);

    @Query(nativeQuery = true, value = "select count(*) from users where enabled and id in " +
            "(select user_id from user_zip_code where zip_code_id in" +
            "(select id from zip_code where code=:zipCode )) and id in" +
            " (select user_id from user_role where role_id in " +
            "(select id from role where role_name ='ROLE_AGENT' ))")
    Long countAgentsByZipCode(@Param("zipCode") String zipCode);

    @Query(nativeQuery = true, value = "select * from users where enabled and id in" +
            "(select user_id from user_role where role_id in " +
            "(select id from role where role_name ='ROLE_AGENT' )) " +
            "and id in(select agent_id from orders where id in " +
            "(select order_id from  time_table where date(from_time)=:date)) limit :size offset (:page*:size)")
    List<User> getAgentsByOrderDate(@Param("date") Date date,@Param("page") int page,@Param("size") int size);

    @Query(nativeQuery = true, value = "select count(*) from users where enabled and id in" +
            "(select user_id from user_role where role_id in " +
            "(select id from role where role_name ='ROLE_AGENT' )) " +
            "and id in(select agent_id from orders where id in " +
            "(select order_id from  time_table where date(from_time)=:date))")
    Long countAgentsByOrderDate(@Param("date") Date date);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "update users set online=not users.online where id=:userId")
    void changeOnlineAndOffline(@Param("userId") UUID userId);

    @Query(nativeQuery = true, value = "select json_build_array(u.*, (select sum((created_at::time))-(select sum(created_at::time) from agent_online_offline_time where agent_id=u.id and online limit (select count(t.*) from agent_online_offline_time t where agent_id=u.id and online=false))" +
            "from agent_online_offline_time t where agent_id=u.id and online=false)) as onlineHour from users u where id in (select user_id from agent_schedule)")
    Object[] getAgentsList(@Param("userId") UUID userId);

    List<User> findAllByEmailStatus(EmailStatus emailStatus);


    @Query(nativeQuery = true, value = "select count(*) from users where :today < created_at and enabled")
    Integer getRegisteredUsersToday(@Param("today") Timestamp today);

    List<User> findAllByEnabledTrue();

    @Query(nativeQuery = true,
    value = "select\n" +
            "    (select count(*) from orders where agent_id=:userId and order_status=:status and id in (select order_id from time_table where date(from_time) between :fromTime and :tillTime)) as order_count,\n" +
            "    (select (select cast(sum(rate) as float ) from feed_back where agent=false and  date(created_at) between :fromTime and :tillTime and order_id in(select id from orders where agent_id=:userId))/\n" +
            "     (select cast(count(*) as float) from feed_back where agent=false and  date(created_at) between :fromTime and :tillTime and order_id in(select id from orders where agent_id=:userId))) as rate,\n" +
            "    (select sum(amount)  from orders where agent_id=:userId and( order_status='COMPLETED' or order_status='CLOSED') and id in (select order_id from time_table where date(from_time) between :fromTime and :tillTime)) as amount,\n" +
            "    (select ((select sum(date_part('hour',created_at)) from agent_online_offline_time where agent_id=:userId and date(created_at) between :fromTime and :tillTime and online=false)+" +
            "(select sum(date_part('minute',created_at)) from agent_online_offline_time where agent_id=:userId and date(created_at) between :fromTime and :tillTime and online=false)/60 )-\n" +
            "            ((select sum(date_part('hour',created_at)) from agent_online_offline_time where agent_id=:userId and date(created_at) between :fromTime and :tillTime and online=true)+" +
            "(select sum(date_part('minute',created_at)) from agent_online_offline_time where agent_id=:userId and date(created_at) between :fromTime and :tillTime and online=true)/60)) as online_hours\n" +
            "from users u where id=:userId"
    )
    List<Object[]> getReportForAgent(@Param("userId")UUID userId, @Param("status")String status, @Param("fromTime")Date fromTime, @Param("tillTime") Date tillTime);
}
