package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Faq;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FaqRepository extends JpaRepository<Faq, Integer> {

    List<Faq> findAllByRole(String role);
}
