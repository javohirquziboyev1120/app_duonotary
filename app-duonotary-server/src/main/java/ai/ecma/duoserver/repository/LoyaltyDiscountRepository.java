package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.LoyaltyDiscount;
import ai.ecma.duoserver.entity.SharingDiscount;
import ai.ecma.duoserver.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface LoyaltyDiscountRepository extends JpaRepository<LoyaltyDiscount, UUID> {
    Optional<LoyaltyDiscount> findByUser(User user);

    Optional<LoyaltyDiscount> findByUser_Id(UUID user_id);

    @Query(nativeQuery = true,
            value = "select coalesce(sum(leftover),0) from loyalty_discount where leftover>0 and user_id=:clientId")
    Double getUserLoyaltyDiscountLeftover(UUID clientId);

    @Query(nativeQuery = true,
            value = "select * from loyalty_discount where leftover>0 " +
                    "and user_id=:clientId")
    List<LoyaltyDiscount> getUserLoyaltyDiscount(UUID clientId);

    @Query(nativeQuery = true,
            value = "UPDATE loyalty_discount\n" +
                    "SET leftover =:left_over\n" +
                    "WHERE id = :id;")
    Double updateLoyaltyDiscount(Double left_over, UUID id);
}
