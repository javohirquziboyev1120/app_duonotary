package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.CustomDiscount;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

@JaversSpringDataAuditable
public interface CustomDiscountRepository extends JpaRepository<CustomDiscount, UUID> {

    /**
     * Client ni barcha custom_discountlarni olish uchun Query
     *
     * @param userId
     * @return List<CustomDiscount>
     */
    @Query(nativeQuery = true,
            value = "select * from custom_discount where order_id in(select id from orders where client_id=:userId)")
    List<CustomDiscount> findAllByUserId(@Param("userId") UUID userId);
}
