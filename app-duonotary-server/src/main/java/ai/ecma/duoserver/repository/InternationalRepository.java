package ai.ecma.duoserver.repository;


import ai.ecma.duoserver.entity.International;
import ai.ecma.duoserver.entity.Order;
import ai.ecma.duoserver.projection.CustomInternational;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;
@CrossOrigin
@JaversSpringDataAuditable
@RepositoryRestResource(path = "international", collectionResourceRel = "list", excerptProjection = CustomInternational.class)
public interface InternationalRepository extends JpaRepository<International, UUID> {
    Optional<International> findByOrder(Order order);

    @Transactional
    @Modifying
    @Query(value = "delete from international where order_id=:id",nativeQuery = true)
    void deleteByOrder(@Param("id")UUID id);
}
