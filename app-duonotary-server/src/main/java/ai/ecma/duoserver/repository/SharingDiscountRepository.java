package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Order;
import ai.ecma.duoserver.entity.SharingDiscount;
import ai.ecma.duoserver.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SharingDiscountRepository extends JpaRepository<SharingDiscount, UUID> {
    List<SharingDiscount> findAllByClientAndLeftoverIsNotNull(String client);

    List<SharingDiscount> findAllByClient_IdAndLeftoverIsNotNull(UUID client_id);

    @Query(nativeQuery = true,
            value = "select coalesce(sum(leftover),0) from sharing_discount where leftover>0 and client_id=:clientId")
    Double getUserSharingDiscountLeftover(UUID clientId);

    @Query(nativeQuery = true,
            value = "UPDATE sharing_discount\n" +
                    "SET leftover =:left_over\n" +
                    "WHERE id = :id;")
    Double updateSharingDiscount(Double left_over, UUID id);


    @Query(nativeQuery = true,
            value = "select l.id, l.updated_at as date_time, l.user_id as client_id, l.amount, l.leftover, true  from loyalty_discount l\n" +
                    "                    where leftover>0 and user_id=:clientId union\n" +
                    "select sh.id, sh.created_at as date_time, sh.client_id as client_id, sh.amount, sh.leftover, false  from sharing_discount sh where leftover>0 and client_id=:clientId\n" +
                    "order by date_time desc")
    List<Object[]> getClientDiscount(UUID clientId);


    Optional<SharingDiscount> findByClientAndOrder(User client, Order order);

    Optional<SharingDiscount> findByClientAndId(User client, UUID id);

    Page<SharingDiscount> findAllByClient_Id(UUID client_id, Pageable pageable);

    @Query(nativeQuery = true, value = "select * from sharing_discount where leftover>0 and client_id=(select id from users where id=client_id and email=:in_email and id=(select user_id from user_role where role_id=(select id from role where role_name='ROLE_AGENT')))")
    List<SharingDiscount> getSharingDiscountByAgent(String in_email);


}
