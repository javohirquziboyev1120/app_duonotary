package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Attachment;
import ai.ecma.duoserver.entity.AttachmentContent;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.UUID;
public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, UUID> {
    AttachmentContent findByAttachmentId(UUID id);
    @Transactional
    @Modifying
    @Query(value = "delete from attachment_content where attachment_id=:id",nativeQuery = true)
    void deleteByAttachment(@Param("id") UUID id);
}
