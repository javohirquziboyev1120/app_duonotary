package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.FeedBack;
import ai.ecma.duoserver.entity.Order;
import ai.ecma.duoserver.entity.User;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;
@JaversSpringDataAuditable
public interface FeedBackRepository extends JpaRepository<FeedBack, UUID> {
    /**
     * IsSeen ni false bolgan Feedbacklarni olib berdi
     * @param seen
     * @return List<FeedBack>
     */
    List<FeedBack> findAllBySeen(boolean seen);

    boolean existsByAgentTrueAndOrder(Order order);

    boolean existsByAgentFalseAndOrder(Order order);

    Page<FeedBack> findAllByAgent(boolean agent, Pageable pageable);

    Page<FeedBack> findAllByAgentAndOrder_Client(boolean agent, User order_client, Pageable pageable);
    Page<FeedBack> findAllByAgentAndOrder_Agent(boolean agent, User order_agent, Pageable pageable);

    Page<FeedBack> findAllByAgentAndRateGreaterThanAndOrder_Agent(boolean agent, Integer rate, User order_agent,Pageable pageable);

    @Query(nativeQuery = true,value ="select date(created_at) from feed_back where agent=true and created_by=:userId group by date(created_at) limit :size offset (:page*:size)")
    List <Date> getAllDateByCreatedBy(@Param("userId") UUID userId,@Param("page") int page,@Param("size") int size);

    @Query(nativeQuery = true,value = "select * from feed_back where agent=true and created_by=:userId and date(created_at)=:date limit 3")
    List<FeedBack> findAllByCreatedAtLimit(@Param("userId") UUID userId,@Param("date") Date date);

    @Query(nativeQuery = true,value = "select * from feed_back where agent=true and created_by=:userId and date(created_at)=:date")
    List<FeedBack> findAllByCreatedAtAndDate(@Param("userId") UUID userId,@Param("date") Date date);

    @Query(nativeQuery = true,value = "select date(created_at) from feed_back where rate>:rate and agent=false and order_id in(select id from orders where agent_id=:userId limit :size offset (:page*:size))")
    List<Date> getClientsFeedbackByAgent(@Param("userId") UUID userId,@Param("page") int page,@Param("size") int size,@Param("rate")Integer rate);

    @Query(nativeQuery = true,value = "select * from feed_back where rate>:rate and agent=false and order_id in(select id from orders where agent_id=:userId) and date(created_at)=:date limit 3")
    List<FeedBack> getClientsFeedbackByDateLimit(@Param("userId") UUID userId,@Param("date") Date date,@Param("rate")Integer rate);

    @Query(nativeQuery = true,value = "select * from feed_back where rate>:rate and agent=false and order_id in(select id from orders where agent_id=:userId) and date(created_at)=:date ")
    List<FeedBack> getClientsFeedbackByDate(@Param("userId") UUID userId,@Param("date") Date date,@Param("rate")Integer rate);




}
