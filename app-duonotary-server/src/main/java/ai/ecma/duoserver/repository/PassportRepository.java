package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Passport;
import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.entity.enums.StatusEnum;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@JaversSpringDataAuditable
public interface PassportRepository extends JpaRepository<Passport, UUID> {
    /**
     * UserId orqali PassportLarnini Olish
     * @param user
     * @return Optional<Passport>
     */
  List<Passport> findAllByUserAndStatusEnum(User user, StatusEnum statusEnum);

    /**
     * Statusga qarab passportlarni olib keladi
     * @param statusEnum
     * @return
     */
    List<Passport> findAllByStatusEnum(StatusEnum statusEnum);

    /**
     * Agent id orqali  passportlarni olish
     * @return CertificateDto
     */
    List<Passport> findAllByUser_IdOrderByCreatedAtDesc(UUID user_id);

    boolean existsByExpiredAndUser_Id(boolean expired, UUID user_id);

    @Query(nativeQuery = true,value = "select * from passport where expire_date<CURRENT_DATE")
    List<Passport> findAllByExpiredForPassport();
}
