package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Partner;
import ai.ecma.duoserver.projection.CustomPartner;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@JaversSpringDataAuditable
@RepositoryRestResource(path = "partner", collectionResourceRel = "list", excerptProjection = CustomPartner.class)
public interface PartnerRepository extends JpaRepository<Partner, UUID> {

    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_SUPER_ADMIN')")
    @Override
    <S extends Partner> S save(S s);

    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_SUPER_ADMIN')")
    @Override
    <S extends Partner> List<S> saveAll(Iterable<S> iterable);

    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_SUPER_ADMIN')")
    @Override
    void deleteById(UUID uuid);

    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_SUPER_ADMIN')")
    @Override
    void delete(Partner partner);

    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_SUPER_ADMIN')")
    @Override
    void deleteAll(Iterable<? extends Partner> iterable);

    @RestResource(path ="/home")
    List<Partner> findAllByActiveTrue();
}
