package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.MainService;
import ai.ecma.duoserver.entity.MainServiceWorkTime;
import ai.ecma.duoserver.projection.CustomMainService;
import ai.ecma.duoserver.projection.CustomMainServiceWorkTime;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@CrossOrigin
@JaversSpringDataAuditable
@RepositoryRestResource(path = "mainServiceWorkTime", collectionResourceRel = "list", excerptProjection = CustomMainServiceWorkTime.class)
public interface MainServiceWorkTimeRepository extends JpaRepository<MainServiceWorkTime, UUID> {

    @PreAuthorize("hasAnyAuthority('DELETE_MAIN_SERVICE_WORK_TIME_PERCENT')")
    @Override
    void deleteById(UUID uuid);

    @PreAuthorize("hasAnyAuthority('SAVE_MAIN_SERVICE_WORK_TIME_PERCENT')")
    @Override
    <S extends MainServiceWorkTime> S save(S s);

    List<MainServiceWorkTime> findAllByMainServiceAndActiveIsTrue(MainService mainService);
    @Query(value = "select coalesce(sum(m.percent),0) from main_service_work_time as m where cast(m.from_time as time)<=cast(:fromTime as time) and cast(m.till_time as time)>=cast(:fromTime as time) and active and m.main_service_id=:mainServiceId", nativeQuery = true)
    double getMainServiceWorkTimePercent(Timestamp fromTime, UUID mainServiceId);
}
