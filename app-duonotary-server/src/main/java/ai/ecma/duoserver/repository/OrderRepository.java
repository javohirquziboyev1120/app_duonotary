package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Order;
import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.entity.enums.OrderStatus;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.*;

@JaversSpringDataAuditable
public interface OrderRepository extends JpaRepository<Order, UUID> {
    Page<Order> findAllByAgent_IdAndActiveIsTrue(UUID agent_id, Pageable pageable);

    Page<Order> findAllByClient_IdAndActiveIsTrue(UUID client_id, Pageable pageable);

    Page<Order> findAllByClient_IdOrAgent_IdAndActiveIsTrue(UUID client_id, UUID agent_id, Pageable pageable);

    @Query(value = "select * from orders where active and client_id = :client_id and created_at >=:month_from and created_at<= :month_to and order_status = :order_status", nativeQuery = true)
    List<Order> findAllByClientIdAndCreatedAtAndOrderStatus(UUID client_id, Timestamp month_from, Timestamp month_to, OrderStatus order_status);

    Page<Order> findAllByAgentAndActiveIsTrue(User agent, Pageable pageable);

    Page<Order> findAllByClientAndActiveIsTrue(User client, Pageable pageable);

    List<Order> findAllByAgent(User agent);

    Page<Order> findAllByClient_IdAndOrderStatusAndActive(UUID client_id, OrderStatus orderStatus, boolean active, Pageable pageable);

    List<Order> findAllByClient_IdAndOrderStatusAndActive(UUID client_id, OrderStatus orderStatus, boolean active);

    Page<Order> findAllByAgent_IdAndOrderStatusAndActive(UUID agent_id, OrderStatus orderStatus, boolean active, Pageable pageable);

    @Query(value = "select (select count(*) from orders where active and order_status=:status_1 and client_id=:client_id) as orders_1,\n" +
            "       (select count(*) from orders where active and order_status=:status_2 and client_id=:client_id) as orders_2,\n" +
            "       (select count(*) from orders where active and order_status=:status_3 and client_id=:client_id) as orders_3", nativeQuery = true)
    Object getCountOfOrderByOrderStatus3(String status_1, String status_2, String status_3, UUID client_id);

    @Query(value = "select (select count(*) from orders where active and order_status=:status_1 and agent_id=:agent_id) as orders_1,\n" +
            "       (select count(*) from orders where active and order_status=:status_2 and agent_id=:agent_id) as orders_2,\n" +
            "       (select count(*) from orders where active and order_status=:status_3 and agent_id=:agent_id) as orders_3,\n" +
            "       (select count(*) from orders where active and order_status=:status_4 and agent_id=:agent_id) as orders_4", nativeQuery = true)
    Object getCountOfOrderByOrderStatus4(String status_1, String status_2, String status_3, String status_4, UUID agent_id);

    @Query(value = "select * from orders where active and agent_id=:agent_id order by created_at desc limit 1", nativeQuery = true)
    Optional<Order> getOrderByAgent(UUID agent_id);

    Integer countAllByAgentIdAndActiveIsTrue(UUID agent_id);

    Page<Order> findAllByActiveIsTrue(Pageable pageable);

    //GET QILAYOPTGANDA ALBATDA ACTIVE=TRUE BOLGANLARINI OLINGLAR

    @Query(value = "select count(*) from orders where client_id=:agent_id or agent_id=:agent_id and active=true", nativeQuery = true)
    int countUserOrders(@Param("agent_id") UUID agent_id);

    List<Order> findAllByClient(User client);

    List<Order> findAllByClient_Id(UUID client_id);

    boolean existsBySerialNumber(String item);


    @Query(nativeQuery = true, value = "select cast(id as varchar) , serial_number from orders o where lower(serial_number) like lower(concat(:serialNumber, '%')) order by created_at")
    List<Object[]> searchBySerialNumber(@Param("serialNumber") String serialNumber);

    @Query(nativeQuery = true,
            value = "select * from orders where serial_number like :search or\n" +
                    " agent_id in(select id from users where id in\n" +
                    "(select user_id from user_role where role_id in\n" +
                    "( select  id from role where role_name='ROLE_AGENT')) and \n" +
                    "lower(first_name) like lower(:search) or lower(last_name) like \n" +
                    "lower(:search) or lower(email) like lower(:search)) or\n" +
                    "client_id in(select id from users where id in\n" +
                    "(select user_id from user_role where role_id in\n" +
                    "( select  id from role where role_name='ROLE_USER')) and \n" +
                    "lower(first_name) like lower(:search) or lower(last_name) like \n" +
                    "lower(:search) or lower(email) like lower(:search)) or\n" +
                    " zip_code_id in (select id from zip_code where code like :search) limit :size offset (:page*:size) ;\n")
    List<Order> getBySearch(@Param("search") String search, @Param(value = "page") int page, @Param(value = "size") int size);

    @Query(nativeQuery = true,
            value = "select count (*) from orders where serial_number like :search or\n" +
                    " agent_id in(select id from users where id in\n" +
                    "(select user_id from user_role where role_id in\n" +
                    "( select  id from role where role_name='ROLE_AGENT')) and \n" +
                    "lower(first_name) like lower(:search) or lower(last_name) like \n" +
                    "lower(:search) or lower(email) like lower(:search)) or\n" +
                    "client_id in(select id from users where id in\n" +
                    "(select user_id from user_role where role_id in\n" +
                    "( select  id from role where role_name='ROLE_USER')) and \n" +
                    "lower(first_name) like lower(:search) or lower(last_name) like \n" +
                    "lower(:search) or lower(email) like lower(:search)) or\n" +
                    " zip_code_id in (select id from zip_code where code like :search) ;\n")
    Long countBySearch(@Param("search") String search);

    @Query(nativeQuery = true,
            value = "select * from orders where (serial_number like :search or\n" +
                    "agent_id in(select id from users where id in\n" +
                    "(select user_id from user_role where role_id in\n" +
                    "( select  id from role where role_name='ROLE_AGENT')) and\n" +
                    "lower(first_name) like lower(:search) or lower(last_name) like\n" +
                    "lower(:search) or lower(email) like lower(:search)) or\n" +
                    "zip_code_id in (select id from zip_code where code like :search)) and\n" +
                    "client_id=:clientId limit :size offset (:page*:size);\n")
    List<Order> getBySearchForClient(@Param("search") String search, @Param("clientId") UUID clientId, @Param(value = "page") int page, @Param(value = "size") int size);

    @Query(nativeQuery = true,
            value = "select count(*) from orders where (serial_number like :search or\n" +
                    "agent_id in(select id from users where id in\n" +
                    "(select user_id from user_role where role_id in\n" +
                    "( select  id from role where role_name='ROLE_AGENT')) and\n" +
                    "lower(first_name) like lower(:search) or lower(last_name) like\n" +
                    "lower(:search) or lower(email) like lower(:search)) or\n" +
                    "zip_code_id in (select id from zip_code where code like :search)) and\n" +
                    "client_id=:clientId ;\n")
    Long countBySearchForClient(@Param("search") String search, @Param("clientId") UUID clientId);

    @Query(nativeQuery = true,
            value = "select * from orders where (serial_number like :search or\n" +
                    "client_id in(select id from users where id in\n" +
                    "(select user_id from user_role where role_id in\n" +
                    "( select  id from role where role_name='ROLE_USER')) and\n" +
                    "lower(first_name) like lower(:search) or lower(last_name) like\n" +
                    "lower(:search) or lower(email) like lower(:search)) or\n" +
                    "zip_code_id in (select id from zip_code where code like :search)) and\n" +
                    "agent_id=:agentId limit :size offset (:page*:size);\n")
    List<Order> getBySearchForAgentId(@Param("search") String search, @Param("agentId") UUID agentId, @Param(value = "page") int page, @Param(value = "size") int size);

    @Query(nativeQuery = true,
            value = "select count(*) from orders where (serial_number like :search or\n" +
                    "client_id in(select id from users where id in\n" +
                    "(select user_id from user_role where role_id in\n" +
                    "( select  id from role where role_name='ROLE_USER')) and\n" +
                    "lower(first_name) like lower(:search) or lower(last_name) like\n" +
                    "lower(:search) or lower(email) like lower(:search)) or\n" +
                    "zip_code_id in (select id from zip_code where code like :search)) and\n" +
                    "agent_id=:agentId ;\n")
    Long countBySearchForAgentId(@Param("search") String search, @Param("agentId") UUID agentId);

    @Query(nativeQuery = true,
            value = "select * from orders where service_price_id in( select id from service_price where orders.zip_code_id in(select id from zip_code where code=:code))")
    List<Order> getFilterByZipCode(@Param("code") String code);

    @Query(nativeQuery = true,
            value = "select * from orders where id in(select order_id from time_table where date(from_time)=:date)")
    List<Order> getFilterByDate(@Param("date") Date date);

    @Query(nativeQuery = true,
            value = "select * from orders where service_price_id in( select id from service_price where orders.zip_code_id in(select id from zip_code where code=:code)) and\n" +
                    "                           order_status=UPPER (:status)")
    List<Order> getFilterByStatusAndZipCode(@Param("status") String status, @Param("code") String code);


    @Query(nativeQuery = true,
            value = "select * from orders where service_price_id in( select id from service_price where orders.zip_code_id in(select id from zip_code where code=:code)) and\n" +
                    "        id in(select order_id from time_table where date(from_time)=:date)")
    List<Order> getFilterByDateAndZipCode(@Param("date") Date date, @Param("code") String code);

    @Query(nativeQuery = true,
            value = "select * from orders where id in(select order_id from time_table where date(from_time)=:date) and\n" +
                    "                           order_status=UPPER (:status)")
    List<Order> getFilterByStatusAndDate(@Param("date") Date date, @Param("status") String status);

    @Query(nativeQuery = true,
            value = "select * from orders where service_price_id in( select id from service_price where orders.zip_code_id in(select id from zip_code where code=:code)) and\n" +
                    "        id in(select order_id from time_table where date(from_time)=:date) and order_status=UPPER (:status)")
    List<Order> getFilterByStatusAndDateAndZipCode(@Param("date") Date date, @Param("status") String status, @Param("code") String code);

    @Query(nativeQuery = true,
            value = "select * from orders where order_status=UPPER (:status)")
    List<Order> getFilterByStatus(@Param("status") String status);


    @Query(nativeQuery = true, value = "select count(*) from orders where :today < created_at and service_price_id in (select id from service_price where service_id in (select id from service where main_service_id = (select id from main_service where online = :isOnline)))")
    Integer getOrdersToday(@Param("today") Timestamp today, @Param("isOnline") boolean isOnline);

    @Query(nativeQuery = true, value = "select count(*) from orders where service_price_id in (select id from service_price where service_id in (select id from service where main_service_id in (select id from main_service where online = :isOnline)))")
    Integer getOrdersAllTime(@Param("isOnline") boolean isOnline);


    @Query(nativeQuery = true, value = "SELECT CAST(client_id as varchar ) FROM orders WHERE order_status='COMPLETED' AND active=true AND created_at<= :to AND created_at>= :from")
    List<String> findAllClientsUUID(@Param("to") Timestamp to, @Param("from") Timestamp from);

//    (select sum(amount), id from orders where created_at>= '2020-11-12 10:18:36.300' and created_at<= '2020-11-12 10:18:36.700' and client_id = '8f046c37-7d74-4da1-bf65-389e9bf1f92b' and active=true and order_status='COMPLETED' and id not in (select orders from loyalty_discount_orders) group by id)
//    (select sum(amount), id from orders where created_at>= :fromTime and created_at<= :now and client_id = :userId and active=true and order_status='COMPLETED' and id not in (select orders from loyalty_discount_orders) group by id)

    //    @Query(nativeQuery = true, value = "select ((select sum(amount) from orders where created_at>= :fromTime and created_at<= :now and client_id = :userId and active=true and order_status='COMPLETED' and id not in (select orders from loyalty_discount_orders)) <= :minPriceAmount)")
//    bo

    @Query(nativeQuery = true, value = "(select amount, CAST(id as varchar) from orders where created_at>=:fromTime " +
            "and created_at<=:now and client_id =:userId and active=true " +
            "and order_status='COMPLETED' and (select (select COUNT(*) from ldt_order where order_id=orders.id " +
            "and loyalty_discount_tariff_id=:ldtId)=0) " +
            "group by id)")
    List<Object[]> checkDiscount(@Param("now") Timestamp now, @Param("fromTime") Timestamp fromTime, @Param("userId") UUID userId, @Param("ldtId") Integer ldtId);

    @Query(nativeQuery = true, value = "select count(*) from orders where :today < created_at and register_by <> 'WEB'")
    Integer getAllCountByCreatedAtLessThanAndRegisterByMobile(@Param("today") Timestamp today);

    @Query(nativeQuery = true, value = "select count(*) from orders where register_by <> 'WEB'")
    Integer getAllByRegisterByMobile();

    @Query(nativeQuery = true, value = "select count(*) from orders_documents where documents_id in (:documentIds)")
    Integer existDocument(@Param("documentIds")List<UUID> documentIds);
}

