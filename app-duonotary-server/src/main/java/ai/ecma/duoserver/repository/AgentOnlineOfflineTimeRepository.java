package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.AgentOnlineOfflineTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

public interface AgentOnlineOfflineTimeRepository extends JpaRepository<AgentOnlineOfflineTime, UUID> {

    @Query(nativeQuery = true,
            value = "select sum(date_part('minute',created_at))-(select sum(date_part('minute',created_at)) from agent_online_offline_time where agent_id=:agentId and online limit (select count(t.*) from agent_online_offline_time t where agent_id=:agentId and online=false))from agent_online_offline_time t where agent_id=:agentId and online=false")
    Object getOnlineAgentHour(@Param("agentId") UUID agentId);
}
