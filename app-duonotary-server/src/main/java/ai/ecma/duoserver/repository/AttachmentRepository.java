package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Attachment;
import ai.ecma.duoserver.entity.enums.AttachmentType;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {
    @Transactional
    @Modifying
    @Query(value = "delete from attachment where id=:id",nativeQuery = true)
    void delete(@Param("id") UUID id);

    @Query(nativeQuery = true,value = "select id from attachment where attachment_type=:type")
    List<UUID> findAllByAttachmentType(@Param("type") String type);

}
