package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.User;
import ai.ecma.duoserver.entity.UserZipCode;
import ai.ecma.duoserver.entity.ZipCode;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
public interface UserZipCodeRepository extends JpaRepository<UserZipCode, UUID> {

    @Transactional
    @Modifying
    void deleteAllByUserId(UUID user_id);

    @Transactional
    @Modifying
    void deleteByZipCodeIdAndUserId(UUID zipCode_id, UUID user_id);

    List<UserZipCode> findAllByUser_id(UUID user_id);
    List<UserZipCode> findAllByUserAndZipCodeIn(User user, Collection<ZipCode> zipCode);
    @Query(nativeQuery = true,value = "select * from user_zip_code where user_id=:userId  and  zip_code_id in(select id from zip_code where county_id in(select id from county where state_id =:stateId))")
    List<UserZipCode> getAllByUserAndState(@Param("userId")UUID userId,@Param("stateId")UUID stateId);

    boolean existsByZipCode_idAndUser_Id(UUID zipCode_id, UUID user_id);


}
