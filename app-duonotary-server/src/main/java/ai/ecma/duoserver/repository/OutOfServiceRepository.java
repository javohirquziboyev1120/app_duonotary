package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.OutOfService;
import ai.ecma.duoserver.projection.CustomOutOfService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.UUID;

@RepositoryRestResource(path = "outOfService", collectionResourceRel = "list", excerptProjection = CustomOutOfService.class)
public interface OutOfServiceRepository extends JpaRepository<OutOfService, UUID> {

    /**
     * email bormi yoqmi tekshirish
     * @param email
     * @return
     */
    boolean existsByEmail(String email);

    /**
     * Zipcode va sent orqali list olish
     * @param zipCode
     * @param sent
     * @return
     */
    List<OutOfService> findAllByZipCodeAndSent(String zipCode, boolean sent);


}
