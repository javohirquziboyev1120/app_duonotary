package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Pricing;
import ai.ecma.duoserver.payload.PricingDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface PricingRepository extends JpaRepository<Pricing, UUID> {

    @Query(value = "select coalesce(price,0.0) from pricing where (from_count<= :countDocument and till_count>=:countDocument) or every_count<=:countDocument and service_price_id=:service limit 1", nativeQuery = true)
    double getPriceByCountDocAndServicePrice(int countDocument, UUID service);

    Page<Pricing> findAllBy(Pageable pageable);

    List<Pricing> findAllByServicePrice_IdAndActiveTrue(UUID servicePrice_id);

    @Query(nativeQuery = true,
            value = "select * from pricing where service_price_id in(select id from service_price where service_id=:serviceId)"
    )
    Pricing getPricingByService(UUID serviceId);

    @Query(nativeQuery = true,
            value = "select  min(p.from_count) as min_from_count, min(p.every_count) as min_every_count, max(p.till_count) as max_till_count, min(p.price) as min_price, max(p.price) max_price,\n" +
                    "(select name from state where id = p.state_id) as state_name, cast(state_id as varchar) as state_id from pricing p where state_id is not null group by  state_id, state_name\n")
    List<Object[]> getPricingForDashboard();
    @Query(nativeQuery = true,
            value = "select  min(p.from_count) as min_from_count, min(p.every_count) as min_every_count, max(p.till_count) as max_till_count, min(p.price) as min_price, max(p.price) max_price,\n" +
                    "(select name from state where id = p.state_id) as state_name, cast(state_id as varchar) as state_id from pricing p where state_id is not null group by  state_id, state_name\n")
    List<PricingDto> getPricingForDashboard2();

    @Query(nativeQuery = true, value = "select min(p.from_count) as min_from_count, min(p.every_count) as min_every_count, max(p.till_count) as max_till_count," +
            " min(p.price) as min_price, max(p.price) max_price, 'online' as state_name from pricing p where p.state_id is null group by state_name")
    Object[] findByStateIsNULL();

    @Query(nativeQuery = true,
            value = "select p.from_count, p.price, p.till_count, p.every_count,p.active,\n" +
                    "       ss.name\n" +
                    "from pricing p join service_price sp on p.service_price_id = sp.id join service s on sp.service_id = s.id join sub_service ss on s.sub_service_id = ss.id\n" +
                    "where state_id = :stateId group by p.from_count, p.price, p.till_count, p.every_count,p.active, ss.name")
    List<Object[]> getPricingForState(UUID stateId);

    @Query(nativeQuery = true,
            value = "select p.from_count, p.price, p.till_count, p.every_count,p.active,\n" +
                    "       ss.name\n" +
                    "from pricing p join service_price sp on p.service_price_id = sp.id join service s on sp.service_id = s.id join sub_service ss on s.sub_service_id = ss.id\n" +
                    "where state_id is null")
    List<Object[]> getPricingForStateIsNull();

    @Query(nativeQuery = true,
            value = "select p.* from pricing p  where p.from_count=:fromCount\n" +
                    "and p.till_count=:tillCount and  p.state_id=:stateId and p.price=:price and  service_price_id in\n" +
                    "(select id from service_price where service_id in (select id from service where sub_service_id in\n" +
                    "(select  id from sub_service where name=:subServiceName)))")
    List<Pricing> findAllByFromCountAndTillCountAndStateIdAndPrice(Integer fromCount, Integer tillCount, UUID stateId, double price, String subServiceName);

    @Query(nativeQuery = true,
            value = "select p.* from pricing p  where p.every_count=:everyCount\n" +
                    "and p.state_id=:stateId and p.price=:price and  service_price_id in\n" +
                    "(select id from service_price where service_id in (select id from service where sub_service_id in\n" +
                    "(select  id from sub_service where name=:subServiceName)))")
    List<Pricing> findAllByEveryCountAndStateIdAndPrice(Integer everyCount, UUID stateId, double price, String subServiceName);

    @Query(nativeQuery = true,
            value = "select p.* from pricing p  where p.from_count=:fromCount\n" +
                    "and p.till_count=:tillCount and  p.state_id is null and p.price=:price and  service_price_id in\n" +
                    "(select id from service_price where service_id in (select id from service where sub_service_id in\n" +
                    "(select  id from sub_service where name=:subServiceName)))")
    List<Pricing> findAllByFromCountAndTillCountAndStateIdNullAndPrice(Integer fromCount, Integer tillCount, double price, String subServiceName);

    @Query(nativeQuery = true,
            value = "select count(*) from pricing where state_id in (:stateList) and service_price_id in(select id from service_price where service_id=:serviceId) and (:fromCount between from_count and till_count  or :tillCount between from_count and till_count)")
    Integer findByServiceIdAndFromCount(UUID serviceId, Integer fromCount, Integer tillCount, List<UUID> stateList);

    @Query(nativeQuery = true,
            value = "select * from pricing where state_id in (:stateList) and service_price_id in(select id from service_price where service_id=:serviceId) and state_id=:stateId and every_count is not null")
    List<Pricing> getByServiceIdAndStateIdAndEveryCount(UUID serviceId, UUID stateId, List<UUID> stateList);

    @Query(nativeQuery = true,
            value = "select * from pricing where service_price_id in(select id from service_price where service_id=:serviceId) and state_id=:stateId and :everyCount between from_count and till_count")
    List<Pricing> findByServiceIdAndStateIdAndEveryCountBetween(UUID serviceId, UUID stateId, Integer everyCount);

    @Query(nativeQuery = true,
            value = "select * from pricing where service_price_id in(select id from service_price where service_id=:serviceId) and state_id=:stateId and every_count=:everyCount ")
    List<Pricing> findByServiceIdAndStateIdAndEveryCount(UUID serviceId, UUID stateId, Integer everyCount);

    @Query(nativeQuery = true,
            value = "select count(*) from pricing where state_id=:stateId and :newFromCount between from_count and till_count")
    Integer findByStateIsAndFromCount(UUID stateId, Integer newFromCount);

    @Query(nativeQuery = true,
            value = "select count(*) from pricing where state_id is null and :newFromCount between from_count and till_count")
    Integer getByStateIsNullAndFromCount(Integer newFromCount);

    @Query(nativeQuery = true,
            value = "select * from pricing where state_id=:stateId\n" +
                    "and from_count<>:fromCount and till_count<>:tillCount\n" +
                    "and service_price_id in\n" +
                    "(select id from service_price where service_id in\n" +
                    "(select id from service where sub_service_id in(select id from sub_service where name=:subServiceName)))")
    List<Pricing> getAntiAllPricingStateAndSubServiceName(UUID stateId, Integer fromCount, Integer tillCount, String subServiceName);

    @Query(nativeQuery = true,
            value = "select * from pricing where state_id=:stateId \n" +
                    "and  every_count<>:everyCount \n" +
                    "and service_price_id in\n" +
                    "(select id from service_price where service_id in\n" +
                    "(select id from service where sub_service_id in(select id from sub_service where name=:subServiceName)))")
    List<Pricing> getAntiAllPricingStateAndSubServiceName2(UUID stateId, Integer everyCount, String subServiceName);

    @Query(nativeQuery = true,
            value = "select * from pricing where state_id is null \n" +
                    "and  every_count<>:everyCount \n" +
                    "and service_price_id in\n" +
                    "(select id from service_price where service_id in\n" +
                    "(select id from service where sub_service_id in(select id from sub_service where name=:subServiceName)))")
    List<Pricing> getAntiAllPricingStateIsNullAndSubServiceName2(Integer everyCount, String subServiceName);

    @Query(nativeQuery = true,
            value = "select * from pricing where state_id=:stateId\n" +
                    "and every_count=:everyCount \n" +
                    "and service_price_id in\n" +
                    "(select id from service_price where service_id in\n" +
                    "(select id from service where sub_service_id in(select id from sub_service where name=:subServiceName)))")
    List<Pricing> getAllPricingStateAndSubServiceName2(UUID stateId, Integer everyCount, String subServiceName);

    @Query(nativeQuery = true,
            value = "select * from pricing where state_id is null\n" +
                    "and every_count=:everyCount \n" +
                    "and service_price_id in\n" +
                    "(select id from service_price where service_id in\n" +
                    "(select id from service where sub_service_id in(select id from sub_service where name=:subServiceName)))")
    List<Pricing> getAllPricingStateIsNullAndSubServiceName(Integer everyCount, String subServiceName);

    @Query(nativeQuery = true,
            value = "select * from pricing where state_id=:stateId\n" +
                    "and  :everyCount between from_count and till_count\n" +
                    "and service_price_id in\n" +
                    "(select id from service_price where service_id in\n" +
                    "(select id from service where sub_service_id in(select id from sub_service where name=:subServiceName)))")
    List<Pricing> getAntiAllPricingStateAndSubServiceNameBetween(UUID stateId, Integer everyCount, String subServiceName);

    @Query(nativeQuery = true,
            value = "select * from pricing where state_id is null \n" +
                    "and  :everyCount between from_count and till_count\n" +
                    "and service_price_id in\n" +
                    "(select id from service_price where service_id in\n" +
                    "(select id from service where sub_service_id in(select id from sub_service where name=:subServiceName)))")
    List<Pricing> getAntiAllPricingStateIsNullAndSubServiceNameBetween(Integer everyCount, String subServiceName);

    @Query(nativeQuery = true,
            value = "select * from pricing where state_id=:stateId\n" +
                    "and from_count=:fromCount and till_count=:tillCount\n" +
                    "and service_price_id in\n" +
                    "(select id from service_price where service_id in\n" +
                    "(select id from service where sub_service_id in(select id from sub_service where name=:subServiceName)))")
    List<Pricing> getAllPricingStateAndSubServiceName(UUID stateId, Integer fromCount, Integer tillCount, String subServiceName);

    @Query(nativeQuery = true,
            value = "select * from pricing where state_id is null \n" +
                    "and from_count<>:fromCount and till_count<>:tillCount\n" +
                    "and service_price_id in\n" +
                    "(select id from service_price where service_id in\n" +
                    "(select id from service where sub_service_id in(select id from sub_service where name=:subServiceName)))")
    List<Pricing> getAllPricingStateIsNullAndSubServiceName(Integer fromCount, Integer tillCount, String subServiceName);

    @Query(nativeQuery = true,
            value = "select * from pricing where state_id is null \n" +
                    "and from_count=:fromCount and till_count=:tillCount\n" +
                    "and service_price_id in\n" +
                    "(select id from service_price where service_id in\n" +
                    "(select id from service where sub_service_id in(select id from sub_service where name=:subServiceName)))")
    List<Pricing> getAllPricingStateIsNullAndSubServiceName2(Integer fromCount, Integer tillCount, String subServiceName);

    @Query(nativeQuery = true,
            value = "select * from pricing where state_id is null \n" +
                    "and every_count=:every_count and price=:price")
    List<Pricing> findAllByEveryCountAndPriceAndStateIsNull(Integer every_count, double price);

    List<Pricing> findAllByStateIsNullAndEveryCountIsNotNull();


    List<Pricing> findAllByFromCountAndTillCountAndPriceAndStateIsNull(Integer fromCount, Integer tillCount, double price);

//    List<Pricing> findAllByEveryCountAndPriceAndStateIsNull(Integer everyCount, double price);


}
