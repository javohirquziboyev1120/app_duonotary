package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.CustomDiscount;
import ai.ecma.duoserver.entity.CustomDiscountTariff;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

@JaversSpringDataAuditable
public interface CustomDiscountTariffRepository extends JpaRepository<CustomDiscountTariff, UUID> {
    @Query(nativeQuery = true,
            value = "select * from custom_discount_tariff where customer_id=:userId and active")
    List<CustomDiscountTariff> findAllByUserId(@Param("userId") UUID userId);
}
