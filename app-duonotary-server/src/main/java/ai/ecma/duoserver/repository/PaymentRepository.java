package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Order;
import ai.ecma.duoserver.entity.Payment;
import ai.ecma.duoserver.entity.enums.PayStatus;
import ai.ecma.duoserver.payload.DashboardOrderCashDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PaymentRepository extends JpaRepository<Payment, UUID> {
    Optional<Payment> findByCheckNumberOrId(String checkNumber, UUID id);

    Optional<Payment> findByClientIdAndOrder(String clientId, Order order);

    Optional<Payment> findByOrder(Order order);

    Optional<Payment> findByOrder_Id(UUID order);

    List<Payment> findAllByOrder_IdAndPayStatus(UUID order_id, PayStatus payStatus);

    Page<Payment> findAllByPayStatus(PayStatus payStatus, Pageable pageable);

    @Query(nativeQuery = true, value = "select sum(pay_sum)                                                                           as allTime,\n" +
            "       (select sum(pay_sum)\n" +
            "        from payment\n" +
            "        where order_id in (select id from orders)\n" +
            "          and pay_status = 'PAYED'\n" +
            "          and updated_at >= :today\n" +
            "          and updated_at < date_trunc('day', cast(:today as date) + INTERVAL '1' day)) as today,\n" +
            "       (select sum(pay_sum)\n" +
            "        from payment\n" +
            "        where order_id in (select id from orders)\n" +
            "          and pay_status = 'PAYED'\n" +
            "          and updated_at >= :lastWeek)                                                   as weekly,\n" +
            "       (select sum(pay_sum)\n" +
            "        from payment\n" +
            "        where order_id in (select id from orders)\n" +
            "          and pay_status = 'PAYED'\n" +
            "          and updated_at >= :lastMonth)                                                   as monthly\n" +
            "from payment\n" +
            "where order_id in (select id from orders)\n" +
            "  and pay_status = 'PAYED'")
    DashboardOrderCashDto getOrdersCashBySelectedDate(@Param("today") Timestamp today, @Param("lastWeek") Timestamp lastWeek, @Param("lastMonth") Timestamp lastMonth);

    Optional<Payment> findByChargeIdAndOrder_Id(String chargeId, UUID order_id);
}
