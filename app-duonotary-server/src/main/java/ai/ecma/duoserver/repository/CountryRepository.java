package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Country;
import ai.ecma.duoserver.projection.CustomCountry;
import org.javers.spring.annotation.JaversSpringDataAuditable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@JaversSpringDataAuditable
@RepositoryRestResource(path = "country", collectionResourceRel = "list", excerptProjection = CustomCountry.class)
public interface CountryRepository extends JpaRepository<Country, UUID> {

    @PreAuthorize("hasAnyAuthority('DELETE_COUNTRY')")
    @Override
    void deleteById(UUID uuid);

    @PreAuthorize("hasAnyAuthority('SAVE_COUNTRY','EDIT_COUNTRY')")
    @Override
    <S extends Country> S save(S s);

    @RestResource(path ="/embassy")
    List<Country> findAllByEmbassyIsTrue();




}
