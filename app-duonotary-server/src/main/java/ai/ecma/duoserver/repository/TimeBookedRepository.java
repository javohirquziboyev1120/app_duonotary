package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.PayType;
import ai.ecma.duoserver.entity.TimeBooked;
import ai.ecma.duoserver.entity.TimeDuration;
import ai.ecma.duoserver.projection.CustomTimeBooked;
import ai.ecma.duoserver.projection.CustomTimeDuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.UUID;


@RepositoryRestResource(path = "timeBooked", collectionResourceRel = "list", excerptProjection = CustomTimeBooked.class)
@CrossOrigin
public interface TimeBookedRepository extends JpaRepository<TimeBooked, Integer> {
    @Query(value = "select * from time_booked order by created_at desc limit 1", nativeQuery = true)
    TimeBooked selectFirst();

    @PreAuthorize("hasAnyAuthority('DELETE_TIME_BOOKED')")
    @Override
    void deleteById(Integer integer);

    @PreAuthorize("hasAnyAuthority('SAVE_TIME_BOOKED')")
    @Override
    <S extends TimeBooked> List<S> saveAll(Iterable<S> iterable);

    @PreAuthorize("hasAnyAuthority('SAVE_TIME_BOOKED')")
    @Override
    <S extends TimeBooked> S save(S s);

}
