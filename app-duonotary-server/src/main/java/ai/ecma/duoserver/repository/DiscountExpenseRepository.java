package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.DiscountExpense;
import ai.ecma.duoserver.entity.PayType;
import com.stripe.model.Discount;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;
import java.util.UUID;

@RepositoryRestResource(path = "discountExpense", collectionResourceRel = "list")
public interface DiscountExpenseRepository extends JpaRepository<DiscountExpense, UUID> {
    boolean existsByDiscountIdAndSharingId(UUID discountId, boolean sharingId);


    @Override
    void deleteById(UUID uuid);

    @Override
    <S extends DiscountExpense> S save(S s);

    List<DiscountExpense> findAllByOrder_Id(UUID order_id);
}
