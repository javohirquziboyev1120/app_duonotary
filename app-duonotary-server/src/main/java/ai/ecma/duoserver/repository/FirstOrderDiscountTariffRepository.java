package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.FirstOrderDiscountTariff;
import ai.ecma.duoserver.entity.ZipCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface FirstOrderDiscountTariffRepository extends JpaRepository<FirstOrderDiscountTariff, UUID> {

    @Query(nativeQuery = true,value = "select coalesce(min(percent),0) as minPercent,coalesce(max(percent),0) as maxPercent,(select count(*)from first_order_discount_tariff where active=true and online=false) activeCount from first_order_discount_tariff where online=false")
    List<Object[]> getMAxAndMinPercents();

    Optional<FirstOrderDiscountTariff> findByZipCodeCode(String zipCode_code);

    Optional<FirstOrderDiscountTariff> findByZipCodeAndActive(ZipCode zipCode, boolean active);

    @Query(nativeQuery = true,value = "update first_order_discount_tariff set active=:active where online=false")
    void changeActiveStatusFirstOrderDiscountTariff(@Param(value="active") boolean active);

    Optional<FirstOrderDiscountTariff> findByOnlineIsTrueAndActiveIsTrue();

    Optional<FirstOrderDiscountTariff> findByOnline(boolean online);
    List<FirstOrderDiscountTariff> findAllByOnline(boolean online);
}
