package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.Service;
import ai.ecma.duoserver.entity.ServicePrice;
import ai.ecma.duoserver.entity.ZipCode;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@JaversSpringDataAuditable
public interface ServicePriceRepository extends JpaRepository<ServicePrice, UUID> {
    boolean existsByServiceIdAndZipCodeId(UUID service_id, UUID zipCode_id);

    List<ServicePrice> findAllById(UUID id);


    @Query(nativeQuery = true,
            value = "select distinct cast(sp.service_id as varchar)as sid ,\n" +
                    "                    (select coalesce(min(sp2.price), 0)from service_price sp2 where sp2.service_id=sp.service_id) minPrice,\n" +
                    "                    (select coalesce(max(sp3.price),0) from service_price sp3 where sp3.service_id=sp.service_id) maxPrice,\n" +
                    "                    (select ms.name  from service s join main_service ms on s.main_service_id = ms.id where s.id=sp.service_id) mainServiceName, \n" + "" +
                    "                    (select ms1.online  from service s1 join main_service ms1 on s1.main_service_id = ms1.id where s1.id=sp.service_id ) online,\n" + "  " +
                    "                    (select ss.name  from service s2 join sub_service ss on s2.sub_service_id = ss.id where s2.id=sp.service_id) subServiceName, \n" +
                    "                    (select s3.active  from service s3  where s3.id=sp.service_id) serviceActive, \n" +
                    "                    (select ss1.description  from service s4 join sub_service ss1 on s4.sub_service_id = ss1.id where s4.id=sp.service_id) subServiceDescription\n" +
                    "                    from service_price sp order by sid ")
    List<Object[]> getServicePriceForDashboard();

    Optional<ServicePrice> findByServiceId(UUID service_id);

    List<ServicePrice> findAllByService_Id(UUID service_id);

    @Query(nativeQuery = true,
            value = "select * from additional_service_price where service_price_id in (SELECT id from  service_price where service_id=:serviceId and zip_code_id in(Select id from zip_code where county_id in(select id from county where state_id=:stateId)))")
    List<ServicePrice> findAllByService_IdAndStateId(@Param("serviceId") UUID serviceId, @Param("stateId") UUID stateId);

    List<ServicePrice> findAllByService_IdAndZipCode_County_State_IdIn(UUID service_id, Collection<UUID> zipCode_county_state_id);

    @Query(nativeQuery = true,
            value = "select * from additional_service_price where service_price_id in (SELECT id from  service_price where service_id=:serviceId and zip_code_id in(Select id from zip_code where county_id =:countyId))")
    List<ServicePrice> findAllByService_IdAndCountyId(@Param("serviceId") UUID serviceId, @Param("countyId") UUID countyId);

    @Query(nativeQuery = true,
            value = "select * from additional_service_price where service_price_id in (SELECT id from  service_price where service_id=:serviceId and zip_code_id =:zipCodeId)")
    List<ServicePrice> findAllByService_IdAndZipcodeId(@Param("serviceId") UUID serviceId, @Param("zipCodeId") UUID zipCodeId);


    Optional<ServicePrice> findByIdAndActiveTrue(UUID id);

    Optional<ServicePrice> findFirstByActiveTrueAndZipCode_ActiveTrueAndZipCode_CodeAndService_MainServiceId(String zipCode_code, UUID service_mainService_id);

    List<ServicePrice> findAllByServiceAndZipCodeIn(Service service, Collection<ZipCode> zipCode);

    List<ServicePrice> findAllByServiceAndZipCode(Service service, ZipCode zipCode);

    List<ServicePrice> findAllByServiceIdAndZipCodeIdIn(UUID service_id, Collection<UUID> zipCode_id);

    @Query(nativeQuery = true,
            value = "select * from service_price where zip_code_id=:zipCodeId and active and service_id in (select id from service where main_service_id in (select id from main_service where online=false and active))")
    List<ServicePrice> findAllByZipCodeAndActiveAndInPerson(UUID zipCodeId);

    List<ServicePrice> findAllByService_MainService_Online(Boolean service_mainService_online);

    List<ServicePrice> findAllByServiceIn(Collection<Service> service);

    @Query(value = "select * from service_price where service_id in(select id from service where sub_service_id in(select id from sub_service where default_input=true)) limit 1", nativeQuery = true)
    Optional<ServicePrice> findByService_SubService_DefaultInputTrue();

    boolean existsByIdAndService_MainService_OnlineIsTrue(UUID id);

}
