package ai.ecma.duoserver.repository;

import ai.ecma.duoserver.entity.CanceledOrder;
import ai.ecma.duoserver.entity.Order;
import ai.ecma.duoserver.entity.User;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;
@JaversSpringDataAuditable
public interface CanceledOrderRepository extends JpaRepository<CanceledOrder, UUID> {

    List<CanceledOrder> findByOrderAndAcceptIsNullAndFromAgent(Order order, User fromAgent);

    List<CanceledOrder> findAllByToAgent_IdAndAcceptIsNull(UUID toAgent_id);
}