INSERT  into role(role_name) values ('ROLE_SUPER_ADMIN'),('ROLE_ADMIN'),('ROLE_AGENT'),('ROLE_USER');

INSERT  INTO week_day(id, day, order_number) VALUES (1, 'SUNDAY', 1),(2, 'MONDAY', 2),(3, 'TUESDAY', 3),(4, 'WEDNESDAY', 4),(5, 'THURSDAY', 5),(6, 'FRIDAY', 6),(7, 'SATURDAY', 7);
-- insert into sharing_discount_tariff(id,active,percent) values (1,false,0);

INSERT INTO time_duration(id, duration_time) VALUES (1, 30);
INSERT INTO time_booked(id, booked_duration) VALUES (1, 15);
insert into timezone_name (name) values ('America/New_York'),('America/Detroit'),('America/Kentucky/Louisville'),('America/Kentucky/Monticello'),('America/Indiana/Indianapolis'),('America/Indiana/Vincennes'),('America/Indiana/Winamac'),('America/Indiana/Marengo'),('America/Indiana/Petersburg'),('America/Indiana/Vevay'),('America/Chicago'),('America/Indiana/Tell_City'),('America/Indiana/Knox'),('America/Menominee'),('America/North_Dakota/Center'),('America/North_Dakota/New_Salem'),('America/North_Dakota/Beulah'),('America/Denver'),('America/Boise'),('America/Phoenix'),('America/Los_Angeles'),('America/Anchorage'),('America/Juneau'),('America/Sitka'),('America/Metlakatla'),('America/Yakutat'),('America/Nome'),('America/Adak'),('Pacific/Honolulu');