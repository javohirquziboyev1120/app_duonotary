<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html
        xmlns="http://www.w3.org/1999/xhtml"
        style="
    width: 100%;
    font-family: lato, 'helvetica neue', helvetica, arial, sans-serif;
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    padding: 0;
    margin: 0;
  "
>
<head>
    <meta charset="UTF-8" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="x-apple-disable-message-reformatting" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta content="telephone=no" name="format-detection" />
    <title>New email</title>
    <!--[if (mso 16)]>
    <style type="text/css">
        a {
            text-decoration: none;
        }
    </style>
    <![endif]-->
    <!--[if gte mso 9
      ]><style>
        sup {
          font-size: 100% !important;
        }
      </style><!
    [endif]-->
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG></o:AllowPNG>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <link
            href="https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i"
            rel="stylesheet"
    />
    <!--<![endif]-->
    <style type="text/css">
        #outlook a {
            padding: 0;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }

        .es-button {
            mso-style-priority: 100 !important;
            text-decoration: none !important;
        }

        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        .es-desk-hidden {
            display: none;
            float: left;
            overflow: hidden;
            width: 0;
            max-height: 0;
            line-height: 0;
            mso-hide: all;
        }

        @media only screen and (max-width: 600px) {
            p,
            ul li,
            ol li,
            a {
                font-size: 16px !important;
                line-height: 150% !important;
            }

            h1 {
                font-size: 30px !important;
                text-align: center;
                line-height: 120% !important;
            }

            h2 {
                font-size: 26px !important;
                text-align: center;
                line-height: 120% !important;
            }

            h3 {
                font-size: 20px !important;
                text-align: center;
                line-height: 120% !important;
            }

            h1 a {
                font-size: 30px !important;
            }

            h2 a {
                font-size: 26px !important;
            }

            h3 a {
                font-size: 20px !important;
            }

            .es-menu td a {
                font-size: 16px !important;
            }

            .es-header-body p,
            .es-header-body ul li,
            .es-header-body ol li,
            .es-header-body a {
                font-size: 16px !important;
            }

            .es-footer-body p,
            .es-footer-body ul li,
            .es-footer-body ol li,
            .es-footer-body a {
                font-size: 16px !important;
            }

            .es-infoblock p,
            .es-infoblock ul li,
            .es-infoblock ol li,
            .es-infoblock a {
                font-size: 12px !important;
            }

            *[class="gmail-fix"] {
                display: none !important;
            }

            .es-m-txt-c,
            .es-m-txt-c h1,
            .es-m-txt-c h2,
            .es-m-txt-c h3 {
                text-align: center !important;
            }

            .es-m-txt-r,
            .es-m-txt-r h1,
            .es-m-txt-r h2,
            .es-m-txt-r h3 {
                text-align: right !important;
            }

            .es-m-txt-l,
            .es-m-txt-l h1,
            .es-m-txt-l h2,
            .es-m-txt-l h3 {
                text-align: left !important;
            }

            .es-m-txt-r img,
            .es-m-txt-c img,
            .es-m-txt-l img {
                display: inline !important;
            }

            .es-button-border {
                display: block !important;
            }

            a.es-button {
                font-size: 20px !important;
                display: block !important;
                border-width: 15px 25px 15px 25px !important;
            }

            .es-btn-fw {
                border-width: 10px 0px !important;
                text-align: center !important;
            }

            .es-adaptive table,
            .es-btn-fw,
            .es-btn-fw-brdr,
            .es-left,
            .es-right {
                width: 100% !important;
            }

            .es-content table,
            .es-header table,
            .es-footer table,
            .es-content,
            .es-footer,
            .es-header {
                width: 100% !important;
                max-width: 600px !important;
            }

            .es-adapt-td {
                display: block !important;
                width: 100% !important;
            }

            .adapt-img {
                width: 100% !important;
                height: auto !important;
            }

            .es-m-p0 {
                padding: 0px !important;
            }

            .es-m-p0r {
                padding-right: 0px !important;
            }

            .es-m-p0l {
                padding-left: 0px !important;
            }

            .es-m-p0t {
                padding-top: 0px !important;
            }

            .es-m-p0b {
                padding-bottom: 0 !important;
            }

            .es-m-p20b {
                padding-bottom: 20px !important;
            }

            .es-mobile-hidden,
            .es-hidden {
                display: none !important;
            }

            tr.es-desk-hidden,
            td.es-desk-hidden,
            table.es-desk-hidden {
                width: auto !important;
                overflow: visible !important;
                float: none !important;
                max-height: inherit !important;
                line-height: inherit !important;
            }

            tr.es-desk-hidden {
                display: table-row !important;
            }

            table.es-desk-hidden {
                display: table !important;
            }

            td.es-desk-menu-hidden {
                display: table-cell !important;
            }

            .es-menu td {
                width: 1% !important;
            }

            table.es-table-not-adapt,
            .esd-block-html table {
                width: auto !important;
            }

            table.es-social {
                display: inline-block !important;
            }

            table.es-social td {
                display: inline-block !important;
            }
        }
    </style>
</head>
<body
        style="
      width: 100%;
      font-family: lato, 'helvetica neue', helvetica, arial, sans-serif;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
      padding: 0;
      margin: 0;
    "
>
<div class="es-wrapper-color" style="background-color: #f4f4f4">
    <!--[if gte mso 9]>
    <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
        <v:fill type="tile" color="#f4f4f4"></v:fill>
    </v:background>
    <![endif]-->
    <table
            class="es-wrapper"
            width="100%"
            cellspacing="0"
            cellpadding="0"
            style="
          mso-table-lspace: 0pt;
          mso-table-rspace: 0pt;
          border-collapse: collapse;
          border-spacing: 0px;
          padding: 0;
          margin: 0;
          width: 100%;
          height: 100%;
          background-repeat: repeat;
          background-position: center top;
        "
    >
        <tr class="gmail-fix" height="0" style="border-collapse: collapse">
            <td style="padding: 0; margin: 0">
                <table
                        cellspacing="0"
                        cellpadding="0"
                        border="0"
                        align="center"
                        style="
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
                border-collapse: collapse;
                border-spacing: 0px;
                width: 600px;
              "
                >
                    <tr style="border-collapse: collapse">
                        <td
                                cellpadding="0"
                                cellspacing="0"
                                border="0"
                                style="
                    padding: 0;
                    margin: 0;
                    line-height: 1px;
                    min-width: 600px;
                  "
                                height="0"
                        >
                            <img
                                    src="https://esputnik.com/repository/applications/images/blank.gif"
                                    style="
                      display: block;
                      border: 0;
                      outline: none;
                      text-decoration: none;
                      -ms-interpolation-mode: bicubic;
                      max-height: 0px;
                      min-height: 0px;
                      min-width: 600px;
                      width: 600px;
                    "
                                    alt
                                    width="600"
                                    height="1"
                            />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="border-collapse: collapse">
            <td valign="top" style="padding: 0; margin: 0">
                <table
                        class="es-header"
                        cellspacing="0"
                        cellpadding="0"
                        align="center"
                        style="
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
                border-collapse: collapse;
                border-spacing: 0px;
                table-layout: fixed !important;
                width: 100%;
                background-color: #ffa73b;
                background-repeat: repeat;
                background-position: center top;
              "
                >
                    <tr style="border-collapse: collapse">
                        <td
                                align="center"
                                bgcolor="#cfe2f3"
                                style="padding: 0; margin: 0; background-color: #cfe2f3"
                        >
                            <table
                                    class="es-header-body"
                                    cellspacing="0"
                                    cellpadding="0"
                                    align="center"
                                    style="
                      mso-table-lspace: 0pt;
                      mso-table-rspace: 0pt;
                      border-collapse: collapse;
                      border-spacing: 0px;
                      background-color: transparent;
                      width: 600px;
                    "
                            >
                                <tr style="border-collapse: collapse">
                                    <td
                                            align="left"
                                            style="
                          padding: 0;
                          margin: 0;
                          padding-top: 20px;
                          padding-left: 30px;
                          padding-right: 30px;
                        "
                                    >
                                        <table
                                                cellpadding="0"
                                                cellspacing="0"
                                                width="100%"
                                                style="
                            mso-table-lspace: 0pt;
                            mso-table-rspace: 0pt;
                            border-collapse: collapse;
                            border-spacing: 0px;
                          "
                                        >
                                            <tr style="border-collapse: collapse">
                                                <td
                                                        align="center"
                                                        valign="top"
                                                        style="padding: 0; margin: 0; width: 540px"
                                                >
                                                    <table
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            width="100%"
                                                            style="
                                  mso-table-lspace: 0pt;
                                  mso-table-rspace: 0pt;
                                  border-collapse: collapse;
                                  border-spacing: 0px;
                                "
                                                    >
                                                        <tr style="border-collapse: collapse">
                                                            <td
                                                                    align="center"
                                                                    style="padding: 0; margin: 0; display: none"
                                                            ></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="border-collapse: collapse">
                                    <td
                                            align="left"
                                            bgcolor="#ffffff"
                                            style="
                          padding: 0;
                          margin: 0;
                          padding-top: 20px;
                          padding-left: 30px;
                          padding-right: 30px;
                          background-color: #ffffff;
                        "
                                    >
                                        <!--[if mso]>
                                        <table style="width:540px" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width:260px" valign="top"><![endif]-->
                                        <table
                                                cellpadding="0"
                                                cellspacing="0"
                                                class="es-left"
                                                align="left"
                                                style="
                            mso-table-lspace: 0pt;
                            mso-table-rspace: 0pt;
                            border-collapse: collapse;
                            border-spacing: 0px;
                            float: left;
                          "
                                        >
                                            <tr style="border-collapse: collapse">
                                                <td
                                                        class="es-m-p20b"
                                                        align="left"
                                                        style="padding: 0; margin: 0; width: 260px"
                                                >
                                                    <table
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            width="100%"
                                                            role="presentation"
                                                            style="
                                  mso-table-lspace: 0pt;
                                  mso-table-rspace: 0pt;
                                  border-collapse: collapse;
                                  border-spacing: 0px;
                                "
                                                    >
                                                        <tr style="border-collapse: collapse">
                                                            <td
                                                                    align="center"
                                                                    style="
                                      padding: 15px;
                                      margin: 0;
                                      font-size: 0px;
                                    "
                                                            >
                                                                <img
                                                                        class="adapt-img"
                                                                        src="https://ofdvxs.stripocdn.email/content/guids/CABINET_11e49951a060d733923895f4706798cd/images/74111604909927222.png"
                                                                        alt
                                                                        style="
                                        display: block;
                                        border: 0;
                                        outline: none;
                                        text-decoration: none;
                                        -ms-interpolation-mode: bicubic;
                                      "
                                                                        width="173"
                                                                />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!--[if mso]></td>
                                        <td style="width:20px"></td>
                                        <td style="width:260px" valign="top"><![endif]-->
                                        <table
                                                cellpadding="0"
                                                cellspacing="0"
                                                class="es-right"
                                                align="right"
                                                style="
                            mso-table-lspace: 0pt;
                            mso-table-rspace: 0pt;
                            border-collapse: collapse;
                            border-spacing: 0px;
                            float: right;
                          "
                                        >
                                            <tr style="border-collapse: collapse">
                                                <td
                                                        align="left"
                                                        style="padding: 0; margin: 0; width: 260px"
                                                >
                                                    <table
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            width="100%"
                                                            style="
                                  mso-table-lspace: 0pt;
                                  mso-table-rspace: 0pt;
                                  border-collapse: collapse;
                                  border-spacing: 0px;
                                "
                                                    >
                                                        <tr style="border-collapse: collapse">
                                                            <td
                                                                    align="center"
                                                                    style="padding: 0; margin: 0; display: none"
                                                            ></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!--[if mso]></td></tr></table><![endif]-->
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table
                        class="es-content"
                        cellspacing="0"
                        cellpadding="0"
                        align="center"
                        style="
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
                border-collapse: collapse;
                border-spacing: 0px;
                table-layout: fixed !important;
                width: 100%;
              "
                >
                    <tr style="border-collapse: collapse">
                        <td
                                style="padding: 0; margin: 0; background-color: #cfe2f3"
                                bgcolor="#cfe2f3"
                                align="center"
                        >
                            <table
                                    class="es-content-body"
                                    style="
                      mso-table-lspace: 0pt;
                      mso-table-rspace: 0pt;
                      border-collapse: collapse;
                      border-spacing: 0px;
                      background-color: transparent;
                      width: 600px;
                    "
                                    cellspacing="0"
                                    cellpadding="0"
                                    align="center"
                            >
                                <tr style="border-collapse: collapse">
                                    <td align="left" style="padding: 0; margin: 0">
                                        <table
                                                width="100%"
                                                cellspacing="0"
                                                cellpadding="0"
                                                style="
                            mso-table-lspace: 0pt;
                            mso-table-rspace: 0pt;
                            border-collapse: collapse;
                            border-spacing: 0px;
                          "
                                        >
                                            <tr style="border-collapse: collapse">
                                                <td
                                                        valign="top"
                                                        align="center"
                                                        style="padding: 0; margin: 0; width: 600px"
                                                >
                                                    <table
                                                            style="
                                  mso-table-lspace: 0pt;
                                  mso-table-rspace: 0pt;
                                  border-collapse: separate;
                                  border-spacing: 0px;
                                  background-color: #ffffff;
                                  border-radius: 4px;
                                "
                                                            width="100%"
                                                            cellspacing="0"
                                                            cellpadding="0"
                                                            bgcolor="#ffffff"
                                                            role="presentation"
                                                    >
                                                        <tr style="border-collapse: collapse">
                                                            <td
                                                                    bgcolor="#ffffff"
                                                                    align="center"
                                                                    style="
                                      margin: 0;
                                      padding-top: 5px;
                                      padding-bottom: 5px;
                                      padding-left: 20px;
                                      padding-right: 20px;
                                      font-size: 0;
                                    "
                                                            >
                                                                <table
                                                                        width="100%"
                                                                        height="100%"
                                                                        cellspacing="0"
                                                                        cellpadding="0"
                                                                        border="0"
                                                                        role="presentation"
                                                                        style="
                                        mso-table-lspace: 0pt;
                                        mso-table-rspace: 0pt;
                                        border-collapse: collapse;
                                        border-spacing: 0px;
                                      "
                                                                >
                                                                    <tr style="border-collapse: collapse">
                                                                        <td
                                                                                style="
                                            padding: 0;
                                            margin: 0;
                                            border-bottom: 1px solid #ffffff;
                                            background: #FFFFFFnone repeat
                                              scroll 0% 0%;
                                            height: 1px;
                                            width: 100%;
                                            margin: 0px;
                                          "
                                                                        ></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table
                        class="es-content"
                        cellspacing="0"
                        cellpadding="0"
                        align="center"
                        style="
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
                border-collapse: collapse;
                border-spacing: 0px;
                table-layout: fixed !important;
                width: 100%;
              "
                >
                    <tr style="border-collapse: collapse">
                        <td
                                align="center"
                                bgcolor="#cfe2f3"
                                style="padding: 0; margin: 0; background-color: #cfe2f3"
                        >
                            <table
                                    class="es-content-body"
                                    style="
                      mso-table-lspace: 0pt;
                      mso-table-rspace: 0pt;
                      border-collapse: collapse;
                      border-spacing: 0px;
                      background-color: #ffffff;
                      width: 600px;
                    "
                                    cellspacing="0"
                                    cellpadding="0"
                                    align="center"
                                    bgcolor="#ffffff"
                            >
                                <tr style="border-collapse: collapse">
                                    <td align="left" style="padding: 0; margin: 0">
                                        <table
                                                width="100%"
                                                cellspacing="0"
                                                cellpadding="0"
                                                style="
                            mso-table-lspace: 0pt;
                            mso-table-rspace: 0pt;
                            border-collapse: collapse;
                            border-spacing: 0px;
                          "
                                        >
                                            <tr style="border-collapse: collapse">
                                                <td
                                                        valign="top"
                                                        align="center"
                                                        style="padding: 0; margin: 0; width: 600px"
                                                >
                                                    <table
                                                            style="
                                  mso-table-lspace: 0pt;
                                  mso-table-rspace: 0pt;
                                  border-collapse: separate;
                                  border-spacing: 0px;
                                  border-radius: 4px;
                                  background-color: #ffffff;
                                "
                                                            width="100%"
                                                            cellspacing="0"
                                                            cellpadding="0"
                                                            bgcolor="#ffffff"
                                                            role="presentation"
                                                    >
                                                        <tr style="border-collapse: collapse">
                                                            <td
                                                                    class="es-m-txt-l"
                                                                    bgcolor="#ffffff"
                                                                    align="left"
                                                                    style="
                                      margin: 0;
                                      padding-top: 20px;
                                      padding-bottom: 20px;
                                      padding-left: 30px;
                                      padding-right: 30px;
                                    "
                                                            >
                                                                <p
                                                                        style="
                                        margin: 0;
                                        -webkit-text-size-adjust: none;
                                        -ms-text-size-adjust: none;
                                        mso-line-height-rule: exactly;
                                        font-size: 18px;
                                        font-family: lato, 'helvetica neue',
                                          helvetica, arial, sans-serif;
                                        line-height: 27px;
                                        color: #000000;
                                      "
                                                                >
                                                                    <strong
                                                                    ><span style="font-size: 18px"
                                                                        >Hi ${fullName},</span
                                                                        ></strong
                                                                    ><br />
                                                                </p>
                                                                <p style="color: #000000"></p>
                                                                <p style="color: #000000">${answer}</p>

                                                                <br />
                                                                Thank you!
                                                                <br />
                                                                <br />

                                                                Kind Regards,
                                                                <br />
                                                                DuoNotary Team
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table
                        cellpadding="0"
                        cellspacing="0"
                        class="es-content"
                        align="center"
                        style="
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
                border-collapse: collapse;
                border-spacing: 0px;
                table-layout: fixed !important;
                width: 100%;
              "
                >
                    <tr style="border-collapse: collapse">
                        <td
                                align="center"
                                bgcolor="#cfe2f3"
                                style="padding: 0; margin: 0; background-color: #cfe2f3"
                        >
                            <table
                                    bgcolor="#ffffff"
                                    class="es-content-body"
                                    align="center"
                                    cellpadding="0"
                                    cellspacing="0"
                                    style="
                      mso-table-lspace: 0pt;
                      mso-table-rspace: 0pt;
                      border-collapse: collapse;
                      border-spacing: 0px;
                      background-color: #ffffff;
                      width: 600px;
                    "
                            >
                                <tr style="border-collapse: collapse">
                                    <td
                                            align="left"
                                            bgcolor="#ffffff"
                                            style="
                          padding: 0;
                          margin: 0;
                          padding-top: 20px;
                          padding-left: 30px;
                          padding-right: 30px;
                          background-color: #ffffff;
                        "
                                    >
                                        <table
                                                cellpadding="0"
                                                cellspacing="0"
                                                class="es-left"
                                                align="left"
                                                style="
                            mso-table-lspace: 0pt;
                            mso-table-rspace: 0pt;
                            border-collapse: collapse;
                            border-spacing: 0px;
                            float: left;
                          "
                                        >
                                            <tr style="border-collapse: collapse">
                                                <td
                                                        class="es-m-p0r es-m-p20b"
                                                        align="center"
                                                        style="padding: 0; margin: 0; width: 167px"
                                                >
                                                    <table
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            width="100%"
                                                            role="presentation"
                                                            style="
                                  mso-table-lspace: 0pt;
                                  mso-table-rspace: 0pt;
                                  border-collapse: collapse;
                                  border-spacing: 0px;
                                "
                                                    >
                                                        <tr style="border-collapse: collapse">
                                                            <td
                                                                    align="left"
                                                                    style="padding: 0; margin: 0"
                                                            >
                                                                <p
                                                                        style="
                                        margin: 0;
                                        -webkit-text-size-adjust: none;
                                        -ms-text-size-adjust: none;
                                        mso-line-height-rule: exactly;
                                        font-size: 18px;
                                        font-family: lato, 'helvetica neue',
                                          helvetica, arial, sans-serif;
                                        line-height: 27px;
                                        color: #000000;
                                      "
                                                                >
                                                                    <strong>Download the app:</strong>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td
                                                        class="es-hidden"
                                                        style="padding: 0; margin: 0; width: 20px"
                                                ></td>
                                            </tr>
                                        </table>
                                        <!--[if mso]></td>
                                        <td style="width:167px" valign="top"><![endif]-->
                                        <table
                                                cellpadding="0"
                                                cellspacing="0"
                                                class="es-left"
                                                align="left"
                                                style="
                            mso-table-lspace: 0pt;
                            mso-table-rspace: 0pt;
                            border-collapse: collapse;
                            border-spacing: 0px;
                            float: left;
                          "
                                        >
                                            <tr style="border-collapse: collapse">
                                                <td
                                                        class="es-m-p20b"
                                                        align="center"
                                                        style="padding: 0; margin: 0; width: 167px"
                                                >
                                                    <table
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            width="100%"
                                                            role="presentation"
                                                            style="
                                  mso-table-lspace: 0pt;
                                  mso-table-rspace: 0pt;
                                  border-collapse: collapse;
                                  border-spacing: 0px;
                                "
                                                    >
                                                        <tr style="border-collapse: collapse">
                                                            <td
                                                                    align="center"
                                                                    style="
                                      padding: 0;
                                      margin: 0;
                                      font-size: 0px;
                                    "
                                                            >
                                                                <a
                                                                        target="_blank"
                                                                        href="https://www.apple.com/app-store/"
                                                                        style="
                                        -webkit-text-size-adjust: none;
                                        -ms-text-size-adjust: none;
                                        mso-line-height-rule: exactly;
                                        font-family: lato, 'helvetica neue',
                                          helvetica, arial, sans-serif;
                                        font-size: 18px;
                                        text-decoration: underline;
                                        color: #ffa73b;
                                      "
                                                                ><img
                                                                            class="adapt-img"
                                                                            src="https://ofdvxs.stripocdn.email/content/guids/CABINET_11e49951a060d733923895f4706798cd/images/11141604910389723.png"
                                                                            alt
                                                                            style="
                                          display: block;
                                          border: 0;
                                          outline: none;
                                          text-decoration: none;
                                          -ms-interpolation-mode: bicubic;
                                        "
                                                                            width="167"
                                                                    /></a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!--[if mso]></td>
                                        <td style="width:20px"></td>
                                        <td style="width:166px" valign="top"><![endif]-->
                                        <table
                                                cellpadding="0"
                                                cellspacing="0"
                                                class="es-right"
                                                align="right"
                                                style="
                            mso-table-lspace: 0pt;
                            mso-table-rspace: 0pt;
                            border-collapse: collapse;
                            border-spacing: 0px;
                            float: right;
                          "
                                        >
                                            <tr style="border-collapse: collapse">
                                                <td
                                                        align="center"
                                                        style="padding: 0; margin: 0; width: 166px"
                                                >
                                                    <table
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            width="100%"
                                                            role="presentation"
                                                            style="
                                  mso-table-lspace: 0pt;
                                  mso-table-rspace: 0pt;
                                  border-collapse: collapse;
                                  border-spacing: 0px;
                                "
                                                    >
                                                        <tr style="border-collapse: collapse">
                                                            <td
                                                                    align="center"
                                                                    style="
                                      padding: 0;
                                      margin: 0;
                                      font-size: 0px;
                                    "
                                                            >
                                                                <a
                                                                        target="_blank"
                                                                        href="https://play.google.com/store"
                                                                        style="
                                        -webkit-text-size-adjust: none;
                                        -ms-text-size-adjust: none;
                                        mso-line-height-rule: exactly;
                                        font-family: lato, 'helvetica neue',
                                          helvetica, arial, sans-serif;
                                        font-size: 18px;
                                        text-decoration: underline;
                                        color: #ffa73b;
                                      "
                                                                ><img
                                                                            class="adapt-img"
                                                                            src="https://ofdvxs.stripocdn.email/content/guids/CABINET_11e49951a060d733923895f4706798cd/images/94831604910403827.png"
                                                                            alt
                                                                            style="
                                          display: block;
                                          border: 0;
                                          outline: none;
                                          text-decoration: none;
                                          -ms-interpolation-mode: bicubic;
                                        "
                                                                            width="151"
                                                                    /></a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!--[if mso]></td></tr></table><![endif]-->
                                    </td>
                                </tr>
                                <tr style="border-collapse: collapse">
                                    <td
                                            align="left"
                                            style="
                          padding: 0;
                          margin: 0;
                          padding-top: 20px;
                          padding-left: 30px;
                          padding-right: 30px;
                        "
                                    >
                                        <table
                                                cellpadding="0"
                                                cellspacing="0"
                                                width="100%"
                                                style="
                            mso-table-lspace: 0pt;
                            mso-table-rspace: 0pt;
                            border-collapse: collapse;
                            border-spacing: 0px;
                          "
                                        >
                                            <tr style="border-collapse: collapse">
                                                <td
                                                        align="center"
                                                        valign="top"
                                                        style="padding: 0; margin: 0; width: 540px"
                                                >
                                                    <table
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            width="100%"
                                                            role="presentation"
                                                            style="
                                  mso-table-lspace: 0pt;
                                  mso-table-rspace: 0pt;
                                  border-collapse: collapse;
                                  border-spacing: 0px;
                                "
                                                    >
                                                        <tr style="border-collapse: collapse">
                                                            <td
                                                                    align="center"
                                                                    style="padding: 0; margin: 0"
                                                            >
                                                                <p
                                                                        style="
                                        margin: 0;
                                        -webkit-text-size-adjust: none;
                                        -ms-text-size-adjust: none;
                                        mso-line-height-rule: exactly;
                                        font-size: 18px;
                                        font-family: lato, 'helvetica neue',
                                          helvetica, arial, sans-serif;
                                        line-height: 27px;
                                        color: #000000;
                                      "
                                                                >
                                                                    <br /><strong>Social networks</strong>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="border-collapse: collapse">
                                    <td
                                            align="left"
                                            style="
                          padding: 0;
                          margin: 0;
                          padding-top: 20px;
                          padding-left: 30px;
                          padding-right: 30px;
                        "
                                    >
                                        <table
                                                cellpadding="0"
                                                cellspacing="0"
                                                width="100%"
                                                style="
                            mso-table-lspace: 0pt;
                            mso-table-rspace: 0pt;
                            border-collapse: collapse;
                            border-spacing: 0px;
                          "
                                        >
                                            <tr style="border-collapse: collapse">
                                                <td
                                                        align="center"
                                                        valign="top"
                                                        style="padding: 0; margin: 0; width: 540px"
                                                >
                                                    <table
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            width="100%"
                                                            role="presentation"
                                                            style="
                                  mso-table-lspace: 0pt;
                                  mso-table-rspace: 0pt;
                                  border-collapse: collapse;
                                  border-spacing: 0px;
                                "
                                                    >
                                                        <tr style="border-collapse: collapse">
                                                            <td
                                                                    align="center"
                                                                    style="padding: 0; margin: 0; font-size: 0"
                                                            >
                                                                <table
                                                                        cellpadding="0"
                                                                        cellspacing="0"
                                                                        class="es-table-not-adapt es-social"
                                                                        role="presentation"
                                                                        style="
                                        mso-table-lspace: 0pt;
                                        mso-table-rspace: 0pt;
                                        border-collapse: collapse;
                                        border-spacing: 0px;
                                      "
                                                                >
                                                                    <tr style="border-collapse: collapse">
                                                                        <td
                                                                                align="center"
                                                                                valign="top"
                                                                                style="
                                            padding: 0;
                                            margin: 0;
                                            padding-right: 10px;
                                          "
                                                                        >
                                                                            <img
                                                                                    title="Facebook"
                                                                                    src="https://ofdvxs.stripocdn.email/content/assets/img/social-icons/circle-colored/facebook-circle-colored.png"
                                                                                    alt="Fb"
                                                                                    width="32"
                                                                                    style="
                                              display: block;
                                              border: 0;
                                              outline: none;
                                              text-decoration: none;
                                              -ms-interpolation-mode: bicubic;
                                            "
                                                                            />
                                                                        </td>
                                                                        <td
                                                                                align="center"
                                                                                valign="top"
                                                                                style="
                                            padding: 0;
                                            margin: 0;
                                            padding-right: 10px;
                                          "
                                                                        >
                                                                            <img
                                                                                    title="Twitter"
                                                                                    src="https://ofdvxs.stripocdn.email/content/assets/img/social-icons/circle-colored/twitter-circle-colored.png"
                                                                                    alt="Tw"
                                                                                    width="32"
                                                                                    style="
                                              display: block;
                                              border: 0;
                                              outline: none;
                                              text-decoration: none;
                                              -ms-interpolation-mode: bicubic;
                                            "
                                                                            />
                                                                        </td>
                                                                        <td
                                                                                align="center"
                                                                                valign="top"
                                                                                style="
                                            padding: 0;
                                            margin: 0;
                                            padding-right: 10px;
                                          "
                                                                        >
                                                                            <img
                                                                                    title="Instagram"
                                                                                    src="https://ofdvxs.stripocdn.email/content/assets/img/social-icons/circle-colored/instagram-circle-colored.png"
                                                                                    alt="Inst"
                                                                                    width="32"
                                                                                    style="
                                              display: block;
                                              border: 0;
                                              outline: none;
                                              text-decoration: none;
                                              -ms-interpolation-mode: bicubic;
                                            "
                                                                            />
                                                                        </td>
                                                                        <td
                                                                                align="center"
                                                                                valign="top"
                                                                                style="padding: 0; margin: 0"
                                                                        >
                                                                            <img
                                                                                    title="Youtube"
                                                                                    src="https://ofdvxs.stripocdn.email/content/assets/img/social-icons/circle-colored/youtube-circle-colored.png"
                                                                                    alt="Yt"
                                                                                    width="32"
                                                                                    style="
                                              display: block;
                                              border: 0;
                                              outline: none;
                                              text-decoration: none;
                                              -ms-interpolation-mode: bicubic;
                                            "
                                                                            />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table
                        cellpadding="0"
                        cellspacing="0"
                        class="es-content"
                        align="center"
                        style="
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
                border-collapse: collapse;
                border-spacing: 0px;
                table-layout: fixed !important;
                width: 100%;
              "
                >
                    <tr style="border-collapse: collapse">
                        <td
                                align="center"
                                bgcolor="#cfe2f3"
                                style="padding: 0; margin: 0; background-color: #cfe2f3"
                        >
                            <table
                                    bgcolor="#ffffff"
                                    class="es-content-body"
                                    align="center"
                                    cellpadding="0"
                                    cellspacing="0"
                                    style="
                      mso-table-lspace: 0pt;
                      mso-table-rspace: 0pt;
                      border-collapse: collapse;
                      border-spacing: 0px;
                      background-color: #ffffff;
                      width: 600px;
                    "
                            >
                                <tr style="border-collapse: collapse">
                                    <td
                                            align="left"
                                            style="
                          padding: 0;
                          margin: 0;
                          padding-top: 20px;
                          padding-left: 30px;
                          padding-right: 30px;
                        "
                                    >
                                        <table
                                                cellpadding="0"
                                                cellspacing="0"
                                                width="100%"
                                                style="
                            mso-table-lspace: 0pt;
                            mso-table-rspace: 0pt;
                            border-collapse: collapse;
                            border-spacing: 0px;
                          "
                                        >
                                            <tr style="border-collapse: collapse">
                                                <td
                                                        align="center"
                                                        valign="top"
                                                        style="padding: 0; margin: 0; width: 540px"
                                                >
                                                    <table
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            width="100%"
                                                            role="presentation"
                                                            style="
                                  mso-table-lspace: 0pt;
                                  mso-table-rspace: 0pt;
                                  border-collapse: collapse;
                                  border-spacing: 0px;
                                "
                                                    >
                                                        <tr style="border-collapse: collapse">
                                                            <td
                                                                    align="center"
                                                                    style="padding: 0; margin: 0"
                                                            >
                                                                <p
                                                                        style="
                                        margin: 0;
                                        -webkit-text-size-adjust: none;
                                        -ms-text-size-adjust: none;
                                        mso-line-height-rule: exactly;
                                        font-size: 14px;
                                        font-family: lato, 'helvetica neue',
                                          helvetica, arial, sans-serif;
                                        line-height: 28px;
                                        color: #000000;
                                      "
                                                                >
                                                                    <br />8475 Michaigan Ave. Santa Monica, CA
                                                                    90645, US
                                                                </p>
                                                                <p
                                                                        style="
                                        margin: 0;
                                        -webkit-text-size-adjust: none;
                                        -ms-text-size-adjust: none;
                                        mso-line-height-rule: exactly;
                                        font-size: 14px;
                                        font-family: lato, 'helvetica neue',
                                          helvetica, arial, sans-serif;
                                        line-height: 28px;
                                        color: #000000;
                                      "
                                                                >
                                                                    © 2020 Duonotary - online &amp; in-person
                                                                    notary service
                                                                </p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="border-collapse: collapse">
                                    <td
                                            align="left"
                                            bgcolor="#cfe2f3"
                                            style="
                          padding: 0;
                          margin: 0;
                          padding-top: 20px;
                          padding-left: 30px;
                          padding-right: 30px;
                          background-color: #cfe2f3;
                        "
                                    >
                                        <table
                                                cellpadding="0"
                                                cellspacing="0"
                                                width="100%"
                                                style="
                            mso-table-lspace: 0pt;
                            mso-table-rspace: 0pt;
                            border-collapse: collapse;
                            border-spacing: 0px;
                          "
                                        >
                                            <tr style="border-collapse: collapse">
                                                <td
                                                        align="center"
                                                        valign="top"
                                                        style="padding: 0; margin: 0; width: 540px"
                                                >
                                                    <table
                                                            cellpadding="0"
                                                            cellspacing="0"
                                                            width="100%"
                                                            style="
                                  mso-table-lspace: 0pt;
                                  mso-table-rspace: 0pt;
                                  border-collapse: collapse;
                                  border-spacing: 0px;
                                "
                                                    >
                                                        <tr style="border-collapse: collapse">
                                                            <td
                                                                    align="center"
                                                                    style="padding: 0; margin: 0; display: none"
                                                            ></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
