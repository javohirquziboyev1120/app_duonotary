import React, {Component} from 'react';
import {connect} from "react-redux";
import {
    getMainServiceList,
    getMainServicesWithPercent,
    getPublicHolidays, getServiceList, saveMainService, saveMainServiceWithPercent, savePublicHolidays, saveService
} from "../redux/actions/AppAction";
import {FaRegEdit} from 'react-icons/fa';
import {
    Button,
    CustomInput,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader
} from "reactstrap";
import {AiOutlinePlusCircle} from "react-icons/ai";
import {MdDateRange} from "react-icons/md";
import {Link} from "react-router-dom";

class MainSetup extends Component {
    componentDidMount() {
        this.props.dispatch(getMainServiceList());
        this.props.dispatch(getServiceList())
        this.props.dispatch(getMainServicesWithPercent())
        this.props.dispatch(getPublicHolidays())

    }

    render() {
        const {dispatch, currentUser, currentItem, isAdmin, mainServices, page, size, services, holidays, showStatusModal, mainServicesWithPercent} = this.props;
        const changeStatusModal = () => {
            if (currentItem != null) {
                let currentService = currentItem
                currentService = {...currentService, active: !currentItem.active}
                if (currentItem.subServiceDto) {
                    dispatch(saveService(currentService));
                } else if (!currentItem.subServiceDto && currentItem.mainService && currentItem.percent) {
                    let currentMainServiceWithPercent = {...currentItem};
                    currentMainServiceWithPercent.active = !currentItem.active;
                    currentMainServiceWithPercent.mainService = "/" + currentItem.mainServiceId
                    dispatch(saveMainServiceWithPercent(currentMainServiceWithPercent))
                } else if (currentItem.date && !currentItem.subServiceDto) {
                    let currentHoliday = {...currentItem};
                    currentHoliday.active = !currentItem.active;
                    currentHoliday.mainService = "/" + currentItem.mainServiceId
                    dispatch(savePublicHolidays(currentHoliday))
                } else {
                    dispatch(saveMainService(currentService));
                }
            } else {
                dispatch({
                    type: 'updateState',
                    payload: {
                        showStatusModal: false,
                    }
                })
            }

        }

        const openStatusModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item
                }
            })
        };
        const openChanStatusModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item
                }
            })
        }
        return (
            <div>
                <h1>Main Setup</h1>
                <h6>Work hours</h6>
                <div className="container">
                    {mainServices && mainServices.length > 0 ?
                        <div>
                            <div className="row">
                                {mainServices.map(item =>
                                    <div key={item.id} className="col-md-5 bg-light m-1 border-white py-3">
                                        <div className="row">
                                            <div className="col-md-3">{item.name}</div>
                                            <div className="col-md-9 text-right ">
                                                {item.fromTime} - {item.tillTime}
                                                <Link to="/mainService">
                                                    <button className="btn btn-light border-0" onClick="">
                                                        <FaRegEdit size={40} className="p-2"/>
                                                    </button>
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                )}
                            </div>
                            <div className="row my-2">
                                {mainServices.map(item =>
                                    <div key={item.id} className="col-md-5 bg-light m-1 border-white py-3">
                                        <h3>{item.name}</h3>
                                        <h6>{item.description}</h6>
                                        <CustomInput type="switch" checked={item.active}
                                                     onClick={() => openChanStatusModal(item)}
                                                     id={item.id}/>
                                    </div>
                                )}
                            </div>
                        </div>
                        : ""}

                    {mainServicesWithPercent && mainServicesWithPercent.length > 0 ?
                        <div className="">
                            {mainServicesWithPercent.map(item =>
                                <div className="row m-2 ml-0">
                                    <div className="col-md-4 bg-light p-2">
                                        <CustomInput type="switch" className="d-inline" checked={item.active}
                                                     onClick={() => openChanStatusModal(item)}
                                                     id={item.id}/> {item.mainService.name}

                                    </div>
                                    <div className="col-md-6 p-2 bg-light text-center">
                                        <div className="float-right ">
                                            <div className="float-left">
                                                <InputGroup className="">
                                                    <InputGroupAddon addonType="prepend rounded">
                                                        <InputGroupText
                                                            className="bg-white rounded-left">+{item.percent}%</InputGroupText>
                                                        <InputGroupText className="bg-white rounded-right">
                                                            {item.fromTime} - {item.tillTime}
                                                        </InputGroupText>
                                                    </InputGroupAddon>
                                                </InputGroup>
                                            </div>
                                            <Link to="/mainServiceWithPercent">
                                                <button type="button" className="btn">
                                                    <AiOutlinePlusCircle size="25"/>
                                                </button>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            )}

                        </div> : ""
                    }

                    <h4 className="py-3">Public holidays</h4>
                    {holidays && holidays.length > 0 ?
                        <div className="row">
                            {holidays.map(item => item.active ?
                                <div className="col-5 bg-light m-1  p-2 border-white">
                                    <div className="row">
                                        <div className="col-7">
                                            <h6 className="text-left mb-0">{item.name}</h6>
                                            <small
                                                className="text-left text-secondary">({item.mainService.name})</small>
                                        </div>
                                        <div className="col-5 text-right ">
                                            <Link to="/holiday">
                                                <button className="btn btn-light border-0 d-inline-block"
                                                        onClick="">
                                                    <MdDateRange size={40} className="p-2"/>
                                                </button>
                                            </Link>
                                            <CustomInput className="d-inline-block" type="switch"
                                                         checked={item.active}
                                                         onClick={() => openChanStatusModal(item)}
                                                         id={item.name}/>
                                        </div>
                                    </div>
                                </div>
                                : ""
                            )}


                        </div>
                        : ""}
                    <h4 className="py-3">Services</h4>

                    {services && services.length > 0 ?
                        <div className="row">
                            {services.map(item =>
                                <div className="col-5 bg-light m-1  p-2 border-white">
                                    <div className="row">
                                        <div className="col-5">
                                            <h5 className="text-left">{item.subServiceDto.name}</h5>
                                        </div>
                                        <div className="col-7 text-right ">
                                            <Link to="/service">
                                                <button className="btn btn-light border-0 d-inline-block" onClick="">
                                                    <FaRegEdit size={40} className="p-2"/>
                                                </button>
                                            </Link>
                                            <CustomInput className="d-inline-block" type="switch" checked={item.active}
                                                         onClick={() => openChanStatusModal(item)}
                                                         id={item.subServiceDto.name}/>
                                        </div>
                                    </div>
                                </div>
                            )}

                        </div>
                        : ""}
                </div>


                <Modal isOpen={showStatusModal && currentItem} toggle={() => openChanStatusModal(null)}>
                    <ModalHeader toggle={() => openChanStatusModal(null)}
                                 charCode="x">Change status</ModalHeader>
                    <ModalBody>
                        Are you sure change
                        status {currentItem ? currentItem.subServiceDto ? currentItem.subServiceDto.name : currentItem.percent ? currentItem.mainService.name : currentItem.name : ""}?
                    </ModalBody>
                    <ModalFooter>
                        <Button onClick={() => openChanStatusModal(null)} color="primary">No</Button>
                        <Button type="button" outline onClick={changeStatusModal}
                                color="secondary">Yes</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default connect(({
                            auth: {currentUser, isAdmin},
                            app: {mainServices, holidays, showStatusModal, currentItem, services, mainServicesWithPercent},
                            admin: {page, size}
                        }) => ({
        currentUser,
        isAdmin,
        mainServices,
        page,
        size,
        services,
        holidays,
        showStatusModal,
        currentItem,
        mainServicesWithPercent
    })
)(MainSetup)
