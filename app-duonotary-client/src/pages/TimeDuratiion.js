import React, {Component} from 'react';
import {getTimeDuration, saveTimeDuration} from "../redux/actions/AppAction";
import {connect} from "react-redux";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

class TimeDuratiion extends Component {
    componentDidMount() {
        this.props.dispatch(getTimeDuration())
    }

    render() {
        const {dispatch, showModal, timeDuration, currentItem} = this.props;

        const saveItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null
            this.props.dispatch(saveTimeDuration(v))
        }

        const openModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showModal: !showModal,
                    currentItem: item,
                }
            })
        };

        return (
            <div>
                {!timeDuration.length ? <div>
                    <Button color="success" outline className="my-2" onClick={() => openModal('')}>+ Add</Button>
                </div> : ''}

                <br/>

                <h1 className="text-center">Time Duration</h1>

                <br/>

                {timeDuration.length ?
                    <div className="d-flex">
                        <h3 className="mr-lg-3">
                            {timeDuration[0].durationTime} min
                        </h3>
                        <Button color="warning" outline onClick={() => openModal(timeDuration[0])}>Edit</Button>
                    </div>
                    :
                    <h3>Time Duration empty</h3>
                }

                <Modal isOpen={showModal} toggle={openModal}>
                    <AvForm onValidSubmit={saveItem}>
                        <ModalHeader toggle={openModal}
                                     charCode="x">{currentItem ? "Edit time duration" : "Add time duration"}</ModalHeader>
                        <ModalBody>
                            <AvField name="durationTime" label="Duration time EX: 30"
                                     required
                                     defaultValue={currentItem != null ? currentItem.durationTime : ""}
                                     placeholder="Enter duration time"/>
                        </ModalBody>
                        <ModalFooter>
                            <Button type="button" color="" onClick={openModal}>Cancel</Button>{' '}
                            <Button color="success">Save</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>

            </div>
        );
    }
}

export default connect(
    ({
         app: {
             timeDuration, showModal, currentItem, active
         }
     }) => ({
        timeDuration,
        showModal,
        currentItem,
        active
    }))(TimeDuratiion);