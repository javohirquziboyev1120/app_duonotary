import React, {Component} from 'react';
import {connect} from "react-redux";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {Button} from "reactstrap";
import {confirmCodeAction} from "../redux/actions/AuthActions";

class ConfirmCode extends Component {
    render() {
        const { dispatch,history} = this.props;
        const confirmCode=(e,v)=>{
          v.username=window.location.pathname.substring(13);
          v.code=v.code.trim();
          dispatch(confirmCodeAction({v,history}))
        }
        return (
            <div>
                <h1>Confirm</h1>
                <AvForm onValidSubmit={confirmCode}>
                    <AvField type="number"  name="code" label="Enter Verification coder"
                             placeholder="code" required/>
                    <Button>Submit</Button>
                </AvForm>
            </div>
        );
    }
}

ConfirmCode.propTypes = {};

export default connect(({app:{}}) => ({}))(ConfirmCode);