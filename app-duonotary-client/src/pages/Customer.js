import React, {Component} from 'react';
import {
    Button, Col,
    Modal, ModalBody, ModalFooter,
    ModalHeader,
    Row,
    Table
} from "reactstrap";
import {saveDiscountAction} from "../redux/actions/AppAction";
import {getOrderList} from "../redux/actions/OrderAction";
import {connect} from "react-redux";
import {AvField, AvForm} from 'availity-reactstrap-validation';

class Customer extends Component {
    componentDidMount() {
        this.props.dispatch(getOrderList())
    }

    render() {
        const {ordersList, showModal, dispatch, currentItem} = this.props;
        const openModal = (boolean, item) => {
            dispatch({type: "updateState", payload: {showModal: boolean, currentItem: item}})
        }
        const saveDiscount = (e, v) => {
            v.orderDto = currentItem;
            dispatch(saveDiscountAction(v))
        }
        return (
            <div>
                <h2 className="text-center">Customers</h2>
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Client</th>
                        <th>Order number</th>
                        <th>Address</th>
                        <th>Date/time of order</th>
                        <th>Amount</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    {ordersList != null ?
                        <tbody>
                        {ordersList.map((item, i) =>
                            <tr key={item.id}>
                                <td>{i + 1}</td>
                                <td>{item.client.firstName} {item.client.lastName}</td>
                                <td>{item.checkNumber}</td>
                                <td>{item.address}, {item.zipCode.city}, {item.zipCode.code}</td>
                                <td>{item.created_at.substring(0, 10)} / {item.created_at.substring(11, 16)}</td>
                                <td>${item.amount}</td>
                                <th><Button onClick={() => openModal(true, item)}>Add Discount</Button></th>
                            </tr>
                        )}
                        </tbody>
                        :
                        <tbody>
                        <tr>
                            <td colSpan="4">
                                <h3 className="text-center mx-auto"> No information </h3>
                            </td>
                        </tr>
                        </tbody>
                    }
                </Table>
                {currentItem != null ? <Modal isOpen={showModal} toggle={() => openModal(false, null)}>
                    <AvForm onValidSubmit={saveDiscount}>
                        <ModalHeader>Discount</ModalHeader>
                        <ModalBody>
                            <Row>
                                <Col md={12}> <AvField type="number" name="percent" placeholder="enter percent"
                                                       required/></Col>
                                <Col md={12}> <AvField type="number" name="amount" placeholder="enter amount" required/></Col>
                                <Col md={12}> <AvField name="description" placeholder="enter description"
                                                       required/></Col>
                            </Row>
                        </ModalBody>
                        <ModalFooter>
                            <Button type="button" onClick={() => openModal(false, null)}>Cancel</Button>
                            <Button color="info">Submit</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal> : ''}
            </div>
        );
    }
}

export default connect(({order: {ordersList}, app: {showModal, currentItem}}) => ({
    ordersList,
    showModal,
    currentItem
}))(Customer);
