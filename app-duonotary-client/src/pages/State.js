import React, {Component} from 'react';
import {
    Button,
    CustomInput,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table
} from "reactstrap";
import {connect} from "react-redux";
import {deleteState, getStateList, saveState} from "../redux/actions/AppAction";
import {AvField, AvForm} from "availity-reactstrap-validation";
import DeleteModal from "../components/Modal/DeleteModal";
import StatusModal from "../components/Modal/StatusModal";

class State extends Component {
    componentDidMount() {
        this.props.dispatch(getStateList());
    }

    render() {
        const {dispatch, showModal, states, currentItem, active, showDeleteModal, showStatusModal} = this.props;

        const openModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showModal: !showModal,
                    currentItem: item,
                    active: item.active
                }
            })
        };
        const openDeleteModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentItem: item
                }
            })
        };

        const changeActive = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    active: !active
                }
            })
        };
        const deleteFunction = () => {
            this.props.dispatch(deleteState(currentItem))
        };

        const saveItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null
            v.active = active
            this.props.dispatch(saveState(v))
        }

        const openStatusModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item
                }
            })
        };
        const changeStatusState = () => {
            let currentState = {...currentItem};
            currentState.active = !currentItem.active;
            this.props.dispatch(saveState(currentState))
        }

        return (
            <div>
                <h2 className="text-center">State list</h2>
                <Button color="success" outline className="my-2" onClick={() => openModal('')}>+ New add</Button>
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th colSpan="2" className="">Operation</th>
                    </tr>
                    </thead>
                    {states.length > 0 ?
                        <tbody>
                        {states.map((item, i) =>
                            <tr key={item.id}>
                                <td>{i + 1}</td>
                                <td>{item.name}</td>
                                <td>
                                    <FormGroup check>
                                        <Label check>
                                            <Input type="checkbox" onClick={() => openStatusModal(item)} id="active"
                                                   checked={item.active}/>
                                            {item.active ? "Active" : "Inactive"}
                                        </Label>
                                    </FormGroup>
                                </td>
                                <td><Button color="warning" outline onClick={() => openModal(item)}>Edit</Button></td>
                                <td><Button color="danger" outline onClick={() => openDeleteModal(item)}>Delete</Button>
                                </td>

                            </tr>
                        )}
                        </tbody>
                        :
                        <tbody>
                        <tr>
                            <td colSpan="4">
                                <h3 className="text-center mx-auto"> No information </h3>
                            </td>
                        </tr>
                        </tbody>
                    }
                </Table>
                <Modal isOpen={showModal} toggle={openModal}>
                    <AvForm onValidSubmit={saveItem}>
                        <ModalHeader toggle={openModal}
                                     charCode="x">{currentItem != null ? "Edit state" : "Add state"}</ModalHeader>
                        <ModalBody>
                            <AvField name="name" label="Name" required
                                     defaultValue={currentItem != null ? currentItem.name : ""}
                                     placeholder="Enter state name"/>
                            <CustomInput type="checkbox" checked={active}
                                         onChange={changeActive}
                                         label="Is active?" id="stateActive"/>
                        </ModalBody>
                        <ModalFooter>
                            <Button type="button" color="secondary" outline onClick={openModal}>Cancel</Button>{' '}
                            <Button color="success">Save</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>

                {showDeleteModal && <DeleteModal text={currentItem != null ? currentItem.name : ''}
                                                 showDeleteModal={showDeleteModal}
                                                 confirm={deleteFunction}
                                                 cancel={openDeleteModal}/>}

                {showStatusModal && <StatusModal text={currentItem != null ? currentItem.name : ''}
                                                 showStatusModal={showStatusModal}
                                                 confirm={changeStatusState}
                                                 cancel={openStatusModal}/>}
            </div>
        );
    }
}

    export default connect(({
                                app: {
                                    states, showModal, currentItem, active, showDeleteModal, showStatusModal
                                }
                            }) => ({
        states, showModal, currentItem, active, showDeleteModal, showStatusModal
    }))(State);