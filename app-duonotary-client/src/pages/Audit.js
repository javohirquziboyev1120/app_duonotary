import React, {Component} from 'react';
import {
    Button,
    Dropdown,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Modal, ModalBody, ModalFooter, ModalHeader,
    Table
} from "reactstrap";
import {
    getAudit,
    getAuditForItemWithCommitId,
    getAuditTables,
    getHistory,
} from "../redux/actions/AppAction";
import * as types from "../redux/actionTypes/AppActionTypes";
import {connect} from "react-redux";
import {isObject} from "reactstrap/es/utils";
import Moment from "moment";

class Audit extends Component {
    componentDidMount() {
        this.props.dispatch(getAudit())
    }

    render() {
        const {
            dispatch, histories, historyTables, auditTables, text, showDropdown, showModal, currentItem, page, size, totalElements, totalPages, currentAuditItems
        } = this.props;
        const selectedTable = (item) => {
            dispatch(
                getAuditTables({tableName: item}),

                {
                    type: types.SET_TO_TEXT,
                    payload: item
                }
            )
        }

        const pagination = (item) => {
            dispatch(
                getHistory({tableName: item})
            )
        }

        const openDropdown = () => {
            dispatch({
                type: types.CHANGE_SHOW_DROPDOWN,
                payload: {
                    showDropdown: !showDropdown
                }
            })
        }

        const openModal = (item) => {
            if (showModal) {
                dispatch({
                    type: 'updateState',
                    payload: {
                        currentItem: null,
                        currentAuditItems: null,
                        showModal: false
                    }
                })
            } else {
                let data = {}
                data.tableName = item.globalId.entity.substring(item.globalId.entity.lastIndexOf('.') + 1);
                data.tableItemId = item.globalId.cdoId
                data.commitId = item.commitMetadata.id
                dispatch(getAuditForItemWithCommitId({...data}));
                dispatch({
                    type: 'updateState',
                    payload: {
                        currentItem: item
                    }
                })

            }
        }

        return (
            <div>
                <h1 className="text-center">History page</h1>
                <Dropdown isOpen={showDropdown} toggle={openDropdown}>
                    <DropdownToggle caret>
                        Please select table name
                    </DropdownToggle>
                    <DropdownMenu modifiers={{
                        setMaxHeight: {
                            enabled: true,
                            order: 890,
                            fn: (data) => {
                                return {
                                    ...data,
                                    styles: {
                                        ...data.styles,
                                        overflow: 'auto',
                                        maxHeight: '150px',
                                    },
                                };
                            },
                        },
                    }}
                    >
                        <DropdownItem onClick={() => dispatch(getHistory({}))}>All</DropdownItem>
                        {historyTables.map(item => <DropdownItem key={item}
                                                                 onClick={() => selectedTable(item.split('_').join(''))}>{item.split('_').join(' ')}</DropdownItem>)}
                    </DropdownMenu>
                </Dropdown>
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Table Name</th>
                        <th>Operation</th>
                        <th>Version</th>
                        <th>Time</th>
                        <th>What did?</th>
                    </tr>
                    </thead>
                    {histories && histories.length > 0 ?
                        <tbody>
                        {histories.map((item, i) =>
                            <tr key={item.id}>
                                <td>{i + 1}</td>
                                <td>{item.globalId ? item.globalId.entity.substring(item.globalId.entity.lastIndexOf('.') + 1) : ""}</td>
                                <td>{item.type}</td>
                                <td>{item.version}</td>
                                <td>{item.commitMetadata.commitDate.substring(0, 19).replace('T', " ")}</td>
                                <td><Button onClick={() => openModal(item)}>Show</Button></td>
                            </tr>
                        )}
                        </tbody> :
                        <tbody>
                        <tr>
                            <td colSpan="4">
                                <h3 className="text-center mx-auto"> No information </h3>
                            </td>
                        </tr>
                        </tbody>
                    }

                </Table>

                <Modal size="lg" isOpen={showModal}>
                    <ModalHeader>What action</ModalHeader>
                    <ModalBody>
                        {currentAuditItems && currentItem && currentAuditItems.length > 0 ?
                            <div>
                                <p><b>Changes this table</b>
                                    - {currentItem.globalId ? currentItem.globalId.entity.substring(currentItem.globalId.entity.lastIndexOf('.') + 1) : ""}
                                </p>
                                <p><b>Change type</b> - {currentItem.type}</p>
                                <p><b>Version -</b> {currentItem.version}</p>
                                <p><b>Time</b> -
                                    {Moment(currentItem.commitMetadata.commitDate).format('hh:MM a, D MMMM, YYYY')}
                                </p>
                                <p><b>Author</b> - {currentItem.commitMetadata.author}</p>
                                <strong>Changes: </strong>
                                {currentAuditItems.map((item, i) =>
                                    <div>
                                        - <strong>'{item.property}'</strong> value changed
                                        from <b>{"" + item.left}</b> to <b>{"" + item.right}</b>
                                    </div>
                                )}
                            </div>
                            : ''}
                        <h5>Current Item status</h5>
                        <Table>
                            <tbody>
                            {currentItem ? Object.entries(currentItem).map(([key, value]) =>
                                <tr key={key}>
                                    <td>{key}:</td>
                                    <td className="text-right">
                                        <Table>
                                            <tbody>
                                            {isObject(value) ? Object.entries(value).map(([key, value]) =>
                                                value ?
                                                    <tr key={key}>
                                                        <td className="text-left">{"" + key}:</td>
                                                        <td className="text-left">{isObject(value) ? "" : "" + value}</td>
                                                    </tr> : ""
                                            ) : value}
                                            </tbody>
                                        </Table>
                                    </td>

                                </tr>
                            ) : ''}
                            </tbody>
                        </Table>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={() => openModal(null)}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default connect(
    ({
         app: {
             historyTables, auditTables, histories, text, showModal, currentItem, active, showDropdown, dispatch, currentAuditItems
         }
     }) => ({
        historyTables,
        auditTables,
        histories,
        text,
        showModal,
        currentItem,
        active,
        showDropdown,
        dispatch,
        currentAuditItems
    }))(Audit);