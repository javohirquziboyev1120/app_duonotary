import React, {Component} from 'react';
import {AvField, AvForm} from "availity-reactstrap-validation";
import DeleteModal from "../components/Modal/DeleteModal";
import StatusModal from "../components/Modal/StatusModal";
import {
    Button,
    CustomInput,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table
} from "reactstrap";
import {connect} from "react-redux";
import {
    deleteService,
    getMainServiceList,
    getServiceList,
    getSubServiceList,
    saveService
} from "../redux/actions/AppAction";
import DynamicModal from "../components/Modal/DynamicModal";
import * as type from "../redux/actionTypes/AppActionTypes";


class Service extends Component {
    componentDidMount() {
        this.props.dispatch(getServiceList());

    }

    render() {
        const {dispatch, showModal, services, currentItem, active, dynamic, showDeleteModal, showStatusModal, showDynamicModal, mainServices, subServices} = this.props;

        const openModal = (item) => {
            this.props.dispatch(getMainServiceList());
            this.props.dispatch(getSubServiceList());
            dispatch({
                type: 'updateState',
                payload: {
                    showModal: !showModal,
                    currentItem: item,
                    active: item.active,
                    dynamic: item.dynamic
                }
            })
        };

        const openDeleteModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentItem: item
                }
            })
        };
        const changeActive = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    active: !active
                }
            })
        };
        const changeDynamic = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    dynamic: !dynamic
                }
            })
        };

        const deleteFunction = () => {
            this.props.dispatch(deleteService(currentItem))
        };
        const saveItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null
            v.active = active
            v.dynamic = dynamic
            this.props.dispatch(saveService(v))
        }

        const changeStatusService = () => {
            let currentService = {...currentItem};
            currentService.active = !currentItem.active;
            // currentService.mainServiceDto = "/" + currentItem.mainServiceId;
            // currentService.subServiceDto = "/" + currentItem.subServiceId;
            this.props.dispatch(saveService(currentService))
        }

        const changeDynamicService = () => {
            let currentService = {...currentItem};
            currentService.dynamic = !currentItem.dynamic;
            // currentService.mainServiceDto = "/" + currentItem.mainServiceId;
            // currentService.subServiceDto = "/" + currentItem.subServiceId;
            this.props.dispatch(saveService(currentService))
        }

        const openStatusModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item
                }
            })
        };

        const openDynamicModal = (item) => {
            dispatch({
                type: type.CHANGE_DYNAMIC,
                payload: {
                    showDynamicModal: !showDynamicModal,
                    currentItem: item
                }
            })
        };

        return (
            <div>
                <h2 className="text-center">Service</h2>
                <Button color="success" outline className="my-2" onClick={() => openModal('')}>+ New add</Button>
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Main Service</th>
                        <th>Sub Service</th>
                        <th>Initial Count</th>
                        <th>Initial Spending time</th>
                        <th>Dynamic</th>
                        <th>Every Count</th>
                        <th>Every Spending Time</th>
                        <th>Charge Minute</th>
                        <th>Charge Percent</th>
                        <th>Status</th>
                        <th colSpan="2" className="">Operation</th>
                    </tr>
                    </thead>
                    {services.length > 0 ?
                        <tbody>
                        {services.map((item, i) =>
                            <tr key={item.id}>
                                <td>{i + 1}</td>
                                <td>{item.mainServiceDto.name}</td>
                                <td>{item.subServiceDto.name}</td>
                                <td>{item.initialCount}</td>
                                <td>{item.initialSpendingTime}</td>

                                <td>
                                    <FormGroup check>
                                        <Label check for="dynamic">
                                            <Input type="checkbox" onClick={() => openDynamicModal(item)} id={item.id}
                                                   checked={item.dynamic}/>
                                            {item.dynamic ? "Dynamic" : "Undynamic"}
                                        </Label>
                                    </FormGroup>
                                </td>
                                <td>{item.dynamic ? item.everyCount : ''}</td>
                                <td>{item.dynamic ? item.everySpendingTime : ''}</td>
                                <td>{item.chargeMinute}</td>
                                <td>{item.chargePercent}</td>
                                <td>
                                    <FormGroup check>
                                        <Label check for="active">
                                            <Input type="checkbox" onClick={() => openStatusModal(item)}
                                                   id={item.id + i}
                                                   checked={item.active}/>
                                            {item.active ? "Active" : "Inactive"}
                                        </Label>
                                    </FormGroup>
                                </td>

                                <td><Button color="warning" outline onClick={() => openModal(item)}>Edit</Button></td>
                                <td><Button color="danger" outline onClick={() => openDeleteModal(item)}>Delete</Button>
                                </td>

                            </tr>
                        )}
                        </tbody>
                        :
                        <tbody>
                        <tr>
                            <td colSpan="4">
                                <h3 className="text-center mx-auto"> No information </h3>
                            </td>
                        </tr>
                        </tbody>
                    }
                </Table>

                <Modal isOpen={showModal} toggle={openModal}>
                    <AvForm onValidSubmit={saveItem}>
                        <ModalHeader toggle={openModal}
                                     charCode="x">{currentItem != null ? "Edit Service" : "Add Service"}</ModalHeader>
                        <ModalBody>
                            {mainServices.length > 0 ?
                                <AvField type="select" name="mainServiceId"
                                         value={currentItem != null ? (currentItem.mainServiceId) : "0"} required>
                                    <option value="0" disabled>Select Main Service</option>
                                    {mainServices.map(item =>
                                        <option key={item.id} value={item.id}>{item.name}</option>
                                    )}
                                </AvField> : 'Main Service not found'
                            }

                            {subServices.length > 0 ?
                                <AvField type="select" name="subServiceId"
                                         value={currentItem != null ? (currentItem.subServiceId) : "0"} required>
                                    <option value="0" disabled>Select Sub Service</option>
                                    {subServices.map(item =>
                                        <option key={item.id} value={item.id}>{item.name}</option>
                                    )}
                                </AvField> : 'Sub Service not found'
                            }
                            <AvField name="initialCount" label="InitialCount" required
                                     defaultValue={currentItem != null ? currentItem.initialCount : ""}
                                     placeholder="Enter InitialCount"/>

                            <AvField name="initialSpendingTime" label="Initial Spending Time" required
                                     defaultValue={currentItem != null ? currentItem.initialCount : ""}
                                     placeholder="Enter Initial Spending Time"/>

                            <CustomInput type="checkbox" checked={dynamic}
                                         onChange={changeDynamic}
                                         label={dynamic ? 'Dynamic' : 'Undynamic'} id="ServiceDynamic"/>
                            <br/>
                            {dynamic ?
                                <AvField name="everyCount" label="Every Count" required
                                         defaultValue={currentItem != null ? currentItem.everyCount : ""}
                                         placeholder="Enter Every Count"/>
                                : ''
                            }
                            {dynamic ?
                                <AvField name="everySpendingTime" label="Every Spending Time" required
                                         defaultValue={currentItem != null ? currentItem.everySpendingTime : ""}
                                         placeholder="Enter Every Spending Time"/>
                                : ''
                            }
                            <AvField name="chargeMinute" label="Charge Minute" required
                                     defaultValue={currentItem != null ? currentItem.chargeMinute : ""}
                                     placeholder="Enter Charge Minute"/>
                            <AvField name="chargePercent" label="Charge Percent" required
                                     defaultValue={currentItem != null ? currentItem.chargePercent : ""}
                                     placeholder="Enter Charge Percent"/>
                            <CustomInput type="checkbox" checked={active}
                                         onChange={changeActive}
                                         label={active ? 'Active' : 'Inactive'} id="ServiceActive"/>
                        </ModalBody>
                        <ModalFooter>
                            <Button type="button" color="secondary" outline onClick={openModal}>Cancel</Button>{' '}
                            <Button color="success">Save</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>

                {showDeleteModal && <DeleteModal text={currentItem != null ? currentItem.name : ''}
                                                 showDeleteModal={showDeleteModal}
                                                 confirm={deleteFunction}
                                                 cancel={openDeleteModal}/>}

                {showStatusModal && <StatusModal text={currentItem != null ? currentItem.name : ''}
                                                 showStatusModal={showStatusModal}
                                                 confirm={changeStatusService}
                                                 cancel={openStatusModal}/>}

                {showDynamicModal && <DynamicModal text={currentItem != null ? currentItem.name : ''}
                                                   showDynamicModal={showDynamicModal}
                                                   confirm={changeDynamicService}
                                                   cancel={openDynamicModal}/>}
            </div>
        );
    }
}

export default connect(({
                            app: {
                                services, showModal, currentItem, active,
                                showDeleteModal, showStatusModal, dynamic, mainServices, subServices, showDynamicModal
                            }
                        }) => ({
    services,
    showModal,
    currentItem,
    active,
    showDeleteModal,
    showStatusModal,
    dynamic,
    mainServices,
    subServices,
    showDynamicModal
}))(Service);