import React, {Component} from 'react';
import {
    Button,
    CustomInput,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table
} from "reactstrap";
import {connect} from "react-redux";
import {deleteCounty, getCountiesList, getStateList, saveCounty} from "../redux/actions/AppAction";
import {AvField, AvForm} from "availity-reactstrap-validation";
import DeleteModal from "../components/Modal/DeleteModal";
import StatusModal from "../components/Modal/StatusModal";

class County extends Component {
    componentDidMount() {
        this.props.dispatch(getCountiesList());
        this.props.dispatch(getStateList());
    }

    render() {
        const {dispatch, showModal, states, counties, currentItem, active, showDeleteModal, showStatusModal} = this.props;

        const openModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showModal: !showModal,
                    currentItem: item,
                    active: item.active
                }
            })
        };
        const openDeleteModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentItem: item
                }
            })
        };
        const changeActive = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    active: !active
                }
            })
        };

        const deleteFunction = () => {
            this.props.dispatch(deleteCounty(currentItem))
        };
        const saveItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null
            v.active = active
            this.props.dispatch(saveCounty(v))
        }
        const changeStatusCounty = () => {
            let currentCounty = {...currentItem};
            currentCounty.active = !currentItem.active;
            currentCounty.state = "/" + currentItem.stateId;
            this.props.dispatch(saveCounty(currentCounty))
        }

        const openStatusModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item
                }
            })
        };


        return (
            <div>
                <h2 className="text-center">County list</h2>
                <Button color="success" outline className="my-2" onClick={() => openModal('')}>+ New add</Button>
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>State</th>
                        <th>Status</th>
                        <th colSpan="2" className="">Operation</th>
                    </tr>
                    </thead>
                    {counties && counties.length > 0 ?
                        <tbody>
                        {counties.map((item, i) =>
                            <tr key={item.id}>
                                <td>{i + 1}</td>
                                <td>{item.name}</td>
                                <td>{item.state.name}</td>
                                <td>
                                    <FormGroup check>
                                        <Label check for="active">
                                            <Input type="checkbox" onClick={() => openStatusModal(item)} id={item.name}
                                                   checked={item.active}/>
                                            {item.active ? "Active" : "Inactive"}
                                        </Label>
                                    </FormGroup>
                                </td>
                                <td><Button color="warning" outline onClick={() => openModal(item)}>Edit</Button></td>
                                <td><Button color="danger" outline onClick={() => openDeleteModal(item)}>Delete</Button>
                                </td>

                            </tr>
                        )}
                        </tbody>
                        :
                        <tbody>
                        <tr>
                            <td colSpan="4">
                                <h3 className="text-center mx-auto"> No information </h3>
                            </td>
                        </tr>
                        </tbody>
                    }
                </Table>
                <Modal isOpen={showModal} toggle={openModal}>
                    <AvForm onValidSubmit={saveItem}>
                        <ModalHeader toggle={openModal}
                                     charCode="x">{currentItem != null ? "Edit state" : "Add state"}</ModalHeader>
                        <ModalBody>
                            <AvField name="name" label="Name" required
                                     defaultValue={currentItem != null ? currentItem.name : ""}
                                     placeholder="Enter state name"/>
                            {states && states.length > 0 ?
                                <AvField type="select" name="state"
                                         value={currentItem != null ? ("/" + currentItem.stateId) : "0"} required>
                                    <option value="0" disabled>Select state</option>
                                    {states.map(item =>
                                        <option key={item.id} value={"/" + item.id}>{item.name}</option>
                                    )}
                                </AvField> :
                                'States not found'}

                            <CustomInput type="checkbox" checked={active}
                                         onChange={changeActive}
                                         label={active ? 'Active' : 'Inactive'} id="countyActive"/>
                        </ModalBody>
                        <ModalFooter>
                            <Button type="button" color="secondary" outline onClick={openModal}>Cancel</Button>{' '}
                            <Button color="success">Save</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>

                {showDeleteModal && <DeleteModal text={currentItem != null ? currentItem.name : ''}
                                                 showDeleteModal={showDeleteModal}
                                                 confirm={deleteFunction}
                                                 cancel={openDeleteModal}/>}

                {showStatusModal && <StatusModal text={currentItem != null ? currentItem.name : ''}
                                                 showStatusModal={showStatusModal}
                                                 confirm={changeStatusCounty}
                                                 cancel={openStatusModal}/>}


            </div>
        );
    }
}

export default connect(({
                            app: {
                                counties, showModal, currentItem, active,
                                showDeleteModal, showStatusModal, states
                            }
                        }) => ({
    counties, showModal, currentItem, active, showDeleteModal, showStatusModal, states
}))(County);