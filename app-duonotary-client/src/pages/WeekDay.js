import React, {Component} from 'react';
import {AvField, AvForm} from 'availity-reactstrap-validation';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader, Table} from 'reactstrap'
import {connect} from "react-redux";
import {getWeekDayList, saveWeekDay} from "../redux/actions/AppAction";

class WeekDay extends Component {
    componentDidMount() {
        this.props.dispatch(getWeekDayList())
    }

    render() {
        const {dispatch, showModal, weekDays, currentItem, active} = this.props;
        const saveItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null
            v.active = active
            this.props.dispatch(saveWeekDay(v))
        }

        const openModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showModal: !showModal,
                    currentItem: item
                }
            })
        };

        return (
            <div>
                <h2 className="text-center">Week day list</h2>
                {weekDays !== [] && weekDays.length < 7 ?
                    <Button color="success" outline className="my-2" onClick={() => openModal('')}>+ Add</Button>
                    : ''}
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>InPerson number</th>
                        <th colSpan="2">Operation</th>
                    </tr>
                    </thead>
                    {weekDays != null ?
                        <tbody>
                        {weekDays.map((item, i) =>
                            <tr key={item.id}>
                                <td>{i + 1}</td>
                                <td>{item.day}</td>
                                <td>{item.orderNumber}</td>
                                <td><Button color="warning" outline onClick={() => openModal(item)}>Edit</Button></td>
                            </tr>
                        )}
                        </tbody> :
                        <tbody>
                        <tr>
                            <td colSpan="4">
                                <h3 className="text-center mx-auto"> No information </h3>
                            </td>
                        </tr>
                        </tbody>
                    }
                </Table>

                <Modal isOpen={showModal} toggle={openModal}>
                    <AvForm onValidSubmit={saveItem}>
                        <ModalHeader toggle={openModal}
                                     charCode="x">{currentItem ? "Edit country" : "Add country"}</ModalHeader>
                        <ModalBody>

                            <AvField name="day" label="Day" required
                                     defaultValue={currentItem != null ? currentItem.day : ""}
                                     placeholder="Enter week day name"/>
                            <AvField name="orderNumber" label="InPerson number" required
                                     defaultValue={currentItem != null ? currentItem.orderNumber : ""}
                                     placeholder="Enter week day order number"/>

                        </ModalBody>
                        <ModalFooter>
                            <Button type="button" color="" onClick={openModal}>Cancel</Button>{' '}
                            <Button color="success">Save</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>

            </div>
        );
    }
}

export default connect(
    ({
         app: {
             weekDays, showModal, currentItem, active, showDeleteModal, showStatusModal
         }
     }) => ({
        weekDays,
        showModal,
        currentItem,
        active, showDeleteModal, showStatusModal
    }))(WeekDay);
