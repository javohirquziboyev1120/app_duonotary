import React, {Component} from 'react';
import {connect} from "react-redux";
import {getAllByUserId} from "../redux/actions/OrderAction";
import {AvField, AvForm} from "availity-reactstrap-validation"
import {
    Button,
    Col,
    Collapse,
    Modal, ModalBody, ModalFooter, ModalHeader,
    Nav,
    Navbar,
    NavbarToggler,
    NavItem,
    Row,
    Table
} from "reactstrap";
import {addFeedBackAction} from "../redux/actions/AppAction";

class DashBoard extends Component {
    componentDidMount() {
        if (!this.props.currentUser) {
            this.props.dispatch(getAllByUserId(window.location.pathname.substring(11)))
        } else {
            this.props.dispatch(getAllByUserId(this.props.currentUser.id))
        }

    }

    render() {
        const {dispatch, isAgent, isUser, currentUser, orders, showModal, currentItem, showFeedBackModal} = this.props;
        const openModal = (boolean, item) => {
            dispatch({type: "updateState", payload: {showModal: boolean, currentItem: item}})
        }
        const openFeedBackModal = (boolean) => {
            dispatch({type: "updateState", payload: {showModal: false, showFeedBackModal: boolean}})
        }
        const addFeedBack = (e, v) => {
            v.orderDto = currentItem;
            v.agent = isAgent;
            v.operationEnum = "INSERT";
            v.seen = false;
            dispatch(addFeedBackAction(v))
        }
        return (
            <div>
                <Navbar color="light" light expand="md">
                    <NavbarToggler onClick={() => console.log()}/>
                    <Collapse isOpen={true} navbar>
                        <Nav className="mr-auto" navbar>
                            <NavItem style={{cursor: 'pointer'}} className="px-2">
                                <p className="mt-2" onClick={() => this.props.history.push('/feedbacks')}>Feed Backs</p>
                            </NavItem>
                            {isUser ?
                                <NavItem style={{cursor: 'pointer'}}
                                         onClick={() => this.props.history.push('/discount')} className="px-2">
                                    <p className="mt-2">Discounts</p>
                                </NavItem> : ''}
                            <NavItem style={{cursor: 'pointer'}} className="px-2">
                                <p className="mt-2">Main page</p>
                            </NavItem>

                        </Nav>
                    </Collapse>
                </Navbar>
                {isAgent ?
                    <Table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Order number</th>
                            <th>Client Full name</th>
                            <th>Service name</th>
                            <th>Service Description</th>
                            <th>Amount</th>
                            <th>PayType</th>
                            <th>Status</th>
                            <th>Location</th>
                        </tr>
                        </thead>
                        <tbody>
                        {orders ? orders.map((order, i) =>
                                <tr key={order.id} style={{cursor: 'pointer'}} onClick={() => openModal(true, order)}>
                                    <th>{i + 1}</th>
                                    <th>{order.checkNumber}</th>
                                    <th>{order.client.firstName + "  " + order.client.lastName}</th>
                                    <th>{order.servicePrice.subServiceName}</th>
                                    <th>{order.servicePrice.subServiceDescription}</th>
                                    <th>{'$' + order.amount}</th>
                                    <th>{order.payType.name}</th>
                                    <th className={order.status === "CANCELLED" ? "bg-danger" : "bg-success"}>{order.status}</th>
                                    <th>{order.address + ", " + order.zipCode.code}</th>

                                </tr>
                            )
                            : 'No   Orders'}
                        </tbody>
                    </Table> :
                    isUser ? <Table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Order Number</th>
                            <th>Agent Full name</th>
                            <th>Service name</th>
                            <th>Service Description</th>
                            <th>Amount</th>
                            <th>PayType</th>
                            <th>Status</th>
                            <th>Location</th>
                        </tr>
                        </thead>
                        <tbody>
                        {orders ? orders.map((order, i) =>
                                <tr key={order.id} style={{cursor: 'pointer'}} onClick={() => openModal(true, order)}>
                                    <th>{i + 1}</th>
                                    <th>{order.checkNumber}</th>
                                    <th>{order.agent.firstName + " " + order.agent.lastName}</th>
                                    <th>{order.servicePrice.subServiceName}</th>
                                    <th>{order.servicePrice.subServiceDescription}</th>
                                    <th>{"$" + order.amount}</th>
                                    <th>{order.payType.name}</th>
                                    <th className={order.status === "CANCELLED" ? "bg-danger" : "bg-success"}>{order.status}</th>
                                    <th>{order.address + ", " + order.zipCode.code}</th>

                                </tr>
                            )
                            : 'No   Orders'}
                        </tbody>
                    </Table> : "no Information"}
                {currentItem != null && showModal ? <Modal isOpen={showModal} toggle={() => openModal(false, null)}>
                    <ModalHeader>Order {currentItem.checkNumber}</ModalHeader>
                    <ModalBody>

                        {isUser ?
                            <Row className="p-3">
                                <Col md={5}>
                                    <Row><strong>Agent name</strong></Row>
                                    <Row>
                                        {currentItem.agent.firstName + "  " + currentItem.agent.lastName}
                                    </Row>
                                </Col>
                                <Col md={5} className="offset-2">
                                    <Row><strong>Service name</strong></Row>
                                    <Row>
                                        {currentItem.servicePrice.subServiceName}
                                    </Row>
                                </Col>
                                <Col md={5}>
                                    <Row><strong>Service Description</strong></Row>
                                    <Row>
                                        {currentItem.servicePrice.subServiceDescription}
                                    </Row>
                                </Col>
                                <Col md={5} className="offset-2">
                                    <Row><strong>Amount</strong></Row>
                                    <Row>
                                        {"$" + currentItem.amount}
                                    </Row>
                                </Col>
                                <Col md={5}>
                                    <Row><strong>PayType</strong></Row>
                                    <Row>
                                        {currentItem.payType.name}
                                    </Row>
                                </Col>
                                <Col md={5} className="offset-2">
                                    <Row><strong>Status</strong></Row>
                                    <Row className={currentItem.status === "CANCELLED" ? "bg-danger" : "bg-success"}>
                                        {currentItem.status}
                                    </Row>
                                </Col>
                                <Col md={5}>
                                    <Row><strong>Location</strong></Row>
                                    <Row>
                                        {currentItem.address + ", " + currentItem.zipCode.code}
                                    </Row>
                                </Col>
                                <Col md={5} className=" mt-2">
                                    <Button className="btn-block " onClick={() => openModal(false, null)} color="danger"
                                            outline>Cancel order</Button>
                                </Col>
                                <Col md={5} className="offset-2 mt-2">
                                    <Button className="btn-block " onClick={() => openFeedBackModal(true)} color="info"
                                            outline>Feed Back</Button>
                                </Col>
                            </Row>
                            :
                            isAgent ?
                                <Row className="p-3">
                                    <Col md={5}>
                                        <Row><strong>Client name</strong></Row>
                                        <Row>
                                            {currentItem.client.firstName + "  " + currentItem.client.lastName}
                                        </Row>
                                    </Col>
                                    <Col md={5} className="offset-2">
                                        <Row><strong>Phone Number</strong></Row>
                                        <Row>
                                            {currentItem.client.phoneNumber}
                                        </Row>
                                    </Col>
                                    <Col md={5}>
                                        <Row><strong>Service name</strong></Row>
                                        <Row>
                                            {currentItem.servicePrice.subServiceName}
                                        </Row>
                                    </Col>
                                    <Col md={5} className="offset-2">
                                        <Row><strong>Service Description</strong></Row>
                                        <Row>
                                            {currentItem.servicePrice.subServiceDescription}
                                        </Row>
                                    </Col>
                                    <Col md={5}>
                                        <Row><strong>Amount</strong></Row>
                                        <Row>
                                            {"$" + currentItem.amount}
                                        </Row>
                                    </Col>
                                    <Col md={5} className="offset-2">
                                        <Row><strong>PayType</strong></Row>
                                        <Row>
                                            {currentItem.payType.name}
                                        </Row>
                                    </Col>
                                    <Col md={5}>
                                        <Row><strong>Status</strong></Row>
                                        <Row
                                            className={currentItem.status === "CANCELLED" ? "bg-danger" : "bg-success"}>
                                            {currentItem.status}
                                        </Row>
                                    </Col>
                                    <Col md={5} className="offset-2">
                                        <Row><strong>Location</strong></Row>
                                        <Row>
                                            {currentItem.address + ", " + currentItem.zipCode.code}
                                        </Row>
                                    </Col>
                                    <Col md={5} className=" mt-2">
                                        <Button className="btn-block " onClick={() => openModal(false, null)}
                                                color="danger" outline>Cancel order</Button>
                                    </Col>
                                    <Col md={5} className="offset-2 mt-2">
                                        <Button className="btn-block " onClick={() => openFeedBackModal(true)}
                                                color="info" outline>Feed Back</Button>
                                    </Col>
                                </Row>
                                : ''
                        }

                    </ModalBody>

                </Modal> : ''}
                {currentItem != null && showFeedBackModal ?
                    <Modal isOpen={showFeedBackModal} toggle={() => openFeedBackModal(false)}>
                        <AvForm onValidSubmit={addFeedBack}>
                            <ModalHeader>Feedback from Order {currentItem.checkNumber}</ModalHeader>
                            <ModalBody>
                                <AvField type="select" name="rate" label="Select order rate" required>
                                    <option selected value="5">5</option>
                                    <option value="4">4</option>
                                    <option value="3">3</option>
                                    <option value="2">2</option>
                                    <option value="1">1</option>
                                </AvField>
                                <AvField name='description' placeholder="Enter description" required/>
                            </ModalBody>
                            <ModalFooter>
                                <Button type="button" onClick={() => openFeedBackModal(false)}>Cancel</Button>
                                <Button color="info">Send</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal> : ''}
            </div>
        );
    }
}

export default connect(({
                            auth: {isUser, isAgent, currentUser},
                            order: {orders,},
                            app: {showModal, showFeedBackModal, currentItem}

                        }) => ({
    isAgent, isUser, currentUser, orders, showModal, currentItem, showFeedBackModal
}))(DashBoard);