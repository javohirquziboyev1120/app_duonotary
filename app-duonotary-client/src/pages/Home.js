import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import {TOKEN} from "../utils/constants";
import {Button} from "reactstrap";
import {userMe} from "../redux/actions/AuthActions";
import {connect} from "react-redux";
import axios from 'axios'
import {config} from "../utils/config";


class Home extends Component {

    render() {
        const {dispatch, currentUser} = this.props;


        const bolta = () => {
            this.props.history.push('/cabinet')
        }

        return (
            <div>
                <div className="container">
                    <div className="row">
                        <Button outline className='ml-auto' onClick={() => {
                            localStorage.removeItem(TOKEN);
                            window.location.replace("/login")
                        }}>Log out</Button>
                    </div>
                    <div className="row">
                        <div className="col">
                            <h3>Home page</h3>
                        </div>

                        <div className="col">
                            <h3><Link to="/country">Country</Link></h3>
                        </div>
                        {currentUser ? <div className="col">
                            <h3><Link to={"/agentProfile/" + currentUser.id}>Agent Profile</Link></h3>
                        </div> : ''}
                        <div className="col">
                            <h3><Link to="/cabinet">Country</Link></h3>
                        </div>

                        <div className="col">
                            <h3><Link to="/login">Login</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/feedBacks">FeedBacks</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/register">Register</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/state">State</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/county">County</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/service">Service</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/mainService">MainService</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/subService">SubService</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/additionalService">Additional Service</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/additionalServicePrice">Additional Service Price</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/timeDuration">TimeDuration</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/timeBooked">TimeBooked</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/discountPercent">DiscountPercent</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/agentSchedule">Agent Schedule</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/weekDay">Week day</Link></h3>
                        </div>

                        <div className="col">
                            <h3><Link to="/agentList">agentList</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/registerAgent">RegisterAgent</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/servicePrice">ServicePrice</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/pricing">Pricing</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/zipCode">ZipCode</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/orderInPerson">Order In-Person</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/orderOnline">Order Online</Link></h3>
                        </div>


                        <div className="col">
                            <h3><Link to="/ketmon">Ketmon</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/ketmon">Ketmon</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/customer">Customers</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/forgotPassword">ForgotPassword</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to={"/discount"}>Discount</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/ketmon">Ketmon</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/mainSetup">MainSetup</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/holiday">Holiday</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/mainServiceWithPercent">Main Service with percent</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/sharingClient">Sharing client</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/admin">Admin</Link></h3>
                        </div>
                        <div className="col">
                            <h3><Link to="/audit">Audit</Link></h3>
                        </div>


                        {currentUser ?
                            <div className="col">
                                <h3><Link to={"/dashBoard/" + currentUser.id}>Dashboard</Link></h3>
                            </div>

                            : ''}

                        <div className="col">
                            <h3><Link to="/downloadPDF">Download PDF</Link></h3>
                        </div>

                        <button onClick={bolta}>GoCabinet</button>
                        <button onClick={bolta}>GoCabinet</button>
                        <div>
                            <button>
                                <a
                                    href={config.BASE_URL + "/attachment/PDF/52758d28-8a8a-4534-ad66-e26b47cbad36?download=true"}
                                    download
                                >Download document</a>

                            </button>
                        </div>
                        <div>
                            <button
                                style={{marginTop: '10px'}}
                                type='button'>
                                <a
                                    target='_blank'
                                    href={config.BASE_URL + "/attachment/PDF/52758d28-8a8a-4534-ad66-e26b47cbad36"}
                                >Open PDF document in new mirror</a>
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

Home.propTypes = {};

export default connect(({
                            auth: {
                                currentUser
                            }
                        }) => ({currentUser}))(Home);
