import React, {Component} from 'react';
import {
    Button,
    CustomInput,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table
} from "reactstrap";
import {connect} from "react-redux";
import {deleteZipCode, getCountiesList, getZipCodesList, saveZipCode,} from "../redux/actions/AppAction";
import {AvField, AvForm} from "availity-reactstrap-validation";
import DeleteModal from "../components/Modal/DeleteModal";
import StatusModal from "../components/Modal/StatusModal";
import {toast} from "react-toastify";

class ZipCode extends Component {
    componentDidMount() {
        this.props.dispatch(getZipCodesList());
        this.props.dispatch(getCountiesList());
    }

    render() {
        const {dispatch,history, showModal, zipCodes, counties, currentItem, active, showDeleteModal, showStatusModal} = this.props;

        const openModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showModal: !showModal,
                    currentItem: item,
                    active: item.active
                }
            })
        };
        const openDeleteModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentItem: item
                }
            })
        };
        const changeActive = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    active: !active
                }
            })
        };

        const deleteFunction = () => {
            this.props.dispatch(deleteZipCode(currentItem))
        };
        const saveItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null
            v.active = active
            this.props.dispatch(saveZipCode(v))
        }
        const changeStatusZipCode = () => {
            let currentCounty = {...currentItem};
            currentCounty.active = !currentItem.active;
            currentCounty.zipCode = currentItem.zipCode;
            this.props.dispatch(deleteZipCode(currentCounty))
        }

        const openStatusModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item
                }
            })
        }
        const openUserZipCode = (item) => {
           if (item.active){
               dispatch({type:'updateState',payload:{currentItem:item}});
               history.push('/userZipCode/'+item.id)
           }else toast.error("this zip code  is not active")
        };
        return (
            <div>
                <h2 className="text-center">ZipCode list</h2>
                <Button color="success" outline className="my-2" onClick={() => openModal('')}>+ New add</Button>
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Code</th>
                        <th>City</th>
                        <th>County</th>
                        <th colSpan="2">Operation</th>
                    </tr>
                    </thead>
                    {zipCodes!=null ?
                        <tbody>
                        {zipCodes.map((item, i) =>
                            <tr key={item.id} onClick={()=>openUserZipCode(item)}>
                                <td>{i + 1}</td>
                                <td>{item.code}</td>
                                <td>{item.city}</td>
                                <td>{item.countyDto.name}</td>
                                <td>
                                    <FormGroup check>
                                        <Label check for="active">
                                            <Input type="checkbox" onClick={() => openStatusModal(item)} id="active"
                                                   checked={item.active}/>
                                            {item.active ? "Active" : "Inactive"}
                                        </Label>
                                    </FormGroup>
                                </td>
                                <td><Button color="warning" outline onClick={() => openModal(item)}>Edit</Button></td>
                                <td><Button color="danger" outline onClick={() => openDeleteModal(item)}>Delete</Button>
                                </td>

                            </tr>
                        )}
                        </tbody>
                        :
                        <tbody>
                        <tr>
                            <td colSpan="4">
                                <h3 className="text-center mx-auto"> No information </h3>
                            </td>
                        </tr>
                        </tbody>
                    }
                </Table>
                <Modal isOpen={showModal} toggle={openModal}>
                    <AvForm onValidSubmit={saveItem}>
                        <ModalHeader toggle={openModal}
                                     charCode="x">{currentItem != null ? "Edit zipCode" : "Add zipCode"}</ModalHeader>
                        <ModalBody>
                            <AvField name="code" label="Code" required
                                     defaultValue={currentItem != null ? currentItem.code : ""}
                                     placeholder="Enter zipCode code"/>

                            <AvField name="city" label="city" required
                                     defaultValue={currentItem != null ? currentItem.city : ""}
                                     placeholder="Enter zipCode city"/>
                            {counties!=null ?
                                <AvField type="select" name="countyId"
                                         value={currentItem != null ? (currentItem.countyId) : "0"} required>
                                    <option value="0" disabled>Select state</option>
                                    {counties.map(item =>
                                        <option key={item.id} value={item.id}>{item.name}</option>
                                    )}
                                </AvField> :
                                'Counties not found'}

                            <CustomInput type="checkbox" checked={active}
                                         onChange={changeActive}
                                         label={active ? 'Active' : 'Inactive'} id="zipCodeActive"/>
                        </ModalBody>
                        <ModalFooter>
                            <Button type="button" color="secondary" outline onClick={openModal}>Cancel</Button>{' '}
                            <Button color="success">Save</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>

                {showDeleteModal && <DeleteModal text={currentItem != null ? currentItem.name : ''}
                                                 showDeleteModal={showDeleteModal}
                                                 confirm={deleteFunction}
                                                 cancel={openDeleteModal}/>}

                {showStatusModal && <StatusModal text={currentItem != null ? currentItem.name : ''}
                                                 showStatusModal={showStatusModal}
                                                 confirm={changeStatusZipCode}
                                                 cancel={openStatusModal}/>}


            </div>
        );
    }
}

export default connect(({
                            app: {
                                counties, showModal, currentItem, active,
                                showDeleteModal, showStatusModal, zipCodes
                            }
                        }) => ({
    counties, showModal, currentItem, active, showDeleteModal, showStatusModal, zipCodes
}))(ZipCode);
