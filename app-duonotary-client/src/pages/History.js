import React, {Component} from 'react';
import {
    Button,
    Dropdown,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Modal, ModalBody, ModalFooter, ModalHeader,
    Table
} from "reactstrap";
import {getHistory, getHistoryTables} from "../redux/actions/AppAction";
import * as types from "../redux/actionTypes/AppActionTypes";
import {connect} from "react-redux";
import {isObject} from "reactstrap/es/utils";

class History extends Component {
    componentDidMount() {
        this.props.dispatch(getHistory({}))
        this.props.dispatch(getHistoryTables())
    }

    render() {
        const {
            dispatch, histories, historyTables, text, showDropdown, showModal, currentItem, page, size, totalElements, totalPages
        } = this.props;

        const selectedTable = (item) => {
            dispatch(
                getHistory({tableName: item}),
                {
                    type: types.SET_TO_TEXT,
                    payload:item
                }
            )
        }

        const pagination = (item) => {
            dispatch(
                getHistory({tableName: item})
            )
        }

        const openDropdown = () => {
            dispatch({
                type: types.CHANGE_SHOW_DROPDOWN,
                payload: {
                    showDropdown: !showDropdown
                }
            })
        }

        const openModal = (item) => {
            dispatch({
                type: types.CHANGE_SHOW_MODAL,
                payload: {
                    currentItem: item,
                    showModal: item ? true : !showModal,
                }
            })
        }

        return (
            <div>
                <h1 className="text-center">History page</h1>
                <Dropdown isOpen={showDropdown} toggle={openDropdown}>
                    <DropdownToggle caret>
                        Please select table name
                    </DropdownToggle>
                    <DropdownMenu modifiers={{
                        setMaxHeight: {
                            enabled: true,
                            order: 890,
                            fn: (data) => {
                                return {
                                    ...data,
                                    styles: {
                                        ...data.styles,
                                        overflow: 'auto',
                                        maxHeight: '150px',
                                    },
                                };
                            },
                        },
                    }}
                    >
                        <DropdownItem onClick={() => dispatch(getHistory({}))}>All</DropdownItem>
                        {historyTables.map(item => <DropdownItem key={item}
                                                                 onClick={() => selectedTable(item.split('_').join(''))}>{item.split('_').join(' ')}</DropdownItem>)}
                    </DropdownMenu>
                </Dropdown>
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Table Name</th>
                        <th>Operation</th>
                        <th>Time</th>
                        <th>Who</th>
                        <th>His/Him role</th>
                        <th>What did?</th>
                    </tr>
                    </thead>
                    {histories != null ?
                        <tbody>
                        {histories.map((item, i) =>
                            <tr key={item.id}>
                                <td>{i + 1}</td>
                                <td>{item.tableName?item.tableName:text.charAt(0).toUpperCase()+text.substring(1)}</td>
                                <td>{item.operationEnum}</td>
                                <td>{item.createdAt.substring(0, 19).replace('T', " ")}</td>
                                <td>{item.user.firstName} {item.user.lastName}</td>
                                <td>{item.user.roles[0].roleName}</td>
                                <td><Button onClick={() => openModal(item.object)}>Show</Button></td>
                            </tr>
                        )}
                        </tbody> :
                        <tbody>
                        <tr>
                            <td colSpan="4">
                                <h3 className="text-center mx-auto"> No information </h3>
                            </td>
                        </tr>
                        </tbody>
                    }

                </Table>

                <Modal isOpen={showModal}>
                    <ModalHeader>What action</ModalHeader>
                    <ModalBody>
                        <Table>
                            <tbody>
                            {currentItem ? Object.entries(currentItem).map(([key, value]) =>
                                <tr key={key}>
                                    <td>{key}</td>
                                    <td className="text-right">{isObject(value) ?
                                        (<Button onClick={() => openModal(value)}>Show</Button>) : value}</td>
                                </tr>
                            ) : ''}
                            </tbody>
                        </Table>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={() => openModal(null)}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default connect(
    ({
         app: {
             historyTables, histories, text, showModal, currentItem, active, showDropdown, dispatch
         }
     }) => ({
        historyTables,
        histories,
        text,
        showModal,
        currentItem,
        active,
        showDropdown,
        dispatch
    }))(History);