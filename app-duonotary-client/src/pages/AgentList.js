import React, {Component} from 'react';
import {getAgents} from "../redux/actions/AgentAction";
import {connect} from "react-redux";
import {Col, Row} from 'reactstrap'
import {config} from "../utils/config";

class AgentList extends Component {
    componentDidMount() {
        this.props.dispatch(getAgents())
    }

    render() {
        const {agents,isAdmin,loading} = this.props;
        return (
            <div>
                {isAdmin?<div>
                    <Row>
                        <Col>
                            <h1 className="text-center">Agents</h1>
                           <h2> {loading?"true":'false'}</h2>
                        </Col>
                    </Row>
                    {agents !=null ? agents.map((agent) =>
                        <Row key={agent.id} className="bg-light rounded m-1 p-3 " onClick={()=>window.location.replace("/agentProfile/"+agent.id)} style={{cursor:'pointer'}}>
                            <Col md={1} >
                                <img src={config.BASE_URL+"/attachment/"+agent.photoId} alt="" className="img-fluid rounded-circle"/>
                            </Col>
                            <Col md={3} >
                                <h3>Agent</h3>
                                <p>{agent.firstName+" "+agent.lastName}</p>
                            </Col>
                        </Row>
                    ) : ''}
                </div>:''}
            </div>
        );
    }
}

export default connect(
    ({app:{
        loading
    },
         agent: {
             agents
         },
        auth:{
            currentUser,isAdmin
        }
     }) => ({
        agents,currentUser,isAdmin,loading
    }))(AgentList);