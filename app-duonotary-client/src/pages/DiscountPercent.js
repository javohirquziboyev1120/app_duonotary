import React, {Component} from 'react';
import {connect} from "react-redux";
import {
    Button,
    CustomInput,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table
} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import DeleteModal from "../components/Modal/DeleteModal";
import StatusModal from "../components/Modal/StatusModal";
import {
    deleteDiscountPercent,
    getDiscountPercentList,
    getZipCodesList,
    saveDiscountPercent
} from "../redux/actions/AppAction";

class DiscountPercent extends Component {
    componentDidMount() {
        this.props.dispatch(getDiscountPercentList())
        this.props.dispatch(getZipCodesList())
    }

    render() {
        const {dispatch, showModal, zipCodes, discountPercents, currentItem, active, showDeleteModal, showStatusModal} = this.props;

        const openModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showModal: !showModal,
                    currentItem: item,
                    active: item.active
                }
            })
        };
        const openDeleteModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentItem: item
                }
            })
        };
        const changeActive = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    active: !active
                }
            })
        };
        const openStatusModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item
                }
            })
        };
        const deleteFunction = () => {
            this.props.dispatch(deleteDiscountPercent(currentItem))
        };
        const saveItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null
            v.active = active
            v.zipCode ={id:v.zipCode}
            this.props.dispatch(saveDiscountPercent(v))
        }
        const changeStatusDiscountPercent = () => {
            let currentDiscountPercent = {...currentItem};
            currentDiscountPercent.active = !currentItem.active;
            currentDiscountPercent.zipCode = {id:currentItem.zipCodeId};
            this.props.dispatch(saveDiscountPercent(currentDiscountPercent))
        }

        return (
            <div>
                <h2 className="text-center">DiscountPercent list</h2>
                <Button color="success" outline className="my-2" onClick={() => openModal('')}>+ Add</Button>
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Percent</th>
                        <th>Zip Code</th>
                        <th>Status</th>
                        <th colSpan="2" className="">Operation</th>
                    </tr>
                    </thead>
                    {discountPercents!=null ?
                        <tbody>
                        {discountPercents.map((item, i) =>
                            <tr key={item.id}>
                                <td>{i + 1}</td>
                                <td>{item.percent}</td>
                                <td>{item.zipCode.code}</td>
                                <td>
                                    <FormGroup check>
                                        <Label check for={item.id}>
                                            <Input
                                                type="checkbox"
                                                onClick={() =>
                                                    openStatusModal(
                                                        item
                                                    )
                                                }
                                                id={i}
                                                checked={item.active}
                                            />
                                            {item.active
                                                ? "Active"
                                                : "Inactive"}
                                        </Label>
                                    </FormGroup>
                                </td>
                                <td><Button color="warning" outline onClick={() => openModal(item)}>Edit</Button></td>
                                <td><Button color="danger" outline onClick={() => openDeleteModal(item)}>Delete</Button>
                                </td>

                            </tr>
                        )}
                        </tbody>
                        :
                        <tbody>
                        <tr>
                            <td colSpan="4">
                                <h3 className="text-center mx-auto"> No information </h3>
                            </td>
                        </tr>
                        </tbody>
                    }
                </Table>
                <Modal isOpen={showModal} toggle={openModal}>
                    <AvForm onValidSubmit={saveItem}>
                        <ModalHeader toggle={openModal}
                                     charCode="x">{currentItem != null ? "Edit discount percent" : "Add discount percent"}</ModalHeader>
                        <ModalBody>
                            <AvField name="percent" label="percent" required
                                     defaultValue={currentItem != null ? currentItem.percent : ""}
                                     placeholder="Enter discount percent name"/>
                            {zipCodes!=null ?
                                <AvField type="select"  name="zipCode"
                                         value={currentItem != null ? (currentItem.zipCodeId) : "0"} required>
                                    <option value="0" disabled>Select zipCode</option>
                                    {zipCodes.map(item =>
                                        <option key={item.id} value={item.id}>{item.code}</option>
                                    )}
                                </AvField> :
                                'ZipCode not found'}

                            <CustomInput type="checkbox" checked={active}
                                         onChange={changeActive}
                                         label={active ? 'Active' : 'Inactive'} id="discountActive"/>
                        </ModalBody>
                        <ModalFooter>
                            <Button type="button" color="secondary" outline onClick={openModal}>Cancel</Button>{' '}
                            <Button color="success">Save</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>

                {showDeleteModal && <DeleteModal text={currentItem != null ? currentItem.name : ''}
                                                 showDeleteModal={showDeleteModal}
                                                 confirm={deleteFunction}
                                                 cancel={openDeleteModal}/>}

                {showStatusModal && <StatusModal text={currentItem != null ? currentItem.name : ''}
                                                 showStatusModal={showStatusModal}
                                                 confirm={changeStatusDiscountPercent}
                                                 cancel={openStatusModal}/>}
            </div>
        );
    }
}

export default connect(({
                            app: {
                                discountPercents, zipCodes, showModal, currentItem, active,
                                showDeleteModal, showStatusModal, states
                            }
                        }) => ({
    discountPercents, zipCodes, showModal, currentItem, active, showDeleteModal, showStatusModal, states
}))(DiscountPercent);