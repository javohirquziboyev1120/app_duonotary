import React, {Component} from 'react';
import AgentLayout from "../../components/AgentLayout";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {Button, Collapse} from "reactstrap";
import {connect} from "react-redux";
import {toast} from "react-toastify";
import {editPasswordAction} from "../../redux/actions/AuthActions";
import {config} from "../../utils/config";
import {addPhoto, removePhoto} from "../../redux/actions/AttachmentAction";
import { editAgentOwnAction} from "../../redux/actions/AgentAction";
import {Link} from "react-router-dom";
import UploadFile from "../../components/UploadFile";

class Settings extends Component {


    constructor(props) {
        super(props);
        this.state = {editProfile: false, editPassword: false}
    }

    toggleEditProfile = () => {
        this.setState({
            editProfile: !this.state.editProfile
        })
    }
    toggleEditPassword = () => {
        this.setState({
            editPassword: !this.state.editPassword
        })
    }

    render() {
        const {currentUser, dispatch} = this.props;

        const editAgent = (e, v) => {
            if (v.phoneNumber === currentUser.phoneNumber && v.email === currentUser.email){
                toast.success("Successfully saved")
                return"";
            }
                v.id = currentUser.id
            dispatch(editAgentOwnAction(v))
        }

        const editPassword = (e, v) => {
            if (v.password !== v.prePassword) {
                toast.error("Passwords are not matches");
                return "";
            }
            this.props.dispatch(editPasswordAction(v))
        }
        return (
            <AgentLayout pathname={this.props.location.pathname}>
                <div className="agent-setting-page">
                    <div className="flex-column edit-profile-section">


                        <AvForm
                            className={this.state.editProfile ? "flex-column form-section form-padding" : "flex-column form-section"}
                            onValidSubmit={editAgent}>
                            <div>
                                <div className="flex-row justify-content-between">
                                        <span className="form-section-title align-self-center margin-right-15">
                                            Edit profile
                                        </span>
                                    <EditIcon onClick={this.toggleEditProfile}/>
                                </div>
                                <Collapse isOpen={this.state.editProfile}>
                                    <div className="flex-row justify-content-end margin-top-15">
                                        <img
                                            className="form-label avatar-img"
                                            src={currentUser && currentUser.photo ? config.BASE_URL + "/attachment/" + currentUser.photo.id : "/assets/img/avatar.png"}
                                            width="100px"
                                            alt=""
                                        />

                                        <div className="input-full">

                                            <UploadFile

                                                onChange={(a) => dispatch(addPhoto(a.target.files[0], currentUser.id))}
                                                name="avatarId"
                                            />
                                        </div>
                                    </div>
                                    <div  className="ml-4">{currentUser&&currentUser.photo==null?'':<Link onClick={()=>dispatch(removePhoto(currentUser.id))}>Remove photo</Link>}</div>
                                    <div className="flex-row margin-top-15 justify-content-end">
                                        <span className="form-label">Phone Number*</span>
                                        <div className="input-full">
                                            <AvField
                                                name="phoneNumber"
                                                placeholder="Enter Phone Number"
                                                errorMessage=" "
                                                required
                                                defaultValue={currentUser && currentUser.phoneNumber}
                                            />
                                        </div>
                                    </div>
                                    <div className="flex-row margin-top-15 justify-content-end">
                                        <span className="form-label">Email*</span>
                                        <div className="input-full">
                                            <AvField
                                                errorMessage=" "
                                                name="email"
                                                type="email"
                                                placeholder="Enter Email"
                                                required
                                                defaultValue={currentUser && currentUser.email}
                                            />
                                        </div>
                                    </div>


                                    <div className="flex-row justify-content-end  margin-top-25">
                                        <Button
                                            type="button"
                                            color="dark"
                                            onClick={this.toggleEditProfile}
                                        >
                                            Cancel
                                        </Button>
                                        <Button
                                            type="submit"
                                            className="margin-left-15"
                                            color="primary"
                                        >
                                            Save
                                        </Button>

                                    </div>
                                </Collapse>
                            </div>
                        </AvForm>


                    </div>
                    <div className="flex-column edit-profile-section">


                        <AvForm
                            className={this.state.editProfile ? "flex-column form-section form-padding" : "flex-column form-section"}
                            onValidSubmit={editPassword}>
                            <div>
                                <div className="flex-row justify-content-between">
                                        <span className="form-section-title align-self-center margin-right-15">
                                            Edit Password
                                        </span>
                                    <EditIcon onClick={this.toggleEditPassword}/>
                                </div>
                                <Collapse isOpen={this.state.editPassword}>
                                    <div className="flex-row margin-top-15 justify-content-end">
                                        <span className="form-label">Old Password*</span>
                                        <div className="input-full">
                                            <AvField
                                                name="oldPassword"
                                                placeholder="Enter old password"
                                                required
                                                errorMessage=" "
                                            />
                                        </div>
                                    </div>
                                    <div className="flex-row margin-top-15 justify-content-end">
                                        <span className="form-label">New Password*</span>
                                        <div className="input-full">
                                            <AvField
                                                name="password"
                                                placeholder="Enter new password"
                                                errorMessage=" "
                                                required
                                            />
                                        </div>
                                    </div>
                                    <div className="flex-row margin-top-15 justify-content-end">
                                        <span className="form-label">Pre Password*</span>
                                        <div className="input-full">
                                            <AvField
                                                errorMessage=" "
                                                name="prePassword"
                                                placeholder="Enter pre password"
                                                required
                                            />
                                        </div>
                                    </div>
                                    <div className="flex-row justify-content-end  margin-top-25">
                                        <Button
                                            color="dark"
                                            onClick={this.toggleEditPassword}
                                        >
                                            Cancel
                                        </Button>
                                        <Button
                                            type="submit"
                                            className="margin-left-15"
                                            color="primary"
                                        >
                                            Save
                                        </Button>

                                    </div>
                                </Collapse>
                            </div>
                        </AvForm>


                    </div>
                </div>
            </AgentLayout>
        );
    }
}


export default connect(({auth: {currentUser}}) => ({currentUser}))(Settings);


function EditIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className} id={props.id}>
            <svg
                style={{cursor: "pointer"}}
                width="18"
                height="18"
                viewBox="0 0 18 18"
                fill="#313E47"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M12.728 6.68599L11.314 5.27199L2 14.586V16H3.414L12.728 6.68599ZM14.142 5.27199L15.556 3.85799L14.142 2.44399L12.728 3.85799L14.142 5.27199ZM4.242 18H0V13.757L13.435 0.321992C13.6225 0.134521 13.8768 0.0292053 14.142 0.0292053C14.4072 0.0292053 14.6615 0.134521 14.849 0.321992L17.678 3.15099C17.8655 3.33852 17.9708 3.59283 17.9708 3.85799C17.9708 4.12316 17.8655 4.37746 17.678 4.56499L4.243 18H4.242Z"/>
            </svg>
        </div>
    );
}