import React, {Component} from 'react';
import AgentLayout from "../../components/AgentLayout";
import {Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import {config} from "../../utils/config";
import {connect} from "react-redux";
import {addAnswer, editFeedBackAction, getFeedBackByMaxRate, getFeedBackByUserId} from "../../redux/actions/AppAction";
import {AvField, AvForm} from "availity-reactstrap-validation";
import PaginationComponent from "react-reactstrap-pagination";

class Feedbacks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            agents: false,
        };
    }

    componentDidMount() {
        this.props.dispatch(getFeedBackByUserId({page: 0, size: this.props.size}));
    }

    handleSelected = (selectedPage) => {
        if (!this.state.agents) this.props.dispatch(getFeedBackByUserId({
            page: selectedPage-1,
            size: this.props.size
        }));
        else this.props.dispatch(getFeedBackByMaxRate({page: selectedPage-1, size: this.props.size}));
        this.props.dispatch(
            {
                type: 'updateState',
                payload: {
                    page: selectedPage - 1
                }
            }
        )
    }

    render() {
        const {feedBacks, dispatch, isAgent, currentItem, showModal, currentUser, page, size, totalElements} = this.props;
        const editFeedback = (e, v) => {
            v.id = currentItem.id
            v.orderDto = currentItem.orderDto;
            v.agent = isAgent;
            v.operationEnum = "UPDATE";
            v.seen = true;
            v.userId = currentUser.id
            dispatch(editFeedBackAction(v))
        }
        const changeIsAgent = (bool) => {
            if (bool) dispatch(getFeedBackByUserId({page: page, size: size}));
            else dispatch(getFeedBackByMaxRate({page: page, size: size}));
            this.setState({agents: !this.state.agents});
            dispatch({type: 'updateState', payload: {page: 0}})
        }
        const answerFeedback = (e, v) => {
            v.id = currentItem.id
            dispatch(addAnswer(v))
        }
        const openModal = (item) => {
            dispatch({type: 'updateState', payload: {showModal: !showModal, currentItem: item}})
        }
        return (
            <AgentLayout pathname={this.props.location.pathname}>
                <div className="client-feedback-page">
                    <Row className="d-flex justify-content-md-end justify-content-center">
                        <div className="d-inline-block">
                            <div className="route-services">
                                <div onClick={() => changeIsAgent(true)} className={this.state.agents ?
                                    "online" : "router-active"}>Own
                                </div>
                                <div onClick={() => changeIsAgent(false)} className={!this.state.agents ?
                                    "in-person" : "router-active"}>Clients
                                </div>
                            </div>
                        </div>
                    </Row>
                    <Row className="sharing-history-row">
                        {feedBacks.length > 0 ? feedBacks.map(feedBack =>
                            <Col md={12} key={feedBack.id} className="history-col"
                                 onClick={feedBack.seen? '' :!feedBack.agent?"": () => openModal(feedBack)}>
                                <Row>
                                    <Col md={2}>
                                        <div className="d-flex">
                                            <div className="avatar-img">
                                                <img className="agent-avatar"
                                                     src={feedBack.orderDto.client.photoId ? config.BASE_URL + "/attachment/" + feedBack.orderDto.client.photoId : "/assets/img/avatar.png"}
                                                     alt=""/>
                                            </div>
                                            <div className="ml-3">
                                                <div className="fs-15">Clients</div>
                                                <div className="fs-20-bold">
                                                    {feedBack.orderDto.client.firstName + " " + feedBack.orderDto.client.lastName}
                                                </div>
                                            </div>
                                        </div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="border-line"/>
                                    </Col>
                                    <Col md={1} className="px-0">
                                        <div className="fs-15">Agent</div>
                                        <div className="fs-15-bold pt-2">
                                            {feedBack.orderDto.agent.firstName + " " + feedBack.orderDto.agent.lastName}
                                        </div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="border-line"/>
                                    </Col>
                                    <Col md={2}>
                                        <div className="fs-15">
                                            {feedBack.orderDto.client.phoneNumber}
                                        </div>
                                        <div className="fs-15-bold pt-2">
                                            {feedBack.orderDto.client.email}
                                        </div>
                                        </Col>
                                    <Col md={1}>
                                        <div className="border-line"/>
                                    </Col>
                                    <Col md={1}>
                                        <div className="fs-15-bold">Amount</div>
                                        <div className="fs-20-bold pt-2">
                                            {'$' + feedBack.orderDto.amount}
                                        </div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="border-line"/>
                                    </Col>
                                    <Col md={1}>
                                        <div className="fs-15">Rate</div>
                                        <div className="fs-15-bold pt-2">
                                            {feedBack.rate}
                                        </div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="fs-15">Status</div>
                                        <div className="fs-15-bold pt-2">
                                            {feedBack.seen ? "Answered" : 'Pending'}
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={12}>
                                        <div className="feedback-text-admin">
                                            <b className="font-weight-bold ">Agent</b> : {feedBack.description}
                                        </div>
                                    </Col>
                                    {feedBack.seen ?
                                        <Col md={12}>
                                            <div className="feedback-text-admin">
                                                <b className="font-weight-bold ">Admin</b> : {feedBack.answer}
                                            </div>
                                        </Col>
                                        : ""}
                                </Row>
                                <Row>

                                </Row>
                            </Col>
                        ) : <Col md={6} className="mx-auto"><h1 className="text-center">No information</h1>
                        </Col>}</Row>
                    {feedBacks && feedBacks.length > 0 ?
                        <Col md={6} className="mt-4">
                            <PaginationComponent
                                defaultActivePage={page + 1}
                                totalItems={totalElements} pageSize={size}
                                onSelect={this.handleSelected}
                            /></Col> : ''}
                </div>


                {(currentItem != null && showModal && isAgent) ?
                    <Modal isOpen={showModal} toggle={openModal} id="allModalStyle">
                        <AvForm onValidSubmit={editFeedback}>
                            <ModalHeader>edit Feedback {currentItem.checkNumber}</ModalHeader>
                            <ModalBody>
                                <AvField type="select" selected={currentItem.rate} name="rate" label="Select order rate"
                                         required>
                                    <option selected value="5">5</option>
                                    <option value="4">4</option>
                                    <option value="3">3</option>
                                    <option value="2">2</option>
                                    <option value="1">1</option>
                                </AvField>
                                <AvField name='description' defaultValue={currentItem.description}
                                         placeholder="Enter description" type="textarea" style={{height: '10rem'}}
                                         required/>
                            </ModalBody>
                            <ModalFooter>
                                <Button type="button" onClick={openModal}>Cancel</Button>
                                <Button color="info">Send</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal> : ''}
            </AgentLayout>
        );
    }
}


export default connect(({
                            app: {
                                feedBacks,
                                currentItem,
                                showModal, page, size, totalElements
                            },
                            auth: {
                                isAgent,
                                currentUser
                            }
                        }) => ({
    feedBacks, isAgent, currentUser, currentItem, showModal, page, size, totalElements
}))(Feedbacks);