import React, {Component} from "react";
import AgentLayout from "../../components/AgentLayout";
import {Button, Col, Input, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import {FEED_BACK_ALLOW} from "../../utils/constants";
import {config} from "../../utils/config";
import {userMe} from "../../redux/actions/AuthActions";
import {
    changeAgent,
    getAllByUserId, getOrderList, getOrderListByStatus,
    getOrdersBySearchForAgent,
} from "../../redux/actions/OrderAction";
import {addFeedBackAction, getCountryList, getDocumentTypeList, getEmbassy} from "../../redux/actions/AppAction";
import {connect} from "react-redux";
import {AvField, AvForm} from "availity-reactstrap-validation";
import PaginationComponent from "react-reactstrap-pagination";
import RealEstateComponent from "../../components/RealEstateComponent";
import International from "../../components/International";
import OrderDocuments from "../../components/Modal/OrderDocuments";
import ServiceDetails from "../../components/ServiceDetails";
import CloseBtn from "../../components/CloseBtn";

class Orders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            editForm: false,
            inProgress: false,
            cancelModal: false,
            documentModal: false
        };
    }

    componentDidMount() {

        this.props.dispatch(getAllByUserId({
            userId: "1b118d3a-a84c-46e5-a601-a98b2e91b8d1",
            param: {page: 0, size: this.props.size}
        }));
        this.props.dispatch(getCountryList())
        this.props.dispatch(getEmbassy())
        this.props.dispatch(getDocumentTypeList())
    }

    ChangeState(obj) {
        if (obj.documentModal === false) this.props.dispatch(
            getAllByUserId({
                userId: this.props.currentUser.id,
                param: {page: this.props.page, size: this.props.size}
            })
        );
        this.setState({...obj})
        this.props.dispatch({type: 'updateState', payload: {documents: obj.documents}})
    }

    render() {
        const {filters,showModal, orders, dispatch, documents, histor, isAgent, isUser, currentItem, currentUser, cancelAmount, showFeedBackModal, page, size, totalElements, countries, embassyCountries, documentTypes, isApostille,realEstateDto,internationalDto} = this.props;
        const openModal = (bool, item) => {
            if (item&&item.servicePrice && item.servicePrice.serviceDto &&
                item.servicePrice.serviceDto.subServiceDto && item.servicePrice.serviceDto.subServiceDto.serviceEnum === "REAL_ESTATE") {
                dispatch({type: 'updateState', payload: {realEstateDto: item.realEstateDto}})
            }
            if (item&&item.servicePrice && item.servicePrice.serviceDto &&
                item.servicePrice.serviceDto.subServiceDto && item.servicePrice.serviceDto.subServiceDto.serviceEnum === "INTERNATIONAL") {
                dispatch({type: 'updateState', payload: {internationalDto: item.internationalDto}})
            }
            dispatch({type: 'updateState', payload: {showModal: bool, currentItem: item}})
        }
        const handleSelected = (selectedPage) => {
            let obj = {page: selectedPage - 1, size: size}
            let zipCode;
            let date;
            let search;
            if (filters) {
                search = filters.filter(item => item.key === 'search')[0]
                zipCode = filters.filter(item => item.key === 'zipCode')[0]
                date = filters.filter(item => item.key === 'date')[0]
                if (search) obj = {...obj, search: filters.filter(item => item.key === 'search')[0].value};
                if (zipCode && date) obj = {
                    ...obj,
                    zipCode: filters.filter(item => item.key === 'zipCode')[0].value,
                    date: filters.filter(item => item.key === 'date')[0].value
                };
                if (zipCode) obj = {...obj, zipCode: filters.filter(item => item.key === 'zipCode')[0].value};
                if (date) obj = {...obj, date: filters.filter(item => item.key === 'date')[0].value};
            }
            dispatch(search ? getOrdersBySearchForAgent({param: obj}) : zipCode || date ? "" : getAllByUserId({userId: this.props.currentUser.id, param: obj}))
            dispatch(
                {
                    type: 'updateState',
                    payload: {
                        page: selectedPage - 1
                    }
                }
            )
        }

        const getStatus = (item) => {
            if (item === 'IN_PROGRESS') item = 'In-Progress';
            if (item === 'CANCELLED') item = 'Cancelled';
            if (item === 'DRAFT') item = 'Draft';
            if (item === 'CLOSED') item = 'Closed';
            if (item === 'REJECTED') item = 'Rejected';
            if (item === 'COMPLETED') item = 'Completed';
            if (item === 'NEW') item = 'New';
            return item;
        }

        let getTimer = (item, num) => {
            if (currentItem || num === 1) {
                let countDownDate = new Date(
                    item.timeTableDto.fromTime
                ).getTime();
                let x = setInterval(function () {
                    let now = new Date().getTime();
                    let distance = countDownDate - now;
                    let days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    let hours = Math.floor(
                        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
                    );
                    let minutes = Math.floor(
                        (distance % (1000 * 60 * 60)) / (1000 * 60)
                    );
                    let seconds = Math.floor((distance % (1000 * 60)) / 1000);
                    let p = document.getElementById(item.id + num);
                    if (p != null) {
                        let status = item.status;
                        if (status !== 'NEW' || distance < 0) {
                            p.style.letterSpacing = '1px'
                            p.style.color = status === 'CANCELLED' ? 'red' : status === 'COMPLETED' ? 'green' : status === 'IN_PROGRESS' || status === 'DRAFT' ? '#FFC30B' : ''
                            clearInterval(x);
                            p.innerHTML = getStatus(status)
                        } else if (days === 0) {
                            p.innerHTML =
                                "- " + hours + ":" + minutes + ":" + seconds;
                        } else {
                            p.innerHTML =
                                "- " +
                                days +
                                "d " +
                                hours +
                                ":" +
                                minutes +
                                ":" +
                                seconds;
                        }
                    }
                }, 100);
            }
        };
        const cancelOrder = () => {
            if (currentItem != null && currentItem.id != null)
                dispatch(changeAgent({agentId: currentItem.agent.id, sender: true, orderId: currentItem.id}))
        }
        const openCancelModal = () => {
            this.setState({
                cancelModal: !this.state.cancelModal
            })
        }
        const openFeedBackModal = (boolean) => {
            dispatch({type: "updateState", payload: {showFeedBackModal: !showFeedBackModal}})
        }
        const addFeedBack = (e, v) => {
            v.orderDto = currentItem;
            v.agent = isAgent;
            v.operationEnum = "INSERT";
            v.seen = false;
            dispatch(addFeedBackAction(v))
        }
        const clearFilter = () => {
            this.props.dispatch(getOrderList({page: 0, size: this.props.size}));
            dispatch({type: 'updateState', payload: {filters: '', search: ''}})
        }

        return (
            <AgentLayout pathname={this.props.location.pathname}>
                <div className="order-page-client">
                    <Col md={12}>
                        {filters ?
                            <Row>
                                <Col><CloseIcon onClick={() => clearFilter(null, false, true)}/></Col>
                                <Col md={12}>
                                    {filters.map(item =>
                                        <Row className="bg-filter  p-2 m-3 rounded">
                                            <Col md={12}>{item.key} : {item.value}</Col>
                                        </Row>
                                    )}
                                </Col>
                            </Row>

                            : ''}

                    </Col>
                    {isAgent && orders ? orders.map((order, i) =>
                            <Col key={order.id} md={12}>
                                <div className="d-flex main-col justify-content-around">
                                    <div className="d-flex w-25">
                                        <div className="user-img">
                                            <img
                                                src={order.client.photoId ? config.BASE_URL + "/attachment/" + order.client.photoId : "/assets/avatar.png"}
                                                alt=""/>
                                        </div>
                                        <div className="userName pt-2">
                                            <div className="name-title">
                                                Clients
                                            </div>
                                            <div className="name">
                                                {order.client.lastName + " " + order.client.firstName}
                                            </div>
                                        </div>

                                    </div>
                                    <div className="border-line"/>

                                    <div className="time-div pt-2">
                                        <div>
                                                <span
                                                    className="pr-2 fs-15">{order.timeTableDto.day}-{order.timeTableDto.month}, {order.timeTableDto.year}</span>
                                            <span
                                                className="fs-15">{order.timeTableDto.hour}:{order.timeTableDto.min}</span>
                                        </div>
                                        <div className="fs-15-bold">
                                            {order.address !== undefined ? order.address : 'online'}
                                        </div>

                                    </div>
                                    <div className="border-line"/>

                                    <div className="time-sum pt-2">
                                        <div className={
                                            !this.state.inProgress ? "time-minus "
                                                : "fs-15-bold progress-color"
                                        }>
                                            <p id={order.id + 1}>
                                                {getTimer(order, 1)}
                                            </p>
                                        </div>
                                        <div className="fs-20-bold pt-1">
                                            {'$' + (order.amount).toFixed(2)}
                                        </div>
                                    </div>
                                    <div className="border-line"/>

                                    <div className="service-type pt-2">
                                        <div className="fs-15">
                                            Service type
                                        </div>
                                        <div className="fs-15-bold pt-2">
                                            <span>{order.servicePrice.serviceDto.subServiceDto.name} </span>
                                        </div>
                                    </div>

                                    <div onClick={() => openModal(true, order)} className="three-dots">
                                        <div className="dot"/>
                                        <div className="dot"/>
                                        <div className="dot"/>
                                    </div>

                                </div>
                            </Col>
                        )
                        : ''}
                    <Col className="mt-4"> {totalElements > 0 ? <PaginationComponent
                        defaultActivePage={page + 1}
                        totalItems={totalElements} pageSize={size}
                        onSelect={handleSelected}
                    /> : ''}</Col>
                    {showModal && currentItem ?
                        <Modal id="allModalStyle" isOpen={showModal} toggle={() => openModal(false, null)}>
                            <div className='position-absolute' style={{top : '45px', left  : '-12%', zIndex : '100'}}>
                                <CloseBtn click={() =>openModal(false, null)} />
                            </div>
                            <div className="blur-color"/>
                            <div className="edit-order">
                                <div className="container">
                                    <Row>
                                        <Col className='mr-auto'>
                                            <div className="main-edit">
                                                {/*<div*/}
                                                {/*    className="edit-icon"*/}
                                                {/*    onClick={() =>*/}
                                                {/*        openModal(false, null)*/}
                                                {/*    }*/}
                                                {/*>*/}
                                                {/*    <img*/}
                                                {/*        src="/assets/icons/close.svg"*/}
                                                {/*        alt="close-img"*/}
                                                {/*    />*/}
                                                {/*</div>*/}
                                            </div>
                                        </Col>
                                        <Col md={10}>
                                            <div className="order-count">
                                                Order -
                                                <span> {currentItem.serialNumber}</span>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row className="mt-5">
                                        <Col md={12}>
                                            <div className="fs-15">
                                                Agents
                                            </div>
                                            <Input
                                                value={currentItem.agent.lastName + " " + currentItem.agent.firstName}/>
                                        </Col>
                                    </Row>
                                    <Row className="mt-4 pt-2">
                                        <Col md={6}>
                                            <div className="fs-15">
                                                Date, time of order
                                            </div>
                                            <div className="fs-15-bold pt-1">
                                            <span className="pr-2">
                                                {currentItem.timeTableDto.day}-{currentItem.timeTableDto.month}, {currentItem.timeTableDto.year}
                                            </span>
                                                <span>{currentItem.timeTableDto.hour}:{currentItem.timeTableDto.min}</span>
                                            </div>
                                        </Col>
                                        <Col md={6}>
                                            <div className="fs-15">
                                                Service type
                                            </div>
                                            <div className="fs-15-bold pt-1">
                                                <span>{currentItem.servicePrice.serviceDto.subServiceDto.name} </span>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row className="mt-4 pt-2">
                                        <Col md={12}>
                                            <div className="fs-15">
                                                Location
                                            </div>
                                            <div className="fs-15-bold">
                                                {currentItem.address}
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row className="mt-4 pt-2">
                                        <Col md={6}>
                                            <div className="fs-15">
                                                Amount
                                            </div>
                                            <div className="fs-32-bold pt-1">
                                                {'$' + (currentItem.amount).toFixed(2)}
                                            </div>
                                        </Col>
                                        <Col md={6}>
                                            <div className="fs-15">
                                                Status
                                            </div>
                                            <div className="fs-20-bold minus-color pt-1">
                                                <p id={currentItem.id + 2}>
                                                    {getTimer(currentItem, 2)}
                                                </p>
                                            </div>
                                        </Col>
                                    </Row>
                                    <div className="bordered-line"/>
                                    <Row>
                                        <Col md={12}>
                                            {currentItem.status === "CANCELLED" ? '' :
                                                <div className="cancel-sum">
                                                    If free Agent was found and accept your request, you will cancel the
                                                    order
                                                </div>
                                            }
                                        </Col>
                                    </Row>
                                    {currentItem.timeTableDto.online || currentItem.servicePrice.serviceDto.subServiceDto.serviceEnum === "REAL_ESTATE" ?
                                        <Col md={12}><Button className="btn-block my-3"
                                                             onClick={() => this.ChangeState({
                                                                 documentModal: true,
                                                                 documents: currentItem.documents
                                                             })} color="secondary">Show Documents</Button></Col>
                                        : ''}
                                    {currentItem.servicePrice.serviceDto.subServiceDto.serviceEnum === "REAL_ESTATE" ?
                                        <RealEstateComponent
                                            data={realEstateDto}
                                            dispatch={dispatch}
                                            printDocumentCount={1}
                                            printDocumentPrice={1}
                                            isAdd={false}
                                            servicePrice={currentItem.servicePrice}
                                            submit={""}
                                            class={true}
                                        /> : currentItem.servicePrice.serviceDto.subServiceDto.serviceEnum === "INTERNATIONAL" ?
                                            <International
                                                isAdd={false}
                                                submit={""}
                                                dispatch={dispatch}
                                                data={internationalDto}
                                                isApostille={!currentItem.internationalDto.embassy}
                                                countries={countries}
                                                documentTypes={documentTypes}
                                                embassyCountries={embassyCountries}
                                            />
                                            : ""}
                                    {currentItem.orderAdditionalServiceDtoList.length > 0 ?
                                        <div className="mt-4">
                                            <Row className=""><Col md={12} className="text-center"
                                                                   style={{fontSize: '32px'}}>Additional services</Col></Row>
                                            {currentItem.orderAdditionalServiceDtoList.map(oas =>
                                                <ServiceDetails count={oas.count}
                                                                name={oas.additionalServicePriceDto.additionalServiceDto.name}
                                                                price={oas.currentPrice}/>
                                            )}
                                        </div>
                                        : ''}

                                    <Button hidden={currentItem.status !== "NEW"}
                                            disabled={currentItem.status === "CANCELLED"} onClick={openCancelModal}
                                            className="cancel-btn">
                                        {currentItem.status === "CANCELLED" ? "Orders was canceled" : "Cancel the order"}
                                    </Button>
                                    {FEED_BACK_ALLOW.includes(currentItem.status)&&(currentItem.feedbackStatus==='CLIENT' ||currentItem.feedbackStatus==="NONE") ? <Button onClick={openFeedBackModal}
                                                                                            className="cancel-btn ml-2">
                                       Close the order
                                    </Button> : ''}
                                    <Modal isOpen={this.state.cancelModal}>
                                        <ModalHeader>
                                            Are you sure to cancel?
                                        </ModalHeader>
                                        <ModalBody className="text-center">
                                            <Button onClick={cancelOrder}
                                                    className="btn btn-light btn-outline-danger m-1">Yes</Button>
                                            <Button onClick={openCancelModal}
                                                    className="btn btn-light btn-outline-primary m-1">No</Button>
                                        </ModalBody>
                                    </Modal>
                                </div>
                            </div>
                        </Modal>
                        : ""}
                    {showFeedBackModal ?
                        <Modal isOpen={showFeedBackModal} id="allModalStyle" toggle={() => openFeedBackModal(false)}>
                            <AvForm onValidSubmit={addFeedBack}>
                                <ModalHeader>Feedback from Order </ModalHeader>
                                <ModalBody>
                                    <AvField type="select" name="rate" label="Select order rate" required>
                                        <option selected value="5">5</option>
                                        <option value="4">4</option>
                                        <option value="3">3</option>
                                        <option value="2">2</option>
                                        <option value="1">1</option>
                                    </AvField>
                                    <AvField name='description' type="textarea" style={{height: '10rem'}}
                                             placeholder="Enter your feedback" />
                                </ModalBody>
                                <ModalFooter>
                                    <Button type="button" onClick={() => openFeedBackModal(false)}>Cancel</Button>
                                    <Button color="info">Send</Button>
                                </ModalFooter>
                            </AvForm>
                        </Modal> : ''}
                </div>
                {this.state.documentModal ?
                    <OrderDocuments
                        data={documents}
                        order={currentItem}
                        dispatch={dispatch}
                        cancel={(item) => this.ChangeState(item)}
                        showModal={this.state.documentModal}
                        isClient={false}
                        isAgent={true}
                    /> : ''
                }
            </AgentLayout>
        );
    }
}

Orders.propTypes = {};

export default connect(({
                            auth: {isUser, currentUser, isAgent},
                            app: {showModal, currentItem, showFeedBackModal, page, size, totalElements, countries, embassyCountries, documentTypes, isApostille,filters,realEstateDto,internationalDto},
                            order: {orders, cancelAmount, documents}
                        }) => ({
    realEstateDto,internationalDto, isUser, documents,
    currentUser,
    showModal,
    currentItem,
    orders,
    cancelAmount,
    showFeedBackModal,
    isAgent,
    page,
    size,
    totalElements,
    countries,
    embassyCountries,
    documentTypes,
    isApostille,
    filters


}))(Orders);


function ThreeDotIcon(props) {
    return (
        <div onClick={props.onClick}>
            <svg
                width="18"
                height="4"
                viewBox="0 0 18 4"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M1.5 0.5C0.675 0.5 0 1.175 0 2C0 2.825 0.675 3.5 1.5 3.5C2.325 3.5 3 2.825 3 2C3 1.175 2.325 0.5 1.5 0.5ZM16.5 0.5C15.675 0.5 15 1.175 15 2C15 2.825 15.675 3.5 16.5 3.5C17.325 3.5 18 2.825 18 2C18 1.175 17.325 0.5 16.5 0.5ZM9 0.5C8.175 0.5 7.5 1.175 7.5 2C7.5 2.825 8.175 3.5 9 3.5C9.825 3.5 10.5 2.825 10.5 2C10.5 1.175 9.825 0.5 9 0.5Z"
                    fill="black"
                />
            </svg>
        </div>
    );
}
function CloseIcon(props) {
    return <div className={props.className}>
        <svg onClick={props.onClick} style={{cursor: 'pointer'}} width="50" height="50" viewBox="0 0 50 50" fill="none"
             xmlns="http://www.w3.org/2000/svg">
            <circle cx="25" cy="25" r="25" fill="white"/>
            <path
                d="M24.955 23.1875L31.1425 17L32.91 18.7675L26.7225 24.955L32.91 31.1425L31.1425 32.91L24.955 26.7225L18.7675 32.91L17 31.1425L23.1875 24.955L17 18.7675L18.7675 17L24.955 23.1875Z"
                fill="#313E47"/>
        </svg>
    </div>
}