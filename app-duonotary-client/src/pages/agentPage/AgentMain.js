import React, {Component} from "react";
import AgentLayout from "../../components/AgentLayout";
import {
    Button,
    Col,
    Container,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Row,
    UncontrolledCollapse,
} from "reactstrap";
import "./agentPages.scss";
import {connect} from "react-redux";
import {
    changeAgent,
    changeStatusOrder,
    changeStatusToComplete,
    getOrdersForAdminOrder2,
    orderCancel
} from "../../redux/actions/OrderAction";
import getDate from "../../components/Date";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {addFeedBackAction} from "../../redux/actions/AppAction";
import {getAmPm} from "../clientPage/InPersonOrder";

class AgentMain extends Component {
    componentDidMount() {
        this.props.dispatch(getOrdersForAdminOrder2())
    }

    constructor(props) {
        super(props);
        this.state = {
            editForm: false,
            inProgress: false,
            cancelModal: false,
            statusModal: false,
            order: '',
            package: false,
            docId: '',
            orders: [],
            notifyModal: false
        };
    }

    render() {
        const {ordersForClientMainPage, dispatch, showFeedBackModal, showModal, currentItem, isAgent} = this.props;
        const openModal = (item) => {
            dispatch({type: 'updateState', payload: {showModal: !showModal, currentItem: item}})
        }

        const openStatusModal = (item) => {
            setState({
                statusModal: !this.state.statusModal,
                order: item,
            })
        }

        if (ordersForClientMainPage && ordersForClientMainPage.object && ordersForClientMainPage.object.canceledOrder && ordersForClientMainPage.object.canceledOrder[0]) {
            let orders = ordersForClientMainPage.object.canceledOrder;
            document.getElementById("notify-count").innerText = orders.length;
            document.getElementById("notification").addEventListener("click", function () {
                setState({notifyModal: true, orders: orders})
            })
        }

        const responseCanceledOrder = (item) => {
            dispatch(changeAgent({
                id: item.id,
                agentId: item.toAgent.id,
                sender: false,
                accept: item.result,
                orderId: item.order.id
            }))
            setState({
                orders: [],
                notifyModal: false
            })


        }

        const setState = (item) => {
            this.setState(item)
        }

        const getTimer = (item, num) => {
            if (currentItem || num === 1) {
                let countDownDate = new Date(item.timeTableDto.fromTime).getTime();
                let x = setInterval(function () {
                    let now = new Date().getTime();
                    let distance = countDownDate - now;
                    let days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    let seconds = Math.floor((distance % (1000 * 60)) / 1000);

                    let p = document.getElementById(item.id + num);
                    if (p != null) {
                        if (days === 0) {
                            p.innerHTML = '- ' + hours + "h "
                                + minutes + "m " + seconds + "s ";
                        } else {
                            p.innerHTML = '- ' + days + "d " + hours + "h "
                                + minutes + "m " + seconds + "s ";
                        }
                        if (distance < 0) {
                            clearInterval(x);
                            p.innerHTML = item.status;
                        }
                    }
                }, 1000);
            }
        }
        const cancelOrder = () => {
            if (currentItem != null && currentItem.id != null) {
            }
            dispatch(orderCancel(currentItem.id))
        }
        const openCancelModal = () => {
            setState({
                cancelModal: !this.state.cancelModal
            })
        }
        const openFeedBackModal = (boolean) => {
            dispatch({type: "updateState", payload: {showFeedBackModal: !showFeedBackModal}})
        }
        const addFeedBack = (e, v) => {
            v.orderDto = currentItem;
            v.agent = isAgent;
            v.operationEnum = "INSERT";
            v.seen = false;
            dispatch(addFeedBackAction(v))
        }

        const getPackage = (e) => {
            setState({
                package: e.target.checked,
                order: {...this.state.order, packet: e.target.checked}
            })
        }
        const getDocId = (e) => {
            setState({
                docId: e.target.value
            })
        }

        const changeStatus = () => {
            let data = {id: this.state.order.id, currentUserId: this.props.currentUser.id}
            if (this.state.order.servicePrice.online) {
                if (this.state.order.status === "IN_PROGRESS" || this.state.order.status === "COMPLETED") {
                    data = {
                        ...data,
                        docId: this.state.docId,
                        isPackage: this.state.package,
                        currentUserId: this.props.currentUser.id
                    }
                }
                dispatch(changeStatusOrder(data));
            } else {
                if (this.state.order.status === 'NEW') {
                    dispatch(changeStatusOrder(data));
                }
                if (this.state.order.status === 'IN_PROGRESS') {
                    dispatch(changeStatusToComplete({
                        id: this.state.order.id,
                        currentUserId: this.props.currentUser.id
                    }));
                }
            }
            openStatusModal("");
        }
        return (
            <AgentLayout pathname={this.props.location.pathname}>
                {ordersForClientMainPage ? (
                    <div className="agent-main-page container">
                        <Row className="agent-cards-row">
                            <Col className='mx-auto' lg={4} md={8}>
                                <div id="agent-upcoming" className="card-name-upcoming">
                                    <div className="color-card"/>
                                    <div className="name">Upcoming</div>
                                    <div className="card-count">
                                        {
                                            ordersForClientMainPage.object && ordersForClientMainPage.object.upcomingCount > 0 ?
                                                ordersForClientMainPage.object.upcomingCount : 0
                                        }
                                    </div>
                                </div>
                                <UncontrolledCollapse toggler="#agent-upcoming" defaultOpen={true}>
                                    {ordersForClientMainPage.object.upcoming.map(
                                        (item) => (
                                            <div onClick={() => openStatusModal(item)}
                                                 className="about-upcoming"
                                                 key={item.id}>
                                                <div className="time-and-service">
                                                    <div className="time-order">
                                                        <div className="time-title">
                                                            Date
                                                        </div>
                                                        <div className="time">
                                                        <span className="pr-1">
                                                            {getDate(
                                                                item
                                                                    .timeTableDto
                                                                    .fromTime,
                                                                "day"
                                                            )}{" "}
                                                        </span>
                                                        </div>
                                                    </div>

                                                    <div className="service">
                                                        <div className="time-title">
                                                            Service
                                                        </div>
                                                        <div className="time">
                                                        <span>
                                                            {item.servicePrice.serviceDto.subServiceDto.name}
                                                        </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div hidden={!item.address} className="location-title mt-3">
                                                    Location
                                                </div>
                                                <div className="location">
                                                    {item.address}
                                                </div>
                                                <div className="location-title mt-3">
                                                    Client
                                                </div>
                                                <div className="location">
                                                    {item.client.firstName + " " + item.client.lastName}
                                                </div>

                                                <div className="time-and-service">
                                                    <div className="time-order">
                                                        <div className="time-title">
                                                            Amount
                                                        </div>
                                                        <div className="amount pr-4">
                                                            ${(item.amount - item.discountAmount).toFixed(2)}
                                                        </div>
                                                    </div>

                                                    <div className="service">
                                                        <div className="time-title">
                                                            Pay Status
                                                        </div>
                                                        <div className="time">
                                                        <span>
                                                            {item.payed || item.stripe ? "PAYED" : 'UNPAYED'}
                                                        </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    )}
                                </UncontrolledCollapse>
                            </Col>
                            <Col className='mx-auto' lg={4} md={8}>
                                <div id="agent-progress" className="card-name-in-progress">
                                    <div className="color-card"/>
                                    <div className="name">In-Progress</div>
                                    <div className="card-count">
                                        {
                                            ordersForClientMainPage.object
                                                .inProgressCount
                                        }
                                    </div>
                                </div>
                                <UncontrolledCollapse toggler="#agent-progress" defaultOpen={true}>
                                    {ordersForClientMainPage.object.inProgress.map(
                                        (item) => (
                                            <div onClick={() => openStatusModal(item)}
                                                 className="about-in-progress"
                                                 key={item.id}
                                            >
                                                <div className="time-and-service">
                                                    <div className="time-order">
                                                        <div className="time-title">
                                                            Date
                                                        </div>
                                                        <div className="time">
                                                        <span className="pr-1">
                                                            {getDate(
                                                                item
                                                                    .timeTableDto
                                                                    .fromTime,
                                                                "day"
                                                            )}{" "}
                                                        </span>
                                                        </div>
                                                    </div>

                                                    <div className="service">
                                                        <div className="time-title">
                                                            Service
                                                        </div>
                                                        <div className="time">
                                                        <span>
                                                            {item.servicePrice.serviceDto.subServiceDto.name}
                                                        </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div hidden={!item.address} className="location-title mt-3">
                                                    Location
                                                </div>
                                                <div className="location">
                                                    {item.address}
                                                </div>
                                                <div className="location-title mt-3">
                                                    Client
                                                </div>
                                                <div className="location">
                                                    {item.client.firstName + " " + item.client.lastName}
                                                </div>

                                                <div className="time-and-service">
                                                    <div className="time-order">
                                                        <div className="time-title">
                                                            Amount
                                                        </div>
                                                        <div className="amount pr-4">
                                                            ${(item.amount - item.discountAmount).toFixed(2)}
                                                        </div>
                                                    </div>

                                                    <div className="service">
                                                        <div className="time-title">
                                                            Pay Status
                                                        </div>
                                                        <div className="time">
                                                        <span>
                                                            {item.payed || item.stripe ? "PAYED" : 'UNPAYED'}
                                                        </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    )}
                                </UncontrolledCollapse>
                            </Col>
                            <Col className='mx-auto' lg={4} md={8}>
                                <div id="agent-completed" className="card-name-completed">
                                    <div className="color-card"/>
                                    <div className="name">Completed</div>
                                    <div className="card-count">
                                        {
                                            ordersForClientMainPage.object
                                                .completedCount
                                        }
                                    </div>
                                </div>
                                <UncontrolledCollapse toggler="#agent-completed" defaultOpen={true}>
                                    {ordersForClientMainPage.object.completed.map(
                                        (item) => (
                                            <div onClick={() => openStatusModal(item)}
                                                 className="about-completed"
                                                 key={item.id}
                                            >
                                                <div className="time-and-service">
                                                    <div className="time-order">
                                                        <div className="time-title">
                                                            Date
                                                        </div>
                                                        <div className="time">
                                                        <span className="pr-1">
                                                            {getDate(
                                                                item
                                                                    .timeTableDto
                                                                    .fromTime,
                                                                "day"
                                                            )}{" "}
                                                        </span>
                                                        </div>
                                                    </div>

                                                    <div className="service">
                                                        <div className="time-title">
                                                            Service
                                                        </div>
                                                        <div className="time">
                                                        <span>
                                                            {item.servicePrice.serviceDto.subServiceDto.name}
                                                        </span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div hidden={!item.address} className="location-title mt-3">
                                                    Location
                                                </div>
                                                <div className="location">
                                                    {item.address}
                                                </div>
                                                <div className="location-title mt-3">
                                                    Client
                                                </div>
                                                <div className="location">
                                                    {item.client.firstName + " " + item.client.lastName}
                                                </div>

                                                <div className="time-and-service">
                                                    <div className="time-order">
                                                        <div className="time-title">
                                                            Amount
                                                        </div>
                                                        <div className="amount pr-4">
                                                            ${(item.amount - item.discountAmount).toFixed(2)}
                                                        </div>
                                                    </div>

                                                    <div className="service">
                                                        <div className="time-title">
                                                            Pay Status
                                                        </div>
                                                        <div className="time">
                                                        <span>
                                                            {item.payed || item.stripe ? "PAYED" : 'UNPAYED'}
                                                        </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    )}
                                </UncontrolledCollapse>
                            </Col>
                        </Row>
                    </div>
                ) : (
                    <div className="agent-main-page">
                        <Row className="agent-cards-row">
                            <Col md={4}>
                                <div className="card-name-upcoming">
                                    <div className="color-card"/>
                                    <div className="name">Upcoming</div>
                                    <div className="card-count">0</div>
                                </div>
                            </Col>
                            <Col md={4}>
                                <div className="card-name-in-progress">
                                    <div className="color-card"/>
                                    <div className="name">In-Progress</div>
                                    <div className="card-count">0</div>
                                </div>
                            </Col>
                            <Col md={4}>
                                <div className="card-name-completed">
                                    <div className="color-card"/>
                                    <div className="name">Completed</div>
                                    <div className="card-count">0</div>
                                </div>
                            </Col>
                        </Row>
                    </div>
                )}
                {showModal && currentItem ?
                    <div>
                        <div onClick={() => openModal(false, null)} className="blur-color"/>
                        <div className="edit-order">
                            <div className="container">
                                <Row>
                                    <Col md={6}>
                                        <div className="order-count">
                                            Order
                                            <span>
                                       300098
                                   </span>
                                        </div>
                                    </Col>
                                    <Col md={6}>
                                        <div className="main-edit">
                                            <div className="edit-icon">
                                                <img src="/assets/icons/edit.png" alt=""/>
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                                <Row className="mt-5">
                                    <Col md={12}>
                                        <div className="fs-15">
                                            Agents
                                        </div>
                                        <Input
                                            placeholder={currentItem.agent.lastName + " " + currentItem.agent.firstName}/>
                                    </Col>
                                </Row>
                                <Row className="mt-4 pt-2">
                                    <Col md={6}>
                                        <div className="fs-15">
                                            Date, time of order
                                        </div>
                                        <div className="fs-15-bold pt-1">
                                            <span className="pr-2">
                                                {getDate(currentItem.timeTableDto.fromTime, "day")}
                                            </span>
                                            <span>{getDate(currentItem.timeTableDto.fromTime, "time")}</span>
                                        </div>
                                    </Col>
                                    <Col md={6}>
                                        <div className="fs-15">
                                            Service type
                                        </div>
                                        <div className="fs-15-bold pt-1">
                                            {currentItem.servicePrice.subServiceName}
                                        </div>
                                    </Col>
                                </Row>
                                <Row className="mt-4 pt-2">
                                    <Col md={12}>
                                        <div className="fs-15">
                                            Location
                                        </div>
                                        <div className="fs-15-bold">
                                            {currentItem.address}
                                        </div>
                                    </Col>
                                </Row>
                                <Row className="mt-4 pt-2">
                                    <Col md={6}>
                                        <div className="fs-15">
                                            Amount
                                        </div>
                                        <div className="fs-32-bold pt-1">
                                            {'$' + (currentItem.amount).toFixed(2)}
                                        </div>
                                    </Col>
                                    <Col md={6}>
                                        <div className="fs-15">
                                            Status
                                        </div>
                                        <div className="fs-20-bold minus-color pt-1">
                                            <p id={currentItem.id + 2}>{getTimer(currentItem, 2)}</p>
                                        </div>
                                    </Col>
                                </Row>
                                <div className="bordered-line"/>
                                <Button disabled={currentItem.status === "REJECTED"} onClick={() => openCancelModal()}
                                        className="cancel-btn">
                                    {currentItem.status === "REJECTED" ? "Orders was rejected" : "Rejected the order"}
                                </Button>
                                <Button onClick={openFeedBackModal}
                                        className="cancel-btn">
                                    Are you have feedback?
                                </Button>
                                <Modal isOpen={this.state.cancelModal}>
                                    <ModalHeader>
                                        Are you sure to cancel?
                                    </ModalHeader>
                                    <ModalBody className="text-center">
                                        <Button onClick={() => cancelOrder()}
                                                className="btn btn-light btn-outline-danger m-1">Yes</Button>
                                        <Button onClick={() => openCancelModal()}
                                                className="btn btn-light btn-outline-primary m-1">No</Button>
                                    </ModalBody>
                                </Modal>
                            </div>
                        </div>
                    </div>
                    : ""}
                {showFeedBackModal ?
                    <Modal isOpen={showFeedBackModal} toggle={() => openFeedBackModal(false)}>
                        <AvForm onValidSubmit={addFeedBack}>
                            <ModalHeader>Feedback from Order </ModalHeader>
                            <ModalBody>
                                <AvField type="select" name="rate" label="Select order rate" required>
                                    <option selected value="5">5</option>
                                    <option value="4">4</option>
                                    <option value="3">3</option>
                                    <option value="2">2</option>
                                    <option value="1">1</option>
                                </AvField>
                                <AvField name='description' placeholder="Enter description" required/>
                            </ModalBody>
                            <ModalFooter>
                                <Button type="button" onClick={() => openFeedBackModal(false)}>Cancel</Button>
                                <Button color="info">Send</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal> : ''}
                {this.state.statusModal ?
                    <Modal isOpen={this.state.statusModal} toggle={() => openStatusModal('')}>
                        <ModalHeader>Feedback from Order </ModalHeader>
                        <ModalBody>
                            {this.state.order.status === 'NEW' ? "Do you want to change order into IN_PROGRESS" :
                                this.state.order.timeTableDto.online ?
                                    <Container>
                                        <h2>{this.state.order.status === "IN_PROGRESS" ? "Enter docVerify ID and choose package or a doc"
                                            : 'Do you want change docVerify ID and TYPE'}</h2>
                                        <Row className={'m-0'}>
                                            <Col md={6} className={'ml-2'}>
                                                <label htmlFor="package" className={'labelDuo'}>
                                                    <div className={'labelBlock'} style={{left: '-30%'}}>
                                                        {this.state.order.packet && <div className={'labelBox'}>
                                                            <i
                                                                className="fas fa-check"
                                                            />
                                                        </div>}
                                                    </div>
                                                    <input
                                                        className={'d-none'}
                                                        id={'package'}
                                                        onChange={getPackage}
                                                        checked={this.state.order.packet}
                                                        type="checkbox"/>
                                                    Is Package?
                                                </label>
                                            </Col>
                                        </Row>
                                        <Row className={'m-0'}>
                                            <Label>Enter DocVerify ID</Label>
                                            <Input onChange={getDocId} placeholder="Enter DocVerify ID"
                                                   defaultValue={this.state.order.status === "COMPLETED" ? this.state.order.docVerifyId : ''}/>
                                        </Row>
                                    </Container> :
                                    <Container>
                                        <h3>Do you want to complete the order?</h3>
                                    </Container>
                            }
                        </ModalBody>
                        <ModalFooter>
                            <Button type="button" onClick={() => openStatusModal('')}>Cancel</Button>
                            <Button onClick={changeStatus} color="info">Ok</Button>
                        </ModalFooter>
                    </Modal> : ''}
                {this.state.notifyModal ?
                    <Modal isOpen={this.state.notifyModal} toggle={() => setState({notifyModal: false, orders: []})}>
                        <ModalBody>
                            <Container>
                                {this.state.orders.map(v =>
                                    <Row key={v.id}>
                                        <Col md={12}>{v.fromAgent.firstName + ' ' + v.fromAgent.lastName} wants to give
                                            you his order. Do you accept?</Col>
                                        <Col md={12} className="text-center">Order info</Col>
                                        <Col md={4}>
                                            <div className="text-center">Date</div>
                                            <div
                                                className="text-center">{getDate(v.order.timeTableDto.fromTime, "day")}</div>
                                        </Col>
                                        <Col md={4}>
                                            <div className="text-center">Start time</div>
                                            <div
                                                className="text-center">{getAmPm(v.order.timeTableDto.fromTime.substring(v.order.timeTableDto.fromTime.indexOf(' ') + 1))}</div>
                                        </Col>
                                        <Col md={4}>
                                            <div className="text-center">Finish time</div>
                                            <div
                                                className="text-center">{getAmPm(v.order.timeTableDto.tillTime.substring(v.order.timeTableDto.tillTime.indexOf(' ') + 1))}</div>
                                        </Col>
                                        <Col md={8}>
                                            Client: {v.order.client.firstName + ' ' + v.order.client.lastName}
                                        </Col>
                                        <Col md={4}>
                                            Amount: {v.order.amount - v.order.discountAmount}$
                                        </Col>
                                        <Col md={6} className="text-center">
                                            <Button onClick={() => responseCanceledOrder({
                                                ...v,
                                                result: true
                                            })}>Accept</Button>
                                        </Col>
                                        <Col md={6} className="text-center">
                                            <Button onClick={() => responseCanceledOrder({
                                                ...v,
                                                result: false
                                            })}>Reject</Button>
                                        </Col>
                                    </Row>
                                )}
                            </Container>
                        </ModalBody>
                    </Modal> : ''
                }

            </AgentLayout>
        );
    }
}

export default connect(
    ({order: {ordersForClientMainPage, cancelAmount}, auth: {currentUser, isAgent}, agent: {showFeedBackModal}, app: {showModal, currentItem}}) => ({
        ordersForClientMainPage,
        currentUser, showFeedBackModal, showModal, currentItem, isAgent, cancelAmount
    })
)(AgentMain);
