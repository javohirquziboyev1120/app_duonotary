import React, {Component} from 'react';
import AgentLayout from "../../components/AgentLayout";
import './ZipCode.scss';
import {AddIcon} from "../../components/Icons";
import Switch from "react-switch";
import {connect} from "react-redux";
import AddUserZipcode from "../../components/Modal/AddUserZipcode";
import {
    addUserZipcodeAction,
    changeActiveUserZipcodeAction,
    getUserZipCodeByUserAction,
    getZipCodeByUser
} from "../../redux/actions/AppAction";
import ChangeActiveModal from "../../components/Modal/ChangeActiveModal";
import {toast} from "react-toastify";

class ZipCode extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            activeModal: false,
            currentItem:''
        };
    }
    componentDidMount() {
        this.props.dispatch(getZipCodeByUser())
        this.props.dispatch(getUserZipCodeByUserAction())
    }
    checkEnable(item){
        return item?'Enable':'Disable';
    }
    render() {
        let {zipCodes,userZipCodes,dispatch}=this.props
        let {modal,activeModal,currentItem}=this.state
        return (
            <AgentLayout pathname={this.props.location.pathname}>
                <div>
                    <div className='d-flex align-items-center'>
                        <div className=''><AddIcon onClick={()=>this.setState({modal:true})}/></div>
                        <div className='font-weight-bold' style={{fontSize : '24px'}}>Add New ZipCode</div>
                    </div>
                    <div className='zipBlock'>
                        {userZipCodes?
                            userZipCodes.map(item=>
                                <div className='zipBox'>
                                    <div className='zipNumber'>{item.zipCode.code}</div>
                                    <div className='zipStatus my-3'>Status : <span className={item.enable?'text-success':'text-danger'}>{this.checkEnable(item.enable)}</span></div>
                                    <div className='zipSwitch'>
                                        <Switch
                                            checked={item.active}
                                            onChange={() => {
                                                if (item.enable)this.setState({activeModal:true,currentItem:item});
                                                else toast.error("First the administrator must give permission")
                                            }}
                                        />
                                    </div>
                                </div>
                            )
                            :<div className="row mx-auto no">You do not have zipcodes</div>}
                    </div>
                </div>
                {modal&&<AddUserZipcode
                    submit={(item)=>dispatch(addUserZipcodeAction(item.map(item=>{return item.value})))}
                    zipCodes={zipCodes}
                    showModal={modal}
                    cancel={()=>this.setState({modal:false})}
                />}
                {activeModal&&<ChangeActiveModal
                cancel={()=>this.setState({activeModal:false})}
                showModal={activeModal}
                headText={"Do you want to change active this zip code : "+currentItem.zipCode.code+" ?"}
                submit={()=>dispatch(changeActiveUserZipcodeAction(currentItem.id))}
                />}
            </AgentLayout>
        );
    }
}
export default connect(({app:{zipCodes},agent:{userZipCodes}})=>({zipCodes,userZipCodes}))(ZipCode);
function AgentAddIcon(props) {
    return (
        <div className={props.className} onClick={props.onClick}>
            <svg
                width="19"
                height="21"
                viewBox="0 0 19 21"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M10 13.252V15.342C9.09492 15.022 8.12628 14.9239 7.1754 15.0558C6.22453 15.1877 5.3192 15.5459 4.53543 16.1002C3.75166 16.6545 3.11234 17.3888 2.67116 18.2414C2.22998 19.094 1.99982 20.04 2 21L2.58457e-07 20.999C-0.000310114 19.7779 0.278921 18.5729 0.816299 17.4764C1.35368 16.3799 2.13494 15.4209 3.10022 14.673C4.0655 13.9251 5.18918 13.4081 6.38515 13.1616C7.58113 12.9152 8.81766 12.9457 10 13.251V13.252ZM8 12C4.685 12 2 9.315 2 6C2 2.685 4.685 0 8 0C11.315 0 14 2.685 14 6C14 9.315 11.315 12 8 12ZM8 10C10.21 10 12 8.21 12 6C12 3.79 10.21 2 8 2C5.79 2 4 3.79 4 6C4 8.21 5.79 10 8 10ZM14 16V13H16V16H19V18H16V21H14V18H11V16H14Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}