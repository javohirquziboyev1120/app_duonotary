import React, {Component} from 'react';
import AgentLayout from "../../components/AgentLayout";
import {Button, Col} from "reactstrap";
import {connect} from "react-redux";
import {addPassportAction, editPassportAction, getPassportsByUserId} from "../../redux/actions/AgentAction";
import {getCountiesList} from "../../redux/actions/AppAction";
import PhotoModal from "../../components/Modal/PhotoModal";
import CertificateModal from "../../components/Modal/CertificateModal";
import {toast} from "react-toastify";
import TimeStampToDate from "../../components/TimeStampToDate";

class Passports extends Component {
    componentDidMount() {
            this.props.dispatch(getPassportsByUserId());
            this.props.dispatch(getCountiesList())
    }
    constructor(props) {
        super(props);
        this.state = {
            agentEdit: true,
            activeModal: false,
            item:''
        };
    }

    render() {
        const {passports,isCertificate, currentUser, attachmentIdsArray, counties, showCertificateModal, dispatch, currentItem, statusEnums} = this.props;
        const checkExpire = (date) => {
            let a = new Date().toJSON().slice(0, 10);
            return (
                new Date(
                    a.substring(0, 4),
                    a.substring(5, 7),
                    a.substring(8)
                ) <=
                new Date(
                    date.substring(0, 4),
                    date.substring(5, 7),
                    date.substring(8)
                )
            );
        };
        const addPassport = (e, v) => {
            if (!checkExpire(v.expireDate)){
                toast.error("Your passport date is invaild")
                return '';
            }
            v.attachmentId = attachmentIdsArray.filter(item => item.key === "document")[0].value
            v.userId=currentUser.id;
            v.userDto=currentUser;
            v.issueDate=new Date();
            dispatch(addPassportAction(v))
        }
        const editPassport = (e, v) => {
            v.attachmentId = attachmentIdsArray.filter(item => item.key === "document")[0].value
            v.userId=currentUser.id;
            v.id=currentItem.id
            dispatch(editPassportAction(v))
        }
        const  openPhoto=(item)=>{
            this.setState({item:item,activeModal:!this.state.activeModal})
        }
        const openCertificateModal = (item=null, boolean=false) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showCertificateModal: !showCertificateModal,
                    currentItem: item,
                    isCertificate: boolean
                }
            })
        }
        return (
            <AgentLayout pathname={this.props.location.pathname}>
                <div className="passport-agent-page container">
                    <Col md={12} className="mb-5 p-0">
                        <Button onClick={openCertificateModal}>Add Photo ID</Button>
                    </Col>
                {passports?passports.map(passport=>
                        <Col lg={6} md={10}  key={passport.id}>
                            <div className="passport">
                                {/*<div className="d-flex">*/}
                                {/*    <div className="name">Issue Date</div>*/}
                                {/*    <div className="ml-4">*/}
                                {/*        <TimeStampToDate item={passport.issueDate}/>*/}
                                {/*    </div>*/}
                                {/*</div>*/}
                                <div className="d-flex ">
                                    <div className="name">Expiration Date:</div>
                                    <div className=" ml-4">
                                        <TimeStampToDate item={passport.expireDate}/>

                                    </div>
                                </div>
                                <div className="d-flex mt-3">
                                    <div className="name pt-1">Status:</div>
                                    <div className="ml-4">
                                        {passport.statusEnum}
                                    </div>
                                </div>
                                <div className="change-status">
                                    <Button className="mr-3"
                                            onClick={()=>openPhoto(passport.attachmentId)}>Document</Button>
                                    {/*<Button*/}
                                    {/*    onClick={() => openCertificateModal(passport, true)}>Edit</Button>*/}
                                </div>
                            </div>
                        </Col>
                ):''}
                </div>
                    <CertificateModal
                        dispatch={dispatch}
                        headerText={isCertificate ? "Certificate" : "Passport"}
                        submit={addPassport}
                        attachmentIdsArray={attachmentIdsArray}
                        counties={counties}
                        data={currentItem}
                        isAdmin={false}
                        isCertificate={isCertificate}
                        showCertificateModal={showCertificateModal}
                        stausEnums={statusEnums}
                        cancel={()=>openCertificateModal(null)}
                    />
                <PhotoModal
                cancel={()=>openPhoto(null)}
                showModal={this.state.activeModal}
                id={this.state.item}
                />
            </AgentLayout>
        );
    }
}

Passports.propTypes = {};

export default connect(({
    auth:{currentUser},agent:{passports,showCertificateModal},attachment:{attachmentIdsArray},app:{currentItem}
                        })=>({
    currentUser,passports,showCertificateModal,attachmentIdsArray,currentItem
}))(Passports);