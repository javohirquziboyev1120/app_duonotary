import React, {Component} from 'react';
import {
    changeScheduleDayOff,
    changeScheduleHourOff,
    deleteAgentSchedule,
    getAgentScheduleList,
    getDaylySchedule,
    getWeekDayList,
    saveAgentSchedule
} from "../../redux/actions/AppAction";
import {connect} from "react-redux";
import {Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import AgentLayout from "../../components/AgentLayout";
import DeleteModal from "../../components/Modal/DeleteModal";
import StatusModal from "../../components/Modal/StatusModal";
import {userMe} from "../../redux/actions/AuthActions";
import "./agentPages.scss"
import {getAmPm} from "../clientPage/InPersonOrder";
import CloseBtn from "../../components/CloseBtn";

class AgentSchedule extends Component {
    componentDidMount() {
        this.props.dispatch(userMe()).then(() => {
            this.props.dispatch(getAgentScheduleList(this.props.currentUser.id))
            this.props.dispatch(getWeekDayList())
        });
    }

    render() {
        const {dispatch, showModal, currentDate, showDaylyModal, daylySchedules, agentSchedules, weekDays, currentItem, active, showDeleteModal, showStatusModal, today, dayOff, agentHours, currentUser} = this.props;

        const saveItem = (e, v) => {
            let hourList = [];
            v.hourList.map(item => {
                if (item.tillTime === "00:00") {
                    item.tillTime = "24:00";
                }
                hourList.push(item);
            })
            v.hourList = hourList;
            v.id = currentItem != null ? currentItem.id : null
            v.active = active
            v.currentUser = currentUser
            this.props.dispatch(saveAgentSchedule(v))
        }

        const openModal = (item) => {
            let hourList = [];
            item.hourList ? item.hourList.map(v => {
                if (v.tillTime === "24:00") {
                    v = {...v, tillTime: "00:00"}
                }
                hourList.push(v);
            }) : hourList = [];
            dispatch({
                type: 'updateState',
                payload: {
                    showModal: !showModal,
                    dayOff: item.dayOff != null ? item.dayOff : false,
                    currentItem: item,
                    agentHours: item.hourList ? item.hourList[0] != null ? hourList : [{id: new Date().toString()}] : []
                }
            })
        };

        const openDaylyModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showDaylyModal: !showDaylyModal,
                    currentDate: item
                }
            })
        };

        const changeStatusAgentScheduleDay = () => {
            let currentSchedule = {...currentItem, dayOff: !currentItem.dayOff, currentUser};
            this.props.dispatch(changeScheduleDayOff(currentSchedule))
        }

        const changeDayOff = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    dayOff: !dayOff,
                }
            })
        }

        const deleteFunction = () => {
            this.props.dispatch(deleteAgentSchedule({...currentItem, currentUser}))
        };

        const openDeleteModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentItem: item
                }
            })
        };

        const openStatusModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item
                }
            })
        };

        const removeHour = (j) => {
            let hours = agentHours.filter(k => k !== j);
            dispatch({
                type: 'updateState',
                payload: {
                    agentHours: hours
                }
            })
        };

        const addHour = () => {
            let hour = {id: new Date().toString()}
            let hours = [...agentHours, hour]
            dispatch({
                type: 'updateState', payload: {agentHours: hours}
            })
        };

        const saveHourOff = (item) => {
            let a = currentDate + ' ' + item.fromTime + ':00';
            const from = a.replaceAll('/', '-')
            this.props.dispatch(changeScheduleHourOff({from, till: item.tillTime + ':00', date: currentDate}));
        };

        const getDaylySchedules = (day) => {
            this.props.dispatch(getDaylySchedule(day))
            dispatch({
                type: 'updateState', payload: {currentDate: day}
            })
        };

        function getDates(item, count) {
            const date = item ? new Date(item) : new Date();
            let datesCollection = []
            datesCollection.push(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate())
            for (let i = 1; i < count; i++) {
                const newDate = new Date(date.getTime() + i * 1000 * 60 * 60 * 24);
                datesCollection.push(newDate.getFullYear() + '/' + (newDate.getMonth() + 1) + '/' + newDate.getDate());
            }
            return datesCollection
        }

        function getCard(v) {
            if (v.hourList[0] != null)
                return (
                    <div className="week">
                        <div className="week-name h3 font-weight-bolder">
                            <div className="none">{v.weekDay.day}</div>
                            <div onClick={() => openStatusModal(v)}
                                 className={v.hourList[0] ? v.dayOff ? "de-active" : "active" : 'red'}/>
                        </div>
                        <div className="week-card">
                            {v.hourList ? v.hourList.map(o =>
                                <div className="day-title fs-20-bold">
                                    {getAmPm(o.fromTime) + ' - ' + getAmPm(o.tillTime)}
                                </div>
                            ) : ''}
                            <div className="day-action">
                                <div className="edit" onClick={() => openModal(v)}>Edit</div>
                                <div className="delete" onClick={() => openDeleteModal(v)}>Delete</div>
                            </div>
                        </div>
                    </div>
                )
            else return (
                <div className="week">
                    <div className="week-name h3 font-weight-bolder">
                        <div className="none">{v.weekDay.day}</div>
                        <div onClick={() => openStatusModal(v)} className={"red"}/>
                    </div>
                    <div className="week-card">
                        <div className="day-action ">
                            <Button className="btn btn-light btn-outline-primary w-100"
                                    onClick={() => openModal(v)}>Set
                                up time</Button>
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <AgentLayout pathname={this.props.location.pathname}>
                <div>
                    <Row>
                        <Col>
                            <div>
                                {getDates(new Date(), 7).map((v, index) =>
                                    <button key={index} className="btn btn-outline-primary m-1"
                                            onClick={() => getDaylySchedules(v)}>{v}
                                    </button>
                                )}
                            </div>
                        </Col>
                    </Row>
                    <Row className="schedule-main">
                        <div className="today">
                            <div className="name">
                                <div className="fs-20-bold none">Today</div>
                                <div className="fs-32-bold">{today ? today.weekDay.day : ''}</div>
                            </div>
                            <div className="times">
                                <Row className="times-row">
                                    {today ? today.hourList.map((v, i) =>
                                        <Col md={today.hourList.length > 4 ? 4 : 6} className="times-col d-flex">
                                            <div className="fs-20-bold">
                                                {getAmPm(v.fromTime) + ' - ' + getAmPm(v.tillTime)}
                                            </div>
                                        </Col>
                                    ) : ''}
                                </Row>
                            </div>
                            {today ? today.hourList[0] ?
                                <div className="action">
                                    <Button className="btn btn-light btn-outline-success w-75 mb-1"
                                            onClick={() => openModal(today)}>Edit</Button>
                                    <Button className="btn btn-light btn-outline-warning w-75 mt-1"
                                            onClick={() => openDeleteModal(today)}>Delete</Button>
                                </div> :
                                <div className="action">
                                    <Button className="btn btn-light btn-outline-primary w-75"
                                            onClick={() => openModal(today)}>Set up time</Button>
                                </div> : ''
                            }

                        </div>
                        <div className="weeks">
                            {agentSchedules[0] ? getCard(agentSchedules[0]) : ''}
                            {agentSchedules[1] ? getCard(agentSchedules[1]) : ''}
                            {agentSchedules[2] ? getCard(agentSchedules[2]) : ''}
                            {agentSchedules[3] ? getCard(agentSchedules[3]) : ''}
                            {agentSchedules[4] ? getCard(agentSchedules[4]) : ''}
                            {agentSchedules[5] ? getCard(agentSchedules[5]) : ''}
                        </div>
                    </Row>
                </div>

                <Modal isOpen={showModal} id="allModalStyle" toggle={openModal}>
                    <AvForm onValidSubmit={saveItem}>
                        <div className="closeBtnAgent">
                            <CloseBtn click={openModal} />
                        </div>
                        <ModalHeader toggle={openModal} charCode="x">
                            {currentItem != null ? "Edit schedule" : "Add schedule"}
                        </ModalHeader>
                        <ModalBody>
                            <div className="w-100">
                                <AvField name="weekDayIds"
                                         style={{height: "155px"}}
                                         value={[currentItem != null ? currentItem.weekDayId : ""]}
                                         type="select" multiple required
                                         label="Week days">
                                    {weekDays.map((item, index) =>
                                        <option key={index} value={item.id}>{item.day}</option>
                                    )}
                                </AvField>
                            </div>
                            <div className="row m-0">
                                <div className="col-md-12">
                                    <AvField
                                        name="dayOff"
                                        type="checkbox"
                                        checked={currentItem != null ? currentItem.dayOff : dayOff}
                                        label="Day off"
                                        onClick={() => changeDayOff(currentItem != null ? !currentItem.dayOff : !dayOff)}
                                    />
                                </div>
                            </div>
                            {agentHours.map((item, i) => (
                                <div key={item.id} className="row m-0">
                                    <div className="d-flex align-items-center">
                                        <AvField
                                            value={agentHours.length > 0 ? agentHours[i].fromTime : ''}
                                            label="From time"
                                            name={"hourList[" + i + "].fromTime"}
                                            placeholder="09:00:00"
                                            type="time"
                                            validate={{
                                                required: {value: true},
                                                // minLength: {value: 13},
                                                // maxLength: {value: 13}
                                            }}/>
                                        <AvField
                                            value={agentHours.length > 0 ? agentHours[i].tillTime : ''}
                                            label="Till time"
                                            name={"hourList[" + i + "].tillTime"}
                                            placeholder="10:00:00"
                                            type="time"
                                            validate={{
                                                required: {value: true}
                                            }}/>
                                        {i !== 0 ?

                                            <div>
                                                <button type="button" className='btn btn-outline-danger' onClick={() => removeHour(item)}>
                                                    -
                                                </button>
                                            </div>

                                            :

                                            <div>
                                                <button type="button" className='btn btn-outline-info' onClick={addHour}>
                                                    +
                                                </button>
                                            </div>

                                        }
                                    </div>
                                </div>
                            ))}
                        </ModalBody>
                        <ModalFooter>
                            {/*<button type="button" className='btn btn-outline-danger' onClick={openModal}>Cancel</button>{' '}*/}
                            <button type='submit' className='btn btn-outline-info'>Save</button>
                        </ModalFooter>
                    </AvForm>
                </Modal>

                <Modal isOpen={showDaylyModal} size="lg" className="dayly-modal" modalClassName="right"
                       toggle={() => openDaylyModal('')}>
                    <ModalHeader>
                        <Row>
                            <Col className="text-center" md={8}>
                                <h2 className="text-primary">{currentDate}</h2>
                            </Col>
                        </Row>
                    </ModalHeader>
                    <ModalBody>
                        {daylySchedules != null ?
                            <div>
                                <Row>
                                    <Col className="text-center" md={4}>
                                        <h3>FREE</h3>
                                        <Row>
                                            <Col className="from-till text-center " md={6}>
                                                <p style={{color: "green"}}>FROM</p>
                                            </Col>
                                            <Col className="from-till text-center" md={6}>
                                                <p style={{color: "red"}}>TILL</p>
                                            </Col>
                                        </Row>
                                        {daylySchedules.map((item, i) => (
                                            <Row className="row-btn row-btn-pointer" key={i + 1}
                                                 onClick={() => saveHourOff(item)}>
                                                {item.hourTypeEnum === 'FREE' ?
                                                    <Col>
                                                        <Row className=''>
                                                            <Col md={6}
                                                                 className="d-flex align-items-center justify-content-start text-center">
                                                                <div className='mx-auto'>{getAmPm(item.fromTime)}</div>
                                                            </Col>
                                                            <Col md={6}
                                                                 className="d-flex align-items-center justify-content-start text-center">
                                                                <div className='mx-auto'>{getAmPm(item.tillTime)}</div>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                    : ''
                                                }
                                            </Row>
                                        ))}
                                    </Col>
                                    <Col className="text-center" md={4}>
                                        <h3>BOOKED</h3>
                                        <Row>
                                            <Col className="from-till text-center" md={6}>
                                                <p>FROM</p>
                                            </Col>
                                            <Col className="from-till text-center" md={6}>
                                                <p>TILL</p>
                                            </Col>
                                        </Row>
                                        {daylySchedules.map((item, i) => (
                                            <Row key={i + 1} className='row-btn'>
                                                {item.hourTypeEnum === 'BOOKED' ?
                                                    <Col>
                                                        <Row>
                                                            <Col md={6}
                                                                 className="d-flex align-items-center justify-content-start">
                                                                <div className="mx-auto">{getAmPm(item.fromTime)}</div>
                                                            </Col>
                                                            <Col md={6}
                                                                 className="d-flex align-items-center justify-content-start">
                                                                <div className="mx-auto">{getAmPm(item.tillTime)}</div>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                    : ''
                                                }
                                            </Row>
                                        ))}
                                    </Col>
                                    <Col className="text-center" md={4}>
                                        <h3>BREAK</h3>
                                        <Row>
                                            <Col className="from-till text-center" md={6}>
                                                <p style={{color: "green"}}>FROM</p>
                                            </Col>
                                            <Col className="from-till text-center" md={6}>
                                                <p style={{color: "red"}}>TILL</p>
                                            </Col>
                                        </Row>
                                        {daylySchedules.map((item, i) => (
                                            <Row className="row-btn row-btn-pointer" key={i + 1}
                                                 onClick={() => saveHourOff(item)}>
                                                {item.hourTypeEnum === 'BREAK' ?
                                                    <Col>
                                                        <Row>
                                                            <Col md={6}
                                                                 className="d-flex align-items-center justify-content-start">
                                                                <div className="mx-auto">{getAmPm(item.fromTime)}</div>
                                                            </Col>
                                                            <Col md={6}
                                                                 className="d-flex align-items-center justify-content-start">
                                                                <div className="mx-auto">{getAmPm(item.tillTime)}</div>
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                    : ''
                                                }
                                            </Row>
                                        ))}
                                    </Col>
                                </Row>
                            </div>
                            : ''}
                    </ModalBody>
                    <ModalFooter>
                        <button className="btn btn-primary" onClick={() => openDaylyModal("")}>
                            Close
                        </button>
                    </ModalFooter>
                </Modal>

                {
                    showDeleteModal && <DeleteModal text={currentItem != null ? currentItem.weekDay.day : ''}
                                                    showDeleteModal={showDeleteModal}
                                                    confirm={deleteFunction}
                                                    cancel={openDeleteModal}
                    />
                }

                {
                    showStatusModal && <StatusModal text={currentItem != null ? currentItem.weekDay.day : ''}
                                                    showStatusModal={showStatusModal}
                                                    confirm={changeStatusAgentScheduleDay}
                                                    cancel={openStatusModal}
                    />
                }
            </AgentLayout>
        );
    }
}


export default connect(
    ({
         app: {
             weekDays, currentDate, agentSchedules, daylySchedules, showModal, showDaylyModal, currentItem, active, showDeleteModal, showStatusModal, today, dayOff, agentHours
         }
         ,
         auth: {currentUser}
     }
    ) =>
        ({
            weekDays,
            agentSchedules,
            daylySchedules,
            showModal,
            currentDate,
            showDaylyModal,
            currentItem,
            active, showDeleteModal, showStatusModal, today, dayOff, agentHours,
            currentUser
        })
)
(AgentSchedule);