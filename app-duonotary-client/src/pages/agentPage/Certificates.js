import React, {Component} from 'react';
import AgentLayout from "../../components/AgentLayout";
import {Button, Col, Input} from "reactstrap";
import {connect} from "react-redux";
import {addCertificateAction, editCertificateAction, getCertificatesByUserId} from "../../redux/actions/AgentAction";
import { getStateList} from "../../redux/actions/AppAction";
import PhotoModal from "../../components/Modal/PhotoModal";
import CertificateModal from "../../components/Modal/CertificateModal";
import {toast} from "react-toastify";
import TimeStampToDate from "../../components/TimeStampToDate";

class Certificates extends Component {
    componentDidMount() {
        this.props.dispatch(getCertificatesByUserId());
        this.props.dispatch(getStateList())
    }

    constructor(props) {
        super(props);
        this.state = {
            agentEdit: true,
            activeModal: false,
            item: ''
        };
    }

    render() {
        const {certificates, isCertificate, currentUser, attachmentIdsArray, states, showCertificateModal, dispatch, currentItem, statusEnums} = this.props;
        const checkExpire = (date) => {
            let a = new Date().toJSON().slice(0, 10);
            return (
                new Date(
                    a.substring(0, 4),
                    a.substring(5, 7),
                    a.substring(8)
                ) <=
                new Date(
                    date.substring(0, 4),
                    date.substring(5, 7),
                    date.substring(8)
                )
            );
        };
        const addCertificate = (e, v) => {

            if (!checkExpire(v.expireDate)) {
                toast.error("Your certificate date is invaild")
                return '';
            }
            let state = {}
            state.id = v.state
            v.attachmentId = attachmentIdsArray.filter(item => item.key === "document")[0].value
            v.userDto = currentUser;
            v.stateDto = state
            v.issueDate=new Date();
            dispatch(addCertificateAction([v], currentUser.id))
        }
        const editCertificate = (e, v) => {
            v.attachmentId = attachmentIdsArray.filter(item => item.key === "document")[0].value
            v.userId = currentUser.id;
            v.id = currentItem.id
            dispatch(editCertificateAction(v))
        }
        const openPhoto = (item) => {
            this.setState({item: item, activeModal: !this.state.activeModal})
        }
        const openCertificateModal = (item = null, boolean = true) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showCertificateModal: !showCertificateModal,
                    currentItem: item,
                    isCertificate: boolean
                }
            })
        }
        return (
            <AgentLayout pathname={this.props.location.pathname}>
                <div className="certificates-agent-page container">
                    <Col md={12} className="mb-5 p-0">
                        <Button onClick={() => openCertificateModal(null, true)}>Add Certificate</Button>
                    </Col>
                    {certificates && states ? certificates.map(certificate =>
                        <Col md={10} lg={6} key={certificate.id}>
                            <div className="certificate">
                                {/*<div className="d-flex">*/}
                                {/*    <div className="name">Issue Date</div>*/}
                                {/*    <div className="ml-4">*/}
                                {/*        <TimeStampToDate item={certificate.issueDate}/>*/}
                                {/*    </div>*/}
                                {/*</div>*/}
                                <div className="d-flex ">
                                    <div className="name">Expire Date:</div>
                                    <div className=" ml-4">
                                        <TimeStampToDate item={certificate.expireDate}/>
                                    </div>
                                </div>
                                <div className="d-flex mt-3">
                                    <div className="name pt-1">Status:</div>
                                    <div className="ml-4">
                                        <Input placeholder="Enter status"
                                               value={certificate.statusEnum} readOnly={true}/></div>
                                </div>
                                <div className="d-flex mt-3">
                                    <div className="name pt-1">State:</div>
                                    <div className="ml-4">
                                        <Input placeholder="Enter state"
                                               value={states !== null ? states.filter(item => item.id === certificate.stateDto.id)[0].name : ''}
                                               readOnly={true}/>
                                    </div>
                                </div>
                                <div className="change-status">
                                    <Button className="mr-3"
                                            onClick={() => openPhoto(certificate.attachmentId)}>Document</Button>
                                    {/*<Button*/}
                                    {/*    onClick={() => openCertificateModal(certificate, true)}>Edit</Button>*/}
                                </div>
                            </div>
                        </Col>
                    ) : ''}
                </div>
                <CertificateModal
                    dispatch={dispatch}
                    headerText={isCertificate ? "Certificate" : "Certificate"}
                    submit={addCertificate}
                    attachmentIdsArray={attachmentIdsArray}
                    states={states}
                    data={currentItem}
                    isAdmin={false}
                    isCertificate={isCertificate}
                    showCertificateModal={showCertificateModal}
                    stausEnums={statusEnums}
                    cancel={() => openCertificateModal(null)}
                />
                <PhotoModal
                    cancel={() => openPhoto(null)}
                    showModal={this.state.activeModal}
                    id={this.state.item}
                />
            </AgentLayout>
        );
    }
}


export default connect(({
                            auth: {currentUser}, agent: {certificates, showCertificateModal, isCertificate}, attachment: {attachmentIdsArray}, app: {currentItem, states}
                        }) => ({
    currentUser, certificates, showCertificateModal, attachmentIdsArray, currentItem, isCertificate, states
}))(Certificates);