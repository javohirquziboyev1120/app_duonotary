import React, {Component} from 'react';
import AgentLayout from "../../components/AgentLayout";
import {Col, Row} from "reactstrap";
import {
    EmailIcon,
    EmailShareButton,
    FacebookIcon,
    FacebookShareButton, LinkedinIcon, LinkedinShareButton, PinterestIcon, PinterestShareButton,
    TwitterIcon,
    TwitterShareButton,
    WhatsappIcon,
    WhatsappShareButton
} from "react-share";
import Moment from "moment";
import {getShareUserQrCode} from "../../redux/actions/SharingAction";
import {getUserSharingDiscount} from "../../redux/actions/AuthActions";
import {connect} from "react-redux";
import {getSharingDiscountList} from "../../redux/actions/AppAction";
import {config} from "../../utils/config";


class Sharing extends Component {
    componentDidMount() {
        this.props.dispatch(getShareUserQrCode());
        this.props.dispatch(getUserSharingDiscount())

    }

    constructor(props) {
        super(props);
        this.state = {showHistory: false};
    }

    toggleShowHistory = () => {
        if (!this.state.showHistory) {
            let count = this.props.size > 0 ? this.props.size : 10
            this.props.dispatch(getSharingDiscountList({page: this.props.page, size: count}));
        }
        this.setState({
            showHistory: !this.state.showHistory
        });
    }

    render() {
        const {
            dispatch, photoUrl, currentUser, page, size, userSharingDiscountList, userSharingDiscount
        } = this.props;
        const getUserSharingDiscountListMore = () => {
            let count = size + 10
            dispatch(getSharingDiscountList({page, size: count}));
        };
        return (
            <AgentLayout pathname={this.props.location.pathname}>
                <div className="agent-sharing-page container">
                    <div className='historyBtn'>
                        <div onClick={this.toggleShowHistory}
                             className={this.state.showHistory ? "mx-auto flex-row sharing-history sharing-history-pressed" : "mx-auto flex-row sharing-history"}>
                            <div className="icon icon-time"/>
                            <div className="history">History</div>
                        </div>
                    </div>
                    {
                        this.state.showHistory ?
                            <div>
                                {userSharingDiscountList && userSharingDiscountList.length > 0 ?
                                    <Row className="sharing-history-row">
                                        {userSharingDiscountList.map((item, i) =>
                                            <Col key={i} md={12} className="history-col my-1">
                                                <Row>
                                                    <Col md={2}>
                                                        <div className="fs-15">Clients</div>
                                                        <div className="fs-20-bold">
                                                            {item && item.client ? item.client.lastName + " " + item.client.firstName : ""}
                                                        </div>
                                                    </Col>
                                                    <Col md={1}>
                                                        <div className="border-line"/>
                                                    </Col>
                                                    <Col md={2}>
                                                        <div className="fs-15">Data, time of order</div>
                                                        <div className="fs-15-bold pt-2">
                                                            <span>{Moment(item.createdAt).format('lll')}</span>
                                                        </div>
                                                    </Col>
                                                    <Col md={1}>
                                                        <div className="border-line"/>
                                                    </Col>
                                                    <Col md={2}>
                                                        <div className="fs-15">Service type</div>
                                                        <div className="fs-15-bold pt-2">
                                                            {item.order && item.order.servicePrice && item.order.servicePrice.service && item.order.servicePrice.service.subService ? item.order.servicePrice.service.subService.name : "Error"}
                                                        </div>
                                                    </Col>
                                                    <Col md={1}>
                                                        <div className="border-line"/>
                                                    </Col>
                                                    <Col md={2}>
                                                        <div className="fs-15">Status</div>
                                                        <div className="fs-15-bold pt-2">
                                                            Bonus $({item.amount.toFixed(2)})
                                                        </div>
                                                    </Col>
                                                    <Col md={1}>
                                                        <img className="right-icons" src="/assets/icons/right.png"
                                                             alt=""/>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        )}
                                        {userSharingDiscountList.totalElements > size ?
                                            <button className="btn btn-primary px-5 py-2"
                                                    onClick={getUserSharingDiscountListMore}>More
                                            </button>
                                            : ""}
                                    </Row>
                                    : <h1 className="text-center">No information</h1>
                                }
                            </div>
                            :
                            <div className="wrap row">
                                <Col lg={4} sm={12} md={6}>
                                    <div className="bonus-card">
                                        <Row>
                                            <Col md={6}>
                                                <div className="bonus-text">
                                                    My bonus
                                                    points
                                                </div>
                                            </Col>
                                        </Row>
                                        <Row className="mt-4">
                                            <Col md={6} className="ml-auto">
                                                <div className="percent">
                                                    ${userSharingDiscount ? userSharingDiscount.toFixed(2) : "0"}
                                                </div>
                                                <div className="percent-text">
                                                    Your bonus amount
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                    <a href="#" className="text-decoration-none text-dark">
                                        <div className="use-now mb-2">
                                            Use now
                                        </div>
                                    </a>
                                </Col>
                                <Col lg={4} sm={12} md={6}>
                                    <div className="use-notary">
                                        <div className="title mb-3">
                                            I use Notary
                                        </div>
                                        <div className="commit">
                                                <span>Share your unique invitation link
                                                and get <span
                                                        className="percent-inside-text">10$</span> for each user</span>
                                        </div>
                                        <div className="bordered"/>
                                        {currentUser && currentUser.id ?
                                            <div className="d-flex social-set row">
                                                <div className="col-8">
                                                    <FacebookShareButton
                                                        url={config.SHARING_URL + "/register/" + currentUser.id}
                                                        quote="Duonotary - online & in-person notary service"
                                                        hashtag="#duonotary"
                                                        className="btn btn-primary ">
                                                        <FacebookIcon size={36} round={true}/>
                                                    </FacebookShareButton>
                                                    <TwitterShareButton
                                                        title="Duonotary - online & in-person notary service!"
                                                        url={config.SHARING_URL + "/register/" + currentUser.id}
                                                        hashtags={["duonotary", "notary", "service"]}
                                                        className="btn btn-primary">
                                                        <TwitterIcon size={36} round={true}/>
                                                    </TwitterShareButton>
                                                    <WhatsappShareButton
                                                        url={config.SHARING_URL + "/register/" + currentUser.id}
                                                        title={"Duonotary - online & in-person notary service"}
                                                        separator=":: "
                                                        className="btn btn-primary ">
                                                        <WhatsappIcon size={36} round={true}/>
                                                    </WhatsappShareButton>
                                                    <br/>
                                                    <LinkedinShareButton
                                                        url={config.SHARING_URL + "/register/" + currentUser.id}
                                                        title={"Duonotary - online & in-person notary service"}
                                                        summary="Hi! Make a notary online and in-person through Duonotary!"
                                                        source="Duonotary.com"
                                                        className="btn btn-primary ">
                                                        <LinkedinIcon size={36} round={true}/>
                                                    </LinkedinShareButton>
                                                    <EmailShareButton
                                                        body={"Hi! Make a notary online and in-person through Duonotary! http://70.32.24.165/register/" + currentUser.id}
                                                        separator=":: "
                                                        subject={"Duonotary - online & in-person notary service"}
                                                        className="btn btn-primary ">
                                                        <EmailIcon size={36} round={true}/>
                                                    </EmailShareButton>
                                                    <PinterestShareButton
                                                        url={config.SHARING_URL + "/register/" + currentUser.id}
                                                        quote={"Duonotary - online & in-person notary service"}
                                                        description={"Duonotary - online & in-person notary service"}
                                                        media="data:image/png;base64,"
                                                        className="btn btn-primary ">
                                                        <PinterestIcon size={36} round={true}/>
                                                    </PinterestShareButton>
                                                </div>
                                                <div className="col-4">
                                                    {photoUrl ?
                                                        <img src={"data:image/png;base64," + photoUrl}
                                                             className="w-100"
                                                             alt=""/>
                                                        : ""}
                                                </div>
                                            </div>
                                            : ""}
                                    </div>
                                </Col>
                            </div>
                    }
                    <hr/>
                </div>
            </AgentLayout>
        );
    }
}

export default connect(({
                            app: {userSharingDiscountList, page, size},
                            share: {photoUrl},
                            auth: {currentUser, userSharingDiscount}
                        }) => ({
        userSharingDiscountList, page, size,
        photoUrl,
        currentUser, userSharingDiscount
    })
)(Sharing)