import React, {Component} from 'react';
import {connect} from "react-redux";
import {editAdminPermission, getAdminList, getCurrentUser, getPermissionList} from "../redux/actions/AdminAction";
import {Button, Table, Modal, ModalBody, ModalFooter, ModalHeader, CustomInput} from "reactstrap";


class Permission extends Component {
    componentDidMount() {
        this.props.dispatch(getCurrentUser());
        this.props.dispatch(getAdminList());
        this.props.dispatch(getPermissionList());
    }


    render() {
        const {dispatch, loading, currentUser, active, currentItem, showModal, admins, permissions, permissionsId} = this.props;

        const savePermission = () => {
            let userDto = {};
            userDto.id = currentItem.id;
            userDto.phoneNumber = currentItem.phoneNumber;
            userDto.permissionsId = permissionsId;
            this.props.dispatch(editAdminPermission(userDto));
        }
        const openModal = (currentAdmin) => {
            let permissionsList = currentAdmin && currentAdmin.permissions.map(item => item.id);
            dispatch({
                type: 'updateState',
                payload: {
                    showModal: !showModal,
                    currentItem: currentAdmin,
                    permissionsId: permissionsList
                }
            })
        };
        const changePermission = (id) => {
            let permissionIdList = permissionsId;
            if (permissionIdList.includes(id)) {
                permissionIdList = permissionIdList.filter(item => item !== id)
            } else {
                permissionIdList = [...permissionIdList, id];
            }
            dispatch({
                type: 'updateState',
                payload: {
                    permissionsId: permissionIdList
                }
            })
        }
        return (

            <div>
                <h2 className="text-center">Permission</h2>
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Operation</th>
                    </tr>
                    </thead>
                    {admins != null ?
                        <tbody>
                        {admins.map((item, i) =>
                            <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{item.firstName + " " + item.lastName}</td>
                                <td>
                                    <Button outline color="warning" onClick={() => openModal(item)}>Permission</Button>
                                </td>
                            </tr>
                        )}
                        </tbody>
                        : ''}
                </Table>

                <Modal isOpen={showModal}>
                    <ModalHeader toggle={() => openModal(null)} charCode="x">Permission</ModalHeader>
                    <ModalBody>
                        {permissions != null ?
                            <div className="m-2">
                                {permissions.map((item, i) =>
                                    <div key={i}>
                                        <CustomInput type="checkbox"
                                                     checked={permissionsId && permissionsId
                                                         .some(permission => permission === item.id)}
                                                     onChange={() => changePermission(item.id)}
                                                     label={item.perName}
                                                     id={item.id}/>
                                    </div>
                                )}
                            </div>
                            : 'Error! Please, try again!'}
                    </ModalBody>
                    <ModalFooter>
                        <Button onClick={() => openModal(null)} color="primary">No</Button>
                        <Button type="button" outline onClick={savePermission}
                                color="secondary">Yes</Button>
                    </ModalFooter>
                </Modal>

            </div>
        );
    }
}

Permission.propTypes = {};

export default connect(({admin: {loading, currentUser, admins, active, showModal, currentItem, permissions, permissionsId}}) => ({
    loading,
    currentUser,
    admins,
    active,
    showModal,
    currentItem,
    permissions,
    permissionsId
}))(Permission)
