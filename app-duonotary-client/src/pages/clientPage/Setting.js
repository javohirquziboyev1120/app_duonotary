import React, {Component} from 'react';
import MainLayout from "../../components/MainLayout";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {Button, Collapse} from "reactstrap";
import {editPasswordAction} from "../../redux/actions/AuthActions";
import {connect} from "react-redux";
import {toast} from "react-toastify";
import {uploadImg} from "../../redux/actions/AttachmentAction";
import {editProfile} from "../../redux/actions/AppAction";
import {config} from "../../utils/config";
import UploadFile from "../../components/UploadFile";

class Setting extends Component {

    constructor(props) {
        super(props);
        this.state = {
            editProfile: false,
            editPassword: false,
            file: null,
        }
        this.handleChange = this.handleChange.bind(this)
    }

    toggleEditProfile = () => {
        this.setState({
            editProfile: !this.state.editProfile
        })
    }
    toggleEditPassword = () => {
        this.setState({
            editPassword: !this.state.editPassword
        })
    }

    handleChange(e) {
        let image = e.target.files[0];
        if (image.size < 3072000) {
            if (image.type === "image/jpeg" || image.type === "image/png" ||
                image.type === "image/svg+xml" || image.type === "image/jpg") {
                this.setState({
                    file: URL.createObjectURL(image)
                });
                this.props.dispatch(uploadImg(image))
            } else {
                toast.error("File format must be jpeg, png or svg. Format your chosen  " + image.type)
            }
        } else {
            toast.error("Size file must be small 3 MB")
        }
    }


    render() {
        const {currentUser, attachmentId} = this.props;

        const editPassword = (e, v) => {
            if (v.password !== v.prePassword) {
                toast.error("Passwords are not matches")
                return "";
            }
            this.setState({
                editPassword: !this.state.editPassword
            })
            this.props.dispatch(editPasswordAction(v))
        }

        const editProfileClient = (e, v) => {
            v.photoId = attachmentId;
            this.props.dispatch(editProfile(v))
            this.setState({
                editProfile: !this.state.editProfile
            })
        }
        return (
            <MainLayout pathname={this.props.location.pathname}>
                <div className="container client-setting-page">

                    <div className="flex-column edit-profile-section">
                        <AvForm
                            className={this.state.editProfile ? "flex-column form-section form-padding" : "flex-column form-section"}
                            onValidSubmit={editProfileClient}>
                            <div>
                                <div className="flex-row justify-content-between">
                                        <span className="form-section-title align-self-center margin-right-15">
                                            Edit profile
                                        </span>
                                    <EditIcon onClick={this.toggleEditProfile}/>
                                </div>
                                <Collapse isOpen={this.state.editProfile}>
                                    <div className="flex-row justify-content-end margin-top-15">
                                        {currentUser && currentUser.photo ? <img
                                                className="form-label avatar-img"
                                                src={config.BASE_URL + "/attachment/" + currentUser.photo.id}
                                                width="100px"
                                                alt=""
                                            /> :
                                            this.state.file ?
                                                <img
                                                    className="form-label avatar-img"
                                                    src={this.state.file}
                                                    width="100px"
                                                    alt=""
                                                />
                                                : <img
                                                    className="form-label avatar-img"
                                                    src="/assets/img/avatar.png"
                                                    width="100px"
                                                    alt=""
                                                />

                                        }

                                        <div className="input-full">

                                            <UploadFile

                                                name="avatarId"

                                                onChange={this.handleChange}
                                            />
                                        </div>
                                    </div>

                                    <div className="flex-row margin-top-15 justify-content-end">
                                        <span className="form-label">Full name</span>
                                        <div className="flex-row input-full  space-between align-items-start">
                                            <div className={"input-half"}>
                                                <AvField
                                                    name="firstName"
                                                    placeholder="First Name"
                                                    required
                                                    errorMessage=" "
                                                    defaultValue={currentUser.firstName}
                                                />
                                            </div>
                                            <div className={"input-half"}>
                                                <AvField
                                                    name="lastName"
                                                    placeholder="First Name"
                                                    errorMessage=" "
                                                    required
                                                    defaultValue={currentUser.lastName}
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="flex-row margin-top-15 justify-content-end">
                                        <span className="form-label">Phone Number*</span>
                                        <div className="input-full">
                                            <AvField
                                                name="phoneNumber"
                                                placeholder="Enter Phone Number"
                                                errorMessage=" "
                                                required
                                                defaultValue={currentUser.phoneNumber}
                                            />
                                        </div>
                                    </div>
                                    <div className="flex-row margin-top-15 justify-content-end">
                                        <span className="form-label">Email*</span>
                                        <div className="input-full">
                                            <AvField
                                                errorMessage=" "
                                                name="email"
                                                type="email"
                                                placeholder="Enter Email"
                                                required
                                                defaultValue={currentUser.email}
                                            />
                                        </div>
                                    </div>
                                    {/*<div className="flex-row margin-top-15 justify-content-end">*/}
                                    {/*    <span className="form-label">Address</span>*/}
                                    {/*    <div className="input-full">*/}
                                    {/*        <AvField*/}
                                    {/*            errorMessage=" "*/}
                                    {/*            name="address"*/}
                                    {/*            type="text"*/}
                                    {/*            placeholder="Enter Address"*/}
                                    {/*        />*/}
                                    {/*    </div>*/}
                                    {/*</div>*/}

                                    <div className="flex-row justify-content-end  margin-top-25">
                                        <Button
                                            color="dark"
                                            onClick={this.toggleEditProfile}
                                        >
                                            Cancel
                                        </Button>
                                        <Button
                                            type="submit"
                                            className="margin-left-15"
                                            color="primary"
                                        >
                                            Save
                                        </Button>

                                    </div>
                                </Collapse>
                            </div>
                        </AvForm>


                    </div>
                    <div className="flex-column edit-profile-section">


                        <AvForm
                            className={this.state.editPassword ? "flex-column form-section form-padding" : "flex-column form-section"}
                            onValidSubmit={editPassword}>
                            <div
                            >
                                <div className="flex-row justify-content-between">
                                        <span className="form-section-title align-self-center margin-right-15">
                                            Edit Password
                                        </span>
                                    <EditIcon onClick={this.toggleEditPassword}/>
                                </div>
                                <Collapse isOpen={this.state.editPassword}>
                                    <div className="flex-row margin-top-15 justify-content-end">
                                        <span className="form-label">Old Password*</span>
                                        <div className="input-full">
                                            <AvField
                                                name="oldPassword"
                                                placeholder="Enter old password"
                                                required
                                                errorMessage=" "
                                            />
                                        </div>
                                    </div>
                                    <div className="flex-row margin-top-15 justify-content-end">
                                        <span className="form-label">New Password*</span>
                                        <div className="input-full">
                                            <AvField
                                                name="password"
                                                placeholder="Enter new password"
                                                errorMessage=" "
                                                required
                                            />
                                        </div>
                                    </div>
                                    <div className="flex-row margin-top-15 justify-content-end">
                                        <span className="form-label">Pre Password*</span>
                                        <div className="input-full">
                                            <AvField
                                                errorMessage=" "
                                                name="prePassword"
                                                placeholder="Enter pre password"
                                                required
                                            />
                                        </div>
                                    </div>
                                    <div className="flex-row justify-content-end  margin-top-25">
                                        <Button
                                            color="dark"
                                            onClick={this.toggleEditPassword}
                                        >
                                            Cancel
                                        </Button>
                                        <Button
                                            type="submit"
                                            className="margin-left-15"
                                            color="primary"
                                        >
                                            Save
                                        </Button>

                                    </div>
                                </Collapse>
                            </div>
                        </AvForm>


                    </div>


                </div>
            </MainLayout>
        );
    }
}

export default connect(({auth: {currentUser}, attachment: {attachmentId}}) => ({currentUser, attachmentId}))(Setting);


function EditIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className} id={props.id}>
            <svg
                style={{cursor: "pointer"}}
                width="18"
                height="18"
                viewBox="0 0 18 18"
                fill="#313E47"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M12.728 6.68599L11.314 5.27199L2 14.586V16H3.414L12.728 6.68599ZM14.142 5.27199L15.556 3.85799L14.142 2.44399L12.728 3.85799L14.142 5.27199ZM4.242 18H0V13.757L13.435 0.321992C13.6225 0.134521 13.8768 0.0292053 14.142 0.0292053C14.4072 0.0292053 14.6615 0.134521 14.849 0.321992L17.678 3.15099C17.8655 3.33852 17.9708 3.59283 17.9708 3.85799C17.9708 4.12316 17.8655 4.37746 17.678 4.56499L4.243 18H4.242Z"/>
            </svg>
        </div>
    );
}
