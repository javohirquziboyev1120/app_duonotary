import React, {Component} from "react";
import {Col, Row} from "reactstrap";
import {connect} from "react-redux";
import MainLayout from "../../components/MainLayout";
import {
    EmailIcon,
    EmailShareButton,
    FacebookIcon,
    FacebookShareButton,
    LinkedinIcon,
    LinkedinShareButton,
    PinterestIcon,
    PinterestShareButton,
    TwitterIcon,
    TwitterShareButton,
    WhatsappIcon,
    WhatsappShareButton,
} from "react-share";
import {
    getUserSharingDiscount,
} from "../../redux/actions/AuthActions";
import {getShareUserQrCode} from "../../redux/actions/SharingAction";
import Moment from "moment";
import {getSharingDiscountList} from "../../redux/actions/AppAction";
import {Link} from "react-router-dom";
import {config} from "../../utils/config";
import * as discountActions from "../../redux/actions/DiscountActions";


class Sharing extends Component {
    componentDidMount() {
        this.props.dispatch(discountActions.getSharingDiscountTariff())
        this.props.dispatch(getShareUserQrCode());
        this.props.dispatch(getUserSharingDiscount());
    }

    render() {
        const {
            dispatch,
            photoUrl,
            userSharingDiscount,
            userSharingDiscountList,
            currentUser,
            page,
            size,
            historTime,
            currentSharingDiscountTariff
        } = this.props;
        const getUserSharingDiscountListMore = () => {
            if (historTime === true) {
                let count = size + 10;
                dispatch(getSharingDiscountList({page, size: count}));
            }
        };
        return (
            <MainLayout pathname={this.props.location.pathname}>
                <div className="client-sharing-page container">
                    <div className="container">
                        {
                            !historTime ?
                                <div className="row">
                                    <Col lg={4} md={6} sm={12}>
                                        <div className="bonus-card">
                                            <Row>
                                                <Col md={12}>
                                                    <div className="bonus-text">
                                                        My bonus
                                                        points
                                                    </div>
                                                </Col>
                                            </Row>
                                            <Row className="mt-4">
                                                <Col md={12} className="ml-auto">
                                                    <div className="percent ml-auto">
                                                        ${userSharingDiscount ? userSharingDiscount.toFixed(2) : "0"}
                                                    </div>
                                                    <div className="percent-text">
                                                        Your bonus amount
                                                    </div>
                                                </Col>
                                            </Row>
                                        </div>
                                        <Link to="/client/online" className="text-decoration-none text-dark">
                                            <div className="use-now">
                                                Use now
                                            </div>
                                        </Link>
                                    </Col>
                                    <Col lg={4} md={6} sm={12}>
                                        <div className="use-notary">
                                            <div className="title">
                                                I use Notary
                                            </div>
                                            <div className="commit">
                                                Share your unique invitation link and get
                                                {currentSharingDiscountTariff && currentSharingDiscountTariff.length > 1 ?
                                                    <>
                                                        {currentSharingDiscountTariff[0].online ? " " + currentSharingDiscountTariff[0].percent + "%(for online) " : currentSharingDiscountTariff[0].minPercent ? " " + currentSharingDiscountTariff[0].minPercent + "%(for in-person) " : ""}
                                                        {currentSharingDiscountTariff[1].online ? " or " + currentSharingDiscountTariff[1].percent + "%(for online) " : currentSharingDiscountTariff[1].minPercent ? " or " + currentSharingDiscountTariff[1].minPercent + "%(for in-person) " : ""}
                                                    </>
                                                    : "--%"}
                                                for each user
                                            </div>
                                            <div className="bordered"/>
                                            {currentUser && currentUser.id ?
                                                <div className="d-flex social-set row">
                                                    <div className="col-6">
                                                        <FacebookShareButton
                                                            url={config.SHARING_URL + "/register/" + currentUser.id}
                                                            quote="Duonotary - online & in-person notary service"
                                                            hashtag="#duonotary"
                                                            className="btn btn-primary ">
                                                            <FacebookIcon size={36} round={true}/>
                                                        </FacebookShareButton>
                                                        <TwitterShareButton
                                                            title="Duonotary - online & in-person notary service!"
                                                            url={config.SHARING_URL + "/register/" + currentUser.id}
                                                            hashtags={["duonotary", "notary", "service"]}
                                                            className="btn btn-primary">
                                                            <TwitterIcon size={36} round={true}/>
                                                        </TwitterShareButton>
                                                        <WhatsappShareButton
                                                            url={config.SHARING_URL + "/register/" + currentUser.id}
                                                            title={"Duonotary - online & in-person notary service"}
                                                            separator=":: "
                                                            className="btn btn-primary ">
                                                            <WhatsappIcon size={36} round={true}/>
                                                        </WhatsappShareButton>
                                                        <br/>
                                                        <LinkedinShareButton
                                                            url={config.SHARING_URL + "/register/" + currentUser.id}
                                                            title={"Duonotary - online & in-person notary service"}
                                                            summary="Hi! Make a notary online and in-person through Duonotary!"
                                                            source="Duonotary.com"
                                                            className="btn btn-primary ">
                                                            <LinkedinIcon size={36} round={true}/>
                                                        </LinkedinShareButton>
                                                        <EmailShareButton
                                                            body={"Hi! Make a notary online and in-person through Duonotary! http://70.32.24.165/register/" + currentUser.id}
                                                            separator=":: "
                                                            subject={"Duonotary - online & in-person notary service"}
                                                            className="btn btn-primary ">
                                                            <EmailIcon size={36} round={true}/>
                                                        </EmailShareButton>
                                                        <PinterestShareButton
                                                            url={config.SHARING_URL + "/register/" + currentUser.id}
                                                            quote={"Duonotary - online & in-person notary service"}
                                                            description={"Duonotary - online & in-person notary service"}
                                                            media={"data:image/png;base64," + photoUrl}
                                                            className="btn btn-primary ">
                                                            <PinterestIcon size={36} round={true}/>
                                                        </PinterestShareButton>
                                                    </div>
                                                    <div className="col-5">
                                                        {photoUrl ?
                                                            <img src={"data:image/png;base64," + photoUrl}
                                                                 className="w-100"
                                                                 alt=""/>
                                                            : ""}
                                                    </div>
                                                </div>

                                                : ""}
                                        </div>

                                    </Col>
                                </div>
                                :
                                <div>
                                    {userSharingDiscountList &&
                                    userSharingDiscountList.length > 0 ? (
                                        <Row className="sharing-history-row">
                                            {userSharingDiscountList.map(
                                                (item, i) => (
                                                    <Col
                                                        key={i}
                                                        md={12}
                                                        className="history-col mt-2"
                                                    >
                                                        <Row>
                                                            <Col md={2}>
                                                                <div className="fs-15">
                                                                    Clients
                                                                </div>
                                                                <div className="fs-20-bold">
                                                                    {item.client.lastName +
                                                                    " " +
                                                                    item.client.firstName}
                                                                </div>
                                                            </Col>
                                                            <Col md={1}>
                                                                <div className="border-line"/>
                                                            </Col>
                                                            <Col md={2}>
                                                                <div className="fs-15">
                                                                    Data, time of
                                                                    order
                                                                </div>
                                                                <div className="fs-15-bold pt-2">
                                                                        <span>
                                                                            {Moment(
                                                                                item.createdAt
                                                                            ).format(
                                                                                "lll"
                                                                            )}
                                                                        </span>
                                                                </div>
                                                            </Col>
                                                            <Col md={2}>
                                                                <div className="fs-15">
                                                                    Service type
                                                                </div>
                                                                <div className="fs-15-bold pt-2">
                                                                    {item.order &&
                                                                    item.order
                                                                        .servicePrice &&
                                                                    item.order
                                                                        .servicePrice
                                                                        .service &&
                                                                    item.order
                                                                        .servicePrice
                                                                        .service
                                                                        .subService
                                                                        ? item.order
                                                                            .servicePrice
                                                                            .service
                                                                            .subService
                                                                            .name
                                                                        : "Error"}
                                                                </div>
                                                            </Col>
                                                            <Col md={1}>
                                                                <div className="border-line"/>
                                                            </Col>
                                                            <Col md={1}>
                                                                <div className="fs-15">
                                                                    Amount
                                                                </div>
                                                                <div className="fs-15-bold pt-2">
                                                                    ${item.amount.toFixed(1)}
                                                                </div>
                                                            </Col>

                                                            <Col md={1}>
                                                                <div className="fs-15">
                                                                    Percent
                                                                </div>
                                                                <div className="fs-15-bold pt-2">
                                                                    {item.percent.toFixed(1)}%
                                                                </div>
                                                            </Col>
                                                            <Col md={1}>
                                                                <div className="fs-15">
                                                                    Leftover
                                                                </div>
                                                                <div className="fs-15-bold pt-2">
                                                                    ${item.leftover.toFixed(2)}
                                                                </div>
                                                            </Col>
                                                            <Col md={1}>
                                                                <img
                                                                    className="right-icons"
                                                                    src="/assets/icons/right.png"
                                                                    alt=""
                                                                />
                                                            </Col>
                                                        </Row>
                                                    </Col>
                                                )
                                            )}
                                            {userSharingDiscountList.totalElements >
                                            size ? (
                                                <button
                                                    className="btn btn-primary px-5 py-2"
                                                    onClick={
                                                        getUserSharingDiscountListMore
                                                    }
                                                >
                                                    More
                                                </button>
                                            ) : (
                                                ""
                                            )}
                                        </Row>
                                    ) : (
                                        <h1 className="text-center">
                                            No information
                                        </h1>
                                    )}
                                </div>
                        }
                    </div>
                </div>
            </MainLayout>
        );
    }
}


export default connect(
    ({
         app: {historTime, userSharingDiscountList, page, size},
         share: {photoUrl},
         discount: {currentSharingDiscountTariff},
         auth: {currentUser, isUser, userSharingDiscount},
     }) => ({
        currentSharingDiscountTariff,
        photoUrl,
        historTime,
        userSharingDiscount,
        currentUser,
        userSharingDiscountList,
        page,
        size,
    })
)(Sharing);
