import React, {Component} from "react";
import {Button, Col, Input, Modal, ModalBody, ModalFooter, ModalHeader, Row,} from "reactstrap";
import {connect} from "react-redux";
import {
    getAllByUserId,
    getAmountForCancelOrder,
    getCertificateByOrder,
    getOrderList, getOrdersBySearchForClient,
    orderCancel,
} from "../../redux/actions/OrderAction";
import {config} from "../../utils/config";
import MainLayout from "../../components/MainLayout";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {
    addFeedBackAction,
    getCountryList,
    getDocumentTypeList,
    getEmbassy,
    getStateList
} from "../../redux/actions/AppAction";
import {FEED_BACK_ALLOW} from "../../utils/constants";
import PaginationComponent from "react-reactstrap-pagination";
import RealEstateComponent from "../../components/RealEstateComponent";
import International from "../../components/International";
import OrderDocuments from "../../components/Modal/OrderDocuments";
import EditOrder from "../../components/order/EditOrder";
import ServiceDetails from "../../components/ServiceDetails";
import CertificateViewModal from "../../components/Modal/CertificateViewModal";
import CloseBtn from "../../components/CloseBtn";
import PhotoModal from "../../components/Modal/PhotoModal";


class Order extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inProgress: false,
            cancelModal: false,
            documentModal: false,
            photo:'',
            photoModal:false
        };
    }

    componentDidMount() {
        this.props.dispatch(getAllByUserId({
            userId: this.props.currentUser.id ? this.props.currentUser.id : '4bd50726-075a-4ce1-bef7-2c1461e7db37',
            param: {page: this.props.page, size: this.props.size}
        }));
        this.props.dispatch(getCountryList())
        this.props.dispatch(getStateList())
        this.props.dispatch(getEmbassy())
        this.props.dispatch(getDocumentTypeList())
        this.props.dispatch({
            type: 'resetState'
        })

    }

    changeState(obj) {
        if (obj.documentModal === false) this.props.dispatch(
            getAllByUserId({
                userId: this.props.currentUser.id,
                param: {page: this.props.page, size: this.props.size}
            })
        );
        this.setState({...obj})
        this.props.dispatch({type: 'updateState', payload: {documents: obj.documents}})
    }




    render() {
        let {
            showModal, documents, orders, dispatch, isAgent, isUser, currentItem, cancelAmount,
            showFeedBackModal, openEditModal, page, size, totalElements, countries, embassyCountries, documentTypes, filters
        } = this.props;
        const handleSelected = (selectedPage) => {
            let obj = {page: selectedPage - 1, size: size}
            let zipCode;
            let date;
            let search;
            if (filters) {
                search = filters.filter(item => item.key === 'search')[0]
                zipCode = filters.filter(item => item.key === 'zipCode')[0]
                date = filters.filter(item => item.key === 'date')[0]
                if (search) obj = {...obj, search: filters.filter(item => item.key === 'search')[0].value};
                if (zipCode && date) obj = {
                    ...obj,
                    zipCode: filters.filter(item => item.key === 'zipCode')[0].value,
                    date: filters.filter(item => item.key === 'date')[0].value
                };
                if (zipCode) obj = {...obj, zipCode: filters.filter(item => item.key === 'zipCode')[0].value};
                if (date) obj = {...obj, date: filters.filter(item => item.key === 'date')[0].value};
            }
            dispatch(search ? getOrdersBySearchForClient({param: obj}) : zipCode || date ? "" : getAllByUserId({userId: this.props.currentUser.id, param: obj}))
            dispatch(
                {
                    type: 'updateState',
                    payload: {
                        page: selectedPage - 1
                    }
                }
            )
        }

        const getTimer = (item, num) => {
            if (this.props.currentItem || num === 1) {
                let countDownDate = new Date(
                    item.timeTableDto.fromTime
                ).getTime();
                let x = setInterval(function () {
                    let now = new Date().getTime();
                    let distance = countDownDate - now;
                    let days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    let hours = Math.floor(
                        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
                    );
                    let minutes = Math.floor(
                        (distance % (1000 * 60 * 60)) / (1000 * 60)
                    );
                    let seconds = Math.floor((distance % (1000 * 60)) / 1000);
                    let p = document.getElementById(item.id + num);
                    if (p != null) {
                        let status = item.status;
                        if (status !== 'NEW' || distance < 0) {
                            p.style.letterSpacing = '1px'
                            p.style.color = status === 'CANCELLED' ? 'red' : status === 'COMPLETED' ? 'green' : status === 'IN_PROGRESS' || status === 'DRAFT' ? '#FFC30B' : ''
                            clearInterval(x);
                            p.innerHTML = getStatus(status)
                        } else if (days === 0) {
                            p.innerHTML =
                                "- " + hours + ":" + minutes + ":" + seconds;
                        } else {
                            p.innerHTML =
                                "- " +
                                days +
                                "d " +
                                hours +
                                ":" +
                                minutes +
                                ":" +
                                seconds;
                        }
                    }
                }, 100);
            }
        };
        const getStatus = (item) => {
            if (item === 'IN_PROGRESS') item = 'In-Progress';
            if (item === 'CANCELLED') item = 'Cancelled';
            if (item === 'DRAFT') item = 'Draft';
            if (item === 'CLOSED') item = 'Closed';
            if (item === 'REJECTED') item = 'Rejected';
            if (item === 'COMPLETED') item = 'Completed';
            if (item === 'NEW') item = 'New';
            return item;
        }

        let openModal = (bool, item) => {
            if (item != null) dispatch(getAmountForCancelOrder(item.id));
            dispatch({
                type: "updateState",
                payload: {showModal: bool, currentItem: item},
            });
            this.setState({
                cancelModal: this.state.cancelModal,
            });
        };
        let cancelOrder = () => {
            if (currentItem != null && currentItem.id != null)
                dispatch(orderCancel(currentItem.id));
        };
        let openCancelModal = () => {
            this.setState({
                cancelModal: !this.state.cancelModal,
            });
        };
        let openFeedBackModal = (boolean) => {
            dispatch({
                type: "updateState",
                payload: {showFeedBackModal: !showFeedBackModal},
            });
        };
        let addFeedBack = (e, v) => {
            v.orderDto = currentItem;
            v.agent = false;
            v.operationEnum = "INSERT";
            v.seen = false;
            dispatch(addFeedBackAction(v));
        };

        function canCancel(item) {
            let today = new Date();
            let fromTime = new Date(item);
            return fromTime > today;
        }


        let stateUpdater = (item) => {
            dispatch({
                type: 'updateOrderState',
                payload: item
            })
            dispatch({
                type: 'updateStateOrder',
                payload: item
            })
        }

        const clearFilter = () => {
            this.props.dispatch(getOrderList({page: 0, size: this.props.size}));
            dispatch({type: 'updateState', payload: {filters: '', search: ''}})
        }

        return (
            <>
                <MainLayout pathname={this.props.location.pathname}>
                    <div className="order-page-client">
                        <div className="container">
                            <Row>
                                <Col md={12}>
                                    {filters ?
                                        <Row>
                                            <Col><CloseIcon onClick={() => clearFilter(null, false, true)}/></Col>
                                            <Col md={12}>
                                                {filters.map(item =>
                                                    <Row className="bg-filter  p-2 m-3 rounded">
                                                        <Col md={12}>{item.key} : {item.value}</Col>
                                                    </Row>
                                                )}
                                            </Col>
                                        </Row>

                                        : ''}

                                </Col>
                                {isUser && orders
                                    ? orders.map((order, i) => (
                                        <Col key={order.id} md={12}>
                                            <div className="d-flex main-col justify-content-around">
                                                <div className="d-flex agent-name justify-content-between">
                                                    <div className="flex-row">
                                                        <div className="user-img">
                                                            <img
                                                                src={
                                                                    order.agent
                                                                        .photoId
                                                                        ? config.BASE_URL +
                                                                        "/attachment/" +
                                                                        order
                                                                            .agent
                                                                            .photoId
                                                                        : "/assets/avatar.png"
                                                                }
                                                                alt=""
                                                            />
                                                        </div>
                                                        <div className="userName pt-2">
                                                            <div className="name-title">
                                                                Agents
                                                            </div>
                                                            <div className="name">
                                                                {order.agent
                                                                    .lastName +
                                                                " " +
                                                                order.agent
                                                                    .firstName}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        onClick={() =>
                                                            openModal(true, order)
                                                        }
                                                        className="three-dots-vertical"
                                                    >
                                                        <div className="dot"/>
                                                        <div className="dot"/>
                                                        <div className="dot"/>
                                                    </div>
                                                </div>
                                                <div className="border-line"/>
                                                <div className="time-div pt-2">
                                                    <div>
                                                      <span className="pr-2 fs-15">
                                                          {
                                                              order.timeTableDto
                                                                  .day
                                                          }
                                                          -
                                                          {
                                                              order.timeTableDto
                                                                  .month
                                                          }
                                                          ,{" "}
                                                          {
                                                              order.timeTableDto
                                                                  .year
                                                          }
                                                      </span>
                                                        <span className="fs-15">
                                                          {
                                                              order.timeTableDto
                                                                  .hour
                                                          }
                                                            :
                                                            {
                                                                order.timeTableDto
                                                                    .min
                                                            }
                                                      </span>
                                                    </div>
                                                    <div className="fs-15-bold">
                                                        {order.servicePrice.online ? 'online' : order.address}
                                                    </div>
                                                </div>
                                                <div className="border-line"/>

                                                <div className="time-sum pt-2">
                                                    <div
                                                        className={
                                                            !this.state.inProgress
                                                                ? "time-minus "
                                                                : "fs-15-bold progress-color"
                                                        }
                                                    >
                                                        <p id={order.id + 1}>
                                                            {getTimer(order, 1)}
                                                        </p>
                                                    </div>
                                                    <div className="fs-20-bold">
                                                        {"$" + (order.amount).toFixed(2)}
                                                    </div>
                                                </div>
                                                <div className="border-line"/>

                                                <div className="service-type pt-2">
                                                    <div className="fs-15">
                                                        Service type
                                                    </div>
                                                    <div className="fs-15-bold pt-2">
                                                        <span>{order.servicePrice.serviceDto.subServiceDto.name} </span>
                                                    </div>
                                                </div>

                                                <div
                                                    onClick={() =>
                                                        openModal(true, order)
                                                    }
                                                    className="three-dots"
                                                >
                                                    <div className="dot"/>
                                                    <div className="dot"/>
                                                    <div className="dot"/>
                                                </div>
                                            </div>
                                        </Col>
                                    ))
                                    : ""}
                                <Col className="mt-3"> {totalElements > 0 ? <PaginationComponent
                                    defaultActivePage={page + 1}
                                    totalItems={totalElements} pageSize={size}
                                    onSelect={handleSelected}
                                /> : ''}</Col>
                            </Row>
                        </div>

                        {showModal && currentItem ? (
                            <div>
                                <div
                                    onClick={() => openModal(false, null)}
                                    className="blur-color"
                                />
                                <div className="edit-order">
                                    <div className='closeBtnBlock'>
                                        <CloseBtn click={() => openModal(false, null)}/>
                                    </div>
                                    <div className="container">
                                        <div className="flex-row align-items-center justify-content-between">
                                            <div>
                                                <div className="order-count">
                                                    Order -
                                                    <span> {currentItem.serialNumber}</span>
                                                </div>
                                            </div>
                                            <div>
                                                {currentItem.status === "NEW" ?
                                                    <div className="main-edit">
                                                        <div
                                                            hidden={!canCancel(currentItem.timeTableDto.fromTime)}
                                                            className="edit-icon"
                                                            onClick={() =>
                                                                stateUpdater({
                                                                    openEditModal: true,
                                                                    order: currentItem,
                                                                    update: 'schedule'
                                                                })
                                                            }
                                                        >
                                                            <img
                                                                src="/assets/icons/edit.png"
                                                                alt="edit-icon"
                                                            />
                                                        </div>
                                                    </div>
                                                    : ""}

                                            </div>
                                        </div>
                                        <Row className="mt-5">
                                            <Col md={12}>
                                                <div className="fs-15">Agents</div>
                                                <Input
                                                    value={
                                                        currentItem.agent.lastName +
                                                        " " +
                                                        currentItem.agent.firstName
                                                    }
                                                    readOnly={true}
                                                />
                                            </Col>
                                        </Row>
                                        <Row className="mt-4 pt-2">
                                            <Col md={6}>
                                                <div className="fs-15">
                                                    Date, time of order
                                                </div>
                                                <div className="fs-15-bold pt-1">
                                                <span className="pr-2">
                                                    {
                                                        currentItem.timeTableDto
                                                            .day
                                                    }
                                                    -
                                                    {
                                                        currentItem.timeTableDto
                                                            .month
                                                    }
                                                    ,{" "}
                                                    {
                                                        currentItem.timeTableDto
                                                            .year
                                                    }
                                                </span>
                                                    <span>
                                                    {
                                                        currentItem.timeTableDto
                                                            .hour
                                                    }
                                                        :
                                                        {
                                                            currentItem.timeTableDto
                                                                .min
                                                        }
                                                </span>
                                                </div>
                                            </Col>
                                            <Col md={6}>
                                                <div className="fs-15">
                                                    Service type
                                                </div>
                                                <div className="fs-15-bold pt-1">
                                                    <span>{currentItem.servicePrice.serviceDto.subServiceDto.name} </span>

                                                </div>
                                            </Col>
                                        </Row>
                                        <Row className="mt-4 pt-2">
                                            <Col md={12}>
                                                <div className="fs-15">
                                                    {currentItem.servicePrice.online
                                                        ? "Document type"
                                                        : "Location"}
                                                </div>
                                                <div className="fs-15-bold">
                                                    {currentItem.servicePrice.online
                                                        ? currentItem.titleDocument
                                                        : currentItem.address}
                                                </div>
                                            </Col>
                                        </Row>
                                        <Row className="mt-4 pt-2">
                                            <Col md={6} sm={6}>
                                                <div className="fs-15">Amount</div>
                                                <div className="fs-32-bold pt-1">
                                                    {"$" + (currentItem.amount).toFixed(2)}
                                                </div>
                                            </Col>
                                            <Col md={6} sm={6}>
                                                <div className="fs-15">Status</div>
                                                <div className="fs-20-bold minus-color pt-1">
                                                    <p id={currentItem.id + 2}>
                                                        {getTimer(currentItem, 2)}
                                                    </p>
                                                </div>
                                            </Col>
                                        </Row>
                                        {currentItem.servicePrice.online || currentItem.servicePrice.serviceDto.subServiceDto.serviceEnum === "REAL_ESTATE" ?
                                            <Col md={12} className='p-0'><Button className="btn-block my-3"
                                                                                 onClick={() => this.changeState({
                                                                                     documentModal: true,
                                                                                     documents: currentItem.documents
                                                                                 })} color="secondary">Show
                                                Documents</Button>
                                            </Col>
                                            : ''}
                                        {currentItem.servicePrice.serviceDto.subServiceDto.serviceEnum === "REAL_ESTATE" ?
                                            <RealEstateComponent
                                                data={currentItem.realEstateDto}
                                                dispatch={dispatch}
                                                isAdd={false}
                                                printDocumentCount={1}
                                                printDocumentPrice={1}
                                                servicePrice={currentItem.servicePrice}
                                                submit={""}
                                                isClient={true}
                                                class={true}
                                            /> : currentItem.servicePrice.serviceDto.subServiceDto.serviceEnum === "INTERNATIONAL" ?
                                                <International

                                                    isAdd={false}
                                                    isClient={true}
                                                    submit={""}
                                                    dispatch={dispatch}
                                                    data={currentItem.internationalDto}
                                                    isApostille={currentItem.internationalDto?!currentItem.internationalDto.embassy:false}
                                                    countries={countries}
                                                    documentTypes={documentTypes}
                                                    embassyCountries={embassyCountries}
                                                />
                                                : ""}
                                        <Row hidden={!canCancel(currentItem.timeTableDto.fromTime)}>
                                            <Col md={12}>
                                                {currentItem.status ===
                                                "CANCELLED" ? (
                                                    ""
                                                ) : (
                                                    <div className="cancel-sum">
                                                        For canceling this
                                                        transaction, you will be
                                                        charged{" "}
                                                        <span>${(cancelAmount).toFixed(2)}</span>
                                                    </div>
                                                )}
                                            </Col>
                                            {currentItem.orderAdditionalServiceDtoList.length > 0 ?
                                                <div className="mt-4">
                                                    <Row className=""><Col md={12} className="text-center font-weight-bold"
                                                                           style={{fontSize: '24px'}}>Additional services</Col></Row>
                                                    {currentItem.orderAdditionalServiceDtoList.map(oas =>
                                                        <ServiceDetails count={oas.count}
                                                                        name={oas.additionalServicePriceDto.additionalServiceDto.name}
                                                                        price={oas.currentPrice}/>
                                                    )}
                                                </div>
                                                : ''}

                                        </Row>
                                        <div>
                                            <Button
                                                hidden={canCancel(currentItem.timeTableDto.fromTime)}
                                                onClick={() => {
                                                    dispatch(getCertificateByOrder({
                                                        userId: currentItem.agent.id,
                                                        zipCode: currentItem.servicePrice.zipCodeDto.id
                                                    }))
                                                }}
                                                color="primary"
                                            > Agent Certificate
                                            </Button>
                                        </div>
                                        <div hidden={!canCancel(currentItem.timeTableDto.fromTime)}
                                             className="flex-row justify-content-between">
                                            {currentItem.status !== "COMPLETED" ?
                                                <Button hidden={!canCancel(currentItem.timeTableDto.fromTime)}
                                                        disabled={currentItem.status === "CANCELLED"}
                                                        onClick={openCancelModal}
                                                        className="cancel-btn">
                                                    {currentItem.status === "CANCELLED" ? "Orders was canceled" : "Cancel the order"}
                                                </Button> : ''}
                                        </div>
                                        {FEED_BACK_ALLOW.includes(currentItem.status) && (currentItem.feedbackStatus === 'AGENT' || currentItem.feedbackStatus === "NONE") ?
                                            <Button onClick={openFeedBackModal}
                                                    color={'primary'}
                                                    className='px-3 py-3 mt-2'>
                                                Close the order
                                            </Button> : ''}
                                        <Modal isOpen={this.state.cancelModal}>
                                            <ModalHeader>
                                                Are you sure to cancel?
                                            </ModalHeader>
                                            <ModalBody className="text-center">
                                                <Button color={'light'}
                                                        onClick={cancelOrder}
                                                        className="btn m-1"
                                                >
                                                    Yes
                                                </Button>
                                                <Button color={'primary'}
                                                        onClick={openCancelModal}
                                                        className="btn m-1"
                                                >
                                                    No
                                                </Button>
                                            </ModalBody>
                                        </Modal>
                                    </div>
                                </div>
                            </div>
                        ) : (
                            ""
                        )}
                        {showFeedBackModal ? (
                            <Modal
                                isOpen={showFeedBackModal}
                                toggle={() => openFeedBackModal(false)} id="allModalStyle"
                            >
                                <AvForm onValidSubmit={addFeedBack}>
                                    <ModalHeader>Feedback from Order </ModalHeader>
                                    <ModalBody>
                                        <AvField
                                            defaultValue="0"
                                            type="select"
                                            name="rate"
                                            label="Select order rate"
                                            required
                                        >
                                            <option value="5">5</option>
                                            <option value="4">4</option>
                                            <option value="3">3</option>
                                            <option value="2">2</option>
                                            <option value="1">1</option>
                                            <option value="0" disabled>Select rate</option>
                                        </AvField>
                                        <AvField
                                            name="description"
                                            placeholder="Enter your feedback"
                                            type="textarea" style={{height: '10rem'}}
                                        />
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button
                                            type="button"
                                            onClick={() => openFeedBackModal(false)}
                                        >
                                            Cancel
                                        </Button>
                                        <Button color="info">Send</Button>
                                    </ModalFooter>
                                </AvForm>
                            </Modal>
                        ) : (
                            ""
                        )}
                    </div>
                    {this.state.documentModal ?
                        <OrderDocuments
                            data={documents}
                            order={currentItem}
                            dispatch={dispatch}
                            cancel={(item) => this.changeState(item)}
                            showModal={this.state.documentModal}
                            isClient={true}
                            isAgent={false}
                        /> : ''
                    }
                    {this.props.certificate && <CertificateViewModal
                        certificate={this.props.certificate}
                        dispatch={dispatch}
                        openPhoto={(it)=>{
                            console.log(it);
                            this.setState({photo:it,photoModal:true})}
                        }
                        states={this.props.states}
                    />
                    }
                    {this.state.photo&&<PhotoModal
                    cancel={()=>this.setState({photoModal:false})}
                    showModal={this.state.photoModal}
                    id={this.state.photo}
                    />}
                </MainLayout>

                {openEditModal ? <EditOrder/> : ''}
            </>
        );
    }
}


export default connect(
    ({
         auth: {isUser, currentUser, isAgent},
         app: {states, showModal, currentItem, showFeedBackModal, page, size, totalElements, countries, embassyCountries, documentTypes, isApostille, filters},
         order: {certificate, orders, cancelAmount, times, error, timeZone, documents, orderState, openEditModal},
     }) => ({
        certificate, states,
        isUser, page, size, totalElements, countries, embassyCountries, documentTypes, isApostille,
        currentUser, openEditModal,
        showModal, documents,
        currentItem,
        orders,
        cancelAmount,
        showFeedBackModal,
        isAgent,
        times,
        error,
        timeZone, orderState, filters
    })
)(Order);

function CloseIcon(props) {
    return <div className={props.className}>
        <svg onClick={props.onClick} style={{cursor: 'pointer'}} width="50" height="50" viewBox="0 0 50 50" fill="none"
             xmlns="http://www.w3.org/2000/svg">
            <circle cx="25" cy="25" r="25" fill="white"/>
            <path
                d="M24.955 23.1875L31.1425 17L32.91 18.7675L26.7225 24.955L32.91 31.1425L31.1425 32.91L24.955 26.7225L18.7675 32.91L17 31.1425L23.1875 24.955L17 18.7675L18.7675 17L24.955 23.1875Z"
                fill="#313E47"/>
        </svg>
    </div>
}