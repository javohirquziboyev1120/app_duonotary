import React, {Component} from 'react';
import './clientPages.scss'
import MainLayout from "../../components/MainLayout";
import {Button, Col, Container, Input, Row} from "reactstrap";
import {
    addOrder,
    getCurrentFreeAgent,
    getOrderPriceAmounts,
    getPayTypes,
    getServiceToNow
} from "../../redux/actions/OrderAction";
import {connect} from "react-redux";
import {ORDER, TEMP_ID_TIME_TABLE, URL_ORDER} from "../../utils/constants";
import {v4 as uuidv4} from "uuid";
import {Link, withRouter} from "react-router-dom";
import LeftMenu from "../../components/LeftMenu";
import AlertModal from "../../components/AlertModal";
import Modall from '../../components/common/modal';
import DateTime from "../../components/order/DateTime";
import {UserInfo} from "../../components/order/UserInfo";
import LoaderMini from "../../components/LoaderMini";
import OrderInfoOnline from "../../components/order/OrderInfoOnline";

class OnlineOrder extends Component {

    state = {
        isOpen: false,
    }

    componentDidMount() {
        let order = JSON.parse(localStorage.getItem(ORDER));
        this.props.dispatch({
            type: 'updateStateOrder',
            payload: {
                alert_mes: "",
                ...order
            }
        })
        if ((order && order.stage > 0) || this.props.stage > 0) {
            this.props.dispatch({
                type: 'resetState',
            })
        }

        if (!order || !order.orderState)
            this.props.dispatch({
                type: 'updateOrderState',
                payload: ''
            })

        this.props.dispatch(getPayTypes());
    }

    handleChangeModal = () => {
        this.setState({isOpen: !this.state.isOpen,})
    };

    render() {
        const {
            alert_mes, allDiscounts, discountCount, discountNeed, amount, order, timesPercent,
            servicePrice, attachmentIdsArray, step, countDocument, selectedDate, error,
            times, time, timeTable, loading, currentUser, checkbox1, checkbox2, checkbox3, modal, dispatch,
            login, signUp, payTypes, payType, allAmount, orderAdditionalServices,
            pricingList, titleDocument, now, modalShow, history, update
        } = this.props;

        if (!payType && !!payTypes) {
            payTypes.map(item => {
                if (item.online) {
                    dispatch({
                        type: 'updateStateOrder',
                        payload: {payType: item.id}
                    })
                    return item.id
                }
            })
        }

        function getAttachmentIds() {
            let ids = [];
            attachmentIdsArray.map(item => {
                if (item.key.indexOf("document") === 0) {
                    ids.push(item.value)
                }
            })
            return ids;
        }

        const saveOrder = () => {
            let amountDiscountSum = 0.0;
            if (discountCount && discountCount.length > 0) {
                for (let i = 0; i < discountCount.length; i++) {
                    amountDiscountSum += discountCount[i].amount;
                }
                if (amountDiscountSum > discountCount[0].needAmount) {
                    amountDiscountSum = discountCount[0].needAmount;
                }
            }
            dispatch(addOrder({
                servicePriceId: servicePrice.id,
                clientId: currentUser.id,
                newUser: signUp,
                username: currentUser.email,
                password: currentUser.password,
                payTypeId: payType,
                agentId: now ? timeTable.userDto.id : timeTable.agentId,
                countDocument: attachmentIdsArray.length,
                timeTableId: timeTable.id,
                discountAmount: amountDiscountSum,
                discountsDtos: discountCount,
                amount: allAmount.amount,
                docAttachmentId: getAttachmentIds(),
                orderAdditionalServiceDtoList: orderAdditionalServices,
                titleDocument: titleDocument,
                step: step,
                user: '',
                registerBy: 'WEB'
            }))
        }

        if (localStorage.getItem(TEMP_ID_TIME_TABLE) == null) {
            localStorage.setItem(TEMP_ID_TIME_TABLE, uuidv4())
        }
        let getAllDiscounts = () => {
            if (allDiscounts == null) {
                dispatch(getOrderPriceAmounts({
                    servicePriceId: servicePrice.id,
                    clientId: currentUser.id,
                    newUser: signUp,
                    username: currentUser.email,
                    password: currentUser.password,
                    countDocument: attachmentIdsArray.length,
                    timeTableId: timeTable.id,
                    orderAdditionalServiceDtoList: orderAdditionalServices,
                    zipCodeId: '',
                }))
            }
        }
        let changeDiscount = (item) => {
            let discountList = [];
            let isHave = false;

            for (let j = 0; j < discountCount.length; j++) {
                if (discountCount[j].discountName === item.discountName) {
                    isHave = true;
                    break;
                }
            }
            if (isHave) {
                discountList = [];
                for (let j = 0; j < discountCount.length; j++) {
                    if (discountCount[j].discountName !== item.discountName) {
                        discountList.push({...discountCount[j], number: j});
                    }
                }
            } else {
                if (!discountNeed) {
                    discountList = (discountCount ? [...discountCount] : []);
                    discountList.push({...item});
                    let discountList2 = []
                    for (let i = 0; i < discountList.length; i++) {
                        discountList2.push({...discountList[i], number: i})
                    }
                    discountList = discountList2
                }
            }
            let discountAllAmount = 0;
            discountList.map(item => discountAllAmount += item.amount);
            dispatch({
                type: "updateStateOrder",
                payload: {
                    discountCount: discountList,
                    discountNeed: (discountAllAmount >= allDiscounts[0].needAmount)
                }
            })
        }


        let getFreeAgentNow = () => {
            dispatch(getCurrentFreeAgent({tempId: localStorage.getItem(TEMP_ID_TIME_TABLE)}))
            dispatch(getServiceToNow())
        }

        let schedule = () => {
            dispatch({
                type: 'updateStateOrder',
                payload: {now: false, update: 'schedule', step: 1}
            })
            dispatch(getServiceToNow())
        }

        let getContent = () => {
            return (
                <>
                    <div className="online-order-client container bg-white p-sm-2 p-md-5 mt-3"
                         hidden={update === 'main'}>
                        <Row>
                            <Col md={12}>
                                <div className="price-order">
                                </div>
                            </Col>
                        </Row>
                        <Row className="all-step-inPerson pb-4">
                            <Col className={step !== 1 ? "mobile-display-none" : ''}>
                                <div className={"circle-active"}>
                                    <div className="steps">
                                        <span className="icon icon-orderInfo"/>
                                    </div>
                                </div>
                                <div className="step-text">
                                    Order info
                                </div>

                            </Col>
                            <Col className="mobile-display-none" hidden={now}>
                                <div className={step > 1 ? "bordered-line-active" : "bordered-line"}/>
                            </Col>
                            <Col className={step !== 2 && "mobile-display-none"} hidden={now}>
                                <div className={step > 1 ? "circle-active" : "circle"}>
                                    <div className="steps">
                                        <span className="icon icon-userInfo"/>
                                    </div>
                                </div>
                                <div className="step-text">
                                    Data / Time
                                </div>

                            </Col>
                            <Col className="mobile-display-none">
                                <div
                                    className={step > 2 && now === false || (now && step > 1) ? "bordered-line-active" : "bordered-line"}/>
                            </Col>
                            <Col className={step !== 3 && "mobile-display-none"}>
                                <div
                                    className={step > 2 && now === false || (now && step > 1) ? "circle-active" : "circle"}>
                                    <div className="steps">
                                        <span className="icon icon-timeOrder"/>
                                    </div>
                                </div>
                                <div className="step-text">
                                    User info
                                </div>

                            </Col>
                            <Col className="mobile-display-none">
                                <div
                                    className={step > 3 || (now && step > 2) ? "bordered-line-active" : "bordered-line"}/>
                            </Col>
                            <Col className={step !== 4 && "mobile-display-none"}>
                                <div className={step > 3 || (now && step > 2) ? "circle-active" : "circle"}>
                                    <div className="steps">
                                        <span className="icon icon-payment"/>
                                    </div>
                                </div>
                                <div className="step-text">
                                    Payment
                                </div>

                            </Col>
                        </Row>
                        <div className="loading-div" hidden={!loading}>
                            <LoaderMini/>
                        </div>
                        {step === 1 ?
                            <OrderInfoOnline
                                dispatch={dispatch}
                                loading={loading}
                                titleDocument={titleDocument}
                                attachmentIdsArray={attachmentIdsArray}
                                checkbox1={checkbox1}
                                checkbox2={checkbox2}
                                pricingList={pricingList}
                                servicePrice={servicePrice}
                                countDocument={countDocument}
                                next={{step: 2}}
                            />
                            : ""}
                        {step === 2 && !now ?
                            <DateTime
                                dispatch={dispatch}
                                loading={loading}
                                times={times}
                                timesPercent={timesPercent}
                                time={time}
                                timeTable={timeTable}
                                order={order}
                                countDocument={countDocument}
                                servicePrice={servicePrice}
                                selectedDate={selectedDate}
                                online={true}
                                nextStep={3}
                            />
                            : ""}
                        {step === 3 && now === false || (now && step === 2) ?
                            <UserInfo
                                dispatch={dispatch}
                                checkbox3={checkbox3}
                                modal={modal}
                                signUp={signUp}
                                login={login}
                                nextValue={now ? {step: 3} : {step: 4}}
                                online={true}
                                error={error}
                            />
                            : ""}
                        {(!now && step === 4) || (now && step === 3) ?
                            <Row className="online-step-three pl-3">
                                {getAllDiscounts()}
                                {allDiscounts && allDiscounts.length > 0 ?
                                    <div className="discount-block bg-white p-4 rounded">
                                        {allDiscounts.map((dis, i) =>
                                            <div key={i} className="one-discount my-3">
                                                <Input type="checkbox"
                                                       disabled={discountNeed ? !discountCount.some(d => d.discountName === dis.discountName) : false}
                                                       id={"discount" + i}
                                                       name={"discount" + i}
                                                       checked={discountCount && discountCount.some(d => d.discountName === dis.discountName)}
                                                       onClick={() => changeDiscount(dis)}/>{' '}
                                                {dis.discountName} - {(dis.amount)}$.
                                                <br/>
                                                {((dis.allAmount / 100) * dis.discountPercent) <= dis.amount ?
                                                    <small className="secondary"> (You can
                                                        use {(dis.allAmount / 100) * dis.discountPercent}$)
                                                    </small>
                                                    : ""
                                                }
                                            </div>)}
                                    </div>
                                    : ""}
                                {allDiscounts && allDiscounts.length > 0 ?
                                    <div className="discount-amount ml-auto mt-3">
                                        <p>You can use discounts only up to {allDiscounts[0].discountPercent.toFixed(2)}%
                                            ({allDiscounts[0].needAmount.toFixed(2)})$ of the total
                                            price.</p>
                                        <h4>Total amount: {allAmount > 0 && (allAmount).toFixed(2)}$</h4>
                                    </div>
                                    : <h4>Total amount: ${amount > 0 && (amount).toFixed(2)}</h4>}
                                <Col md={8} className="ml-auto">
                                    <Button onClick={() => saveOrder()} color="primary" className="ml-auto px-3">
                                        Booked
                                    </Button>
                                </Col>
                            </Row>
                            : ((now && step === 4) || (!now && step === 5)) ?
                                <Row className="online-step-three pl-3">
                                    {alert_mes && alert_mes.length > 2 ?
                                        <AlertModal title={"Message"} body={alert_mes} action={() => {
                                            dispatch({
                                                type: 'resetState'
                                            })
                                            dispatch(getPayTypes())
                                        }}/> :
                                        <div className="w-100 row">
                                            <Col md={12} className="">
                                                <div className="title">Congratulations!</div>
                                                <div className="commit">Your order successfully registered</div>
                                            </Col>
                                            <Link className="ml-auto"
                                                  to={this.props.location.pathname === "/online" ? "/" : "/client/main-page"}>
                                                <Button color="primary">Go home</Button>
                                            </Link>
                                        </div>
                                    }
                                </Row> : ""}
                    </div>
                    <Container className="online" hidden={update !== 'main'}>
                        <div
                            className="d-flex align-items-center justify-content-center h-100 flex-column position-relative"
                        >
                            {this.props.location.pathname === '/client/online' ? null :
                                <Link to={'/'} className='icon_mobile'>
                                    <div>Home</div>
                                    <i className="fas fa-long-arrow-alt-left"/>
                                </Link>
                            }
                            {this.props.location.pathname === '/client/online' ? null :
                                <i className="fas fa-bars icon_mobile" onClick={this.handleChangeModal}/>
                            }
                            <div>
                                <div className="now-schedule-title">Online</div>
                            </div>
                            <div className='linkBlock'>
                                <div className="box-now-schedule text-center flex-column align-items-center">
                                    <Button onClick={() => getFreeAgentNow()} className="btn-now-schedule">Now <i
                                        className="fas fa-location-arrow"/></Button>
                                    <div className="btn-now-schedule-helper">
                                        We will ensure that we have available agent for you
                                    </div>
                                </div>
                                <div className="box-now-schedule text-center flex-column align-items-center">
                                    <Button onClick={() => schedule()} className="btn-now-schedule">Schedule <i
                                        className="fas fa-location-arrow"/></Button>
                                    <div className="btn-now-schedule-helper">
                                        You can select date and time for you online notary
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Container>
                </>
            )
        }

        const showForms = () => {
            switch (this.props.location.pathname) {
                case "/client/online":
                    return (
                        <MainLayout pathname={this.props.location.pathname}>
                            {getContent()}
                        </MainLayout>
                    );
                case "/online":
                    return (
                        <>
                            <div className="home-page">
                                <div className='position-fixed top-left' style={{zIndex: '100'}}>
                                    <LeftMenu
                                        onToggleModal={this.handleChangeModal}
                                        show={this.state.isOpen}
                                    />
                                </div>
                                <div className="rightMenuOrder">
                                    {getContent()}
                                    <Modall
                                        onToggleModal={this.handleChangeModal}
                                        visiblity={this.state.isOpen}
                                        onLogin={this.handleLogin}
                                    />
                                </div>
                            </div>
                        </>
                    )
                default :
                    return;
            }
        }

        return (
            showForms()
        );
    }
}


export default withRouter(
    connect(({
                 order: {
                     alert_mes, order, timesPercent,
                     amount, currentUser, login, signUp,
                     discountNeed, update, attachmentIdsArray,
                     allDiscounts, orderState,
                     discountCount,
                     servicePrices, servicePrice, checkbox1, checkbox2, checkbox3, step, countDocument,
                     selectedDate, times, timeTable, error, modal, time, loading, payTypes, payType, allAmount,
                     additionalServicePrices, orderAdditionalServices, pricingList, titleDocument, now, stage
                 },
                 auth: {
                     modalShow
                 }
             }) => ({
        alert_mes,
        update,
        orderState,
        amount,
        order,
        timesPercent,
        discountNeed,
        allDiscounts,
        discountCount,
        servicePrices,
        servicePrice,
        checkbox1,
        checkbox2,
        checkbox3,
        step,
        countDocument,
        selectedDate,
        timeTable,
        error,
        modal,
        time,
        loading,
        times,
        currentUser,
        login,
        signUp,
        attachmentIdsArray,
        payTypes,
        payType,
        allAmount,
        additionalServicePrices,
        orderAdditionalServices,
        pricingList,
        titleDocument,
        now,
        stage,
        modalShow
    }))(OnlineOrder)
);

export function getDates(item, count) {
    const date = item ? new Date(item) : new Date();
    let datesCollection = []
    datesCollection.push(`${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`)
    for (let i = 1; i < count; i++) {
        const newDate = new Date(date.getTime() + i * 1000 * 60 * 60 * 24);
        datesCollection.push(`${newDate.getFullYear()}-${newDate.getMonth() + 1}-${newDate.getDate()}`);
    }
    return datesCollection
}