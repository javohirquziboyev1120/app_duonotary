import React, {Component} from 'react';
import MainLayout from "../../components/MainLayout";
import {Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import {config} from "../../utils/config";
import {connect} from "react-redux";
import {editFeedBackAction, getFeedBackByUserId} from "../../redux/actions/AppAction";
import {AvField, AvForm} from "availity-reactstrap-validation";
import PaginationComponent from "react-reactstrap-pagination";
import CloseBtn from "../../components/CloseBtn";

class Feedback extends Component {
    componentDidMount() {
        this.props.dispatch(getFeedBackByUserId({page: 0, size: this.props.size}));
    }

    handleSelected = (selectedPage) => {
        this.props.dispatch(getFeedBackByUserId({
            page: selectedPage - 1,
            size: this.props.size
        }));

        this.props.dispatch(
            {
                type: 'updateState',
                payload: {
                    page: selectedPage - 1
                }
            }
        )
    }

    render() {
        const {feedBacks, dispatch, isUser, currentItem, showModal, isAgent, currentUser, page, size, totalElements} = this.props;
        const editFeedback = (e, v) => {
            v.id = currentItem.id
            v.orderDto = currentItem.orderDto;
            v.agent = isAgent;
            v.operationEnum = "UPDATE";
            v.seen = false;
            v.userId = currentUser.id
            dispatch(editFeedBackAction(v))
        }
        const openModal = (item) => {
            dispatch({type: 'updateState', payload: {showModal: !showModal, currentItem: item}})
        }
        return (

            <MainLayout pathname={this.props.location.pathname}>
                <div className="client-feedback-page container">
                    <Row className="sharing-history-row">

                        {feedBacks && feedBacks.length > 0 ? <Col md={12}> {feedBacks.map(feedBack =>
                            <Col md={12} key={feedBack.id} className="history-col"
                                 onClick={feedBack.seen ? '' : () => openModal(feedBack)}>
                                <Row>
                                    <Col md={3}>
                                        <div className="d-flex">
                                            <div className="avatar-img ">
                                                <img className="agent-avatar"
                                                     src={feedBack.orderDto.agent.photoId ? config.BASE_URL + "/attachment/" + feedBack.orderDto.agent.photoId : "/assets/img/avatar.png"}
                                                     alt=""/>
                                            </div>
                                            <div className="ml-3">
                                                <div className="fs-15">Agent</div>
                                                <div className="fs-20-bold">
                                                    {feedBack.orderDto.agent.firstName + " " + feedBack.orderDto.agent.lastName}
                                                </div>
                                            </div>
                                        </div>
                                    </Col>
                                        <div className="border-line"/>
                                    <Col md={2} className="">
                                        <div className="fs-15">Client</div>
                                        <div className="fs-15-bold pt-2">
                                            {feedBack.orderDto.client.firstName + " " + feedBack.orderDto.client.lastName}
                                        </div>
                                    </Col>
                                        <div className="border-line"/>
                                    <Col md={3}>
                                        <div className="fs-15">
                                            {feedBack.orderDto.agent.phoneNumber}
                                        </div>
                                        <div className="fs-15-bold pt-2">
                                            {feedBack.orderDto.agent.email}
                                        </div>
                                    </Col>
                                        <div className="border-line"/>
                                    <Col md={1}>
                                        <div className="fs-15-bold">Amount</div>
                                        <div className="fs-20-bold pt-2">
                                            {'$' + (feedBack.orderDto.amount ? feedBack.orderDto.amount : '')}
                                        </div>
                                    </Col>
                                        <div className="border-line"/>
                                    <Col md={1}>
                                        <div className="fs-15">Rate</div>
                                        <div className="fs-15-bold pt-2">
                                            {feedBack.rate}
                                        </div>
                                    </Col>
                                    <div className="border-line"/>
                                    <Col md={1}>
                                        <div className="fs-15">Status</div>
                                        <div className="fs-15-bold pt-2">
                                            {feedBack.seen ? "Answered" : 'Pending'}
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={12}>
                                        <div className="feedback-text-admin">
                                            <b className="font-weight-bold ">Client</b> : {feedBack.description}
                                        </div>
                                    </Col>
                                    {feedBack.seen ?
                                        <Col md={12}>
                                            <div className="feedback-text-admin">
                                                <b className="font-weight-bold ">Admin</b> : {feedBack.answer}
                                            </div>
                                        </Col>
                                        : ""}
                                </Row>
                            </Col>
                        )}
                            {feedBacks && feedBacks.length > 0 ?
                                <Col md={6} className="mt-4">
                                    <PaginationComponent
                                        defaultActivePage={page + 1}
                                        totalItems={totalElements} pageSize={size}
                                        onSelect={this.handleSelected}
                                    /></Col> : ''}
                        </Col> : <Col md={6} className="offset-4"><h1>No Information</h1></Col>}

                    </Row>
                </div>
                {(currentItem != null && showModal && isAgent) || (currentItem != null && showModal && isUser) ?
                    <Modal isOpen={showModal} toggle={() => openModal(false)} id="allModalStyle">
                        <div className="closeBtnBlock">
                            <CloseBtn click={() => openModal(false)} />
                        </div>
                        <AvForm onValidSubmit={editFeedback}>
                            <ModalHeader>Edit feedback {currentItem.checkNumber}</ModalHeader>
                            <ModalBody>
                                <AvField type="select" selected={currentItem ? currentItem.rate : "0"} name="rate"
                                         label="Select order rate"
                                         required>
                                    <option value="0" disabled>Select rate</option>
                                    <option value="5">5</option>
                                    <option value="4">4</option>
                                    <option value="3">3</option>
                                    <option value="2">2</option>
                                    <option value="1">1</option>
                                </AvField>
                                <AvField name='description' defaultValue={currentItem && currentItem.description}
                                         placeholder="Enter description" type="textarea" style={{height: '10rem'}}
                                         required/>
                            </ModalBody>
                            <ModalFooter>
                                <div className="d-flex justify-content-start">
                                    {/*<div><Button type="button" margin className="mr-3"*/}
                                    {/*             onClick={() => openModal(false)}>Close</Button></div>*/}
                                    <div><Button type="submit"
                                                 color="info">Submit</Button></div>
                                </div>
                            </ModalFooter>
                        </AvForm>
                    </Modal> : ''}

            </MainLayout>

        );
    }
}


export default connect(({
                            app: {
                                feedBacks,
                                currentItem,
                                showModal, page, size, totalElements
                            },
                            auth: {
                                isUser,
                                currentUser, isAgent
                            }
                        }) => ({
    feedBacks, isUser, currentUser, currentItem, showModal, isAgent, page, size, totalElements
}))(Feedback);