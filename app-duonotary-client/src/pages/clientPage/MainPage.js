import React, {Component} from 'react';
import './clientPages.scss'
import {connect} from "react-redux";
import {getOrdersForClientMainPage} from "../../redux/actions/OrderAction";
import MainLayout from "../../components/MainLayout";
import Orders3 from "../../components/order/OrderCard3";
import LoaderMini from "../../components/LoaderMini";

class MainPage extends Component {
    componentDidMount() {
        this.props.dispatch(getOrdersForClientMainPage({page: 0, size: 5}));
    }

    handleSelected = (selectedPage) => {
        let {dispatch} = this.props;
        dispatch(getOrdersForClientMainPage({
            page: selectedPage - 1,
            size: 3
        }))
        dispatch(
            {
                type: 'updateState',
                payload: {
                    page: selectedPage - 1
                }
            }
        )
    }

    render() {
        const {ordersForClientMainPage,loading} = this.props;
        return (
            <MainLayout pathname={this.props.location.pathname}>
                <div className="main-page-client">
                    <div className="container">
                        {ordersForClientMainPage ?
                            <div>
                                <div className="loading-div" hidden={!loading}>
                                    <LoaderMini/>
                                </div>
                                <Orders3 orders={ordersForClientMainPage}/>
                            </div>
                            : ''}
                    </div>
                </div>
            </MainLayout>
        );
    }
}


export default connect(({order: {ordersForClientMainPage, loading}, auth: {currentUser}, app: {page, size, totalElements}}) => ({
    ordersForClientMainPage, loading,
    currentUser, page, size, totalElements
}))(MainPage);
