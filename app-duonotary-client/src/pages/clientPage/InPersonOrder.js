import React, {Component} from 'react';
import MainLayout from "../../components/MainLayout";
import {Button, Col, Input, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import {connect} from "react-redux";
import {addOrder, getOrderPriceAmounts, getPayTypes} from "../../redux/actions/OrderAction";
import Stripe from "../../components/Stripe";
import {getCountryList, getDocumentTypeList, getEmbassy} from "../../redux/actions/AppAction";
import {Link} from "react-router-dom";
import LeftMenu from "../../components/LeftMenu";
import AlertModal from "../../components/AlertModal";
import OrderInfoComponent from "../../components/order/OrderInfoComponent";
import DateTime from "../../components/order/DateTime";
import Address from "../../components/order/Address";
import {UserInfo} from "../../components/order/UserInfo";
import {ORDER} from "../../utils/constants";
import LoaderMini from "../../components/LoaderMini";
import './clientPages.scss'
import Modall from '../../components/common/modal'
import {SET_SERVICE_PRICE} from "../../redux/actionTypes/OrderActionTypes";

class InPersonOrder extends Component {
    componentDidMount() {
        let {dispatch, stage, step} = this.props;

        let order = JSON.parse(localStorage.getItem(ORDER));
        dispatch({
            type: 'updateStateOrder',
            payload: {...order, alert_mes: ""}
        })

        if (step>0 || (order && order.step>0)){
            dispatch({
                type:'resetState'
            })
        }

        if (stage === 1) {
            dispatch({
                type: 'updateOrderState',
                payload: {
                    update: 'main'
                }
            })
        }
        if ((order && order.stage===0) && stage===0 ){
            dispatch({
                type: 'updateStateOrder',
                payload: {stage:1}
            })
        }else if (!order && stage===0){
            dispatch({
                type: 'updateStateOrder',
                payload: {stage:1}
            })
        }
        dispatch(getPayTypes())
        dispatch(getCountryList())
        dispatch(getEmbassy())
        dispatch(getDocumentTypeList())

    }

    constructor(props) {
        super(props);

        this.state = {
            payment: false,
            isOpen: false,
        }
    }

    handleChangeModal = () => {
        this.setState({isOpen: !this.state.isOpen})
    };

    render() {
        const {
            alert_mes, orderState,
            allDiscounts, discountCount, discountNeed, amount, attachmentIdsArray, timesPercent, code, zipCode,
            servicePrices, servicePrice, countDocument, selectedDate, error, times, time, timeTable, loading,
            currentUser, checkbox3, modal, dispatch, login, signUp, payTypes, stripe, allAmount, order, street_address,
            additionalServicePrices, orderAdditionalServices, internationalDto, realEstateDto, pricingList, titleDocument,
            address, stage, outOfService, countries, embassyCountries, documentTypes, isApostille, modalShow, history, lat, lng
        } = this.props;

        let getAllDiscounts = () => {
            if (allDiscounts == null) {
                dispatch(getOrderPriceAmounts({
                    servicePriceId: servicePrice.id,
                    clientId: currentUser.id ? currentUser.id : '',
                    newUser: signUp,
                    username: currentUser.id ? '' : currentUser.email,
                    password: currentUser.id ? '' : currentUser.password,
                    countDocument: countDocument,
                    timeTableId: timeTable.id,
                    orderAdditionalServiceDtoList: orderAdditionalServices,
                    zipCodeId: zipCode ? zipCode.id : ''
                }))
            }
        }

        const saveOrder = (v) => {
            let amountDiscountSum = 0.0;
            if (discountCount && discountCount.length > 0) {
                for (let i = 0; i < discountCount.length; i++) {
                    amountDiscountSum += discountCount[i].amount;
                }
                if (amountDiscountSum > discountCount[0].needAmount) {
                    amountDiscountSum = discountCount[0].needAmount;
                }
            }
            dispatch(addOrder({
                id: order ? order.id : null,
                realEstateDto: realEstateDto,
                internationalDto: internationalDto,
                address: address,
                servicePriceId: servicePrice.id,
                clientId: currentUser.id,
                newUser: signUp,
                username: currentUser.email,
                password: currentUser.password,
                payTypeId: v.id,
                agentId: timeTable.agentId,
                countDocument: countDocument,
                timeTableId: timeTable.id,
                amount: allAmount.amount,
                discountAmount: amountDiscountSum,
                discountsDtos: discountCount,
                orderAdditionalServiceDtoList: orderAdditionalServices,
                titleDocument: titleDocument,
                lat: lat,
                lng: lng,
                zipCodeId: zipCode ? zipCode.id : '',
                main: v.main,
                user: '',
                registerBy: 'WEB',
                docAttachmentId: attachmentIdsArray&&attachmentIdsArray[0]&&attachmentIdsArray.map(item => {
                    return item.value
                })
            }))
        }

        function canPay() {
            let created = new Date(timeTable.createdAt)
            let start = new Date(timeTable.fromTime)
            let result = start.getTime() - created.getTime();
            let day = Math.floor(result / 1000 / 60 / 60 / 24)
            return day < 6;
        }

        const changeDiscount = (item) => {
            let discountList = [];
            let isHave = false;

            for (let j = 0; j < discountCount.length; j++) {
                if (discountCount[j].discountName === item.discountName) {
                    isHave = true;
                    break;
                }
            }
            if (isHave) {
                discountList = [];
                for (let j = 0; j < discountCount.length; j++) {
                    if (discountCount[j].discountName !== item.discountName) {
                        discountList.push({...discountCount[j], number: j});
                    }
                }
            } else {
                if (!discountNeed) {
                    discountList = (discountCount ? [...discountCount] : []);
                    discountList.push({...item});
                    let discountList2 = []
                    for (let i = 0; i < discountList.length; i++) {
                        discountList2.push({...discountList[i], number: i})
                    }
                    discountList = discountList2
                }
            }
            let discountAllAmount = 0;
            discountList.map(item => discountAllAmount += item.amount);
            dispatch({
                type: "updateStateOrder",
                payload: {
                    discountCount: discountList,
                    discountNeed: (discountAllAmount >= allDiscounts[0].needAmount)
                }
            })
        }

        let getContent = () => {
            return (
                <>
                    <div className="inPerson-order-page container">
                        <div className="title">
                            We provide notary services at your location,
                            whether you are located at a hospital, nursing home,
                            office, or at home.
                        </div>
                        <Row className="all-step-inPerson pb-5">
                            <Col className={stage !== 1 ? "mobile-display-none" : ''}>
                                <div className={"circle-active"}>
                                    <div className="active-step">
                                        <span className="icon icon-address"/>
                                    </div>
                                </div>
                                <div className="step-text">
                                    Address
                                </div>

                            </Col>
                            <Col className="mobile-display-none">
                                <div className={stage > 1 ? "bordered-line-active" : "bordered-line"}/>
                            </Col>
                            <Col className={stage !== 2 ? "mobile-display-none" : ''}>
                                <div className={stage > 1 ? "circle-active" : "circle"}>
                                    <div className="steps">
                                        <span className="icon icon-orderInfo"/>
                                    </div>
                                </div>
                                <div className="step-text">
                                    Order info
                                </div>

                            </Col>
                            <Col className="mobile-display-none">
                                <div className={stage > 2 ? "bordered-line-active" : "bordered-line"}/>
                            </Col>
                            <Col className={stage !== 3 ? "mobile-display-none" : ''}>
                                <div className={stage > 2 ? "circle-active" : "circle"}>
                                    <div className="steps">
                                        <span className="icon icon-userInfo"/>
                                    </div>
                                </div>
                                <div className="step-text">
                                    Data/Time
                                </div>

                            </Col>
                            <Col className="mobile-display-none">
                                <div className={stage > 3 ? "bordered-line-active" : "bordered-line"}/>
                            </Col>
                            <Col className={stage !== 4 ? "mobile-display-none" : ''}>
                                <div className={stage > 3 ? "circle-active" : "circle"}>
                                    <div className="steps">
                                        <span className="icon icon-timeOrder"/>
                                    </div>
                                </div>
                                <div className="step-text">
                                    User info
                                </div>

                            </Col>
                            <Col className="mobile-display-none">
                                <div className={stage > 4 ? "bordered-line-active" : "bordered-line"}/>
                            </Col>
                            <Col className={stage !== 4 ? "mobile-display-none" : ''}>
                                <div className={stage > 4 ? "circle-active" : "circle"}>
                                    <div className="steps">
                                        <span className="icon icon-payment"/>
                                    </div>
                                </div>
                                <div className="step-text">
                                    Payment
                                </div>

                            </Col>
                        </Row>
                        <div className="loading-div" hidden={!loading}>
                            <LoaderMini/>
                        </div>
                        {stage === 1 &&
                        <Address
                            dispatch={dispatch}
                            loading={loading}
                            address={address}
                            outOfService={outOfService}
                            code={code}
                            street_address={street_address}
                        />
                        }

                        {stage === 2 && <OrderInfoComponent
                            dispatch={dispatch}
                            embassyCountries={embassyCountries}
                            documentTypes={documentTypes}
                            countries={countries}
                            isApostille={isApostille}
                            servicePrice={servicePrice}
                            additionalServicePrices={additionalServicePrices}
                            countDocument={countDocument}
                            orderAdditionalServices={orderAdditionalServices}
                            pricingList={pricingList}
                            servicePrices={servicePrices}
                        />
                        }

                        {stage === 3 &&
                        <DateTime
                            dispatch={dispatch}
                            loading={loading}
                            times={times}
                            timesPercent={timesPercent}
                            time={time}
                            timeTable={timeTable}
                            countDocument={countDocument}
                            zipCode={zipCode}
                            servicePrice={servicePrice}
                            selectedDate={selectedDate}
                            online={false}
                            nextStep={4}
                        />
                        }

                        {stage === 4 &&
                        <UserInfo
                            dispatch={dispatch}
                            checkbox3={checkbox3}
                            modal={modal}
                            signUp={signUp}
                            login={login}
                            nextValue={{stage: 5}}
                            online={false}
                            error={error}
                        />
                        }
                        {stage === 5 &&
                        <Row className="order-step-five">
                            {getAllDiscounts()}
                            {allDiscounts && allDiscounts.length > 0 ?
                                <div className="discount-block bg-white p-4 rounded block">
                                    {allDiscounts.map((dis, i) =>
                                        <div key={i} className="one-discount my-3">
                                            <Input type="checkbox"
                                                   disabled={discountNeed ? !discountCount.some(d => d.discountName === dis.discountName) : false}
                                                   id={"discount" + i}
                                                   name={"discount" + i}
                                                   checked={discountCount && discountCount.some(d => d.discountName === dis.discountName)}
                                                   onClick={() => changeDiscount(dis)}/>{' '}
                                            {dis.discountName} - {(dis.amount)}$.
                                            <br/>
                                            {((dis.allAmount / 100) * dis.discountPercent) <= dis.amount ?
                                                <small className="secondary"> (You can
                                                    use {(dis.allAmount / 100) * dis.discountPercent}$)
                                                </small>
                                                : ""
                                            }
                                        </div>
                                    )
                                    }
                                </div>
                                : ""}
                            {allDiscounts && allDiscounts.length > 0 ?
                                <div className="discount-amount ml-auto mt-3 block">
                                    <p>You can use discounts only up to {allDiscounts[0].discountPercent.toFixed(2)}%
                                        ({allDiscounts[0].needAmount.toFixed(2)})$ of the total
                                        price.</p>
                                    <h4>Total amount: {allAmount.toFixed(2)}$</h4>
                                </div>
                                : <h4 className='totalResult'>Total amount: ${amount === -1 ?
                                    <span className="spinner"/> : amount.toFixed(2)}</h4>}
                            {alert_mes.length > 2 ?
                                <AlertModal title={"Message"} body={alert_mes} action={() => {
                                    dispatch({
                                        type: 'resetState'
                                    })
                                    history.push(window.location.pathname === '/client/inPerson' ? '/client/main-page' : '/')
                                }}/>
                                : ""}
                            <Col md={12} className="row">
                                {payTypes.map(v =>
                                    <Col md={4} className={'p-0'} key={v.id} onClick={function () {
                                        if (v.online)
                                            dispatch({type: 'updateStateOrder', payload: {stripe: true}})
                                        saveOrder({id: v.id, main: !v.online})
                                    }}>
                                        <div className="order-type">
                                            <div className="type">{v.name}</div>
                                            <div className="commit">{v.description}</div>
                                            <div className="selected-order-type">
                                                <img src="/assets/img/step.png" alt=""/>
                                            </div>
                                        </div>
                                    </Col>
                                )}
                            </Col>
                            <Col md={12}>
                                <Button onClick={() => {
                                    dispatch({type: 'previousStep'})
                                    dispatch({type: 'updateStateOrder', payload: {signUp: false}})
                                }} className="next-step-btn-previous">
                                    Previous step
                                </Button>
                            </Col>

                            <Modal isOpen={stripe && !!order}>
                                <ModalHeader>
                                    You can pay now!
                                </ModalHeader>
                                <ModalBody>
                                    {canPay() ?
                                        <Stripe type="future" orderId={order.id} history={history}/> :
                                        "Congratulation! Your order successfully registered. You can pay 5 days before the start of your order"}
                                </ModalBody>
                                <ModalFooter>
                                    <Link to={window.location.path === '/inPerson' ? '/' : '/client/main-page'}>
                                        <Button onClick={() => {
                                            dispatch({
                                                type: 'updateStateOrder',
                                                payload: orderState
                                            })
                                        }} className="w-100 btn btn-warning">Later</Button>
                                    </Link>
                                </ModalFooter>
                            </Modal>
                        </Row>
                        }
                    </div>

                </>
            )
        }

        const showForms = () => {
            switch (this.props.location.pathname) {
                case "/client/inPerson":
                    return (
                        <MainLayout pathname={'/client/inPerson"'}>
                            {getContent()}
                        </MainLayout>
                    );
                case "/inPerson":
                    return (
                        <>
                            <div className="home-page">
                                <div className='leftMenuOrder' style={{zIndex: '100'}}>
                                    <LeftMenu
                                        onToggleModal={this.handleChangeModal}
                                        show={this.state.isOpen}
                                    />
                                </div>
                                <div className='rightMenuOrder'>
                                    {getContent()}
                                    <Modall
                                        onToggleModal={this.handleChangeModal}
                                        visiblity={this.state.isOpen}
                                        onLogin={this.handleLogin}
                                    />
                                </div>
                            </div>
                        </>
                    )
                default:
                    return;
            }
        }

        return (
            showForms()
        );
    }
}


export default connect(({
                            order: {
                                alert_mes, timesPercent, code, currentUser, login, signUp, orderState, update,step,
                                allDiscounts, discountCount, discountNeed, amount, zipCode, stripe, attachmentIdsArray,
                                embassyCountries, documentTypes, isApostille, internationalDto, realEstateDto, order,
                                loading, address, servicePrices, servicePrice, pricingList, countDocument,
                                orderAdditionalServices, additionalServicePrices, selectedDate, error, outOfService,
                                times, time, timeTable, stage, checkbox3, modal, dispatch, payTypes, payType, allAmount,
                                serviceArray, lat, lng, street_address
                            },
                            auth: {modalShow},
                            app: {countries},
                        }) => ({
    alert_mes, timesPercent, code, zipCode, modalShow, street_address,
    amount, lat, lng, stripe,
    discountNeed,step,
    allDiscounts,
    discountCount,
    loading,
    order, update,
    countries,
    embassyCountries, attachmentIdsArray,
    documentTypes,
    isApostille,
    internationalDto,
    realEstateDto,
    address,
    servicePrices,
    servicePrice,
    pricingList,
    countDocument,
    orderAdditionalServices,
    outOfService,
    additionalServicePrices,
    selectedDate,
    error,
    times,
    time,
    timeTable,
    stage,
    checkbox3,
    modal,
    dispatch,
    login,
    signUp,
    payTypes,
    payType,
    allAmount,
    currentUser,
    serviceArray, orderState
}))(InPersonOrder);

export function getAmPm(item) {
    if (item.length > 5)
        if (item >= "12:00:00" && item < "13:00:00") {
            return item.substring(0, 5) + ' PM'
        } else if (item >= "13:00:00") {
            return (item.substring(0, 2) - 12) + item.substring(2, 5) + ' PM'
        } else return item.substring(0, 5) + ' AM'
    else {
        if (item >= "12:00" && item < "13:00") {
            return item + ' PM'
        } else if (item >= "13:00" && item !== '24:00') {
            return (item.substring(0, 2) - 12) + item.substring(2) + ' PM'
        } else return (item === '00:00' || item === '24:00' ? '12:00 AM' : item + ' AM')
    }
}

