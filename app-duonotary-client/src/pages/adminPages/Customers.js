import React, {Component} from 'react';
import AdminLayout from "../../components/AdminLayout";
import {Col, Row} from "reactstrap";
import {
    getClientForAdmin,
    getForCustomer,
} from "../../redux/actions/OrderAction";
import {getCustomersBySearch, saveDiscountAction} from "../../redux/actions/AppAction";
import {connect} from "react-redux";
import {config} from "../../utils/config";
import getDate from "../../components/Date";
import Orders3 from "../../components/order/OrderCard3";
import {Link} from "react-router-dom";
import PaginationComponent from "react-reactstrap-pagination";

class Customers extends Component {
    componentDidMount() {
        this.props.dispatch(getForCustomer({page: 0, size: 20}))
    }

    render() {
        const {ordersList, showModal, dispatch, currentItem, currentUser, filters, ordersForClientMainPage, customers, page, size, totalElements} = this.props;
        const openModal = (boolean, item) => {
            dispatch({type: "updateState", payload: {showModal: boolean, currentItem: item}})
        }
        const handleSelected = (selectedPage) => {
            let obj = {page: selectedPage - 1, size: size}
            let search;
            if (filters) {
                search = filters.filter(item => item.key === 'search')[0]
                if (search) obj = {...obj, search: filters.filter(item => item.key === 'search')[0].value};
            }
            console.log(obj);
            dispatch(search ? getCustomersBySearch(obj) : getForCustomer(obj))
            dispatch(
                {
                    type: 'updateState',
                    payload: {
                        page: selectedPage - 1
                    }
                }
            )

            dispatch(getForCustomer({page: selectedPage - 1, size: size}))
            dispatch(
                {
                    type: 'updateState',
                    payload: {
                        page: selectedPage - 1
                    }
                }
            )
        }
        const saveDiscount = (e, v) => {
            v.orderDto = currentItem;
            dispatch(saveDiscountAction(v))
        }
        const clearFilter = () => {
            this.props.dispatch(getForCustomer({page: 0, size: this.props.size}));
            dispatch({type: 'updateState', payload: {filters: '', search: ''}})
        }

        const selectCustomer = (item) => {
            if (item) dispatch(getClientForAdmin(item))
            else dispatch({type: 'updateState', payload: {ordersForClientMainPage: null}})
            dispatch({
                type: "updateState", payload: {
                    currentItem: item,
                }
            })
        }
        return (
            <AdminLayout pathname={this.props.location.pathname}>

                <div className="admin-customer-page container">
                    <Row className="sharing-history-row">
                        <>
                            <Col md={12}>
                                {filters ?
                                    <Row>
                                        <Col><CloseIcon onClick={() => clearFilter(null, false, true)}/></Col>
                                        <Col md={12}>
                                            {filters.map(item =>
                                                <Row className="bg-filter  p-2 m-3 rounded">
                                                    <Col md={12}>{item.key} : {item.value}</Col>
                                                </Row>
                                            )}

                                        </Col>
                                    </Row>

                                    : ''}

                            </Col>
                            {customers && !currentItem ?
                                customers.map(item =>
                                    <Col className="history-col container">
                                        <Row>
                                            <Col md={3}>
                                                <div className="d-flex">
                                                    <div className="">
                                                        <img className="agent-avatar"
                                                             src={item.photo_id ? config.BASE_URL + "/attachment/" + item.photo_id : "/assets/img/avatar.png"}
                                                             alt=""/>
                                                    </div>
                                                    <div className="ml-3">
                                                        <div className="fs-15">Clients</div>
                                                        <div className="fs-20-bold">
                                                            {item.first_name + ' ' + item.last_name}
                                                        </div>
                                                        <div className="fs-15">{item.email}</div>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col md={1}>
                                                <div className="border-line"/>
                                            </Col>
                                            <Col md={2} className="px-0">
                                                <div className="fs-15">Order number</div>
                                                <div className="fs-15-bold pt-2">
                                                    {item.order ? item.order.order_number : 'Hasn\'t ordered yet'}
                                                </div>
                                            </Col>
                                            <Col md={1}>
                                                <div className="border-line"/>
                                            </Col>
                                            <Col md={2}>
                                                <div className="fs-15">
                                                    {item.order ? getDate(item.order.time_table.from_time, 'day') + ' | ' + getDate(item.order.time_table.from_time, 'time') : ''}
                                                </div>
                                                <div className="fs-15-bold pt-2">
                                                    {item.order ? item.order.address ? item.order.address : item.order.time_table.online ? 'Online' : '----' : 'Hasn\'t ordered yet'}
                                                </div>
                                            </Col>
                                            <Col md={1}>
                                                <div className="border-line"/>
                                            </Col>
                                            <Col md={1}>
                                                <div className="fs-15-bold">Amount</div>
                                                <div className="fs-20-bold">
                                                    {item.order ? item.order.amount.toFixed(2) : '0'}$
                                                </div>
                                            </Col>
                                            <Col md={1} className="mt-2 pl-4">
                                                <img className="right-icons" src="/assets/icons/dots.png" alt=""
                                                     onClick={() => selectCustomer(item)}/>
                                            </Col>
                                        </Row>
                                    </Col>
                                )
                                :
                                currentItem ?
                                    <Col md={12} className="admin-agent-order">
                                        <div><CloseIcon onClick={() => selectCustomer(null)}/></div>
                                        <div className="container mt-2">
                                            <Row>
                                                <Col md={6} className="bg-white p-3 pl-4 userInfoWrapper">
                                                    <h6>User info:</h6>
                                                    <h6>First name: {currentItem.first_name}</h6>
                                                    <h6>Last name: {currentItem.last_name}</h6>
                                                    <h6>Email: <Link to={''}
                                                                     target={'_blank'}>{currentItem.email}</Link></h6>
                                                    <h6>Phone number: {currentItem.phone_number}</h6>
                                                </Col>
                                            </Row>
                                        </div>
                                        {ordersForClientMainPage ?
                                            <div className="container">
                                                <Orders3 orders={ordersForClientMainPage}/>
                                            </div>
                                            : ''}
                                    </Col> : ''}
                            {customers && !currentItem ?
                                <div className="w-100 mt-3">
                                    <PaginationComponent
                                        defaultActivePage={page + 1}
                                        pageSize={size}
                                        onSelect={handleSelected}
                                    />
                                </div>
                                : ''}
                        </>
                    </Row>
                </div>
            </AdminLayout>
        );
    }
}

export default connect(({order: {ordersList, ordersForClientMainPage}, app: {filters, showModal, currentItem, customers, page, size, totalElements}, auth: {currentUser}}) => ({
    customers, page, size, totalElements,
    ordersForClientMainPage,
    ordersList,
    showModal,
    currentItem,
    currentUser, filters
}))(Customers);

function CloseIcon(props) {
    return <div className={props.className}>
        <svg onClick={props.onClick} style={{cursor: 'pointer'}} width="50" height="50" viewBox="0 0 50 50" fill="none"
             xmlns="http://www.w3.org/2000/svg">
            <circle cx="25" cy="25" r="25" fill="white"/>
            <path
                d="M24.955 23.1875L31.1425 17L32.91 18.7675L26.7225 24.955L32.91 31.1425L31.1425 32.91L24.955 26.7225L18.7675 32.91L17 31.1425L23.1875 24.955L17 18.7675L18.7675 17L24.955 23.1875Z"
                fill="#313E47"/>
        </svg>
    </div>
}