import React, {Component} from 'react';
import {connect} from "react-redux";
import {
    deletePayment,
    getOrderBySearch,
    getPayments,
    getPayTypes,
    savePayment
} from "../../redux/actions/AppAction";
import {Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row, Table} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import AdminLayout from "../../components/AdminLayout";
import Moment from "moment";
import {AddIcon} from "../../components/Icons";
import {EditIcon} from "./Service";
import {DeleteIcon} from "./AdminOrder";
import Pagination from "react-js-pagination";

class Payment extends Component {
    componentDidMount() {
        this.props.dispatch(getPayments({page: 0, size: 20}))
        this.props.dispatch(getPayTypes())
    }

    handlePageChange(pageNumber) {
        this.props.dispatch(getPayments({page: pageNumber - 1, size: 20}))
        this.props.dispatch(
            {
                type: 'updateState',
                payload: {
                    page: pageNumber - 1,
                    size: 20
                }
            })
    }

    render() {
        const {payments, page, size, totalElements, totalPages, dispatch, currentItem, showModal, showDeleteModal, payType, pinPayType, orders, selectedOrder} = this.props;

        const handleSavePayment = (e, v) => {
            const stringDate = v.date + ' ' + v.time + ':00';
            let b = new Date(stringDate);
            b.setHours(b.getHours() + 5);
            const fullDate = b.getTime();
            let req = {
                id: currentItem.id,
                orderId: v.orderId,
                payTypeId: v.payTypeId,
                chargeId: v.chargeId,
                paySum: v.amount,
                payStatus: v.payStatus,
                date: fullDate
            }
            this.props.dispatch(savePayment(req))
        }
        const delPayment = (item) => {
            this.props.dispatch(deletePayment(item.id))
            dispatch(
                {
                    type: 'updateState',
                    payload: {
                        showDeleteModal: !showDeleteModal,
                        currentItem: ''
                    }
                })
        }
        const openModal = (item) => {
            dispatch(
                {
                    type: 'updateState',
                    payload: {
                        currentItem: item,
                        selectedOrder: item ? item.order.id : '',
                        showModal: !showModal,
                        orders: []
                    }
                })
        }
        const openDelModal = (item) => {
            dispatch(
                {
                    type: 'updateState',
                    payload: {
                        showDeleteModal: !showDeleteModal,
                        currentItem: item
                    }
                })
        }
        const searchOrder = (e) => {
            const data = e.target.value;
            if (data.length > 2)
                this.props.dispatch(getOrderBySearch(data))
        }
        const changePayType = (pay) => {
            payType.map(item => item.id === pay ?
                dispatch(
                    {
                        type: 'updateState',
                        payload: {
                            pinPayType: item.online,
                        }
                    }) : "")
        }
        const selectOrder = (order) => {
            payType.map(item => item.id === order ?
                dispatch(
                    {
                        type: 'updateState',
                        payload: {
                            selectedOrder: item,
                        }
                    }) : "")
        }

        return (
            <AdminLayout pathname={this.props.location.pathname}>
                <div className="main-service-page container">
                    <Row>
                        <Col>
                            <AddIcon
                                title={true}
                                onClick={() => openModal("")}
                            />
                        </Col>
                    </Row>
                    {payments != null ?
                        <>
                            <Table className="custom-table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Clients</th>
                                    <th>Agents</th>
                                    <th>Order-number</th>
                                    <th>Pay date</th>
                                    <th>Amount</th>
                                    <th colSpan="2">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {payments.map((payment, i) =>
                                    <tr key={payment.id}>
                                        <th>{i + 1}</th>
                                        <th>{payment.client.firstName} {payment.client.lastName} </th>
                                        <th>{payment.agent.firstName} {payment.agent.lastName} </th>
                                        <th>{payment.order.serialNumber}</th>
                                        <th>{Moment(payment.date).format("lll")}</th>
                                        <th>${payment.paySum.toFixed(2)}</th>
                                        <th>
                                            <EditIcon
                                                onClick={() => openModal(payment)}
                                            />
                                        </th>
                                        <th>
                                            <DeleteIcon
                                                onClick={() => openDelModal(payment)}
                                            />
                                        </th>
                                    </tr>
                                )}
                                </tbody>
                            </Table>
                            <Pagination
                                activePage={page + 1}
                                itemsCountPerPage={size}
                                totalItemsCount={totalElements}
                                pageRangeDisplayed={5}
                                onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                                linkClass="page-link"
                            />
                        </>
                        : <h1>Payments empty</h1>
                    }


                    <Modal isOpen={showModal} toggle={() => openModal("")}>
                        <AvForm onValidSubmit={handleSavePayment}>
                            <ModalHeader>{currentItem ? 'Edit payment' : 'Add payment'}</ModalHeader>
                            <ModalBody>
                                <AvField
                                    type="text"
                                    name="a"
                                    label="Search order:"
                                    placeholder="e.g: Fxq1y"
                                    onChange={searchOrder}/>
                                <AvField
                                    type="select"
                                    name="orderId"
                                    value={selectedOrder ? selectedOrder : ''}
                                    label="Select order:"
                                    onChange={(a) => selectOrder(a.target.value)}
                                    required={!!currentItem}
                                >
                                    <option value="">
                                        {orders ? orders.length > 0 ? 'Select order' : 'Search order' : 'Orders not found'}
                                    </option>
                                    {orders ? orders.length > 0 ? orders.map(item =>
                                        <option value={item.id}>{item.serialNumber}</option>
                                    ) : '' : 'empty'}
                                </AvField>

                                <AvField
                                    type="select"
                                    name="payTypeId"
                                    placeholder="Select pay type:"
                                    value={currentItem && currentItem.payType ? currentItem.payType.id : ''}
                                    onChange={(a) => changePayType(a.target.value)}
                                    required>
                                    <option selected disabled>Select pay type</option>
                                    {payType.map(item =>
                                        <option value={item.id}>{item.name}</option>
                                    )}
                                </AvField>

                                {pinPayType ?
                                    <AvField
                                        type="text"
                                        name="chargeId"
                                        value={currentItem ? currentItem.chargeId : ''}
                                        placeholder="Enter charge id"
                                        required/>
                                    : ''
                                }

                                <AvField
                                    type="select"
                                    name="payStatus"
                                    value={currentItem ? currentItem.payTypeName : ''}
                                    placeholder="Select pay type"
                                    required>
                                    <option value="0">Select pay status</option>
                                    <option value="STRIPE">STRIPE</option>
                                    <option value="SCHEDULE">SCHEDULE</option>
                                    <option value="BACKOFF">BACKOFF</option>
                                    <option value="PAYED">PAID</option>
                                </AvField>

                                <AvField
                                    type="number"
                                    value={currentItem ? currentItem.paySum : ''}
                                    name="amount"
                                    placeholder="Enter amount:"
                                    required/>

                                <AvField
                                    type="date"
                                    name="date"
                                    value={currentItem && currentItem.date && currentItem.date > 10 ? currentItem.date.substring(0, 10) : ''}
                                    placeholder="Select date:"
                                    required/>

                                <AvField
                                    type="time"
                                    value={currentItem && currentItem.date && currentItem.date > 16 ? currentItem.date.substring(11, 16) : ''}
                                    name="time"
                                    placeholder="Select time:"
                                    required/>

                            </ModalBody>
                            <ModalFooter>
                                <Button type="button" onClick={() => openModal("")}>Cancel</Button>
                                <Button color="info">Save</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>
                    <Modal isOpen={showDeleteModal} toggle={() => openDelModal("")}>
                        <ModalHeader>Payment delete</ModalHeader>
                        <ModalFooter>
                            <Button type="button" onClick={() => openDelModal("")}>Cancel</Button>
                            <Button color="danger" onClick={() => delPayment(currentItem)}>Delete</Button>
                        </ModalFooter>
                    </Modal>
                </div>
            </AdminLayout>
        );
    }
}

export default connect(
    ({
         admin: {
             totalElements, totalPages,
             payments, page, size, currentItem, showModal, showDeleteModal, pinPayType, orders, selectedOrder
         },
         app: {
             payType
         }
     }) => ({
        totalElements, totalPages,
        payments, page, size, currentItem, showModal, showDeleteModal, payType, pinPayType, orders, selectedOrder
    })
)(Payment);