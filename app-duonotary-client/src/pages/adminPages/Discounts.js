import React, {Component} from "react";
import AdminLayout from "../../components/AdminLayout";
import {
    Button,
    Col,
    Container,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    CustomInput,
    Row,
    Table,
} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {connect} from "react-redux";
import * as discountActions from "../../redux/actions/DiscountActions"
import Pagination from "react-js-pagination";
import Select from "react-select";
import {
    deleteDiscountPercent, getAgentsByDiscount,
    getDiscountPercentList, getDiscountPercentPage,
    getStateList, getZipCodesList, resetDiscount,
    saveDiscountPercent
} from "../../redux/actions/AppAction";
import {getCountyByState, getZipCodeByCounty} from "../../redux/actions/AdditionalServicePriceAction";
import * as zpActions from "../../redux/actions/ZipCodeActions";
import * as additionalServicePriceAction from "../../redux/actions/AdditionalServicePriceAction";
import axios from 'axios'
import {config} from "../../utils/config";
import {Link} from "react-router-dom";
import OnlineModal from "../../components/Modal/OnlineModal";
import StatusModal from "../../components/Modal/StatusModal";
import DeleteModal from "../../components/Modal/DeleteModal";
import {AddIcon} from "../../components/Icons";
import Moment from "moment";
import {EditIcon} from "./Service";
import {toast} from "react-toastify";
import CloseBtn from "../../components/CloseBtn";


class Discounts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inPerson: false,
            checked: false,
            tarifMain: true,
            discountMain: false,
            expensesMain: false,
            tarifSharing: false,
            tarifLoyalty: false,
            discountSharing: false,
            discountPercent: false,
            discountLoyalty: false,
            discountCustomer: false,
            deleteModal: false,
            agentDiscountModal: false,
            sharingModal: false,
            loyaltyModal: false,
            customModal: false,
            tempSharingPercent: '',
            removingLoyaltyId: '',
            removingCustomDiscountId: '',
            removingCustomDiscountTariffId: '',
            searchingClientEmail: ''
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handlePageChange6(pageNumber) {
        const {dispatch} = this.props
        dispatch(discountActions.getAllSharingDiscountGiven({page: pageNumber - 1, size: 10}))
    }

    handlePageChange2(pageNumber) {
        const {dispatch} = this.props
        dispatch(discountActions.getAllLoyaltyDiscountGiven({page: pageNumber - 1, size: 10}))
    }

    handlePageChange3(pageNumber) {
        const {dispatch} = this.props
        dispatch(discountActions.getAllDiscountExpense({page: pageNumber - 1, size: 10}))
    }

    handlePageChange4(pageNumber) {
        const {dispatch} = this.props
        dispatch(discountActions.getAllCustomerDiscount({page: pageNumber - 1, size: 10}))
    }

    handleChange(checked) {
        this.setState({checked});
    }

    handlePageChange(pageNumber) {
        this.props.dispatch(getDiscountPercentPage({page: pageNumber - 1, size: 10}))
        this.props.dispatch(
            {
                type: 'updateState',
                payload: {
                    activePageDisPer: pageNumber - 1
                }
            })
    }

    componentDidMount() {
        const {dispatch} = this.props
        dispatch(zpActions.allStates())
        dispatch(discountActions.getSharingDiscountTariff())
        dispatch(discountActions.getAllLoyaltyDiscountTariff())
        dispatch(discountActions.getAllSharingDiscountGiven({page: 0, size: 10}))
        dispatch(discountActions.getAllLoyaltyDiscountGiven({page: 0, size: 10}))
        dispatch(discountActions.getAllDiscountExpense({page: 0, size: 10}))
        dispatch(discountActions.getAllCustomerDiscount({page: 0, size: 10}))
        dispatch(discountActions.getAllCustomDiscountTariff({page: 0, size: 10}))
        dispatch(getStateList())
        dispatch(getDiscountPercentList())
        dispatch(getZipCodesList())
        dispatch(getDiscountPercentPage({page: 0, size: 10}));

    }

    SharingModal = () => {
        this.setState({
            sharingModal: !this.state.sharingModal,
        });
        this.props.dispatch(additionalServicePriceAction.removeCountyArrZipCodeArr())
    }
    AgentDiscountResetModal = () => {
        this.setState({
            agentDiscountModal: !this.state.agentDiscountModal,
        });
    }
    AgentDiscountReset = (e, v) => {
        this.props.dispatch(resetDiscount(v)).then(res => {
            if (res.payload.success) {
                toast.success(res.payload.message)
                this.setState({
                    agentDiscountModal: false,
                });
                this.props.dispatch(discountActions.getAllDiscountExpense({page: 0, size: 10}))
            } else {
                toast.error(res.payload.message)
            }
        }).catch((err) => {
            toast.error("Error! Please, try again!");
        })
    }
    DeleteModal = () => {
        this.setState({
            deleteModal: !this.state.deleteModal,
            removingLoyaltyId: '',
            removingCustomDiscountTariffId: ''
        });
    };


    LoyaltyModal = () => {
        this.props.dispatch({
            type: "updateState", payload: {
                currentLoyaltyDiscountTariff: '',
                currentCustomDiscountTariff: ''
            }
        })
        this.setState(
            {
                loyaltyModal: !this.state.loyaltyModal,
            });
    };
    CustomModal = () => {
        this.props.dispatch({type: "updateState", payload: {currentCustomDiscount: ''}})
        this.setState({
            customModal: !this.state.customModal,
        });
    };
    clickEditCustomDiscount = (item) => {
        this.props.dispatch({type: "updateState", payload: {currentCustomDiscount: item}})
        this.setState({
            customModal: !this.state.customModal,
        });
    }
    clickDiscountPercent = () => {
        this.props.dispatch(discountActions.getSharingDiscountTariff()).then(res => {
            this.props.dispatch({
                type: "updateState", payload: {
                    isTarifTab: true,
                    isTarifSharing: false,
                    isDiscountPercent: true,
                    isTarifLoyalty: false,
                    isTariffCustom: false,
                    isGettingTab: false,
                    isGettingSharing: false,
                    isGettingLoyalty: false,
                    isGettingCustomer: false,
                    isExpenceTab: false,
                    isTariffFirstOrder: false,
                    currentFirstOrderDiscountTariff: ''
                }
            })
        })
    };
    clickTarifMain = () => {
        this.props.dispatch(discountActions.getSharingDiscountTariff()).then(res => {
            this.props.dispatch({
                type: "updateState", payload: {
                    isTarifTab: true,
                    isTarifSharing: true,
                    isDiscountPercent: false,
                    isTarifLoyalty: false,
                    isTariffCustom: false,
                    isGettingTab: false,
                    isGettingSharing: false,
                    isGettingLoyalty: false,
                    isGettingCustomer: false,
                    isExpenceTab: false,
                    isTariffFirstOrder: false,
                    currentFirstOrderDiscountTariff: ''
                }
            })
        })
    };
    clickDiscountMain = () => {
        this.props.dispatch(discountActions.getSharingDiscountTariff()).then(res => {
            this.props.dispatch({
                type: "updateState", payload: {
                    isTarifTab: false,
                    isTarifSharing: false,
                    isDiscountPercent: false,
                    isTarifLoyalty: false,
                    isTariffCustom: false,
                    isGettingTab: true,
                    isGettingSharing: true,
                    isGettingLoyalty: false,
                    isGettingCustomer: false,
                    isExpenceTab: false,
                    isTariffFirstOrder: false,
                    currentFirstOrderDiscountTariff: ''
                }
            })
        })
    };
    clickExpensesMain = () => {
        this.props.dispatch(discountActions.getSharingDiscountTariff()).then(res => {
            this.props.dispatch({
                type: "updateState", payload: {
                    isTarifTab: false,
                    isTarifSharing: false,
                    isDiscountPercent: false,
                    isTarifLoyalty: false,
                    isTariffCustom: false,
                    isGettingTab: false,
                    isGettingSharing: false,
                    isGettingLoyalty: false,
                    isGettingCustomer: false,
                    isExpenceTab: true,
                    isTariffFirstOrder: false,
                    currentFirstOrderDiscountTariff: ''
                }
            })
        })
    };
    clickTarifSharing = () => {
        this.props.dispatch(discountActions.getSharingDiscountTariff()).then(res => {
            this.props.dispatch({
                type: "updateState", payload: {
                    isTarifTab: true,
                    isTarifSharing: true,
                    isDiscountPercent: false,
                    isTarifLoyalty: false,
                    isTariffCustom: false,
                    isGettingTab: false,
                    isGettingSharing: false,
                    isGettingLoyalty: false,
                    isGettingCustomer: false,
                    isExpenceTab: false,
                    isTariffFirstOrder: false,
                    currentFirstOrderDiscountTariff: ''
                }
            })
        })
    };
    clickTarifLoyalty = () => {
        this.props.dispatch(discountActions.getSharingDiscountTariff()).then(res => {
            this.props.dispatch({
                type: "updateState", payload: {
                    isTarifTab: true,
                    isTarifSharing: false,
                    isDiscountPercent: false,
                    isTarifLoyalty: true,
                    isTariffCustom: false,
                    isGettingTab: false,
                    isGettingSharing: false,
                    isGettingLoyalty: false,
                    isGettingCustomer: false,
                    isExpenceTab: false,
                    isTariffFirstOrder: false,
                    currentFirstOrderDiscountTariff: ''
                }
            })
        })
    };
    clickTarifCustom = () => {
        // this.props.dispatch(discountActions.getSharingDiscountTariff()).then(res => {
        this.props.dispatch({
            type: "updateState", payload: {
                isTarifTab: true,
                isTarifSharing: false,
                isDiscountPercent: false,
                isTarifLoyalty: false,
                isTariffCustom: true,
                isGettingTab: false,
                isGettingSharing: false,
                isGettingLoyalty: false,
                isGettingCustomer: false,
                isExpenceTab: false,
                currentCustomDiscountTariff: '',
                isTariffFirstOrder: false,
                currentFirstOrderDiscountTariff: ''
            }
        })
        // })
    };
    clickTarifFirstOrder = () => {
        this.props.dispatch(discountActions.getFirstOrderDiscountTariff()).then(res => {
            this.props.dispatch({
                type: "updateState", payload: {
                    isTarifTab: true,
                    isTarifSharing: false,
                    isTarifLoyalty: false,
                    isDiscountPercent: false,
                    isTariffCustom: false,
                    isGettingTab: false,
                    isGettingSharing: false,
                    isGettingLoyalty: false,
                    isGettingCustomer: false,
                    isExpenceTab: false,
                    currentCustomDiscountTariff: '',
                    isTariffFirstOrder: true,
                }
            })
        })
    };
    clickDiscountSharing = () => {
        this.props.dispatch(discountActions.getSharingDiscountTariff()).then(res => {
            this.props.dispatch({
                type: "updateState", payload: {
                    isTarifTab: false,
                    isTarifSharing: false,
                    isDiscountPercent: false,
                    isTarifLoyalty: false,
                    isTariffCustom: false,
                    isGettingTab: true,
                    isGettingSharing: true,
                    isGettingLoyalty: false,
                    isGettingCustomer: false,
                    isExpenceTab: false,
                    isTariffFirstOrder: false,
                    currentFirstOrderDiscountTariff: ''
                }
            })
        })
    };
    clickDiscountLoyalty = () => {
        this.props.dispatch(discountActions.getSharingDiscountTariff()).then(res => {
            this.props.dispatch({
                type: "updateState", payload: {
                    isTarifTab: false,
                    isTarifSharing: false,
                    isDiscountPercent: false,
                    isTarifLoyalty: false,
                    isTariffCustom: false,
                    isGettingTab: true,
                    isGettingSharing: false,
                    isGettingLoyalty: true,
                    isGettingCustomer: false,
                    isExpenceTab: false,
                    isTariffFirstOrder: false,
                    currentFirstOrderDiscountTariff: ''
                }
            })
        })
    };
    clickDiscountCustomer = () => {
        this.props.dispatch(discountActions.getSharingDiscountTariff()).then(res => {
            this.props.dispatch({
                type: "updateState", payload: {
                    isTarifTab: false,
                    isTarifSharing: false,
                    isDiscountPercent: false,
                    isTarifLoyalty: false,
                    isTariffCustom: false,
                    isGettingTab: true,
                    isGettingSharing: false,
                    isGettingLoyalty: false,
                    isGettingCustomer: true,
                    isExpenceTab: false,
                    isTariffFirstOrder: false,
                    currentFirstOrderDiscountTariff: ''
                }
            })
        })
    };

    render() {
        const {
            permissions,
            dispatch, loading, isTarifTab, isGettingTab, isDiscountPercent, isExpenceTab, isTarifSharing, isTarifLoyalty, isTariffCustom, isGettingSharing, isGettingLoyalty,
            isGettingCustomer, currentSharingDiscountTariff, currentLoyaltyDiscountTariff, loyaltyDiscountTariffList, sharingGivenArray,
            totalSharingGivenElements, sharingGivenActivePage, loyaltyGivenArray, totalLoyaltyGivenElements, loyaltyGivenActivePage,
            discountExpenseArray, totalDiscountExpenseElements, discountExpenseActivePage, customerDiscountArray, totalCustomerDiscountElements,
            customerDiscountActivePage, currentCustomDiscount, isSelectAllzipCodes, isZipCodeActive,
            stateOptions, zipCodeOptions, countyOptions, selectZipCodes, selectCounties, selectStates, currentCustomDiscountTariff,
            customDiscountTariffArray,
            customDiscountTariffClientArray,
            customDiscountTariffTotalElements,
            customDiscountTariffActivePage,
            zipCodeArray, countyArray, stateArray,
            customTariffActive, customTariffUnlimited,
            isTariffFirstOrder, currentFirstOrderDiscountTariff,
            isOnlineOrInPerson,
            all, showEditModal,
            activePageDisPer,
            totalElementsDisPer,
            discountPercentPage, showModal, zipCodes, online, discountPercents, showOnlineModal, currentItem,
            active, showDeleteModal, showStatusModal, agentsByDiscount
        } = this.props;

        const changeActiveStatusSharingDiscountTariff = (e, id, online) => {
            let s = e.target.checked
            dispatch(discountActions.changeActiveStatusSharingDiscountTariff({
                id: id,
                active: s,
                online: online
            })).then(res => {
                dispatch(discountActions.getSharingDiscountTariff())
            })
        }
        const changeActiveStatusFirstOrderDiscountTariff = (e, id, online) => {
            let s = e.target.checked
            dispatch(discountActions.changeActiveStatusFirstOrderDiscountTariff({
                id: id,
                active: s,
                online: online
            })).then(res => {
                dispatch(discountActions.getFirstOrderDiscountTariff())
            })
        }
        const getSharingPercent = (e) => {
            this.setState({tempSharingPercent: e.target.value})
        }
        const editSharingDiscountTariff = () => {
            let req = {
                allZipCode: isSelectAllzipCodes,
                active: isZipCodeActive,
                stateIds: selectStates,
                countyIds: selectCounties,
                zipCodeIds: selectZipCodes,
                percent: this.state.tempSharingPercent,
                online: isOnlineOrInPerson
            }
            if (isTarifSharing) {
                dispatch(discountActions.changePercentSharingDiscountTariff(req)).then(res => {
                    dispatch(discountActions.getSharingDiscountTariff())
                })
            }
            if (isTariffFirstOrder) {
                dispatch(discountActions.changePercentFirstOrderDiscountTariff(req)).then(res => {
                    dispatch(discountActions.getFirstOrderDiscountTariff())
                })
            }
            this.setState({
                sharingModal: !this.state.sharingModal,
            });
        }
        const saveOrEditLoyaltyDiscountTariff = (e, v) => {
            e.preventDefault()
            if (isTarifLoyalty) {
                if (currentLoyaltyDiscountTariff) {
                    v = {...v, id: currentLoyaltyDiscountTariff.id}
                }
                dispatch(discountActions.saveOrEditLoyaltyDiscountTariff(v)).then(res => {
                    this.props.dispatch({
                        type: "updateState", payload: {
                            currentLoyaltyDiscountTariff: ''
                        }
                    })
                    this.setState({
                        loyaltyModal: !this.state.loyaltyModal,
                    });
                    dispatch(discountActions.getAllLoyaltyDiscountTariff())
                })
            }
            if (isTariffCustom) {
                let data = {
                    percent: v.percent,
                    active: customTariffActive,
                    unlimited: customTariffUnlimited
                }
                if (this.state.searchingClientEmail) {
                    data = {...data, clientEmail: this.state.searchingClientEmail}
                }
                if (!customTariffUnlimited) {
                    data = {...data, count: v.count}
                }
                if (currentCustomDiscountTariff) {
                    data = {
                        ...data,
                        clientEmail: currentCustomDiscountTariff.customDiscountTariffClient.email,
                        id: currentCustomDiscountTariff.id
                    }
                }
                dispatch(discountActions.saveOrEditCustomDiscountTariff(data)).then(res => {
                    dispatch({
                        type: "updateState", payload: {
                            customTariffActive: false,
                            customTariffUnlimited: false,
                            currentCustomDiscountTariff: ''
                        }
                    })

                    dispatch(discountActions.getAllCustomDiscountTariff({page: 0, size: 10}))

                })
                this.setState({
                    loyaltyModal: !this.state.loyaltyModal,
                    searchingClientEmail: ''
                });
            }
        }
        const editLoyalty = (item) => {
            dispatch({
                type: "updateState", payload: {
                    currentLoyaltyDiscountTariff: item
                }
            })
            this.setState({
                loyaltyModal: !this.state.loyaltyModal,
            });
        }
        const editCustomDiscountTariff = (item) => {
            dispatch({
                type: "updateState", payload: {
                    currentCustomDiscountTariff: item,
                    customTariffActive: item.active,
                    customTariffUnlimited: item.unlimited
                }
            })
            this.setState({
                loyaltyModal: !this.state.loyaltyModal,
            });
        }
        const changeLoyaltyActive = (id) => {
            dispatch(discountActions.changeLoyaltyDiscountTariffActive({id: id})).then(res => {
                dispatch(discountActions.getAllLoyaltyDiscountTariff())
            })
        }
        const removeLoyaltyDiscountTariff = (id) => {
            this.setState({
                removingLoyaltyId: id,
                deleteModal: !this.state.deleteModal,
                removingCustomDiscountId: '',
                removingCustomDiscountTariffId: ''
            })
        }
        const removeCustomDiscountTariff = (id) => {
            this.setState({
                removingLoyaltyId: '',
                deleteModal: !this.state.deleteModal,
                removingCustomDiscountId: '',
                removingCustomDiscountTariffId: id
            })
        }
        const removeButton = (type) => {
            if (type === 'loyalty') {
                if (this.state.removingLoyaltyId) {
                    dispatch(discountActions.removeLoyaltiDiscountTariff({id: this.state.removingLoyaltyId})).then(res => {
                        dispatch(discountActions.getAllLoyaltyDiscountTariff())
                    })
                }
            }
            if (type === 'custom') {
                if (this.state.removingCustomDiscountId) {
                    dispatch(discountActions.removeCustomDiscountOrder({id: this.state.removingCustomDiscountId})).then(res => {
                        dispatch(discountActions.getAllCustomerDiscount({page: 0, size: 10}))
                    })
                }
            }
            if (type === 'customTariff') {
                if (this.state.removingCustomDiscountTariffId) {
                    dispatch(discountActions.removeCustomDiscountTariff({id: this.state.removingCustomDiscountTariffId})).then(res => {
                        dispatch(discountActions.getAllCustomDiscountTariff({page: 0, size: 10}))
                    })
                }
            }
            this.setState({
                deleteModal: !this.state.deleteModal,
                removingLoyaltyId: '',
                removingCustomDiscountTariffId: ''
            });
        }
        const openRemoveCustomDiscountModal = (id) => {
            this.setState({removingCustomDiscountId: id, deleteModal: !this.state.deleteModal, removingLoyaltyId: ''})
        }
        const editCustomDiscount = (e, v) => {
            e.preventDefault()
            if (currentCustomDiscount) {
                v = {...v, id: currentCustomDiscount.id}
                dispatch(discountActions.saveOrEditCustomDiscount(v)).then(res => {
                    this.props.dispatch({
                        type: "updateState", payload: {
                            currentCustomDiscount: ''
                        }
                    })
                    this.setState({
                        loyaltyModal: !this.state.loyaltyModal,
                    });
                    dispatch(discountActions.getAllCustomerDiscount({page: 0, size: 10}))
                })
            }

        }
        const selectAllZipCode = (e) => {
            let s = e.target.checked
            dispatch({
                type: "updateState", payload: {
                    isSelectAllzipCodes: s
                }
            })
        }
        const selectOnlineOrInPerson = (e) => {
            let s = e.target.checked
            dispatch({
                type: "updateState", payload: {
                    isOnlineOrInPerson: s
                }
            })
        }
        const selectZipCodeActiveStatus = (e) => {
            let s = e.target.checked
            dispatch({
                type: "updateState", payload: {
                    isZipCodeActive: s
                }
            })
        }
        const getCounty = (e, v) => {
            if (!all) {
                let arr = [];
                if (e && e.length === 1) {
                    if (v.action === "remove-value") {
                        v.option = e[0];
                    }
                    dispatch(getCountyByState(v));
                }
                if (e) {
                    e.map((item) => arr.push(item.value));
                    dispatch({
                        type: "updateState",
                        payload: {
                            selectStates: arr,
                            selectCounties: null,
                        },
                    });
                } else {
                    dispatch({
                        type: "updateState",
                        payload: {
                            selectCounties: null,
                            selectZipCodes: null,
                        },
                    });
                }
            }
        };
        const getZipCode = (e, v) => {
            let arr = [];
            if (e && e.length === 1) {
                if (v.action === "remove-value") {
                    v.option = e[0];
                }
                dispatch(getZipCodeByCounty(v));
            }
            if (e) {
                e.map((item) => arr.push(item.value));
                dispatch({
                    type: "updateState",
                    payload: {
                        selectCounties: arr,
                    },
                });
            } else {
                dispatch({
                    type: "updateState",
                    payload: {
                        selectCounties: null,
                        selectZipCodes: null,
                    },
                });
            }
        };
        const saveZipCodeForSharingDiscount = (e, v) => {
            let arr = [];
            if (e) {
                e.map((item) => arr.push(item.value));
                dispatch({
                    type: "updateState",
                    payload: {
                        selectZipCodes: arr,
                    },
                });
            } else {
                dispatch({
                    type: "updateState",
                    payload: {
                        selectZipCodes: null,
                    },
                });
            }
        };

        const doUnlimitedForCustomDiscountTariff = (e) => {
            let s = e.target.checked
            dispatch({
                type: "updateState", payload: {
                    customTariffUnlimited: s
                }
            })
        }
        const changeActiveStatusForCustomDiscountTariff = (e) => {
            let s = e.target.checked
            dispatch({
                type: "updateState", payload: {
                    customTariffActive: s
                }
            })
        }
        const getUsersBySearch = (e) => {
            let search = e.target.value
            if (search.length > 1) {
                axios.get(config.BASE_URL + "/discount/getBySearch?search=" + search).then(res => {
                    dispatch({
                        type: "updateState", payload: {
                            customDiscountTariffClientArray: res.data
                        }
                    })
                })
            }
            if (search.includes("@")) {
                this.setState({searchingClientEmail: search})
            } else {
                this.setState({searchingClientEmail: ''})
            }
        }
        const getAgentsBySearch = (e) => {
            let search = e.target.value
            if (search.length > 1) {
                dispatch(getAgentsByDiscount({search}))
            }
            if (search.includes("@")) {
                this.setState({searchingClientEmail: search})
            } else {
                this.setState({searchingClientEmail: ''})
            }
        }

        const openModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showModal: !showModal,
                    currentItem: item,
                    active: item.active
                }
            })
        };
        const openDeleteModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentItem: item
                }
            })
        };

        const changeActive = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    active: !active
                }
            })
        };
        const changeOnline = () => {
            dispatch({
                type: "updateState",
                payload: {
                    online: !online,
                },
            });
        };
        const openStatusModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item
                }
            })
        };
        const openOnlineModal = (item) => {
            dispatch({
                // type: type.CHANGE_ONLINE,
                type: "updateState",
                payload: {
                    showOnlineModal: !showOnlineModal,
                    currentItem: item,
                },
            });
        };
        const deleteFunction = () => {
            this.props.dispatch(deleteDiscountPercent(currentItem))
        };
        const saveItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null
            v.active = active
            v.online = online
            // v.zipCode =online?null:{id:v.zipCode}
            v.all = online ? null : all
            v.statesId = online ? null : selectCounties !== null ? [] : selectStates
            v.countiesId = online ? null : selectZipCodes !== null ? [] : selectCounties
            v.zipCodesId = online ? null : selectZipCodes !== null ? selectZipCodes : []
            this.props.dispatch(saveDiscountPercent(v))
        }
        const changeStatusDiscountPercent = () => {
            let currentDiscountPercent = {...currentItem};
            currentDiscountPercent.active = !currentItem.active;
            currentDiscountPercent.zipCode = {id: currentItem.zipCodeId};
            this.props.dispatch(saveDiscountPercent(currentDiscountPercent))
        }
        const changeOnlineDiscountPercent = () => {
            let currentDiscountPercent = {...currentItem};
            currentDiscountPercent.online = !currentItem.online;
            currentDiscountPercent.zipCode = {id: currentItem.zipCodeId};
            this.props.dispatch(saveDiscountPercent(currentDiscountPercent))
        }

        const changeAll = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    all: !all
                }
            })
        };


        const saveZipCodeForServicePrice = (e, v) => {
            let arr = [];
            if (v.action === "remove-value") {

            } else {
                e.map((item) => arr.push(item.value))
            }
            dispatch({
                type: 'updateState',
                payload: {
                    selectZipCodes: arr
                }
            })
        };
        const editItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null
            v.active = active
            v.online = online
            this.props.dispatch(saveDiscountPercent(v))
        }
        const editModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showEditModal: !showEditModal,
                    currentItem: item,
                    active: item.active,
                    online: item.online
                }
            })
        };
        const showEditDisModal = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    showEditModal: !showEditModal,
                }
            })

        }

        return (
            <AdminLayout pathname={this.props.location.pathname}>
                <div className="admin-discount-page container">

                        <div className="d-flex justify-content-end">
                            <div className="active-router-discount">
                                <div
                                    onClick={this.clickTarifMain}
                                    className={
                                        !isTarifTab
                                            ? "default-route"
                                            : "active-route"
                                    }
                                >
                                    Discount Tariff
                                </div>
                                <div
                                    onClick={this.clickDiscountMain}
                                    className={
                                        !isGettingTab
                                            ? "default-route"
                                            : "active-route"
                                    }
                                >
                                    Given Discount
                                </div>
                                <div
                                    onClick={this.clickExpensesMain}
                                    className={
                                        !isExpenceTab
                                            ? "default-route"
                                            : "active-route"
                                    }
                                >
                                    Expensed Discount
                                </div>
                            </div>
                        </div>

                    {/*Sub menus*/}
                    {isTarifTab && (

                        <Row className="margin-top-15">
                            <Col md={6} className="offset-lg-6">
                                <div className="active-router-discount">

                                    <div
                                        onClick={this.clickTarifSharing}
                                        className={
                                            !isTarifSharing
                                                ? "default-route"
                                                : "active-route"
                                        }
                                    >
                                        Sharing
                                    </div>
                                    <div
                                        onClick={this.clickDiscountPercent}
                                        className={
                                            !isDiscountPercent
                                                ? "default-route"
                                                : "active-route"
                                        }
                                    >
                                        Percent
                                    </div>
                                    <div
                                        onClick={this.clickTarifLoyalty}
                                        className={
                                            !isTarifLoyalty
                                                ? "default-route"
                                                : "active-route"
                                        }
                                    >
                                        Loyalty
                                    </div>
                                    <div
                                        onClick={this.clickTarifCustom}
                                        className={
                                            !isTariffCustom
                                                ? "default-route"
                                                : "active-route"
                                        }
                                    >
                                        Custom
                                    </div>
                                    <div
                                        onClick={this.clickTarifFirstOrder}
                                        className={
                                            !isTariffFirstOrder
                                                ? "default-route"
                                                : "active-route"
                                        }
                                    >
                                        First Order
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    )}
                    {isGettingTab && (
                        <div className="margin-top-15 d-flex justify-content-end">
                                <div className="active-router-discount">
                                    <div
                                        onClick={this.clickDiscountSharing}
                                        className={
                                            !isGettingSharing
                                                ? "default-route"
                                                : "active-route"
                                        }
                                    >
                                        Sharing
                                    </div>
                                    <div
                                        onClick={this.clickDiscountLoyalty}
                                        className={
                                            !isGettingLoyalty
                                                ? "default-route"
                                                : "active-route"
                                        }
                                    >
                                        Loyalty
                                    </div>
                                </div>
                        </div>
                    )}

                    {isTarifSharing && (
                        <div>
                            <div className="sharing-history-row ">
                                {currentSharingDiscountTariff ? currentSharingDiscountTariff.map(item =>
                                    <div className="w-100">
                                        <Col className="history-col">
                                            <div>
                                                <Row>
                                                    <Col md={4}>
                                                        <div className="fs-15">
                                                            Sharing percent {item.online ? "Online" : "In Person"}
                                                        </div>
                                                        <div className="fs-20-bold pt-2">
                                                            {item.percent ? item.percent + " %" : item.minPercent === item.maxPercent ? item.maxPercent + " %" : item.minPercent + " - " + item.maxPercent + " %"}
                                                        </div>
                                                    </Col>
                                                    <Col md={1}>
                                                        <div className="border-line"/>
                                                    </Col>
                                                    {item.zipCodeDto ?
                                                        <div><Col md={2}>
                                                            <div className="fs-15">ZipCode</div>
                                                        </Col>
                                                            <Col md={1}>
                                                                <div className="border-line"/>
                                                            </Col></div> : ''
                                                    }
                                                    <Col
                                                        md={item.zipCodeDto ? 1 : 2}>
                                                        <div className="fs-15">Status</div>
                                                        <div>
                                                            <FormGroup check>
                                                                <Label check className={'labelDuo w-100'}>
                                                                    <div className={'labelBlock'}
                                                                         style={{top: '50%', left: '32%'}}>
                                                                        {item.active && <div className={'labelBox'}>
                                                                            <i className="fas fa-check"/>
                                                                        </div>}
                                                                    </div>
                                                                    <Input
                                                                        className="d-none"
                                                                        type="checkbox"
                                                                        onChange={(e) => changeActiveStatusSharingDiscountTariff(e, item.id, item.online)}
                                                                        checked={item.active}
                                                                    />
                                                                </Label>
                                                            </FormGroup>
                                                        </div>
                                                    </Col>
                                                    <Col md={1}>
                                                        <div className="border-line"/>
                                                    </Col>
                                                    <Col md={2} className="mt-3 pl-4">
                                                        <EditIcon
                                                            onClick={this.SharingModal}
                                                        />
                                                    </Col>
                                                </Row>
                                            </div>
                                        </Col>
                                    </div>
                                ) : ''
                                }
                            </div>
                        </div>
                    )}
                    {isDiscountPercent && (
                        <div>
                            <div className="d-flex align-items-center justify-content-between">
                                <AddIcon
                                    title={true}
                                    onClick={() => openModal("")}
                                />
                            </div>
                            <Table className="custom-table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Percent</th>
                                    <th>Zip Code</th>
                                    <th>Status</th>
                                    <th>Online</th>
                                    <th colSpan="2" className="">Operation</th>
                                </tr>
                                </thead>

                                {discountPercentPage ? (
                                    <tbody>
                                    {discountPercentPage.map((item, i) => (
                                        <tr className="block" key={item.id}>
                                            <td>{i + 1}</td>
                                            <td>{item.percent}</td>
                                            <td>{item.zipCode ? item.zipCode.code : "online"}</td>
                                            <td>
                                                <FormGroup check>
                                                    <Label check className={'labelDuo'}>
                                                        <div className={'labelBlock'}>
                                                            {item.active && <div className={'labelBox'}>
                                                                <i className="fas fa-check"></i>
                                                            </div>}
                                                        </div>
                                                        <Input
                                                            className={'d-none'}
                                                            type="checkbox"
                                                            onClick={() =>
                                                                openStatusModal(
                                                                    item
                                                                )
                                                            }
                                                            checked={item.active}
                                                        />
                                                        Active
                                                    </Label>
                                                </FormGroup>
                                            </td>
                                            <td>
                                                <FormGroup check>
                                                    <Label check className={'labelDuo'}>
                                                        <div className={'labelBlock'}>
                                                            {item.online && <div className={'labelBox'}>
                                                                <i className="fas fa-check"></i>
                                                            </div>}
                                                        </div>
                                                        <Input
                                                            className={'d-none'}
                                                            type="checkbox"
                                                            onClick={() =>
                                                                openOnlineModal(
                                                                    item
                                                                )
                                                            }
                                                            checked={item.online}
                                                        />
                                                        Online
                                                    </Label>
                                                </FormGroup>
                                            </td>
                                            <td>
                                                <EditIcon
                                                    onClick={() => editModal(item)}
                                                />
                                            </td>
                                            <td>
                                                <DeleteIcon
                                                    onClick={() =>
                                                        openDeleteModal(item)
                                                    }
                                                />
                                            </td>
                                        </tr>
                                    ))}
                                    </tbody>
                                ) : (
                                    <tbody>
                                    <tr>
                                        <td colSpan="4">
                                            <h3 className="text-center mx-auto">
                                                {" "}
                                                No information{" "}
                                            </h3>
                                        </td>
                                    </tr>
                                    </tbody>
                                )}
                            </Table>

                            <Row>
                                <Col md={12}>
                                    <Pagination
                                        activePage={activePageDisPer + 1}
                                        itemsCountPerPage={10}
                                        totalItemsCount={totalElementsDisPer}
                                        pageRangeDisplayed={5}
                                        onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                                        linkClass="page-link"
                                    />
                                </Col>
                            </Row>

                            <Modal
                                id="allModalStyle"
                                isOpen={showModal}
                                toggle={openModal}
                            >
                                <div className='position-absolute' style={{top: '45px', left: '-12%'}}>
                                    <CloseBtn click={openModal}/>
                                </div>
                                <AvForm onValidSubmit={saveItem}>
                                    <ModalHeader
                                        toggle={openModal}
                                        charCode="x"
                                        className="model-head"
                                    >
                                        {currentItem != null && currentItem && currentItem.id
                                            ? "Edit discount percent"
                                            : "Add discount percent"}
                                    </ModalHeader>
                                    <ModalBody>
                                        <AvField name="percent" label="Surge:" type="number" required
                                                 defaultValue={currentItem != null ? currentItem.percent : ""}
                                                 placeholder="Enter discount percent name"/>

                                        <div className="flex-row space-around">
                                            <label className={'labelDuo'}>
                                                <div className={'labelBlock'} style={{left: '-25%'}}>
                                                    {all && <div className={'labelBox'}>
                                                        <i className="fas fa-check"></i>
                                                    </div>}
                                                </div>
                                                <Input
                                                    className="check-box-title d-none"
                                                    type="checkbox"
                                                    checked={all}
                                                    onChange={changeAll}
                                                />
                                                Add all zip code
                                            </label>

                                            <label className={'labelDuo'}>
                                                <div className={'labelBlock'}>
                                                    {active && <div className={'labelBox'}>
                                                        <i className="fas fa-check"></i>
                                                    </div>}
                                                </div>
                                                <Input
                                                    className="check-box-title d-none"
                                                    type="checkbox"
                                                    checked={active}
                                                    onChange={changeActive}
                                                />
                                                Active
                                            </label>


                                            {/*{discountPercents.map(item=> item.online===true?'':*/}

                                            <label className={'labelDuo'}>
                                                <div className={'labelBlock'}>
                                                    {online && <div className={'labelBox'}>
                                                        <i className="fas fa-check"></i>
                                                    </div>}
                                                </div>
                                                <Input
                                                    className="check-box-title d-none"
                                                    type="checkbox"
                                                    checked={online}
                                                    onChange={changeOnline}
                                                />
                                                Online
                                            </label>

                                            {/*)}*/}
                                        </div>
                                        {online ? '' :
                                            <Select
                                                isDisabled={all}
                                                defaultValue="Select State"
                                                isMulti
                                                name="statesId"
                                                options={stateOptions}
                                                onChange={getCounty}
                                                className="basic-multi-select"
                                                classNamePrefix="select"
                                            />}
                                        {online ? '' :
                                            selectStates !== null && selectStates.length === 1 ?
                                                <Select
                                                    isDisabled={all}
                                                    defaultValue="Select County"
                                                    isMulti
                                                    name="countiesId"
                                                    options={countyOptions}
                                                    onChange={getZipCode}
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                /> : ""}
                                        {online ? '' :
                                            selectCounties !== null && selectCounties.length === 1 ?
                                                <Select
                                                    isDisabled={all}
                                                    defaultValue="Select Zipcode"
                                                    isMulti
                                                    name="zipCodesId"
                                                    options={zipCodeOptions}
                                                    onChange={saveZipCodeForServicePrice}
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                /> : ""}


                                    </ModalBody>
                                    <ModalFooter>
                                        {/*<Button*/}
                                        {/*    type="button"*/}
                                        {/*    color="secondary"*/}
                                        {/*    outline*/}
                                        {/*    onClick={openModal}*/}
                                        {/*>*/}
                                        {/*    Cancel*/}
                                        {/*</Button>{" "}*/}
                                        <Button color="info" outline>Save</Button>
                                    </ModalFooter>
                                </AvForm>
                            </Modal>


                            <Modal id="allModalStyle" isOpen={showEditModal} toggle={editModal}>
                                <AvForm onValidSubmit={editItem}>
                                    <ModalHeader toggle={showEditDisModal}
                                                 charCode="x">Edit discount percent</ModalHeader>
                                    <ModalBody style={{backgroundColor: "#F9F9F9"}}>
                                        <AvField name="zipcode" disabled
                                                 value={currentItem && currentItem.zipCode && currentItem.zipCode.code ? currentItem.zipCode.code : ''}/>
                                        <AvField name="percent" label="Percent" required
                                                 defaultValue={currentItem ? currentItem.percent : " "}
                                                 placeholder="Enter Percent"/>

                                        <div className="flex-row space-around">
                                            <label className={'labelDuo'}>
                                                <div className={'labelBlock'}>
                                                    {active && <div className={'labelBox'}>
                                                        <i className="fas fa-check"></i>
                                                    </div>}
                                                </div>
                                                <Input
                                                    className="check-box-title d-none"
                                                    type="checkbox"
                                                    checked={active}
                                                    onChange={changeActive}
                                                />
                                                Active
                                            </label>


                                            {discountPercents.map(item => item.online === true ? "" :
                                                <label className={'labelDuo'}>
                                                    <div className={'labelBlock'}>
                                                        {online && <div className={'labelBox'}>
                                                            <i className="fas fa-check"></i>
                                                        </div>}
                                                    </div>
                                                    <Input
                                                        className="check-box-title d-none"
                                                        type="checkbox"
                                                        checked={online}
                                                        onChange={changeOnline}
                                                    />
                                                    Online
                                                </label>
                                            )}
                                        </div>
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button type="button" color="secondary" outline
                                                onClick={showEditDisModal}>Cancel</Button>{' '}
                                        <Button color="success">Save</Button>
                                    </ModalFooter>
                                </AvForm>
                            </Modal>

                            {showDeleteModal && <DeleteModal text={currentItem != null ? currentItem.name : ''}
                                                             showDeleteModal={showDeleteModal}
                                                             confirm={deleteFunction}
                                                             cancel={openDeleteModal}/>}

                            {showStatusModal && <StatusModal text={currentItem != null ? currentItem.name : ''}
                                                             showStatusModal={showStatusModal}
                                                             confirm={changeStatusDiscountPercent}
                                                             cancel={openStatusModal}/>}
                            {showOnlineModal && (
                                <OnlineModal
                                    text={currentItem != null ? currentItem.name : ""}
                                    showOnlineModal={showOnlineModal}
                                    confirm={changeOnlineDiscountPercent}
                                    cancel={openOnlineModal}
                                />
                            )}
                        </div>
                    )}

                    {isTariffFirstOrder && (
                        <div>
                            <Row className="sharing-history-row ">
                                <Col md={12} className="history-col">
                                    {currentFirstOrderDiscountTariff ? currentFirstOrderDiscountTariff.map(item =>
                                        <Row>
                                            <Col md={4}>
                                                <div className="fs-15">
                                                    New Client First Order
                                                    percent {item.online ? "Online" : "In Person"}
                                                </div>
                                                <div className="fs-20-bold pt-2">
                                                    {item.percent ? item.percent + " %" : item.minPercent == item.maxPercent ? item.maxPercent + " %" : item.minPercent + " - " + item.maxPercent + " %"}
                                                </div>
                                            </Col>
                                            <Col md={1}>
                                                <div className="border-line"/>
                                            </Col>
                                            {item.zipCodeDto ?
                                                <div><Col md={2}>
                                                    <div className="fs-15">ZipCode</div>
                                                </Col>
                                                    <Col md={1}>
                                                        <div className="border-line"/>
                                                    </Col></div> : ''
                                            }
                                            <Col
                                                md={item.zipCodeDto ? 1 : 2}>
                                                <div className="fs-15">Status</div>
                                                <div>
                                                    <FormGroup check>
                                                        <Label check className={'labelDuo w-100'} style={{left: '85%'}}>
                                                            <div className={'labelBlock'}>
                                                                {item.active && <div className={'labelBox'}>
                                                                    <i className="fas fa-check"/>
                                                                </div>}
                                                            </div>
                                                            <Input
                                                                className="mt-0 d-none"
                                                                type="checkbox"
                                                                onChange={(e) => changeActiveStatusFirstOrderDiscountTariff(e, item.id, item.online)}
                                                                checked={item.active}
                                                            />
                                                        </Label>
                                                    </FormGroup>
                                                </div>
                                            </Col>
                                            <Col md={1}>
                                                <div className="border-line"/>
                                            </Col>
                                            <Col md={2} className="mt-3 pl-4">
                                                <EditIcon
                                                    onClick={this.SharingModal}
                                                />
                                            </Col>
                                        </Row>
                                    ) : ''}
                                </Col>
                            </Row>
                        </div>
                    )}

                    {isTarifLoyalty && (
                        <div>
                            <Row className="sharing-history-row ">
                                <Col onClick={this.LoyaltyModal} md={12}>
                                    <div className="add-discount">
                                        Add loyalty discount
                                    </div>
                                </Col>

                                <div className="flex-column width-100">
                                    <div
                                        className="agent-block flex-row align-items-center margin-top-15 margin-left-15 margin-right-15">
                                        <Col md={1}>
                                            <div className="fs-15">Month</div>
                                        </Col>
                                        <Col md={1}>
                                            <div className="border-line"/>
                                        </Col>
                                        <Col md={2}>
                                            <div className="fs-15">Percent</div>
                                        </Col>
                                        <Col md={1}>
                                            <div className="border-line"/>
                                        </Col>
                                        <Col md={2}>
                                            <div className="fs-15">
                                                Minimum sum
                                            </div>

                                        </Col>
                                        <Col md={1}>
                                            <div className="border-line"/>
                                        </Col>

                                        <Col md={1}>
                                            <div className="fs-15">Status</div>
                                        </Col>
                                        <Col md={1}>
                                            <div className="border-line"/>
                                        </Col>
                                        <Col md={2} className="mt-2 pl-4">
                                            <div className="fs-15">Actions</div>
                                        </Col>
                                    </div>
                                </div>

                                {loyaltyDiscountTariffList ? loyaltyDiscountTariffList.map((item, index) =>
                                    <div className="flex-column width-100">
                                        <div
                                            className="agent-block flex-row align-items-center margin-top-15 margin-left-15 margin-right-15">
                                            <Col md={1}>
                                                <div className="fs-20-bold pt-2">
                                                    {item.month}
                                                </div>
                                            </Col>
                                            <Col md={1}>
                                                <div className="border-line"/>
                                            </Col>
                                            <Col md={2}>
                                                <div className="fs-20-bold pt-2">
                                                    {item.percent}
                                                </div>
                                            </Col>
                                            <Col md={1}>
                                                <div className="border-line"/>
                                            </Col>
                                            <Col md={2}>
                                                <div className="fs-20-bold pt-2">
                                                    {item.minSum}
                                                </div>
                                            </Col>
                                            <Col md={1}>
                                                <div className="border-line"/>
                                            </Col>

                                            <Col md={1}>
                                                <div>
                                                    <FormGroup check>
                                                        <Label check className={'labelDuo w-100'}>
                                                            <div className={'labelBlock'}
                                                                 style={{top: '50%', left: '-20%'}}>
                                                                {item.active && <div className={'labelBox'}>
                                                                    <i className="fas fa-check"/>
                                                                </div>}
                                                            </div>
                                                            <Input
                                                                className="mt-0 d-none"
                                                                type="checkbox"
                                                                checked={item.active}
                                                                onChange={() => changeLoyaltyActive(item.id)}
                                                            />
                                                        </Label>
                                                    </FormGroup>
                                                </div>
                                            </Col>
                                            <Col md={1}>
                                                <div className="border-line"/>
                                            </Col>
                                            <Col md={1} className="mt-2 pl-4">
                                                <EditIcon
                                                    onClick={() => editLoyalty(item)}
                                                />
                                            </Col>
                                            <Col md={1} className="mt-2 pl-4">
                                                <DeleteIcon onClick={() => removeLoyaltyDiscountTariff(item.id)}/>
                                            </Col>
                                        </div>
                                    </div>
                                ) : ''}
                            </Row>
                        </div>
                    )}
                    {isTariffCustom && (
                        <div>
                            <Row className="sharing-history-row ">
                                <Col onClick={this.LoyaltyModal} md={12}>
                                    <div className="add-discount">
                                        Add custom discount tariff
                                    </div>
                                </Col>
                                <div className="flex-column width-100">
                                    <div
                                        className="agent-block flex-row align-items-center margin-top-15 margin-left-15 margin-right-15">
                                        <Col md={3} className="text-center border-right">
                                            <div className="fs-15">Client</div>
                                        </Col>
                                        <Col md={2} className="text-center border-right">
                                            <div className="fs-15">Percent</div>
                                        </Col>
                                        <Col md={1} className="text-center border-right">
                                            <div className="fs-15">
                                                Count
                                            </div>
                                        </Col>
                                        <Col md={2} className="text-center border-right">
                                            <div className="fs-15">Using Count</div>
                                        </Col>
                                        <Col md={1} className="text-center border-right">
                                            <div className="fs-15">Active</div>
                                        </Col>
                                        <Col md={1} className="text-center border-right">
                                            <div className="fs-15">Unlimited</div>
                                        </Col>
                                        <Col md={2} className="text-center">
                                            <div className="fs-15">Actions</div>
                                        </Col>
                                    </div>
                                </div>

                                {customDiscountTariffArray ? customDiscountTariffArray.map(item =>
                                    <div className="flex-column width-100">
                                        <div
                                            className="agent-block flex-row align-items-center margin-top-15 margin-left-15 margin-right-15">
                                            <Col md={3} className="text-center border-right">
                                                <div
                                                    className="fs-15">{item.customDiscountTariffClient.fullName + " " + item.customDiscountTariffClient.phoneNumber}</div>
                                            </Col>
                                            <Col md={2} className="text-center border-right">
                                                <div className="fs-15">{item.percent + " %"}</div>
                                            </Col>
                                            <Col md={1} className="text-center border-right">
                                                <div className="fs-15">
                                                    {item.count ? item.count : ''}
                                                </div>
                                            </Col>
                                            <Col md={2} className="text-center border-right">
                                                <div className="fs-15">{item.usingCount}</div>
                                            </Col>
                                            <Col md={1} className="border-right">
                                                <div>
                                                    <label className={'labelDuo w-100'}
                                                           style={{top: '50%', left: '80%'}}>
                                                        <div className={'labelBlock'}>
                                                            {item.active && <div className={'labelBox'}>
                                                                <i className="fas fa-check"/>
                                                            </div>}
                                                        </div>
                                                        <input className={'d-none'} type="checkbox"
                                                               checked={item.active}/>
                                                    </label>
                                                </div>
                                            </Col>
                                            <Col md={1} className="text-center border-right">
                                                <div className="">
                                                    <label className={'labelDuo w-100'}
                                                           style={{top: '50%', left: '80%'}}>
                                                        <div className={'labelBlock '}>
                                                            {item.unlimited && <div className={'labelBox'}>
                                                                <i className="fas fa-check"/>
                                                            </div>}
                                                        </div>
                                                        <input className={'d-none'} type="checkbox"
                                                               checked={item.unlimited}/>
                                                    </label>
                                                </div>
                                            </Col>
                                            <Col md={1} className="mt-2 pl-4">
                                                <EditIcon
                                                    onClick={() => editCustomDiscountTariff(item)}
                                                />
                                            </Col>
                                            <Col md={1} className="mt-2 pl-4">
                                                <DeleteIcon onClick={() => removeCustomDiscountTariff(item.id)}/>
                                            </Col>
                                        </div>
                                    </div>
                                ) : ''}
                            </Row>
                        </div>
                    )}
                    {isGettingSharing && (
                        <div>
                            <Table className="custom-table margin-top-15">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Client</th>
                                    <th>Invited client</th>
                                    <th>Date</th>
                                    <th>Surge</th>
                                    <th>Amount</th>
                                    <th>Leftover</th>
                                </tr>
                                </thead>
                                <tbody>
                                {/*Map*/}
                                {sharingGivenArray ? sharingGivenArray.map((item, index) =>
                                    <tr key={index}>
                                        <td>{(index + 1) + (sharingGivenActivePage * 10)}</td>
                                        <td>{item.clientFullName}</td>
                                        <td>{item.invitedClientFullName ? item.invitedClientFullName : ''}</td>
                                        <td>{Moment(item.date).format('lll')}</td>
                                        <td>{item.percent > 0 ? item.percent.toFixed(2) : "$10"}</td>
                                        <td>${item.sum.toFixed(2)}</td>
                                        <td>{item.leftover > 0 ? item.leftover.toFixed(2) : "$0"}</td>
                                    </tr>
                                ) : ''}
                                {/*Map end*/}
                                </tbody>
                            </Table>
                            <Row>
                                <Col md={12}>
                                    <Pagination
                                        activePage={sharingGivenActivePage + 1}
                                        itemsCountPerPage={10}
                                        totalItemsCount={totalSharingGivenElements}
                                        pageRangeDisplayed={5}
                                        onChange={this.handlePageChange6.bind(this)} itemClass="page-item"
                                        linkClass="page-link"
                                    />
                                </Col>
                            </Row>
                        </div>
                    )}
                    {isGettingLoyalty && (
                        <div>
                            <Table className="custom-table margin-top-15">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Client</th>
                                    <th>Loyalty Discount Percent</th>
                                    <th>Loyalty Disount Sum</th>
                                </tr>
                                </thead>
                                <tbody>
                                {/*map*/}
                                {loyaltyGivenArray ? loyaltyGivenArray.map((item, index) =>
                                    <tr>
                                        <td>{(index + 1) + (loyaltyGivenActivePage * 10)}</td>
                                        <td>{item.clientFullName}</td>
                                        <td>{item.percent.toFixed(2)}</td>
                                        <td>{item.sum.toFixed(2)}</td>
                                    </tr>
                                ) : ''}
                                {/*map end*/}
                                </tbody>
                            </Table>
                            <Row>
                                <Col md={12}>
                                    <Pagination
                                        activePage={loyaltyGivenActivePage + 1}
                                        itemsCountPerPage={10}
                                        totalItemsCount={totalLoyaltyGivenElements}
                                        pageRangeDisplayed={5}
                                        onChange={this.handlePageChange2.bind(this)} itemClass="page-item"
                                        linkClass="page-link"
                                    />
                                </Col>
                            </Row>
                        </div>
                    )}
                    {isGettingCustomer && (
                        <div>
                            <Row className="sharing-history-row ">
                                {/*<Col onClick={this.CustomModal} md={12}>*/}
                                {/*    <div className="add-discount">*/}
                                {/*        Add discount*/}
                                {/*    </div>*/}
                                {/*</Col>*/}

                                <div className="flex-column margin-top-15 width-100">
                                    {/*map*/}
                                    <div
                                        className="flex-row space-around agent-block margin-top-15 margin-right-15 margin-left-15">
                                        <div className="flex-column">
                                            <span className="fs-15">Photo</span>
                                        </div>
                                        <div className="border-line"></div>
                                        <div className="flex-column">
                                            <span className="fs-15">Clien</span>
                                        </div>
                                        <div className="border-line"></div>
                                        <div className="flex-column">
                                            <span className="fs-15">
                                                Time
                                            </span>
                                        </div>
                                        <div className="border-line"></div>
                                        <div className="flex-column">
                                            <span className="fs-15">
                                                Description
                                            </span>
                                        </div>
                                        <div className="border-line"></div>
                                        <div className="flex-column">
                                            <span className="fs-15">
                                                Percent
                                            </span>
                                        </div>
                                        <div className="border-line"></div>
                                        <div className="flex-column">
                                            <span className="fs-15">
                                                Sum
                                            </span>
                                        </div>
                                        <div className="border-line"></div>
                                        <div className="flex-column">
                                            <span className="fs-15">
                                                Actions
                                            </span>
                                        </div>
                                        <div className="border-line"></div>
                                    </div>
                                    {customerDiscountArray ? customerDiscountArray.map(item =>
                                        <div
                                            className="flex-row space-around agent-block margin-top-15 margin-right-15 margin-left-15">
                                            <img
                                                style={{
                                                    width: "50px",
                                                    height: "50px",
                                                    borderRadius: "100px",
                                                }}
                                                src={item.clientPhotoId ? (config.BASE_URL + "/attachment/" + item.clientPhotoId) : "/assets/img/avatar.png"}
                                                alt=""
                                            />
                                            <div className="border-line"></div>
                                            <div className="flex-column">
                                                <span className="fs-20-bold">
                                                {item.clientFullName}
                                            </span>
                                            </div>
                                            <div className="border-line"></div>
                                            <div className="flex-column">
                                            <span className="fs-15">
                                                {item.createdAt}
                                            </span>
                                            </div>
                                            <div className="border-line"></div>
                                            <div className="flex-column">
                                            <span className="fs-15">
                                                {item.description} </span>
                                            </div>
                                            <div className="border-line"></div>
                                            <div className="flex-column">
                                            <span className="fs-15">
                                                {item.percent} </span>
                                            </div>
                                            <div className="border-line"></div>
                                            <div className="flex-column">
                                            <span className="fs-15">
                                                {item.sum} </span>
                                            </div>
                                            <div className="border-line"></div>
                                            <div className="flex-column">
                                                <EditIcon onClick={this.clickEditCustomDiscount(item)}/>
                                            </div>
                                            <div className="flex-column">
                                                <DeleteIcon onClick={() => openRemoveCustomDiscountModal(item.id)}/>
                                            </div>
                                            <div className="border-line"></div>
                                        </div>
                                    ) : ''}
                                    {/*map end*/}
                                </div>
                                <Row>
                                    <Col md={12}>
                                        <Pagination
                                            activePage={customerDiscountActivePage + 1}
                                            itemsCountPerPage={10}
                                            totalItemsCount={totalCustomerDiscountElements}
                                            pageRangeDisplayed={5}
                                            onChange={this.handlePageChange4.bind(this)} itemClass="page-item"
                                            linkClass="page-link"
                                        />
                                    </Col>
                                </Row>
                            </Row>
                        </div>
                    )}
                    {isExpenceTab && (
                        <div>
                            <Button color="primary" onClick={this.AgentDiscountResetModal}>Add agent discount</Button>
                            <Table className="custom-table margin-top-15">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Client</th>
                                    <th>Time</th>
                                    <th>Spent sum</th>
                                </tr>
                                </thead>
                                <tbody>
                                {/*map*/}
                                {discountExpenseArray ? discountExpenseArray.map((item, index) =>
                                    <tr key={index}>
                                        <td>{index + 1 + (discountExpenseActivePage * 10)}</td>
                                        <td>{item.clientFullName}</td>
                                        <td>{Moment(item.createdAt).format('lll')}</td>
                                        <td>${item.sum.toFixed(2)}</td>
                                    </tr>
                                ) : ''}
                                {/*map end*/}
                                </tbody>
                            </Table>
                            <Row>
                                <Col md={12}>
                                    <Pagination
                                        activePage={discountExpenseActivePage + 1}
                                        itemsCountPerPage={10}
                                        totalItemsCount={totalDiscountExpenseElements}
                                        pageRangeDisplayed={5}
                                        onChange={this.handlePageChange3.bind(this)} itemClass="page-item"
                                        linkClass="page-link"
                                    />
                                </Col>
                            </Row>
                        </div>
                    )}
                </div>
                <Modal
                    id="allModalStyle"
                    isOpen={this.state.agentDiscountModal}
                    toggle={this.AgentDiscountResetModal}
                >
                    <div className='position-absolute' style={{top: '45px', left: '-12%'}}>
                        <CloseBtn click={this.AgentDiscountResetModal}/>
                    </div>
                    <AvForm onValidSubmit={this.AgentDiscountReset}>
                        <ModalHeader>Add given agent discount</ModalHeader>
                        <ModalBody>
                            <div className="mt-3">
                                <AvField placeholder="Search Customer ..." className="form-control"
                                         onChange={getAgentsBySearch} list="browsers2" name="email"/>
                                <datalist id="browsers2">
                                    {agentsByDiscount ? agentsByDiscount.map(item =>
                                        <option key={item.id}
                                                value={item.email}>{item.fullName + " " + item.phoneNumber} </option>
                                    ) : ''}
                                </datalist>
                                <AvField className="form-control" placeholder="Enter given discount" name="amount"
                                         required/>
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            {/*<Button onClick={this.AgentDiscountResetModal} color="secondary">*/}
                            {/*    Cancel*/}
                            {/*</Button>*/}
                            <Button color="info" outline>
                                Save
                            </Button>{" "}

                        </ModalFooter>
                    </AvForm>
                </Modal>
                <Modal
                    id="allModalStyle"
                    isOpen={this.state.sharingModal}
                    toggle={this.SharingModal}
                >
                    <div className='position-absolute' style={{top: '45px', left: '-12%'}}>
                        <CloseBtn click={this.SharingModal}/>
                    </div>
                    <ModalHeader>Add {isTarifSharing ? "sharing" : isTariffFirstOrder ? "new client first order" : ''} discount</ModalHeader>
                    <ModalBody>
                        {isTarifSharing && (<Container>
                            <Row>
                                <Col md={10} className="margin-top-15">
                                    <div className="flex-row space-around">
                                        <label className={'labelDuo'}>
                                            <div className={'labelBlock'} style={{left: '-25%'}}>
                                                {isSelectAllzipCodes && <div className={'labelBox'}>
                                                    <i className="fas fa-check"></i>
                                                </div>}
                                            </div>
                                            <CustomInput
                                                className="check-box-title d-none"
                                                type="checkbox"
                                                checked={isSelectAllzipCodes}
                                                onChange={selectAllZipCode}
                                            />
                                            Add all zip code
                                        </label>

                                        <label className={'labelDuo'}>
                                            <div className={'labelBlock'}>
                                                {isOnlineOrInPerson && <div className={'labelBox'}>
                                                    <i className="fas fa-check"></i>
                                                </div>}
                                            </div>
                                            <CustomInput
                                                className="check-box-title d-none"
                                                type="checkbox"
                                                checked={isOnlineOrInPerson}
                                                onChange={selectOnlineOrInPerson}
                                            />
                                            Online
                                        </label>


                                        <label className={'labelDuo'}>
                                            <div className={'labelBlock'}>
                                                {isZipCodeActive && <div className={'labelBox'}>
                                                    <i className="fas fa-check"></i>
                                                </div>}
                                            </div>
                                            <CustomInput
                                                className="check-box-title d-none"
                                                type="checkbox"
                                                checked={isZipCodeActive}
                                                onChange={selectZipCodeActiveStatus}
                                            />
                                            Active
                                        </label>

                                    </div>
                                    <Select
                                        isDisabled={isSelectAllzipCodes || isOnlineOrInPerson}
                                        defaultValue="Select State"
                                        isMulti
                                        name="statesId"
                                        options={stateOptions}
                                        onChange={getCounty}
                                        className="basic-multi-select"
                                        classNamePrefix="select"
                                    />
                                    {selectStates !== null && selectStates.length > 0 ?
                                        <Select
                                            isDisabled={isSelectAllzipCodes || isOnlineOrInPerson}
                                            defaultValue="Select County"
                                            isMulti
                                            name="countiesId"
                                            options={countyOptions}
                                            onChange={getZipCode}
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        /> : ""}
                                    {selectCounties !== null && selectCounties.length > 0 ?
                                        <Select
                                            isDisabled={isSelectAllzipCodes || isOnlineOrInPerson}
                                            defaultValue="Select Zipcode"
                                            isMulti
                                            name="zipCodesId"
                                            options={zipCodeOptions}
                                            onChange={saveZipCodeForSharingDiscount}
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        /> : ""}
                                </Col>
                            </Row>
                            <Row>
                                <Col md={10}>
                                    <Input onChange={getSharingPercent} type="number"
                                           defaultValue={0}
                                           placeholder="Enter sharing tariff percent"/>
                                </Col>
                            </Row>
                        </Container>)}
                        {isTariffFirstOrder && (<Container>
                            <Row>
                                <Col md={10} className="margin-top-15">
                                    <div className="flex-row space-around">
                                        <label className={'labelDuo'}>
                                            <div className={'labelBlock'} style={{left: '-20%'}}>
                                                {isSelectAllzipCodes && <div className={'labelBox'}>
                                                    <i className="fas fa-check"></i>
                                                </div>}
                                            </div>
                                            <CustomInput
                                                className="check-box-title d-none"
                                                type="checkbox"
                                                checked={isSelectAllzipCodes}
                                                onChange={selectAllZipCode}
                                            />
                                            Add all zip code
                                        </label>

                                        <label className={'labelDuo'}>
                                            <div className={'labelBlock'}>
                                                {isOnlineOrInPerson && <div className={'labelBox'}>
                                                    <i className="fas fa-check"></i>
                                                </div>}
                                            </div>
                                            <CustomInput
                                                className="check-box-title d-none"
                                                type="checkbox"
                                                checked={isOnlineOrInPerson}
                                                onChange={selectOnlineOrInPerson}
                                            />
                                            Online
                                        </label>

                                        <label className={'labelDuo'}>
                                            <div className={'labelBlock'}>
                                                {isZipCodeActive && <div className={'labelBox'}>
                                                    <i className="fas fa-check"></i>
                                                </div>}
                                            </div>
                                            <CustomInput
                                                className="check-box-title d-none"
                                                type="checkbox"
                                                checked={isZipCodeActive}
                                                onChange={selectZipCodeActiveStatus}
                                            />
                                            Active
                                        </label>

                                    </div>
                                    <Select
                                        isDisabled={isSelectAllzipCodes}
                                        defaultValue="Select State"
                                        isMulti
                                        name="statesId"
                                        options={stateOptions}
                                        onChange={getCounty}
                                        className="basic-multi-select"
                                        classNamePrefix="select"
                                    />
                                    {selectStates !== null && selectStates.length > 0 ?
                                        <Select
                                            isDisabled={isSelectAllzipCodes}
                                            defaultValue="Select County"
                                            isMulti
                                            name="countiesId"
                                            options={countyOptions}
                                            onChange={getZipCode}
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        /> : ""}
                                    {selectCounties !== null && selectCounties.length > 0 ?
                                        <Select
                                            isDisabled={isSelectAllzipCodes}
                                            defaultValue="Select Zipcode"
                                            isMulti
                                            name="zipCodesId"
                                            options={zipCodeOptions}
                                            onChange={saveZipCodeForSharingDiscount}
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        /> : ""}
                                </Col>
                            </Row>
                            <Row>
                                <Col md={10}>
                                    <Input onChange={getSharingPercent} type="number"
                                           defaultValue={0}
                                           placeholder={isTarifSharing ? "Enter sharing tariff percent" : isTariffFirstOrder ? "Enter new client first order tariff percent" : ''}/>
                                </Col>
                            </Row>
                        </Container>)}

                    </ModalBody>
                    <ModalFooter>
                        <Button onClick={editSharingDiscountTariff} color="info" outline>
                            Save
                        </Button>{" "}
                        {/*<Button onClick={this.SharingModal} color="secondary">*/}
                        {/*    Cancel*/}
                        {/*</Button>*/}
                    </ModalFooter>
                </Modal>

                <Modal
                    id="allModalStyle"
                    isOpen={this.state.loyaltyModal}
                    toggle={this.LoyaltyModal}
                >
                    <div className='position-absolute' style={{top: '45px', left: '-12%'}}>
                        <CloseBtn click={this.LoyaltyModal}/>
                    </div>
                    <ModalHeader>Add {isTarifLoyalty ? "loyalty" : "custom"} discount</ModalHeader>
                    {isTarifLoyalty ?
                        <AvForm onValidSubmit={saveOrEditLoyaltyDiscountTariff}>
                            <ModalBody>
                                <Container>
                                    <Row>
                                        <Col md={10}>
                                            {/*<Input type="number" defaultValue={currentLoyaltyDiscountTariff?currentLoyaltyDiscountTariff.month:''} placeholder="Enter month" />*/}
                                            <AvField
                                                type="number"
                                                name="month"
                                                label="Month"
                                                required
                                                min={1}
                                                max={12}
                                                defaultValue={
                                                    currentLoyaltyDiscountTariff ? currentLoyaltyDiscountTariff.month
                                                        : ""
                                                }
                                                placeholder="Enter month"
                                            />
                                        </Col>
                                    </Row>
                                    <Row className="mt-2">
                                        <Col md={10}>
                                            {/*<Input type="number" defaultValue={currentLoyaltyDiscountTariff?currentLoyaltyDiscountTariff.percent:''} placeholder="Enter percent" />*/}
                                            <AvField
                                                type="number"
                                                name="percent"
                                                label="Percent"
                                                required
                                                defaultValue={
                                                    currentLoyaltyDiscountTariff ? currentLoyaltyDiscountTariff.percent
                                                        : ""
                                                }
                                                placeholder="Enter percent"
                                            />
                                        </Col>
                                    </Row>
                                    <Row className="mt-2">
                                        <Col md={10}>
                                            {/*<Input type="number" defaultValue={currentLoyaltyDiscountTariff?currentLoyaltyDiscountTariff.percentSum:''} placeholder="Enter percent sum" />*/}
                                            <AvField
                                                type="number"
                                                name="minSum"
                                                label="Minimum Discount Sum"
                                                required
                                                defaultValue={
                                                    currentLoyaltyDiscountTariff ? currentLoyaltyDiscountTariff.minSum
                                                        : ""
                                                }
                                                placeholder="Enter Minimum Discount Sum"
                                            />
                                        </Col>
                                    </Row>
                                </Container>
                            </ModalBody>
                            <ModalFooter>
                                <Button type="submit" color="info" outline>
                                    Save
                                </Button>{" "}
                                {/*<Button onClick={this.LoyaltyModal} color="secondary">*/}
                                {/*    Cancel*/}
                                {/*</Button>*/}
                            </ModalFooter>
                        </AvForm>
                        :
                        <AvForm onValidSubmit={saveOrEditLoyaltyDiscountTariff}>
                            <ModalBody>
                                <Container>
                                    {currentCustomDiscountTariff ?
                                        <Row>
                                            <Col md={10} className="margin-top-15">
                                                <div className="flex-row space-around">
                                                    <label className={'labelDuo'}>
                                                        <div className={'labelBlock'}>
                                                            {customTariffUnlimited && <div className={'labelBox'}>
                                                                <i className="fas fa-check"></i>
                                                            </div>}
                                                        </div>
                                                        <CustomInput
                                                            className="check-box-title d-none"
                                                            type="checkbox"
                                                            checked={customTariffUnlimited}
                                                            onChange={doUnlimitedForCustomDiscountTariff}
                                                        />
                                                        Unlimited
                                                    </label>


                                                    <label className={'labelDuo'}>
                                                        <div className={'labelBlock'}>
                                                            {customTariffActive && <div className={'labelBox'}>
                                                                <i className="fas fa-check"></i>
                                                            </div>}
                                                        </div>
                                                        <CustomInput
                                                            className="check-box-title d-none"
                                                            type="checkbox"
                                                            checked={customTariffActive}
                                                            onChange={changeActiveStatusForCustomDiscountTariff}
                                                        />
                                                        Active
                                                    </label>

                                                </div>
                                            </Col>
                                        </Row>
                                        :
                                        <Row>
                                            <Col md={10} className="margin-top-15">
                                                <div className="flex-row space-around">
                                                    <label className={'labelDuo'}>
                                                        <div className={'labelBlock'}>
                                                            {customTariffUnlimited && <div className={'labelBox'}>
                                                                <i className="fas fa-check"></i>
                                                            </div>}
                                                        </div>
                                                        <CustomInput
                                                            className="check-box-title d-none"
                                                            type="checkbox"
                                                            checked={customTariffUnlimited}
                                                            onChange={doUnlimitedForCustomDiscountTariff}
                                                        />
                                                        Unlimited
                                                    </label>

                                                    <label className={'labelDuo'}>
                                                        <div className={'labelBlock'}>
                                                            {customTariffActive && <div className={'labelBox'}>
                                                                <i className="fas fa-check"></i>
                                                            </div>}
                                                        </div>
                                                        <CustomInput
                                                            className="check-box-title d-none"
                                                            type="checkbox"
                                                            checked={customTariffActive}
                                                            onChange={changeActiveStatusForCustomDiscountTariff}
                                                        />
                                                        Active
                                                    </label>
                                                </div>
                                                <div className="mt-3">
                                                    <input placeholder="Search Customer ..." className="form-control"
                                                           onChange={getUsersBySearch} list="browsers" name="browser"/>
                                                    <datalist id="browsers">
                                                        {customDiscountTariffClientArray ? customDiscountTariffClientArray.map(item =>
                                                            <option key={item.id}
                                                                    value={item.email}>{item.fullName + " " + item.phoneNumber} </option>
                                                        ) : ''}
                                                    </datalist>
                                                </div>
                                            </Col>
                                        </Row>

                                    }

                                    <Row>
                                        <Col md={10} className="mt-3">

                                            <AvField
                                                type="number"
                                                name="percent"
                                                label="Percent"
                                                required
                                                defaultValue={
                                                    currentCustomDiscountTariff ? currentCustomDiscountTariff.percent
                                                        : ""
                                                }
                                                placeholder="Enter percent"
                                            />
                                            <AvField
                                                disabled={customTariffUnlimited}
                                                required={!customTariffUnlimited}
                                                type="number"
                                                name="count"
                                                label="Count"
                                                defaultValue={
                                                    currentCustomDiscountTariff ? currentCustomDiscountTariff.count
                                                        : ""
                                                }
                                                placeholder="Enter count"
                                            />

                                        </Col>
                                    </Row>
                                </Container>
                            </ModalBody>
                            <ModalFooter>
                                <Button type="submit" color="info" outline>
                                    Save
                                </Button>{" "}
                                {/*<Button onClick={this.LoyaltyModal} color="secondary">*/}
                                {/*    Cancel*/}
                                {/*</Button>*/}
                            </ModalFooter>
                        </AvForm>
                    }

                </Modal>

                <Modal
                    id="allModalStyle"
                    isOpen={this.state.customModal}
                    toggle={this.CustomModal}
                >
                    <div className='position-absolute' style={{top: '45px', left: '-12%'}}>
                        <CloseBtn click={this.CustomModal}/>
                    </div>
                    <ModalHeader>Edit custom discount</ModalHeader>
                    <AvForm onValidSubmit={editCustomDiscount}>
                        <ModalBody>
                            <Container>
                                <Row>
                                    <Col md={10}>
                                        <h4>{currentCustomDiscount ? currentCustomDiscount.clientFullName : ''}</h4>
                                    </Col>
                                </Row>
                                <Row className="mt-2">
                                    <Col md={10}>
                                        <h4>{currentCustomDiscount ? "Order date : " + currentCustomDiscount.createdAt : ""}</h4>
                                    </Col>
                                </Row>
                                <Row className="mt-2">
                                    <Col md={10}>
                                        <AvField
                                            type="number"
                                            name="month"
                                            label="Month"
                                            required
                                            min={1}
                                            max={12}
                                            defaultValue={
                                                currentCustomDiscount ? currentCustomDiscount.percent
                                                    : ""
                                            }
                                            placeholder="Enter Percent"
                                        />
                                    </Col>
                                </Row>
                                <Row className="mt-2">
                                    <Col md={10}>
                                        <AvField
                                            type="number"
                                            name="month"
                                            label="Month"
                                            required
                                            min={1}
                                            max={12}
                                            defaultValue={
                                                currentCustomDiscount ? currentCustomDiscount.sum
                                                    : ""
                                            }
                                            placeholder="Enter Sum"
                                        />
                                    </Col>
                                </Row>
                            </Container>
                        </ModalBody>
                        <ModalFooter>
                            <Button type="submit" color="info" outline>
                                Save
                            </Button>{" "}
                            {/*<Button onClick={this.CustomModal} color="secondary">*/}
                            {/*    Cancel*/}
                            {/*</Button>*/}
                        </ModalFooter>
                    </AvForm>
                </Modal>
                <Modal
                    id="allModalStyle"
                    isOpen={this.state.deleteModal}
                    toggle={this.DeleteModal}
                >
                    <ModalHeader
                        toggle={this.DeleteModal}
                        charCode="x"
                        className="model-head"
                    >
                        Do you want delete?
                    </ModalHeader>
                    <ModalBody
                        style={{backgroundColor: "#F9F9F9"}}
                    ></ModalBody>
                    <ModalFooter>
                        <Button
                            type="button"
                            color="secondary"
                            outline
                            onClick={this.DeleteModal}
                        >
                            Cancel
                        </Button>{" "}
                        <Button color="success" type="button"
                                onClick={() => removeButton(this.state.removingLoyaltyId ? "loyalty" : this.state.removingCustomDiscountId ? "custom" : this.state.removingCustomDiscountTariffId ? "customTariff" : '')}>
                            Delete
                        </Button>
                    </ModalFooter>
                </Modal>
            </AdminLayout>
        );
    }
}


export default connect(({
                            auth: {
                                permissions
                            },
                            discount: {
                                loading, isTarifTab, isGettingTab, isDiscountPercent, isExpenceTab, isTarifSharing, isTarifLoyalty, isTariffCustom, isGettingSharing, isGettingLoyalty,
                                isGettingCustomer, currentSharingDiscountTariff, currentLoyaltyDiscountTariff, loyaltyDiscountTariffList,
                                sharingGivenArray, totalSharingGivenElements, sharingGivenActivePage, loyaltyGivenArray,
                                totalLoyaltyGivenElements, loyaltyGivenActivePage, discountExpenseArray, totalDiscountExpenseElements,
                                discountExpenseActivePage, customerDiscountArray, totalCustomerDiscountElements, customerDiscountActivePage,
                                currentCustomDiscount, isSelectAllzipCodes, isZipCodeActive, currentCustomDiscountTariff,
                                customDiscountTariffArray,
                                customDiscountTariffClientArray,
                                customDiscountTariffTotalElements,
                                customDiscountTariffActivePage,
                                zipCodeArray,
                                customTariffUnlimited, customTariffActive,
                                isTariffFirstOrder, currentFirstOrderDiscountTariff, isOnlineOrInPerson
                            }, app: {
        agentsByDiscount,
        discountPercents, zipCodes, showModal, currentItem, active,
        showDeleteModal, showStatusModal, states,
        activePageDisPer,
        totalElementsDisPer,
        discountPercentPage, online, showOnlineModal, stateOptions, showEditModal
    },
                            service: {
                                zipCodeOptions, countyOptions, all,
                                selectZipCodes, selectCounties, selectStates,
                            },

                            zipCode: {countyArray, stateArray}
                        }) =>
    ({
        permissions,
        agentsByDiscount,
        discountPercents,
        zipCodes,
        showModal,
        currentItem,
        active,
        showDeleteModal,
        showStatusModal,
        states,
        activePageDisPer,
        totalElementsDisPer,
        discountPercentPage,
        online,
        showOnlineModal,
        loading,
        isTarifTab,
        isGettingTab,
        isExpenceTab,
        isTarifSharing,
        isDiscountPercent,
        isTarifLoyalty,
        isTariffCustom,
        isGettingSharing,
        isGettingLoyalty,
        isGettingCustomer,
        currentSharingDiscountTariff,
        currentLoyaltyDiscountTariff,
        loyaltyDiscountTariffList,
        sharingGivenArray,
        totalSharingGivenElements,
        sharingGivenActivePage,
        loyaltyGivenArray,
        totalLoyaltyGivenElements,
        loyaltyGivenActivePage,
        discountExpenseArray,
        totalDiscountExpenseElements,
        discountExpenseActivePage,
        customerDiscountArray,
        totalCustomerDiscountElements,
        customerDiscountActivePage,
        currentCustomDiscount,
        isSelectAllzipCodes,
        isZipCodeActive,
        stateOptions,
        zipCodeOptions,
        countyOptions,
        selectZipCodes,
        selectCounties,
        selectStates,
        currentCustomDiscountTariff,
        customDiscountTariffArray,
        customDiscountTariffClientArray,
        customDiscountTariffTotalElements,
        customDiscountTariffActivePage,
        zipCodeArray,
        countyArray,
        stateArray,
        customTariffUnlimited,
        customTariffActive,
        isTariffFirstOrder,
        currentFirstOrderDiscountTariff,
        isOnlineOrInPerson, showEditModal, all
    }))(Discounts);

function DeleteIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{cursor: "pointer"}}
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M2 6H18V19C18 19.2652 17.8946 19.5196 17.7071 19.7071C17.5196 19.8946 17.2652 20 17 20H3C2.73478 20 2.48043 19.8946 2.29289 19.7071C2.10536 19.5196 2 19.2652 2 19V6ZM4 8V18H16V8H4ZM7 10H9V16H7V10ZM11 10H13V16H11V10ZM5 3V1C5 0.734784 5.10536 0.48043 5.29289 0.292893C5.48043 0.105357 5.73478 0 6 0H14C14.2652 0 14.5196 0.105357 14.7071 0.292893C14.8946 0.48043 15 0.734784 15 1V3H20V5H0V3H5ZM7 2V3H13V2H7Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}



