import React, {Component} from 'react';
import AdminLayout from "../../components/AdminLayout";
import {Button, Col, Modal, ModalBody, ModalHeader, Row} from "reactstrap";
import {addAnswer, editFeedBackAction, getFeedBacksAction} from "../../redux/actions/AppAction";
import {connect} from "react-redux";
import {config} from "../../utils/config";
import {AvField, AvForm} from "availity-reactstrap-validation";
import PaginationComponent from "react-reactstrap-pagination";
import ModalFooter from "reactstrap/es/ModalFooter";
import CloseBtn from "../../components/CloseBtn";

class Feedback extends Component {
    constructor(props) {
        super(props);
        this.state = {
            agents: false,
            activePage: 1
        };
    }

    ShowInperson = () => {
        this.setState({
            agents: !this.state.agents
        });
    };

    handlePageChange(pageNumber, boolen) {
        this.props.dispatch(getFeedBacksAction({
            page: pageNumber - 1,
            size: this.props.size,
            isAgent: !this.state.agents
        }));
        this.setState({activePage: pageNumber})
    }

    componentDidMount() {
        this.props.dispatch(getFeedBacksAction({
            page: 0,
            size: this.props.size,
            isAgent: !this.state.agents
        }));

    }

    render() {
        const {feedBacks, isAdmin, page, size, dispatch, isUser, isAgent, currentItem, showModal, totalElements} = this.props;
        const changeIsAgent = (bool) => {
            dispatch(getFeedBacksAction({
                page: 0,
                size: this.props.size,
                isAgent: bool
            }));
            this.setState({
                agents: !this.state.agents
            });
        }
        const handleSelected = (selectedPage) => {
            dispatch(getFeedBacksAction({page: selectedPage - 1, size: size, isAgent: !this.state.agents}))
            dispatch(
                {
                    type: 'updateState',
                    payload: {
                        page: selectedPage - 1
                    }
                }
            )
        }
        const editFeedback = (e, v) => {
            v.id = currentItem.id
            v.orderDto = currentItem.orderDto;
            v.agent = isAgent;
            v.operationEnum = "UPDATE";
            v.seen = false;
            dispatch(editFeedBackAction(v))
        }
        const answerFeedback = (e, v) => {
            v.id = currentItem.id
            dispatch(addAnswer(v, !this.state.agents))
        }
        const openModal = (boolean, item) => {
            dispatch({type: 'updateState', payload: {showModal: boolean, currentItem: item}})
        }
        return (
            <AdminLayout pathname={this.props.location.pathname}>
                <div className="feedback-admin-page container">
                    <div className="d-flex justify-content-end">
                        <div className="route-services">
                            <div onClick={() => changeIsAgent(true)} className={this.state.agents ?
                                "online" : "router-active"}>Agent
                            </div>
                            <div onClick={() => changeIsAgent(false)} className={!this.state.agents ?
                                "in-person" : "router-active"}>Client
                            </div>
                        </div>
                    </div>
                    {feedBacks ? !this.state.agents && feedBacks.agents.length > 0 ?
                        <div className="sharing-history-row">
                            <div className="row">
                                <div className="col font-weight-bold">Total agents feedback : {totalElements}</div>
                            </div>
                            {feedBacks.agents.map(feedBack =>
                                <Col md={12} key={feedBack.id} onClick={() => openModal(true, feedBack)}
                                     className="history-col">
                                    <Row>
                                        <Col md={2}>
                                            <div className="d-flex">
                                                <div className="avatar-img">
                                                    <img className="agent-avatar"
                                                         src={feedBack.orderDto.agent.photoId ? config.BASE_URL + "/attachment/" + feedBack.orderDto.agent.photoId : "/assets/img/avatar.png"}
                                                         alt=""/>
                                                </div>
                                                <div className="ml-3">
                                                    <div className="fs-15">Clients</div>
                                                    <div className="fs-20-bold">
                                                        {feedBack.orderDto.agent.firstName + " " + feedBack.orderDto.agent.lastName}
                                                    </div>
                                                </div>
                                            </div>
                                        </Col>
                                        <Col md={1}>
                                            <div className="border-line"/>
                                        </Col>
                                        <Col md={1} className="px-0">
                                            <div className="fs-15">Client</div>
                                            <div className="fs-15-bold pt-2">
                                                {feedBack.orderDto.client.firstName + " " + feedBack.orderDto.client.lastName}
                                            </div>
                                        </Col>
                                        <Col md={1}>
                                            <div className="border-line"/>
                                        </Col>
                                        <Col md={2}>
                                            <div className="fs-15">
                                                {feedBack.orderDto.agent.phoneNumber}
                                            </div>
                                            <div className="fs-15-bold pt-2">
                                                {feedBack.orderDto.agent.email}
                                            </div>
                                        </Col>
                                        <Col md={1}>
                                            <div className="border-line"/>
                                        </Col>
                                        <Col md={1}>
                                            <div className="fs-15-bold">Amount</div>
                                            <div className="fs-20-bold pt-2">
                                                {"$" + feedBack.orderDto.amount}
                                            </div>
                                        </Col>
                                        <Col md={1}>
                                            <div className="border-line"/>
                                        </Col>
                                        <Col md={1}>
                                            <div className="fs-15">Rate</div>
                                            <div className="fs-15-bold pt-2">
                                                {feedBack.rate}
                                            </div>
                                        </Col>
                                        <Col md={1}>
                                            <div className="fs-15">Status</div>
                                            <div className="fs-15-bold pt-2">
                                                {feedBack.seen ? "Answered" : 'Pending'}
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12}>
                                            <div className="feedback-text-admin">
                                                <b className="font-weight-bold ">Agent</b> : {feedBack.description}
                                            </div>
                                        </Col>
                                        {feedBack.seen ?
                                            <Col md={12}>
                                                <div className="feedback-text-admin">
                                                    <b className="font-weight-bold ">Admin</b> : {feedBack.answer}
                                                </div>
                                            </Col>
                                            : ""}
                                    </Row>
                                </Col>)}
                        </div>
                        : !this.state.agents && feedBacks.agents.length < 1 ?
                            <h1 className="text-center">No information</h1> :
                            feedBacks.clients.length > 0 ?
                                <Row className="sharing-history-row px-3">
                                    <div className="row">
                                        <div className="col font-weight-bold">Total clients feedback
                                            : {totalElements}</div>
                                    </div>
                                    {feedBacks.clients.map(feedBack =>
                                        <Col md={12} key={feedBack.id} className="history-col"
                                             onClick={() => openModal(true, feedBack)}>
                                            <Row>
                                                <Col md={2}>
                                                    <div className="d-flex">
                                                        <div className="avatar-img">
                                                            <img
                                                                src={feedBack.orderDto.client.photoId ? config.BASE_URL + "/attachment/" + feedBack.orderDto.client.photoId : "/assets/img/avatar.png"}
                                                                alt=""/>
                                                        </div>
                                                        <div className="ml-3">
                                                            <div className="fs-15">Clients</div>
                                                            <div className="fs-20-bold">
                                                                {feedBack.orderDto.client.firstName + " " + feedBack.orderDto.client.lastName}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Col>
                                                <Col md={1}>
                                                    <div className="border-line"/>
                                                </Col>
                                                <Col md={1} className="px-0">
                                                    <div className="fs-15">Agent</div>
                                                    <div className="fs-15-bold pt-2">
                                                        {feedBack.orderDto.agent.firstName + " " + feedBack.orderDto.agent.lastName}
                                                    </div>
                                                </Col>
                                                <Col md={1}>
                                                    <div className="border-line"/>
                                                </Col>
                                                <Col md={2}>
                                                    <div className="fs-15">
                                                        {feedBack.orderDto.client.phoneNumber}
                                                    </div>
                                                    <div className="fs-15-bold pt-2">
                                                        {feedBack.orderDto.client.email}
                                                    </div>
                                                </Col>
                                                <Col md={1}>
                                                    <div className="border-line"/>
                                                </Col>
                                                <Col md={1}>
                                                    <div className="fs-15-bold">Amount</div>
                                                    <div className="fs-20-bold pt-2">
                                                        {'$' + feedBack.orderDto.amount}
                                                    </div>
                                                </Col>
                                                <Col md={1}>
                                                    <div className="border-line"/>
                                                </Col>
                                                <Col md={1}>
                                                    <div className="fs-15">Rate</div>
                                                    <div className="fs-15-bold pt-2">
                                                        {feedBack.rate}
                                                    </div>
                                                </Col>
                                                <Col md={1}>
                                                    <div className="fs-15">Status</div>
                                                    <div className="fs-15-bold pt-2">
                                                        {feedBack.seen ? "Answered" : 'Pending'}
                                                    </div>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={12}>
                                                    <div className="feedback-text-admin">
                                                        <b className="font-weight-bold ">Client</b> : {feedBack.description}
                                                    </div>
                                                </Col>
                                                {feedBack.seen ?
                                                    <Col md={12}>
                                                        <div className="feedback-text-admin">
                                                            <b className="font-weight-bold ">Admin</b> : {feedBack.answer}
                                                        </div>
                                                    </Col>
                                                    : ""}
                                            </Row>
                                        </Col>
                                    )}

                                </Row> : <h1 className="text-center">No information</h1> : ''}


                    <Row>
                        {feedBacks.clients && feedBacks.clients.length > 0 || feedBacks.agents && feedBacks.agents.length > 0 ?
                            <Col md={6} className="pagination-bottom mx-auto mb-2">
                                <PaginationComponent
                                    defaultActivePage={page + 1}
                                    totalItems={totalElements} pageSize={size}
                                    onSelect={handleSelected}
                                /></Col> : ''}
                    </Row>
                    {currentItem && showModal && isAdmin ?
                        <Modal id="allModalStyle" isOpen={showModal} toggle={() => openModal(false)}>
                            <AvForm onValidSubmit={answerFeedback}>
                                <div className='position-absolute' style={{top: '45px', left: '-12%'}}>
                                    <CloseBtn click={() => openModal(false)}/>
                                </div>
                                <ModalHeader>Answer
                                    to {currentItem.agent ? currentItem.orderDto.agent.firstName + " " + currentItem.orderDto.agent.lastName : currentItem.orderDto.client.firstName + " " + currentItem.orderDto.client.lastName}</ModalHeader>
                                <ModalBody><Col md={12}><AvField defaultValue={currentItem && currentItem.answer}
                                                                 type="textarea" style={{height: '10rem'}} name='answer'
                                                                 placeholder="enter your answer" required/>
                                </Col>
                                </ModalBody>
                                <ModalFooter>
                                    {/*<Button type="button" */}
                                    {/*        className="pr-4 ml-4 mr-3"*/}
                                    {/*        onClick={() => openModal(false)}*/}
                                    {/*>*/}
                                    {/*    Cancel*/}
                                    {/*</Button>*/}
                                    <Button color="info" outline>Send</Button></ModalFooter>
                            </AvForm>
                        </Modal>
                        : ''}
                </div>
            </AdminLayout>
        );
    }
}


export default connect(({
                            app: {
                                feedBacks,
                                page,
                                size,
                                currentItem,
                                showModal,
                                totalElements
                            },
                            auth: {
                                isAdmin,
                                isAgent,
                                isUser,
                                currentUser
                            }
                        }) => ({
    feedBacks, page, size, isAdmin, isUser, isAgent, currentUser, currentItem, showModal, totalElements
}))(Feedback);
