import React, {Component, createRef} from 'react';
import AdminLayout from "../../components/AdminLayout";
import {Button, Col,  Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import Switch from "react-switch";
import {connect} from "react-redux";
import {
    editServiceChangeActive,
    getCountyByState,
    getServiceList,
    getServicePriceDashboard,
    getStateList,
    getZipCodeByCounty,
    saveServicePrice
} from "../../redux/actions/AppAction";
import {AvField, AvForm} from "availity-reactstrap-validation";
import Select from "react-select";
import StatusModal from "../../components/Modal/StatusModal";
import {AddIcon} from "../../components/Icons";
import CloseBtn from "../../components/CloseBtn";

class ServicePrice extends Component {
    componentDidMount() {
        this.props.dispatch(getServiceList())
        this.props.dispatch(getServicePriceDashboard())
        this.props.dispatch(getStateList())
    }

    constructor(props) {
        super(props);
        this.state = {
            inPerson: false,
            checked: false,
            isOnline: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.select = createRef()
    }

    handleChange(checked) {
        this.setState({checked});
    }

    ShowInperson = () => {
        this.setState({
            inPerson: !this.state.inPerson
        });
    };

    handleSelect = (id) => {
        this.props.services.map(a => {
            if (a.id === id) {
                this.setState({isOnline: a.mainService.online})
            }
        })
    }


    render() {
        const {
            selectZipCodes, selectCounties, selectStates,
            dispatch, stateOptions, countyOptions, zipCodeOptions, showServiceModal,
            services, dashboard, currentService, all, active, showStatusModal, mainServices, servicePrices, loading,
        } = this.props;

        const saveItem = (e, v) => {
            v.id = currentService != null ? currentService.id : null
            v.active = active
            v.all = all
            v.statesId = this.state.isOnline?null: selectCounties !== null ? [] : selectStates
            v.countiesId =this.state.isOnline?null: selectZipCodes !== null ? [] : selectCounties
            v.zipCodesId =this.state.isOnline?null: selectZipCodes !== null ? selectZipCodes : []
            this.props.dispatch(saveServicePrice(v))
        }
        const changeStatusServicePrice = () => {
            let currentServicePrice = {...currentService};
            currentServicePrice.active = !currentService.active;
            this.props.dispatch(editServiceChangeActive(currentServicePrice))
        }
        const openModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    selectCounties:null,
                    selectZipCodes:null,
                    selectStates:null,
                    showServiceModal: !showServiceModal,
                    currentService: item,
                    active: item.active,
                    all: item.all,

                }
            })
        };
        const openStatusModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showStatusModal: !showStatusModal,
                    currentService: item
                }
            })
        };

        const changeActive = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    active: !active
                }
            })
        };
        const changeAll = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    all: !all
                }
            })
        };
        const getCounty = (e, v) => {
            if (!all) {
                let arr = [];
                if (e && e.length === 1) {
                    if (v.action === "remove-value") {
                        v.option = e[0];
                    }
                    dispatch(getCountyByState(v));
                }
                if (e) {
                    e.map((item) => arr.push(item.value));
                    dispatch({
                        type: "updateState",
                        payload: {
                            selectStates: arr,
                            selectCounties: null,
                        },
                    });
                } else {
                    dispatch({
                        type: "updateState",
                        payload: {
                            selectCounties: null,
                            selectZipCodes: null,
                        },
                    });
                }
            }
        };
        const getZipCode = (e, v) => {
            let arr = [];
            if (e && e.length === 1) {
                if (v.action === "remove-value") {
                    v.option = e[0];
                }
                dispatch(getZipCodeByCounty(v));
            }
            if (e) {
                e.map((item) => arr.push(item.value));
                dispatch({
                    type: "updateState",
                    payload: {
                        selectCounties: arr,
                    },
                });
            } else {
                dispatch({
                    type: "updateState",
                    payload: {
                        selectCounties: null,
                        selectZipCodes: null,
                    },
                });
            }
        };
        const saveZipCodeForServicePrice = (e, v) => {
            let arr = [];
            if (e) {
                e.map((item) => arr.push(item.value));
                dispatch({
                    type: "updateState",
                    payload: {
                        selectZipCodes: arr,
                    },
                });
            } else {
                dispatch({
                    type: "updateState",
                    payload: {
                        selectZipCodes: null,
                    },
                });
            }
        };
        return (
            <AdminLayout pathname={this.props.location.pathname}>
                <div className="admin-service-page container">
                    <Row>
                        <Col md={3}>
                            <AddIcon
                                title={true}
                                onClick={() => openModal("")}
                            />
                        </Col>
                        <Col>
                            <div className="route-services ml-auto">
                                <div onClick={this.ShowInperson} className={this.state.inPerson ?
                                    "online" : "router-active"}>Online
                                </div>
                                <div onClick={this.ShowInperson} className={!this.state.inPerson ?
                                    "in-person" : "router-active"}>In-person
                                </div>
                            </div>
                        </Col>
                    </Row>

                    {this.state.inPerson ?
                        dashboard.filter(o => !o.online).map(item =>
                            <Row className="mt-4">
                                <Col md={12}>
                                    <div className="services-section">
                                        <Row>
                                            <Col md={6}>
                                                <div className={!item.serviceActive ?
                                                    "notary-text" : "notary-text-active"}>
                                                    {item.subServiceName}
                                                </div>
                                                <div className="notary-commit">
                                                    {item.subServiceDescription}
                                                </div>
                                                <div className={!item.serviceActive ?
                                                    "notary-price" : "notary-price-active"}>
                                                    {item.minPrice === item.maxPrice ? "$" + item.minPrice :
                                                        "$" + item.minPrice + " - " + "$" + item.maxPrice
                                                    }
                                                </div>
                                            </Col>
                                            <Col md={2} className="offset-4">
                                                <div className="edit-and-switch">
                                                    <div className="edit-services " onClick={() => openModal(item)}>
                                                        <img src="/assets/icons/edit.png" alt=""/>
                                                    </div>
                                                    <div className="mt-2 pt-1 ml-3">
                                                        <Switch
                                                            onChange={() => openStatusModal(item)}
                                                            id={item.serviceId}
                                                            checked={item.serviceActive}
                                                        />
                                                    </div>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                </Col>
                            </Row>
                        )
                        :
                        dashboard.filter(o => o.online).map(item =>
                            <Row className="mt-4">
                                <Col md={12}>
                                    <div className="services-section">
                                        <Row>
                                            <Col md={6}>
                                                <div className={!item.serviceActive ?
                                                    "notary-text" : "notary-text-active"}>
                                                    <td>{item.subServiceName}</td>
                                                </div>
                                                <div className="notary-commit">
                                                    {item.subServiceDescription}
                                                </div>
                                                <div className={!item.serviceActive ?
                                                    "notary-price" : "notary-price-active"}>
                                                    {item.minPrice === item.maxPrice ? "$" + item.minPrice :
                                                        "$" + item.minPrice + " - " + "$" + item.maxPrice
                                                    }
                                                </div>
                                            </Col>
                                            <Col md={2} className="offset-4">
                                                <div className="edit-and-switch">
                                                    <div className="edit-services " onClick={() => openModal(item)}>
                                                        <img src="/assets/icons/edit.png" alt=""/>
                                                    </div>
                                                    <div className="mt-2 pt-1 ml-3">
                                                        <Switch
                                                            onChange={() => openStatusModal(item)}
                                                            id={item.serviceId}
                                                            checked={item.serviceActive}
                                                        />
                                                    </div>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                </Col>
                            </Row>
                        )
                    }
                    <Modal id="allModalStyle"
                           isOpen={showServiceModal} toggle={openModal}>
                        <AvForm onValidSubmit={saveItem}>
                            <div className='position-absolute' style={{top : '45px', left : '-12%'}}>
                                <CloseBtn click={openModal} />
                            </div>
                            <ModalHeader
                                toggle={openModal}
                                className="model-head"
                            >
                                {currentService != null && currentService.serviceId
                                    ? "Edit  Service price"
                                    : "Add  Service price"}
                            </ModalHeader>
                            <ModalBody>
                                <div className="container">
                                    <div className="row">
                                        <Col md={12}>
                                            {currentService&& currentService.online!=null&&currentService.online ?
                                                <AvField
                                                    type="select"
                                                    onChange={(a) => this.handleSelect(a.target.value)}
                                                    disabled
                                                    name="serviceId"
                                                    value={currentService  ?
                                                        (currentService.serviceDto  ?
                                                            currentService.serviceDto.mainServiceDto.name + " " +
                                                            "" + currentService.serviceDto.subServiceDto.name :
                                                            currentService.serviceId) : "0"}
                                                    required
                                                >
                                                    <option value="0" selected>Select Service</option>
                                                    {services.map(item =>
                                                        <option key={item}
                                                                value={item.id}>{item.mainService.name} - {item.subService.name}</option>
                                                    )}
                                                </AvField>
                                                :
                                                <AvField
                                                    type="select"
                                                    onChange={(a) => this.handleSelect(a.target.value)}
                                                    name="serviceId"
                                                    value={currentService != null ?
                                                        (currentService.serviceDto != null ?
                                                            currentService.serviceDto.mainServiceDto.name + " " +
                                                            "" + currentService.serviceDto.subServiceDto.name :
                                                            currentService.serviceId) : "0"}
                                                    required
                                                >
                                                    <option value="0" selected>Select Service</option>
                                                    {services.map(item =>
                                                        <option key={item}
                                                                value={item.id}>{item.mainService.name} - {item.subService.name}</option>
                                                    )}
                                                </AvField>
                                            }
                                        </Col>
                                        <Col md={12}>
                                            <AvField
                                                name="price"
                                                label="Price"
                                                type="number"
                                                required
                                                placeholder="Enter price"
                                            />
                                        </Col>
                                        <Col md={12}>
                                            <div>
                                                <AvField
                                                    name="chargeMinute"
                                                    label="Charge minute"
                                                    type="number"
                                                    required
                                                    placeholder="Enter Charge minute"/>

                                            </div>
                                        </Col>
                                        <Col md={12}>
                                            <div>
                                                <AvField
                                                    name="chargePercent"
                                                    label="Charge percent"
                                                    type="number"
                                                    required
                                                    placeholder="Enter Charge percent"/>
                                            </div>
                                        </Col>
                                        <Col md={12}>
                                            <div className="flex-row space-around">
                                                {this.state.isOnline ||currentService&& currentService.online!=null&&currentService.online ?
                                                         "" :
                                                    <label className={'labelDuo'}>
                                                        <div className={'labelBlock'} style={{left : '-20%'}}>
                                                            {all && <div className={'labelBox'}>
                                                                <i className="fas fa-check"/>
                                                            </div>}
                                                        </div>
                                                        <input
                                                            className="d-none"
                                                            type="checkbox"
                                                            checked={all}
                                                            onChange={changeAll}
                                                        />
                                                        <span>Add all zip code</span>
                                                    </label>
                                                }
                                                <label className={'labelDuo'}>
                                                    <div className={'labelBlock'}>
                                                        {active && <div className={'labelBox'}>
                                                            <i className="fas fa-check"></i>
                                                        </div>}
                                                    </div>
                                                    <input
                                                        className="check-box-title d-none"
                                                        type="checkbox"
                                                        checked={active}
                                                        onChange={changeActive}
                                                    />
                                                    Active
                                                </label>

                                            </div>
                                        </Col>
                                    </div>
                                </div>
                                {this.state.isOnline ||currentService&& currentService.online!=null&&currentService.online? "" :
                                    <Select
                                        isDisabled={all}
                                        defaultValue="Select State"
                                        isMulti
                                        name="statesId"
                                        options={stateOptions}
                                        onChange={getCounty}
                                        className="basic-multi-select"
                                        classNamePrefix="select"
                                    />
                                }
                                {this.state.isOnline ||currentService&& currentService.online!=null&&currentService.online? "" :
                                    selectStates !== null && selectStates.length === 1 ?
                                        <Select
                                            isDisabled={all}
                                            defaultValue="Select County"
                                            isMulti
                                            name="countiesId"
                                            options={countyOptions}
                                            onChange={getZipCode}
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        /> : ""
                                }
                                {this.state.isOnline ||currentService&& currentService.online!=null&&currentService.online? "" :
                                    selectCounties !== null && selectCounties.length === 1 ?
                                        <Select
                                            isDisabled={all}
                                            defaultValue="Select Zipcode"
                                            isMulti
                                            name="zipCodesId"
                                            options={zipCodeOptions}
                                            onChange={saveZipCodeForServicePrice}
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        />
                                        : ""
                                }
                            </ModalBody>
                            <ModalFooter>
                                {/*<Button*/}
                                {/*    type="button"*/}
                                {/*    color="secondary"*/}
                                {/*    outline*/}
                                {/*    onClick={openModal}*/}
                                {/*>*/}
                                {/*    Cancel*/}
                                {/*</Button>{" "}*/}
                                <Button color="info" outline disabled={loading}>Save</Button>
                            </ModalFooter>
                        </AvForm>

                    </Modal>

                    {showStatusModal && <StatusModal
                        text={currentService.name}
                        showStatusModal={showStatusModal}
                        confirm={changeStatusServicePrice}
                        cancel={openStatusModal}/>}
                </div>
            </AdminLayout>
        );
    }
}

export default connect(
    ({
         service: {
             zipCodeOptions, countyOptions, dashboard, servicePrices, showServiceModal, currentService, all,
             active, selectZipCodes, selectCounties, selectStates,
         },
         app: {
             stateOptions,
             zipCodes,
             states, counties, showDeleteModal,
             showStatusModal, services, mainServices,loading
         }
     }) => ({
        dashboard,
        selectZipCodes, selectCounties, selectStates,
        stateOptions, countyOptions, zipCodeOptions,
        zipCodes,
        states,
        counties,
        showServiceModal,
        currentService, all,
        active, showDeleteModal, showStatusModal, services, servicePrices, mainServices, loading
    }))(ServicePrice);

