import React, {Component} from "react";
import AdminLayout from "../../components/AdminLayout";
import {
    deleteService,
    getMainServiceList,
    getServicePage,
    getSubServiceList,
    saveFirstService,
    saveService,
} from "../../redux/actions/AppAction";
import {
    Button,
    CustomInput,
    FormGroup,
    Input,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Row,
    Table,
} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import DeleteModal from "../../components/Modal/DeleteModal";
import StatusModal from "../../components/Modal/StatusModal";
import DefaultInputModal from "../../components/Modal/DefaultInputModal";
import {connect} from "react-redux";
import Pagination from "react-js-pagination";
import Select from "react-select";
import {AddIcon} from "../../components/Icons";
import CloseBtn from "../../components/CloseBtn";

class Service extends Component {
    componentDidMount() {
        this.props.dispatch(getServicePage({page: 0, size: 10}));
        this.props.dispatch(saveFirstService())
    }

    constructor(props) {
        super(props);
        this.state = {
            inPerson: false,
            checked: false,
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(checked) {
        this.setState({checked});
    }
    handlePageChange(pageNumber) {
        this.props.dispatch(getServicePage({page: pageNumber - 1, size: 10}))
        this.props.dispatch(
            {
                type: 'updateState',
                payload: {
                    activePageService: pageNumber - 1
                }
            })
    }

    render() {
        const {
            dispatch,
            showModal,
            currentItem,
            active,
            dynamic,
            showDeleteModal,
            showStatusModal,
            showDynamicModal,
            mainServices,
            subServices,
            activePageService,
            totalElementsService,
            servicePage,
            mainServicesMultiSelect,
            serviceEnum,
            selectMainServices,
            showEditModal,
            loading
        } = this.props;



        const openModal = (item) => {
            this.props.dispatch(getMainServiceList());
            dispatch({
                type: "updateState",
                payload: {
                    showModal: !showModal,
                    currentItem: item,
                    active: item.active,
                    dynamic: item.dynamic,
                },
            });
        };
        const editServiceModal = (item) => {
            this.props.dispatch(getMainServiceList());
            this.props.dispatch(getSubServiceList());
            dispatch({
                type: "updateState",
                payload: {
                    showEditModal: !showEditModal,
                    currentItem: item,
                    active: item.active,
                    dynamic: item.dynamic,
                    defaultInput: item.subServiceDto ? item.subServiceDto.defaultInput : false,
                },
            });
        };
        const openDeleteModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentItem: item,
                },
            });
        };
        const changeActive = () => {
            dispatch({
                type: "updateState",
                payload: {
                    active: !active,
                },
            });
        };
        const changeDynamic = () => {
            dispatch({
                type: "updateState",
                payload: {
                    dynamic: !dynamic,
                },
            });
        };

        const deleteFunction = () => {
            this.props.dispatch(deleteService(currentItem));
        };
        const saveItem = (e, v) => {
            v.id = currentItem ? currentItem.id : '';
            v.active = active;
            v.dynamic = dynamic;
            v.mainServices = selectMainServices
            if (selectMainServices && selectMainServices.length > 0)
                this.props.dispatch(saveService(v))
        };
        const editItem = (e, v) => {
            v.id = currentItem ? currentItem.id : '';
            v.active = active;
            v.dynamic = dynamic;
            // v.defaultInput = defaultInput;
            // v.mainServices =  selectMainServices
            this.props.dispatch(saveService(v));
        };

        const changeStatusService = () => {
            let currentService = {...currentItem};
            currentService.active = !currentItem.active;
            this.props.dispatch(saveService(currentService));
        };

        const changeDynamicService = () => {
            let currentService = {...currentItem};
            currentService.dynamic = !currentItem.dynamic;
            this.props.dispatch(saveService(currentService));
        };

        const openStatusModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item,
                },
            });
        };

        const openDynamicModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showDynamicModal: !showDynamicModal,
                    currentItem: item,
                },
            });
        };
        const getZipCode = (e, v) => {
            let arr = [];
            if (e && e.length === 1) {
                if (v.action === "remove-value") {
                    v.option = e[0];
                }
            }
            if (e) {
                e.map((item) => arr.push(item.value));
                dispatch({
                    type: "updateState",
                    payload: {
                        selectMainServices: arr,
                    },
                });
            } else {
                dispatch({
                    type: "updateState",
                    payload: {
                        selectCounties: null,
                        selectZipCodes: null,
                    },
                });
            }
        };
        return (
            <AdminLayout pathname={this.props.location.pathname}>
                <div className="main-service-page container">
                    <AddIcon title={true} onClick={() => openModal("")}/>
                    <Table className="custom-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Service</th>
                            <th>Initial Count - Time</th>
                            <th>Every Count - Time</th>
                            <th>Dynamic</th>
                            <th>Status</th>
                            <th colSpan="2">Operation</th>
                        </tr>
                        </thead>

                        {servicePage.length > 0 ? (
                            <tbody>
                            {servicePage.map((item, i) => (
                                <tr className="block" key={item.id}>
                                    <td>{i + 1}</td>
                                    <td>
                                        {item.mainServiceDto.name}-
                                        {item.subServiceDto.name}
                                    </td>
                                    <td>{item.initialCount} - {item.initialSpendingTime}</td>
                                    <td>
                                        {item.dynamic
                                            ? item.everyCount + " - " + item.everySpendingTime
                                            : ""}
                                    </td>
                                    <td>
                                        <FormGroup check>
                                            <label className={'labelDuo'}>
                                                <div className={'labelBlock'} style={{
                                                    left : '-35%'
                                                }}>
                                                    {item.dynamic && <div className={'labelBox'}>
                                                        <i className="fas fa-check"/>
                                                    </div>}
                                                </div>
                                                <Input
                                                    className={'d-none'}
                                                    type="checkbox"
                                                    onClick={() =>
                                                        openDynamicModal(
                                                            item
                                                        )
                                                    }
                                                    checked={item.dynamic}
                                                />
                                                Dynamic
                                            </label>
                                        </FormGroup>
                                    </td>
                                    <td>
                                        <FormGroup check>
                                            <label className={'labelDuo'}>
                                                <div className={'labelBlock'}>
                                                    {item.active && <div className={'labelBox'}>
                                                        <i className="fas fa-check"></i>
                                                    </div>}
                                                </div>
                                                <Input
                                                    className={'d-none'}
                                                    type="checkbox"
                                                    onClick={() =>
                                                        openStatusModal(
                                                            item
                                                        )
                                                    }
                                                    checked={item.active}
                                                />
                                                Active
                                            </label>
                                        </FormGroup>
                                    </td>
                                    <td>
                                        <EditIcon
                                            onClick={() => editServiceModal(item)}/>
                                    </td>
                                    <td>
                                        <DeleteIcon
                                            onClick={() =>
                                                openDeleteModal(item)
                                            }
                                        />
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        ) : (
                            <tbody>
                            <tr>
                                <td colSpan="4">
                                    <h3 className="text-center mx-auto">
                                        {" "}
                                        No information{" "}
                                    </h3>
                                </td>
                            </tr>
                            </tbody>
                        )}
                    </Table>
                    <Row>
                        <div className="w-100">
                            <Pagination
                                activePage={activePageService + 1}
                                itemsCountPerPage={10}
                                totalItemsCount={totalElementsService}
                                pageRangeDisplayed={5}
                                onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                                linkClass="page-link"
                            />
                        </div>
                    </Row>
                    <Modal
                        id="allModalStyle"
                        isOpen={showModal}
                        toggle={openModal}
                    >
                        <AvForm onValidSubmit={saveItem}>
                            <div className='position-absolute' style={{top : '45px', left : '-12%'}}>
                                <CloseBtn click={openModal} />
                            </div>
                            <ModalHeader
                                toggle={openModal}
                                charCode="x"
                                className="model-head"
                            >
                                {currentItem && currentItem.id
                                    ? "Edit Service"
                                    : "Add Service"}
                            </ModalHeader>
                            <ModalBody>
                                {mainServicesMultiSelect.filter(it => !it.online).length > 0 ?
                                    <Select
                                        defaultValue="Select MainService"
                                        isMulti
                                        name="mainServices"
                                        options={mainServicesMultiSelect.filter(it => !it.online)}
                                        onChange={getZipCode}
                                        className="basic-multi-select"
                                        classNamePrefix="select"
                                    />
                                    : <AvField
                                        name="mainServices"
                                        hidden
                                        required
                                        defaultValue={mainServicesMultiSelect.filter(item => !item.online).length && mainServicesMultiSelect.filter(item => !item.online)[0].id}
                                        placeholder="Enter Sub Service name"
                                    />}
                                <AvField
                                    name="name"
                                    label="Name"
                                    required
                                    defaultValue={
                                        currentItem && currentItem.subServiceDto ? currentItem.subServiceDto.name : ""
                                    }
                                    placeholder="Enter Sub Service name"
                                />
                                <AvField
                                    type="select"
                                    name="serviceEnum"
                                    required
                                    defaultValue={
                                        currentItem && currentItem.subServiceDto ? currentItem.subServiceDto.serviceEnum : ""
                                    }>
                                    <option value="0">
                                        Select service type
                                    </option>
                                    {serviceEnum.map((item) => (
                                        <option
                                            value={item}
                                        >
                                            {item}
                                        </option>
                                    ))}
                                </AvField>
                                <AvField
                                    name="description"
                                    label="Description"
                                    required
                                    defaultValue={
                                        currentItem && currentItem.subServiceDto ? currentItem.subServiceDto.description : ""
                                    }
                                    placeholder="Enter description"
                                />
                                <div className="flex-row-times margin-top-15">
                                    <AvField
                                        required
                                        name="initialCount"
                                        type="number"
                                        label="Initial Count"
                                        defaultValue={currentItem ? currentItem.initialCount : ""
                                        }
                                        placeholder="Enter Count"
                                    />
                                    <AvField
                                        required
                                        name="initialSpendingTime"
                                        type="number"
                                        label="Initial Spending Time"
                                        defaultValue={
                                            currentItem ? currentItem.initialSpendingTime : ""
                                        }
                                        placeholder="Enter Time"
                                    />
                                </div>
                                <div className="flex-row-times margin-top-15">
                                    <AvField
                                        name="everyCount"
                                        type="number"
                                        label="Every Count"
                                        defaultValue={
                                            currentItem ? currentItem.everyCount : ""
                                        }
                                        placeholder="Enter  Count"
                                    />

                                    <AvField
                                        type="number"
                                        name="everySpendingTime"
                                        label="Every Spending Time"
                                        defaultValue={
                                            currentItem
                                                ? currentItem.everySpendingTime
                                                : ""
                                        }
                                        placeholder="Enter Time"
                                    />
                                </div>
                                <div className="flex-row space-around">
                                    <label className={'labelDuo'}>
                                        <div className={'labelBlock'} style={{left : '-35%'}}>
                                            {dynamic && <div className={'labelBox'}>
                                                <i className="fas fa-check" />
                                            </div>}
                                        </div>
                                        <CustomInput
                                            className="check-box-title d-none"
                                            type="checkbox"
                                            checked={dynamic}
                                            onChange={changeDynamic}
                                        />
                                        Dynamic
                                    </label>

                                    <label className={'labelDuo'}>
                                        <div className={'labelBlock'}>
                                            {active && <div className={'labelBox'}>
                                                <i className="fas fa-check" />
                                            </div>}
                                        </div>
                                        <CustomInput
                                            className="check-box-title d-none"
                                            type="checkbox"
                                            checked={active}
                                            onChange={changeActive}
                                        />
                                        Active
                                    </label>

                                </div>
                            </ModalBody>
                            <ModalFooter>
                                {/*<Button*/}
                                {/*    type="button"*/}
                                {/*    color="secondary"*/}
                                {/*    outline*/}
                                {/*    onClick={openModal}*/}
                                {/*>*/}
                                {/*    Cancel*/}
                                {/*</Button>{" "}*/}
                                <Button color="info" outline disabled={loading}>Save</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>

                    {showDeleteModal && (
                        <DeleteModal
                            id="allModalStyle"
                            text={currentItem && currentItem.subServiceDto
                                ? currentItem.subServiceDto.name
                                : ""}
                            showDeleteModal={showDeleteModal}
                            confirm={deleteFunction}
                            cancel={openDeleteModal}
                        />
                    )}
                    <Modal
                        id="allModalStyle"
                        isOpen={showEditModal}
                        toggle={editServiceModal}>
                        <AvForm onValidSubmit={editItem}>
                            <div className='position-absolute' style={{top : '45px', left : '-12%'}}>
                                <CloseBtn click={editServiceModal} />
                            </div>
                            <ModalHeader
                                toggle={editServiceModal}
                                charCode="x"
                                className="model-head">
                                {currentItem ? "Edit Service"
                                    : "Add Service"}
                            </ModalHeader>
                            <ModalBody>
                                {currentItem && currentItem.mainServiceDto && currentItem.mainServiceDto.online ?
                                    <AvField
                                        disabled
                                        name="mainServiceId"
                                        required
                                        type="select"
                                        value={currentItem && currentItem.mainServiceDto && currentItem.mainServiceDto.id ? currentItem.mainServiceDto.id : ''}>
                                        <option
                                            key={currentItem.mainServiceDto.id}
                                            value={currentItem.mainServiceDto.id}>
                                            {currentItem.mainServiceDto.name}
                                        </option>
                                    </AvField>
                                    :
                                    mainServices ? (
                                        <AvField
                                            type="select"
                                            name="mainServiceId"
                                            required
                                            value={
                                                currentItem && currentItem.id ? currentItem.mainServiceDto.id
                                                    : "0"
                                            }>
                                            <option value="0" disabled>
                                                Select Main Service
                                            </option>

                                            {mainServices.filter(item => !item.online).length > 0 ?
                                                mainServices.filter(item => !item.online).map(i =>
                                                    (
                                                        <option
                                                            key={i.id}
                                                            value={i.id}
                                                        >
                                                            {i.name}
                                                        </option>
                                                    )) :
                                                ""}
                                        </AvField>
                                    ) : (
                                        "Main Service not found"
                                    )}
                                {currentItem && currentItem.subServiceDto && currentItem.subServiceDto.defaultInput ?
                                    <AvField
                                        disabled
                                        name="subServiceId"
                                        required
                                        type="select"
                                        value={currentItem && currentItem.subServiceDto && currentItem.subServiceDto.id ? currentItem.subServiceDto.id : ''}>
                                        <option
                                            key={currentItem.subServiceDto.id}
                                            value={currentItem.subServiceDto.id}>
                                            {currentItem.subServiceDto.name}
                                        </option>
                                    </AvField>
                                    :
                                    subServices != null ? (
                                        <AvField
                                            type="select"
                                            name="subServiceId"
                                            required
                                            value={
                                                currentItem && currentItem.subServiceDto && currentItem.subServiceDto.id
                                                    ? currentItem.subServiceDto.id
                                                    : "0"
                                            }

                                        >
                                            <option value="0" disabled>
                                                Select Sub Service
                                            </option>
                                            {subServices.filter((item) => !item.defaultInput).length > 0 ?
                                                subServices.filter((item) => !item.defaultInput).map(i =>
                                                    (
                                                        <option
                                                            key={i.id}
                                                            value={i.id}
                                                        >
                                                            {i.name}
                                                        </option>
                                                    )) : ""}
                                        </AvField>
                                    ) : (
                                        "Sub Service not found"
                                    )}
                                <AvField
                                    name="description"
                                    label="Description"
                                    required
                                    defaultValue={
                                        currentItem && currentItem.subServiceDto ? currentItem.subServiceDto.description : ""
                                    }
                                    placeholder="Enter description"
                                />
                                <div className="flex-row-times margin-top-15">
                                    <AvField
                                        name="initialCount"
                                        label="Initial Count"
                                        defaultValue={
                                            currentItem
                                                ? currentItem.initialCount
                                                : ""
                                        }
                                        placeholder="Enter Count"
                                    />
                                    <AvField
                                        type="number"
                                        name="initialSpendingTime"
                                        label="Initial Spending Time"
                                        defaultValue={
                                            currentItem
                                                ? currentItem.initialSpendingTime
                                                : ""
                                        }
                                        placeholder="Enter Time"
                                    />
                                </div>
                                <div className="flex-row-times margin-top-15">
                                    <AvField
                                        type="number"
                                        name="everyCount"
                                        label="Every Count"
                                        defaultValue={
                                            currentItem ? currentItem.everyCount : ""
                                        }
                                        placeholder="Enter  Count"
                                    />

                                    <AvField
                                        type="number"
                                        name="everySpendingTime"
                                        label="Every Spending Time"
                                        defaultValue={
                                            currentItem ? currentItem.everySpendingTime
                                                : ""
                                        }
                                        placeholder="Enter Time"
                                    />
                                </div>
                                <div className="flex-row space-around">
                                    <label className={'labelDuo'}>
                                        <div className={'labelBlock'}>
                                            {dynamic && <div className={'labelBox'}>
                                                <i className="fas fa-check"/>
                                            </div>}
                                        </div>
                                        <CustomInput
                                            className="check-box-title d-none"
                                            type="checkbox"
                                            checked={dynamic}
                                            onChange={changeDynamic}
                                        />
                                        Dynamic
                                    </label>

                                    <label className={'labelDuo'}>
                                        <div className={'labelBlock'}>
                                            {active && <div className={'labelBox'}>
                                                <i className="fas fa-check"/>
                                            </div>}
                                        </div>
                                        <CustomInput
                                            className="check-box-title d-none"
                                            type="checkbox"
                                            checked={active}
                                            onChange={changeActive}
                                        />
                                        Active
                                    </label>
                                </div>
                            </ModalBody>
                            <ModalFooter>
                                {/*<Button*/}
                                {/*    type="button"*/}
                                {/*    color="secondary"*/}
                                {/*    outline*/}
                                {/*    onClick={editServiceModal}>*/}
                                {/*    Cancel*/}
                                {/*</Button>{" "}*/}
                                <Button color="info" outline disabled={loading}>Save</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>


                    {showStatusModal && (
                        <StatusModal
                            text={
                                currentItem && currentItem.subServiceDto ? currentItem.subServiceDto.name
                                    : ""
                            }
                            showStatusModal={showStatusModal}
                            confirm={changeStatusService}
                            cancel={openStatusModal}
                        />
                    )}

                    {showDynamicModal && (
                        <DefaultInputModal
                            text={currentItem && currentItem.subServiceDto ? currentItem.subServiceDto.name : ""}
                            showDefaultInputModal={showDynamicModal}
                            confirm={changeDynamicService}
                            cancel={openDynamicModal}
                        />
                    )}
                </div>
            </AdminLayout>
        );
    }
}

export default connect(
    ({
         app: {
             services,
             showModal,
             showEditModal,
             currentItem,
             active,
             showDeleteModal,
             showStatusModal,
             dynamic,
             mainServices,
             subServices,
             showDynamicModal,
             activePageService,
             totalElementsService,
             servicePage, loading,
             mainServicesMultiSelect, serviceEnum,
             selectMainServices,
             defaultInput, showDefaultInputModal
         },
     }) => ({
        services,
        showModal,
        showEditModal,
        currentItem,
        active,
        showDeleteModal,
        showStatusModal,
        dynamic,
        mainServices,
        subServices,
        showDynamicModal,
        activePageService,
        totalElementsService,
        servicePage,
        mainServicesMultiSelect,
        serviceEnum,
        selectMainServices,
        defaultInput,
        showDefaultInputModal,
        loading
    })
)(Service);

// Icons
export function EditIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{cursor: "pointer"}}
                width="18"
                height="18"
                viewBox="0 0 18 18"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M12.728 6.68599L11.314 5.27199L2 14.586V16H3.414L12.728 6.68599ZM14.142 5.27199L15.556 3.85799L14.142 2.44399L12.728 3.85799L14.142 5.27199ZM4.242 18H0V13.757L13.435 0.321992C13.6225 0.134521 13.8768 0.0292053 14.142 0.0292053C14.4072 0.0292053 14.6615 0.134521 14.849 0.321992L17.678 3.15099C17.8655 3.33852 17.9708 3.59283 17.9708 3.85799C17.9708 4.12316 17.8655 4.37746 17.678 4.56499L4.243 18H4.242Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}

function DeleteIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{cursor: "pointer"}}
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M2 6H18V19C18 19.2652 17.8946 19.5196 17.7071 19.7071C17.5196 19.8946 17.2652 20 17 20H3C2.73478 20 2.48043 19.8946 2.29289 19.7071C2.10536 19.5196 2 19.2652 2 19V6ZM4 8V18H16V8H4ZM7 10H9V16H7V10ZM11 10H13V16H11V10ZM5 3V1C5 0.734784 5.10536 0.48043 5.29289 0.292893C5.48043 0.105357 5.73478 0 6 0H14C14.2652 0 14.5196 0.105357 14.7071 0.292893C14.8946 0.48043 15 0.734784 15 1V3H20V5H0V3H5ZM7 2V3H13V2H7Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}
