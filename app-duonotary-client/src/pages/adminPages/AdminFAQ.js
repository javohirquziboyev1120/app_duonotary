import React, {Component} from 'react';
import AdminLayout from "../../components/AdminLayout";
import {Button, Modal, ModalFooter} from "reactstrap";
import {AddIcon} from "../../components/Icons";
import TabMenu from "../../components/Calendar/TabMenu";
import UncontrolledCollapse from "reactstrap/es/UncontrolledCollapse";
import {EditIcon} from "./Service";
import ModalHeader from "reactstrap/es/ModalHeader";
import ModalBody from "reactstrap/es/ModalBody";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {deleteFaq, getFaq, saveFaq} from "../../redux/actions/AppAction";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import CloseBtn from "../../components/CloseBtn";

class AdminFaq extends Component {

    constructor(props) {
        super(props);
        this.state={
            modal: false,
            tabIndex: 0,
            deleteModal: false,
            currentItem: {}
        }
    }
    role=["CLIENT", "AGENT"];
    componentDidMount() {
        this.props.dispatch(getFaq())
    }

    toggleModal = () => {
        if(this.state.modal){
            this.setCurrentItem({});
        }
        this.setState({
            modal: !this.state.modal
        })
    }
    toggleDeleteModal = () => {

        if(this.state.deleteModal){
            this.setCurrentItem({});
        }
        this.setState({
            deleteModal: !this.state.deleteModal
        })
    }

    newFaq=(e, v)=>{
        v.id=this.state.currentItem?this.state.currentItem.id:""
        this.props.dispatch(saveFaq(v))
        this.toggleModal();
    }


    deleteFaq=()=>{
        this.props.dispatch(deleteFaq(this.state.currentItem.id));
        this.toggleDeleteModal();
    }

    setCurrentItem=(i)=>{
        this.setState({currentItem: i});
    }
    render() {
        const {faq} = this.props;
        return (
            <AdminLayout pathname={this.props.location.pathname}>
                <div className="container">
                        <div>
                            <AddIcon onClick={this.toggleModal} title={true}/>
                        </div>

                        <div className="faq-page  my-2 mx-5">
                            <div className="d-flex justify-content-center">
                                <TabMenu tabs={["Clients", "Agents"]} getTabIndex={(i)=>{
                                    this.setState({tabIndex: i})}} />
                            </div>

                                {
                                    faq?faq.map((item, index)=>
                                            this.role[this.state.tabIndex]===item.role?
                                                <div key={item.id} className="w-100 faq-item flex-column">
                                        <div id={"id"+item.id} className="d-flex justify-content-between align-items-center">
                                            <span className="title">{item.header}</span>
                                            <div className="d-flex align-items-center">
                                                <EditIcon onClick={()=>{this.toggleModal(); this.setCurrentItem(item)}} />
                                                <span className="icon ml-2 icon-delete" onClick={()=>{this.toggleDeleteModal(); this.setCurrentItem(item)}}/>
                                            </div>
                                        </div>
                                        <UncontrolledCollapse toggler={"id"+item.id}>
                                            <span className="content">{item.text}</span>
                                        </UncontrolledCollapse>
                                    </div>:""
                                    ):""
                                }
                        </div>
                </div>
                <Modal id="allModalStyle" isOpen={this.state.modal} toggle={this.toggleModal} >
                    <div className='position-absolute' style={{top : '45px', left : '-12%'}}>
                        <CloseBtn click={()=>{this.toggleModal(); this.setCurrentItem({})}} />
                    </div>
                    <AvForm onValidSubmit={this.newFaq}>
                        <ModalHeader>
                            Add new FAQ
                        </ModalHeader>
                        <ModalBody>
                            <AvField
                            type="select"
                            required
                            name="role"
                            value={this.state.currentItem.role?this.state.currentItem.role:"AGENT"}
                            >
                                <option value="AGENT">Agent</option>
                                <option value="CLIENT">Client</option>
                            </AvField>
                            <AvField
                                type="text"
                                name="header"
                                placeholder="Question"
                                defaultValue={this.state.currentItem?this.state.currentItem.header:""}
                                required/>
                            <AvField
                                type="textarea"
                                style={{height: 150}}
                                name="text"
                                placeholder="Answer"
                                value={this.state.currentItem?this.state.currentItem.text:""}
                                required/>
                        </ModalBody>
                        <ModalFooter>
                            {/*<Button type="button" onClick={()=>{this.toggleModal(); this.setCurrentItem({})}} color="dark">Cancel</Button>*/}
                            <Button type="submit" color='info' outline >Submit</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>

                <Modal isOpen={this.state.deleteModal} toggle={this.toggleDeleteModal}>
                    <ModalHeader>Delete Review</ModalHeader>
                    <ModalFooter>
                        <Button type="button" onClick={()=>{this.toggleDeleteModal(); this.setCurrentItem({})}}>Cancel</Button>
                        <Button color="danger" onClick={this.deleteFaq}>Delete</Button>
                    </ModalFooter>
                </Modal>
            </AdminLayout>
        );
    }
}

export default connect(
    ({
         app: {
             faq,
         },
     }) => ({
        faq
    }))
(withRouter(AdminFaq));