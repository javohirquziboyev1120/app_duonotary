import React, {Component} from 'react';
import AdminLayout from "../../components/AdminLayout";
import {Button, Col, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import {
    addOrder,
    changeCustomerNames,
    deleteOrder,
    findAllAgent,
    findAllUserByEmail,
    getAllByUserId,
    getOrderList, getOrderListByStatus,
    getOrdersByFilter,
    getOrdersBySearch,
    getServicePrices,
    getTimesByDate,
    getZipcodeByCode,
    postTimeTable
} from "../../redux/actions/OrderAction";
import {connect} from "react-redux";
import * as types from "../../redux/actionTypes/OrderActionTypes";
import {CHANGE_CURRENT_ORDER, SHOW_MODAL} from "../../redux/actionTypes/OrderActionTypes";
import {AvForm} from "availity-reactstrap-validation";
import getDate from "../../components/Date";
import {toast} from "react-toastify";
import {getDates} from "../clientPage/OnlineOrder";
import {getAmPm} from "../clientPage/InPersonOrder";
import PaginationComponent from "react-reactstrap-pagination";
import RealEstateComponent from "../../components/RealEstateComponent";
import International from "../../components/International";
import {getCountryList, getDocumentTypeList, getEmbassy} from "../../redux/actions/AppAction";
import OrderDocuments from "../../components/Modal/OrderDocuments";
import ServiceDetails from "../../components/ServiceDetails";
import TabMenu from "../../components/Calendar/TabMenu";
import CloseBtn from "../../components/CloseBtn";

class AdminOrder extends Component {
    componentDidMount() {
        this.props.dispatch(getOrderList({page: 0, size: this.props.size}));
        if ((this.props.zipCodes && this.props.zipCodes.length > 0 && this.props.zipCodes[0]) ||
            (this.props.servicePrices && this.props.servicePrices.length > 0 && this.props.servicePrices[0])
            || this.props.currentOrder
        ) {
            window.location.reload();
        }
        this.props.dispatch(getCountryList())
        this.props.dispatch(getEmbassy())
        this.props.dispatch(getDocumentTypeList())
    }

    state = {
        disable: true,
        editModal: false,
        editUser: false,
        field: '',
        label: '',
        placeholder: '',
        order: '',
        title: '',
        client: '',
        agent: '',
        zipCode: '',
        address: '',
        status: '',
        selected: '',
        delete: false,
        firstName: '',
        lastName: '',
        documentModal: false,
        documents: ''
    }

    ChangeState(obj) {
        if (obj.documentModal === false) this.props.dispatch(
            getAllByUserId({
                userId: this.props.currentUser.id,
                param: {page: this.props.page, size: this.props.size}
            })
        );
        this.setState({...obj})
        this.props.dispatch({type: 'updateState', payload: {documents: obj.documents}})
    }


    render() {
        let {
            filters, ordersList, modal, dispatch, currentOrder, loading, searchAgent, status, zipCodes, servicePrices, realEstateDto, internationalDto,
            currentUser, error, times, time, selectedDate, page, size, totalElements, countries, embassyCountries, documentTypes, isApostille, documents
        } = this.props;

        const handleSelected = (selectedPage) => {
            let obj = {page: selectedPage - 1, size: size}
            let zipCode;
            let date;
            let search;
            if (filters) {
                search = filters.filter(item => item.key === 'search')[0]
                zipCode = filters.filter(item => item.key === 'zipCode')[0]
                date = filters.filter(item => item.key === 'date')[0]
                if (search) obj = {...obj, search: filters.filter(item => item.key === 'search')[0].value};
                if (zipCode && date) obj = {
                    ...obj,
                    zipCode: filters.filter(item => item.key === 'zipCode')[0].value,
                    date: filters.filter(item => item.key === 'date')[0].value
                };
                if (zipCode) obj = {...obj, zipCode: filters.filter(item => item.key === 'zipCode')[0].value};
                if (date) obj = {...obj, date: filters.filter(item => item.key === 'date')[0].value};
            }
            dispatch(search ? getOrdersBySearch(obj) : zipCode || date ? getOrdersByFilter(obj) : getOrderList(obj))
            dispatch(
                {
                    type: 'updateState',
                    payload: {
                        page: selectedPage - 1
                    }
                }
            )
        }
        const getStatus = (item) => {
            if (item === 'IN_PROGRESS') item = 'In-Progress';
            if (item === 'CANCELLED') item = 'Cancelled';
            if (item === 'DRAFT') item = 'Draft';
            if (item === 'CLOSED') item = 'Closed';
            if (item === 'REJECTED') item = 'Rejected';
            if (item === 'COMPLETED') item = 'Completed';
            if (item === 'NEW') item = 'New';
            return item;
        }
        let getTimer = (item, num) => {
            if (currentOrder || num === 1) {
                let countDownDate = new Date(
                    item.timeTableDto.fromTime
                ).getTime();
                let x = setInterval(function () {
                    let now = new Date().getTime();
                    let distance = countDownDate - now;
                    let days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    let hours = Math.floor(
                        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
                    );
                    let minutes = Math.floor(
                        (distance % (1000 * 60 * 60)) / (1000 * 60)
                    );
                    let seconds = Math.floor((distance % (1000 * 60)) / 1000);
                    let p = document.getElementById(item.id + num);
                    if (p != null) {
                        let status = item.status;
                        if (status !== 'NEW' || distance < 0) {
                            p.style.letterSpacing = '1px'
                            p.style.color = status === 'CANCELLED' ? 'red' : status === 'COMPLETED' ? 'green' : status === 'IN_PROGRESS' || status === 'DRAFT' ? '#FFC30B' : ''
                            clearInterval(x);
                            p.innerHTML = getStatus(status)
                        } else if (days === 0) {
                            p.innerHTML =
                                "- " + hours + ":" + minutes + ":" + seconds;
                        } else {
                            p.innerHTML =
                                "- " +
                                days +
                                "d " +
                                hours +
                                ":" +
                                minutes +
                                ":" +
                                seconds;
                        }
                    }
                }, 100);
            }
        };


        let setCurrentOrder = (item) => {
            console.log(item);
            if (item.servicePrice && item.servicePrice.serviceDto &&
                item.servicePrice.serviceDto.subServiceDto && item.servicePrice.serviceDto.subServiceDto.serviceEnum === "REAL_ESTATE") {
                dispatch({type: 'updateState', payload: {realEstateDto: item.realEstateDto}})
            }
            if (item.servicePrice && item.servicePrice.serviceDto &&
                item.servicePrice.serviceDto.subServiceDto && item.servicePrice.serviceDto.subServiceDto.serviceEnum === "INTERNATIONAL") {
                dispatch({type: 'updateState', payload: {internationalDto: item.internationalDto}})
            }
            dispatch({
                type: CHANGE_CURRENT_ORDER,
                payload: item
            })
            dispatch({
                type: "updateState",
                payload: {
                    times: [],
                    time: '',
                    isApostille: item.internationalDto && !item.internationalDto.embassy,
                }
            })
        }
        let changeModal = () => {
            dispatch({
                type: SHOW_MODAL,
                payload: !modal
            })
        }

        function checkService() {
            let fine = true;
            let service = document.getElementById("servicePriceId");
            if (!!servicePrices[0]) {
                if (!!service)
                    servicePrices.map(v => {
                        if (v.id !== service.value)
                            if (v.service.subService.name === currentOrder.servicePrice.subServiceName)
                                fine = false;
                    })
                return fine;
            }
            return false;
        }

        let editOrder = () => {
            let clientId = document.getElementById("clientId").value;
            let agentId = document.getElementById("agentId").value;
            let zipCodeId = document.getElementById("zipCodeId").value;
            let servicePriceId = document.getElementById("servicePriceId").value;
            let countDocument = document.getElementById("countDocument").value;
            let address = document.getElementById("address").value;
            let amount = document.getElementById("amount").value.substring(1);
            amount = amount ? amount : 0
            let discountAmount = document.getElementById("discountAmount").value.substring(1);
            discountAmount = discountAmount ? discountAmount : 0
            let status = document.getElementById("status").value;
            let international = null;
            let realEstate = null;
            if (currentOrder && currentOrder.internationalDto) {
                international = {...internationalDto}
                international.id = currentOrder.internationalDto.id
            } else if (currentOrder && currentOrder.realEstateDto) {
                realEstate = {...realEstateDto}
                realEstate.id = currentOrder.realEstateDto.id
            } else {
                toast.error("Something is wrong")
                return "";
            }
            console.log(realEstate);
            console.log(international)
            let order = {
                admin: true,
                ...currentOrder, clientId,
                agentId,
                zipCodeId,
                servicePriceId,
                countDocument,
                address,
                amount,
                discountAmount,
                status,
                timeTableId: currentOrder.timeTableDto.id,
                payTypeId: currentOrder.payType.id,
                user: {id: currentUser.id},
                realEstateDto: realEstate,
                internationalDto: international
            };
            dispatch(addOrder(order))
            changeModal()
            setState({
                disable: true,
                editModal: false,
                field: '',
                label: '',
                placeholder: '',
                order: '',
                title: '',
                client: '',
                agent: '',
                zipCode: '',
                address: '',
                status: '',
                selected: ''
            })
            getOrderList()
        }

        const tabs = ["All", "Upcoming", "In-progress", "Completed", "Rejected"];
        const fetchData = (i) => {
            switch (i) {
                case 0:
                    dispatch(getOrderList({page: 0}));
                    break;
                case 1:
                    dispatch(getOrderListByStatus({page: 0, size: 20, status: 'NEW'}));
                    break;
                case 2:
                    dispatch(getOrderListByStatus({page: 0, size: 20, status: 'IN_PROGRESS'}));
                    break;
                case 3:
                    dispatch(getOrderListByStatus({page: 0, size: 20, status: 'COMPLETED'}));
                    break;
                case 4:
                    dispatch(getOrderListByStatus({page: 0, size: 20, status: 'REJECTED'}));
                    break;
                default:
                    break;
            }
        }


        let setState = (item) => {
            this.setState(item)
        }

        let submit = (v) => {
            if (this.state.field === 'clientId' && v.length > 3)
                dispatch(findAllUserByEmail({email: v}))
            if (this.state.field === 'zipCodeId' && v.length > 3)
                dispatch(getZipcodeByCode(v))

            if (this.state.field !== 'agentId') setState({selected: false})
        }
        let setValue = (item) => {
            if (this.state.field === 'clientId') {
                document.getElementById("input-order-edit").value = (item.firstName + ' ' + item.lastName + ' ' + item.email);
                setState({client: item, selected: true})
            } else if (this.state.field === 'agentId') {
                document.getElementById("input-order-edit").value = (item.firstName + ' ' + item.lastName + ' ' + item.email);
                setState({agent: item, selected: true})
            } else if (this.state.field === 'zipCodeId') {
                document.getElementById("input-order-edit").value = (item.code + ' ' + item.city);
                setState({zipCode: item, selected: true})
            }
        }
        let onValidSubmit = (e, v) => {
            if (this.state.selected) {
                setState({editModal: false})
                if (!!this.state.client && this.state.field === 'clientId') {
                    document.getElementById("clientId").value = this.state.client.id
                }
                if (!!this.state.agent && this.state.field === 'agentId') {
                    document.getElementById("agentId").value = this.state.agent.id
                }
                if (!!this.state.zipCode && this.state.field === 'zipCodeId') {
                    document.getElementById("zipCodeId").value = this.state.zipCode.id
                    dispatch(getServicePrices(this.state.zipCode.id))
                }
            }
        }

        let saveCustomer = () => {
            let first = document.getElementById("firstName").value;
            let last = document.getElementById("lastName").value;
            if (last.length > 1 && last.length > 1)
                dispatch(changeCustomerNames({id: currentOrder.client.id, firstName: first, lastName: last}))
            else toast("Please fill the inputs")
            setState({editUser: false})
        }

        let saveToTimeTable = (time) => {
            dispatch(postTimeTable({
                id: currentOrder.timeTableDto.id,
                fromTime: selectedDate + ' ' + time + '.0',
                userDto: {id: currentOrder.agent.id},
                orderDto: {
                    servicePriceId: currentOrder.servicePrice.id,
                    countDocument: currentOrder.countDocument,
                    id: currentOrder.id
                },
                online: currentOrder.timeTableDto.online,
                step: 3,
                error: {
                    error: error,
                    servicePriceId: currentOrder.servicePrice.id,
                    countDocument: currentOrder.countDocument,
                    online: currentOrder.timeTableDto.online,
                    date: selectedDate
                }
            }))
        }
        const clearFilter = () => {
            this.props.dispatch(getOrderList({page: 0, size: this.props.size}));
            dispatch({type: 'updateState', payload: {filters: '', search: ''}})
        }

        return (
            <AdminLayout pathname={this.props.location.pathname}>
                <div className="admin-order-page container">
                    <Row className="sharing-history-row m-0">
                        <Col md={12}>
                            {filters ?
                                <Row>
                                    <Col><CloseIcon onClick={() => clearFilter(null, false, true)}/></Col>
                                    <Col md={12}>
                                        {filters.map(item =>
                                            <Row className="bg-filter  p-2 m-3 rounded">
                                                <Col md={12}>{item.key} : {item.value}</Col>
                                            </Row>
                                        )}
                                    </Col>
                                </Row>

                                : ''}

                        </Col>
                        <Col md={12}>
                            <Row className="justify-content-end">
                                <TabMenu tabs={tabs} getTabIndex={fetchData}/>
                            </Row>
                        </Col>
                        {ordersList ? ordersList.map(item =>
                            <Col md={12} className="history-col" key={item.id}>
                                <Row>
                                    <Col md={3}>
                                        <div className="d-flex">
                                            <div className="avatar-img">
                                                <img src="/assets/img/avatar.png" alt=""/>
                                            </div>
                                            <div className="ml-3">
                                                <div className="fs-15">Client</div>
                                                <div className="fs-15-bold pt-2">
                                                    {item.client.firstName + " " + item.client.lastName}
                                                </div>
                                            </div>
                                        </div>
                                    </Col>
                                    <Col md={2} className="px-0">
                                        <div className="fs-15">Agent</div>
                                        <div className="fs-15-bold pt-2">
                                            {item.agent.firstName + " " + item.agent.lastName}
                                        </div>
                                    </Col>
                                    <Col md={3}>
                                        <div className="fs-15">Address</div>
                                        <div className="fs-15-bold pt-2">
                                            {item.address ? item.address.length > 30 ? item.address.substring(0, 30) + "..." : item.address : 'online'}
                                        </div>
                                    </Col>
                                    <Col md={2}>
                                        <div className="fs-15">Zip code</div>
                                        <div className="fs-15-bold pt-2">
                                            {item.zipCode && item.zipCode.code && item.zipCode.city ? item.zipCode.code + " " + item.zipCode.city : 'online'}
                                        </div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="fs-15-bold minus-color"
                                             id={item.id + '1'}>{getTimer(item, 1)}
                                        </div>
                                        <div className="fs-20-bold pt-2">
                                            ${(item.amount - item.discountAmount).toFixed(2)}
                                        </div>
                                    </Col>
                                    <Col md={1} className="mt-2 pl-2 text-center" onClick={() => {
                                        setCurrentOrder(item)
                                    }}>
                                        <img className="right-icons"
                                             src="/assets/icons/right-arrov.png" alt=""/>
                                    </Col>
                                </Row>
                            </Col>
                        ) : ''}
                        <div className="w-100 mt-2"> {totalElements > 0 ? <PaginationComponent
                            defaultActivePage={page + 1}
                            totalItems={totalElements} pageSize={size}
                            onSelect={handleSelected}
                        /> : ''}</div>
                        {currentOrder ?
                            <Modal id="allModalStyle" isOpen={modal} toggle={changeModal}>
                                <FormGroup>
                                    <div className='position-absolute' style={{top: '40px', left: '-12%'}}>
                                        <CloseBtn click={changeModal}/>
                                    </div>
                                    <ModalHeader toggle={changeModal} className="model-header">
                                        <Row>
                                            <Col md={6} className="pr-5">
                                                Order
                                            </Col>
                                            <Col className="pl-5" md={3} disabled={loading}>
                                                <DeleteIcon onClick={() => setState({delete: true})}/>
                                            </Col>
                                        </Row>
                                    </ModalHeader>
                                    <ModalBody className="modal-body">
                                        <div className="container">
                                            <div className="row">
                                                <Col md={12}>
                                                    <Label>Client</Label>
                                                    <Input type="select" id="clientId"
                                                           onChange={function (e) {
                                                               if (e.target.value === 'ok')
                                                                   setState({
                                                                       editUser: true
                                                                   })
                                                           }}>
                                                        {this.state.client ? <option
                                                            value={this.state.client.id}>{this.state.client.firstName + ' ' + this.state.client.lastName}</option> : ''}
                                                        <option className="py-2"
                                                                value={currentOrder.client.id}>{currentOrder.client.firstName + " " + currentOrder.client.lastName}</option>
                                                        <option className="py-2" value="ok">Change Customer full name
                                                        </option>
                                                    </Input>
                                                </Col>
                                                <Col md={12}>
                                                    <Label>Agent</Label>
                                                    <Input type="select" id="agentId"
                                                           onChange={function (e) {
                                                               if (e.target.value === 'ok') {
                                                                   setState({
                                                                       editModal: true,
                                                                       field: 'agentId',
                                                                       title: 'Search agent',
                                                                       label: 'Select agent',
                                                                       placeholder: "First name Last name",
                                                                       selected: false,
                                                                   })
                                                                   dispatch(findAllAgent({
                                                                       online: currentOrder.timeTableDto.online,
                                                                       zipCodeId: currentOrder.zipCode ? currentOrder.zipCode.id : null,
                                                                       start: currentOrder.timeTableDto.fromTime,
                                                                       end: currentOrder.timeTableDto.tillTime,
                                                                   }))
                                                               }
                                                           }}>
                                                        {this.state.agent ? <option
                                                            value={this.state.agent.id}>{this.state.agent.firstName + ' ' + this.state.agent.lastName}</option> : ''}
                                                        <option
                                                            value={currentOrder.agent.id}>{currentOrder.agent.firstName + " " + currentOrder.agent.lastName}</option>
                                                        <option className="py-2" value="ok">Set other agent</option>
                                                    </Input>
                                                </Col>
                                                <Col md={12}>
                                                    <Row>
                                                        <Col md={6} hidden={!currentOrder.zipCode}>
                                                            <Label>Zip code</Label>
                                                            <Input type="select" id="zipCodeId"
                                                                   onChange={function (e) {
                                                                       if (e.target.value === 'ok')
                                                                           setState({
                                                                               editModal: true,
                                                                               field: 'zipCodeId',
                                                                               title: 'Search zip code',
                                                                               label: 'Enter zip code',
                                                                               placeholder: '12345',
                                                                               selected: false,
                                                                           })
                                                                   }}>
                                                                {this.state.zipCode ? <option
                                                                    value={this.state.zipCode.id}>{this.state.zipCode.code + ' ' + this.state.zipCode.city}</option> : ''}
                                                                <option
                                                                    value={currentOrder.zipCode ? currentOrder.zipCode.id : ''}>{currentOrder.zipCode ? currentOrder.zipCode.code + " " + currentOrder.zipCode.city : ''}</option>
                                                                <option className="py-2" value="ok">Set other zip code
                                                                </option>
                                                            </Input>
                                                        </Col>
                                                        <Col md={currentOrder.zipCode ? 6 : 12}>
                                                            <Label>Services</Label>
                                                            <Input type="select" id="servicePriceId"
                                                                   invalid={checkService()}>
                                                                {servicePrices[0] ? servicePrices.map(v =>
                                                                    <option
                                                                        value={v.id}>{v.service.subService.name}</option>
                                                                ) : ''}
                                                                <option
                                                                    value={currentOrder.servicePrice ? currentOrder.servicePrice.id : currentOrder.id}>{currentOrder.servicePrice ? currentOrder.servicePrice.subServiceName : 'Service not found'}</option>
                                                                <option className="py-2" value="ok">Set other service
                                                                </option>
                                                            </Input>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                                <Col md={12} hidden={!currentOrder.address}>
                                                    <Label>Address</Label>
                                                    <Input id="address" defaultValue={currentOrder.address}/>
                                                </Col>
                                                <Col md={12}>
                                                    <Row>
                                                        <Col md={6}>
                                                            <Label>Amount</Label>
                                                            <Input type="input" id="amount"
                                                                   onChange={function (e) {
                                                                       if (!!e.target.value) {
                                                                           document.getElementById("amount").value = '$' + e.target.value.replace(/[^0-9.-]+/g, "");
                                                                       } else {
                                                                           document.getElementById("amount").value = '$0'
                                                                       }
                                                                   }}
                                                                   defaultValue={'$' + currentOrder.amount}/>
                                                        </Col>
                                                        <Col md={6}>
                                                            <Label>Discount</Label>
                                                            <Input type="input" id="discountAmount"
                                                                   onChange={function (e) {
                                                                       if (!!e.target.value) {
                                                                           document.getElementById("discountAmount").value = '$' + e.target.value.replace(/[^0-9.-]+/g, "");
                                                                       } else {
                                                                           document.getElementById("discountAmount").value = '$0'
                                                                       }
                                                                   }}
                                                                   defaultValue={'$' + currentOrder.discountAmount}/>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                                <Col md={12}>
                                                    <Row>
                                                        <Col md={6}>
                                                            <Label>Status</Label>
                                                            <Input type="select" id="status">
                                                                <option>{currentOrder.status}</option>
                                                                {status.map(s =>
                                                                    s !== currentOrder.status ?
                                                                        <option key={s}>{s}</option> : ''
                                                                )}
                                                            </Input>
                                                        </Col>
                                                        <Col md={6}>
                                                            <Label>Documents</Label>
                                                            <Input type="number" min={1}
                                                                   defaultValue={currentOrder.countDocument}
                                                                   id="countDocument"/>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                                <Col md={12}>
                                                    <Row>
                                                        <Col md={6}>
                                                            <Label>Date</Label>
                                                            <Input type="select"
                                                                   onChange={(e) => getTimes(e.target.value, currentOrder, dispatch)}
                                                                   defaultValue={currentOrder.timeTableDto.id}>
                                                                {getDates(null, 14).map(v =>
                                                                    <option key={v}
                                                                            value={getDate(v, "day") === getDate(currentOrder.timeTableDto.fromTime, 'day') ? currentOrder.timeTableDto.id : v}
                                                                    >{getDate(v, "day")}</option>
                                                                )}
                                                            </Input>
                                                        </Col>
                                                        <Col md={6}>
                                                            <Label>Time</Label>
                                                            <Input type="select"
                                                                   onChange={(e) => saveToTimeTable(e.target.value)}
                                                                   defaultValue={currentOrder.timeTableDto.id}>
                                                                {times[0] ? times.map(v =>
                                                                    <option value={v.time} key={v.time}
                                                                            hidden={!v.enable || v.agentId !== currentOrder.agent.id}>{getAmPm(v.time)}</option>
                                                                ) : <option
                                                                    value={currentOrder.timeTableDto.id}>{getDate(currentOrder.timeTableDto.fromTime, "time")}</option>}
                                                            </Input>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            </div>
                                            {currentOrder.timeTableDto.online || currentOrder.servicePrice && currentOrder.servicePrice.serviceDto &&
                                            currentOrder.servicePrice.serviceDto.subServiceDto && currentOrder.servicePrice.serviceDto.subServiceDto.serviceEnum === "REAL_ESTATE" ?
                                                <Col md={12}><Button className="btn-block my-3"
                                                                     onClick={() => this.ChangeState({
                                                                         documentModal: true,
                                                                         documents: currentOrder.documents
                                                                     })} color="secondary">Show Documents</Button></Col>
                                                : ''}
                                            {currentOrder.servicePrice && currentOrder.servicePrice.serviceDto &&
                                            currentOrder.servicePrice.serviceDto.subServiceDto && currentOrder.servicePrice.serviceDto.subServiceDto.serviceEnum === "REAL_ESTATE" ?
                                                <div>
                                                    <RealEstateComponent
                                                        data={realEstateDto}
                                                        isAdd={false}
                                                        dispatch={dispatch}
                                                        printDocumentCount={1}
                                                        printDocumentPrice={1}
                                                        servicePrice={currentOrder.servicePrice}
                                                        submit={""}
                                                        class={true}
                                                    />
                                                </div>
                                                : currentOrder.servicePrice && currentOrder.servicePrice.serviceDto &&
                                                currentOrder.servicePrice.serviceDto.subServiceDto && currentOrder.servicePrice.serviceDto.subServiceDto.serviceEnum === "INTERNATIONAL" ?
                                                    <International
                                                        isAdd={false}
                                                        submit={""}
                                                        dispatch={dispatch}
                                                        data={internationalDto}
                                                        isApostille={currentOrder.internationalDto ? !currentOrder.internationalDto.embassy : true}
                                                        countries={countries}
                                                        documentTypes={documentTypes}
                                                        embassyCountries={embassyCountries}
                                                        mobile={true}
                                                    />
                                                    : ""}
                                            {currentOrder.orderAdditionalServiceDtoList.length > 0 ?
                                                <div>
                                                    <Row className=""><Col md={12} className="text-center"
                                                                           style={{fontSize: '32px'}}>Additional
                                                        services</Col></Row>
                                                    {currentOrder.orderAdditionalServiceDtoList.map(oas =>
                                                        <ServiceDetails count={oas.count}
                                                                        name={oas.additionalServicePriceDto.additionalServiceDto.name}
                                                                        price={oas.currentPrice.toFixed(2)}/>
                                                    )}
                                                </div>
                                                : ''}
                                        </div>
                                    </ModalBody>
                                    <ModalFooter className="modal-footer">
                                        {/*<Button*/}
                                        {/*    color="secondary"*/}
                                        {/*    onClick={changeModal}*/}
                                        {/*>*/}
                                        {/*    Cancel*/}
                                        {/*</Button>{" "}*/}
                                        <button type='button' onClick={() => editOrder()}
                                                className='btn btn-outline-info'>Save
                                        </button>
                                    </ModalFooter>
                                </FormGroup>

                            </Modal> : ''}
                        <Modal isOpen={this.state.editModal}>
                            <AvForm onValidSubmit={onValidSubmit}>
                                <ModalHeader>
                                    {this.state.title}
                                </ModalHeader>
                                <ModalBody>
                                    <Input onChange={(e) => submit(e.target.value)}
                                           autoComplete="off"
                                           label={this.state.label} id="input-order-edit"
                                           placeholder={this.state.placeholder}
                                           name={this.state.field} type="input"/>

                                    {searchAgent && searchAgent.length > 0 && searchAgent[0] && !this.state.selected && this.state.field === 'agentId' ? searchAgent.map(v =>
                                        <li onClick={() => setValue(v)} key={v.id}
                                            value={v.id}>{v.firstName + ' ' + v.lastName + ' ' + v.email}</li>) : ''}
                                    {zipCodes && zipCodes.length > 0 && zipCodes[0] && !this.state.selected && this.state.field === 'zipCodeId' ? zipCodes.map(v =>
                                        <li onClick={() => setValue(v)} key={v.id}
                                            value={v.id}>{v.code + ' ' + v.city}</li>) : ''}
                                </ModalBody>
                                <ModalFooter>
                                    <Button onClick={() => setState({editModal: false})}>Cancel</Button>
                                    <button type='button' className='btn btn-outline-info'>Save</button>
                                </ModalFooter>
                            </AvForm>
                        </Modal>
                        <Modal isOpen={this.state.editUser}>
                            <ModalHeader>Change customer name</ModalHeader>
                            <ModalBody>
                                <Label>First Name</Label>
                                <Input id="firstName"/>
                                <Label>Last Name</Label>
                                <Input id="lastName"/>
                            </ModalBody>
                            <ModalFooter>
                                <Button onClick={() => setState({editUser: false})}>Cancel</Button>
                                <Button onClick={() => saveCustomer()}>Save</Button>
                            </ModalFooter>
                        </Modal>
                    </Row>
                </div>
                <Modal isOpen={this.state.delete}>
                    <ModalHeader>
                        Are you sure to delete {currentOrder ? currentOrder.servicePrice.subServiceName : ''}
                    </ModalHeader>
                    <ModalFooter>
                        <Button className="px-3 py-1 mx-4" onClick={() => setState({delete: false})}>NO</Button>
                        <Button className="px-3 py-1 mx-4" onClick={function () {
                            dispatch(deleteOrder(currentOrder.id))
                            setState({delete: false})
                        }}>YES</Button>
                    </ModalFooter>
                </Modal>
                {this.state.documentModal ?
                    <OrderDocuments
                        data={documents}
                        order={currentOrder}
                        dispatch={dispatch}
                        cancel={(item) => this.ChangeState(item)}
                        showModal={this.state.documentModal}
                        isClient={false}
                    /> : ''
                }
            </AdminLayout>
        );
    }
}


export default connect(({order: {realEstateDto, internationalDto, error, times, time, selectedDate, documents, servicePrices, zipCodes, status, ordersList, modal, order, payload, currentOrder, loading, searchUser, searchAgent}, auth: {currentUser}, app: {filters, page, size, totalElements, countries, embassyCountries, documentTypes, isApostille}}) => ({
    error, realEstateDto, internationalDto,
    times,
    time,
    selectedDate,
    page,
    size,
    totalElements,
    countries,
    embassyCountries,
    documentTypes,
    isApostille,
    servicePrices,
    zipCodes,
    status,
    ordersList,
    modal,
    order,
    documents,
    payload,
    currentOrder,
    currentUser,
    loading,
    searchUser,
    searchAgent,
    filters
}))(AdminOrder);


export function DeleteIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className} hidden={props.hidden}>
            <svg
                style={{cursor: "pointer"}}
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M2 6H18V19C18 19.2652 17.8946 19.5196 17.7071 19.7071C17.5196 19.8946 17.2652 20 17 20H3C2.73478 20 2.48043 19.8946 2.29289 19.7071C2.10536 19.5196 2 19.2652 2 19V6ZM4 8V18H16V8H4ZM7 10H9V16H7V10ZM11 10H13V16H11V10ZM5 3V1C5 0.734784 5.10536 0.48043 5.29289 0.292893C5.48043 0.105357 5.73478 0 6 0H14C14.2652 0 14.5196 0.105357 14.7071 0.292893C14.8946 0.48043 15 0.734784 15 1V3H20V5H0V3H5ZM7 2V3H13V2H7Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}

export const getTimes = (date, order, dispatch) => {
    if (date === order.timeTableDto.id)
        date = order.timeTableDto.fromTime
    dispatch({
        type: types.CHANGE_DATE,
        payload: date
    })
    dispatch(
        getTimesByDate({
            servicePriceId: order.servicePrice.id,
            countDocument: order.countDocument,
            online: order.timeTableDto.online,
            date: date,
            zipCodeId: order.zipCode.id
        })
    )
}

function CloseIcon(props) {
    return <div className={props.className}>
        <svg onClick={props.onClick} style={{cursor: 'pointer'}} width="50" height="50" viewBox="0 0 50 50" fill="none"
             xmlns="http://www.w3.org/2000/svg">
            <circle cx="25" cy="25" r="25" fill="white"/>
            <path
                d="M24.955 23.1875L31.1425 17L32.91 18.7675L26.7225 24.955L32.91 31.1425L31.1425 32.91L24.955 26.7225L18.7675 32.91L17 31.1425L23.1875 24.955L17 18.7675L18.7675 17L24.955 23.1875Z"
                fill="#313E47"/>
        </svg>
    </div>
}