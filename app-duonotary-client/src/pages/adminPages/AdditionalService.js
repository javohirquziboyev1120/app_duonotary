import React, {Component} from "react";
import AdminLayout from "../../components/AdminLayout";
import {
    deleteAdditionalService,
    editServiceChangeActive,
    getAdditionalServiceList,
    getAdditionalServicePage,
    getServiceList,
    getServicePriceDashboard,
    getStateList,
    saveAdditionalService,
} from "../../redux/actions/AppAction";
import {
    Button, Col,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader, Row,
    Table,
} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import DeleteModal from "../../components/Modal/DeleteModal";
import StatusModal from "../../components/Modal/StatusModal";
import {connect} from "react-redux";
import Pagination from "react-js-pagination";
import {AddIcon} from "../../components/Icons";
import * as types from "../../redux/actionTypes/AppActionTypes";
import * as appAction from "../../redux/actions/AppAction";
import * as appAddAction from "../../redux/actions/AdditionalServicePriceAction";
import {
    deleteAdditionalServicePrice,
    getAdditionalServicePriceDashboard, getCountyByState, getZipCodeByCounty,
    saveAdditionalServicePrice
} from "../../redux/actions/AdditionalServicePriceAction";
import Select from "react-select";
import CloseBtn from "../../components/CloseBtn";


class AdditionalService extends Component {
    componentDidMount() {
        this.props.dispatch(getAdditionalServicePage({page: 0, size: 10}));
        this.props.dispatch(getAdditionalServiceList())
        this.props.dispatch(getStateList())
        this.props.dispatch(getServicePriceDashboard())
        this.props.dispatch(getAdditionalServicePriceDashboard())
        this.props.dispatch(getServiceList())
    }

    constructor(props) {
        super(props);
        this.state = {
            inPerson: false,
            checked: false,
            isOnline: false
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(checked) {
        this.setState({checked});
    }

    handleChangeOnline = (id) => {
        this.props.services.map(a => {
            if (a.id === id) this.setState({isOnline: a.mainService.online})
        })
    }

    handlePageChange(pageNumber) {
        this.props.dispatch(getAdditionalServicePage({page: pageNumber - 1, size: 10}))
        this.props.dispatch(
            {
                type: 'updateState',
                payload: {
                    activePageAddService: pageNumber - 1
                }
            })
    }

    render() {
        const {
            permissions,
            services,
            dispatch,
            showModal,
            additionalServices,
            currentItem,
            active,
            showDeleteModal,
            showStatusModal,
            activePageAddService,
            totalElementsAdditional,
            additionalServicePage,
            isAdditional,
            isAdditionalPrice,
            selectZipCodes, selectCounties, dashboard, selectStates, additionalDashboard,
            stateOptions, countyOptions, zipCodeOptions, showServiceModal,
            additionalServicePrices, currentAdditionalService, all, loading
        } = this.props;

        const saveItemPrice = (e, v) => {
            v.id = currentAdditionalService != null ? currentAdditionalService.id : null
            v.active = active
            v.all = all
            v.statesId = this.state.isOnline ? null : selectCounties !== null ? [] : selectStates
            v.countiesId = this.state.isOnline ? null : selectZipCodes !== null ? [] : selectCounties
            v.zipCodesId = this.state.isOnline ? null : selectZipCodes !== null ? selectZipCodes : []
            this.props.dispatch(saveAdditionalServicePrice(v))
        }
        const changeStatusAdditionalServicePrice = () => {
            let currentAdditionalServicePrice = {...currentAdditionalService};
            currentAdditionalServicePrice.active = !currentAdditionalService.active;
            this.props.dispatch(editServiceChangeActive(currentAdditionalServicePrice))
        }


        const changeAll = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    all: !all
                }
            })
        };
        const getCounty = (e, v) => {
            if (!all) {
                let arr = [];
                if (e && e.length === 1) {
                    if (v.action === "remove-value") {
                        v.option = e[0];
                    }
                    dispatch(getCountyByState(v));
                }
                if (e) {
                    e.map((item) => arr.push(item.value));
                    dispatch({
                        type: "updateState",
                        payload: {
                            selectStates: arr,
                            selectCounties: null,
                        },
                    });
                } else {
                    dispatch({
                        type: "updateState",
                        payload: {
                            selectCounties: null,
                            selectZipCodes: null,
                        },
                    });
                }
            }
        };
        const getZipCode = (e, v) => {
            let arr = [];
            if (e && e.length === 1) {
                if (v.action === "remove-value") {
                    v.option = e[0];
                }
                dispatch(getZipCodeByCounty(v));
            }
            if (e) {
                e.map((item) => arr.push(item.value));
                dispatch({
                    type: "updateState",
                    payload: {
                        selectCounties: arr,
                    },
                });
            } else {
                dispatch({
                    type: "updateState",
                    payload: {
                        selectCounties: null,
                        selectZipCodes: null,
                    },
                });
            }
        };
        const saveZipCodeForAdditionalServicePrice = (e, v) => {
            let arr = [];
            if (e) {
                e.map((item) => arr.push(item.value));
                dispatch({
                    type: "updateState",
                    payload: {
                        selectZipCodes: arr,
                    },
                });
            } else {
                dispatch({
                    type: "updateState",
                    payload: {
                        selectZipCodes: null,
                    },
                });
            }
        };

        const openModal = (item) => {
            isAdditional ?
                dispatch({
                    type: "updateState",
                    payload: {
                        showModal: !showModal,
                        currentItem: item,
                        active: item.active,
                    },
                })
                :
                dispatch({
                    type: 'updateState',
                    payload: {
                        showServiceModal: !showServiceModal,
                        currentAdditionalService: item,
                        active: item.active,
                        all: item.all,

                    }
                })

        };

        const openDeleteModal = (item) => {
            isAdditional ?
                dispatch({
                    type: "updateState",
                    payload: {
                        showDeleteModal: !showDeleteModal,
                        currentItem: item,
                    },
                })
                :
                dispatch({
                    type: 'updateState',
                    payload: {
                        showDeleteModal: !showDeleteModal,
                        currentAdditionalService: item
                    }
                })

        };

        const changeActive = () => {
            dispatch({
                type: "updateState",
                payload: {
                    active: !active,
                },
            });
        };

        const deleteFunction = () => {
            isAdditional ?
                this.props.dispatch(deleteAdditionalService(currentItem))
                :
                this.props.dispatch(deleteAdditionalServicePrice(currentAdditionalService))

        };

        const saveItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null;
            v.active = active;
            this.props.dispatch(saveAdditionalService(v));
        };

        const openStatusModal = (item) => {
            isAdditional ?
                dispatch({
                    type: "updateState",
                    payload: {
                        showStatusModal: !showStatusModal,
                        currentItem: item,
                    },
                })
                :
                dispatch({
                    type: 'updateState',
                    payload: {
                        showStatusModal: !showStatusModal,
                        currentAdditionalService: item
                    }
                })
        };

        const changeStatusAdditionalService = () => {
            let currentAdditionalService = {...currentItem};
            currentAdditionalService.active = !currentItem.active;
            this.props.dispatch(
                saveAdditionalService(currentAdditionalService)
            );
        };

        const changeTabePane = (tabePane) => {
            if (tabePane === 'additionalService') {
                dispatch({type: types.CHANGE_ADDITIONAL_BOOLEAN})
                dispatch(appAction.getAdditionalServicePage({page: 0, size: 10}))
            }
            if (tabePane === 'additionalServicePrice') {
                dispatch({type: types.CHANGE_ADDITIONAL_PRICE_BOOLEAN})
                dispatch(appAddAction.getAdditionalServicePriceDashboard())
            }
        }
        return (
            <AdminLayout pathname={this.props.location.pathname}>
                <div className="main-service-page container">
                    <Row className="feedback-admin-page">
                        <Col md={3} className="">
                            <div>
                                <h3>{isAdditional ? "Additional Service" : isAdditionalPrice ? "Additional Price" : ''}</h3>
                            </div>
                        </Col>
                        <Col md={12} className="">
                            <div className="route-services ml-auto" style={{width: '32%'}}>
                                <div
                                    onClick={() => changeTabePane('additionalService')}
                                    className={!isAdditional ?
                                        "online" : "router-active"}
                                >
                                    Additional Service
                                </div>
                                <div onClick={() => changeTabePane('additionalServicePrice')}
                                     className={!isAdditionalPrice ?
                                         "in-person" : "router-active"}
                                >
                                    Additional Price
                                </div>
                            </div>
                        </Col>
                    </Row>
                    <div className="flex-row align-items-center button-container">
                        {permissions && permissions.some(item => (item.permissionName === "SAVE_ADDITIONAL_SERVICE" && isAdditional)
                            || (item.permissionName === "SAVE_ADDITIONAL_SERVICE_PRICE" && isAdditionalPrice)) ?
                            <AddIcon
                                className="add-button"
                                onClick={() => openModal('')}
                            />
                            : ""}
                        <span
                            className="dark-title ml-3">
                           {permissions && permissions.some(item => item.permissionName === "SAVE_ADDITIONAL_SERVICE" && isAdditional) ? "Add new additional service" : ""}
                            {permissions && permissions.some(item => item.permissionName === "SAVE_ADDITIONAL_SERVICE_PRICE" && isAdditionalPrice) ? "Add new additional service price" : ""}
                        </span>
                    </div>


                    <Table className="custom-table zipCodeTable-style">
                        <thead>
                        {isAdditional ?
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Status</th>
                                {permissions && permissions.some(item => item.permissionName === "SAVE_ADDITIONAL_SERVICE" || item.permissionName === "DELETE_ADDITIONAL_SERVICE") ?
                                    <th colSpan="2" className="">
                                        Operation
                                    </th>
                                    : ""}
                            </tr>
                            : isAdditionalPrice ?
                                <tr>
                                    <th>#</th>
                                    <th>Service Price</th>
                                    <th>Additional Service</th>
                                    <th>Price</th>
                                    <th>Status</th>
                                    {permissions && permissions.some(item => item.permissionName === "EDIT_ADDITIONAL_SERVICE_PRICE" || item.permissionName === "DELETE_ADDITIONAL_SERVICE_PRICE") ?
                                        <th colSpan="2" className="">
                                            Operation
                                        </th> : ""}
                                </tr> : ""
                        }
                        </thead>
                        {
                            isAdditional ?
                                <tbody>
                                {additionalServicePage && additionalServicePage.map((item, i) => (
                                    <tr key={item.id}>
                                        <td>{i + 1}</td>
                                        <td>{item.name}</td>
                                        <td>{item.description}</td>
                                        <td>
                                            <FormGroup check>
                                                <Label check for="active" className={'labelDuo'}>
                                                    <div className={'labelBlock'}>
                                                        {item.active && <div className={'labelBox'}>
                                                            <i className="fas fa-check"></i>
                                                        </div>}
                                                    </div>
                                                    {permissions && permissions.some(item => item.permissionName === "SAVE_ADDITIONAL_SERVICE") ?
                                                        <Input
                                                            className='d-none'
                                                            type="checkbox"
                                                            onClick={() =>
                                                                openStatusModal(
                                                                    item
                                                                )
                                                            }
                                                            id="active"
                                                            checked={item.active}
                                                        /> : ""}
                                                    {"Active"}
                                                </Label>
                                            </FormGroup>
                                        </td>
                                        {permissions && permissions.some(item => item.permissionName === "SAVE_ADDITIONAL_SERVICE") ?
                                            <td>
                                                <EditIcon
                                                    onClick={() => openModal(item)}
                                                />
                                            </td> : ""}
                                        {permissions && permissions.some(item => item.permissionName === "DELETE_ADDITIONAL_SERVICE") ?
                                            <td>
                                                <DeleteIcon
                                                    onClick={() =>
                                                        openDeleteModal(item)
                                                    }
                                                />
                                            </td> : ""}
                                    </tr>
                                ))}
                                </tbody>
                                :
                                <tbody>
                                {additionalDashboard && additionalDashboard.map((item, i) => (
                                    <tr>
                                        <td>{i + 1}</td>
                                        <td>{item.mainServiceName} -
                                            {item.subServiceName}
                                        </td>
                                        <td>
                                            {item.additionalServiceName}
                                        </td>
                                        <td>
                                            {item.minPrice === item.maxPrice ? "$" + item.minPrice :
                                                "$" + item.minPrice + " - " + "$" + item.maxPrice
                                            }
                                        </td>
                                        <td>
                                            <FormGroup check>
                                                <Label
                                                    check
                                                    for={active}
                                                    className={'labelDuo'}
                                                >
                                                    <div className={'labelBlock'}>
                                                        {item.serviceActive && <div className={'labelBox'}>
                                                            <i className="fas fa-check"></i>
                                                        </div>}
                                                    </div>
                                                    {permissions && permissions.some(item => item.permissionName === "ACTIVE_ADDITIONAL_SERVICE_PRICE") ?
                                                        <Input
                                                            className={'d-none'}
                                                            type="checkbox"
                                                            onClick={() =>
                                                                openStatusModal(item)
                                                            }
                                                            id={item.id}
                                                            checked={item.serviceActive}
                                                        />
                                                        : ""}
                                                    {"Active"}
                                                </Label>
                                            </FormGroup>

                                        </td>
                                        <td>
                                            <EditIcon
                                                onClick={() => openModal(item)}
                                            />
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                        }
                    </Table>

                    {isAdditional ?
                        <Row>
                            <Col md={12}>
                                <Pagination
                                    activePage={activePageAddService + 1}
                                    itemsCountPerPage={10}
                                    totalItemsCount={totalElementsAdditional}
                                    pageRangeDisplayed={5}
                                    onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                                    linkClass="page-link"
                                />
                            </Col>
                        </Row>
                        : ""}

                    {isAdditional ?
                        <Modal
                            id="allModalStyle"
                            isOpen={showModal}
                            toggle={openModal}
                        >
                            <AvForm onValidSubmit={saveItem}>
                                <div className='position-absolute' style={{top: '45px', left: '-12%'}}>
                                    <CloseBtn click={openModal}/>
                                </div>
                                <ModalHeader
                                    toggle={openModal}
                                    charCode="x"
                                    className="model-head"
                                >
                                    {currentItem != null && currentItem.name
                                        ? "Edit Additional Service"
                                        : "Add Additional Service"}
                                </ModalHeader>
                                <ModalBody>
                                    <AvField
                                        name="name"
                                        label="Name"
                                        required
                                        defaultValue={
                                            currentItem != null
                                                ? currentItem.name
                                                : ""
                                        }
                                        placeholder="Enter Additional Service Name"
                                    />
                                    <AvField
                                        name="description"
                                        label="Description"
                                        required
                                        defaultValue={
                                            currentItem != null
                                                ? currentItem.description
                                                : ""
                                        }
                                        placeholder="Enter description"
                                    />

                                    <div className="flex-row justify-content-center">
                                        <label className={'labelDuo'}>
                                            <div className={'labelBlock'}>
                                                {active && <div className={'labelBox'}>
                                                    <i className="fas fa-check"></i>
                                                </div>}
                                            </div>
                                            <Input
                                                className="check-box-title d-none"
                                                type="checkbox"
                                                checked={active}
                                                onChange={changeActive}
                                            />
                                            Active
                                        </label>

                                        <br/>
                                    </div>
                                </ModalBody>
                                <ModalFooter>
                                    {/*<Button*/}
                                    {/*    type="button"*/}
                                    {/*    color="secondary"*/}
                                    {/*    outline*/}
                                    {/*    onClick={openModal}*/}
                                    {/*>*/}
                                    {/*    Cancel*/}
                                    {/*</Button>{" "}*/}
                                    <Button color="info" outline disabled={loading}>Save</Button>
                                </ModalFooter>
                            </AvForm>
                        </Modal>
                        :
                        <Modal id="allModalStyle" isOpen={showServiceModal} toggle={openModal}>
                            <AvForm onValidSubmit={saveItemPrice}>
                                <div className='position-absolute' style={{top: '45px', left: '-12%'}}>
                                    <CloseBtn click={openModal}/>
                                </div>
                                <ModalHeader
                                    toggle={openModal}
                                    charCode="x"
                                    className="model-head">
                                    {currentAdditionalService.serviceId != null
                                        ? "Edit Additional Service Price"
                                        : "Add Additional Service Price"}
                                </ModalHeader>
                                <ModalBody>
                                    <AvField type="select" name="serviceId"
                                             onChange={(item) => this.handleChangeOnline(item.target.value)}
                                             value={currentAdditionalService ?
                                                 (currentAdditionalService.mainServiceName + " " + currentAdditionalService.subServiceName) : "0"}
                                             required>
                                        <option value="0" selected>Select Service</option>
                                        {dashboard.map(item =>
                                            <option key={item.serviceId}
                                                    value={item.serviceId}>{item.subServiceName} - {item.mainServiceName} ,
                                                ${item.minPrice === item.maxPrice ? item.minPrice :
                                                    item.minPrice + " - " + item.maxPrice
                                                }

                                            </option>
                                        )}
                                    </AvField>
                                    <AvField
                                        type="select" name="additionalServiceId"
                                        selected={currentAdditionalService ? (currentAdditionalService.additionalServiceName) : "0"}
                                        required>
                                        <option value="0" selected>Select additional service</option>
                                        {additionalServices.map(item =>
                                            <option key={item.id} value={item.id}> {item.name}</option>
                                        )}
                                    </AvField>
                                    <AvField
                                        name="price"
                                        label="Price" required
                                        type="number"
                                        defaultValue={currentAdditionalService ?
                                            currentAdditionalService.price : ""}
                                        placeholder="Enter price"/>

                                    <div className="flex-row space-around">
                                        {this.state.isOnline ? "" :
                                            <label className={'labelDuo'}>
                                                <div className={'labelBlock'} style={{left: '-20%'}}>
                                                    {all && <div className={'labelBox'}>
                                                        <i className="fas fa-check"></i>
                                                    </div>}
                                                </div>
                                                <Input
                                                    className="check-box-title d-none"
                                                    type="checkbox"
                                                    checked={all}
                                                    onChange={changeAll}
                                                />
                                                Add all zip code
                                            </label>
                                        }
                                        <label className={'labelDuo'}>
                                            <div className={'labelBlock'}>
                                                {active && <div className={'labelBox'}>
                                                    <i className="fas fa-check"></i>
                                                </div>}
                                            </div>
                                            <Input
                                                className="check-box-title d-none"
                                                type="checkbox"
                                                checked={active}
                                                onChange={changeActive}
                                            />
                                            Active
                                        </label>

                                    </div>
                                    {this.state.isOnline ? "" :
                                        <Select
                                            isDisabled={all}
                                            defaultValue="Select State"
                                            isMulti
                                            name="statesId"
                                            options={stateOptions}
                                            onChange={getCounty}
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        />
                                    }
                                    {this.state.isOnline ? "" :
                                        selectStates !== null && selectStates && selectStates.length === 1 ?
                                            <Select
                                                isDisabled={all}
                                                defaultValue="Select County"
                                                isMulti
                                                name="countiesId"
                                                options={countyOptions}
                                                onChange={getZipCode}
                                                className="basic-multi-select"
                                                classNamePrefix="select"
                                            /> : ""}
                                    {this.state.isOnline ? "" :
                                        selectCounties !== null && selectCounties && selectCounties.length === 1 ?
                                            <Select
                                                isDisabled={all}
                                                defaultValue="Select Zipcode"
                                                isMulti
                                                name="zipCodesId"
                                                options={zipCodeOptions}
                                                onChange={saveZipCodeForAdditionalServicePrice}
                                                className="basic-multi-select"
                                                classNamePrefix="select"
                                            /> : ""}
                                </ModalBody>
                                <ModalFooter>
                                    {/*<Button*/}
                                    {/*    type="button"*/}
                                    {/*    color="secondary"*/}
                                    {/*    outline*/}
                                    {/*    onClick={openModal}*/}
                                    {/*>*/}
                                    {/*    Cancel*/}
                                    {/*</Button>{" "}*/}
                                    <Button color="info" outline disabled={loading}>Save</Button>
                                </ModalFooter>
                            </AvForm>
                        </Modal>

                    }


                    {isAdditionalPrice ?
                        showStatusModal &&
                        <StatusModal text={currentAdditionalService ? currentAdditionalService.name : ''}
                                     showStatusModal={showStatusModal}
                                     confirm={changeStatusAdditionalServicePrice}
                                     cancel={openStatusModal}/>
                        :
                        showStatusModal && (
                            <StatusModal
                                text={currentItem != null ? currentItem.name : ""}
                                showStatusModal={showStatusModal}
                                confirm={changeStatusAdditionalService}
                                cancel={openStatusModal}
                            />
                        )
                    }

                    {isAdditionalPrice ? showDeleteModal &&
                        <DeleteModal text={currentAdditionalService ? currentAdditionalService.name : ''}
                                     showDeleteModal={showDeleteModal}
                                     confirm={deleteFunction}
                                     cancel={openDeleteModal}
                        />
                        :
                        showDeleteModal && (
                            <DeleteModal
                                text={currentItem != null ? currentItem.name : ""}
                                showDeleteModal={showDeleteModal}
                                confirm={deleteFunction}
                                cancel={openDeleteModal}
                            />)
                    }

                </div>
            </AdminLayout>
        );
    }
}


export default connect(
    ({
         auth: {permissions},
         app: {
             activePageAddService,
             totalElementsAdditional,
             additionalServicePage,
             showModal,
             currentItem,
             active,
             showDeleteModal,
             showStatusModal,
             additionalServices,
             isAdditional,
             isAdditionalPrice,
             stateOptions,
             zipCodes,
             states, counties, services, loading,

         },
         additionalServicePrice: {
             zipCodeOptions, countyOptions, additionalServicePrices, showServiceModal, currentAdditionalService, all,
             selectZipCodes, selectCounties, selectStates, additionalDashboard
         },
         service: {
             dashboard
         },
     }) => ({
        permissions,
        activePageAddService,
        totalElementsAdditional,
        additionalServicePage,
        showModal,
        currentItem,
        active,
        showDeleteModal,
        showStatusModal,
        additionalServices,
        isAdditional,
        isAdditionalPrice,
        zipCodeOptions, selectZipCodes, selectCounties, selectStates,
        stateOptions, countyOptions,
        additionalServicePrices,
        zipCodes,
        states,
        counties,
        showServiceModal,
        currentAdditionalService, all,
        dashboard, additionalDashboard, services, loading
    })
)(AdditionalService);

// Icons
function EditIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{cursor: "pointer"}}
                width="18"
                height="18"
                viewBox="0 0 18 18"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M12.728 6.68599L11.314 5.27199L2 14.586V16H3.414L12.728 6.68599ZM14.142 5.27199L15.556 3.85799L14.142 2.44399L12.728 3.85799L14.142 5.27199ZM4.242 18H0V13.757L13.435 0.321992C13.6225 0.134521 13.8768 0.0292053 14.142 0.0292053C14.4072 0.0292053 14.6615 0.134521 14.849 0.321992L17.678 3.15099C17.8655 3.33852 17.9708 3.59283 17.9708 3.85799C17.9708 4.12316 17.8655 4.37746 17.678 4.56499L4.243 18H4.242Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}

function DeleteIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{cursor: "pointer"}}
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M2 6H18V19C18 19.2652 17.8946 19.5196 17.7071 19.7071C17.5196 19.8946 17.2652 20 17 20H3C2.73478 20 2.48043 19.8946 2.29289 19.7071C2.10536 19.5196 2 19.2652 2 19V6ZM4 8V18H16V8H4ZM7 10H9V16H7V10ZM11 10H13V16H11V10ZM5 3V1C5 0.734784 5.10536 0.48043 5.29289 0.292893C5.48043 0.105357 5.73478 0 6 0H14C14.2652 0 14.5196 0.105357 14.7071 0.292893C14.8946 0.48043 15 0.734784 15 1V3H20V5H0V3H5ZM7 2V3H13V2H7Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}
