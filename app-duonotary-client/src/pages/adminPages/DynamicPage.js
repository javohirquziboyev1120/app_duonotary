import React, {Component} from 'react';
import AdminLayout from "../../components/AdminLayout";
import {uploadFile} from "../../redux/actions/AttachmentAction";
import TextEditor from "../../components/TextEditor";
import {PostSmall} from "../../components/BlogPost";
import {config} from "../../utils/config";
import {Col, Row} from "reactstrap";
import Pagination from "react-js-pagination";
import {connect} from "react-redux";
import {deleteBlog, getDynamicList, saveDynamic} from "../../redux/actions/AppAction";
import {toast} from "react-toastify";
import UploadFile from "../../components/UploadFile";

class DynamicPage extends Component {
    componentDidMount() {
        this.props.dispatch(getDynamicList({page: 0, size: 10}))
    }
    constructor(props) {
        super(props);
        this.state={
            title: '',
            editorHtml: '',
            id: '',
            isEditing: false,
        }
    }
    handleTitleChange = (e) => {
        this.setState({title: e.target.value})
    }
    handleChange = (html) => {
        this.setState({editorHtml: html});

    }
    handlePageChange(pageNumber) {
        this.props.dispatch(getDynamicList({page:pageNumber-1, size:10}))
        this.props.dispatch(
            {type:'updateState',
                payload:{
                    activePageDynamic: pageNumber-1
                }})
    }

    saveItem=()=>{
        let attach=this.props.attachmentIdsArray.filter(item=>item.key==="category")[0];
        let value={};
        if( this.state.editorHtml !== '' &&
            this.state.title !== '' &&
            attach){
            value={
                id:this.state.id!=null?this.state.id:null,
                title: this.state.title,
                text: this.state.editorHtml,
                attachmentId:attach?attach.value:null,
            }
            this.props.dispatch(saveDynamic(value));
            this.defaultState();
        } else {
            toast.warning("Please fill all required fields");
        }

    }

    toggleEditPostPage = () => {
        this.setState({
            isEditing: !this.state.isEditing
        })
    }
    setEditValues = (item) =>{
        this.setState({
            title:item.title,
            editorHtml : item.text,
            attachment:item.attachmentId,
            id:item.id,
        })
    }

    defaultState =() =>{
        this.setState({
            editorHtml: '',
            editPostPage: false,
            title: '',
            attachment:'',
            id:'',
        })
    }

    render() {
        const {attachmentIdsArray,
            showEmbassyModal, page, size,embassy, dispatch, showModal,  currentItem, active, showDeleteModal, showStatusModal,
            activePageDynamic,
            totalElementsDynamic,
            dynamicPage,
        } = this.props;

        const deleteBlogs=(e)=>{
            let deleteBl={};
            deleteBl={id:this.state.id}
            this.props.dispatch(deleteBlog(deleteBl))
        }
        return (
            <AdminLayout  pathname={this.props.location.pathname}>
                <div className="d-flex container mt-5 dynamic-page">
                    <div className="w-75 mr-3 flex-column new-post-section edit-post-section margin-bottom-50">
                            <span className="dark-title margin-top-25">Create new page</span>
                            <input className="input-title" name="title" value={this.state.title} onChange={this.handleTitleChange}
                                   placeholder="Title"/>
                        {
                            this.state.attachment?<img src={config.BASE_URL+"/attachment/"+this.state.attachment} className="margin-top-15 edit-post-image"
                                                      alt="blog-img"/>:''
                        }
                            <div className="flex-row align-items-center margin-top-15">
                                <span className="dark-title margin-right-15">Choose main image*: </span>
                                <UploadFile onChange={(item)=>dispatch(uploadFile(item.target.files[0],"category"))} className="input-file"/>
                            </div>
                            <div className="input-text-area">
                                <TextEditor editorHtml={this.state.editorHtml}  name="text"  handleChange={this.handleChange}/>
                            </div>
                        {
                            this.state.isEditing ? (
                                <div className="flex-row align-self-end">

                                    <button className="publish-btn delete-btn-color margin-right-15" onClick={(e) => {
                                        deleteBlogs(e);
                                        this.defaultState();
                                    }}>Delete
                                    </button>
                                    <button className="publish-btn cancel-btn-color margin-right-15"
                                            onClick={() => {
                                                this.defaultState();
                                            }}>Cancel
                                    </button>
                                    <button className="publish-btn" onClick={(e) => {
                                        this.saveItem(e);
                                        this.defaultState();
                                    }}>Update
                                    </button>
                                </div>
                            ) : <button className="publish-btn align-self-end" onClick={this.saveItem}>Publish</button>
                        }


                    </div>

                    <div className="w-25 flex-column small-posts p-0 ml-3">
                        <span className="featured-posts-title w-100">Edit pages</span>
                        {
                            dynamicPage?dynamicPage.map((item, index)=>(
                                <PostSmall key={index}  title={item.title}
                                           mainImg={config.BASE_URL+"/attachment/"+item.attachmentId}
                                           onClick={
                                               ()=>{
                                                   this.setEditValues(item);
                                                   this.toggleEditPostPage();
                                               }
                                           }
                                />
                            )):""
                        }


                        <Row>
                            <Col md={7} className="mx-auto">
                                <Pagination
                                    activePage={activePageDynamic+1}
                                    itemsCountPerPage={10}
                                    totalItemsCount={totalElementsDynamic}
                                    pageRangeDisplayed={5}
                                    onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                                    linkClass="page-link"
                                />
                            </Col>
                        </Row>
                    </div>
                </div>
            </AdminLayout>
        );
    }
}

export default connect(
    ({
         app: {
             page, size, totalElements,
             embassy,
             showEmbassyModal,
             countries, showModal, currentItem, active, showDeleteModal, showStatusModal,
             activePageDynamic,
             totalElementsDynamic,
             dynamicPage,

         },
         attachment:{attachmentIdsArray}
     }) => ({
        page, size, totalElements,attachmentIdsArray,
        embassy,
        showEmbassyModal,
        countries,
        showModal,
        currentItem,
        active, showDeleteModal, showStatusModal,
        activePageDynamic,
        totalElementsDynamic,
        dynamicPage,

    }))(DynamicPage);