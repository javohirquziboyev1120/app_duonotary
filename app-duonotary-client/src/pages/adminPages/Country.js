import React, {Component} from 'react';
import {AvField, AvForm} from 'availity-reactstrap-validation';
import {
    Button,
    CustomInput,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table
} from 'reactstrap'
import {connect} from "react-redux";
import {deleteCountry, getCountryList, saveCountry} from "../../redux/actions/AppAction";
import DeleteModal from "../../components/Modal/DeleteModal";
import StatusModal from "../../components/Modal/StatusModal";
import AdminLayout from "../../components/AdminLayout";
import Pagination from "react-js-pagination";
import {AddIcon} from "../../components/Icons";

class Country extends Component {
    componentDidMount() {
        this.props.dispatch({
            type: "updateState",
            payload: {
                showModal: false,
                showStatusModal: false,
                showDeleteModal: false,
                page: 0,
                size: 20,
                totalElements: 0
            }
        })
        this.props.dispatch(getCountryList())
    }

    handlePageChange(pageNumber) {
        this.props.dispatch(getCountryList({page: pageNumber - 1, size: 20}))
        this.props.dispatch(
            {
                type: 'updateState',
                payload: {
                    page: pageNumber - 1,
                    size: 20
                }
            })
    }

    render() {
        const {permissions, showEmbassyModal, page, size, totalElements, embassy, dispatch, showModal, countries, currentItem, active, showDeleteModal, showStatusModal} = this.props;

        const saveItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null
            v.active = active
            v.embassy = embassy
            this.props.dispatch(saveCountry(v))
        }
        const changeStatusCountry = () => {
            let currentCountry = {...currentItem};
            currentCountry.active = !currentItem.active;
            this.props.dispatch(saveCountry(currentCountry))
        }
        const changeEmbassyCountry = () => {
            let currentCountry = {...currentItem};
            currentCountry.embassy = !currentItem.embassy;
            this.props.dispatch(saveCountry(currentCountry))
        }

        const openModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showModal: !showModal,
                    currentItem: item,
                    active: item.active,
                    embassy: item.embassy
                }
            })
        };
        const openDeleteModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentItem: item
                }
            })
        };
        const deleteFunction = () => {
            this.props.dispatch(deleteCountry(currentItem))
        };
        const openStatusModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item
                }
            })
        };
        const openEmbassyModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showEmbassyModal: !showEmbassyModal,
                    currentItem: item
                }
            })
        };
        const changeActive = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    active: !active
                }
            })
        };
        const changeEmbassy = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    embassy: !embassy
                }
            })
        };

        return (
            <AdminLayout pathname={this.props.location.pathname}>
                <div className="country-page">
                    <h2 className="text-center">Country list</h2>
                    {permissions && permissions.some(item => item.permissionName === "SAVE_COUNTRY") ?
                        <AddIcon
                            title={true}
                            onClick={() => openModal("")}
                        /> : ""}
                    <Table className="custom-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Embassy</th>
                            {permissions && permissions.some(item => item.permissionName === "SAVE_COUNTRY" || item.permissionName === "DELETE_COUNTRY") ?
                                <th colSpan="2">Operation</th>
                                : ""}
                        </tr>
                        </thead>
                        {countries != null ?
                            <tbody>
                            {countries.map((item, i) =>
                                <tr key={item.id}>
                                    <td>{i + 1}</td>
                                    <td>{item.name}</td>
                                    <td>
                                        <FormGroup check>
                                            <Label check className={'labelDuo'}>
                                                <div className={'labelBlock'}>
                                                    {item.active && <div className={'labelBox'}>
                                                        <i className="fas fa-check"></i>
                                                    </div>}
                                                </div>
                                                {permissions && permissions.some(item => item.permissionName === "SAVE_COUNTRY") ?
                                                    <Input
                                                        className={'d-none'}
                                                        type="checkbox"
                                                        onClick={() => openStatusModal(item)}
                                                        checked={item.active}
                                                    /> : ""}
                                                Active
                                            </Label>
                                        </FormGroup>
                                    </td>
                                    <td>
                                        <FormGroup check>
                                            <Label check className={'labelDuo'}>
                                                <div className={'labelBlock'}>
                                                    {item.embassy && <div className={'labelBox'}>
                                                        <i className="fas fa-check"></i>
                                                    </div>}
                                                </div>
                                                {permissions && permissions.some(item => item.permissionName === "SAVE_COUNTRY") ?
                                                    <Input
                                                        className={'d-none'}
                                                        type="checkbox"
                                                        onClick={() => openEmbassyModal(item)}
                                                        checked={item.embassy}
                                                    /> : ""}
                                                Embassy
                                            </Label>
                                        </FormGroup>
                                    </td>
                                    {permissions && permissions.some(item => item.permissionName === "SAVE_COUNTRY") ?
                                        <td>
                                            <EditIcon
                                                onClick={() => openModal(item)}
                                            />
                                        </td> : ""}
                                    {permissions && permissions.some(item => item.permissionName === "DELETE_COUNTRY") ?
                                        <td>
                                            <DeleteIcon
                                                onClick={() =>
                                                    openDeleteModal(item)
                                                }
                                            />
                                        </td> : ""}
                                </tr>
                            )}
                            </tbody> :
                            <tbody>
                            <tr>
                                <td colSpan="4">
                                    <h3 className="text-center mx-auto"> No information </h3>
                                </td>
                            </tr>
                            </tbody>
                        }
                    </Table>
                    <Pagination
                        activePage={page + 1}
                        itemsCountPerPage={size}
                        totalItemsCount={totalElements}
                        pageRangeDisplayed={5}
                        onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                        linkClass="page-link"
                    />

                    <Modal isOpen={showModal} toggle={openModal}>
                        <AvForm onValidSubmit={saveItem}>
                            <ModalHeader toggle={openModal}
                                         charCode="x">{currentItem != null ? "Edit country" : "Add country"}</ModalHeader>
                            <ModalBody>
                                <AvField name="name" label="Name" required
                                         defaultValue={currentItem != null ? currentItem.name : ""}
                                         placeholder="Enter country name"
                                />

                                <div className='d-flex align-items-center justify-content-around'>
                                    <div>
                                        <label className={'labelDuo'}>
                                            <div className={'labelBlock'}>
                                                {active && <div className={'labelBox'}>
                                                    <i className="fas fa-check"></i>
                                                </div>}
                                            </div>
                                            <CustomInput
                                                className={'d-none'}
                                                type="checkbox"
                                                checked={active}
                                                onChange={changeActive}
                                            />
                                            Active
                                        </label>
                                    </div>

                                    <div>
                                        <label className={'labelDuo'}>
                                            <div className={'labelBlock'}>
                                                {embassy && <div className={'labelBox'}>
                                                    <i className="fas fa-check"></i>
                                                </div>}
                                            </div>
                                            <CustomInput
                                                className={'d-none'}
                                                type="checkbox"
                                                checked={embassy}
                                                onChange={changeEmbassy}
                                            />
                                            Embassy
                                        </label>
                                    </div>
                                </div>
                            </ModalBody>
                            <ModalFooter>
                                <Button type="button" color="" onClick={openModal}>Cancel</Button>{' '}
                                <Button color="success">Save</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>

                    {showDeleteModal && <DeleteModal text={currentItem != null ? currentItem.name : ''}
                                                     showDeleteModal={showDeleteModal}
                                                     confirm={deleteFunction}
                                                     cancel={openDeleteModal}
                    />}

                    {showStatusModal && <StatusModal text={currentItem != null ? currentItem.name : ''}
                                                     showStatusModal={showStatusModal}
                                                     confirm={changeStatusCountry}
                                                     cancel={openStatusModal}
                    />}
                    {showEmbassyModal && <StatusModal text={currentItem != null ? (" embassy " + currentItem.name) : ''}
                                                      showStatusModal={showEmbassyModal}
                                                      confirm={changeEmbassyCountry}
                                                      cancel={openEmbassyModal}
                    />}

                </div>
            </AdminLayout>
        );
    }
}

export default connect(
    ({
         app: {
             page, size, totalElements,
             embassy,
             showEmbassyModal,
             countries, showModal, currentItem, active, showDeleteModal, showStatusModal
         },
         auth: {permissions}
     }) => ({
        permissions,
        page, size, totalElements,
        embassy,
        showEmbassyModal,
        countries,
        showModal,
        currentItem,
        active, showDeleteModal, showStatusModal
    }))(Country);

// Icons
function EditIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{cursor: "pointer"}}
                width="18"
                height="18"
                viewBox="0 0 18 18"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M12.728 6.68599L11.314 5.27199L2 14.586V16H3.414L12.728 6.68599ZM14.142 5.27199L15.556 3.85799L14.142 2.44399L12.728 3.85799L14.142 5.27199ZM4.242 18H0V13.757L13.435 0.321992C13.6225 0.134521 13.8768 0.0292053 14.142 0.0292053C14.4072 0.0292053 14.6615 0.134521 14.849 0.321992L17.678 3.15099C17.8655 3.33852 17.9708 3.59283 17.9708 3.85799C17.9708 4.12316 17.8655 4.37746 17.678 4.56499L4.243 18H4.242Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}


function DeleteIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{cursor: "pointer"}}
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M2 6H18V19C18 19.2652 17.8946 19.5196 17.7071 19.7071C17.5196 19.8946 17.2652 20 17 20H3C2.73478 20 2.48043 19.8946 2.29289 19.7071C2.10536 19.5196 2 19.2652 2 19V6ZM4 8V18H16V8H4ZM7 10H9V16H7V10ZM11 10H13V16H11V10ZM5 3V1C5 0.734784 5.10536 0.48043 5.29289 0.292893C5.48043 0.105357 5.73478 0 6 0H14C14.2652 0 14.5196 0.105357 14.7071 0.292893C14.8946 0.48043 15 0.734784 15 1V3H20V5H0V3H5ZM7 2V3H13V2H7Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}