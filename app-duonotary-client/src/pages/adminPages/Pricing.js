import React, {Component} from "react";
import AdminLayout from "../../components/AdminLayout";
import {
    deletePricing,
    editPricing,
    editServiceChangeActive,
    getDashboardPricingList,
    getPricingByStates,
    getServiceList,
    getServicePriceDashboard,
    getServicePrices,
    getStateForPricing,
    savePricing,
} from "../../redux/actions/AppAction";
import {Button, Col, Input, Modal, ModalBody, ModalFooter, ModalHeader, Table,} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import StatusModal from "../../components/Modal/StatusModal";
import {connect} from "react-redux";
import Select from "react-select";
import {AddIcon} from "../../components/Icons";
import {toast} from "react-toastify";
import DeleteModal from "../../components/Modal/DeleteModal";
import CloseBtn from "../../components/CloseBtn";

class Pricing extends Component {
    componentDidMount() {
        this.props.dispatch(getDashboardPricingList());
        this.props.dispatch(getServicePrices());
        this.props.dispatch(getServiceList())
        this.props.dispatch(getServicePriceDashboard())
    }

    constructor(props) {
        super(props);
        this.state = {
            inPerson: false,
            checked: false,
            serviceId: '',
            isPricing: false,
            stateName: "",
            statesId: null

        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(checked) {
        this.setState({checked});
    }

    handleChangeOnline = (id) => {
        this.props.services.map(a => {
            if (a.id === id) {
                this.setState({isOnline: a.mainService.online, serviceId: a.id})
                this.props.dispatch(getStateForPricing(a.id))
            }
        })
    }

    handleGetPricing = (stateId, stateName) => {
        let data = {stateId: stateId}
        this.props.dispatch(getPricingByStates(data))
        this.setState({isPricing: true, stateName: stateName, statesId: stateId})
    }

    render() {
        const {
            services,
            dispatch,
            showModal,
            currentItem,
            active,
            showDeleteModal,
            showStatusModal,
            dashboard,
            all,
            pricingDashboard,
            loading,
            selectStateByPricing,
            selectCountyByPricing,
            optionsStateByPricing,
            pricingByStates,
            showEditModal
        } = this.props;

        const backButton = () => {
            this.setState({isPricing: false, stateName: ''})
            dispatch({
                type: 'updateState',
                payload: {
                    pricingByStates: []
                }
            })
        }
        const openModal = (item) => {
            if (item && item.serviceId)
                dispatch(getStateForPricing(item.serviceId))
            this.setState({serviceId: item.serviceId})
            dispatch({
                type: "updateState",
                payload: {
                    selectStateByPricing: null,
                    selectCountyByPricing: null,
                    selectZipCodeByPricing: null,
                    showModal: !showModal,
                    currentItem: item,
                    active: item.active,
                }
            });
        };
        const openEditModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    selectStateByPricing: null,
                    selectCountyByPricing: null,
                    selectZipCodeByPricing: null,
                    showEditModal: !showEditModal,
                    currentItem: item,
                    active: item.active,
                }
            });
        };

        const changeActive = () => {
            dispatch({
                type: "updateState",
                payload: {
                    active: !active,
                },
            });
        };
        const changeAll = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    all: !all
                }
            })
        };

        const saveItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null
            v.active = active
            v.all = all
            v.statesId = this.state.isOnline ? null : selectStateByPricing
            if (all) {
                v.statesId = optionsStateByPricing.map(item => {
                    return item.value
                });
            }
            if (parseInt(v.fromCount) > 0 && parseInt(v.tillCount) > 0
                && parseInt(v.fromCount) < parseInt(v.tillCount)
                && v.everyCount === "") {
                this.props.dispatch(savePricing(v))
            } else if (v.fromCount.length === 0 && v.tillCount.length === 0 && v.everyCount.length > 0 && parseInt(v.everyCount) > 0) {
                this.props.dispatch(savePricing(v))
            } else {
                toast.error("You inserting the start value must not be less than the end value!");
            }
        }
        const editItem = (e, v) => {
            v.active = active
            v.fromCount = currentItem.fromCount
            v.tillCount = currentItem.tillCount
            v.everyCount = currentItem.everyCount
            v.price = currentItem.price
            v.stateId = this.state.statesId
            v.subServiceName = currentItem.subServiceName
            if (parseInt(v.newFromCount) > 0 && parseInt(v.newTillCount) > 0
                && parseInt(v.newFromCount) < parseInt(v.newTillCount)
                && v.newEveryCount === "") {
                this.props.dispatch(editPricing(v))
            } else if (v.newFromCount.length === 0 && v.newTillCount.length === 0 && v.newEveryCount.length > 0 && parseInt(v.newEveryCount) > 0) {
                this.props.dispatch(editPricing(v))
            } else {
                toast.error("You inserting the start value must not be less than the end value!");
            }
            // this.props.dispatch(editPricing(v))
        }

        const deleteFunction = () => {
            let v = {
                fromCount: currentItem.fromCount,
                tillCount: currentItem.tillCount,
                everyCount: currentItem.everyCount,
                price: currentItem.price,
                stateId: this.state.statesId,
                subServiceName: currentItem.subServiceName
            }
            this.props.dispatch(deletePricing(v))
        }
        const openDeleteModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentItem: item,
                },
            });
        };

        const changeStatusPricing = () => {
            let currentPricing = {...currentItem};
            currentPricing.active = !currentItem.active;
            this.props.dispatch(editServiceChangeActive(currentPricing))
        };

        const openStatusModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item,
                },
            });
        };

        const getCounty = (e, v) => {
            v.serviceId = this.state.serviceId
            if (!all) {
                let arr = [];
                if (e && e.length === 1) {
                    if (v.action === "remove-value") {
                        v.option = e[0];
                    }
                    // dispatch(getCountyForPricing(v));
                }
                if (e) {
                    e.map((item) => arr.push(item.value));
                    dispatch({
                        type: "updateState",
                        payload: {
                            selectStateByPricing: arr,
                            selectCountyByPricing: null,
                        },
                    });
                } else {
                    dispatch({
                        type: "updateState",
                        payload: {
                            selectCountyByPricing: null,
                            selectZipCodeByPricing: null,
                        },
                    });
                }
            }
        };
        return (
            <AdminLayout pathname={this.props.location.pathname}>
                {this.state.isPricing && pricingByStates ?
                    <div className="main-service-page container">
                        <h2>{this.state.stateName}</h2>
                        <CloseIcon disabled={loading} onClick={backButton}/>
                        <Table className="custom-table mt-2">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Service</th>
                                <th>Count</th>
                                <th>Every Count</th>
                                <th>Price</th>
                                <th colSpan="2">Operation</th>
                            </tr>
                            </thead>
                            {pricingByStates ? (
                                <tbody>
                                {pricingByStates.map((item, i) => (
                                    <tr key={i}>
                                        <td>{i + 1}</td>
                                        <td>{item.subServiceName}</td>
                                        <td>{item.fromCount} - {item.tillCount}</td>
                                        <td>{item.everyCount}</td>
                                        <td> $ {item.price}</td>
                                        <td>
                                            <EditIcon
                                                onClick={() => openEditModal(item)}
                                            />
                                        </td>
                                        <td>
                                            <DeleteIcon
                                                onClick={() =>
                                                    openDeleteModal(item)
                                                }
                                            />
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            ) : (
                                <tbody>
                                <tr>
                                    <td colSpan="4">
                                        <h3 className="text-center mx-auto">
                                            {" "}
                                            No information{" "}
                                        </h3>
                                    </td>
                                </tr>
                                </tbody>
                            )}
                        </Table>

                        <Modal
                            id="allModalStyle"
                            isOpen={showEditModal}
                            toggle={openEditModal}
                        >
                            <div className='position-absolute' style={{top : '45px', left : '-12%'}}>
                                <CloseBtn click={openEditModal} />
                            </div>
                            <AvForm onValidSubmit={editItem}>
                                <ModalHeader
                                    toggle={openModal}
                                    charCode="x"
                                    className="model-head"
                                >
                                    {"Edit Pricing"}
                                </ModalHeader>
                                <ModalBody>
                                    <AvField
                                        name="newPrice"
                                        label="Price"
                                        type="number"
                                        defaultValue={
                                            currentItem != null
                                                ? currentItem.price
                                                : " "}
                                        placeholder="Enter Price $"
                                    />

                                    <div className="flex-row-times margin-top-15">
                                        <AvField
                                            name="newFromCount"
                                            label="From Count"
                                            type="number"
                                            defaultValue={
                                                currentItem != null
                                                    ? currentItem.fromCount
                                                    : " "}
                                            placeholder="Enter from count "
                                        />
                                        <AvField
                                            name="newTillCount"
                                            label="Till Count"
                                            type="number"
                                            defaultValue={
                                                currentItem != null
                                                    ? currentItem.tillCount
                                                    : " "
                                            }
                                            placeholder="Enter till count "
                                        />
                                    </div>
                                    <AvField
                                        name="newEveryCount"
                                        label="Every Count"
                                        type="number"
                                        defaultValue={
                                            currentItem != null
                                                ? currentItem.everyCount
                                                : " "
                                        }
                                        placeholder="Enter every count "
                                    />
                                    <div className="flex-row space-around">
                                        <label className={'labelDuo'}>
                                            <div className={'labelBlock'}>
                                                {active && <div className={'labelBox'}>
                                                    <i className="fas fa-check"></i>
                                                </div>}
                                            </div>
                                            <Input
                                                className="check-box-title d-none"
                                                type="checkbox"
                                                checked={active}
                                                onChange={changeActive}
                                            />
                                            Active
                                        </label>
                                    </div>
                                </ModalBody>
                                <ModalFooter>
                                    {/*<Button*/}
                                    {/*    type="button"*/}
                                    {/*    color="secondary"*/}
                                    {/*    outline*/}
                                    {/*    onClick={openEditModal}*/}
                                    {/*>*/}
                                    {/*    Cancel*/}
                                    {/*</Button>{" "}*/}
                                    <Button color="info" outline disabled={loading}>Save</Button>
                                </ModalFooter>
                            </AvForm>
                        </Modal>

                        {showDeleteModal && (
                            <DeleteModal
                                text={currentItem != null ? currentItem.name : ""}
                                showDeleteModal={showDeleteModal}
                                confirm={deleteFunction}
                                cancel={openDeleteModal}
                            />
                        )}

                    </div> :

                    <div className="main-service-page container">
                        <AddIcon
                            title={true}
                            onClick={() => openModal("")}
                        />
                        <Table className="custom-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>State</th>
                                <th>From Count</th>
                                <th>Till Count</th>
                                <th>Every Count</th>
                                <th>Price</th>
                            </tr>
                            </thead>
                            {pricingDashboard.length > 0 ? (
                                <tbody>
                                {pricingDashboard.map((item, i) => (
                                    <tr onClick={() => this.handleGetPricing(item.stateId, item.stateName)}
                                        key={item.id}>
                                        <td>{i + 1}</td>
                                        <td>
                                            {item.stateName}
                                        </td>
                                        <td> {item.minFromCount}</td>
                                        <td> {item.maxTillCount}</td>
                                        <td> {item.minEveryCount}</td>
                                        <td> {item.minPrice === item.maxPrice ? "$" + item.minPrice :
                                            "$" + item.minPrice + " - " + "$" + item.maxPrice
                                        }
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            ) : (
                                <tbody>
                                <tr>
                                    <td colSpan="4">
                                        <h3 className="text-center mx-auto">
                                            {" "}
                                            No information{" "}
                                        </h3>
                                    </td>
                                </tr>
                                </tbody>
                            )}
                        </Table>
                        <Modal
                            id="allModalStyle"
                            isOpen={showModal}
                            toggle={openModal}
                        >
                            <AvForm onValidSubmit={saveItem}>
                                <div className='position-absolute' style={{top: '45px', left: '-12%'}}>
                                    <CloseBtn click={openModal}/>
                                </div>
                                <ModalHeader
                                    toggle={openModal}
                                    charCode="x"
                                    className="model-head"
                                >
                                    {currentItem != null && currentItem.serviceId
                                        ? "Edit Pricing"
                                        : "Add Pricing"}
                                </ModalHeader>
                                <ModalBody>
                                    <AvField type="select"
                                             name="serviceId"
                                             onChange={(item) => this.handleChangeOnline(item.target.value)}
                                             value={currentItem != null ?
                                                 (currentItem.serviceDto != null ?
                                                     currentItem.serviceDto.mainServiceDto.name + " " +
                                                     "" + currentItem.serviceDto.subServiceDto.name :
                                                     currentItem.serviceId) : "0"}
                                             required>
                                        <option value="0" selected>Select Service</option>
                                        {dashboard.map(item =>
                                            <option key={item.serviceId}
                                                    value={item.serviceId}>{item.subServiceName} - {item.mainServiceName} ,
                                                ${item.minPrice === item.maxPrice ? item.minPrice :
                                                    item.minPrice + " - " + item.maxPrice
                                                }

                                            </option>
                                        )}
                                    </AvField>
                                    <AvField
                                        name="price"
                                        label="Price"
                                        type="number"
                                        defaultValue={
                                            currentItem != null
                                                ? currentItem.price
                                                : " "
                                        }
                                        placeholder="Enter price $"
                                    />

                                    <div className="flex-row-times margin-top-15">
                                        <AvField
                                            name="fromCount"
                                            label="From Count"
                                            type="number"
                                            defaultValue={
                                                currentItem != null
                                                    ? currentItem.fromCount
                                                    : " "
                                            }
                                            placeholder="Enter from count "
                                        />
                                        <AvField
                                            name="tillCount"
                                            label="Till Count"
                                            type="number"
                                            defaultValue={
                                                currentItem != null
                                                    ? currentItem.tillCount
                                                    : " "
                                            }
                                            placeholder="Enter till count "
                                        />
                                    </div>
                                    <AvField
                                        name="everyCount"
                                        label="Every Count"
                                        type="number"
                                        defaultValue={
                                            currentItem != null
                                                ? currentItem.everyCount
                                                : " "
                                        }
                                        placeholder="Enter every count "
                                    />

                                    <Col md={12}>
                                        <div className="flex-row space-around">
                                            {this.state.isOnline ? "" :
                                                <label className={'labelDuo'}>
                                                    <div className={'labelBlock'} style={{left: '-20%'}}>
                                                        {all && <div className={'labelBox'}>
                                                            <i className="fas fa-check"></i>
                                                        </div>}
                                                    </div>
                                                    <Input
                                                        className="check-box-title d-none"
                                                        type="checkbox"
                                                        checked={all}
                                                        onChange={changeAll}
                                                    />
                                                    Add all zip code
                                                </label>
                                            }
                                            <label className={'labelDuo'}>
                                                <div className={'labelBlock'}>
                                                    {active && <div className={'labelBox'}>
                                                        <i className="fas fa-check"></i>
                                                    </div>}
                                                </div>
                                                <Input
                                                    className="check-box-title d-none"
                                                    type="checkbox"
                                                    checked={active}
                                                    onChange={changeActive}
                                                />
                                                Active
                                            </label>
                                        </div>
                                    </Col>
                                    {this.state.isOnline ? "" :
                                        <Select
                                            isDisabled={all}
                                            defaultValue="Select State"
                                            isMulti
                                            name="statesId"
                                            options={optionsStateByPricing}
                                            onChange={getCounty}
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                        />}
                                </ModalBody>
                                <ModalFooter>
                                    {/*<Button*/}
                                    {/*    type="button"*/}
                                    {/*    color="secondary"*/}
                                    {/*    outline*/}
                                    {/*    onClick={openModal}*/}
                                    {/*>*/}
                                    {/*    Cancel*/}
                                    {/*</Button>{" "}*/}
                                    <Button color="info" outline disabled={loading}>Save</Button>
                                </ModalFooter>
                            </AvForm>
                        </Modal>

                        {showStatusModal && (
                            <StatusModal
                                text={currentItem != null ? currentItem.name : ""}
                                showStatusModal={showStatusModal}
                                confirm={changeStatusPricing}
                                cancel={openStatusModal}
                            />
                        )}
                    </div>
                }
            </AdminLayout>
        );
    }
}

export default connect(
    ({
         service: {
             zipCodeOptions, countyOptions, dashboard, servicePrices,
             selectZipCodes, selectCounties, selectStates,
         },
         app: {
             pricingList,
             showModal,
             showDeleteModal,
             currentItem,
             active,
             showStatusModal,
             all,
             stateOptions,
             zipCodes,
             pricingDashboard,
             services,
             loading,
             selectStateByPricing,
             selectCountyByPricing,
             selectZipCodeByPricing,
             optionsStateByPricing,
             optionsCountyByPricing,
             optionsZipCodeByPricing,
             pricingByStates, showEditModal
         },
     }) => ({
        pricingList,
        showModal,
        showDeleteModal,
        currentItem,
        active,
        showStatusModal,
        zipCodeOptions, countyOptions, dashboard, servicePrices,
        selectZipCodes, selectCounties, selectStates,
        all,
        stateOptions,
        zipCodes,
        pricingDashboard,
        services,
        loading,
        selectStateByPricing,
        selectCountyByPricing,
        selectZipCodeByPricing,
        optionsStateByPricing,
        optionsCountyByPricing,
        optionsZipCodeByPricing,
        pricingByStates, showEditModal
    })
)(Pricing);

// Icons
function EditIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{cursor: "pointer"}}
                width="18"
                height="18"
                viewBox="0 0 18 18"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M12.728 6.68599L11.314 5.27199L2 14.586V16H3.414L12.728 6.68599ZM14.142 5.27199L15.556 3.85799L14.142 2.44399L12.728 3.85799L14.142 5.27199ZM4.242 18H0V13.757L13.435 0.321992C13.6225 0.134521 13.8768 0.0292053 14.142 0.0292053C14.4072 0.0292053 14.6615 0.134521 14.849 0.321992L17.678 3.15099C17.8655 3.33852 17.9708 3.59283 17.9708 3.85799C17.9708 4.12316 17.8655 4.37746 17.678 4.56499L4.243 18H4.242Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}

function CloseIcon(props) {
    return <div className={props.className}>
        <svg onClick={props.onClick} style={{cursor: 'pointer'}} width="50" height="50" viewBox="0 0 50 50" fill="none"
             xmlns="http://www.w3.org/2000/svg">
            <circle cx="25" cy="25" r="25" fill="white"/>
            <path
                d="M24.955 23.1875L31.1425 17L32.91 18.7675L26.7225 24.955L32.91 31.1425L31.1425 32.91L24.955 26.7225L18.7675 32.91L17 31.1425L23.1875 24.955L17 18.7675L18.7675 17L24.955 23.1875Z"
                fill="#313E47"/>
        </svg>
    </div>
}

function DeleteIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{cursor: "pointer"}}
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M2 6H18V19C18 19.2652 17.8946 19.5196 17.7071 19.7071C17.5196 19.8946 17.2652 20 17 20H3C2.73478 20 2.48043 19.8946 2.29289 19.7071C2.10536 19.5196 2 19.2652 2 19V6ZM4 8V18H16V8H4ZM7 10H9V16H7V10ZM11 10H13V16H11V10ZM5 3V1C5 0.734784 5.10536 0.48043 5.29289 0.292893C5.48043 0.105357 5.73478 0 6 0H14C14.2652 0 14.5196 0.105357 14.7071 0.292893C14.8946 0.48043 15 0.734784 15 1V3H20V5H0V3H5ZM7 2V3H13V2H7Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}
