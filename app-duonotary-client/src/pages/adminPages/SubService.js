import React, { Component } from "react";
import PropTypes from "prop-types";
import AdminLayout from "../../components/AdminLayout";
import {
    deleteSubService, getMainServicePage,
    getSubServiceList, getSubServicePage,
    saveSubService,
} from "../../redux/actions/AppAction";
import * as type from "../../redux/actionTypes/AppActionTypes";
import {
    Button, Col,
    CustomInput,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader, Row,
    Table,
} from "reactstrap";
import { AvField, AvForm } from "availity-reactstrap-validation";
import DeleteModal from "../../components/Modal/DeleteModal";
import StatusModal from "../../components/Modal/StatusModal";
import DefaultInputModal from "../../components/Modal/DefaultInputModal";
import { connect } from "react-redux";
import Switch from "react-switch";
import OnlineModal from "../../components/Modal/OnlineModal";
import Pagination from "react-js-pagination";

class SubService extends Component {
    componentDidMount() {
        this.props.dispatch(getSubServicePage({page:0,size:10}));
    }

    constructor(props) {
        super(props);
        this.state = {
            inPerson: false,
            checked: false,
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(checked) {
        this.setState({ checked });
    }
    handlePageChange(pageNumber) {
        this.props.dispatch(getSubServicePage({page:pageNumber-1, size:10}))
        this.props.dispatch(
            {type:'updateState',
                payload:{
                    activePageSub: pageNumber-1
                }})
    }

    render() {
        const {
            dispatch,
            showModal,
            subServices,
            currentItem,
            active,
            defaultInput,
            showDeleteModal,
            showStatusModal,
            showDefaultInputModal,
            activePageSub,
            totalElementsSub,
            subServicePage,
            serviceEnum,
        } = this.props;

        const openModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showModal: !showModal,
                    currentItem: item,
                    active: item.active,
                    defaultInput: item.defaultInput,
                },
            });
        };
        const openDeleteModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentItem: item,
                },
            });
        };
        const changeActive = () => {
            dispatch({
                type: "updateState",
                payload: {
                    active: !active,
                },
            });
        };

        const changeDefaultInput = () => {
            dispatch({
                type: "updateState",
                payload: {
                    defaultInput: !defaultInput,
                },
            });
        };

        const deleteFunction = () => {
            this.props.dispatch(deleteSubService(currentItem));
        };

        const saveItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null;
            v.active = active;
            v.defaultInput = defaultInput;
            this.props.dispatch(saveSubService(v));
        };

        const changeStatusSubService = () => {
            let currentMainService = { ...currentItem };
            currentMainService.active = !currentItem.active;
            this.props.dispatch(saveSubService(currentMainService));
        };

        const changeDefaultInputSubService = () => {
            let currentSubService = { ...currentItem };
            currentSubService.defaultInput = !currentItem.defaultInput;
            this.props.dispatch(saveSubService(currentSubService));
        };

        const openStatusModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item,
                },
            });
        };
        const openDefaultInputModal = (item) => {
            dispatch({
                type: type.CHANGE_DEFAULT_INPUT,
                payload: {
                    showDefaultInputModal: !showDefaultInputModal,
                    currentItem: item,
                },
            });
        };

        return (
            <AdminLayout pathname={this.props.location.pathname}>
                <div className="main-service-page">
                    <div className="button-container">
                        <AddIcon
                            className="add-button"
                            onClick={() => openModal("")}
                        />
                        <span className="dark-title">Add New</span>
                    </div>
                    <Table className="custom-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>ServiceType</th>
                            <th>Default</th>
                            <th>Status</th>
                            <th colSpan="2">Operation</th>
                        </tr>
                        </thead>

                        {subServicePage.length > 0 ? (
                            <tbody>
                            {subServicePage.map((item, i) => (
                                <tr key={item.id}>
                                    <td>{i + 1}</td>
                                    <td>{item.name}</td>
                                    <td>{item.description}</td>
                                    <td>{item.serviceEnum}</td>
                                        <td>
                                            <FormGroup check>
                                                <Label check for={item.id}>
                                                    <Input
                                                        type="checkbox"
                                                        onClick={() =>
                                                            openDefaultInputModal(
                                                                item
                                                            )
                                                        }
                                                        id={item.id}
                                                        checked={
                                                            item.defaultInput
                                                        }
                                                    />
                                                    {"Default"}
                                                </Label>
                                            </FormGroup>
                                        </td>
                                    <td>
                                        <FormGroup check>
                                            <Label check for={"active" + i}>
                                                <Input
                                                    type="checkbox"
                                                    onClick={() =>
                                                        openStatusModal(
                                                            item
                                                        )
                                                    }
                                                    id={"active" + i}
                                                    checked={item.active}
                                                />
                                                {"Active"}
                                            </Label>
                                        </FormGroup>
                                    </td>
                                    <td>
                                        <EditIcon
                                            onClick={() => openModal(item)}
                                        />
                                    </td>
                                    <td>
                                        <DeleteIcon
                                            onClick={() =>
                                                openDeleteModal(item)
                                            }
                                        />
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        ) : (
                            <tbody>
                            <tr>
                                <td colSpan="4">
                                    <h3 className="text-center mx-auto">
                                        {" "}
                                        No information{" "}
                                    </h3>
                                </td>
                            </tr>
                            </tbody>
                        )}
                    </Table>
                    <Row>
                        <Col md={6} className="offset-6">
                            <Pagination
                                activePage={activePageSub+1}
                                itemsCountPerPage={10}
                                totalItemsCount={totalElementsSub}
                                pageRangeDisplayed={5}
                                onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                                linkClass="page-link"
                            />
                        </Col>
                    </Row>
                    <Modal
                        id="allModalStyle"
                        isOpen={showModal}
                        toggle={openModal}
                    >
                        <AvForm onValidSubmit={saveItem}>
                            <ModalHeader
                                toggle={openModal}
                                charCode="x"
                                className="model-head"
                            >
                                {currentItem != null && currentItem.name
                                    ? "Edit Sub Service"
                                    : "Add Sub Service"}
                            </ModalHeader>
                            <ModalBody style={{ backgroundColor: "#F9F9F9" }}>
                                <AvField
                                    className="modal-input"
                                    name="name"
                                    label="Name"
                                    required
                                    defaultValue={
                                        currentItem != null
                                            ? currentItem.name
                                            : ""
                                    }
                                    placeholder="Enter Sub Service name"
                                />
                                <AvField
                                    type="select"
                                    name="serviceEnum"
                                    required
                                    defaultValue={
                                        currentItem!=null
                                            ? currentItem.serviceEnum
                                            : "0"
                                    }

                                >
                                    <option value="0" disabled>
                                        Select service type
                                    </option>
                                    {serviceEnum.map((item) => (
                                        <option
                                            value={item}
                                        >
                                            {item}
                                        </option>
                                    ))}
                                </AvField>
                                <AvField
                                    className="modal-input"
                                    name="description"
                                    label="Description"
                                    required
                                    defaultValue={
                                        currentItem != null
                                            ? currentItem.description
                                            : ""
                                    }
                                    placeholder="Enter description"
                                />

                                <div className="flex-row space-around">
                                    <CustomInput
                                        className="check-box-title"
                                        type="checkbox"
                                        checked={active}
                                        onChange={changeActive}
                                        label= "Active"
                                        id="mainServiceActive"
                                    />
                                    <br />
                                    <CustomInput
                                        type="checkbox"
                                        checked={defaultInput}
                                        onChange={changeDefaultInput}
                                        label="Default"
                                        id="subServiceDefaultInput"
                                    />
                                </div>
                            </ModalBody>
                            <ModalFooter>
                                <Button
                                    type="button"
                                    color="secondary"
                                    outline
                                    onClick={openModal}
                                >
                                    Cancel
                                </Button>{" "}
                                <Button color="success">Save</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>

                    {showDeleteModal && (
                        <DeleteModal
                            text={currentItem != null ? currentItem.name : ""}
                            showDeleteModal={showDeleteModal}
                            confirm={deleteFunction}
                            cancel={openDeleteModal}
                        />
                    )}

                    {showStatusModal && (
                        <StatusModal
                            text={currentItem != null ? currentItem.name : ""}
                            showStatusModal={showStatusModal}
                            confirm={changeStatusSubService}
                            cancel={openStatusModal}
                        />
                    )}

                    {showDefaultInputModal && (
                        <DefaultInputModal
                            text={currentItem != null ? currentItem.name : ""}
                            showDefaultInputModal={showDefaultInputModal}
                            confirm={changeDefaultInputSubService}
                            cancel={openDefaultInputModal}
                        />
                    )}
                </div>
            </AdminLayout>
        );
    }
}

SubService.propTypes = {};
export default connect(
    ({
         app: {
             activePageSub,
             totalElementsSub,
             subServicePage,
             subServices,
             showModal,
             currentItem,
             active,
             showDeleteModal,
             showStatusModal,
             defaultInput,
             showDefaultInputModal,
             serviceEnum
         },
     }) => ({
        activePageSub,
        totalElementsSub,
        subServicePage,
        subServices,
        showModal,
        currentItem,
        active,
        showDeleteModal,
        showStatusModal,
        defaultInput,
        showDefaultInputModal,
        serviceEnum
    })
)(SubService);

// Icons
function EditIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{ cursor: "pointer" }}
                width="18"
                height="18"
                viewBox="0 0 18 18"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M12.728 6.68599L11.314 5.27199L2 14.586V16H3.414L12.728 6.68599ZM14.142 5.27199L15.556 3.85799L14.142 2.44399L12.728 3.85799L14.142 5.27199ZM4.242 18H0V13.757L13.435 0.321992C13.6225 0.134521 13.8768 0.0292053 14.142 0.0292053C14.4072 0.0292053 14.6615 0.134521 14.849 0.321992L17.678 3.15099C17.8655 3.33852 17.9708 3.59283 17.9708 3.85799C17.9708 4.12316 17.8655 4.37746 17.678 4.56499L4.243 18H4.242Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}

function AddIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                width="50"
                height="50"
                viewBox="0 0 50 50"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <rect width="50" height="50" rx="25" fill="#00B238" />
                <path
                    d="M24 24V18H26V24H32V26H26V32H24V26H18V24H24Z"
                    fill="white"
                />
            </svg>
        </div>
    );
}

function DeleteIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{ cursor: "pointer" }}
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M2 6H18V19C18 19.2652 17.8946 19.5196 17.7071 19.7071C17.5196 19.8946 17.2652 20 17 20H3C2.73478 20 2.48043 19.8946 2.29289 19.7071C2.10536 19.5196 2 19.2652 2 19V6ZM4 8V18H16V8H4ZM7 10H9V16H7V10ZM11 10H13V16H11V10ZM5 3V1C5 0.734784 5.10536 0.48043 5.29289 0.292893C5.48043 0.105357 5.73478 0 6 0H14C14.2652 0 14.5196 0.105357 14.7071 0.292893C14.8946 0.48043 15 0.734784 15 1V3H20V5H0V3H5ZM7 2V3H13V2H7Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}
