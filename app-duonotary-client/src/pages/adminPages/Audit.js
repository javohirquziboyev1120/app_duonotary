import React, {Component} from 'react';
import AdminLayout from "../../components/AdminLayout";
import {
    getAudit, getAuditForItemWithCommitId, getAuditItemOneTable, getAuditTables, getMainServiceList,
    getTotalElementsCount, getUserForAudit,
} from "../../redux/actions/AppAction";
import {
    Row,
    Col,
    Button,
    Dropdown, DropdownItem, DropdownMenu, DropdownToggle,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table
} from "reactstrap";
import {connect} from "react-redux";
import Moment from "moment";
import {isObject} from "reactstrap/es/utils";
import * as types from "../../redux/actionTypes/AppActionTypes";
import * as types2 from "../../redux/actionTypes/AdminActionTypes";
import PaginationComponent from "react-reactstrap-pagination";
import {Link} from "react-router-dom";
import { getCurrentUser, getUserTotalCount} from "../../redux/actions/AdminAction";
import * as adminActions from "../../redux/actions/AdminAction";
import CloseBtn from "../../components/CloseBtn";


class Audit extends Component {
    componentDidMount() {
        this.props.dispatch(getCurrentUser());
        this.props.dispatch(getMainServiceList());
        this.props.dispatch(getAudit({page: 0, size: 20}))
        this.props.dispatch(getAuditTables())
        this.props.dispatch(getTotalElementsCount())
        // this.props.dispatch({type: types2.CHANGE_TABLE_BOOLEAN})
    }

    handlePageChange(pageNumber) {
        const {dispatch, isAdminUser, isTable, isClient, isAgent} = this.props
        if (isAdminUser) {
            dispatch(adminActions.getAdminList({page: pageNumber - 1, size: 20}))
        }
        if (isTable) {
            dispatch(getAudit({page: 0, size: 20}))
        }
        if (isAgent) {
            dispatch(adminActions.getAgentList({page: pageNumber - 1, size: 20}))
        }
        if (isClient) {
            dispatch(adminActions.getClientList({page: pageNumber - 1, size: 20}))
        }


    }

    render() {
        const {
            mainServices, clients, agents, totalItems,
            admins, isSuperAdmin, isAdminUser, isTable, isClient, isAgent,
            dispatch, histories, showDropdown, currentAuditItemUser, auditTables, itemType,
            showModal, currentItem, page, size, totalElements,
            currentAuditItems,
        } = this.props;
        const getAuditItemForTable = (item) => {
            dispatch(getAuditItemOneTable({tableName: item, page: 0, size: 20}));
            dispatch(getTotalElementsCount({tableName: item}))
            dispatch({
                type: "updateState",
                payload: {
                    itemType: item,
                    page: 0,
                    size: 20
                }
            })
        }
        const changeTabPanel = (tabePane) => {
            if (tabePane === 'table') {
                dispatch(getTotalElementsCount())
                dispatch(getAudit({page: 0, size: 20}))
                dispatch({type: types2.CHANGE_TABLE_BOOLEAN})
                dispatch(getAuditTables())
            }
            if (tabePane === 'admin') {
                dispatch({type: types2.CHANGE_ADMIN_BOOLEAN})
                dispatch(adminActions.getAdminList({page: 0, size: 20}))
                dispatch(getUserTotalCount({roleName: "ROLE_ADMIN"}))
            }
            if (tabePane === 'client') {
                dispatch({type: types2.CHANGE_CLIENT_BOOLEAN})
                dispatch(getUserTotalCount({roleName: "ROLE_USER"}))
                dispatch(adminActions.getClientList({page: 0, size: 20}))
            }
            if (tabePane === 'agent') {
                dispatch(adminActions.getAgentList({page: 0, size: 20}))
                dispatch({type: types2.CHANGE_AGENT_BOOLEAN})
                dispatch(getUserTotalCount({roleName: "ROLE_AGENT"}))
            }
        };
        const openDropdown = () => {
            dispatch({
                type: types.CHANGE_SHOW_DROPDOWN,
                payload: {
                    showDropdown: !showDropdown
                }
            })
        }
        const getAuditAll = () => {
            dispatch({
                type: "updateState",
                payload: {
                    itemType: null
                }
            })
            this.props.dispatch(getTotalElementsCount())
            dispatch(getAudit({page: 0, size: 20}))
        }
        const openModal = (item) => {
            if (showModal) {
                dispatch({
                    type: 'updateState',
                    payload: {
                        currentItem: null,
                        currentAuditItems: null,
                        showModal: false
                    }
                })
            } else {
                let data = {}

                if (item && item.commitMetadata && item.commitMetadata.author && item.globalId && item.globalId.entity && item.commitMetadata.id) {
                    data.tableName = item.globalId.entity.substring(item.globalId.entity.lastIndexOf('.') + 1);
                    data.tableItemId = item.globalId.cdoId
                    data.commitId = item.commitMetadata.id
                    dispatch(getUserForAudit({id: item.commitMetadata.author}));
                }
                if (data && data.tableName && data.tableItemId) {
                    dispatch(getAuditForItemWithCommitId({...data}));
                }
                dispatch({
                    type: 'updateState',
                    payload: {
                        currentItem: item
                    }
                })

            }
        };
        const handleSelected = (selectedPage) => {
            if (isAdminUser) {
                dispatch(adminActions.getAdminList({page: selectedPage - 1, size: 20}))
            }
            if (isTable) {
                if (itemType != null && itemType.length > 0) {
                    dispatch(getAuditItemOneTable({tableName: itemType, page: selectedPage - 1, size: 20}));
                } else {
                    dispatch(getAudit({page: selectedPage - 1, size: 20}))
                }
            }
            if (isAgent) {
                dispatch(adminActions.getAgentList({page: selectedPage - 1, size: 20}))
            }
            if (isClient) {
                dispatch(adminActions.getClientList({page: selectedPage - 1, size: 20}))
            }
            dispatch(
                {
                    type: "updateState",
                    payload: {
                        page: selectedPage - 1
                    }
                }
            )
        }
        return (
            <AdminLayout pathname={this.props.location.pathname}>
                {isSuperAdmin ?
                    <div className="main-service-page container">
                        <h2 className="text-center">Operation history</h2>
                        <Row className="feedback-admin-page">
                            <Col md={3} className="">
                                <div>
                                    <h3>{isTable ? "Table" : isAdminUser ? "Admin" : isAgent ? "Agent"
                                        : isClient ? "Client" : ''}</h3>
                                </div>
                            </Col>
                            <Col md={12} className="">
                                <div className="route-services ml-auto">
                                    <div onClick={() => changeTabPanel('table')} className={!isTable ?
                                        "online" : "router-active"} style={{width : '21.5%'}}>Table
                                    </div>
                                    <div onClick={() => changeTabPanel('admin')} className={!isAdminUser ?
                                        "in-person" : "router-active"} style={{width : '21.5%'}}>Admin
                                    </div>
                                    <div onClick={() => changeTabPanel('agent')} className={!isAgent ?
                                        "in-person" : "router-active"} style={{width : '21.5%'}}>Agent
                                    </div>
                                    <div onClick={() => changeTabPanel('client')} className={!isClient ?
                                        "in-person" : "router-active"} style={{width : '21.5%'}}>Client
                                    </div>
                                </div>
                            </Col>
                        </Row>


                        {isAdminUser ?
                            <div className="per-admin-page">
                                {admins != null && (
                                    <div className="flex-column margin-top-25">
                                        {admins.map((item, i) => (
                                            <div
                                                key={i}
                                                className="flex-row single-admin margin-top-15"
                                            >
                                                <div className="avatar-container mr-4">
                                                    <img
                                                        src="/assets/img/avatar.png"
                                                        alt="avatar img"
                                                        className="avatar-img"
                                                    />
                                                    <span
                                                        className={item.online ? "avatar-status-online" : "avatar-status-offline"}></span>
                                                </div>
                                                <div className="flex-column float-left name-div">
                                                    <span className="label-text">Admin</span>
                                                    <span className="main-text-17 text-break">
                                                    {item.firstName + " " + item.lastName}
                                                </span>
                                                </div>
                                                <div className="flex-column float-left name-div pl-3">
                                                    <span className="label-text">E-mail:</span>
                                                    <span className="main-text-17 text-break">
                                                    {item.email}
                                                </span>
                                                </div>
                                                <div className="flex-column float-left name-div pl-3">
                                                    <span className="label-text">Phone:</span>
                                                    <span className="main-text-17 text-break">
                                                    {item.phoneNumber}
                                                </span>
                                                </div>
                                                <div className="ml-auto service-type-div">
                                                    <Link to={"/admin/auditByAuthor/" + item.id}>
                                                        <img className="right-icons" src="/assets/icons/right-arrov.png"
                                                             alt=""/>
                                                    </Link>
                                                </div>
                                            </div>
                                        ))}
                                        {/*    Map end*/}
                                    </div>
                                )}
                            </div>
                            : isAgent ?
                                <div className="per-admin-page">
                                    {agents != null && (
                                        <div className="flex-column margin-top-25">
                                            {agents.map((item, i) => (
                                                <div
                                                    key={i}
                                                    className="flex-row single-admin margin-top-15"
                                                >
                                                    <div className="avatar-container mr-4">
                                                        <img
                                                            src="/assets/img/avatar.png"
                                                            alt="avatar img"
                                                            className="avatar-img"
                                                        />
                                                        <span
                                                            className={item.online ? "avatar-status-online" : "avatar-status-offline"}></span>
                                                    </div>
                                                    <div className="flex-column float-left name-div">
                                                        <span className="label-text">Agent</span>
                                                        <span className="main-text-17 text-break">
                                                    {item.firstName + " " + item.lastName}
                                                </span>
                                                    </div>
                                                    <div className="flex-column float-left name-div pl-3">
                                                        <span className="label-text">E-mail:</span>
                                                        <span className="main-text-17 text-break">
                                                    {item.email}
                                                </span>
                                                    </div>
                                                    <div className="flex-column float-left name-div pl-3">
                                                        <span className="label-text">Phone:</span>
                                                        <span className="main-text-17 text-break">
                                                    {item.phoneNumber}
                                                </span>
                                                    </div>
                                                    <div className="ml-auto service-type-div">
                                                        <Link to={"/admin/auditByAuthor/" + item.id}>
                                                            <img className="right-icons"
                                                                 src="/assets/icons/right-arrov.png"
                                                                 alt=""/>
                                                        </Link>
                                                    </div>
                                                </div>
                                            ))}
                                            {/*    Map end*/}
                                        </div>
                                    )}
                                </div>
                                : isClient ?
                                    <div className="per-admin-page">
                                        {clients != null && (
                                            <div className="flex-column margin-top-25">
                                                {clients.map((item, i) => (
                                                    <div
                                                        key={i}
                                                        className="flex-row single-admin margin-top-15"
                                                    >
                                                        <div className="avatar-container mr-4">
                                                            <img
                                                                src="/assets/img/avatar.png"
                                                                alt="avatar img"
                                                                className="avatar-img"
                                                            />
                                                            <span
                                                                className={item.online ? "avatar-status-online" : "avatar-status-offline"}></span>
                                                        </div>
                                                        <div className="flex-column float-left name-div">
                                                            <span className="label-text">Client</span>
                                                            <span className="main-text-17 text-break">
                                                    {item.firstName + " " + item.lastName}
                                                </span>
                                                        </div>
                                                        <div
                                                            className="flex-column float-left name-div pl-3">
                                                            <span className="label-text">E-mail:</span>
                                                            <span className="main-text-17 size text-break">
                                                    {item.email}
                                                </span>
                                                        </div>
                                                        <div className="flex-column float-left name-div pl-3">
                                                            <span className="label-text">Phone:</span>
                                                            <span className="main-text-17 text-break">
                                                    {item.phoneNumber}
                                                </span>
                                                        </div>
                                                        <div className="ml-auto service-type-div">
                                                            <Link to={"/admin/auditByAuthor/" + item.id}>
                                                                <img className="right-icons"
                                                                     src="/assets/icons/right-arrov.png"
                                                                     alt=""/>
                                                            </Link>
                                                        </div>
                                                    </div>
                                                ))}
                                                {/*    Map end*/}
                                            </div>
                                        )}
                                    </div>
                                    : isTable ?
                                        <div>
                                            <div className="button-container">
                                                <Dropdown isOpen={showDropdown} toggle={openDropdown}>
                                                    <DropdownToggle caret>
                                                        Please select table name
                                                    </DropdownToggle>
                                                    <DropdownMenu modifiers={{
                                                        setMaxHeight: {
                                                            enabled: true,
                                                            order: 890,
                                                            fn: (data) => {
                                                                return {
                                                                    ...data,
                                                                    styles: {
                                                                        ...data.styles,
                                                                        overflow: 'auto',
                                                                        maxHeight: '150px',
                                                                    },
                                                                };
                                                            },
                                                        },
                                                    }}
                                                    >
                                                        <DropdownItem onClick={() => getAuditAll()}>All</DropdownItem>
                                                        {auditTables && auditTables.length > 0 ?
                                                            auditTables.map(item =>
                                                                <DropdownItem
                                                                    onClick={() => getAuditItemForTable(item)}>{item}</DropdownItem>
                                                            )
                                                            : ""}
                                                    </DropdownMenu>
                                                </Dropdown>
                                            </div>
                                            <Table className="custom-table fs-20">
                                                <thead>
                                                <tr className="fs-20">
                                                    {/* eslint-disable-next-line react/style-prop-object */}
                                                    <th className="fs-20">#</th>
                                                    <th className="fs-20">Table Name</th>
                                                    <th className="fs-20">Operation</th>
                                                    <th className="fs-20">Version</th>
                                                    <th className="fs-20">Time</th>
                                                    <th className="fs-20" colSpan="2">What did?</th>
                                                </tr>
                                                </thead>

                                                {histories && histories.length > 0 ? (
                                                    <tbody>
                                                    {histories.map((item, i) =>
                                                        <tr className="block" key={item.id}>
                                                            <td className="dark-title">{page > 0 ? ((page * size) + i + 1) : (i + 1)}</td>
                                                            <td className="dark-title">{item.globalId ? item.globalId.entity.substring(item.globalId.entity.lastIndexOf('.') + 1) : ""}</td>
                                                            <td className="dark-title">{item.type}</td>
                                                            <td className="dark-title">{item.version}</td>
                                                            <td className="dark-title">{Moment(item.commitMetadata.commitDate).format('hh:MM a, D MMM, YYYY')}</td>
                                                            <td className="dark-title"><Button
                                                                onClick={() => openModal(item)}>Show</Button>
                                                            </td>
                                                            <td className="dark-title">
                                                                <Link to={"/admin/audit/" + item.globalId.cdoId}>
                                                                    Read more
                                                                </Link>
                                                            </td>
                                                        </tr>
                                                    )}
                                                    </tbody>
                                                ) : (
                                                    <tbody>
                                                    <tr>
                                                        <td colSpan="4">
                                                            <h3 className="text-center mx-auto">
                                                                {" "}
                                                                No information{" "}
                                                            </h3>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                )}
                                            </Table>

                                        </div>
                                        : ""
                        }
                        <PaginationComponent
                            defaultActivePage={page + 1}
                            totalItems={isTable ? totalElements : totalItems} pageSize={20}
                            onSelect={handleSelected}
                        />
                        <Modal id="allModalStyle" size="lg" isOpen={showModal}>
                            <div className='position-absolute' style={{top : '45px', left : '-12%'}}>
                                <CloseBtn click={() => openModal(null)} />
                            </div>
                            <ModalHeader>What action</ModalHeader>
                            <ModalBody>
                                {currentAuditItems && currentItem ?
                                    <div>
                                        <p><b>Changes this table</b>
                                            - {currentItem.globalId ? currentItem.globalId.entity.substring(currentItem.globalId.entity.lastIndexOf('.') + 1) : ""}
                                        </p>
                                        <p><b>Change type</b> - {currentItem.type}</p>
                                        <p><b>Version -</b> {currentItem.version}</p>
                                        <p><b>Time</b> -
                                            {Moment(currentItem.commitMetadata.commitDate).format('hh:MM a, D MMM, YYYY')}
                                        </p>
                                        <p>
                                            <b>Changed
                                                author</b> - {currentAuditItemUser ? (currentAuditItemUser.firstName + " " + currentAuditItemUser.lastName) : ""}
                                        </p>
                                        <strong>Changes: </strong>
                                        {currentAuditItems.map((item, i) =>
                                            item.property === "updatedBy" || item.property === "createdBy" ? "" : item.property === "mainService" ?
                                                <div>
                                                    - <strong>"{" Main service "}"</strong> value changed
                                                    from
                                                    <b>
                                                        {" \""} {mainServices.map(m => item.left.cdoId === m.id && m.name)}{"\"  "}
                                                    </b>
                                                    to <b>{"  \""} {mainServices.map(m => item.right.cdoId === m.id && m.name)}{"\" "} </b>
                                                </div>
                                                :
                                                ("" + item.property) === "fromTime" || ("" + item.property) === "tillTime" ?
                                                    <div>
                                                        - <strong>'{item.property === "fromTime" ? "From time " : "Till time "}'</strong> value
                                                        changed from
                                                        <b>
                                                            {" " + Moment(Moment(new Date()).format("YYYY") + " " + item.left + "").format('hh:mm A')}
                                                        </b> to
                                                        <b>
                                                            {" " + Moment(Moment(new Date()).format("YYYY") + " " + item.right + "").format('hh:mm A')}
                                                        </b>
                                                    </div>
                                                    :
                                                    <div>
                                                        - <strong>'{item.property + " "}'</strong> value changed
                                                        from <b>{"" + item.left}</b> to <b>{"" + item.right}</b>
                                                    </div>
                                        )}
                                    </div>
                                    : <p>
                                        <b>Changed
                                            author</b> - {currentAuditItemUser ? (currentAuditItemUser.firstName + " " + currentAuditItemUser.lastName) : ""}
                                    </p>}
                                <h5 className="mt-3">Current item: </h5>
                                <Table>
                                    <tbody>
                                    {currentItem ? Object.entries(currentItem).map(([key, value]) =>
                                        key.toString() === "commitMetadata" || key.toString() === "globalId" || key.toString() === "changedProperties" ? "" :
                                            <tr key={key}>
                                                <th>{key}:</th>
                                                <td className="text-right">
                                                    <Table>
                                                        <tbody>
                                                        {isObject(value) ? Object.entries(value).map(([key, value]) =>
                                                            value ?
                                                                key.toString() === "id" || key.toString() === "createdBy" || key.toString() === "updatedBy" ? "" :
                                                                    key.toString() === "mainService" ?
                                                                        <tr key={key}>
                                                                            <th className="text-left">
                                                                                <b>{" Main service "}:</b></th>
                                                                            <th className="text-left">
                                                                                <b>
                                                                                    {mainServices.map(m => value.cdoId === m.id && m.name)}
                                                                                </b>
                                                                            </th>
                                                                        </tr>
                                                                        :
                                                                        <tr key={key}
                                                                            className={isObject(value) ? "d-none" : ""}>

                                                                            <th className="text-left">
                                                                                <b>{key.toString() === "fromTime" ? "From time" : key.toString() === "tillTime" ? "Till time" : "" + key}:</b>
                                                                            </th>
                                                                            <th className="text-left">
                                                                                <b>{isObject(value) ? "" :
                                                                                    ("" + key) === "createdAt" || ("" + key) === "updatedAt" ?
                                                                                        Moment(value).format('hh:MM a, D MMM, YYYY') :
                                                                                        ("" + key) === "fromTime" || ("" + key) === "tillTime" ?
                                                                                            (Moment(Moment(new Date()).format("YYYY") + " " + value + "").format('hh:mm A')) :
                                                                                            "" + value}</b>
                                                                            </th>
                                                                        </tr>
                                                                : ""
                                                        ) : value}
                                                        </tbody>
                                                    </Table>
                                                </td>

                                            </tr>
                                    ) : ''}
                                    </tbody>
                                </Table>
                            </ModalBody>
                            <ModalFooter>
                                {/*<Button color="secondary" onClick={() => openModal(null)}>Cancel</Button>*/}
                            </ModalFooter>
                        </Modal>
                    </div>
                    : <h1 className="text-center">404 - Page not found</h1>}
            </AdminLayout>
        );
    }
}

export default connect(
    ({
         auth: {
             isSuperAdmin,
         },
         admin: {
             clients, agents,
             totalItems,
             isAdminUser, isTable, isClient, isAgent,
             currentUser,
             admins
         },
         app: {
             mainServices,
             itemType,
             currentAuditItemUser,
             historyTables,
             auditTables,
             histories,
             text,
             showModal,
             currentItem,
             showDropdown,
             dispatch,
             totalElements,
             currentAuditItems,
             size,
             page
         },
     }) => ({
        clients, agents,
        totalItems,
        isAdminUser, isTable, isClient, isAgent,
        isSuperAdmin,
        mainServices,
        currentUser,
        admins,
        itemType,
        currentAuditItemUser,
        historyTables,
        auditTables,
        histories,
        text,
        size,
        page,
        showModal,
        currentItem,
        totalElements,
        showDropdown,
        dispatch,
        currentAuditItems
    })
)(Audit);



