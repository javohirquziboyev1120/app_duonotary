import React, {Component} from 'react';
import AdminLayout from "../../components/AdminLayout";
import FirstBlock from "../../components/Calendar/FirstBlock";
import "../../components/Calendar/Calendar.scss";
import ZipCodeFilter from "../../components/Calendar/ZipCodeFilter";
import Orders from "../../components/Calendar/Orders";
import ServiceIndicator from "../../components/Calendar/ServiceIndicator";
import randomColor from "randomcolor";
import Dashboard from "../../components/Calendar/Dashboard";
import {
    getDashboard,
    getOrdersCash, getSubServiceList,
    getTotalNumbersForDashboard,
    getCountyByStateForCalendar,
    getZipCodeByCountyForCalendar,
} from "../../redux/actions/AppAction";
import {connect} from "react-redux";
import {getStateList} from "../../redux/actions/AdminAction";
import Toolbar from "../../components/Calendar/Toolbar";

class Calendar extends Component {

    matchColors = {}

    componentDidMount() {
        this.props.dispatch(getTotalNumbersForDashboard())
        this.props.dispatch(getOrdersCash())
        this.props.dispatch(getSubServiceList()).then(() => {
            this.props.subServices.map((item, index) => {
                this.matchColors[item.id] = randomColor();
            })
        })
        this.props.dispatch(getStateList())
        this.props.dispatch(getDashboard({date: this.state.today.toISOString()}))
    }

    constructor(props) {
        super(props);
        this.state = {
            today: new Date(),
            toolbarTab: 0
        }
    }

    toggleTab = (i) => {
        this.setState({
            toolbarTab: i
        })
    }


    getCountyByState = (stateId) => {
        if (stateId) {
            this.props.dispatch(getCountyByStateForCalendar(stateId))
        }
    }

    getZipCodeByCounty = (countyId) => {
        if (countyId) {
            this.props.dispatch(getZipCodeByCountyForCalendar(countyId))
        }
    }

    filter = (stateId, countyId, zipCodeId) => {
        this.props.dispatch(getDashboard({date: this.state.today.toISOString(), stateId, countyId, zipCodeId}))
    }

    nextWeek = () => {
        if (this.state.today.toDateString() === new Date().toDateString()) {

        } else {
            this.setState({
                today: new Date(this.state.today.setDate(this.state.today.getDate() + 7))
            }, () => {
                console.log("Request sent");
                this.props.dispatch(getDashboard({date: this.state.today.toISOString()}))
                console.log(this.props.ordersForDashboard);
            })
        }
    }
    previousWeek = () => {
        this.setState({
            today: new Date(this.state.today.setDate(this.state.today.getDate() - 7))
        }, () => {
            this.props.dispatch(getDashboard({date: this.state.today.toISOString()}))

        })
    }
    currentDay = () => {
        this.setState({
            today: new Date()
        }, () => {
            this.props.dispatch(getDashboard({date: this.state.today.toISOString()}))
        })
    }


    render() {

        const {dispatch, usersAndOrdersCount, ordersCash, ordersForDashboard, subServices, states, zipCodeOptions, countyOptions} = this.props;
        console.log(ordersForDashboard);

        return (
            <AdminLayout pathname={this.props.location.pathname}>
                <div className="calendar-page m-3">
                    {
                        <div className="d-flex justify-content-between">
                            <div className="w-25 mr-3"><FirstBlock color="#FFC206" name={"Registered Users"}
                                                                   total={usersAndOrdersCount[0] && usersAndOrdersCount[0].all}
                                                                   today={usersAndOrdersCount[0] && usersAndOrdersCount[0].today}/>
                            </div>
                            <div className="w-25 mr-3"><FirstBlock color="#00B533" name={"Online orders"}
                                                                   total={usersAndOrdersCount[1] && usersAndOrdersCount[1].all}
                                                                   today={usersAndOrdersCount[1] && usersAndOrdersCount[1].today}/>
                            </div>
                            <div className="w-25 mr-3"><FirstBlock color="#00B533" name={"In-person orders"}
                                                                   total={usersAndOrdersCount[2] && usersAndOrdersCount[2].all}
                                                                   today={usersAndOrdersCount[2] && usersAndOrdersCount[2].today}/>
                            </div>
                            <div className="w-25"><FirstBlock color="#04A6FB" name={"Mobile app orders"}
                                                              total={usersAndOrdersCount[3] && usersAndOrdersCount[3].all}
                                                              today={usersAndOrdersCount[3] && usersAndOrdersCount[3].today}/>
                            </div>
                        </div>
                    }

                    <div className="d-flex mt-3">
                        <div className="w-100 mr-3">
                            <ZipCodeFilter stateOptions={states} countyOptions={countyOptions ? countyOptions : ""}
                                           zipCodeOptions={zipCodeOptions ? zipCodeOptions : ""}
                                           getCounty={this.getCountyByState}
                                           getZipCode={this.getZipCodeByCounty} filter={this.filter}/>
                        </div>
                        <div className="w-100 mr-3">
                            <Orders today={ordersCash.today ? ordersCash.today : "0.00"}
                                    week={ordersCash.weekly ? ordersCash.weekly : "0.00"}
                                    month={ordersCash.monthly ? ordersCash.monthly : "0.00"}/>
                        </div>
                        <div className="w-100">
                            <ServiceIndicator services={subServices} colors={this.matchColors}/>
                        </div>
                    </div>
                    <div className="mt-3 block p-0">
                        <Toolbar nextWeek={this.nextWeek} previousWeek={this.previousWeek} currentDay={this.currentDay}
                                 toggleTab={this.toggleTab}
                                 today={this.state.today}/>
                        <Dashboard colors={this.matchColors} today={this.state.today} services={ordersForDashboard}
                                   toolbarTab={this.state.toolbarTab}/>
                    </div>
                </div>
            </AdminLayout>

        );
    }
}


export default connect(
    ({
         admin: {
             usersAndOrdersCount, ordersCash, ordersForDashboard, states
         },
         app: {
             subServices,
         },
         additionalServicePrice: {
             countyOptions, zipCodeOptions
         }
     }) => ({
        usersAndOrdersCount, ordersCash, ordersForDashboard, states, countyOptions, zipCodeOptions, subServices
    })
)(Calendar);