import React, { Component } from "react";
import PropTypes from "prop-types";
import AdminLayout from "../../components/AdminLayout";
import {Col, Row} from "reactstrap";
import TimePicker from "react-time-picker";
class ZipCodes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            aboutZipCode: true,
            workHours: {
                online: {
                    enabled: false,
                    percentage: 30,
                    from: "7:00",
                    to: "19:00",
                },
                inPerson: {
                    enabled: false,
                    percentage: 30,
                    from: "7:00",
                    to: "19:00",
                },
                online2: false,
                inPerson2: false,
            },
        };
        this.handleWorkHours = this.handleWorkHours.bind(this);

    }
    onTimeChange = (time) => {
        // handle working time
    };
    handleWorkHours(type) {
        if (type === "online") {
            this.setState((prev) => {
                prev.workHours.online.enabled = !prev.workHours.online.enabled;
                return prev;
            });
        } else if (type === "inPerson") {
            this.setState((prev) => {
                prev.workHours.inPerson.enabled = !prev.workHours.inPerson
                    .enabled;
                return prev;
            });
        } else if (type === "online2") {
            this.setState((prev) => {
                prev.workHours.online2 = !prev.workHours.online2;
                return prev;
            });
        } else if (type === "inPerson2") {
            this.setState((prev) => {
                prev.workHours.inPerson2 = !prev.workHours.inPerson2;
                return prev;
            });
        }
    }

    ShowZipCode = () => {
        this.setState({
            aboutZipCode: !this.state.aboutZipCode
        });
    };
    render() {
        return (
            <AdminLayout pathname={this.props.location.pathname}>
                {this.state.aboutZipCode ?
                    <div className="admin-zip-codes-page flex-column container">
                        <div className="state-section flex-column">

                            <span className="main-title">Washington</span>
                            <div className="flex-row">
                                <div className="single-zip-code-section flex-row space-between">
                                    <span className="zip-code-text">50805</span>
                                    <ThreeDotIcon onClick={this.ShowZipCode}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    :

                    <div className="admin-zip-codes-page-open flex-column container">
                        <div className="zipcode-agents-about">
                            <span>Alaska - </span>
                            <span> 50805</span>
                            <span className="icon icon-delete"/>
                        </div>

                        <div className="flex-column margin-top-25">
                            <span className="main-title">Work hours</span>
                            <div className="flex-row margin-top-15">
                                <div className="half-left inner-section flex-row  space-between">
                                    <span className="dark-title">Online</span>
                                    <div className="flex-row">
                                        <div>
                                            <TimePicker
                                                className="time-text"
                                                clearIcon={null}
                                                clockIcon={null}
                                                onChange={this.onTimeChange}
                                                value={"11:00"}
                                                disableClock={true}
                                            />
                                        </div>
                                        <span className="time-text"> - </span>
                                        <div>
                                            <TimePicker
                                                className="time-text"
                                                clearIcon={null}
                                                disableClock={true}
                                                clockIcon={null}
                                                value={"7:00"}
                                            />
                                        </div>
                                        <CalendarIcon/>
                                    </div>
                                </div>
                                <div className="half-right inner-section flex-row  space-between">
                                    <span className="dark-title">Online</span>
                                    <div className="flex-row">
                                        <div>
                                            <TimePicker
                                                className="time-text"
                                                clearIcon={null}
                                                clockIcon={null}
                                                onChange={this.onTimeChange}
                                                value={"11:00"}
                                                disableClock={true}
                                            />
                                        </div>
                                        <span className="time-text"> - </span>
                                        <div>
                                            <TimePicker
                                                className="time-text"
                                                clearIcon={null}
                                                disableClock={true}
                                                clockIcon={null}
                                                value={"7:00"}
                                            />
                                        </div>
                                        <CalendarIcon/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="flex-column margin-top-25">
                            <span className="main-title">Discount</span>
                            <div className="flex-row margin-top-15">
                                <div className="half-left">
                                    <input
                                        type="text"
                                        className="discount-input"
                                        placeholder="Type here"
                                    />
                                </div>
                                <div className="half-left"/>
                            </div>
                        </div>
                        <div className="flex-column margin-top-25">
                            <div className="flex-row">
                                <span className="main-title mr-3">Agents</span>
                                <AgentAddIcon/>
                            </div>
                        </div>
                        <Row className="sharing-history-row">
                            <Col md={12} className="history-col">
                                <Row>
                                    <Col md={3}>
                                        <div className="d-flex">
                                            <div className="avatar-img">
                                                <img src="/assets/img/avatar.png" alt=""/>
                                            </div>
                                            <div className="ml-3">
                                                <div className="fs-15">Clients</div>
                                                <div className="fs-20-bold">
                                                    Rajabov Doston
                                                </div>
                                            </div>
                                        </div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="border-line"/>
                                    </Col>
                                    <Col md={3}>
                                        <div className="fs-15">Location (2 min ago)</div>
                                        <div className="fs-15-bold pt-2">
                                            3517 W. Gray St., Pennsylvania 57867
                                        </div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="border-line"/>
                                    </Col>
                                    <Col md={1}>
                                        <div style={{fontSize: "12px"}}>Online time</div>
                                        <div className="fs-15-bold pt-2">
                                            2:30
                                        </div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="border-line"/>
                                    </Col>
                                    <Col md={1}>
                                        <div className="fs-15">Orders</div>
                                        <div className="fs-15-bold pt-2">
                                            22
                                        </div>
                                    </Col>
                                    <Col md={1}>
                                        <img className="right-icons" src="/assets/icons/right-arrov.png" alt=""/>
                                    </Col>
                                </Row>
                            </Col>
                            <Col md={12} className="history-col">
                                <Row>
                                    <Col md={3}>
                                        <div className="d-flex">
                                            <div className="avatar-img">
                                                <img src="/assets/img/avatar.png" alt=""/>
                                            </div>
                                            <div className="ml-3">
                                                <div className="fs-15">Clients</div>
                                                <div className="fs-20-bold">
                                                    Rajabov Doston
                                                </div>
                                            </div>
                                        </div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="border-line"/>
                                    </Col>
                                    <Col md={3}>
                                        <div className="fs-15">Location (2 min ago)</div>
                                        <div className="fs-15-bold pt-2">
                                            3517 W. Gray St., Pennsylvania 57867
                                        </div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="border-line"/>
                                    </Col>
                                    <Col md={1}>
                                        <div style={{fontSize: "12px"}}>Online time</div>
                                        <div className="fs-15-bold pt-2">
                                            2:30
                                        </div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="border-line"/>
                                    </Col>
                                    <Col md={1}>
                                        <div className="fs-15">Orders</div>
                                        <div className="fs-15-bold pt-2">
                                            22
                                        </div>
                                    </Col>
                                    <Col md={1}>
                                        <img className="right-icons" src="/assets/icons/right-arrov.png" alt=""/>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </div>
                }
            </AdminLayout>
        );
    }
}
ZipCodes.propTypes = {};

export default ZipCodes;

function ThreeDotIcon(props) {
    return (
        <div onClick={props.onClick}>
            <svg
                width="18"
                height="4"
                viewBox="0 0 18 4"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M1.5 0.5C0.675 0.5 0 1.175 0 2C0 2.825 0.675 3.5 1.5 3.5C2.325 3.5 3 2.825 3 2C3 1.175 2.325 0.5 1.5 0.5ZM16.5 0.5C15.675 0.5 15 1.175 15 2C15 2.825 15.675 3.5 16.5 3.5C17.325 3.5 18 2.825 18 2C18 1.175 17.325 0.5 16.5 0.5ZM9 0.5C8.175 0.5 7.5 1.175 7.5 2C7.5 2.825 8.175 3.5 9 3.5C9.825 3.5 10.5 2.825 10.5 2C10.5 1.175 9.825 0.5 9 0.5Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}

function CalendarIcon(props) {
    return (
        <div onClick={props.onClick}>
            <svg
                className="calendar-icon"
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="#313E47"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M15 2H19C19.2652 2 19.5196 2.10536 19.7071 2.29289C19.8946 2.48043 20 2.73478 20 3V19C20 19.2652 19.8946 19.5196 19.7071 19.7071C19.5196 19.8946 19.2652 20 19 20H1C0.734784 20 0.48043 19.8946 0.292893 19.7071C0.105357 19.5196 0 19.2652 0 19V3C0 2.73478 0.105357 2.48043 0.292893 2.29289C0.48043 2.10536 0.734784 2 1 2H5V0H7V2H13V0H15V2ZM13 4H7V6H5V4H2V8H18V4H15V6H13V4ZM18 10H2V18H18V10Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}

function AgentAddIcon(props) {
    return (
        <div className={props.className} onClick={props.onClick}>
            <svg
                width="19"
                height="21"
                viewBox="0 0 19 21"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M10 13.252V15.342C9.09492 15.022 8.12628 14.9239 7.1754 15.0558C6.22453 15.1877 5.3192 15.5459 4.53543 16.1002C3.75166 16.6545 3.11234 17.3888 2.67116 18.2414C2.22998 19.094 1.99982 20.04 2 21L2.58457e-07 20.999C-0.000310114 19.7779 0.278921 18.5729 0.816299 17.4764C1.35368 16.3799 2.13494 15.4209 3.10022 14.673C4.0655 13.9251 5.18918 13.4081 6.38515 13.1616C7.58113 12.9152 8.81766 12.9457 10 13.251V13.252ZM8 12C4.685 12 2 9.315 2 6C2 2.685 4.685 0 8 0C11.315 0 14 2.685 14 6C14 9.315 11.315 12 8 12ZM8 10C10.21 10 12 8.21 12 6C12 3.79 10.21 2 8 2C5.79 2 4 3.79 4 6C4 8.21 5.79 10 8 10ZM14 16V13H16V16H19V18H16V21H14V18H11V16H14Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}
