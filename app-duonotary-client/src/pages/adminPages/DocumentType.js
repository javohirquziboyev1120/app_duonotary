import React, {Component} from "react";
import AdminLayout from "../../components/AdminLayout";
import {
    deleteDocumentType,
    getDocumentTypePage,
    saveDocumentType,
} from "../../redux/actions/AppAction";
import {
    Button, Col,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader, Row,
    Table,
} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import DeleteModal from "../../components/Modal/DeleteModal";
import StatusModal from "../../components/Modal/StatusModal";
import DefaultInputModal from "../../components/Modal/DefaultInputModal";
import {connect} from "react-redux";
import Pagination from "react-js-pagination";
import {AddIcon} from "../../components/Icons";
import CloseBtn from "../../components/CloseBtn";

class DocumentType extends Component {
    componentDidMount() {
        this.props.dispatch(getDocumentTypePage({page: 0, size: 10}));
    }

    constructor(props) {
        super(props);
        this.state = {
            inPerson: false,
            checked: false,
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(checked) {
        this.setState({checked});
    }

    handlePageChange(pageNumber) {
        this.props.dispatch(getDocumentTypePage({page: pageNumber - 1, size: 10}))
        this.props.dispatch(
            {
                type: 'updateState',
                payload: {
                    activePageDocType: pageNumber - 1
                }
            })
    }

    render() {
        const {
            dispatch,
            showModal,
            currentItem,
            active,
            showDeleteModal,
            showStatusModal,
            showDefaultInputModal,
            activePageDocType,
            totalElementsDocType,
            docTypePage,
            fbi,
        } = this.props;

        const openModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showModal: !showModal,
                    currentItem: item,
                    active: item.active,
                    fbi: item.fbi,
                },
            });
        };
        const openDeleteModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentItem: item,
                },
            });
        };
        const changeActive = () => {
            dispatch({
                type: "updateState",
                payload: {
                    active: !active,
                },
            });
        };

        const changeFbi = () => {
            dispatch({
                type: "updateState",
                payload: {
                    fbi: !fbi,
                },
            });
        };

        const deleteFunction = () => {
            this.props.dispatch(deleteDocumentType(currentItem));
        };

        const saveItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null;
            v.active = active;
            v.fbi = fbi;
            this.props.dispatch(saveDocumentType(v));
        };

        const changeStatusDocType = () => {
            let currentDocType = {...currentItem};
            currentDocType.active = !currentItem.active;
            this.props.dispatch(saveDocumentType(currentDocType));
        };

        const changeFbiDocType = () => {
            let currentDocType = {...currentItem};
            currentDocType.fbi = !currentItem.fbi;
            this.props.dispatch(saveDocumentType(currentDocType));
        };

        const openStatusModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item,
                },
            });
        };
        const openFbiModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showDefaultInputModal: !showDefaultInputModal,
                    currentItem: item,
                },
            });
        };

        return (
            <AdminLayout pathname={this.props.location.pathname}>
                <div className="main-service-page container">
                    <AddIcon
                        title={true}
                        onClick={() => openModal("")}
                    />
                    <Table className="custom-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>FBI</th>
                            <th>Status</th>
                            <th colSpan="2">Operation</th>
                        </tr>
                        </thead>

                        {docTypePage.length > 0 ? (
                            <tbody>
                            {docTypePage.map((item, i) => (
                                <tr key={item.id}>
                                    <td>{i + 1}</td>
                                    <td>{item.name}</td>
                                    <td>{item.description}</td>
                                    <td>
                                        <FormGroup check>
                                            <Label check for={item.id} className={'labelDuo'}>
                                                <div className={'labelBlock'} style={{left : '-100%'}}>
                                                    {item.fbi && <div className={'labelBox'}>
                                                        <i className="fas fa-check"/>
                                                    </div>}
                                                </div>
                                                <Input
                                                    className={'d-none'}
                                                    type="checkbox"
                                                    onClick={() =>
                                                        openFbiModal(
                                                            item
                                                        )
                                                    }
                                                    id={item.id}
                                                    checked={
                                                        item.fbi
                                                    }
                                                />
                                                {"FBI"}
                                            </Label>
                                        </FormGroup>
                                    </td>
                                    <td>
                                        <FormGroup check>
                                            <Label check for={"active" + i} className={'labelDuo'}>
                                                <div className={'labelBlock'}>
                                                    {item.active && <div className={'labelBox'}>
                                                        <i className="fas fa-check"></i>
                                                    </div>}
                                                </div>
                                                <Input
                                                    className={'d-none'}
                                                    type="checkbox"
                                                    onClick={() =>
                                                        openStatusModal(
                                                            item
                                                        )
                                                    }
                                                    id={"active" + i}
                                                    checked={item.active}
                                                />
                                                {"Active"}
                                            </Label>
                                        </FormGroup>
                                    </td>
                                    <td>
                                        <EditIcon
                                            onClick={() => openModal(item)}
                                        />
                                    </td>
                                    <td>
                                        <DeleteIcon
                                            onClick={() =>
                                                openDeleteModal(item)
                                            }
                                        />
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        ) : (
                            <tbody>
                            <tr>
                                <td colSpan="4">
                                    <h3 className="text-center mx-auto">
                                        {" "}
                                        No information{" "}
                                    </h3>
                                </td>
                            </tr>
                            </tbody>
                        )}
                    </Table>
                    <Row>
                        <Col md={12}>
                            <Pagination
                                activePage={activePageDocType + 1}
                                itemsCountPerPage={10}
                                totalItemsCount={totalElementsDocType}
                                pageRangeDisplayed={5}
                                onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                                linkClass="page-link"
                            />
                        </Col>
                    </Row>
                    <Modal
                        id="allModalStyle"
                        isOpen={showModal}
                        toggle={openModal}
                    >
                        <AvForm onValidSubmit={saveItem}>
                            <div className='position-absolute' style={{top : '45px', left : '-12%'}}>
                                <CloseBtn click={openModal} />
                            </div>
                            <ModalHeader
                                toggle={openModal}
                                charCode="x"
                                className="model-head"
                            >
                                {currentItem && currentItem.id != null
                                    ? "Edit document type"
                                    : "Add document type"}
                            </ModalHeader>
                            <ModalBody>
                                <AvField
                                    name="name"
                                    label="Name"
                                    required
                                    defaultValue={
                                        currentItem != null
                                            ? currentItem.name
                                            : ""
                                    }
                                    placeholder="Enter Sub Service name"
                                />
                                <AvField
                                    name="description"
                                    label="Description"
                                    required
                                    defaultValue={
                                        currentItem != null
                                            ? currentItem.description
                                            : ""
                                    }
                                    placeholder="Enter description"
                                />

                                <div className="flex-row space-around">
                                    <label className={'labelDuo'}>
                                        <div className={'labelBlock'}>
                                            {active && <div className={'labelBox'}>
                                                <i className="fas fa-check"></i>
                                            </div>}
                                        </div>
                                        <Input
                                            className="check-box-title d-none"
                                            type="checkbox"
                                            checked={active}
                                            onChange={changeActive}
                                        />
                                        Active
                                    </label>

                                    <label className={'labelDuo'}>
                                        <div className={'labelBlock'} style={{left : '-100%'}}>
                                            {fbi && <div className={'labelBox'}>
                                                <i className="fas fa-check"></i>
                                            </div>}
                                        </div>
                                        <Input
                                            className={'d-none'}
                                            type="checkbox"
                                            checked={fbi}
                                            onChange={changeFbi}
                                        />
                                        FBI
                                    </label>

                                </div>
                            </ModalBody>
                            <ModalFooter>
                                {/*<Button*/}
                                {/*    type="button"*/}
                                {/*    color="secondary"*/}
                                {/*    outline*/}
                                {/*    onClick={openModal}*/}
                                {/*>*/}
                                {/*    Cancel*/}
                                {/*</Button>{" "}*/}
                                <Button color="info" outline >Save</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>

                    {showDeleteModal && (
                        <DeleteModal
                            text={currentItem != null ? currentItem.name : ""}
                            showDeleteModal={showDeleteModal}
                            confirm={deleteFunction}
                            cancel={openDeleteModal}
                        />
                    )}

                    {showStatusModal && (
                        <StatusModal
                            text={currentItem != null ? currentItem.name : ""}
                            showStatusModal={showStatusModal}
                            confirm={changeStatusDocType}
                            cancel={openStatusModal}
                        />
                    )}

                    {showDefaultInputModal && (
                        <DefaultInputModal
                            text={currentItem != null ? currentItem.name : ""}
                            showDefaultInputModal={showDefaultInputModal}
                            confirm={changeFbiDocType()}
                            cancel={openFbiModal}
                        />
                    )}
                </div>
            </AdminLayout>
        );
    }
}

export default connect(
    ({
         app: {
             activePageDocType,
             totalElementsDocType,
             docTypePage,
             fbi,
             showModal,
             currentItem,
             active,
             showDeleteModal,
             showStatusModal,
             showDefaultInputModal
         },
     }) => ({
        activePageDocType,
        totalElementsDocType,
        docTypePage,
        fbi,
        showModal,
        currentItem,
        active,
        showDeleteModal,
        showStatusModal,
        showDefaultInputModal
    })
)(DocumentType);

// Icons
function EditIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{cursor: "pointer"}}
                width="18"
                height="18"
                viewBox="0 0 18 18"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M12.728 6.68599L11.314 5.27199L2 14.586V16H3.414L12.728 6.68599ZM14.142 5.27199L15.556 3.85799L14.142 2.44399L12.728 3.85799L14.142 5.27199ZM4.242 18H0V13.757L13.435 0.321992C13.6225 0.134521 13.8768 0.0292053 14.142 0.0292053C14.4072 0.0292053 14.6615 0.134521 14.849 0.321992L17.678 3.15099C17.8655 3.33852 17.9708 3.59283 17.9708 3.85799C17.9708 4.12316 17.8655 4.37746 17.678 4.56499L4.243 18H4.242Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}

function DeleteIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{cursor: "pointer"}}
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M2 6H18V19C18 19.2652 17.8946 19.5196 17.7071 19.7071C17.5196 19.8946 17.2652 20 17 20H3C2.73478 20 2.48043 19.8946 2.29289 19.7071C2.10536 19.5196 2 19.2652 2 19V6ZM4 8V18H16V8H4ZM7 10H9V16H7V10ZM11 10H13V16H11V10ZM5 3V1C5 0.734784 5.10536 0.48043 5.29289 0.292893C5.48043 0.105357 5.73478 0 6 0H14C14.2652 0 14.5196 0.105357 14.7071 0.292893C14.8946 0.48043 15 0.734784 15 1V3H20V5H0V3H5ZM7 2V3H13V2H7Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}
