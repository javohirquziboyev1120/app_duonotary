import React, {Component} from "react";
import {
    editAdminPermission,
    getAdminList,
    getCurrentUser,
    getStateList,
    getCountyByState,
    getZipCodeByCounty,
    getPermissionList,
    saveAdmin,
    changeAdminEnabled,
    editAdmin,
    editAdminZipCode,
    deleteAdmin, getAdminZipCode, getNewPermissionList,
} from "../../redux/actions/AdminAction";
import {connect} from "react-redux";
import {AvForm, AvField} from "availity-reactstrap-validation";
import Switch from "react-switch";
import {
    Button,
    Modal,
    ModalBody,
    ModalFooter, UncontrolledCollapse,
    ModalHeader,
    CustomInput,
} from "reactstrap";
import Select from "react-select";
import StatusModal from "../../components/Modal/StatusModal";
import DeleteModal from "../../components/Modal/DeleteModal";
import Moment from "moment";
import AdminLayout from "../../components/AdminLayout";
import {toast} from "react-toastify";
import {AddIcon} from "../../components/Icons";
import CloseBtn from "../../components/CloseBtn";

class Admin extends Component {
    componentDidMount() {
        this.props.dispatch(getCurrentUser());
        this.props.dispatch(getAdminList());
        this.props.dispatch(getStateList());
        this.props.dispatch(getPermissionList());
        this.props.dispatch(getNewPermissionList());
        this.props.dispatch({
            type: "updateState",
            payload: {
                showModal: false,
                showDeleteModal: false,
                showStatusModal: false,
                showPerModal: false,
                showEditModal: false,
                showZipCodeModal: false,
                showInfoModal: false,
            }
        })
    }

    constructor(props) {
        super(props);
        this.state = {showPreEditModal: false, currentItemForPreEdit: {}};
    }

    toggleShowPreEditModal = (item) => {
        this.setState({
            showPreEditModal: !this.state.showPreEditModal,
            currentItemForPreEdit: item,
        });
    };

    render() {
        const {
            isSuperAdmin,
            newPermissions,
            adminZipCodes,
            dispatch,
            zipCodeOptions,
            selectCounties,
            selectStates,
            stateOptions,
            countyOptions,
            all,
            currentItem,
            showModal,
            admins,
            permissionsId,
            selectZipCodes,
            showStatusModal,
            showPerModal,
            showDeleteModal,
            showEditModal,
            showZipCodeModal,
            showInfoModal,
            bigPermissionsId
        } = this.props;
        const openModal = (currentAdmin) => {
            let permissionsList =
                currentAdmin && currentAdmin.permissions.map((item) => item.id);
            dispatch({
                type: "updateState",
                payload: {
                    showModal: !showModal,
                    currentItem: currentAdmin,
                    permissionsId: permissionsList,
                    bigPermissionsId: null
                },
            });
        };
        const openDeleteModal = (currentAdmin) => {
            dispatch({
                type: "updateState",
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentItem: currentAdmin,
                },
            });
        };
        const openInfoModal = (currentAdmin) => {
            if (!showInfoModal && currentAdmin) {
                dispatch(getAdminZipCode(currentAdmin))
            }
            dispatch({
                type: "updateState",
                payload: {
                    showInfoModal: !showInfoModal,
                    currentItem: currentAdmin,
                },
            });
        };
        const openEditModal = (currentAdmin) => {
            dispatch({
                type: "updateState",
                payload: {
                    showEditModal: !showEditModal,
                    currentItem: currentAdmin,
                },
            });
            this.toggleShowPreEditModal()
        };
        const openZipCodeModal = (currentAdmin) => {
            if (!showZipCodeModal && currentAdmin) {
                dispatch(getAdminZipCode(currentAdmin))
            }
            if (currentAdmin != null) {
                dispatch({
                    type: "updateState",
                    payload: {
                        showZipCodeModal: !showZipCodeModal,
                        currentItem: currentAdmin,
                        selectStates: currentAdmin.statesId,
                        selectCounties: currentAdmin.countiesId,
                        selectZipCodes: currentAdmin.zipCodesId,
                    },
                });
            } else {
                dispatch({
                    type: "updateState",
                    payload: {
                        showZipCodeModal: !showZipCodeModal,
                        currentItem: null,
                    },
                });
            }
        };
        const openPerModal = (currentAdmin) => {
            dispatch({
                type: "updateState",
                payload: {
                    bigPermissionsId: null,
                    currentItem: null,
                    permissionsId: null
                }
            });
            let permissionsList =
                currentAdmin && currentAdmin.permissions.map((item) => item.id);
            let permissionIdList = []
            if (currentAdmin && currentAdmin.permissions && currentAdmin.permissions.length > 0) {
                let allId = 0;
                for (let i = 0; i < newPermissions.length; i++) {
                    allId = 0;
                    for (let j = 0; j < newPermissions[i].perList.length; j++) {
                        // eslint-disable-next-line no-loop-func
                        permissionsList.map(item => {
                                if (newPermissions[i].perList[j].id === item) {
                                    allId++;
                                }
                            }
                        );
                    }
                    if (allId === newPermissions[i].perList.length) {
                        permissionIdList.push(newPermissions[i].id)
                    }
                }
            }
            dispatch({
                type: "updateState",
                payload: {
                    bigPermissionsId: permissionIdList,
                    showPerModal: !showPerModal,
                    currentItem: currentAdmin,
                    permissionsId: permissionsList
                }
            });
        };
        const changeBigPermission = (id) => {
            for (let i = 0; i < newPermissions.length; i++) {
                if (newPermissions[i].id === id) {
                    let permissionsIdList = permissionsId != null ? permissionsId : [];
                    let permissionIdList = bigPermissionsId != null ? bigPermissionsId : [];
                    if (permissionIdList.includes(id)) {
                        // eslint-disable-next-line no-loop-func
                        permissionIdList = permissionIdList.filter((k) => k !== id);
                        newPermissions[i].perList.map(l =>
                            permissionsIdList = permissionsIdList.filter((k) => k !== l.id)
                        )
                    } else {
                        permissionIdList = [...permissionIdList, id];
                        permissionsIdList = [...permissionsIdList, ...(newPermissions[i].perList.map(l => l.id))]
                    }
                    dispatch({
                        type: "updateState",
                        payload: {
                            bigPermissionsId: permissionIdList,
                            permissionsId: permissionsIdList,
                        },
                    });
                    break
                }

            }
        }
        const changePermission = (id, bigId) => {

            let permissionIdList = permissionsId != null ? permissionsId : [];
            if (permissionIdList.includes(id)) {
                permissionIdList = permissionIdList.filter((item) => item !== id);
                if (bigId) {
                    let permissionIdList = bigPermissionsId != null ? bigPermissionsId : [];
                    permissionIdList = permissionIdList.filter((k) => k !== bigId);
                    dispatch({
                        type: "updateState",
                        payload: {
                            bigPermissionsId: permissionIdList
                        },
                    });
                }
            } else {
                permissionIdList = [...permissionIdList, id];
            }
            dispatch({
                type: "updateState",
                payload: {
                    permissionsId: permissionIdList,
                },
            });
        };
        const getCounty = (e, v) => {
            if (!all) {
                let arr = [];
                if (e && e.length === 1) {
                    if (v.action === "remove-value") {
                        v.option = e[0];
                    }
                    dispatch(getCountyByState(v));
                }
                if (e) {
                    e.map((item) => arr.push(item.value));
                    dispatch({
                        type: "updateState",
                        payload: {
                            selectStates: arr,
                            selectCounties: null,
                        },
                    });
                } else {
                    dispatch({
                        type: "updateState",
                        payload: {
                            selectCounties: null,
                            selectZipCodes: null,
                        },
                    });
                }
            }
        };
        const getZipCode = (e, v) => {
            let arr = [];
            if (e && e.length === 1) {
                if (v.action === "remove-value") {
                    v.option = e[0];
                }
                dispatch(getZipCodeByCounty(v));
            }
            if (e) {
                e.map((item) => arr.push(item.value));
                dispatch({
                    type: "updateState",
                    payload: {
                        selectCounties: arr,
                    },
                });
            } else {
                dispatch({
                    type: "updateState",
                    payload: {
                        selectCounties: null,
                        selectZipCodes: null,
                    },
                });
            }
        };
        const saveZipCodeForServicePrice = (e, v) => {
            let arr = [];
            if (e) {
                e.map((item) => arr.push(item.value));
                dispatch({
                    type: "updateState",
                    payload: {
                        selectZipCodes: arr,
                    },
                });
            } else {
                dispatch({
                    type: "updateState",
                    payload: {
                        selectZipCodes: null,
                    },
                });
            }
        };
        const savedPerAdmin = (e, v) => {
            let userDto = {};
            userDto.id = currentItem.id;
            userDto.phoneNumber = currentItem.phoneNumber;
            userDto.permissionsId = permissionsId;
            this.props.dispatch(editAdminPermission(userDto));
            this.toggleShowPreEditModal();
        };
        const savedAdmin = (e, v) => {
            v.statesId = selectStates;
            if (selectStates && selectStates.length === 1) {
                v.statesId = [];
                v.countiesId = selectCounties;
                if (selectCounties && selectCounties.length === 1) {
                    v.countiesId = [];
                    v.zipCodesId = selectZipCodes;
                }
            } else if (all) {
                if (stateOptions && stateOptions.length > 0) {
                    let arr = []
                    stateOptions.map((item) => arr.push(item.value));
                    v.statesId = arr;
                }
            }
            v.permissionsId = permissionsId;
            if (v.statesId || v.countiesId || v.zipCodesId) {
                this.props.dispatch(saveAdmin(v));
            } else {
                toast.error("Please select zipcode!")
            }
        };
        const changeStatusAdmin = () => {
            this.props.dispatch(changeAdminEnabled(currentItem));
        };
        const openStatusModal = (admin) => {
            dispatch({
                type: "updateState",
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: admin,
                },
            });
        };
        const changeAll = (e, v) => {
            dispatch({
                type: "updateState",
                payload: {
                    all: !all
                }
            })
            if (all) {
                let arr = []
                stateOptions && stateOptions.length > 0 && stateOptions.map((item) => arr.push(item.value));
                dispatch({
                    type: "updateState",
                    payload: {
                        selectStates: arr,
                        selectCounties: null,
                        selectZipCodes: null,
                    }
                })
            } else {
                dispatch({
                    type: "updateState",
                    payload: {
                        arr: null,
                        selectStates: null,
                        selectCounties: null,
                        selectZipCodes: null,
                    }
                })
            }
        };
        const savedZipCodeAdmin = (e, v) => {
            if (currentItem && currentItem.id) {
                v.statesId = selectStates;
                if (selectStates && selectStates.length === 1) {
                    if (selectCounties && selectCounties.length > 0) {
                        v.statesId = [];
                        v.countiesId = selectCounties;
                        if (selectCounties && selectCounties.length === 1) {
                            v.countiesId = [];
                            v.zipCodesId = selectZipCodes;
                        }
                    }
                } else if (all) {
                    let arr = []
                    stateOptions.map((item) => arr.push(item.value));
                    v.statesId = arr;
                }
                v.id = currentItem.id;
                this.props.dispatch(editAdminZipCode(v));
            } else {
                dispatch({
                    type: "updateState",
                    payload: {
                        showZipCodeModal: false
                    }
                })
            }
            this.toggleShowPreEditModal();

        };
        const savedEditAdmin = (e, v) => {
            v.id = currentItem.id;
            this.props.dispatch(editAdmin(v));
            this.toggleShowPreEditModal();
        };
        const deleteFunction = () => {
            this.props.dispatch(deleteAdmin(currentItem));
        };

        return (
            <AdminLayout pathname={this.props.location.pathname}>
                {isSuperAdmin ?
                    <div className="per-admin-page container">
                        <h3 className="text-center">Admin list</h3>
                        <AddIcon
                            title={true}
                            onClick={() => openModal(null)}
                        />
                        {admins != null && (
                            <div className="flex-column margin-top-25">
                                {admins.map((item, i) => (
                                    <div
                                        key={i}
                                        className="flex-row align-items-center justify-content-between single-admin margin-top-15"
                                    >
                                        <div className="avatar-container">
                                            <img
                                                src="/assets/img/avatar.png"
                                                alt="avatar img"
                                                className="avatar-img"
                                            />
                                            <span
                                                className={item.online ? "avatar-status-online" : "avatar-status-offline"}> </span>
                                        </div>
                                        <div className="flex-column name-div">
                                            <span className="label-text">Admin</span>
                                            <span className="main-text-20">
                                            {item.firstName + " " + item.lastName}
                                        </span>
                                        </div>
                                        <div className="flex-column name-div">
                                            <span className="label-text">E-mail:</span>
                                            <span className="main-text-15">
                                                    {item.email}
                                                </span>
                                        </div>
                                        <div className="flex-column name-div">
                                            <span className="label-text">Status</span>
                                            <Switch className="margin-top-15" checked={item.enabled}
                                                    id={"active" + 1}
                                                    onChange={() => {
                                                        openStatusModal(item)
                                                    }}/>
                                        </div>

                                        <div className="flex-column service-type-div">
                                    <span className="fake-anchor" onClick={() => openInfoModal(item)}>
                                        More info
                                    </span>
                                        </div>
                                        <div className="flex-column">
                                            <EditIcon
                                                onClick={() => {
                                                    this.toggleShowPreEditModal(item);
                                                }}
                                            />
                                        </div>
                                        <DeleteIcon
                                            onClick={() => openDeleteModal(item)}
                                        />
                                    </div>
                                ))}
                                {/*    Map end*/}
                            </div>
                        )}

                        <Modal
                            id="allModalStyle"
                            size="lg"
                            isOpen={this.state.showPreEditModal}
                            toggle={() => {
                                this.toggleShowPreEditModal({});
                            }}
                        >
                            <div className='position-absolute' style={{top: '45px', left: '-12%'}}>
                                <CloseBtn click={() => {
                                    this.toggleShowPreEditModal({});
                                }}/>
                            </div>
                            <ModalHeader>
                                <span className="main-text-20">Edit menu</span>
                            </ModalHeader>
                            <ModalBody>
                                <div className="flex-column">
                                    <div
                                        className="flex-row edit-btn"
                                        onClick={() =>
                                            openEditModal(
                                                this.state.currentItemForPreEdit
                                            )
                                        }
                                    >
                                        <EditIcon className="margin-right-15"/>
                                        <span>Edit info</span>
                                    </div>
                                    <div
                                        className="flex-row edit-btn"
                                        onClick={() =>
                                            openZipCodeModal(
                                                this.state.currentItemForPreEdit
                                            )
                                        }
                                    >
                                        <EditIcon className="margin-right-15"/>
                                        <span>Edit zipcodes</span>
                                    </div>
                                    <div
                                        className="flex-row edit-btn"
                                        onClick={() =>
                                            openPerModal(
                                                this.state.currentItemForPreEdit
                                            )
                                        }
                                    >
                                        <EditIcon className="margin-right-15"/>
                                        <span>Edit permission</span>
                                    </div>
                                </div>
                            </ModalBody>
                        </Modal>

                        <Modal id="allModalStyle" size="lg" isOpen={showEditModal} toggle={() => openEditModal(null)}
                        >
                            <AvForm onValidSubmit={savedEditAdmin}>
                                <div className='position-absolute' style={{top: '40px', left: '-12%'}}>
                                    <CloseBtn click={() => openEditModal(null)}/>
                                </div>
                                <ModalHeader
                                    charCode="x"
                                >
                                    Admin
                                </ModalHeader>
                                <ModalBody>
                                    <div className="flex-column">
                                        <AvField
                                            name="firstName"
                                            value={currentItem && currentItem.firstName}
                                            label="Enter the first name:"
                                            type="text"
                                            required
                                        />
                                        <AvField
                                            name="lastName"
                                            value={currentItem && currentItem.lastName}
                                            label="Enter the last name:"
                                            type="text"
                                            required
                                        />
                                        <AvField
                                            name="email"
                                            value={currentItem && currentItem.email}
                                            label="Email Address"
                                            type="email"
                                            required
                                        />
                                        <AvField
                                            name="phoneNumber"
                                            value={
                                                currentItem && currentItem.phoneNumber
                                            }
                                            label="Enter the phone number"
                                            type="text"
                                            required
                                        />
                                    </div>
                                </ModalBody>
                                <ModalFooter>
                                    {/*<Button*/}
                                    {/*    onClick={() => openEditModal(null)}*/}
                                    {/*    color="dark"*/}
                                    {/*>*/}
                                    {/*    Cancel*/}
                                    {/*</Button>*/}
                                    <Button type="submit" color={'info'} outline>
                                        Save
                                    </Button>
                                </ModalFooter>
                            </AvForm>
                        </Modal>

                        <Modal id="allModalStyle" size="lg" isOpen={showModal} toggle={() => openModal(null)}>
                            <AvForm onValidSubmit={savedAdmin}>
                                <div className='position-absolute' style={{top: '40px', left: '-12%'}}>
                                    <CloseBtn click={() => openModal(null)}/>
                                </div>
                                <ModalHeader
                                    charCode="x"
                                >
                                    Add new admin
                                </ModalHeader>
                                <ModalBody>
                                    <div className="flex-column">
                                        <AvField
                                            name="firstName"
                                            label="Enter the first name:"
                                            type="text"
                                            required
                                        />
                                        <AvField
                                            name="lastName"
                                            label="Enter the last name:"
                                            type="text"
                                            required
                                        />
                                        <AvField
                                            name="email"
                                            label="Email Address"
                                            type="email"
                                            required
                                        />
                                        <AvField
                                            name="phoneNumber"
                                            label="Enter the phone number"
                                            type="text"
                                            required
                                        />
                                        <CustomInput
                                            className="check-box-title ml-5"
                                            type="checkbox"
                                            checked={all}
                                            name="allZipCode"
                                            onClick={changeAll}
                                            label='Add all zip code'
                                            id="all"
                                        />
                                        <Select
                                            isDisabled={all}
                                            defaultValue="Select State"
                                            isMulti
                                            label="Select zim code"
                                            name="statesId"
                                            options={stateOptions}
                                            onChange={getCounty}
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                            required
                                        />
                                        {selectStates != null && !all &&
                                        selectStates.length === 1 ? (
                                            <div>
                                                <Select
                                                    defaultValue="Select county"
                                                    isMulti
                                                    name="countiesId"
                                                    options={countyOptions}
                                                    onChange={getZipCode}
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                />
                                                {selectCounties != null &&
                                                selectCounties.length === 1 ? (
                                                    <Select
                                                        defaultValue="Select Zipcode"
                                                        isMulti
                                                        name="zipCodesId"
                                                        options={zipCodeOptions}
                                                        onChange={
                                                            saveZipCodeForServicePrice
                                                        }
                                                        className="basic-multi-select"
                                                        classNamePrefix="select"
                                                    />
                                                ) : (
                                                    ""
                                                )}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                    </div>

                                    {/*Permission exp*/}
                                    <div className="flex-column checkbox-section mt-5 pt-5">
                                        <span className="main-text-20">Permissions</span>
                                        {/*Map*/}
                                        {newPermissions && newPermissions.length > 0 ?
                                            <div>
                                                {newPermissions.map((item, i) =>
                                                    <div key={i} className="single-checkbox-section">
                                                        <div className="flex-row check-box-all">
                                                            <CustomInput type="checkbox"
                                                                         id={item.id}
                                                                         checked={
                                                                             bigPermissionsId &&
                                                                             bigPermissionsId.some(
                                                                                 (permission) =>
                                                                                     permission ===
                                                                                     item.id
                                                                             )
                                                                         }
                                                                         onClick={() => changeBigPermission(item.id)}/>
                                                            <span color="primary"
                                                                  id={("s" + item.id)}> {/* shu id bilan pastdagi toggler="" ichidagi id bir xil va unique bo'lishi kerak */}
                                                                {item.generalName}
                                                            </span>
                                                        </div>
                                                        {item.perList && item.perList.length > 0 ?
                                                            <UncontrolledCollapse className="ml-3"
                                                                                  toggler={("#s" + item.id)}>
                                                                <div className="flex-column">
                                                                    {item.perList.map((per, k) =>
                                                                        <CustomInput key={per.id} type="checkbox"
                                                                                     id={per.id}
                                                                                     checked={
                                                                                         permissionsId &&
                                                                                         permissionsId.some(
                                                                                             (permission) =>
                                                                                                 permission ===
                                                                                                 per.id
                                                                                         )
                                                                                     }
                                                                                     onClick={() =>
                                                                                         changePermission(
                                                                                             per.id, item.id
                                                                                         )
                                                                                     }
                                                                                     label={per.perName}/>
                                                                    )}
                                                                </div>
                                                            </UncontrolledCollapse>
                                                            : ""}
                                                    </div>
                                                )}
                                            </div>
                                            : ""}
                                        {/*Map end*/}

                                    </div>
                                    {/*Permission exp end*/}

                                </ModalBody>
                                <ModalFooter>
                                    {/*<Button*/}
                                    {/*    onClick={() => openModal(null)}*/}
                                    {/*    color="dark"*/}
                                    {/*>*/}
                                    {/*    Cancel*/}
                                    {/*</Button>*/}
                                    <Button type="submit" color={'info'} outline>
                                        Save
                                    </Button>
                                </ModalFooter>
                            </AvForm>
                        </Modal>

                        <Modal id="allModalStyle" size="lg" isOpen={showZipCodeModal}
                               toggle={() => openZipCodeModal(null)}
                        >
                            <AvForm onValidSubmit={savedZipCodeAdmin}>
                                <div className='position-absolute' style={{top: '40px', left: '-12%'}}>
                                    <CloseBtn click={() => openZipCodeModal(null)}/>
                                </div>
                                <ModalHeader
                                    charCode="x"
                                >
                                    Admin
                                </ModalHeader>
                                <ModalBody>
                                    <div className="col">
                                        <CustomInput
                                            className="check-box-title ml-5"
                                            type="checkbox"
                                            checked={all}
                                            onChange={changeAll}
                                            label='Add all zip code'
                                            id="all"
                                        />
                                        <Select
                                            isDisabled={all}
                                            defaultValue="Select State"
                                            isMulti
                                            name="statesId"
                                            options={stateOptions}
                                            onChange={getCounty}
                                            className="basic-multi-select"
                                            classNamePrefix="select"
                                            required
                                        />
                                        {selectStates != null && !all &&
                                        selectStates.length === 1 ? (
                                            <div>
                                                <Select
                                                    defaultValue="Select county"
                                                    isMulti
                                                    name="countiesId"
                                                    options={countyOptions}
                                                    onChange={getZipCode}
                                                    className="basic-multi-select"
                                                    classNamePrefix="select"
                                                />
                                                {selectCounties != null &&
                                                selectCounties.length === 1 ? (
                                                    <Select
                                                        defaultValue="Select Zipcode"
                                                        isMulti
                                                        name="zipCodesId"
                                                        options={zipCodeOptions}
                                                        onChange={
                                                            saveZipCodeForServicePrice
                                                        }
                                                        className="basic-multi-select"
                                                        classNamePrefix="select"
                                                    />
                                                ) : (
                                                    ""
                                                )}
                                            </div>
                                        ) : (
                                            ""
                                        )}
                                        {adminZipCodes && adminZipCodes.length > 0 ?
                                            <div className="mt-3  w-100 p-2">
                                                <h5>Current admin zip codes:</h5>
                                                <div className="mt-1 flex-row flex-wrap">
                                                    {adminZipCodes.map((item, i) =>
                                                        <span key={i}
                                                              className="bg-white rounded shadow-sm py-2 px-3 m-1">{item.code}</span>
                                                    )}
                                                </div>
                                            </div> : ""
                                        }
                                    </div>
                                </ModalBody>
                                <ModalFooter>
                                    {/*<Button*/}
                                    {/*    onClick={() => openZipCodeModal(null)}*/}
                                    {/*    color="dark"*/}
                                    {/*>*/}
                                    {/*    Cancel*/}
                                    {/*</Button>*/}
                                    <Button type="submit" color={'info'} outline>
                                        Save
                                    </Button>
                                </ModalFooter>
                            </AvForm>
                        </Modal>

                        <Modal id="allModalStyle" size="lg" isOpen={showPerModal} toggle={() => openPerModal(null)}
                        >
                            <AvForm
                                onValidSubmit={savedPerAdmin}
                            >
                                <div className='position-absolute' style={{top: '40px', left: '-12%'}}>
                                    <CloseBtn click={() => openPerModal(null)}/>
                                </div>
                                <ModalHeader
                                    charCode="x"
                                >
                                    Admin
                                </ModalHeader>
                                <ModalBody>
                                    <div className="row mt-4 ml-3">
                                        <div className="col">
                                            {/*Permission exp*/}
                                            <div className="flex-column checkbox-section mt-5 pt-5">
                                                <span className="main-text-20">Permissions</span>
                                                {/*Map*/}
                                                {newPermissions && newPermissions.length > 0 ?
                                                    <div>
                                                        {newPermissions.map((item, i) =>
                                                            <div key={i} className="single-checkbox-section">
                                                                <div className="flex-row check-box-all">
                                                                    <CustomInput type="checkbox"
                                                                                 id={item.id}
                                                                                 checked={
                                                                                     bigPermissionsId &&
                                                                                     bigPermissionsId.some(
                                                                                         (permission) =>
                                                                                             permission ===
                                                                                             item.id
                                                                                     )
                                                                                 }
                                                                                 onClick={() => changeBigPermission(item.id)}/>
                                                                    <span color="primary"
                                                                          id={("s" + item.id)}> {/* shu id bilan pastdagi toggler="" ichidagi id bir xil va unique bo'lishi kerak */}
                                                                        {item.generalName}
                                                            </span>
                                                                </div>
                                                                {item.perList && item.perList.length > 0 ?
                                                                    <UncontrolledCollapse className="ml-3"
                                                                                          toggler={("#s" + item.id)}>
                                                                        <div className="flex-column">
                                                                            {item.perList.map((per, k) =>
                                                                                <CustomInput key={per.id}
                                                                                             type="checkbox"
                                                                                             id={per.id}
                                                                                             checked={
                                                                                                 permissionsId &&
                                                                                                 permissionsId.some(
                                                                                                     (permission) =>
                                                                                                         permission ===
                                                                                                         per.id
                                                                                                 )
                                                                                             }
                                                                                             onClick={() =>
                                                                                                 changePermission(
                                                                                                     per.id, item.id
                                                                                                 )
                                                                                             }
                                                                                             label={per.perName}/>
                                                                            )}
                                                                        </div>
                                                                    </UncontrolledCollapse>
                                                                    : ""}
                                                            </div>
                                                        )}
                                                    </div>
                                                    : ""}
                                                {/*Map end*/}

                                            </div>
                                            {/*Permission exp end*/}
                                        </div>
                                    </div>
                                </ModalBody>
                                <ModalFooter>
                                    {/*<Button*/}
                                    {/*    onClick={() => openPerModal(null)}*/}
                                    {/*    color="dark"*/}
                                    {/*>*/}
                                    {/*    Cancel*/}
                                    {/*</Button>*/}
                                    <Button type="submit" color={'info'} outline>
                                        Save
                                    </Button>
                                </ModalFooter>
                            </AvForm>
                        </Modal>

                        <Modal id="allModalStyle" size="lg" isOpen={showInfoModal}>
                            <div className='position-absolute' style={{top: '40px', left: '-12%'}}>
                                <CloseBtn click={() => openInfoModal(null)}/>
                            </div>
                            <ModalHeader
                                toggle={() => openInfoModal(null)}
                                charCode="x"
                            >
                                Info
                            </ModalHeader>
                            <ModalBody>
                                {currentItem ? (

                                    <div className="info-box flex-column">
                                        <div className="flex-row margin-top-15">
                                            <span className="label">First name: </span>
                                            <span className="info-item">{currentItem.firstName}</span>
                                        </div>
                                        <div className="flex-row margin-top-15">
                                            <span className="label">Last name: </span>
                                            <span className="info-item">{currentItem.lastName}</span>
                                        </div>
                                        <div className="flex-row margin-top-15">
                                            <span className="label">Email: </span>
                                            <span className="info-item">{currentItem.email}</span>
                                        </div>
                                        <div className="flex-row margin-top-15">
                                            <span className="label">Phone number: </span>
                                            <span className="info-item">{currentItem.phoneNumber}</span>
                                        </div>
                                        <div className="flex-row margin-top-15">
                                            <span className="label">Online: </span>
                                            <span className="info-item">{currentItem.online
                                                ? "On"
                                                : "Off"}</span>
                                        </div>
                                        <div className="flex-row margin-top-15">
                                            <span className="label">Enabled: </span>
                                            <span className="info-item">{currentItem.enabled
                                                ? "Enabled"
                                                : "Blocked"}</span>
                                        </div>
                                        <div className="flex-row margin-top-15">
                                            <span className="label">Add time: </span>
                                            <span className="info-item">{Moment(
                                                currentItem.createdAt
                                            ).format("lll")}</span>
                                        </div>
                                        <div className="flex-row margin-top-15">
                                            <span className="label">Last updated time: </span>
                                            <span className="info-item">{Moment(
                                                currentItem.updateAt
                                            ).format("lll")}</span>
                                        </div>
                                        {adminZipCodes && adminZipCodes.length > 0 ?
                                            <div className="mt-3 flex-row flex-wrap w-100">
                                                <h5>Current admin zip codes:</h5>
                                                {adminZipCodes.map((item, i) =>
                                                    <span key={i}
                                                          className="bg-white rounded shadow-sm py-2 px-3 m-1">{item.code}</span>
                                                )}
                                            </div> : ""
                                        }
                                    </div>


                                ) : (
                                    "Error!"
                                )}
                            </ModalBody>
                            <ModalFooter>
                                {/*<Button*/}
                                {/*    onClick={() => openInfoModal(null)}*/}
                                {/*    color="dark"*/}
                                {/*>*/}
                                {/*    Close*/}
                                {/*</Button>*/}
                            </ModalFooter>
                        </Modal>


                        {showDeleteModal && (
                            <DeleteModal
                                text={
                                    currentItem != null
                                        ? currentItem.firstName +
                                        " " +
                                        currentItem.lastName
                                        : ""
                                }
                                showDeleteModal={showDeleteModal}
                                confirm={deleteFunction}
                                cancel={openDeleteModal}
                            />
                        )}

                        {showStatusModal && (
                            <StatusModal
                                text={
                                    currentItem != null
                                        ? currentItem.firstName +
                                        " " +
                                        currentItem.lastName
                                        : ""
                                }
                                showStatusModal={showStatusModal}
                                confirm={changeStatusAdmin}
                                cancel={openStatusModal}
                            />
                        )}
                    </div>
                    : <h1 className="text-center">404 - Page not found</h1>}

            </AdminLayout>
        );
    }
}

export default connect(
    ({
         auth: {
             isSuperAdmin,
         },
         admin: {
             newPermissions,
             all,
             showZipCodeModal,
             showEditModal,
             showDeleteModal,
             showPerModal,
             showStatusModal,
             selectZipCodes,
             zipCodeOptions,
             selectCounties,
             selectStates,
             countyOptions,
             stateOptions,
             loading,
             currentUser,
             admins,
             active,
             showModal,
             currentItem,
             permissions,
             permissionsId,
             showInfoModal,
             adminZipCodes,
             bigPermissionsId
         }
         ,
     }
    ) =>
        ({
            isSuperAdmin,
            newPermissions,
            all,
            adminZipCodes,
            showZipCodeModal,
            showEditModal,
            showDeleteModal,
            showPerModal,
            showStatusModal,
            selectZipCodes,
            zipCodeOptions,
            selectCounties,
            selectStates,
            countyOptions,
            stateOptions,
            loading,
            currentUser,
            admins,
            active,
            showModal,
            currentItem,
            permissions,
            permissionsId,
            showInfoModal,
            bigPermissionsId
        })
)
(Admin);

function ThreeDotIcon(props) {
    return (
        <div onClick={props.onClick}>
            <svg
                width="18"
                height="4"
                viewBox="0 0 18 4"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M1.5 0.5C0.675 0.5 0 1.175 0 2C0 2.825 0.675 3.5 1.5 3.5C2.325 3.5 3 2.825 3 2C3 1.175 2.325 0.5 1.5 0.5ZM16.5 0.5C15.675 0.5 15 1.175 15 2C15 2.825 15.675 3.5 16.5 3.5C17.325 3.5 18 2.825 18 2C18 1.175 17.325 0.5 16.5 0.5ZM9 0.5C8.175 0.5 7.5 1.175 7.5 2C7.5 2.825 8.175 3.5 9 3.5C9.825 3.5 10.5 2.825 10.5 2C10.5 1.175 9.825 0.5 9 0.5Z"
                    fill="black"
                />
            </svg>
        </div>
    );
}

function EditIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{cursor: "pointer"}}
                width="18"
                height="18"
                viewBox="0 0 18 18"
                fill="#313E47"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M12.728 6.68599L11.314 5.27199L2 14.586V16H3.414L12.728 6.68599ZM14.142 5.27199L15.556 3.85799L14.142 2.44399L12.728 3.85799L14.142 5.27199ZM4.242 18H0V13.757L13.435 0.321992C13.6225 0.134521 13.8768 0.0292053 14.142 0.0292053C14.4072 0.0292053 14.6615 0.134521 14.849 0.321992L17.678 3.15099C17.8655 3.33852 17.9708 3.59283 17.9708 3.85799C17.9708 4.12316 17.8655 4.37746 17.678 4.56499L4.243 18H4.242Z"/>
            </svg>
        </div>
    );
}


function DeleteIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{cursor: "pointer"}}
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M2 6H18V19C18 19.2652 17.8946 19.5196 17.7071 19.7071C17.5196 19.8946 17.2652 20 17 20H3C2.73478 20 2.48043 19.8946 2.29289 19.7071C2.10536 19.5196 2 19.2652 2 19V6ZM4 8V18H16V8H4ZM7 10H9V16H7V10ZM11 10H13V16H11V10ZM5 3V1C5 0.734784 5.10536 0.48043 5.29289 0.292893C5.48043 0.105357 5.73478 0 6 0H14C14.2652 0 14.5196 0.105357 14.7071 0.292893C14.8946 0.48043 15 0.734784 15 1V3H20V5H0V3H5ZM7 2V3H13V2H7Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}
