import React, {Component} from 'react';
import TextEditor from "../../components/TextEditor";

import AdminLayout from "../../components/AdminLayout";
import {PostSmall} from "../../components/BlogPost";
import {
    deleteBlog,
    getBlogList,
    getCategorys,
    saveBlog,
} from "../../redux/actions/AppAction";
import {toast} from "react-toastify";

import {connect} from "react-redux";
import {userMe} from "../../redux/actions/AuthActions";
import {uploadFile} from "../../redux/actions/AttachmentAction";
import { Col, Row} from "reactstrap";
import Pagination from "react-js-pagination";
import {config} from "../../utils/config";
import Select from "react-select";
import {Link} from "react-router-dom";
import UploadFile from "../../components/UploadFile";


class Blogs extends Component {
    componentDidMount() {
        this.props.dispatch(userMe())
        this.props.dispatch(getCategorys())
        this.props.dispatch(getBlogList({page: 0, size: 10}))
    }

    constructor(props) {
        super(props);
        this.state = {
            editorHtml: '',
            editPostPage: false,
            title: '',
            featured: false,
            attachment:'',
            id:'',
            categoryId: null
        };
    }



    handlePageChange(pageNumber) {
        this.props.dispatch(getBlogList({page:pageNumber-1, size:10}))
        this.props.dispatch(
            {type:'updateState',
                payload:{
                    activePageBlog: pageNumber-1
                }})
    }

    handleTitleChange = (e) => {
        this.setState({title: e.target.value})
    }
    toggleEditPostPage = () => {
        this.setState({
            editPostPage: !this.state.editPostPage
        })
    }
    setEditValues = (item, options) =>{
        this.setState({
            title:item.title,
            editorHtml : item.text,
            featured : item.featured,
            attachment:item.attachmentId,
            id:item.id,
            categoryId: options.find((opt)=>{

                if(opt.value===item.categoryId){
                    return opt;
                } else {
                    return null;
                }
            })
        })
    }

    defaultState =() =>{
        this.setState({editorHtml: '',
            editPostPage: false,
            title: '',
            featured: false,
            attachment:'',
            id:'',
            categoryId: null
        })
    }
    handleChange = (html) => {
        this.setState({editorHtml: html});

    }
    handleCategory = categoryId => {

        this.setState({ categoryId:categoryId });
    };



    render() {


        const {agents, isAdmin, currentUser, states} = this.props;
        const {attachmentIdsArray,
            showEmbassyModal, page, size,categorys, totalElements, embassy, dispatch, showModal, countries, currentItem, active, showDeleteModal, showStatusModal,
            activePageBlog,
            totalElementsBlog,
            blogsPage,
        } = this.props;
        const {editorState} = this.state;

        let options=[];
        categorys.map(item=>{
            options.push({value: item.id, label: item.name});
        });


        const saveItem = (e) => {
            let value={};
            let attach=attachmentIdsArray.filter(item=>item.key==="blog")[0];
            if( this.state.editorHtml !== '' &&
                this.state.title !== '' &&
                attach){
                value={
                    id:this.state.id!=null?this.state.id:null,
                    title : this.state.title,
                    text : this.state.editorHtml,
                    featured : this.state.featured,
                    attachmentId:attach?attach.value:null,
                    categoryId:this.state.categoryId!=null? this.state.categoryId.value: null,
                    url:'test',
                }
                this.props.dispatch(saveBlog(value));
                this.defaultState();
            } else {
                toast.warning("Please fill all required fields");
            }

        }
        const deleteBlogs=(e)=>{
            let deleteBl={};
            deleteBl={id:this.state.id}
            this.props.dispatch(deleteBlog(deleteBl))
        }
        return (
            <AdminLayout pathname={this.props.location.pathname}>
                {!this.state.editPostPage ?
                    <div className="container admin-blog-page flex-row align-items-start">

                        <div className="flex-column new-post-section margin-bottom-50">
                            <span className="dark-title margin-top-25">Create new post</span>

                            <input className="input-title" name="title"   value={this.state.title} onChange={this.handleTitleChange}
                                   placeholder="Title"/>
                            <div className="flex-row margin-top-15">
                                <span className="dark-title margin-right-15">Choose Category*: </span>
                                <Select options={options} className="flex-grow-1 margin-right-15" value={this.state.categoryId} onChange={this.handleCategory} />
                                <Link to="/admin/blog/category">
                                    <button className="publish-btn m-0">New category</button>
                                </Link>
                            </div>
                            <div className="flex-row align-items-center margin-top-15">
                                <div className="w-75"><UploadFile onChange={(item)=>dispatch(uploadFile(item.target.files[0],"blog"))} /></div>
                                <label className={'labelDuo ml-5'}>
                                    <div className={'labelBlock'}>
                                        {this.state.featured && <div className={'labelBox'}>
                                            <i className="fas fa-check"></i>
                                        </div>}
                                    </div>
                                    <input
                                        className={'d-none'}
                                        type="checkbox"
                                        value={this.state.featured}
                                        onChange={()=>{this.setState({featured: !this.state.featured})}}
                                    />
                                    Featured
                                </label>
                            </div>
                            <div className="input-text-area">
                                <TextEditor editorHtml={this.state.editorHtml}  name="text"  handleChange={this.handleChange}/>
                            </div>


                            <button className="publish-btn align-self-end" onClick={(e)=> {
                                saveItem(e)
                            }}>Publish
                            </button>

                        </div>
                        <div className="flex-column small-posts margin-top-50">
                            <span className="featured-posts-title">Edit posts</span>
                            {blogsPage.map((item,index)=>
                                <PostSmall key={index}  onClick={()=> {
                                    this.setEditValues(item, options)
                                    this.toggleEditPostPage();
                                }} title={item.title}
                                           mainImg={config.BASE_URL+"/attachment/"+item.attachmentId}/>
                            )}
                            <Row>
                                <Col md={7} className="mx-auto">
                                        <Pagination
                                            activePage={activePageBlog+1}
                                            itemsCountPerPage={10}
                                            totalItemsCount={totalElementsBlog}
                                            pageRangeDisplayed={5}
                                            onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                                            linkClass="page-link"
                                        />
                                </Col>
                            </Row>
                        </div>
                    </div>
                    :
                    //Editing post

                    <div className="container admin-blog-page flex-row align-items-start">
                        <div className="flex-column new-post-section edit-post-section margin-bottom-50">
                            <span className="dark-title margin-top-25">Edit post</span>

                            <input className="input-title" name="title"    value={this.state.title} onChange={this.handleTitleChange}
                                   placeholder="Title"/>
                            <div className="margin-top-15">
                                <span className="dark-title margin-right-15">Choose Category*: </span>
                                <Select options={options}  className="flex-grow-1 margin-right-15" value={this.state.categoryId} onChange={this.handleCategory} />

                            </div>

                            {/*mainImg={config.BASE_URL+"/attachment/"+item.attachmentId}/>*/}
                            <img src={config.BASE_URL+"/attachment/"+this.state.attachment} className="margin-top-15 edit-post-image"
                                 alt="blog-img"/>

                            <div className="flex-row align-items-center margin-top-15"><span
                                className="dark-title margin-right-15">Choose main image</span>
                                <input type="file"    onChange={(item)=>dispatch(uploadFile(item.target.files[0],"blog"))} required className="input-file"/>
                                <span className="dark-title margin-right-15">Featured*: </span>
                                <label className={'labelDuo'}>
                                    <div className={'labelBlock'}>
                                        {this.state.featured && <div className={'labelBox'}>
                                            <i className="fas fa-check"></i>
                                        </div>}
                                    </div>
                                    <input
                                        className={'d-none'}
                                        type="checkbox"
                                        value={this.state.featured}
                                        onChange={()=>{this.setState({featured: !this.state.featured})}}
                                    />
                                </label>
                            </div>
                            <div className="input-text-area">
                                <TextEditor editorHtml={this.state.editorHtml} handleChange={this.handleChange}/>
                            </div>
                            <div className="flex-row align-self-end">

                                <button className="publish-btn delete-btn-color margin-right-15" onClick={(e)=>{
                                    deleteBlogs(e);
                                    this.defaultState();
                                }}>Delete</button>
                                <button className="publish-btn cancel-btn-color margin-right-15"
                                        onClick={()=>{
                                            this.defaultState();
                                        }}>Cancel
                                </button>
                                <button className="publish-btn" onClick={(e)=> {
                                    saveItem(e);
                                    this.defaultState();
                                }}>Update</button>
                            </div>

                        </div>
                    </div>


                }
            </AdminLayout>

        );
    }
}


export default connect(
    ({
         app: {
             page, size, totalElements,
             embassy,
             showEmbassyModal,
             countries, showModal, currentItem, active, showDeleteModal, showStatusModal,
             activePageBlog,
             totalElementsBlog,
             blogsPage,categorys
         },
         auth: {
             currentUser, isAdmin
         },
        attachment:{attachmentIdsArray}
     }) => ({
        page, size, totalElements,attachmentIdsArray,
        embassy,
        showEmbassyModal,
        countries,
        showModal,
        currentItem,
        active, showDeleteModal, showStatusModal, activePageBlog,
        totalElementsBlog,
        blogsPage, currentUser, isAdmin,categorys
    }))(Blogs);

