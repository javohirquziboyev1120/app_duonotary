import React, {Component} from 'react';
import {connect} from "react-redux";
import {withRouter} from 'react-router-dom';
import {deleteReview, getReviews, saveReview} from "../../redux/actions/AppAction";
import {Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row, Table} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {uploadImgReview} from "../../redux/actions/AttachmentAction";
import {config} from "../../utils/config";
import Pagination from "react-js-pagination";
import AdminLayout from "../../components/AdminLayout";
import {AddIcon} from "../../components/Icons";
import styled from "styled-components"
import {EditIcon} from "./Service";
import {DeleteIcon} from "./AdminOrder";
import CloseBtn from "../../components/CloseBtn";

class Review extends Component {
    componentDidMount() {
        this.props.dispatch(getReviews())
    }

    handlePageChange(pageNumber) {
        this.props.dispatch(getReviews({page: pageNumber - 1, size: 20}))
        this.props.dispatch(
            {
                type: 'updateState',
                payload: {
                    page: pageNumber - 1,
                    size: 20
                }
            })
    }

    render() {
        const {dispatch, currentItem, page, size, totalElements, totalPages, reviews, reviewModal, attachmentIdsArray, showDeleteModal, photo, workIcon} = this.props
        const openModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    reviewModal: !reviewModal,
                    currentItem: item,
                    photo: '',
                    workIcon: ''
                }
            })
        }
        const openDelModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    currentItem: item,
                    showDeleteModal: !showDeleteModal
                }
            })
        }
        const delReview = () => {
            this.props.dispatch(deleteReview(currentItem.id))
        }
        const handleSubmit = (e, v) => {
            v.photo = {id: photo ? photo : currentItem.photoId};
            v.id = currentItem ? currentItem.id : '';
            this.props.dispatch(saveReview(v))
        }

        return (
            <AdminLayout pathname={this.props.location.pathname}>
                <div className="admin-customer-page container">
                    <h3>Review list</h3>
                    <Row className="mt-md-5">
                        <Col>
                            <AddIcon onClick={openModal} title={true}/>
                        </Col>
                    </Row>
                    <Table className="custom-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Photo</th>
                            <th>Full name</th>
                            <th>Company name</th>
                            {/*<th>Title</th>*/}
                            <th>Description</th>
                            <th colSpan="2">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {reviews ?
                            reviews.map((item, i) =>
                                <tr key={item.id}>
                                    <th>{i + 1}</th>
                                    <th><Avatar
                                        src={item.photoId ? config.BASE_URL + "/attachment/" + item.photoId : "/assets/img/avatar.png"}
                                        alt=""/>
                                    </th>
                                    <th>{item.fullName}</th>
                                    <th>{item.companyName}</th>
                                    {/*<th>{item.title}</th>*/}
                                    <th>{item.description}</th>
                                    <th>
                                        <EditIcon onClick={() => openModal(item)}/>
                                    </th>
                                    <th>
                                        <DeleteIcon onClick={() => openDelModal(item)}/>
                                    </th>
                                </tr>
                            )
                            : <h3>Reviews empty</h3>
                        }
                        </tbody>
                    </Table>
                    <Pagination
                        activePage={page + 1}
                        itemsCountPerPage={size}
                        totalItemsCount={totalElements}
                        pageRangeDisplayed={5}
                        onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                        linkClass="page-link"
                    />

                    <Modal isOpen={reviewModal} toggle={openModal} id="allModalStyle">
                        <div className='position-absolute' style={{top : '45px', left : '-12%'}}>
                            <CloseBtn click={openModal} />
                        </div>
                        <AvForm onValidSubmit={handleSubmit}>
                            <ModalHeader
                                charCode="x">{currentItem != null ? "Edit review" : "Add review"}</ModalHeader>
                            <ModalBody>
                                <Row>
                                    <Col md={8}>
                                        <AvField type="file" name="photo"
                                                 onChange={(item) => {
                                                     dispatch(uploadImgReview({item: item.target.files[0]}))
                                                 }}
                                                 label="Select reviewer picture"
                                                 required={!currentItem}
                                                 accept="image/*"/>
                                    </Col>
                                    <Col className="mt-md-4">
                                        {currentItem ? <Img2 className="img-fluid"
                                                             src={currentItem !== '' && photo === '' ? config.BASE_URL + "/attachment/" + currentItem.photoId : photo !== '' ? config.BASE_URL + "/attachment/" + photo : "/assets/img/avatar.png"}
                                                             alt=""/> : ''}
                                    </Col>
                                </Row>

                                <AvField
                                    type="text"
                                    name="fullName"
                                    placeholder="Full name"
                                    value={currentItem ? currentItem.fullName : ''}
                                    required/>

                                <AvField
                                    type="text"
                                    name="companyName"
                                    placeholder="Company name"
                                    value={currentItem ? currentItem.companyName : ''}
                                    required/>
                                {/*<AvField*/}
                                {/*    type="text"*/}
                                {/*    name="title"*/}
                                {/*    placeholder="Title"*/}
                                {/*    value={currentItem ? currentItem.title : ''}*/}
                                {/*    required/>*/}
                                <AvField
                                    type="textarea"
                                    style={{height: 150}}
                                    name="description"
                                    placeholder="Description"
                                    value={currentItem ? currentItem.description : ''}
                                    required/>

                            </ModalBody>
                            <ModalFooter>
                                {/*<Button type="button" onClick={openModal} color="dark">Cancel</Button>*/}
                                <Button type="submit" color='info' outline >Submit</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal>
                    <Modal isOpen={showDeleteModal} toggle={openDelModal}>
                        <ModalHeader>Delete Review</ModalHeader>
                        <ModalFooter>
                            <Button type="button" onClick={openDelModal}>Cancel</Button>
                            <Button color="danger" onClick={() => delReview()}>Delete</Button>
                        </ModalFooter>
                    </Modal>

                </div>
            </AdminLayout>
        );
    }
}


export default connect(
    ({
         app: {
             page, size, totalElements, totalPages, reviews, reviewModal, currentItem, showDeleteModal
         },
         attachment: {attachmentIdsArray, photo, workIcon}
     }) => ({
        page,
        size,
        totalElements,
        totalPages,
        reviews,
        attachmentIdsArray,
        photo,
        workIcon,
        reviewModal,
        currentItem, showDeleteModal
    }))
(withRouter(Review));

const Img = styled.img`
                            max-width: 250px;
                            height: 100px;
                            object-fit: contain;
                            `;

const Img2 = styled.img`
                            max-height: 50px;
                            max-width: 50px;
                            `;

const Avatar = styled.img`
                            height: 80px;
                            width: 80px;
                            border-radius: 50%;
                            `;