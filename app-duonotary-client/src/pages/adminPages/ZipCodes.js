import React, {Component} from "react";
import AdminLayout from "../../components/AdminLayout";
import {Button, Col, CustomInput, Modal, ModalBody, ModalFooter, ModalHeader, Row, Table} from "reactstrap";
import {connect} from "react-redux";
import zipCode from "../../redux/reducers/ZipCodeReducer";
import * as types from "../../redux/actionTypes/ZipCodeTypes";
import * as zpActions from "../../redux/actions/ZipCodeActions"
import {AvField, AvForm} from "availity-reactstrap-validation";
import Pagination from "react-js-pagination";
import {AddIcon} from "../../components/Icons";
import CloseBtn from "../../components/CloseBtn";

class ZipCodes extends Component {

    componentDidMount() {
        this.props.dispatch({
            type: "updateState",
            payload: {
                showModal: false,
                showDeleteModal: false,
            }
        })
        this.props.dispatch(zpActions.getAllZipCodes({page: 0, size: 10}))
        this.props.dispatch(zpActions.allStates())
        this.props.dispatch(zpActions.allStates2())
        this.props.dispatch(zpActions.getTimezoneName())
    }

    handlePageChange(pageNumber) {
        const {dispatch, isZipCode, isCounty, isState} = this.props
        if (isZipCode) {
            dispatch(zpActions.getAllZipCodes({page: pageNumber - 1, size: 10}))
        }
        if (isCounty) {
            dispatch(zpActions.getAllCounties({page: pageNumber - 1, size: 10}))
        }
        if (isState) {
            dispatch(zpActions.getAllStatesByPageable({page: pageNumber - 1, size: 10}))
        }


    }

    render() {
        const {
            permissions, active,
            dispatch, isZipCode, isCounty, isState, countyArray, stateArray, stateArrayPageable,
            currentZipCode, currentCounty, currentState, showModal, showDeleteModal, removingType, removingItemId,
            totalCountyElements, totalStateElements, activePage, statesWithZipCodes, timezoneName,
            showCurrentZipCode, currentZipCode2, removingAgentId, agentList, isAddAgent, tempSelectedStateId
        } = this.props

        const changeTabePane = (tabePane) => {
            if (tabePane === 'zipCode') {
                dispatch({type: types.CHANGE_ZIP_CODE_BOOLEAN})
                dispatch(zpActions.allStates2())
                this.props.dispatch(zpActions.allStates())
            }
            if (tabePane === 'county') {
                dispatch({type: types.CHANGE_COUNTY_BOOLEAN})
                dispatch(zpActions.getAllCounties({page: 0, size: 10})).then(() => {
                    dispatch(zpActions.allStates())
                })
            }
            if (tabePane === 'state') {
                dispatch(zpActions.getAllStatesByPageable({page: 0, size: 10}))
                dispatch({type: types.CHANGE_STATE_BOOLEAN})
            }

        };
        const openModal = () => {
            dispatch({type: types.OPEN_CLOSE_MODAL})
        }
        const openModalCancel = () => {
            dispatch({type: types.OPEN_CLOSE_MODAL_CANCEL})
        }
        const openCloseDeleteModal = () => {
            dispatch({type: types.OPEN_CLOSE_DELETE_MODAL})
        }
        const openEditModal = (item, type) => {
            if (type === 'zipCode') {
                dispatch({
                    type: "updateState", payload: {
                        currentZipCode: item,
                        currentCounty: '',
                        currentState: '',
                        showModal: true
                    }
                })
                dispatch(zpActions.getCountyBySelectedState({id: item.countyDto.stateDto.id}))
            }
            if (type === 'county') {
                dispatch({
                    type: "updateState", payload: {
                        currentCounty: item,
                        currentZipCode: '',
                        currentState: '',
                        showModal: true
                    }
                })
            }
            if (type === 'state') {
                dispatch({
                    type: "updateState", payload: {
                        currentState: item,
                        currentZipCode: '',
                        currentCounty: '',
                        showModal: true
                    }
                })
            }
        }
        const openDeleteModal = (id, typ, agentId) => {
            dispatch({type: types.GET_REMOVING_TYPE_AND_ID, payload: {typ, id, agentId}})
        }
        const saveItem = (e, v) => {
            if (isState) {
                let data = {
                    name: v.name
                }
                if (currentState) {
                    data = {...data, id: currentState.id}
                }
                dispatch(zpActions.saveOrEditState(data)).then(() => {
                    dispatch(zpActions.getAllStatesByPageable({page: 0, size: 10}))
                })
            }
            if (isCounty) {
                let data = {
                    name: v.name,
                    stateDto: {
                        id: v.stateId
                    }
                }
                if (currentCounty) {
                    data = {...data, id: currentCounty.id}
                }
                dispatch(zpActions.saveOrEditCounty(data)).then(() => {
                    dispatch(zpActions.getAllCounties({page: 0, size: 10}))
                    dispatch({type: types.OPEN_CLOSE_MODAL})
                })
            }
            if (isZipCode && !isAddAgent) {
                let data = {
                    city: v.name,
                    code: v.code,
                    active: v.active,
                    countyId: v.countyId,
                    timezoneNameId: v.timezoneNameId
                }
                if (currentZipCode2) {
                    data = {...data, id: currentZipCode2.id}
                }
                dispatch(zpActions.saveOrEditZipCode(data)).then(() => {
                    getCurrentZipCode(currentZipCode2.id)
                    dispatch(zpActions.allStates2())

                })
                dispatch({
                    type: 'updateState',
                    payload: {
                        showModal: !showModal
                    }
                })
            }
            if (isZipCode && showCurrentZipCode && isAddAgent) {
                dispatch(zpActions.addAgentToZipCode({zcId: currentZipCode2.id, agId: v.agentId})).then(res => {
                    getCurrentZipCode(currentZipCode2.id)
                    dispatch({
                        type: 'updateState', payload: {
                            showModal: !showModal,
                            agentList: [],
                            isAddAgent: false
                        }
                    })
                })
            }
        }
        const removeItem = () => {
            if (removingType === 'zipCode') {
                dispatch(zpActions.deleteZipCode({id: removingItemId})).then(res => {
                    dispatch(zpActions.allStates2())
                    dispatch({
                        type: "updateState", payload: {
                            showCurrentZipCode: !showCurrentZipCode
                        }
                    })
                })
            }
            if (removingType === 'county') {
                dispatch(zpActions.deleteCounty({id: removingItemId})).then(res => {
                    dispatch(zpActions.getAllCounties({page: 0, size: 10}))
                })

            }
            if (removingType === 'state') {
                dispatch(zpActions.deleteState({id: removingItemId})).then(res => {
                    dispatch(zpActions.getAllStatesByPageable({page: 0, size: 10}))
                })
            }
            if (removingType === 'agent') {
                dispatch(zpActions.deleteAgentByZipCode({zcId: removingItemId, agId: removingAgentId})).then(res => {
                    getCurrentZipCode(removingItemId)
                })
            }
            dispatch({
                type: "updateState", payload: {
                    showDeleteModal: !showDeleteModal
                }
            })
        }
        const changeActive = () => {
            dispatch({
                type: "updateState",
                payload: {
                    active: !active,
                },
            });
        };
        const changeZipCodeActive = (e, id) => {
            dispatch(zpActions.changeActiveOfZipCode({id: id})).then(res => {
                getCurrentZipCode(id)
            })
        }
        const getCountyBySelectedState = (e) => {
            let stateId = e.target.value
            let data = {
                id: stateId
            }
            dispatch({
                type: "updateState", payload: {
                    tempSelectedStateId: stateId
                }
            })
            dispatch(zpActions.getCountyBySelectedState(data))
        }
        const getCurrentZipCode = (id) => {
            dispatch(zpActions.getCurrentZipCode({id: id}))
        }
        const backFromZipCode = () => {
            dispatch({
                type: "updateState", payload: {

                    showCurrentZipCode: !showCurrentZipCode,
                    currentZipCode2: ""
                }
            })
        }
        const addAgentToZipCode = (id) => {
            // console.log(id,"adsfasdfasdf")
            // dispatch({type: types.OPEN_CLOSE_MODAL})
            dispatch(zpActions.getAgentsByZipCode({zcId: id}))
            dispatch({
                type: 'updateState', payload: {
                    showModal: !showModal,
                    isAddAgent: true
                }
            })
        }
        return (
            <AdminLayout pathname={this.props.location.pathname}>
                <div className="admin-zip-codes-page flex-column">
                    {isZipCode && showCurrentZipCode ? "" : <Row className="feedback-admin-page">
                        <Col md={3} className="">
                            <div>
                                <h3>{isZipCode ? "Zip Code" : isCounty ? "County" : isState ? "State" : ''}</h3>
                            </div>
                        </Col>
                        <Col md={12}>
                            <div className="route-services ml-auto">
                                <div onClick={() => changeTabePane('zipCode')} className={!isZipCode ?
                                    "online" : "router-active"} style={{fontSize: '13px'}}>Zip Code
                                </div>
                                <div onClick={() => changeTabePane('county')} className={!isCounty ?
                                    "in-person" : "router-active"}>Country
                                </div>
                                <div onClick={() => changeTabePane('state')} className={!isState ?
                                    "in-person" : "router-active"}>State
                                </div>
                            </div>
                        </Col>
                    </Row>}
                    <div className="state-section flex-column">
                        <div>
                            {isZipCode && showCurrentZipCode ? "" :
                                <div className="flex-row align-items-center button-container">
                                    {permissions && permissions.some(item => (item.permissionName === "ADD_ZIP_CODE" && isZipCode) ||
                                        (item.permissionName === "SAVE_STATE" && isState) || (item.permissionName === "SAVE_COUNTY" && isCounty)) ?
                                        <AddIcon
                                            onClick={openModal}
                                            title={false}
                                        /> : ""}
                                    <span
                                        className="dark-title ml-3">
                                        {permissions && permissions.some(item => item.permissionName === "ADD_ZIP_CODE") && isZipCode ? "Add new zip code" : ""}
                                        {permissions && permissions.some(item => item.permissionName === "SAVE_STATE") && isState ? "Add new state" : ""}
                                        {permissions && permissions.some(item => item.permissionName === "SAVE_COUNTY") && isCounty ? "Add new county" : ""}
                                        {/*Add new {*/}
                                        {/*isZipCode ? "zip code" :*/}
                                        {/*    isCounty ? "county" :*/}
                                        {/*        isState ? "state" : ''}*/}
                                    </span>
                                </div>}
                            {!isZipCode ?
                                <Table className="custom-table zipCodeTable-style">
                                    <thead>
                                    {
                                        isCounty ?
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>State</th>
                                                {permissions && permissions.some(item => item.permissionName === "SAVE_COUNTY" || item.permissionName === "DELETE_COUNTY") ?
                                                    <th className="ml-5" colSpan="2">
                                                        Operation
                                                    </th>
                                                    : ""}
                                            </tr>
                                            :
                                            isState ?
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    {permissions && permissions.some(item => item.permissionName === "SAVE_STATE" || item.permissionName === "DELETE_STATE") ?
                                                        <th className="ml-5" colSpan="2">
                                                            Operation
                                                        </th> : ""}
                                                </tr>
                                                :
                                                ''
                                    }
                                    </thead>
                                    {
                                        isCounty && countyArray ?

                                            <tbody>
                                            {countyArray.map((item, index) =>
                                                <tr>
                                                    <td>{index + 1}</td>
                                                    <td>{item.name}</td>
                                                    <td>{item.stateDto.name}</td>
                                                    {permissions && permissions.some(item => item.permissionName === "SAVE_COUNTY") ?
                                                        <td>
                                                            <EditIcon
                                                                onClick={() => openEditModal(item, "county")}
                                                            />
                                                        </td> : ""}
                                                    {permissions && permissions.some(item => item.permissionName === "DELETE_COUNTY") ?
                                                        <td>
                                                            <DeleteIcon
                                                                onClick={() =>
                                                                    openDeleteModal(item.id, "county", '')
                                                                }
                                                            />
                                                        </td> : ""}
                                                </tr>
                                            )}
                                            </tbody>
                                            :
                                            isState && stateArrayPageable ?
                                                <tbody>
                                                {stateArrayPageable.map((item, index) =>
                                                    <tr>
                                                        <td>{((index + 1) + (activePage * 10))}</td>
                                                        <td>{item.name}</td>
                                                        {permissions && permissions.some(item => item.permissionName === "SAVE_STATE") ?
                                                            <td>
                                                                <EditIcon
                                                                    onClick={() => openEditModal(item, "state")}
                                                                />
                                                            </td> : ""}
                                                        {permissions && permissions.some(item => item.permissionName === "DELETE_STATE") ?
                                                            <td>
                                                                <DeleteIcon
                                                                    onClick={() =>
                                                                        openDeleteModal(item.id, "state", '')
                                                                    }
                                                                />
                                                            </td> : ""}
                                                    </tr>
                                                )}
                                                </tbody>
                                                :
                                                ''
                                    }

                                </Table> : ""}

                            {!isZipCode ?
                                <div className="w-100">
                                    <Pagination
                                        activePage={activePage + 1}
                                        itemsCountPerPage={10}
                                        totalItemsCount={isCounty ? totalCountyElements : isState ? totalStateElements : 0}
                                        pageRangeDisplayed={5}
                                        onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                                        linkClass="page-link"
                                    />
                                </div>
                                : ""}
                            {
                                isZipCode && !showCurrentZipCode ? <div>
                                    <div className="admin-zip-codes-page flex-column container">
                                        {statesWithZipCodes ? statesWithZipCodes.map((item, index) =>

                                            <div className="state-section flex-column" key={index}>
                                                <span className="main-title">{item.name}</span>
                                                <div className="flex-row flex-wrap">
                                                    {
                                                        item.zipCodeDtoList ? item.zipCodeDtoList.map((item2, index2) =>
                                                            <div key={index2}
                                                                 className="single-zip-code-section flex-row space-between">
                                                                <span className="zip-code-text">{item2.code}</span>
                                                                <ThreeDotIcon
                                                                    onClick={() => getCurrentZipCode(item2.id)}/>
                                                            </div>
                                                        ) : ""
                                                    }
                                                </div>
                                            </div>
                                        ) : ""}
                                    </div>
                                </div> : ""
                            }
                            {
                                isZipCode && showCurrentZipCode ? <div>
                                    {currentZipCode2 ?
                                        <div className="admin-zip-codes-page-open flex-column">
                                            <div className="zipcode-agents-about flex-row space-between">
                                                <div className="flex-row align-items-center">
                                                    <span>{currentZipCode2.countyDto.stateDto.name + ' - ' + currentZipCode2.code} </span>
                                                    <span className='ml-5 mr-5'>
                                                        <label className={'labelDuo'}>
                                                            <div className={'labelBlock'}>
                                                                {currentZipCode2.active && <div className={'labelBox'}>
                                                                    <i className="fas fa-check"></i>
                                                                </div>}
                                                            </div>
                                                            <input
                                                                className={'d-none'}
                                                                type="checkbox"
                                                                onChange={(e) => changeZipCodeActive(e, currentZipCode2.id)}
                                                                checked={currentZipCode2.active}
                                                            />
                                                        </label>
                                                    </span>
                                                    <EditIcon className="margin-left-35"
                                                              onClick={() => openEditModal(currentZipCode2, 'zipCode')}/>
                                                    <DeleteIcon className="margin-left-15"
                                                                onClick={() => openDeleteModal(currentZipCode2.id, 'zipCode', '')}/>

                                                </div>
                                                <CloseIcon onClick={backFromZipCode}/>

                                            </div>
                                            <div className="flex-column margin-top-25">
                                                <span className="main-title">Work hours</span>
                                                <div className="flex-row margin-top-15">
                                                    {currentZipCode2.workHours ? currentZipCode2.workHours.map((item3, index3) =>
                                                        <div className="half-left inner-section flex-row space-between">
                                                            <span className="dark-title">{item3.name}</span>
                                                            <span
                                                                className="time-text">{item3.startTime} - {item3.endTime}</span>
                                                        </div>
                                                    ) : ""}
                                                </div>
                                            </div>
                                            {currentZipCode2.discount ?
                                                <div className="flex-column margin-top-25">
                                                    <span className="main-title">Discount</span>
                                                    <div className="flex-row margin-top15">
                                                        <div className="half-left inner-section flex-row space-between">
                                                            <span className="dark-title">Discount</span>
                                                            <span
                                                                className="time-text">+{currentZipCode2.discount}%</span>
                                                        </div>
                                                    </div>
                                                </div> : ""}
                                            <div className="flex-column margin-top-25">
                                                <div className="flex-row">
                                                    <span className="main-title">Agnets  </span>
                                                    <div className="ml-5">
                                                        <AgentAddIcon
                                                            onClick={() => addAgentToZipCode(currentZipCode2.id)}/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="flex-column">
                                                {currentZipCode2.agents ? currentZipCode2.agents.map((item4, index4) =>
                                                    <div>
                                                        <div className="flex-row agent-block">
                                                            <Col md={3}>
                                                                <div className="d-flex">
                                                                    <div className="avatar-img  agent-avatar">
                                                                        <img className="img-fluid"
                                                                             src={item4.imgUrl ? item4.imgUrl : "/assets/img/avatar.png"}
                                                                             alt="avatar"/>
                                                                    </div>
                                                                    <div className="ml-3">
                                                                        <div className="fs-15">Agent</div>
                                                                        <div className="fs-20-bold">
                                                                            {item4.fullName}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </Col>
                                                            <Col md={1}>
                                                                <div className="border-line"></div>
                                                            </Col>
                                                            <Col md={3}>
                                                                <div className="fs-15">Location</div>
                                                                <div className="fs-15-bold pt-2">
                                                                    {item4.location}
                                                                </div>
                                                            </Col>
                                                            <Col md={1}>
                                                                <div className="border-line"></div>
                                                            </Col>
                                                            <Col md={1}>
                                                                <div style={{fontSize: "12px"}}>Online time</div>
                                                                <div className="fs-15-bold pt-2">
                                                                    {item4.onlineTime}
                                                                </div>
                                                            </Col>
                                                            <Col md={1}>
                                                                <div className="border-line"></div>
                                                            </Col>
                                                            <Col md={1}>
                                                                <div className="fs-15">Orders</div>
                                                                <div className="fs-15-bold pt-2">
                                                                    {item4.ordersCount}
                                                                </div>
                                                            </Col>
                                                            <Col md={1}>
                                                                <DeleteIcon
                                                                    onClick={() => openDeleteModal(currentZipCode2.id, 'agent', item4.id)}/>
                                                            </Col>
                                                        </div>
                                                    </div>) : ""}
                                            </div>


                                        </div> : ""}
                                </div> : ""
                            }

                            <Modal isOpen={showModal} toggle={openModalCancel} id="allModalStyle">
                                <AvForm onValidSubmit={saveItem}>
                                    <div className='position-absolute' style={{top: '45px', left: '-12%'}}>
                                        <CloseBtn click={openModalCancel}/>
                                    </div>
                                    <ModalHeader
                                        toggle={openModalCancel}
                                        charCode="x"
                                        className="model-head"
                                    >
                                        {currentZipCode || currentCounty || currentState ?
                                            currentZipCode ? "Edit Zip Code"
                                                : currentCounty ? "Edit County"
                                                : currentState ? "Edit State"
                                                    : ''
                                            : isZipCode && !showCurrentZipCode ? "Add Zip Code"
                                                : isCounty ? "Add County"
                                                    : isState ? "Add State"
                                                        : isZipCode && showCurrentZipCode ? "Add agent to zip code" : ''}
                                    </ModalHeader>
                                    <ModalBody>
                                        {isZipCode && !isAddAgent ?
                                            <div>
                                                {stateArray ?
                                                    <AvField type="select" name="stateId"
                                                             onChange={getCountyBySelectedState}
                                                             value={currentZipCode2 ? currentZipCode2.countyDto.stateDto ? currentZipCode2.countyDto.stateDto.id : tempSelectedStateId : tempSelectedStateId}
                                                             required>
                                                        <option value="" disabled>Select State</option>
                                                        {stateArray.map(item =>
                                                            <option key={item.id} value={item.id}>{item.name}</option>
                                                        )}
                                                    </AvField> : 'State not found'
                                                }
                                                <AvField type="select" name="countyId"
                                                         value={currentZipCode2 ? currentZipCode2.countyDto ? currentZipCode2.countyDto.id : "0" : "0"}
                                                         required>
                                                    <option value="0" disabled>Select County</option>
                                                    {countyArray ? countyArray.map(item =>
                                                        <option key={item.id} value={item.id}>{item.name}</option>
                                                    ) : ''}
                                                </AvField>
                                                <AvField
                                                    name="name"
                                                    label="Name"
                                                    defaultValue={
                                                        currentZipCode2 ? currentZipCode2.city
                                                            : ""
                                                    }
                                                    placeholder="Enter Name"
                                                />
                                                <AvField
                                                    type="number"
                                                    name="code"
                                                    label="Zip code"
                                                    required
                                                    defaultValue={
                                                        currentZipCode2 ? currentZipCode2.code
                                                            : ""
                                                    }
                                                    placeholder="Enter zip code"
                                                />
                                                {timezoneName ?
                                                    <AvField type="select" name="timezoneNameId"
                                                             required>
                                                        <option value="" disabled>Select Timezone</option>
                                                        {timezoneName.map(item =>
                                                            <option key={item.id} value={item.id}>{item.name}</option>
                                                        )}
                                                    </AvField> : 'State not found'
                                                }
                                                {currentZipCode2 ?
                                                    ''
                                                    :
                                                    <div className='d-flex justify-content-center'>
                                                        <label className={'labelDuo'}>
                                                            <div className={'labelBlock'}>
                                                                {active ?
                                                                    <div className={'labelBox'}>
                                                                        <i className="fas fa-check"/>
                                                                    </div> : ''}
                                                            </div>
                                                            <CustomInput
                                                                className={'d-none'}
                                                                type="checkbox"
                                                                name="active"
                                                                checked={active}
                                                                onChange={changeActive}
                                                            />
                                                            Active
                                                        </label>
                                                    </div>
                                                }
                                            </div>
                                            : isCounty ?
                                                <div>
                                                    <AvField
                                                        name="name"
                                                        label="Name"
                                                        required
                                                        defaultValue={
                                                            currentCounty ? currentCounty.name
                                                                : ""
                                                        }
                                                        placeholder="Enter name"
                                                    />
                                                    {stateArray ?
                                                        <AvField type="select" name="stateId"
                                                                 value={currentCounty ? currentCounty.stateDto.id : ""}
                                                                 required
                                                        >
                                                            <option value='' disabled>Select State</option>
                                                            {stateArray.map(item =>
                                                                <option key={item.id}
                                                                        value={item.id}>{item.name}</option>
                                                            )}
                                                        </AvField> : 'State not found'
                                                    }
                                                </div>
                                                : isState ?
                                                    <div>
                                                        <AvField
                                                            name="name"
                                                            label="Name"
                                                            required
                                                            defaultValue={
                                                                currentState ? currentState.name
                                                                    : ""
                                                            }
                                                            placeholder="Enter name"
                                                        />
                                                    </div>
                                                    :
                                                    isZipCode && showCurrentZipCode && isAddAgent ?
                                                        <div>
                                                            {agentList ?
                                                                <AvField type="select" name="agentId"
                                                                    // onChange={getCountyBySelectedState}
                                                                         required>
                                                                    <option value="" disabled>Select Agent</option>
                                                                    {agentList.map(item =>
                                                                        <option key={item.id}
                                                                                value={item.id}>{item.fullName + " " + item.phoneNumber + " " + item.email}</option>
                                                                    )}
                                                                </AvField> : 'Agent not found'
                                                            }
                                                        </div>
                                                        : ''
                                        }
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button color="info" outline type="submit">Save</Button>
                                    </ModalFooter>
                                </AvForm>
                            </Modal>

                            <Modal isOpen={showDeleteModal} toggle={openCloseDeleteModal}>
                                <ModalHeader
                                    toggle={openCloseDeleteModal}
                                    charCode="x"
                                    className="model-head"
                                >
                                    Do you want delete {removingType}?
                                </ModalHeader>
                                <ModalBody style={{backgroundColor: "#F9F9F9"}}>

                                </ModalBody>
                                <ModalFooter>
                                    <Button
                                        type="button"
                                        color="secondary"
                                        outline
                                        onClick={openCloseDeleteModal}
                                    >
                                        Cancel
                                    </Button>{" "}
                                    <Button color="success" type="button" onClick={removeItem}>Delete</Button>
                                </ModalFooter>
                            </Modal>
                        </div>
                    </div>
                </div>
            </AdminLayout>
        );
    }
}

export default connect(({
                            auth: {permissions},
                            zipCode: {
                                active,
                                isZipCode,
                                isCounty,
                                isState,
                                loading,
                                zipCodeArray,
                                countyArray,
                                stateArray,
                                stateArrayPageable,
                                timezoneName,
                                currentZipCode,
                                currentCounty,
                                currentState,
                                showModal,
                                showDeleteModal,
                                removingType,
                                removingItemId,
                                zipCodeActiveState,
                                totalZipCodeElements,
                                totalCountyElements,
                                totalStateElements,
                                activePage,
                                statesWithZipCodes,
                                showCurrentZipCode,
                                currentZipCode2,
                                removingAgentId,
                                agentList,
                                isAddAgent,
                                tempSelectedStateId
                            }
                        }) => ({
    active,
    permissions, timezoneName,
    isZipCode,
    isCounty,
    isState,
    loading,
    zipCodeArray,
    countyArray,
    stateArray,
    stateArrayPageable,
    currentZipCode,
    currentCounty,
    currentState,
    showModal,
    showDeleteModal,
    removingType,
    removingItemId,
    zipCodeActiveState,
    totalZipCodeElements,
    totalCountyElements,
    totalStateElements,
    activePage,
    statesWithZipCodes,
    showCurrentZipCode,
    currentZipCode2,
    removingAgentId,
    agentList,
    isAddAgent,
    tempSelectedStateId
}))(ZipCodes);

function EditIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{cursor: "pointer"}}
                width="18"
                height="18"
                viewBox="0 0 18 18"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M12.728 6.68599L11.314 5.27199L2 14.586V16H3.414L12.728 6.68599ZM14.142 5.27199L15.556 3.85799L14.142 2.44399L12.728 3.85799L14.142 5.27199ZM4.242 18H0V13.757L13.435 0.321992C13.6225 0.134521 13.8768 0.0292053 14.142 0.0292053C14.4072 0.0292053 14.6615 0.134521 14.849 0.321992L17.678 3.15099C17.8655 3.33852 17.9708 3.59283 17.9708 3.85799C17.9708 4.12316 17.8655 4.37746 17.678 4.56499L4.243 18H4.242Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}

function DeleteIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{cursor: "pointer"}}
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M2 6H18V19C18 19.2652 17.8946 19.5196 17.7071 19.7071C17.5196 19.8946 17.2652 20 17 20H3C2.73478 20 2.48043 19.8946 2.29289 19.7071C2.10536 19.5196 2 19.2652 2 19V6ZM4 8V18H16V8H4ZM7 10H9V16H7V10ZM11 10H13V16H11V10ZM5 3V1C5 0.734784 5.10536 0.48043 5.29289 0.292893C5.48043 0.105357 5.73478 0 6 0H14C14.2652 0 14.5196 0.105357 14.7071 0.292893C14.8946 0.48043 15 0.734784 15 1V3H20V5H0V3H5ZM7 2V3H13V2H7Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}

function ThreeDotIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                width="18"
                height="4"
                viewBox="0 0 18 4"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M1.5 0.5C0.675 0.5 0 1.175 0 2C0 2.825 0.675 3.5 1.5 3.5C2.325 3.5 3 2.825 3 2C3 1.175 2.325 0.5 1.5 0.5ZM16.5 0.5C15.675 0.5 15 1.175 15 2C15 2.825 15.675 3.5 16.5 3.5C17.325 3.5 18 2.825 18 2C18 1.175 17.325 0.5 16.5 0.5ZM9 0.5C8.175 0.5 7.5 1.175 7.5 2C7.5 2.825 8.175 3.5 9 3.5C9.825 3.5 10.5 2.825 10.5 2C10.5 1.175 9.825 0.5 9 0.5Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}

function AgentAddIcon(props) {
    return (
        <div className={props.className} onClick={props.onClick}>
            <svg
                width="19"
                height="21"
                viewBox="0 0 19 21"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M10 13.252V15.342C9.09492 15.022 8.12628 14.9239 7.1754 15.0558C6.22453 15.1877 5.3192 15.5459 4.53543 16.1002C3.75166 16.6545 3.11234 17.3888 2.67116 18.2414C2.22998 19.094 1.99982 20.04 2 21L2.58457e-07 20.999C-0.000310114 19.7779 0.278921 18.5729 0.816299 17.4764C1.35368 16.3799 2.13494 15.4209 3.10022 14.673C4.0655 13.9251 5.18918 13.4081 6.38515 13.1616C7.58113 12.9152 8.81766 12.9457 10 13.251V13.252ZM8 12C4.685 12 2 9.315 2 6C2 2.685 4.685 0 8 0C11.315 0 14 2.685 14 6C14 9.315 11.315 12 8 12ZM8 10C10.21 10 12 8.21 12 6C12 3.79 10.21 2 8 2C5.79 2 4 3.79 4 6C4 8.21 5.79 10 8 10ZM14 16V13H16V16H19V18H16V21H14V18H11V16H14Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}

function CloseIcon(props) {
    return <div className={props.className} onClick={props.onClick}>
        <svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="25" cy="25" r="25" fill="white"/>
            <path
                d="M24.955 23.1875L31.1425 17L32.91 18.7675L26.7225 24.955L32.91 31.1425L31.1425 32.91L24.955 26.7225L18.7675 32.91L17 31.1425L23.1875 24.955L17 18.7675L18.7675 17L24.955 23.1875Z"
                fill="#313E47"/>
        </svg>

    </div>
}
