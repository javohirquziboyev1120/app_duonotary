import React from "react";
import { ModalHeader, Modal, ModalBody, ModalFooter, Button } from "reactstrap";
import styled from "styled-components";
import { AvForm } from "availity-reactstrap-validation";
import AvField from "availity-reactstrap-validation/lib/AvField";
import { useDispatch } from "react-redux";
import { uploadFile } from "../../../redux/actions/AttachmentAction";
import { addPartners } from "../../../redux/actions/AppAction";
import { useState } from "react";

export const UniversalModal = ({
  InnerBodyComponent,
  InnerFooterComponent,
  onToggle,
  visiblity,
  className = "",
}) => (
  <Modal
    isOpen={visiblity}
    toggle={onToggle}
    className={className}
    backdrop={true}>
    <ModalHeader toggle={onToggle}>Add Partners</ModalHeader>
    <ModalBody>
      <InnerBodyComponent />
    </ModalBody>
    <ModalFooter>
      {InnerFooterComponent && <InnerFooterComponent />}
    </ModalFooter>
  </Modal>
);

// rasmning key orqali id olish uchun
const PARTNER = "partner";

export const ModalBodyForAdd = ({ attach, data }) => {
  const [attech, setAttech] = useState(null);
  const dispatch = useDispatch();

  const handleAddPartner = (e, v) => {
    v.attachment = {
      id: attach.filter((item) => item.key === PARTNER)[0].value,
    };
    dispatch(addPartners(v));
  };
  return (
    <AvForm onValidSubmit={handleAddPartner}>
      <AvField
        type='file'
        name='attech'
        onChange={(a) => {
          dispatch(uploadFile(a.target.files[0], PARTNER));
        }}
      />
      <AvField name='name' placeholder='enter name' label='Name' required />
      <AvField
        name='description'
        placeholder='enter description'
        label='Description'
      />
      <Button type='submit'>Jonatish</Button>
    </AvForm>
  );
};

export const ModalFooterForAdd = ({ onClick }) => (
  <>
    <Button color='primary' onClick={onClick}>
      Do Something
    </Button>
    <Button color='secondary' onClick={onClick}>
      Cancel
    </Button>
  </>
);

export const IconWrap = styled.div`
  cursor: pointer;
  padding: 0 0.5rem;
  &:active {
    transform: scale(0.9);
    transition: 0.25s all ease;
  }
`;

export const ActionWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  max-width: 80%;
`;
