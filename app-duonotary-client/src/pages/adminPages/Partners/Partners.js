import React, { Component } from "react";

import { connect } from "react-redux";
import {
    addPartners,
    deletePartners,
    editPartners,
    getCountryList,
    getPartners,
} from "../../../redux/actions/AppAction";
import AdminLayout from "../../../components/AdminLayout";
import Pagination from "react-js-pagination";
import { AddIcon } from "../../../components/Icons";
import styled from "styled-components";
import { BsEye } from "react-icons/bs";

import {
    ActionWrapper,
    IconWrap,
    ModalBodyForAdd,
    ModalFooterForAdd,
    UniversalModal,
} from "./Partners.elements";
import PartnerModal from "../../../components/Modal/PartnerModal";
import PhotoModal from "../../../components/Modal/PhotoModal";
import DeleteModal from "../../../components/Modal/DeleteModal";
import { toast } from "react-toastify";
import { EditIcon } from "../Service";
import { DeleteIcon } from "../AdminOrder";

class Partners extends Component {
    state = {
        fakeData: [],
        showModal: false,
        showDeleteModal: false,
        currentItem: "",
        photoModal: false,
        photoId: "",
        activeModal: false,
    };

    componentDidMount() {
        this.props.dispatch(getPartners());
    }

    handleEdit = (data) => {
        this.setState({ currentItem: data, showModal: !this.state.showModal });
    };
    handleDelete = (data) => {
        this.setState({
            currentItem: data,
            showDeleteModal: !this.state.showDeleteModal,
        });
    };
    handleActive = (data) => {
        this.setState({ currentItem: data, activeModal: !this.state.activeModal });
    };
    handleView = (id) => {
        console.log(id);
        this.setState({ photoModal: !this.state.photoModal, photoId: id });
    };

    toggleModal = () => {
        this.setState(({ showModal }) => ({
            showModal: !showModal,
        }));
    };
    addPartners = (e, v) => {
        let photo = this.props.attachmentIdsArray.filter(
            (item) => item.key === "partner"
        )[0];
        if (!photo) {
            toast.error("select logo");
            return "";
        }
        v.attachment = { id: photo.value };
        this.props.dispatch(addPartners(v));
        this.toggleModal();
    };
    editPartners = (e, v) => {
        v.id = this.state.currentItem.id;
        let photo = this.props.attachmentIdsArray.filter(
            (item) => item.key === "partner"
        )[0];
        v.attachment = {
            id: photo ? photo.value : this.state.currentItem.attachmentId,
        };
        this.props.dispatch(editPartners(v));
        this.handleEdit();
    };
    deletePartners = () => {
        this.props.dispatch(deletePartners(this.state.currentItem.id));
        this.handleDelete();
    };
    changeActivePartners = () => {
        let obj = { ...this.state.currentItem };
        obj.attachment = { id: obj.attachmentId };
        obj.active = !this.state.currentItem.active;
        this.props.dispatch(editPartners(obj));
        this.handleActive();
    };

    render() {
        const { partners, dispatch } = this.props;
        const {
            showModal,
            showDeleteModal,
            currentItem,
            photoModal,
            photoId,
            activeModal,
        } = this.state;
        return (
            <AdminLayout pathname={this.props.location.pathname}>
                <div className='country-page'>
                    <h2 className='text-center'>Partners list</h2>
                    <AddIcon title={true} onClick={this.toggleModal} />
                    <table className='table custom-table'>
                        <thead>
                        <tr>
                            <th scope='col'>#</th>
                            <th scope='col'>Name</th>
                            <th scope='col'>Active</th>
                            <th scope='col'>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {partners &&
                        partners.map((data, i) => (
                            <tr key={data.id}>
                                <td>{i + 1}</td>
                                <td>{data.name}</td>
                                <td>
                                    <label className='labelDuo'>
                                        <div className='labelBlock'>
                                            {data.active && (
                                                <div className='labelBox'>
                                                    <i className='fas fa-check' />
                                                </div>
                                            )}
                                        </div>
                                        <input
                                            className='d-none'
                                            type='checkbox'
                                            onChange={() => this.handleActive(data)}
                                            checked={data.active}
                                        />
                                    </label>
                                </td>
                                <td>
                                    <ActionWrapper>
                                        <IconBtn
                                            onClick={() => this.handleView(data.attachmentId)}
                                            Icon={BsEye}
                                        />
                                        <EditIcon
                                            className='icon-edit-partners mr-3'
                                            onClick={() => this.handleEdit(data)}
                                        />
                                        <DeleteIcon
                                            className='icon-delete-partners'
                                            onClick={() => this.handleDelete(data)}
                                        />
                                    </ActionWrapper>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                    {showModal && (
                        <PartnerModal
                            cancel={this.toggleModal}
                            data={currentItem}
                            dispatch={dispatch}
                            showModal={showModal}
                            submit={currentItem ? this.editPartners : this.addPartners}
                        />
                    )}
                    {photoModal && (
                        <PhotoModal
                            showModal={photoModal}
                            cancel={this.handleView}
                            id={photoId}
                        />
                    )}
                    {showDeleteModal && (
                        <DeleteModal
                            cancel={this.handleDelete}
                            confirm={this.deletePartners}
                            showDeleteModal={showDeleteModal}
                        />
                    )}
                    {activeModal && (
                        <DeleteModal
                            showDeleteModal={activeModal}
                            confirm={this.changeActivePartners}
                            cancel={this.handleActive}
                        />
                    )}
                </div>
            </AdminLayout>
        );
    }
}

export default connect(
    ({ app: { partners }, attachment: { attachmentIdsArray } }) => ({
        partners,
        attachmentIdsArray,
    })
)(Partners);

const IconBtn = ({ onClick, Icon }) => {
    return (
        <IconWrap style={{ margin: "0 .5rem" }} onClick={onClick}>
            <Icon size={25} />
        </IconWrap>
    );
};