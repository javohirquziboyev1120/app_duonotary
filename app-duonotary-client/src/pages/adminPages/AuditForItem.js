import React, {Component} from 'react';
import AdminLayout from "../../components/AdminLayout";
import {
    getAuditForItemWithCommitId,
    getMainServiceList,
    getOneItemAudit,
    getTotalElementsCount, getUserForAudit,
} from "../../redux/actions/AppAction";
import {
    Button,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table
} from "reactstrap";
import {RiArrowGoBackLine} from "react-icons/ri"
import {connect} from "react-redux";
import Moment from "moment";
import {isObject} from "reactstrap/es/utils";
import PaginationComponent from "react-reactstrap-pagination";
import {Link} from "react-router-dom";

class AuditForItem extends Component {
    componentDidMount() {
        if (this.props.match.params.id) {
            this.props.dispatch(getMainServiceList());
            this.props.dispatch(getOneItemAudit({id: this.props.match.params.id, page: 0, size: 20}));
            this.props.dispatch({
                type: "updateState",
                payload: {
                    page: 0,
                    size: 0,
                    totalElements: 0
                }
            })
            this.props.dispatch(getTotalElementsCount({id: this.props.match.params.id}))

        }
    }

    render() {
        const {
            isSuperAdmin,
            mainServices,
            dispatch, histories,
            currentAuditItemUser,
            showModal, currentItem, page, size, totalElements,
            currentAuditItems,
        } = this.props;
        const openModal = (item) => {
            if (showModal) {
                dispatch({
                    type: 'updateState',
                    payload: {
                        currentItem: null,
                        currentAuditItems: null,
                        showModal: false
                    }
                })
            } else {
                let data = {}
                if (item && item.commitMetadata && item.commitMetadata.author && item.globalId && item.globalId.entity && item.commitMetadata.id) {
                    data.tableName = item.globalId.entity.substring(item.globalId.entity.lastIndexOf('.') + 1);
                    data.tableItemId = item.globalId.cdoId
                    data.commitId = item.commitMetadata.id
                    dispatch(getUserForAudit({id: item.commitMetadata.author}));
                }
                if (data && data.tableName && data.tableItemId) {
                    dispatch(getAuditForItemWithCommitId({...data}));
                }
                dispatch({
                    type: 'updateState',
                    payload: {
                        currentItem: item
                    }
                })

            }
        };
        const handleSelected = (selectedPage) => {
            dispatch(getOneItemAudit({id: this.props.match.params.id, page: selectedPage - 1, size: 20}))
            dispatch(
                {
                    type: 'updateState',
                    payload: {
                        page: selectedPage - 1
                    }
                }
            )
        }
        return (
            <AdminLayout pathname={this.props.location.pathname}>
                {isSuperAdmin ?

                    <div className="main-service-page container">
                        <h2 className="text-center">Operation history</h2>
                        <Link to="/admin/audit">
                            <Button className="btn btn-secondary my-3">
                                <RiArrowGoBackLine/> Back
                            </Button>
                        </Link>

                        <Table className="custom-table">
                            <thead>
                            <tr>
                                <th className="dark-title">#</th>
                                <th className="dark-title">Table Name</th>
                                <th className="dark-title">Operation</th>
                                <th className="dark-title">Version</th>
                                <th className="dark-title">Time</th>
                                <th className="dark-title" colSpan="2">What did?</th>
                            </tr>
                            </thead>

                            {histories && histories.length > 0 ? (
                                <tbody>
                                {histories.map((item, i) =>
                                    <tr className="block" key={item.id}>
                                        <td className="dark-title">{page > 0 ? ((page * size) + i + 1) : (i + 1)}</td>
                                        <td className="dark-title">{item.globalId ? item.globalId.entity.substring(item.globalId.entity.lastIndexOf('.') + 1) : ""}</td>
                                        <td className="dark-title">{item.type}</td>
                                        <td className="dark-title">{item.version}</td>
                                        <td className="dark-title">{Moment(item.commitMetadata.commitDate).format('hh:MM a, D MMM, YYYY')}</td>
                                        <td className="dark-title"><Button onClick={() => openModal(item)}>Show</Button>
                                        </td>
                                    </tr>
                                )}
                                </tbody>
                            ) : (
                                <tbody>
                                <tr>
                                    <td colSpan="4">
                                        <h3 className="text-center mx-auto">
                                            {" "}
                                            No information{" "}
                                        </h3>
                                    </td>
                                </tr>
                                </tbody>
                            )}
                        </Table>
                        <Modal id="allModalStyle" size="lg" isOpen={showModal}>
                            <ModalHeader>What action</ModalHeader>
                            <ModalBody>
                                {currentAuditItems && currentItem ?
                                    <div>
                                        <p><b>Changes this table</b>
                                            - {currentItem.globalId ? currentItem.globalId.entity.substring(currentItem.globalId.entity.lastIndexOf('.') + 1) : ""}
                                        </p>
                                        <p><b>Change type</b> - {currentItem.type}</p>
                                        <p><b>Version -</b> {currentItem.version}</p>
                                        <p><b>Time</b> -
                                            {Moment(currentItem.commitMetadata.commitDate).format('hh:MM a, D MMM, YYYY')}
                                        </p>
                                        <p>
                                            <b>Changed
                                                author</b> - {currentAuditItemUser ? (currentAuditItemUser.firstName + " " + currentAuditItemUser.lastName) : ""}
                                        </p>
                                        <strong>Changes: </strong>
                                        {currentAuditItems.map((item, i) =>
                                            item.property === "updatedBy" ? "" : item.property === "mainService" ?
                                                <div>
                                                    - <strong>"{" Main service "}"</strong> value changed
                                                    from
                                                    <b>
                                                        {" \""} {mainServices.map(m => item.left.cdoId === m.id && m.name)}{"\"  "}
                                                    </b>
                                                    to <b>{"  \""} {mainServices.map(m => item.right.cdoId === m.id && m.name)}{"\" "} </b>
                                                </div>
                                                :
                                                ("" + item.property) === "fromTime" || ("" + item.property) === "tillTime" ?
                                                    <div>
                                                        - <strong>'{item.property === "fromTime" ? "From time " : "Till time "}'</strong> value
                                                        changed from
                                                        <b>
                                                            {" " + Moment(Moment(new Date()).format("YYYY") + " " + item.left + "").format('hh:mm A')}
                                                        </b> to
                                                        <b>
                                                            {" " + Moment(Moment(new Date()).format("YYYY") + " " + item.right + "").format('hh:mm A')}
                                                        </b>
                                                    </div>
                                                    :
                                                    <div>
                                                        - <strong>'{item.property + " "}'</strong> value changed
                                                        from <b>{"" + item.left}</b> to <b>{"" + item.right}</b>
                                                    </div>
                                        )}
                                    </div>
                                    : <p>
                                        <b>Changed
                                            author</b> - {currentAuditItemUser ? (currentAuditItemUser.firstName + " " + currentAuditItemUser.lastName) : ""}
                                    </p>}
                                <h5 className="mt-3">Current item: </h5>
                                <Table>
                                    <tbody>
                                    {currentItem ? Object.entries(currentItem).map(([key, value]) =>
                                        key.toString() === "commitMetadata" || key.toString() === "globalId" || key.toString() === "changedProperties" ? "" :
                                            <tr key={key}>
                                                <th>{key}:</th>
                                                <td className="text-right">
                                                    <Table>
                                                        <tbody>
                                                        {isObject(value) ? Object.entries(value).map(([key, value]) =>
                                                            value ?
                                                                key.toString() === "id" || key.toString() === "createdBy" || key.toString() === "updatedBy" ? "" :
                                                                    key.toString() === "mainService" ?
                                                                        <tr key={key}>
                                                                            <th className="text-left">
                                                                                <b>{" Main service "}:</b></th>
                                                                            <th className="text-left">
                                                                                <b>
                                                                                    {mainServices.map(m => value.cdoId === m.id && m.name)}
                                                                                </b>
                                                                            </th>
                                                                        </tr>
                                                                        :
                                                                        <tr key={key}
                                                                            className={isObject(value) ? "d-none" : ""}>
                                                                            <th className="text-left">
                                                                                <b>{key.toString() === "fromTime" ? "From time" : key.toString() === "tillTime" ? "Till time" : "" + key}:</b>
                                                                            </th>
                                                                            <th className="text-left">
                                                                                <b>{isObject(value) ? "" :
                                                                                    ("" + key) === "createdAt" || ("" + key) === "updatedAt" ?
                                                                                        Moment(value).format('hh:MM a, D MMM, YYYY') :
                                                                                        ("" + key) === "fromTime" || ("" + key) === "tillTime" ?
                                                                                            (Moment(Moment(new Date()).format("YYYY") + " " + value + "").format('hh:mm A')) :
                                                                                            "" + value}</b>
                                                                            </th>
                                                                        </tr> : ""
                                                        ) : value}
                                                        </tbody>
                                                    </Table>
                                                </td>

                                            </tr>
                                    ) : ''}
                                    </tbody>
                                </Table>
                            </ModalBody>

                            <ModalFooter>
                                <Button color="secondary" onClick={() => openModal(null)}>Cancel</Button>
                            </ModalFooter>
                        </Modal>
                        <PaginationComponent
                            defaultActivePage={page + 1}
                            totalItems={totalElements} pageSize={20}
                            onSelect={handleSelected}
                        />
                    </div>
                    : <h1 className="text-center">404 - Page not found</h1>}

            </AdminLayout>
        );
    }
}


export default connect(
    ({
         auth: {
             isSuperAdmin,
         },
         app: {
             mainServices,
             itemType,
             currentAuditItemUser,
             historyTables,
             auditTables,
             histories,
             text,
             page,
             size,
             totalElements,
             showModal,
             currentItem,
             showDropdown,
             dispatch,
             currentAuditItems
         },
     }) => ({
        isSuperAdmin,
        mainServices,
        itemType,
        currentAuditItemUser,
        historyTables,
        auditTables,
        histories,
        text,
        page,
        size,
        totalElements,
        showModal,
        currentItem,
        showDropdown,
        dispatch,
        currentAuditItems
    })
)(AuditForItem);



