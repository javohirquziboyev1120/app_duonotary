import React, {Component} from 'react';
import AdminLayout from "../../components/AdminLayout";
import {Button, Col, Input, Row} from "reactstrap";
import './adminPages.scss'
import {
    changeActiveAgentAction,
    changeActiveOnlineAgentAction,
    changeDocumentStatus,
    editAgentAction,
    getAgent,
    getAgents,
    getAgentsByFilter,
    getAgentsBySearch
} from "../../redux/actions/AgentAction";
import {connect} from "react-redux";
import {config} from "../../utils/config";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import ChangeStatusModal from "../../components/Modal/ChangeStatusModal";
import {toast} from "react-toastify";
import {
    changeEnableUserZipcodeAction,
    getStateList,
    getUserZipCodeByUserForAdminAction
} from "../../redux/actions/AppAction";
import PhotoModal from "../../components/Modal/PhotoModal";
import Switch from "react-switch";
import ChangeActiveModal from "../../components/Modal/ChangeActiveModal";
import getDate from "../../components/Date";
import {getOrdersForAdminOrder} from "../../redux/actions/OrderAction";
import PaginationComponent from "react-reactstrap-pagination";
import EditAgentModal from "../../components/Modal/EditAgentModal";
import DownloadRequestModal from "../../components/Modal/DownloadRequestModal";
import {downloadFileAction} from "../../redux/actions/AttachmentAction";
import AgentDocumentComponent from "../../components/agent/AgentDocumentComponent";
import AgentUserZipCodes from "../../components/agent/AgentUserZipCodes";

class Agents extends Component {
    componentDidMount() {
        this.props.dispatch({type: "updateState", payload: {ordersForClientMainPage: ''}})
        this.props.dispatch(getAgents({page: 0, size: this.props.size}))
        this.props.dispatch(getStateList())
        this.props.dispatch({
            type: "updateState",
            payload: {
                currentAgent: ''
            }
        })


    }

    constructor(props) {
        super(props);
        this.state = {
            agentEditModal: false,
            activeModal: false,
            showModal: false,
            text: '',
            path: '',
            zipcodeEnable: false,
            currentItem: ''
        };
    }

    changeBooleansState = (item, data) => {
        if (item === "activeModal") this.setState({activeModal: !this.state.activeModal, text: data});
        if (item === "agentEditModal") this.setState({agentEditModal: !this.state.agentEditModal});
        if (item === "showModal") this.setState({text: data.text, path: data.path, showModal: !this.state.showModal})
    };

    checkEnable(item) {
        return item ? 'Active' : 'Disactive';
    }


    render() {
        const {ordersForClientMainPage,  attachmentIdsArray, dispatch, size, totalElements, page, filters, userZipCodes,zipCodes,agents, isAdmin, currentAgent, states, certificates, passports} = this.props
        const downloadFile = (item) => {
            dispatch(downloadFileAction(item));
            this.changeBooleansState("showModal", {})
        }
        const handleSelected = (selectedPage) => {
            let obj = {page: selectedPage - 1, size: size}
            let zipCode;
            let date;
            let search;
            if (filters) {
                search = filters.filter(item => item.key === 'search')[0]
                zipCode = filters.filter(item => item.key === 'zipCode')[0]
                date = filters.filter(item => item.key === 'date')[0]
                if (search) obj = {...obj, search: filters.filter(item => item.key === 'search')[0].value};
                if (zipCode && date) obj = {
                    ...obj,
                    zipCode: filters.filter(item => item.key === 'zipCode')[0].value,
                    date: filters.filter(item => item.key === 'date')[0].value
                };
                if (zipCode) obj = {...obj, zipCode: filters.filter(item => item.key === 'zipCode')[0].value};
                if (date) obj = {...obj, date: filters.filter(item => item.key === 'date')[0].value};
            }
            dispatch(search ? getAgentsBySearch(obj) : zipCode || date ? getAgentsByFilter(obj) : getAgents(obj))
            dispatch(
                {
                    type: 'updateState',
                    payload: {
                        page: selectedPage - 1
                    }
                }
            )
        }
        const selectAgent = (agent) => {
            if (agent != null) {
                dispatch(getAgent(agent.id));
            } else dispatch({
                type: "updateState",
                payload: {currentAgent: '', ordersForClientMainPage: '', userZipCodes: ''}
            })
        }

        const editAgent = (e, v) => {
            v.id = currentAgent.id
            v.photoId = attachmentIdsArray.filter(item => item.key === "avatar")[0].value
            dispatch(editAgentAction(v))
            this.changeBooleansState("agentEditModal")
        }
        const changeActive = () => {
            if (this.state.text === "active") {
                let countyList = [];
                currentAgent.certificateDtoList.map(certifcate => countyList.push(certifcate.stateDto.id))
                dispatch(changeActiveAgentAction({id: currentAgent.id, counties: countyList}))
                selectAgent(currentAgent);
            }
            if (this.state.text === "online") {
                let data = {}
                data.id = currentAgent.id;
                data.onlineAgent = !currentAgent.onlineAgent
                dispatch(changeActiveOnlineAgentAction({id: currentAgent.id, onlineAgent: !currentAgent.onlineAgent}))
            }
            this.changeBooleansState("activeModal", '');
        }

        const selectAgentForOrders = (agent, i, isExit) => {
            if (isExit) {
                selectAgent(null);
                dispatch({type: 'updateState', payload: {filters: ''}})
                dispatch(getAgents({page: 0, size: this.props.size}))
            } else {
                if (i === 0) dispatch({type: "updateState", payload: {ordersForClientMainPage: '', userZipCodes: ''}});
                if (i === 1) {
                    dispatch({type: "updateState", payload: {userZipCodes: ''}});
                    dispatch(getOrdersForAdminOrder(agent.id))
                } else if (i === 2) {
                    dispatch({type: "updateState", payload: {ordersForClientMainPage: ''}});
                    dispatch(getUserZipCodeByUserForAdminAction(agent.id))
                }
            }


        }
        return (
            <AdminLayout pathname={this.props.location.pathname}>
                <div className="admin-agent-page container">

                    {!currentAgent ?
                        <div className="sharing-history-row flex-column justify-content-between">
                            <Col md={12}>
                                {filters ?
                                    <Row>
                                        <Col><CloseIcon onClick={() => selectAgentForOrders(null, 12, true)}/></Col>
                                        <Col md={12}>
                                            {filters.map(item =>
                                                <Row className="bg-filter  p-2 m-3 rounded">
                                                    <Col md={12}>{item.key} : {item.value}</Col>
                                                </Row>
                                            )}

                                        </Col>
                                    </Row>

                                    : ''}

                            </Col>
                            {isAdmin && agents.length > 0 ? agents.map(agent =>
                                    <div>
                                        <Col key={agent.id} className="history-col">
                                            <Row>
                                                <Col md={3} className=''>
                                                    <div className="d-flex dataBlock">
                                                        <div className="avatar-img">
                                                            <img className="agent-avatar"
                                                                 src={agent.photoId ? config.BASE_URL + "/attachment/" + agent.photoId : "/assets/img/avatar.png"}
                                                                 alt=""/>
                                                        </div>
                                                        <div className="ml-3">
                                                            <div className="fs-15">Agent</div>
                                                            <div className="fs-20-bold">
                                                                {agent.firstName + " " + agent.lastName}
                                                            </div>
                                                            <div className="fs-15">{agent.email}</div>
                                                        </div>
                                                    </div>
                                                </Col>
                                                <Col md={1} className=''>
                                                    <div className="border-line" style={{left: '80%'}}/>
                                                </Col>
                                                <Col md={3}>
                                                    <div className="fs-15">Location
                                                        ({agent.agentLocationDto.createdAt === null ? "no Information" : agent.agentLocationDto.lastSeen.value + " " + agent.agentLocationDto.lastSeen.type + " ago"})
                                                    </div>
                                                    <div className="fs-15-bold pt-2">
                                                        {agent.agentLocationDto.createdAt === null ? "no Information" : agent.agentLocationDto.fullLocation + ", " + agent.agentLocationDto.zipCode}
                                                    </div>
                                                </Col>
                                                <Col md={1}>
                                                    <div className="border-line"/>
                                                </Col>
                                                <Col md={1}>
                                                    <div className="fs-15"
                                                         style={{minWidth: '5rem', textAlign: 'left'}}>Online
                                                        hours
                                                    </div>
                                                    <div className="fs-15 pt-2">
                                                        {agent.agentOnlineHour ? agent.agentOnlineHour : "00:00"}
                                                    </div>
                                                </Col>
                                                <Col md={1}>
                                                    <div className="border-line"/>
                                                </Col>
                                                <Col md={1}>
                                                    <div className="fs-15">Orders</div>
                                                    <div className="fs-15-bold pt-2 text-center">
                                                        {agent.countOrder}
                                                    </div>
                                                </Col>
                                                <Col md={1} onClick={() => selectAgent(agent)}>
                                                    <img className="right-icons" src="/assets/icons/right-arrov.png"
                                                         alt=""/>
                                                </Col>
                                            </Row>
                                        </Col>

                                    </div>
                                ) :
                                <Col md={6} className="offset-3"><h1 className="text-center">No information</h1></Col>
                            }
                            <div className="pagination-bottom mt-2">
                                {totalElements > 0 ? <PaginationComponent
                                    defaultActivePage={page + 1}
                                    totalItems={totalElements} pageSize={size}
                                    onSelect={handleSelected}
                                /> : ''}
                            </div>
                        </div>
                        : currentAgent ?
                            <Row className="agent-edit-page">
                                <Col md={12}>
                                    <div><CloseIcon onClick={() => selectAgentForOrders(null, 0, true, false, true)}/>
                                    </div>
                                    <div className="title">Agent info</div>
                                </Col>
                                <Col md={12}>
                                    <div className="admin-agent-info">
                                        <Row>
                                            <Col md={4}>
                                                <div className="d-flex">
                                                    <div className="user-img">
                                                        <img className="agent-avatar"
                                                             src={currentAgent.photoId ? config.BASE_URL + "/attachment/" + currentAgent.photoId : "/assets/img/avatar.png"}
                                                             alt=""/>
                                                    </div>
                                                    <div className="pt-1">
                                                        <div className="fs-15">Agent</div>
                                                        <div
                                                            className="fs-20-bold"> {currentAgent.firstName + " " + currentAgent.lastName}</div>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col md={2}>
                                                <div className="user-info">
                                                    <div className="">Phone number</div>
                                                    <div className="">{currentAgent.phoneNumber}</div>
                                                </div>
                                            </Col>
                                            <Col md={3}>
                                                <div className="user-info">
                                                    <div className="">Email</div>
                                                    <div className="">{currentAgent.email}</div>
                                                </div>
                                            </Col>
                                            <Col md={1}>
                                                <div className="user-info">
                                                    <Row>Online Agent</Row>
                                                    <div className="">
                                                        <Switch
                                                            checked={currentAgent.onlineAgent}
                                                            onChange={() => this.changeBooleansState("activeModal", "online")}
                                                            id={currentAgent.id}/></div>
                                                </div>
                                            </Col>
                                            <Col md={1}>
                                                <div className="user-info">
                                                    <Row className="ml-auto">Active</Row>
                                                    <div className="">
                                                        <Switch
                                                            checked={currentAgent.active}
                                                            onChange={() => this.changeBooleansState("activeModal", "active")}
                                                            id={currentAgent.id}/></div>
                                                </div>
                                            </Col>
                                            <Col md={1}>
                                                <div className="user-info margin-top-15">
                                                    <div className="">
                                                        <EditIcon
                                                            onClick={() => this.changeBooleansState("agentEditModal")}/>
                                                    </div>
                                                </div>
                                            </Col>

                                        </Row>
                                    </div>

                                    <Col md={5} className="ml-auto mt-4">
                                        <div className="route-services">
                                            <div onClick={() => selectAgentForOrders(currentAgent, 0, false)}
                                                 className={!ordersForClientMainPage && !userZipCodes ?
                                                     "router-active" : "online"}>Documents
                                            </div>
                                            <div onClick={() => selectAgentForOrders(currentAgent, 1, false)}
                                                 className={ordersForClientMainPage && !userZipCodes ?
                                                     "router-active" : "in-person"}>Orders
                                            </div>
                                            <div onClick={() => selectAgentForOrders(currentAgent, 2, false)}
                                                 className={userZipCodes && !ordersForClientMainPage ?
                                                     "router-active" : "in-person"}>Zipcodes
                                            </div>
                                        </div>

                                    </Col>
                                    <Row>

                                    </Row>
                                    {ordersForClientMainPage ?
                                        <div className="admin-agent-order">
                                            <Row className="pl-3">
                                                <Button onClick={() => this.changeBooleansState("showModal", {
                                                    path: "/order/" + currentAgent.id,
                                                    text: "Select file type"
                                                })}>Download Agents Orders</Button>
                                            </Row>
                                            <div className="container-fluid">
                                                <Row className="main-row">
                                                    <Col md={4}>
                                                        <div className="card-name-upcoming">
                                                            <div className="color-card"/>
                                                            <div className="name">
                                                                Upcoming
                                                            </div>
                                                            <div className="card-count">
                                                                {ordersForClientMainPage.object.upcomingCount}
                                                            </div>
                                                        </div>
                                                        {ordersForClientMainPage.object.upcoming.map(item =>
                                                            <div className="about-upcoming" key={item.id}>
                                                                <div className="time-and-service">
                                                                    <div className="time-order">
                                                                        <div className="time-title">
                                                                            Date
                                                                        </div>
                                                                        <div className="time">
                                                    <span
                                                        className="pr-1">{getDate(item.timeTableDto.fromTime, "day")} </span>
                                                                        </div>
                                                                    </div>

                                                                    <div className="service">
                                                                        <div className="time-title">
                                                                            Service
                                                                        </div>
                                                                        <div className="time">
                                                                            <span>{item.servicePrice.serviceDto.subServiceDto.name} </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="time-and-service">
                                                                    <div className="time-order">
                                                                        <div className="time-title">
                                                                            Order number
                                                                        </div>
                                                                        <div className="time">
                                                                            {item.serialNumber ? item.serialNumber : ""}
                                                                        </div>
                                                                    </div>
                                                                    {item.titleDocument ?
                                                                        <div className="service">
                                                                            <div className="time-title">
                                                                                Document title
                                                                            </div>
                                                                            <div className="time">
                                                                                <span>{item.titleDocument ? item.titleDocument : ""}</span>
                                                                            </div>
                                                                        </div> : ""}
                                                                </div>
                                                                <div className="location-title mt-3">
                                                                    Location
                                                                </div>
                                                                <div className="location">
                                                                    {item.servicePrice.online ? "online" : item.address != null ? item.address.length < 26 ? item.address : item.address.substring(0, 25) + '...' : ""}
                                                                </div>

                                                                <div className="time-and-service">
                                                                    <div className="time-order">
                                                                        <div className="time-title">
                                                                            Amount
                                                                        </div>
                                                                        <div className="amount">
                                                                            ${(item.amount).toFixed(2)} {item.discountAmount ?
                                                                            "(-$" + item.discountAmount.toFixed(2) + ")" : ""}
                                                                        </div>
                                                                    </div>

                                                                    <div className="service">
                                                                        <div className="time-title">
                                                                            Client
                                                                        </div>
                                                                        <div className="time">
                                                                            <span>{item.client.firstName + ' ' + item.client.lastName} </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>)}

                                                    </Col>
                                                    <Col md={4}>
                                                        <div className="card-name-in-progress">
                                                            <div className="color-card"/>
                                                            <div className="name">
                                                                In-Progress
                                                            </div>
                                                            <div className="card-count">
                                                                {ordersForClientMainPage.object.inProgressCount}
                                                            </div>
                                                        </div>
                                                        {ordersForClientMainPage.object.inProgress.map(item =>
                                                            <div className="about-in-progress" key={item.id}>
                                                                <div className="time-and-service">
                                                                    <div className="time-order">
                                                                        <div className="time-title">
                                                                            Date
                                                                        </div>
                                                                        <div className="time">
                                                    <span
                                                        className="pr-1">{getDate(item.timeTableDto.fromTime, "day")} </span>
                                                                        </div>
                                                                    </div>

                                                                    <div className="service">
                                                                        <div className="time-title">
                                                                            Service
                                                                        </div>
                                                                        <div className="time">
                                                                            <span>{item.servicePrice.serviceDto.subServiceDto.name} </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="time-and-service">
                                                                    <div className="time-order">
                                                                        <div className="time-title">
                                                                            Order number
                                                                        </div>
                                                                        <div className="time">
                                                                            {item.serialNumber ? item.serialNumber : ""}
                                                                        </div>
                                                                    </div>
                                                                    {item.titleDocument ?
                                                                        <div className="service">
                                                                            <div className="time-title">
                                                                                Document title
                                                                            </div>
                                                                            <div className="time">
                                                                                <span>{item.titleDocument ? item.titleDocument : ""}</span>
                                                                            </div>
                                                                        </div> : ""}
                                                                </div>
                                                                <div className="location-title mt-3">
                                                                    Location
                                                                </div>
                                                                <div className="location">
                                                                    {item.servicePrice.online ? "online" : item.address != null ? item.address.length < 26 ? item.address : item.address.substring(0, 25) + '...' : ""}
                                                                </div>

                                                                <div className="time-and-service">
                                                                    <div className="time-order">
                                                                        <div className="time-title">
                                                                            Amount
                                                                        </div>
                                                                        <div className="amount">
                                                                            ${(item.amount).toFixed(2)} {item.discountAmount ?
                                                                            "(-$" + item.discountAmount.toFixed(2) + ")" : ""}
                                                                        </div>
                                                                    </div>

                                                                    <div className="service">
                                                                        <div className="time-title">
                                                                            Client
                                                                        </div>
                                                                        <div className="time">
                                                                            <span>{item.client.firstName + ' ' + item.client.lastName} </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>)}
                                                    </Col>
                                                    <Col md={4}>
                                                        <div className="card-name-completed">
                                                            <div className="color-card"/>
                                                            <div className="name">
                                                                Completed
                                                            </div>
                                                            <div className="card-count">
                                                                {ordersForClientMainPage.object.completedCount}
                                                            </div>
                                                        </div>
                                                        {ordersForClientMainPage.object.completed.map(item =>
                                                            <div className="about-completed" key={item.id}>
                                                                <div className="time-and-service">
                                                                    <div className="time-order">
                                                                        <div className="time-title">
                                                                            Date
                                                                        </div>
                                                                        <div className="time">
                                                    <span
                                                        className="pr-1">{getDate(item.timeTableDto.fromTime, "day")} </span>
                                                                        </div>
                                                                    </div>

                                                                    <div className="service">
                                                                        <div className="time-title">
                                                                            Service
                                                                        </div>
                                                                        <div className="time">
                                                                            <span>{item.servicePrice.serviceDto.subServiceDto.name} </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="time-and-service">
                                                                    <div className="time-order">
                                                                        <div className="time-title">
                                                                            Order number
                                                                        </div>
                                                                        <div className="time">
                                                                            {item.serialNumber ? item.serialNumber : ""}
                                                                        </div>
                                                                    </div>
                                                                    {item.titleDocument ?
                                                                        <div className="service">
                                                                            <div className="time-title">
                                                                                Document title
                                                                            </div>
                                                                            <div className="time">
                                                                                <span>{item.titleDocument ? item.titleDocument : ""}</span>
                                                                            </div>
                                                                        </div> : ""}
                                                                </div>
                                                                <div className="location-title mt-3">
                                                                    Location
                                                                </div>
                                                                <div className="location">
                                                                    {item.servicePrice.online ? "online" : item.address && item.address.length < 26 ? item.address : item.address && item.address.substring(0, 25) + '...'}
                                                                </div>

                                                                <div className="time-and-service">
                                                                    <div className="time-order">
                                                                        <div className="time-title">
                                                                            Amount
                                                                        </div>
                                                                        <div className="amount">
                                                                            ${(item.amount).toFixed(2)} {item.discountAmount ?
                                                                            "(-$" + item.discountAmount.toFixed(2) + ")" : ""}
                                                                        </div>
                                                                    </div>

                                                                    <div className="service">
                                                                        <div className="time-title">
                                                                            Client
                                                                        </div>
                                                                        <div className="time">
                                                                            <span>{item.client.firstName + ' ' + item.client.lastName} </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>)}
                                                    </Col>
                                                </Row>
                                            </div>
                                        </div>

                                        : userZipCodes ? <AgentUserZipCodes
                                            userZipCodes={userZipCodes}
                                            agent={currentAgent}
                                            /> :
                                            <AgentDocumentComponent
                                            certificates={certificates}
                                            passports={passports}
                                            states={states}
                                            agent={currentAgent}
                                        />}
                                </Col>
                                <ChangeActiveModal
                                    cancel={() => this.changeBooleansState("activeModal")}
                                    showModal={this.state.activeModal}
                                    headText={"Are you sure this agent has changed your activity?"}
                                    submit={changeActive}
                                />
                            </Row> : ''}


                </div>

                {this.state.agentEditModal && currentAgent ?
                    <EditAgentModal
                        dispatch={dispatch}
                        submit={editAgent}
                        cancel={() => this.changeBooleansState("agentEditModal")}
                        data={currentAgent}
                        showModal={this.state.agentEditModal}
                    />
                    : ''}
                {this.state.showModal ?
                    <DownloadRequestModal
                        showModal={this.state.showModal}
                        cancel={() => this.changeBooleansState("showModal", {text: '', path: ''})}
                        dispatch={dispatch}
                        path={this.state.path}
                        confirm={(item) => downloadFile(item)}
                        text={this.state.text}
                    /> : ""}
            </AdminLayout>
        );
    }
}


export default connect(
    ({
         agent: {agents, currentAgent, certificates, passports, userZipCodes},
         app: {currentItem, states, size, totalElements, page, filters,zipCodes},
         auth: {isAdmin},
         order: {ordersForClientMainPage},
         attachment: {attachmentIdsArray}
     }) => ({
        ordersForClientMainPage,  attachmentIdsArray, size, totalElements, page, filters, userZipCodes,zipCodes,agents, isAdmin, currentAgent, states, certificates, passports
    }))(Agents);

function EditIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{cursor: "pointer"}}
                width="18"
                height="18"
                viewBox="0 0 18 18"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M12.728 6.68599L11.314 5.27199L2 14.586V16H3.414L12.728 6.68599ZM14.142 5.27199L15.556 3.85799L14.142 2.44399L12.728 3.85799L14.142 5.27199ZM4.242 18H0V13.757L13.435 0.321992C13.6225 0.134521 13.8768 0.0292053 14.142 0.0292053C14.4072 0.0292053 14.6615 0.134521 14.849 0.321992L17.678 3.15099C17.8655 3.33852 17.9708 3.59283 17.9708 3.85799C17.9708 4.12316 17.8655 4.37746 17.678 4.56499L4.243 18H4.242Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}

function CloseIcon(props) {
    return <div className={props.className}>
        <svg onClick={props.onClick} style={{cursor: 'pointer'}} width="50" height="50" viewBox="0 0 50 50" fill="none"
             xmlns="http://www.w3.org/2000/svg">
            <circle cx="25" cy="25" r="25" fill="white"/>
            <path
                d="M24.955 23.1875L31.1425 17L32.91 18.7675L26.7225 24.955L32.91 31.1425L31.1425 32.91L24.955 26.7225L18.7675 32.91L17 31.1425L23.1875 24.955L17 18.7675L18.7675 17L24.955 23.1875Z"
                fill="#313E47"/>
        </svg>
    </div>
}
