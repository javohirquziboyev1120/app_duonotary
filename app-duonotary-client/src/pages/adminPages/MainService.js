import React, {Component} from "react";
import AdminLayout from "../../components/AdminLayout";
import {
    deleteMainService, deleteMainServiceWithPercent,
    getMainServiceList, getMainServicesWithPercent,
    saveMainService, saveMainServiceWithPercent,
} from "../../redux/actions/AppAction";
import {
    Button,
    CustomInput,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table,
} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import DeleteModal from "../../components/Modal/DeleteModal";
import StatusModal from "../../components/Modal/StatusModal";
import {connect} from "react-redux";
import OnlineModal from "../../components/Modal/OnlineModal";
import Moment from "moment";
import {toast} from "react-toastify";
import * as types from "../../redux/actionTypes/AppActionTypes";
import * as appAction from "../../redux/actions/AppAction";
import {AddIcon} from "../../components/Icons";
import CloseBtn from "../../components/CloseBtn";

class MainService extends Component {
    componentDidMount() {
        this.props.dispatch(getMainServiceList());
        this.props.dispatch(getMainServicesWithPercent());
        this.props.dispatch({
            type: "updateState",
            payload: {
                showModal: false,
                showStatusModal: false,
                showDeleteModal: false,
                showOnlineModal: false,
            }
        })
        this.isActiveTab = this.props.location.pathname === "/admin/mainService";

    }

    render() {
        const {
            permissions,
            dispatch,
            showModal,
            mainServices,
            currentItem,
            active,
            online,
            showDeleteModal,
            showStatusModal,
            showOnlineModal,
            isMainService,
            isMainServicePercent,
            mainServicesWithPercent
        } = this.props;


        const saveItemPercent = (e, v) => {
            let isPermission = true;
            if (mainServicesWithPercent && mainServicesWithPercent.length > 0) {
                // eslint-disable-next-line array-callback-return
                mainServicesWithPercent.map(service => {
                    if (service.id !== (currentItem && currentItem.id ? currentItem.id : "0")) {
                        if (service.mainServiceId === (currentItem && currentItem.id ? currentItem.mainServiceId : (v.mainService.substring(1)))
                            && (
                                (service.fromTime <= v.fromTime
                                    && v.fromTime <= service.tillTime)
                                || (service.fromTime <= v.tillTime
                                    && v.tillTime <= service.tillTime)
                                || (v.fromTime <= service.fromTime && service.fromTime <= v.tillTime)
                                || (v.fromTime <= service.tillTime && service.tillTime <= v.tillTime)
                                || v.fromTime >= v.tillTime
                            )
                        ) {
                            isPermission = false
                        }
                    }
                })
            }
            if (mainServices && mainServices.length > 0) {
                // eslint-disable-next-line array-callback-return
                mainServices.map(service => {
                    if (service.id === (currentItem && currentItem.id ? currentItem.mainServiceId : (v.mainService.substring(1)))
                        && (
                            (service.fromTime <= v.fromTime
                                && v.fromTime <= service.tillTime)
                            || (service.fromTime <= v.tillTime
                                && v.tillTime <= service.tillTime)
                            || (v.fromTime <= service.fromTime && service.fromTime <= v.tillTime)
                            || (v.fromTime <= service.tillTime && service.tillTime <= v.tillTime)
                            || v.fromTime >= v.tillTime
                        )
                    ) {
                        isPermission = false
                    }
                })
            }
            if (isPermission) {
                v.id = currentItem ? currentItem.id : null;
                v.active = active;
                this.props.dispatch(saveMainServiceWithPercent({...v}));
            } else {
                toast.error("You inserting from time and till time error!");
            }
        };
        const changeStatusMainServiceWithPercent = () => {
            let currentMainServiceWithPercent = {...currentItem};
            currentMainServiceWithPercent.active = !currentItem.active;
            currentMainServiceWithPercent.mainService =
                "/" + currentItem.mainServiceId;
            this.props.dispatch(
                saveMainServiceWithPercent(currentMainServiceWithPercent)
            );
        };


        const openModalMain = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showModal: !showModal,
                    currentItem: item,
                    active: item.active,
                    online: item.online,
                },
            })

        };

        const openModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showModal: !showModal,
                    currentItem: item,
                    active: item.active,
                },
            });
        };

        let isOnline = false;
        const openDeleteModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentItem: item,
                },
            });
        };
        const changeActive = () => {
            dispatch({
                type: "updateState",
                payload: {
                    active: !active,
                },
            });
        };

        const changeOnline = () => {
            dispatch({
                type: "updateState",
                payload: {
                    online: !online,
                },
            });
        };

        const deleteFunction = () => {
            isMainService ?
                this.props.dispatch(deleteMainService(currentItem))
                :
                this.props.dispatch(deleteMainServiceWithPercent(currentItem));

        };

        const saveItem = (e, v) => {
            v.id = currentItem ? currentItem.id : null;
            v.active = active;
            v.online = online;
            if (v.tillTime > v.fromTime) {
                this.props.dispatch(saveMainService(v));
            } else {
                toast.error("You inserting from time and till time error!");
            }
        };

        const changeStatusMainService = () => {
            let currentMainService = {...currentItem};
            currentMainService.active = !currentItem.active;
            this.props.dispatch(saveMainService(currentMainService));
        };

        const changeOnlineMainService = () => {
            let currentMainServiceOnline = {...currentItem};
            currentMainServiceOnline.online = !currentItem.online;
            this.props.dispatch(saveMainService(currentMainServiceOnline));
        };

        const openStatusModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item,
                },
            });
        };

        const openOnlineModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showOnlineModal: !showOnlineModal,
                    currentItem: item,
                },
            });
        };


        const changeTabePane = (tabePane) => {
            if (tabePane === 'mainService') {
                dispatch({type: types.CHANGE_MAIN_SERVICE_BOOLEAN})
                dispatch(appAction.getMainServiceList())
            }
            if (tabePane === 'mainServiceWithPercent') {
                dispatch({type: types.CHANGE_MAIN_SERVICE_PERCENT_BOOLEAN})
                dispatch(appAction.getMainServicesWithPercent())
            }
        }
        return (
            <AdminLayout pathname={this.props.location.pathname}>
                <div className="main-service-page container">
                    <div className="feedback-admin-page">
                        <div className="w-100 d-flex justify-content-end">
                            <div className="route-services ">
                                <div onClick={() => changeTabePane('mainService')} className={!isMainService ?
                                    "online" : "router-active"}>Main Service
                                </div>
                                <div onClick={() => changeTabePane('mainServiceWithPercent')}
                                     className={!isMainServicePercent ?
                                         "in-person px-0" : "router-active px-0"}>After hour service
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="flex-row align-items-center button-container">
                        {isMainService ?
                            (permissions && permissions.some(item => item.permissionName === "SAVE_MAIN_SERVICE") ?
                                <AddIcon
                                    className="add-button"
                                    onClick={() => openModalMain('')}
                                /> : "")
                            :
                            (permissions && permissions.some(item => item.permissionName === "SAVE_MAIN_SERVICE_WORK_TIME_PERCENT") ?
                                <AddIcon
                                    className="add-button"
                                    onClick={() => openModal('')}
                                /> : "")
                        }
                        <span
                            className="dark-title ml-3">
                            {permissions && permissions.some(item => item.permissionName === "SAVE_MAIN_SERVICE") && isMainService ? "Add new main service" : ""}
                            {permissions && permissions.some(item => item.permissionName === "SAVE_MAIN_SERVICE_WORK_TIME_PERCENT") && isMainServicePercent ? "Add after hours work" : ""}
                            </span>
                    </div>


                    <Table className="custom-table zipCodeTable-style">
                        <thead>
                        {
                            isMainService ?
                                <tr>
                                    <th>#</th>
                                    <th>Type</th>
                                    <th>Time</th>
                                    <th>Description</th>
                                    {/*<th>In person number</th>*/}
                                    <th>Status</th>
                                    <th>Online</th>
                                    {permissions && permissions.some(item => item.permissionName === "SAVE_MAIN_SERVICE" || item.permissionName === "DELETE_MAIN_SERVICE") ?
                                        <th colSpan="2" className="">
                                            Edit
                                        </th> : ""}
                                </tr>
                                : isMainServicePercent ?
                                <tr>
                                    <th>#</th>
                                    <th className="white-space:nowrap">Type</th>
                                    <th className="white-space:nowrap">Time</th>
                                    <th>Surge</th>
                                    <th>Status</th>
                                    {permissions && permissions.some(item => item.permissionName === "SAVE_MAIN_SERVICE_WORK_TIME_PERCENT" || item.permissionName === "DELETE_MAIN_SERVICE_WORK_TIME_PERCENT") ?
                                        <th colSpan="2">
                                            Edit
                                        </th> : ""}
                                </tr> : ""
                        }
                        </thead>
                        {isMainService ?
                            <tbody>
                            {mainServices && mainServices.map((item, i) => (
                                <tr key={item.id}>
                                    <td>{i + 1}</td>
                                    <td>{item.name}</td>
                                    <td>
                                        {Moment(Moment(new Date()).format("YYYY") + " " + item.fromTime + "").format('hh:mm A')} - {Moment(Moment(new Date()).format("YYYY") + " " + item.tillTime + "").format('hh:mm A')}
                                    </td>
                                    <td>{item.description}</td>
                                    {/*<td>{item.personNumber}</td>*/}
                                    <td>
                                        <FormGroup check>
                                            <Label check for={"active" + i} className={'labelDuo'}>
                                                <div className={'labelBlock'}>
                                                    {item.active && <div className={'labelBox'}>
                                                        <i className="fas fa-check"></i>
                                                    </div>}
                                                </div>
                                                {permissions && permissions.some(item => item.permissionName === "SAVE_MAIN_SERVICE") ?
                                                    <Input
                                                        className={'d-none'}
                                                        type="checkbox"
                                                        onClick={() =>
                                                            openStatusModal(
                                                                item
                                                            )
                                                        }
                                                        id={"active" + i}
                                                        checked={item.active}
                                                    /> : ""}
                                                {"Active"}
                                            </Label>
                                        </FormGroup>
                                    </td>
                                    <td>
                                        <FormGroup check>
                                            <Label check for={"online" + i} className={'labelDuo'}
                                            //        style={item.online === false? {
                                            //     pointerEvents : 'none'
                                            // } : {}}>
                                                   style={
                                                       { pointerEvents : 'none'}}>
                                                <div className={'labelBlock'}>
                                                    {item.online && <div className={'labelBox'}>
                                                        <i className="fas fa-check"></i>
                                                    </div>}
                                                </div>
                                                {permissions && permissions.some(item => item.permissionName === "SAVE_MAIN_SERVICE") ?
                                                    <Input
                                                        className={'d-none'}
                                                        type="checkbox"
                                                        onClick={() =>
                                                            openOnlineModal(
                                                                item
                                                            )
                                                        }
                                                        id={"online" + i}
                                                        checked={item.online}
                                                    /> : ""}
                                                {"Online"}
                                            </Label>
                                        </FormGroup>
                                    </td>
                                    <td>
                                        {permissions && permissions.some(item => item.permissionName === "SAVE_MAIN_SERVICE") ?
                                            <EditIcon
                                                onClick={() => openModalMain(item)}
                                            />
                                            : ""}
                                    </td>
                                    <td>
                                        {permissions && permissions.some(item => item.permissionName === "DELETE_MAIN_SERVICE") ?
                                            <DeleteIcon
                                                onClick={() =>
                                                    openDeleteModal(item)
                                                }
                                            />
                                            : ""}
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                            :
                            <tbody>
                            {mainServicesWithPercent && mainServicesWithPercent.map((item, i) => (
                                <tr key={item.id}>
                                    <td>{i + 1}</td>
                                    <td>
                                        {item.mainService.name}
                                    </td>
                                    <td>
                                        {Moment(Moment(new Date()).format("YYYY") + " " + item.fromTime + "").format('hh:mm A ')}
                                        - {Moment(Moment(new Date()).format("YYYY") + " " + item.tillTime + "").format('hh:mm A ')}
                                    </td>
                                    <td>
                                        +{item.percent}%
                                    </td>
                                    <td>
                                        <FormGroup check>
                                            <Label
                                                check
                                                className={'labelDuo'}
                                            >
                                                <div className={'labelBlock'}>
                                                    {item.active && <div className={'labelBox'}>
                                                        <i className="fas fa-check"></i>
                                                    </div>}
                                                </div>
                                                {permissions && permissions.some(item => item.permissionName === "SAVE_MAIN_SERVICE_WORK_TIME_PERCENT") ?
                                                    <Input
                                                        className={'d-none'}
                                                        type="checkbox"
                                                        onClick={() =>
                                                            openStatusModal(item)
                                                        }
                                                        id={item.id}
                                                        checked={item.active}
                                                    /> : ""}
                                                {"Active"}
                                            </Label>
                                        </FormGroup>
                                    </td>
                                    <td>
                                        {permissions && permissions.some(item => item.permissionName === "SAVE_MAIN_SERVICE_WORK_TIME_PERCENT") ?
                                            <EditIcon
                                                onClick={() => openModal(item)}
                                            />
                                            : ""}
                                    </td>
                                    <td>
                                        {permissions && permissions.some(item => item.permissionName === "DELETE_MAIN_SERVICE_WORK_TIME_PERCENT") ?
                                            <DeleteIcon
                                                onClick={() =>
                                                    openDeleteModal(item)
                                                }
                                            />
                                            : ""}
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        }
                    </Table>

                    {isMainService ?
                        <Modal
                            id="allModalStyle"
                            isOpen={showModal}
                            toggle={openModalMain}
                        >
                            <AvForm onValidSubmit={saveItem}>
                                <div className='position-absolute' style={{top : '45px', left : '-12%'}}>
                                    <CloseBtn click={() => openModalMain("")} />
                                </div>
                                <ModalHeader
                                    toggle={openModalMain}
                                    charCode="x"
                                    className="model-head"
                                >
                                    {currentItem != null && currentItem.name
                                        ? "Edit Main Service"
                                        : "Add Main Service"}
                                </ModalHeader>
                                <ModalBody>
                                    <AvField
                                        name="name"
                                        label="Name:"
                                        required
                                        defaultValue={
                                            currentItem != null
                                                ? currentItem.name
                                                : ""
                                        }
                                        placeholder="Enter Main Service name"
                                    />
                                    <div className="flex-row-times margin-top-15">
                                        <AvField
                                            type="time"
                                            name="fromTime"
                                            className="half-left"
                                            label="Start time:"
                                            required
                                            defaultValue={
                                                currentItem != null
                                                    ? currentItem.fromTime
                                                    : ""
                                            }
                                            placeholder="Enter start time"
                                        />
                                        <AvField
                                            type="time"
                                            name="tillTime"
                                            className="half-right"
                                            label="End time:"
                                            required
                                            defaultValue={
                                                currentItem != null
                                                    ? currentItem.tillTime
                                                    : ""
                                            }
                                            placeholder="Enter end time"
                                        />
                                    </div>
                                    <AvField
                                        name="description"
                                        label="Description:"
                                        required
                                        defaultValue={
                                            currentItem != null
                                                ? currentItem.description
                                                : ""
                                        }
                                        placeholder="Enter description"
                                    />
                                    {/*<AvField*/}
                                    {/*    name="orderNumber"*/}
                                    {/*    label="InPerson Number"*/}
                                    {/*    className="modal-input"*/}
                                    {/*    defaultValue={*/}
                                    {/*        currentItem != null*/}
                                    {/*            ? currentItem.orderNumber*/}
                                    {/*            : ""*/}
                                    {/*    }*/}
                                    {/*    placeholder="Enter order number"*/}
                                    {/*/>*/}

                                    <div className="flex-row space-around">
                                        <label className={'labelDuo'}>
                                            <div className={'labelBlock'}>
                                                {active && <div className={'labelBox'}>
                                                    <i className="fas fa-check"></i>
                                                </div>}
                                            </div>
                                            <CustomInput
                                                className="check-box-title d-none"
                                                type="checkbox"
                                                checked={active}
                                                onChange={changeActive}
                                            />
                                            Active
                                        </label>

                                        {mainServices && mainServices.map(item =>
                                            item.online === true ?
                                                isOnline = true
                                                :
                                                " "
                                        )}
                                        {isOnline ? " " :
                                            <label className={'labelDuo'}>
                                                <div className={'labelBlock'}>
                                                    {online && <div className={'labelBox'}>
                                                        <i className="fas fa-check"></i>
                                                    </div>}
                                                </div>
                                                <CustomInput
                                                    className="check-box-title d-none"
                                                    type="checkbox"
                                                    checked={online}
                                                    onChange={changeOnline}
                                                />
                                                Online
                                            </label>
                                        }
                                    </div>
                                </ModalBody>
                                <ModalFooter>
                                    {/*<Button*/}
                                    {/*    type="button"*/}
                                    {/*    color="secondary"*/}
                                    {/*    outline*/}
                                    {/*    onClick={() => openModalMain("")}*/}
                                    {/*>*/}
                                    {/*    Cancel*/}
                                    {/*</Button>{" "}*/}
                                    <Button color="info" outline>Save</Button>
                                </ModalFooter>
                            </AvForm>
                        </Modal> : isMainServicePercent ?
                            <Modal id="allModalStyle" isOpen={showModal} toggle={openModal}>
                                <AvForm onValidSubmit={saveItemPercent}>
                                    <div className='position-absolute' style={{top : '45px', left : '-12%'}}>
                                        <CloseBtn click={() => openModal('')}/>
                                    </div>
                                    <ModalHeader toggle={openModal} charCode="x">
                                        {currentItem != null && currentItem.name
                                            ? "Edit after hour service"
                                            : "Add after hour service"}
                                    </ModalHeader>
                                    <ModalBody>
                                        {mainServices && mainServices.length > 0 ? (
                                            <AvField
                                                type="select"
                                                name="mainService"
                                                value={
                                                    currentItem
                                                        ? "/" + currentItem.mainServiceId
                                                        : ""
                                                }
                                                required
                                            >
                                                <option value="" disabled>
                                                    Select main services
                                                </option>
                                                {mainServices.map((item) => (
                                                    <option
                                                        key={item.id}
                                                        value={"/" + item.id}
                                                    >
                                                        {item.name}
                                                    </option>
                                                ))}
                                            </AvField>
                                        ) : (
                                            "Services not found"
                                        )}

                                        <AvField
                                            type="time"
                                            name="fromTime"
                                            label="Start time:"
                                            required
                                            defaultValue={
                                                currentItem ? currentItem.fromTime : ""
                                            }
                                            placeholder="Enter from time"
                                        />

                                        <AvField
                                            type="time"
                                            name="tillTime"
                                            label="End time:"
                                            required
                                            defaultValue={
                                                currentItem ? currentItem.tillTime : ""
                                            }
                                            placeholder="Enter till time"
                                        />

                                        <AvField
                                            type="number"
                                            name="percent"
                                            label="Surge(percent):"
                                            required
                                            defaultValue={
                                                currentItem ? currentItem.percent : ""
                                            }
                                            placeholder="Enter percent"
                                        />

                                        <label className={'labelDuo ml-5'}>
                                            <div className={'labelBlock'}>
                                                {active && <div className={'labelBox'}>
                                                    <i className="fas fa-check"/>
                                                </div>}
                                            </div>
                                            <CustomInput
                                                className={'d-none'}
                                                type="checkbox"
                                                checked={active}
                                                onChange={changeActive}
                                            />
                                            Active
                                        </label>
                                    </ModalBody>
                                    <ModalFooter>
                                        {/*<Button type="button" color="" onClick={() => openModal('')}>*/}
                                        {/*    Cancel*/}
                                        {/*</Button>{" "}*/}
                                        <Button color="info" outline>Save</Button>
                                    </ModalFooter>
                                </AvForm>
                            </Modal>
                            : ""}
                    {showOnlineModal && (
                        <OnlineModal
                            text={currentItem != null ? currentItem.name : ""}
                            showOnlineModal={showOnlineModal}
                            confirm={changeOnlineMainService}
                            cancel={openOnlineModal}
                        />
                    )}

                    {isMainServicePercent ? showDeleteModal && (
                        <DeleteModal
                            text={
                                currentItem != null
                                    ? currentItem.mainService.name
                                    : ""
                            }
                            showDeleteModal={showDeleteModal}
                            confirm={deleteFunction}
                            cancel={openDeleteModal}
                        />
                    ) :
                        showDeleteModal && (
                            <DeleteModal
                                text={currentItem != null ? currentItem.name : ""}
                                showDeleteModal={showDeleteModal}
                                confirm={deleteFunction}
                                cancel={openDeleteModal}
                            />
                        )
                    }

                    {isMainServicePercent ? showStatusModal && (
                        <StatusModal
                            text={
                                currentItem != null
                                    ? currentItem.mainService.name
                                    : ""
                            }
                            showStatusModal={showStatusModal}
                            confirm={changeStatusMainServiceWithPercent}
                            cancel={openStatusModal}
                        />
                    ) :
                        showStatusModal && (
                            <StatusModal
                                text={currentItem != null ? currentItem.name : ""}
                                showStatusModal={showStatusModal}
                                confirm={changeStatusMainService}
                                cancel={openStatusModal}
                            />
                        )}

                </div>
            </AdminLayout>
        );

    }
}

export default connect(
    ({
         app: {
             permissions,
             mainServices,
             showModal,
             currentItem,
             active,
             online,
             showDeleteModal,
             showStatusModal,
             showOnlineModal,
             isMainService,
             isMainServicePercent,
             mainServicesWithPercent
         },
     }) => ({
        permissions,
        mainServices,
        showModal,
        currentItem,
        active,
        online,
        showDeleteModal,
        showStatusModal,
        showOnlineModal,
        isMainService,
        isMainServicePercent,
        mainServicesWithPercent
    })
)(MainService);

// Icons
function EditIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{cursor: "pointer"}}
                width="18"
                height="18"
                viewBox="0 0 18 18"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M12.728 6.68599L11.314 5.27199L2 14.586V16H3.414L12.728 6.68599ZM14.142 5.27199L15.556 3.85799L14.142 2.44399L12.728 3.85799L14.142 5.27199ZM4.242 18H0V13.757L13.435 0.321992C13.6225 0.134521 13.8768 0.0292053 14.142 0.0292053C14.4072 0.0292053 14.6615 0.134521 14.849 0.321992L17.678 3.15099C17.8655 3.33852 17.9708 3.59283 17.9708 3.85799C17.9708 4.12316 17.8655 4.37746 17.678 4.56499L4.243 18H4.242Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}

function DeleteIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{cursor: "pointer"}}
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M2 6H18V19C18 19.2652 17.8946 19.5196 17.7071 19.7071C17.5196 19.8946 17.2652 20 17 20H3C2.73478 20 2.48043 19.8946 2.29289 19.7071C2.10536 19.5196 2 19.2652 2 19V6ZM4 8V18H16V8H4ZM7 10H9V16H7V10ZM11 10H13V16H11V10ZM5 3V1C5 0.734784 5.10536 0.48043 5.29289 0.292893C5.48043 0.105357 5.73478 0 6 0H14C14.2652 0 14.5196 0.105357 14.7071 0.292893C14.8946 0.48043 15 0.734784 15 1V3H20V5H0V3H5ZM7 2V3H13V2H7Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}
