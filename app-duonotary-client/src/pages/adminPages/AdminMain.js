import React, {Component} from "react";
import AdminLayout from "../../components/AdminLayout";
import Switch from "react-switch";
import {
    getMainServiceList,
    getMainServicesWithPercent,
    getPublicHolidays,
    getSubServiceList, saveHolidays,
    saveMainService,
    saveMainServiceWithPercent,
    saveSubService
} from "../../redux/actions/AppAction";
import {
    Button,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader
} from "reactstrap";
import {connect} from "react-redux";
import Moment from "moment";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {isSameDay} from 'date-fns'
import {enGB} from 'date-fns/locale'
import {Calendar} from 'react-nice-dates'
import 'react-nice-dates/build/style.css'
import {toast} from "react-toastify";
import {CalendarIcon, PlusIconSmall} from "../../components/Icons";
import CloseBtn from "../../components/CloseBtn";

class AdminMain extends Component {
    componentDidMount() {
        this.props.dispatch(getMainServiceList());
        this.props.dispatch(getSubServiceList())
        this.props.dispatch(getMainServicesWithPercent())
        this.props.dispatch(getPublicHolidays())
        this.props.dispatch({
            type: "updateState",
            payload: {
                showModal: false,
                showStatusModal: false,
            }
        })
    }

    render() {
        const {
            permissions,
            dispatch, currentItem, mainServices, subServices, holiday,
            holidays, showStatusModal, selectedDates, selectHolidays,
            mainServicesWithPercent, showModal, active, itemType
        } = this.props;
        const changeStatusModal = () => {
            if (itemType === "changeHolidaysStatus") {
                changeHolidayInMainService(currentItem);
            } else if (currentItem != null) {
                let currentService = currentItem
                currentService = {...currentService, active: !currentItem.active}
                if (!currentItem.tillTime && !currentItem.fromTime && !currentItem.data) {
                    dispatch(saveSubService(currentService));
                } else if (!currentItem.subServiceDto && currentItem.mainService && currentItem.percent) {
                    let currentMainServiceWithPercent = {...currentItem};
                    currentMainServiceWithPercent.active = !currentItem.active;
                    currentMainServiceWithPercent.mainService = "/" + currentItem.mainServiceId
                    dispatch(saveMainServiceWithPercent(currentMainServiceWithPercent))
                } else {
                    dispatch(saveMainService(currentService));
                }
            } else {
                dispatch({
                    type: 'updateState',
                    payload: {
                        showStatusModal: false,
                    }
                })
            }

        }
        const openChanHolidaysStatusModal = (item, active, blockType) => {
            dispatch({
                type: 'updateState',
                payload: {
                    itemType: blockType,
                    showStatusModal: !showStatusModal,
                    currentItem: item,
                    active
                }
            })
        }
        const openChanStatusModal = (item, blockType) => {
            dispatch({
                type: 'updateState',
                payload: {
                    itemType: blockType,
                    showStatusModal: !showStatusModal,
                    currentItem: item
                }
            })
        }
        const openModal = (item, blockType) => {
            if (blockType === "publicHoliday") {
                changeCal(item);
            }
            dispatch({
                type: 'updateState',
                payload: {
                    showModal: !showModal,
                    currentItem: item,
                    itemType: blockType,
                    active: item ? item.active : false,
                    online: item ? item.online : false,
                }
            })
        }
        const changeHolidayInMainService = (mainService) => {
            let changeHolidays = [];
            holidays && holidays.length > 0 && holidays.map(item => {
                    if (("" + item.mainServiceId) === ("" + mainService.id) &&
                        new Date(item.date) > new Date()
                    ) {
                        let currentHoliday = {...item};
                        currentHoliday.active = !active;
                        changeHolidays.push(currentHoliday);
                    }
                }
            )
            this.props.dispatch(saveHolidays(changeHolidays))

        };
        const changeCal = (mainService) => {
            let holidayDates = []
            let selectHoliday = []

            holidays && holidays.length > 0 && holidays.map(item => {
                    if (item.mainServiceId === mainService.id) {
                        holidayDates.push(new Date(item.date))
                        selectHoliday.push(item)
                    }
                }
            )
            dispatch({
                type: "updateState",
                payload: {
                    selectedDates: holidayDates,
                    selectHolidays: selectHoliday

                },
            });
        };
        const saveMainServiceModal = (e, v) => {
            if (itemType === "mainService") {
                v.id = currentItem ? currentItem.id : null;
                if (v.tillTime > v.fromTime) {
                    this.props.dispatch(saveMainService(v));
                } else {
                    toast.error("You inserting from time and till time error!");
                }

            } else if (itemType === "mainServiceWithPercent") {
                let isPermission = true;
                if (mainServicesWithPercent && mainServicesWithPercent.length > 0) {
                    mainServicesWithPercent.map(service => {
                        if (service.id !== (currentItem && currentItem.id ? currentItem.id : "0")) {
                            if (service.mainServiceId === (currentItem && currentItem.id ? currentItem.mainServiceId : (v.mainService.substring(1)))
                                && (
                                    (service.fromTime <= v.fromTime
                                        && v.fromTime <= service.tillTime)
                                    || (service.fromTime <= v.tillTime
                                        && v.tillTime <= service.tillTime)
                                    || (v.fromTime <= service.fromTime && service.fromTime <= v.tillTime)
                                    || (v.fromTime <= service.tillTime && service.tillTime <= v.tillTime)
                                    || v.fromTime >= v.tillTime
                                )
                            ) {
                                isPermission = false
                            }
                        }
                    })
                }
                if (mainServices && mainServices.length > 0) {
                    mainServices.map(service => {
                        if (service.id === (currentItem && currentItem.id ? currentItem.mainServiceId : (v.mainService.substring(1)))
                            && (
                                (service.fromTime <= v.fromTime && v.fromTime <= service.tillTime) || (service.fromTime <= v.tillTime && v.tillTime <= service.tillTime)
                                ||
                                (v.fromTime <= service.fromTime && service.fromTime <= v.tillTime) || (v.fromTime <= service.tillTime && service.tillTime <= v.tillTime)
                                || v.fromTime >= v.tillTime
                            )
                        ) {
                            isPermission = false
                        }
                    })
                }
                if (isPermission) {
                    v.id = currentItem ? currentItem.id : null;
                    v.active = active;
                    this.props.dispatch(saveMainServiceWithPercent({...v}));
                } else {
                    toast.error("You inserting from time and till time error!");
                }
            } else if (itemType === "service") {
                v.id = currentItem != null ? currentItem.id : null
                this.props.dispatch(saveSubService(v))
            }
        };
        const modifiers = {
            selected: date => selectedDates.some(selectedDate => isSameDay(selectedDate, date))
        }
        const handleDayClick = date => {
            dispatch({
                type: "updateState",
                payload: {
                    holiday: ""
                }
            })
            selectedDates.map(item =>
                Moment(item).format("MM/DD/YYYY") === Moment(date).format("MM/DD/YYYY") ?
                    selectHolidays.map(item1 =>
                        Moment(item1.date).format("MM/DD/YYYY") === Moment(date).format("MM/DD/YYYY") ?
                            dispatch({
                                type: "updateState",
                                payload: {
                                    holiday: item1
                                }
                            })
                            : ""
                    )
                    : ""
            )
        }
        return (
            <AdminLayout pathname={this.props.location.pathname}>
                <div className="admin-main-page flex-column container">
                    <div className="section flex-column">
                        <span className="main-title">Work hours</span>
                        {mainServices && mainServices.length > 0 ?
                            <div className="row px-3  margin-top-15">
                                {mainServices.map((item, i) =>
                                    <div key={i}
                                         className={i % 2 !== 0 ? "col-6 mr-0 mt-2 pr-0" : "col-6 mr-0 mt-2 pl-0"}>
                                        <div
                                            className="inner-section flex-row space-between">
                                            <span className="dark-title">{item.name}</span>
                                            <div className="flex-row">
                                                <div>
                                                    {Moment(Moment(new Date()).format("YYYY") + " " + item.fromTime + "").format('hh:mm A')}
                                                </div>
                                                <span className="time-text"> - </span>
                                                <div>
                                                    {Moment(Moment(new Date()).format("YYYY") + " " + item.tillTime + "").format('hh:mm A')}
                                                </div>
                                                {permissions && permissions.some(item => item.permissionName === "SAVE_MAIN_SERVICE") ?
                                                    <button className="btn btn-white border-0 border-0"
                                                            onClick={() => openModal(item, "mainService")}>
                                                        <CalendarIcon size={40} className="p-2"/>
                                                    </button> : ""}
                                            </div>
                                        </div>
                                    </div>
                                )}
                            </div>
                            : ""}
                        {mainServicesWithPercent && mainServicesWithPercent.length > 0 ?
                            mainServicesWithPercent.map((item, i) =>
                                <div key={i} className="inner-section margin-top-25 flex-row space-between">
                                    <div className="flex-row">
                                        <Switch
                                            className="margin-right-50"
                                            checked={item.active}
                                            onChange={() => openChanStatusModal(item)}
                                            id={item.id}/>
                                        <span className={item.active?"dark-title":"light-title"}>{item.mainService.name}</span>
                                    </div>
                                    <div className="flex-row">
                                <span className="time-text percentage">
                                    <span className="time-text mr-3">{item.percent}%</span>
                                    <div className="border-line bg-secondary px-5"></div>
                                </span>
                                        <div className="d-flex align-items-center w-75">
                                            <div>
                                                {Moment(Moment(new Date()).format("YYYY") + " " + item.fromTime + "").format('hh:mm A')}
                                            </div>
                                            <span className="time-text"> - </span>
                                            <div>
                                                {Moment(Moment(new Date()).format("YYYY") + " " + item.tillTime + "").format('hh:mm A')}
                                            </div>
                                            <DropdownIcon onClick={() => openModal(item, "mainServiceWithPercent")}
                                                          className="margin-left-15 margin-right-40"/>
                                            {permissions && permissions.some(item => item.permissionName === "SAVE_MAIN_SERVICE_WORK_TIME_PERCENT") ?
                                                <PlusIconSmall className="margin-left-15 margin-right-40"
                                                               onClick={() => openModal(null, "mainServiceWithPercent")}/>
                                                : ""}
                                        </div>
                                    </div>
                                </div>)
                            : <span className="main-title margin-top-25">
                                After work hours<span className="d-inline-flex"><PlusIconSmall
                                className="margin-left-15 d-inline"
                                onClick={() => openModal(null, "mainServiceWithPercent")}/>
                                                                    </span>
                                </span>}

                        {mainServices && mainServices.length > 0 ?
                            <div className="row px-3 margin-top-25">
                                {mainServices.map((item, i) =>
                                    <div key={i}
                                         className={i % 2 !== 0 ? "col-6 mr-0 mt-2 pr-0" : "col-6 mr-0 mt-2 pl-0"}>
                                        <div key={i} className="flex-column inner-section ">
                                            <span className="text-28">{item.name}</span>
                                            <span className="text-black-16 margin-top-15">
                                                    {item.description}
                                                 </span>
                                            <Switch
                                                className="margin-top-50"
                                                checked={item.active}
                                                onChange={() => openChanStatusModal(item, "mainService")}
                                                id={item.id}/>
                                        </div>
                                    </div>
                                )}
                            </div>
                            : ""}
                    </div>
                    <div className="section flex-column">
                        <span className="main-title margin-top-25">
                            Public holiday
                        </span>
                        {mainServices && mainServices.length > 0 ?
                            <div className="row px-3 margin-top-15">
                                {mainServices.map((item, l) =>
                                    <div key={l}
                                         className={l % 2 !== 0 ? "col-6 mr-0 mt-2 pr-0" : "col-6 mr-0 mt-2 pl-0"}>
                                        <div
                                            className="flex-row inner-section space-between">
                                            <span className="light-title">{item.name}
                                            </span>
                                            <div className="flex-row">
                                                <CalendarIcon className="margin-left-15 margin-right-40 cursor-p"
                                                              onClick={() => openModal(item, "publicHoliday")}/>
                                                {permissions && permissions.some(item => item.permissionName === "SAVE_HOLIDAY") ?
                                                    <Switch
                                                        className="margin-left-35"
                                                        checked={holidays && holidays.some(i => i.active
                                                            && i.mainServiceId === item.id
                                                            && new Date(i.date) >= new Date())}
                                                        onChange={() => openChanHolidaysStatusModal(item, (holidays && holidays.some(i => i.active
                                                            && i.mainServiceId === item.id
                                                            && new Date(i.date) >= new Date())), "changeHolidaysStatus")}
                                                        id={item.name}/>
                                                    : ""}
                                            </div>
                                        </div>
                                    </div>
                                )}
                            </div>
                            : ""}
                    </div>
                    <div className="section flex-column">
                        <span className="main-title margin-top-25">
                            Services
                        </span>
                        {subServices && subServices.length > 0 ?
                            <div className="row px-3 margin-top-15">
                                {subServices.map((item, i) =>
                                        <div key={i}
                                             className={i % 2 !== 0 ? "col-6 mr-0 mt-2 pr-0" : "col-6 mr-0 mt-2 pl-0"}>
                                            <div
                                                className="flex-row inner-section space-between">
                                <span  className={item.active?"dark-title":"light-title"}>
                                    {item.name}
                                </span>
                                                <div className="flex-row">
                                                    <EditIcon onClick={() => openModal(item, "service")}/>
                                                    <Switch
                                                        className="margin-left-35"
                                                        checked={item.active}
                                                        onChange={() => openChanStatusModal(item)}
                                                        id={item.name}/>
                                                </div>
                                            </div>
                                        </div>
                                )}
                            </div>

                            : "Services not found"}
                    </div>

                    <Modal isOpen={showStatusModal && currentItem}
                           toggle={() => openChanStatusModal(null)}>
                        <ModalHeader toggle={() => openChanStatusModal(null)}
                                     charCode="x">Change status</ModalHeader>
                        <ModalBody>
                            Are you sure change
                            status {currentItem ? currentItem.percent ? currentItem.mainService.name : currentItem.name : ""}?
                        </ModalBody>
                        <ModalFooter>
                            <Button onClick={() => openChanStatusModal(null)} color="primary">No</Button>
                            <Button type="button" outline onClick={changeStatusModal}
                                    color="secondary">Yes</Button>
                        </ModalFooter>
                    </Modal>

                    <Modal id="allModalStyle" isOpen={showModal} toggle={() => openModal(null)}>
                        <AvForm onValidSubmit={saveMainServiceModal}>
                            <div className='position-absolute' style={{top :'7%', left : '-12%'}}>
                                <CloseBtn click={() => openModal(null)} />
                            </div>
                            <ModalHeader
                                toggle={openModal}
                                className="model-head"
                                charCode="x">{currentItem ? itemType === "publicHoliday" ? "Public holidays" : "Edit item" : "Add item"}</ModalHeader>
                            <ModalBody>
                                {itemType === "publicHoliday" ?
                                    <AvField
                                        className="modal-input"
                                        name={currentItem ? currentItem.name : "modal"}
                                        label="Name"
                                        disabled={true}
                                        defaultValue={
                                            currentItem ? currentItem.name : ""
                                        }
                                    />
                                    : ""}
                                {itemType === "publicHoliday" && holiday ?
                                    <h5 className="text-bold w-75 mx-auto">{holiday.name}</h5> : ""
                                }
                                {itemType === "publicHoliday" ?
                                    <div className="w-75 mx-auto">
                                        <Calendar
                                            onDayClick={handleDayClick}
                                            modifiers={modifiers}
                                            locale={enGB}/>
                                    </div>
                                    : ""
                                }
                                {mainServices && mainServices.length > 0 && itemType === "mainServiceWithPercent" ? (
                                    <AvField
                                        type="select"
                                        name={currentItem ? "mainService" : "mainService"}
                                        value={
                                            currentItem
                                                ? "/" + currentItem.mainServiceId
                                                : "0"
                                        }
                                        required
                                    >
                                        <option value="0" disabled>
                                            Select main services
                                        </option>
                                        {mainServices.map((item) => (
                                            <option
                                                key={item.id}
                                                value={"/" + item.id}
                                            >
                                                {item.name}
                                            </option>
                                        ))}
                                    </AvField>
                                ) : (
                                    ""
                                )}
                                {itemType === "mainService" ?
                                    <div>
                                        <AvField
                                            className="modal-input"
                                            name={currentItem ? currentItem.name : "modal"}
                                            label="Name"
                                            disabled={true}
                                            defaultValue={
                                                currentItem ? currentItem.name : ""
                                            }
                                        />
                                        <AvField
                                            className="modal-input"
                                            name="modaldescription"
                                            label="Description"
                                            disabled={true}
                                            defaultValue={
                                                currentItem
                                                    ? currentItem.description
                                                    : ""
                                            }
                                            placeholder="Enter description"
                                        />
                                    </div>
                                    : ""}
                                {itemType === "service" ?
                                    <div>
                                        <AvField
                                            className="modal-input"
                                            name={"name"}
                                            label="Name"
                                            defaultValue={
                                                currentItem ? currentItem.name : ""
                                            }
                                        />
                                        <AvField
                                            className="modal-input"
                                            name="description"
                                            label="Description"
                                            defaultValue={
                                                currentItem
                                                    ? currentItem.description
                                                    : ""
                                            }
                                            placeholder="Enter description"
                                        />
                                    </div>
                                    : ""}
                                {itemType === "service" || itemType === "publicHoliday" ? "" :
                                    <div className="flex-row-times margin-top-15">
                                        <AvField
                                            name="fromTime"
                                            type="time"
                                            className="half-left"
                                            label="Start time"
                                            required
                                            defaultValue={
                                                currentItem != null
                                                    ? currentItem.fromTime
                                                    : ""
                                            }
                                            placeholder="Enter from time"
                                        />
                                        <AvField
                                            name="tillTime"
                                            type="time"
                                            className="half-right"
                                            label="Till time"
                                            required
                                            defaultValue={
                                                currentItem != null
                                                    ? currentItem.tillTime
                                                    : ""
                                            }
                                            placeholder="Enter till time"
                                        />
                                    </div>

                                }
                                {itemType === "mainServiceWithPercent" ?
                                    <AvField
                                        type="number"
                                        name="percent"
                                        label="Percent:"
                                        required
                                        defaultValue={
                                            currentItem ? currentItem.percent : ""
                                        }
                                        placeholder="Enter percent"
                                    />
                                    : ""}

                            </ModalBody>
                            <ModalFooter>
                                {/*<Button onClick={() => openModal(null)} color="secondary">Cancel</Button>*/}
                                {itemType === "publicHoliday" ? "" :
                                    <button className='btn btn-outline-info'>Save</button>
                                }
                            </ModalFooter>
                        </AvForm>
                    </Modal>

                </div>
            </AdminLayout>
        );
    }
}


export default connect(({
                            auth: {currentUser, isAdmin, permissions},
                            app: {
                                selectedDates, setSelectedDates,
                                subServices, date, setDate, changeCalendar,
                                mainServices, holidays, showStatusModal, currentItem, holiday,
                                mainServicesWithPercent, showModal, active, online, itemType, selectHolidays
                            },
                            admin: {page, size}
                        }) => ({
        permissions,
        holiday,
        selectHolidays,
        changeCalendar,
        date, setDate,
        selectedDates, setSelectedDates,
        currentUser,
        itemType,
        active, online,
        showModal,
        isAdmin,
        mainServices,
        page,
        size,
        subServices,
        holidays,
        showStatusModal,
        currentItem,
        mainServicesWithPercent
    })
)(AdminMain)

// Reusable components


function DropdownIcon(props) {
    return (
        <div onClick={props.onClick}>
            <svg
                className={props.className}
                width="14"
                height="8"
                viewBox="0 0 14 8"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M6.99974 5.172L11.9497 0.222L13.3637 1.636L6.99974 8L0.635742 1.636L2.04974 0.222L6.99974 5.172Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}


function EditIcon(props) {
    return (
        <div onClick={props.onClick} className='cursor-p'>
            <svg
                width="18"
                height="18"
                viewBox="0 0 18 18"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M12.728 6.68599L11.314 5.27199L2 14.586V16H3.414L12.728 6.68599ZM14.142 5.27199L15.556 3.85799L14.142 2.44399L12.728 3.85799L14.142 5.27199ZM4.242 18H0V13.757L13.435 0.321992C13.6225 0.134521 13.8768 0.0292053 14.142 0.0292053C14.4072 0.0292053 14.6615 0.134521 14.849 0.321992L17.678 3.15099C17.8655 3.33852 17.9708 3.59283 17.9708 3.85799C17.9708 4.12316 17.8655 4.37746 17.678 4.56499L4.243 18H4.242Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}
