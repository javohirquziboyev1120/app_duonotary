import React, {Component} from 'react';
import {connect} from "react-redux";
import {
    Button,
    CustomInput,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table
} from "reactstrap";
import {
    getAdditionalServiceList,
    saveAdditionalService,
    deleteAdditionalService,
} from "../redux/actions/AppAction";
import {AvField, AvForm} from "availity-reactstrap-validation";
import DeleteModal from "../components/Modal/DeleteModal";
import StatusModal from "../components/Modal/StatusModal";


class AdditionalService extends Component {
    componentDidMount() {
        this.props.dispatch(getAdditionalServiceList());
    }

    render() {
        const {dispatch, showModal, additionalServices, currentItem, active, showDeleteModal, showStatusModal} = this.props;

        const openModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showModal: !showModal,
                    currentItem: item,
                    active: item.active
                }
            })
        };

        const openDeleteModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentItem: item
                }
            })
        };

        const changeActive = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    active: !active
                }
            })
        };

        const deleteFunction = () => {
            this.props.dispatch(deleteAdditionalService(currentItem))
        };

        const saveItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null
            v.active = active
            this.props.dispatch(saveAdditionalService(v))
        };

        const openStatusModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item
                }
            })
        };
        const changeStatusAdditionalService = () => {
            let currentAdditionalService = {...currentItem};
            currentAdditionalService.active = !currentItem.active;
            this.props.dispatch(saveAdditionalService(currentAdditionalService))
        }
        return (
            <div>
                <h2 className="text-center">Additional Service list</h2>
                <Button color="success" outline className="my-2" onClick={() => openModal('')}>+ New add Additional Service</Button>

                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th colSpan="2" className="">Operation</th>
                    </tr>
                    </thead>
                    {additionalServices.length > 0 ?
                        <tbody>
                        {additionalServices.map((item, i) =>
                            <tr key={item.id}>
                                <td>{i + 1}</td>
                                <td>{item.name}</td>
                                <td>{item.description}</td>
                                <td>
                                    <FormGroup check>
                                        <Label check>
                                            <Input type="checkbox" onClick={() => openStatusModal(item)} id="active"
                                                   checked={item.active}/>
                                            {item.active ? "Active" : "Inactive"}
                                        </Label>
                                    </FormGroup>
                                </td>
                                <td><Button color="warning" outline onClick={() => openModal(item)}>Edit</Button></td>
                                <td><Button color="danger" outline onClick={() => openDeleteModal(item)}>Delete</Button>
                                </td>

                            </tr>
                        )}
                        </tbody>
                        :
                        <tbody>
                        <tr>
                            <td colSpan="4">
                                <h3 className="text-center mx-auto"> No information </h3>
                            </td>
                        </tr>
                        </tbody>
                    }
                </Table>

                <Modal isOpen={showModal} toggle={openModal}>
                    <AvForm onValidSubmit={saveItem}>
                        <ModalHeader toggle={openModal}
                                     charCode="x">{currentItem != null ? "Edit Additional Service" : "Add Additional Service"}</ModalHeader>
                        <ModalBody>
                            <AvField name="name" label="Name" required
                                     defaultValue={currentItem != null ? currentItem.name : ""}
                                     placeholder="Enter Additional Service name"/>
                            <AvField name="description" label="Description" required
                                     defaultValue={currentItem != null ? currentItem.description : ""}
                                     placeholder="Enter description name"/>
                            <CustomInput type="checkbox" checked={active}
                                         onChange={changeActive}
                                         label="Is active?" id="stateActive"/>
                        </ModalBody>
                        <ModalFooter>
                            <Button type="button" color="secondary" outline onClick={openModal}>Cancel</Button>{' '}
                            <Button color="success">Save</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>

                {showDeleteModal && <DeleteModal text={currentItem != null ? currentItem.name : ''}
                                                 showDeleteModal={showDeleteModal}
                                                 confirm={deleteFunction}
                                                 cancel={openDeleteModal}/>}

                {showStatusModal && <StatusModal text={currentItem != null ? currentItem.name : ''}
                                                 showStatusModal={showStatusModal}
                                                 confirm={changeStatusAdditionalService}
                                                 cancel={openStatusModal}/>}
            </div>
        );
    }
}

export default connect(({
                            app: {
                                showModal, currentItem, active, showDeleteModal, showStatusModal, additionalServices
                            }
                        }) => ({
    showModal, currentItem, active, showDeleteModal, showStatusModal, additionalServices
}))(AdditionalService);