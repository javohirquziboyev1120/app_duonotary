import React, {Component} from "react";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {
    Button,
    Col,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
} from "reactstrap";
import {connect} from "react-redux";
import {getStateList} from "../redux/actions/AppAction";
import {uploadFile} from "../redux/actions/AttachmentAction";
import {
    pushCertificateArrayForRegister,
    removeCertificateRow,
    registerAgentAction, checkEmailAndPhoneNumber,
} from "../redux/actions/AgentAction";
import {toast} from "react-toastify";
import {Link} from "react-router-dom";
import {AddIcon, DeleteIconRed} from "../components/Icons";
import LeftMenu from "../components/LeftMenu";
import UploadFile from "../components/UploadFile";
import Modall from '../components/common/modal';
import Loader from "../components/Loader";
import {ATTACHMENT, REGISTER_AGENT} from "../utils/constants";
import UploadFileBig from "../components/UploadFileBig";

class RegisterAgent extends Component {


    constructor(props) {
        super(props);
        this.state = {
            step: 0,
            agent: {
                phoneNumber: ""
            },
            passport: '',
            loader: true,
            isOnline: false,
            isOpen : false,
        }
    }

    componentDidMount() {
        this.props.dispatch(getStateList());
        this.props.dispatch({
            type: 'updateState',
            payload: {certificates: [{id: "344fc027-7bb6-4302-b01d-9bfc6aeb8397"}],
            attachmentIdsArray:[ {
                key: "avatar",
                src: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAMFBMVEXBx9D///+9w83Y3OHDydL19vfS1t3q7O/IzdXt7/HN0tnd4OXGy9Tl5+v4+frg4+dnyPTjAAAKUUlEQVR4nN2d28KjKgyFGUTF8/u/7dba/tWWQ0IWSve6mYuZqX5yTEiC+pdfc9cuQ9X01o7GKGNGa/umGpa2my94usr543M3VdboVcql7S+Mraa8oLkI53boNzI324lzI+2HNhdmDsJ5aoyn2QKg2jRTDko4YVdZNt2b0lYd+oWwhG2jkvFekKppoe8EJNzwRHRvSiQkirCuQHhPSFXVoDfDEE4WifeEtBPk3QCE8wBtvgOjGgCTq5iwbvLgPSEbcWcVEublgzCKCOs+Nx+AUUA4Z2+/N6NgPKYTVlfxPRirywmnC/F2pa4daYT1eGUD7tJj2nBMIry0gx4Yk7pqAmF3C96uBMuDT3jZDOpSQjNyCTtzI98mwx2NTMLhzgbcpYeMhHMGE4IvbVnrP4fwzinmLM6EwyAsoIe+pJcchJfssqnSPZxwHu+G+tBIHYxEwvpuIIeIywaNsC2ph76kafMNiXAqEXBFJJkbFMKlTEDilEogLBaQhhgnLGgZ/BZhCxclLBqQghgjLLiL7op21AhhobPoUbEZNUz4A4BRxCBh9wuAsaU/RFj/BqAKb+BChHe/N0NphPbu12bIphD26Ld4hJXswh84+u1FLyF2IdRbmMXSdnU913XXLlvABvYB3mXRR4icRrVqpu+5oJ5QkQ37Q3wTqodwBj668U/mHdK97DH6PYSoWUabmA03GRSkZ7ZxE4K223E+JKNnE+4kxAxCTT7ymzAD0j0UnYSQswndEPk2YcajoRI2iKcpXuBWC3mm66M6CBGONR3YZLg1IyY37fisDkLEk1JOayEnyxTCSv4YzrHCQYht1Pen/SIEmEw0P6ZDAINbf22evgjl5xPJgBDEMUYof0ZiF90l76hf3/eTUPoASfTSJsB0EyaUTzPsZeJD8kXj4xOfCWf4F+RL/Ab6bGSc30i8myGeeIUk3xSfdzYnQvlKIRuEu8Qj5bxinAjlrhkAIKCfnpw2x3cSN6FgJTxKvGKdGvFIKG5C6Tz6kng+PTbigVDehKhMF7F1c2zEA6F4Iv3aMCVLvHU8TKdvQvFaCBqFm+Qj8b0mvgkH4Y+CJtLna0n19kq9X6uItfAl+fb0mxA7RUsFXLj+CMUztNPRlSyxu+9v5XoRyj8aspMCuulfl1KwX8Qm8Ir3339f/EUo/L0vm0UqnB33/FPuI0Xt2F4SL/qvHdaTUO7m5vjwKYK90ZNQ3ick/ieXFvEb6SOhvJPCdt0vwV5pJ5R3CfBUCjnhaw6E4h/D7mg2IXzvb0LA9wIvFpDlYu9XD0KAG1aDARGT377oPwgBR3clEu5r9EYI6BBlEj6GzkaIiCItcRzuJtRGiDi3L5LwsV5shIjQixJXi91mVaCvVeCeRu09S6GSmsrbl6r9uytIaALcxEfl/FcPQkyUHto+hL2Vgiw8Cr8gwt5KYSaa8vw0z7eaV0JU9iQzTT4iuQf+ofW7K8ykpZDnMptQIbzLSoiJRATvakBDZ9vVKFxaBXJFRHWsdTJVmHDZTchuCsuNNysh6reQsykwF+KfAqZv0escxITL19G1An4umH0B/Oq6U8iiXahGRKZcTQo2aynYSIQmdi4KmquN2X4ji4zoQUFsp7/fQ6yJ2Ky5SqG2NLsAGxvYdmZXo8CJlPJ+Ci6E0yt0LqzU1oeOmlUWTiiMjIJXALAKXh1JtGTgKwBYha+hJ9jaZKgAYDIQpiPmKHGQqQpiWkfNVKQiC2OSBzxPmZEsvVQlOYgzlX01+Ll0F7N8Y76ikyN8PXyLszDmK7yMX/Hf0pY6p9YZq4Za9L70JFql8byVz3uwbfEhHa8Yn7syf4O1Dx0KX1OR42KMsyqsje+U1r2jtMnaessFJVFXGx/ppwk8SPWHm6u2m676TNd+fGqB+trCehQXMsYo7yVeOTQh/aUlSndIn3eJ0jXw3KJMIc+eipRBnh8WKQs8Ay5TDfAcv0wtwFiMIqVbXDxNmXrE04Cij8qUBsa1lSmLi00sVBUwvrRIPeNL/8dTzTNG+H+8b3vGeSN2NTqH5K/1itWXudO1Gvsqj/pR5gj4y7dIH4ju6rJI1YugUu1fzkzqiqgtOgXBrWSH3F/eU9qhiO7ztt5RadeBHnLXEnw12sIv0A6qS2jHQ/4h35PBvfwMIH5HO+SQ8teLaxtwF/tStGMeMHPjRr5NCivmrVqnXG6eBYVOj6GLNemf8vFZ3RRbpoUnzgbzXFOB003v6aK7GLXiP+pi0GdTeGkBnhgL24vs+Sd5LkZn4XFFtde/6tNQjy+wuT8pIk6oXzWGiNPUzX10E7GfftWJIppQuJSKdJFiKxy1vkhLYgFNSGzEd8Inr+befWv9UZQB5aq5R7GDcZURJSKctDjrJhL2NfDCCWkitIWz9iVhwSijkxK6qad+aXSSgufcpyq6PfHUoI02IrwyRKpiu2hvHeFYI8Kre6Qq1hTeWtCx/1nIRBOdagL1vGPT6aUYIYVfM1CTPfJx7jR9zwoawsG6+mHb5EcIg3cjhNv/Rwg//i3njpKfIIzeURIyMH+CMHrPTGjF+AVCwl1BgcnmFwgJ9z0FJptfIPz+t5x718onJN675t3ZlE9IvDvP+wPFE5LvP/T5ekonZNxh6bmHtHBCzj2kPj8BunJgspxvx7pL1nPGc8PZtlPuTsq7D9gzFItAnN19lHmns6/CSAHOqNrdvdj3cvucNqw7cHPIE6+QcLe61yvJTGEGy2PdBTy5AULvifKNLjefpzTw1UPeJZ8hBbzYiSlP8FfQzRn0n/nOsW4ajL6QofCZX9hD6PVp3DEYffWjIl0q4gP1Il7u4fcWXYiNmZiX11t46+Ke6r2ZPFpeLOrH9uZ6a+bt6RL5ixLEd1lxT70/nZ1WMgGgyRsITdhGEs4i/BXi9CXH3oGqGZQKeJTTloCXWI/ZozMCx6GkhZl0nhRyhGcO9w6VGKTN57QTs2AIS8bhOJnQg2ndh3gm6DZZXoi6ysIY5qNuj8mnnsGAOUKVFraWMB85LoR+rhtJedA9cnmcq3CmjKYH2DFOrmN1XrRZQJ21jSWQcLwpnLP5eMgcoiHrSPMpZgAhK/qAUHJMq0YCWQ9j/BE8w4YZX0GpSLRBJnXXbqCk/nD9fdwIko6UD6C1HXibnW4hFh0y3E0UP0aGWptL67EiJSfWbWWpCaMJNltCFBAn/2jF3ApEuUHnbhoay0mHZTdgGiE3jUw/soSN7ZumGoahqqqm6a3hp/qmuaPTIrlSywA+/ldiCjO9SCGCMGcpR59STdH0aLxM9UbdEpyXCOIN81Z0PPFJ7DNRRGVaAjKbT2ZjC2NG8zOKfQjiqNi81TkBdicg7nceMhV51GoAmGOYyOYcZUjDhU/pQsVuE6w6Fp6qUG4RYHR6K6jR8YEnsjE/hI2/3yBllBqL9w9NuKqjm0IOPFvBfeg5cijmqTFsytX6aKYcbtdcWSJzO/RU62j9d/2Q5vggKGsezNwtjX3UDfaRKWObpct6SHdFpk/dtctQrVavHY1Rxox2tYarYWk9tj9W/wHyKYDIdACaHQAAAABJRU5ErkJggg==",
                value: ''
            },
                {
                    key: "passport",
                    src: "/assets/img/no-image.jpg",
                    value: ''
                },
                {
                    key: "344fc027-7bb6-4302-b01d-9bfc6aeb8397",
                    src: "/assets/img/no-image.jpg",
                    value: ''
                },]
            }
        });
        let state = localStorage.getItem(REGISTER_AGENT)
        let attach = localStorage.getItem(ATTACHMENT)
        if (attach) {
            this.props.dispatch({type: 'updateState', payload: {attachmentIdsArray: JSON.parse(attach)}})
        }
        if (state) {
            this.setState(JSON.parse(state))
        }

        setTimeout(() => {
            this.setState({loader: false})
        }, 2000)
    }

    updateState = (obj) => {
        this.setState(obj)
    }

    formatPhone=(e)=>{
        let input = e.target.value;
        // if( input.charAt(input.length-1).toUpperCase() != input.charAt(input.length-1).toLowerCase() ){
        //     return this.setState({
        //         agent: {...this.state.agent, phoneNumber: input.substr(0, input.length-1)}
        //     });
        // } else{
            let output = "(";
            input.replace( /^\D*(\d{0,3})\D*(\d{0,3})\D*(\d{0,4})/, function( match, g1, g2, g3 )
                {
                    if ( g1.length ) {
                        output += g1;
                        if ( g1.length == 3 ) {
                            output += ")";
                            if ( g2.length ) {
                                output += " " + g2;
                                if ( g2.length == 3 ) {
                                    output += " - ";
                                    if ( g3.length ) {
                                        output += g3;
                                    }
                                }
                            }
                        }
                    }
                }
            );
            output = output.trim();
            while(output.charAt(output.length-1)==="-"||output.charAt(output.length-1)===")"||output.charAt(output.length-1)==="%"){
                output=output.substring(0, output.length-1);
                output = output.trim();
            }
            return this.setState({
                agent: {...this.state.agent, phoneNumber: output==="("?"":output}
            });
        // }


    }

    nextStep = () => {
        if (this.state.step < 2) {
            this.setState({
                step: this.state.step + 1
            })
            localStorage.setItem(REGISTER_AGENT, JSON.stringify(this.state))
            localStorage.setItem(ATTACHMENT, JSON.stringify(this.props.attachmentIdsArray))
        }
    }

    previousStep = () => {
        if (this.state.step > 0) {
            this.setState({
                step: this.state.step - 1
            })
        }
    }
    checkExpire = (date) => {
        let a = new Date().toJSON().slice(0, 10);
        return (
            new Date(
                a.substring(0, 4),
                a.substring(5, 7),
                a.substring(8)
            ) <=
            new Date(
                date.substring(0, 4),
                date.substring(5, 7),
                date.substring(8)
            )
        );
    };

    handleChangeModal = () => {
        this.setState({isOpen : !this.state.isOpen})
    };

    render() {
        const {
            dispatch,
            attachmentIdsArray,
            certificates,
            states,
            resMessage,
        } = this.props;
        const {isOnline} = this.state;
        const addCertificateRow = () => {
            if (certificates.length < 3) {
                const aa = require("uuid/v4");
                let obj = {id: aa(), values: []};
                dispatch(pushCertificateArrayForRegister(obj));
            } else toast.error("You cannot add Certificate");
        };

        const stepOne = (e, v) => {
            if (v.password !== v.prePassword) {
                toast.error("Passwords does not match");
                return "";
            } else {
                    v.phoneNumber ="+1"+ this.state.agent.phoneNumber.replace("-", "").replace(/\s/g, '').replace("(", "").replace(")", "");
                console.log(v.phoneNumber);

                dispatch(checkEmailAndPhoneNumber(v)).then(res => {
                    let {message, success} = res.payload;
                    if (success) {
                        if (!attachmentIdsArray.filter(item => item.key === 'avatar')[0].value) {
                            toast.info("Please, upload avatar")
                        }
                        v.photoId = attachmentIdsArray.filter((item) => item.key === "avatar")[0].value;
                        this.updateState({agent: v});
                        this.nextStep();
                    } else toast.error(message)
                })
            }

        }
        const stepTwo = (e, v) => {
            if (!this.checkExpire(v.expireDate)) {
                toast.error("invalid expire date");
                return "";
            }
            if (!attachmentIdsArray.filter(item => item.key === 'passport')[0].value) {
                toast.error("You must upload photo of your ID")
                return "";
            }
                // else if(this.checkExpire(v.issueDate)){
                //     toast.error("Invalid issue date");
                //     return "";
            // }
            else {
                v.issueDate = new Date();
                v.attachmentId = attachmentIdsArray.filter((item) => item.key === "passport")[0].value;
                this.setState({passport: v})
                this.nextStep()
            }

        }
        const saveAll = (e, v) => {
            let successCount = 0;
            let certificateDtoList = [];
            let count = 0
            if (!isOnline) {
                Object.keys(v).map((item) => {
                    if (item.includes("/expireDate") && this.checkExpire(v[item])) {
                        count++;
                    }
                    // if (item.includes("/issueDate") && !this.checkExpire(v[item])) {
                    //     count++;
                    // }
                });
                // if (certificates.length !== count) {
                //     toast.error("Your certificates  have expired  ");
                //     return "";
                // }

                certificates.map((certificate, i) => {
                    let certificateForPush = {};
                    Object.keys(v).map((item, i) => {
                        if (item.includes(certificate.id)) {
                            // if (item.includes(certificate.id + "/issueDate")) {
                            //     certificateForPush.issueDate = v[item];
                            // }
                            if (item.includes(certificate.id + "/expireDate")) {
                                if (this.checkExpire(v[item])) {
                                    certificateForPush.expireDate = v[item];
                                } else {
                                    toast.error('expired expiration date of 0' + (i + 1) + '# certificate')
                                    successCount--
                                }
                            }
                            if (item.includes(certificate.id + "/stateId")) {
                                certificateForPush.stateDto = {};
                                certificateForPush.stateDto.id = v[item];
                                console.log(v[item]);
                            } else {
                                certificateForPush.issueDate = new Date();
                            }
                        }
                    });
                    attachmentIdsArray.map((item, i) => {
                        if (item.key === certificate.id) {
                            certificateForPush.attachmentId = item.value;
                        }
                    });
                    if (certificateForPush.attachmentId === 'i') {
                        toast.error('You must upload photo of #0' + (i + 1) + ' certificate');
                        return --successCount;
                    }
                    if (certificateForPush.stateDto && certificateForPush.stateDto.id === '') {
                        toast.error('You must select state of #0' + (i + 1) + ' certificate');
                        return --successCount;
                    }
                    if (certificateForPush.expireDate === '') {
                        toast.error('You must select expiration date of #0' + (i + 1) + ' certificate');
                        return --successCount;
                    }
                        // else if(certificateForPush.issueDate != null) {
                        //     toast.error('You must select issueDate date of '+i+1+'# certificate');
                        //     return '';
                    // }
                    else {
                        certificateDtoList.push(certificateForPush);
                        successCount++
                    }
                });
            }
            v.passportDtoList = [this.state.passport];
            v.certificateDtoList = certificateDtoList;
            v.onlineAgent = isOnline;
            if (isOnline || count === successCount) dispatch(registerAgentAction({...this.state.agent, ...v}));
        };
        const pushToHome = () => {
            dispatch({
                type: "updateState",
                payload: {
                    showModal: false,
                },
            });
            if (resMessage.success) {
                window.location.replace("/");
                localStorage.removeItem("attach");
                localStorage.removeItem("registerAgent")
            }
        };
        const upload = (file, key) => {
            if (file !== undefined) {
                dispatch(uploadFile(file, key));
            } else toast.error("Selected none");
        };
        let {passport, agent} = this.state;

        if (this.state.loader) return <Loader/>

        return (
            <div className="d-flex align-items-start">
                <LeftMenu
                    onToggleModal={this.handleChangeModal}
                    show={this.state.isOpen}
                />
                <div className="register-agent-page">
                    <div className="flex-column form-section mx-auto">
                        <Link to={'/'} className={'icon_back'}>
                            <i className="fas fa-long-arrow-alt-left"/>
                        </Link>
                        <i className="fas fa-bars mobileMenu" onClick={this.handleChangeModal}/>
                        <h3 className="form-section-title mx-auto">
                            Agent Registration
                        </h3>
                        <div className="d-flex stepCounter">
                            <Col className={this.state.step !== 0 ? "mobile-display-none" : ''}>
                                <div className={"circle-active"}>
                                    <div className="active-step">
                                        <span className="icon icon-timeOrder"/>
                                    </div>
                                </div>
                                <div className="step-text">
                                    Agent info
                                </div>
                            </Col>
                            <Col className="mobile-display-none">
                                <div className={this.state.step > 0 ? "bordered-line-active" : "bordered-line"}/>
                            </Col>
                            <Col className={this.state.step !== 1 ? "mobile-display-none" : ''}>
                                <div className={this.state.step > 0 ? "circle-active" : "circle"}>
                                    <div className="steps">
                                        <span className="icon icon-order"/>
                                    </div>
                                </div>
                                <div className="step-text">
                                    Photo ID
                                </div>
                            </Col>
                            <Col className="mobile-display-none">
                                <div className={this.state.step > 1 ? "bordered-line-active" : "bordered-line"}/>
                            </Col>
                            <Col className={this.state.step !== 2 ? "mobile-display-none" : ''}>
                                <div className={this.state.step > 1 ? "circle-active" : "circle"}>
                                    <div className="steps">
                                        <span className="icon icon-order"/>
                                    </div>
                                </div>
                                <div className="step-text">
                                    Notary Certificates
                                </div>

                            </Col>
                        </div>

                        {
                            this.state.step === 0 ?
                                <AvForm className="w-100 fourKVersion" onValidSubmit={stepOne}>
                                    <div className="flex-column">
                                        <div
                                            className="d-flex flex-column flex-md-row align-items-center">
                                            <div className="w-25 mb-sm-2 m-md-0 mb-2">
                                                <img
                                                    className="form-label avatar-img"
                                                    src={
                                                        attachmentIdsArray.filter(
                                                            (item) => item.key === "avatar"
                                                        )[0].src
                                                    }
                                                    alt="agent profile image"
                                                />
                                            </div>

                                            <div className="w-100 ml-md-3 m-sm-0">
                                                <UploadFile
                                                    name="avatarId"
                                                    onChange={(item) =>
                                                        upload(item.target.files[0], "avatar")
                                                    }
                                                    required={true}
                                                    title="Upload avatar"
                                                />
                                            </div>
                                        </div>
                                        <div
                                            className="d-sm-flex flex-sm-column d-md-flex flex-md-row w-100 mt-2 space-between align-items-start">
                                            <div className="w-100 blockInput">
                                                <AvField
                                                    className='modal-blockInput'
                                                    name="firstName"
                                                    placeholder="First name"
                                                    required
                                                    defaultValue={agent && agent.firstName}
                                                    errorMessage=" "
                                                />
                                            </div>
                                            <div className="w-100 blockInput">
                                                <AvField
                                                    className='modal-blockInput'
                                                    name="lastName"
                                                    placeholder="Last name"
                                                    errorMessage=" "
                                                    defaultValue={agent && agent.lastName}
                                                    required
                                                />
                                            </div>
                                        </div>

                                        <div className="d-sm-flex flex-sm-column d-md-flex flex-md-row w-100 mt-2">
                                            <div className="w-100 blockInput">
                                                <input
                                                    className='modal-blockInput'
                                                    name="phoneNumber"
                                                    placeholder="Phone number ex. (233)323-3232"
                                                    onChange={this.formatPhone}
                                                    value={agent && agent.phoneNumber}
                                                    required
                                                    type="text"
                                                />
                                            </div>


                                            <div className="w-100 blockInput">
                                                <AvField
                                                    className='modal-blockInput'
                                                    errorMessage=" "
                                                    name="email"
                                                    type="email"
                                                    defaultValue={agent && agent.email}
                                                    placeholder="Email"
                                                    required
                                                />
                                            </div>
                                        </div>

                                        <div
                                            className="d-sm-flex flex-sm-column d-md-flex flex-md-row mt-2 space-between w-100">
                                            <div className="w-100 blockInput">
                                                <AvField
                                                    className='modal-blockInput'
                                                    name="password"
                                                    type="password"
                                                    placeholder="Enter Password"
                                                    errorMessage=" "
                                                    defaultValue={agent && agent.password}
                                                    required
                                                />
                                            </div>
                                            <div className="w-100 blockInput">
                                                <AvField
                                                    className='modal-blockInput'
                                                    name="prePassword"
                                                    type="password"
                                                    errorMessage=" "
                                                    placeholder="Re-enter password"
                                                    defaultValue={agent && agent.prePassword}
                                                    required
                                                />
                                            </div>
                                        </div>


                                    </div>
                                    <div className="flex-column">
                                        <Button
                                            className="save-btn align-self-end"
                                        >
                                            Next
                                        </Button>

                                    </div>
                                </AvForm> : ''
                        }

                        {
                            this.state.step === 1 ?
                                <AvForm className="w-100 fourKVersion_2" onValidSubmit={stepTwo}>
                                    <div className="d-flex responsiveVersion justify-content-between h-300">
                                        <div className="imgFrame p-1">
                                            {attachmentIdsArray.filter((item) => item.key === "passport")[0].value === '' ?
                                                <div className="w-100">
                                                    <UploadFileBig
                                                        name="passportAttachment"
                                                        onChange={(item) =>
                                                            upload(item.target.files[0], "passport")
                                                        }
                                                        required={true}
                                                        title="Upload Photo ID"
                                                    />
                                                </div>
                                                : <img
                                                    className="w-100 img-frame"
                                                    src={
                                                        attachmentIdsArray.filter(
                                                            (item) => item.key === "passport"
                                                        )[0].src
                                                    }
                                                />}
                                        </div>
                                        <div className="dateTime">
                                            {/*<div className="w-100 mb-2">*/}
                                            {/*    <AvField*/}
                                            {/*        name="issueDate"*/}
                                            {/*        errorMessage=" "*/}
                                            {/*        type="date"*/}
                                            {/*        required*/}
                                            {/*        defaultValue={passport&&passport.issueDate}*/}

                                            {/*    />*/}
                                            {/*</div>*/}
                                            <div className="w-100">
                                                <AvField
                                                    className='dateTimeInput'
                                                    errorMessage=" "
                                                    label="Expiration date"
                                                    name="expireDate"
                                                    type="date"
                                                    required
                                                    defaultValue={passport && passport.expireDate}

                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="d-flex justify-content-between registerBtn">
                                        <Button
                                            type="button"
                                            onClick={this.previousStep}
                                            className="save-btn margin-top-25"
                                            disabled={this.state.step === 0}
                                        >
                                            Previous
                                        </Button>
                                        <Button
                                            type="submit"
                                            className="save-btn margin-top-25">
                                            Next
                                        </Button>
                                    </div>
                                </AvForm> : ''
                        }

                        {
                            this.state.step === 2 ?
                                <AvForm className="w-100" onValidSubmit={saveAll}>
                                    {/*<Row><Col md={12} className="p-3">*/}
                                    {/*    <Input type="checkbox"*/}
                                    {/*           checked={isOnline}*/}
                                    {/*           onClick={() => this.setState({isOnline: !isOnline}, () => {*/}
                                    {/*               console.log(isOnline)*/}
                                    {/*           })}/>*/}

                                    {/*</Col></Row>*/}

                                    {!isOnline ? <div className="flex-column">
                                        {certificates && certificates.map((item, i) => (
                                            <div className="flex-column" key={item.id}>
                                                <div className="row text-center">
                                                    <Col
                                                        md={12}
                                                        className="text-center certificate"
                                                    >
                                                        Certificate #0{i + 1}
                                                    </Col>
                                                </div>
                                                <div
                                                    className='d-flex align-items-center justify-content-between register-agent-page_3'>
                                                    <div className="imgFrame p-1">
                                                        {
                                                            attachmentIdsArray.filter((att) => att.key === item.id)[0].value ?
                                                                <img
                                                                    className="w-100 img-frame"
                                                                    src={
                                                                        attachmentIdsArray.filter(
                                                                            (att) => att.key === item.id
                                                                        )[0].src
                                                                    }
                                                                    alt=""
                                                                /> : <div className="w-100">
                                                                    <UploadFileBig
                                                                        name="attachmentId"
                                                                        onChange={(att) =>
                                                                            upload(
                                                                                att.target.files[0],
                                                                                item.id
                                                                            )
                                                                        }
                                                                        required={true}
                                                                        title="Upload certificate photo"
                                                                    />
                                                                </div>
                                                        }
                                                    </div>
                                                    <div className='w-40 blockServBtn'>
                                                        <div className="d-flex justify-content-around">
                                                            <DeleteIconRed
                                                                onClick={() => {
                                                                    if (certificates.length > 1)
                                                                        dispatch(
                                                                            removeCertificateRow(item)
                                                                        );
                                                                    else toast.error("Error");
                                                                }}
                                                            />
                                                            {i + 1 === certificates.length && (
                                                                <AddIcon
                                                                    onClick={addCertificateRow}
                                                                />
                                                            )}
                                                        </div>
                                                        <div className="d-flex flex-column">
                                                            {/*<div className="w-100 my-2">*/}
                                                            {/*    <AvField*/}
                                                            {/*        name={item.id + "/issueDate"}*/}
                                                            {/*        errorMessage=" "*/}
                                                            {/*        type="date"*/}
                                                            {/*        required*/}
                                                            {/*    />*/}
                                                            {/*</div>*/}
                                                            <div className="w-100 my-2">
                                                                <label htmlFor="dateTimeInput_2"
                                                                       className='label_3'>Expiration date</label>
                                                                <AvField
                                                                    id='dateTimeInput_2'
                                                                    name={item.id + "/expireDate"}
                                                                    type="date"
                                                                    errorMessage=" "
                                                                    required
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div
                                                    className="d-flex margin-top-15 justify-content-end align-items-center">

                                                    <div className="w-100">
                                                        <AvField
                                                            id='dateTimeInput_3'
                                                            name={item.id + "/stateId"}
                                                            type="select"
                                                        >
                                                            <option value="0" selected>
                                                                Select states
                                                            </option>
                                                            {states
                                                            && states.map((item) => (
                                                                <option
                                                                    key={item.id}
                                                                    value={item.id}
                                                                >
                                                                    {item.name}
                                                                </option>
                                                            ))
                                                            }
                                                        </AvField>
                                                    </div>
                                                </div>

                                            </div>
                                        ))}
                                    </div> : ''}
                                    <div className="d-flex justify-content-between margin-top">
                                        <Button
                                            type="button"
                                            onClick={this.previousStep}
                                            className="save-btn margin-top-25"
                                            disabled={this.state.step === 0}
                                        >
                                            Previous
                                        </Button>
                                        <Button
                                            type="submit"
                                            onClick={() => saveAll("", {})}
                                            className="save-btn submit-btn margin-top-25"
                                        >
                                            Submit
                                        </Button>

                                    </div>
                                </AvForm> : ''
                        }


                    </div>
                </div>
                <Modall
                    onToggleModal={this.handleChangeModal}
                    visiblity={this.state.isOpen}
                />

                <Modal isOpen={resMessage.message}>
                    <ModalHeader>Congratulation</ModalHeader>
                    <ModalBody>
                        <p>{resMessage.message}</p>
                    </ModalBody>
                    <ModalFooter>
                        <Button onClick={pushToHome}>Ok</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

RegisterAgent.propTypes = {};

export default connect(
    ({
         app: {states},
         agent: {certificates, resMessage, showModal},
         attachment: {attachmentIdsArray},
         auth: {modalShow}
     }) => ({
        states,
        attachmentIdsArray,
        certificates,
        resMessage,
        showModal,
        modalShow,
    })
)(RegisterAgent);


