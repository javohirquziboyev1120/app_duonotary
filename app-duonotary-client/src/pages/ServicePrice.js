import React, {Component} from 'react';
import {AvField, AvForm} from 'availity-reactstrap-validation';
import {
    Button,
    CustomInput,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table
} from 'reactstrap'
import {connect} from "react-redux";
import {
    getServicePriceList,
    saveServicePrice,
    getStateList,
    getServiceList,
    getCountyByState, getZipCodeByCounty, editServicePriceActive, getMainServiceList
} from "../redux/actions/AppAction";
import StatusModal from "../components/Modal/StatusModal";
import Select from "react-select";

class ServicePrice extends Component {
    componentDidMount() {
        this.props.dispatch(getMainServiceList()).then(() => {
            this.props.dispatch(getServicePriceList({
                mainServices: this.props.mainServices
            }));
        });
        this.props.dispatch(getServiceList())
        this.props.dispatch(getStateList())
    }

    render() {
        const {
            selectZipCodes, selectCounties, selectStates,
            dispatch, stateOptions, countyOptions, zipCodeOptions, showServiceModal,
            services, servicePrices, currentService, all, active, showStatusModal
        } = this.props;

        const saveItem = (e, v) => {
            v.id = currentService != null ? currentService.id : null
            v.active = active
            v.all = all
            v.statesId = selectCounties !== null ? [] : selectStates
            v.countiesId = selectZipCodes !== null ? [] : selectCounties
            v.zipCodesId = selectZipCodes !== null ? selectZipCodes : []
            this.props.dispatch(saveServicePrice(v))
        }
        const changeStatusServicePrice = () => {
            let currentServicePrice = {...currentService};
            currentServicePrice.active = !currentService.active;
            this.props.dispatch(editServicePriceActive(currentServicePrice))
        }

        const openModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showServiceModal: !showServiceModal,
                    currentService: item,
                    active: item.active,
                    all: item.all,
                }
            })
        };
        const openStatusModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showStatusModal: !showStatusModal,
                    currentService: item
                }
            })
        };

        const changeActive = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    active: !active
                }
            })
        };
        const changeAll = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    all: !all
                }
            })
        };
        const getCounty = (e, v) => {
            let arr = [];
            if (v.action === "remove-value") {

            } else {
                dispatch(getCountyByState(v))
                e.map((item) => arr.push(item.value))
            }
            dispatch({
                type: 'updateState',
                payload: {
                    selectStates: arr
                }
            })
        };
        const getZipCode = (e, v) => {
            let arr = [];
            if (v.action === "remove-value") {

            } else {
                dispatch(getZipCodeByCounty(v))
                e.map((item) => arr.push(item.value))
            }
            dispatch({
                type: 'updateState',
                payload: {
                    selectCounties: arr
                }
            })
        };
        const saveZipCodeForServicePrice = (e, v) => {
            let arr = [];
            if (v.action === "remove-value") {

            } else {
                e.map((item) => arr.push(item.value))
            }
            dispatch({
                type: 'updateState',
                payload: {
                    selectZipCodes: arr
                }
            })
        };
        return (
            <div>
                <h2 className="text-center">ServicePrice list</h2>
                <Button color="success" outline className="my-2" onClick={() => openModal('')}>+ New Add</Button>
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Service</th>
                        {/*<th>SubService</th>*/}
                        <th>ZipCode</th>
                        <th>Price</th>
                        <th>ChargePercent</th>
                        <th>ChargeMinute</th>
                        <th>Active</th>
                        <th>Operation</th>
                    </tr>
                    </thead>
                    {servicePrices != null ?
                        <tbody>
                        {servicePrices.map((item, i) =>
                            <tr key={item.id}>
                                <td>{i + 1}</td>
                                <td>{item.serviceDto.mainServiceDto.name} - {item.serviceDto.subServiceDto.name}</td>
                                {/*<td>{item.serviceDto.subServiceDto.name}</td>*/}
                                <td>{item.zipCodeDto.code}</td>
                                <td>{item.price}</td>
                                <td>{item.chargePercent}</td>
                                <td>{item.chargeMinute}</td>
                                <td>
                                    <FormGroup check>
                                        <Label check>
                                            <Input type="checkbox" onClick={() => openStatusModal(item)}
                                                   id="countryActive"
                                                   checked={item.active}/>
                                            {item.active ? "Active" : "Inactive"}
                                        </Label>
                                    </FormGroup>
                                </td>
                                <td><Button color="warning" outline onClick={() => openModal(item)}>Edit</Button></td>
                            </tr>
                        )}
                        </tbody> :
                        <tbody>
                        <tr>
                            <td colSpan="4">
                                <h3 className="text-center mx-auto"> No information </h3>
                            </td>
                        </tr>
                        </tbody>
                    }
                </Table>
                <Modal isOpen={showServiceModal} toggle={openModal}>
                    <AvForm onValidSubmit={saveItem}>
                        <ModalHeader toggle={openModal}
                                     charCode="x">{currentService != null ? "Edit ServicePrice" : "Add ServicePrice"}</ModalHeader>
                        <ModalBody>
                            <AvField type="select" name="serviceId"
                                     value={currentService != null ? (currentService.serviceDto != null ? currentService.serviceDto.mainServiceDto.name+" "+currentService.serviceDto.subServiceDto.name : currentService.serviceId) : "0"}
                                     required>
                                <option value="0" selected >Select Service</option>
                                {services.map(item =>
                                    <option key={item.id} value={item.id}>{item.mainServiceDto.name} - {item.subServiceDto.name}</option>
                                )}
                            </AvField>
                            {/*<AvField type="select" name="serviceId"*/}
                            {/*         value={currentService != null ? (currentService.serviceDto != null ? currentService.serviceDto.subServiceDto.name : currentService.serviceId) : "0"}*/}
                            {/*         required>*/}
                            {/*    <option value="0" selected >Select subService</option>*/}
                            {/*    {services ? services.map(item =>*/}
                            {/*        <option key={item.id} value={item.id}>{item.subServiceDto.name}</option>*/}
                            {/*    ) : ""}*/}
                            {/*</AvField>*/}
                            <AvField name="price" label="Price" required
                                     defaultValue={ currentService.price}
                                     placeholder="Enter price"/>
                            <AvField name="chargeMinute" label="Charge minute" required
                                     defaultValue={ currentService.chargeMinute }
                                     placeholder="Enter chargeMinute"/>
                            <AvField name="chargePercent" label="chargePercent" required
                                     defaultValue={ currentService.chargePercent}
                                     placeholder="Enter chargePercent"/>

                            <CustomInput type="checkbox" checked={all}
                                         onChange={changeAll}
                                         label={all ? 'Add all zip code' : 'No all zip code'} id="all"/>
                            <CustomInput type="checkbox" checked={active}
                                         onChange={changeActive}
                                         label={active ? 'Active' : 'Inactive'} id="active"/>

                            <Select
                                isDisabled={all}
                                defaultValue="Select State"
                                isMulti
                                name="statesId"
                                options={stateOptions}
                                onChange={getCounty}
                                className="basic-multi-select"
                                classNamePrefix="select"
                            />
                            {selectStates !== null && selectStates.length === 1 ?
                                <Select
                                    isDisabled={all}
                                    defaultValue="Select County"
                                    isMulti
                                    name="countiesId"
                                    options={countyOptions}
                                    onChange={getZipCode}
                                    className="basic-multi-select"
                                    classNamePrefix="select"
                                /> : ""}
                            {selectCounties !== null && selectCounties.length === 1 ?
                                <Select
                                    isDisabled={all}
                                    defaultValue="Select Zipcode"
                                    isMulti
                                    name="zipCodesId"
                                    options={zipCodeOptions}
                                    onChange={saveZipCodeForServicePrice}
                                    className="basic-multi-select"
                                    classNamePrefix="select"
                                /> : ""}
                        </ModalBody>
                        <ModalFooter>
                            <Button type=" button" color="" onClick={openModal}>Cancel</Button>{' '}
                            <Button color=" success">Save</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>

                {showStatusModal && <StatusModal text={currentService != null ? currentService.name : ''}
                                                 showStatusModal={showStatusModal}
                                                 confirm={changeStatusServicePrice}
                                                 cancel={openStatusModal}/>}

            </div>


        );
    }
}


export default connect(
    ({
         service: {
             zipCodeOptions, countyOptions, servicePrices, showServiceModal, currentService, all,
             active, selectZipCodes, selectCounties, selectStates,
         },
         app: {
             stateOptions,
             zipCodes,
             states, counties, showDeleteModal,
             showStatusModal, services,
         }
     }) => ({
        selectZipCodes, selectCounties, selectStates,
        stateOptions, countyOptions, zipCodeOptions,
        zipCodes,
        states,
        counties,
        showServiceModal,
        currentService, all,
        active, showDeleteModal, showStatusModal, services, servicePrices
    }))(ServicePrice);
