import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {addOrEditUserZipCode, deleteZipCode, getUsersByZipCode, getZipCode} from "../redux/actions/AppAction";
import {Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import {config} from "../utils/config";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {getAgents} from "../redux/actions/AgentAction";

class UserZipCode extends Component {
    componentDidMount() {
        if (this.props.currentItem==null) {
            this.props.dispatch(getZipCode(window.location.pathname.substring(13)));
        }
        this.props.dispatch(getUsersByZipCode(window.location.pathname.substring(13)));
    }

    render() {
        let {currentItem,agents,dispatch,showModal}=this.props;
        const deleteZipCode1=(item)=>{
            dispatch(deleteZipCode(currentItem))
        }
        const openModal=(boool,all)=>{
            if (all)dispatch(getAgents());
            else dispatch(getUsersByZipCode(window.location.pathname.substring(13)));
            dispatch({type:'updateState',payload:{showModal:boool}})
        }
        const addAgent=(e,v)=>{
            v.userDto={id:v.id}
            v.zipCodesId=[currentItem.id]
            dispatch(addOrEditUserZipCode(v))
        }
        return (
            <div>
                {currentItem?<div>
                    <Row>
                        <Col md={3}>{currentItem.countyDto.name+' - '+currentItem.code}</Col>
                        <Col md={1}><Button color="danger" onClick={()=>deleteZipCode1(currentItem)}>Delete</Button></Col>
                        {/*<Col md={1}  className="offset-7"><Button color="success">Save</Button></Col>*/}
                    </Row>
                    <Row className="mt-5 pt-5">
                        <Col md={1}>
                            <h5>Agents</h5>
                        </Col>
                        <Col md={2}>
                            <Button onClick={()=>openModal(true,true)}>Add Agent</Button>
                        </Col>
                    </Row>
                    {agents !=null && !showModal ? agents.map((agent) =>
                        <Row key={agent.id} className="bg-light rounded m-1 p-3 " onClick={()=>window.location.replace("/agentProfile/"+agent.id)} style={{cursor:'pointer'}}>
                            <Col md={1} >
                                <img src={config.BASE_URL+"/attachment/"+agent.photoId} alt="" className="img-fluid rounded-circle"/>
                            </Col>
                            <Col md={3} >
                                <h3>Agent</h3>
                                <p>{agent.firstName+" "+agent.lastName}</p>
                            </Col>
                        </Row>
                    ) : ''}
                    {showModal ?
                        <Modal isOpen={showModal} toggle={() => openModal(false,false)}>
                            <AvForm onValidSubmit={addAgent}>
                                <ModalHeader>Feedback from Order {currentItem.checkNumber}</ModalHeader>
                                <ModalBody>
                                    <AvField type="select" name="id" label="Select order rate" required>
                                        {agents.map((agent,i)=>
                                            <option key={agent.id} value={agent.id}>{i+1+" "+agent.firstName+"  "+agent.lastName}</option>
                                        )}
                                    </AvField>
                                </ModalBody>
                                <ModalFooter>
                                    <Button type="button" onClick={() => openModal(false,false)}>Cancel</Button>
                                    <Button color="info">Add</Button>
                                </ModalFooter>
                            </AvForm>
                        </Modal> : ''}

                </div>:''}

            </div>
        );
    }
}

UserZipCode.propTypes = {};

export default connect(({
    app:{
        currentItem,showModal
    },
    agent:{
        agents
    }
                        })=>({
    currentItem,
    agents,showModal
}))(UserZipCode);