import React, {Component} from 'react';
import {PostBig, PostSmall} from "../components/BlogPost";
import Footer from "../components/Footer";
import LeftMenu from "../components/LeftMenu";
import { userMe} from "../redux/actions/AuthActions";
import {getBlogByCategory, getBlogList, getBlogListFeatured, getCategorys} from "../redux/actions/AppAction";
import {connect} from "react-redux";
import Pagination from "react-js-pagination";
import {config} from "../utils/config"
import {withRouter} from 'react-router-dom';
import Loader from "../components/Loader";
import Modal from "../components/common/modal";


class BlogPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showArray: [],
            loader : true,
            isOpen : false,
        }
    }

    setShowArray = (arr) => {
        this.setState({
            showArray: arr
        })
    }
    routeChange = (url) => {
        let path = "/blog/" + url;
        this.props.history.push(path);
    }

    componentDidMount() {
        this.props.dispatch(userMe())
        this.props.dispatch(getBlogListFeatured())
        this.props.dispatch(getCategorys())
        this.props.dispatch(getBlogList({page: 0, size: 10}));
        setTimeout(() => {
            this.setState({loader : false});
        },2000)
    }

    handlePageChange(pageNumber) {
        this.props.dispatch(getBlogList({page: pageNumber - 1, size: 10}))
        this.props.dispatch(
            {
                type: 'updateState',
                payload: {
                    activePageBlog: pageNumber - 1
                }
            })
    }

    GetByTag = (item) => {
        this.props.dispatch(getBlogByCategory(item))
    }

    handleChangeModal = () => {
        this.setState({isOpen : !this.state.isOpen})
    };

    render() {
        const {agents, isAdmin, currentUser, states} = this.props;
        const {
            attachmentIdsArray,
            showEmbassyModal, page, size, totalElements, embassy, dispatch, showModal, countries, currentItem, active, showDeleteModal, showStatusModal,
            activePageBlog,
            totalElementsBlog,
            blogsPage,
            blogFeatureds, categorys, blogByCategory, modalShow,
            history,
        } = this.props;

        if(this.state.loader) return <Loader/>

        return (
            <div className="flex-row align-items-start bg-danger">
                <LeftMenu
                    onToggleModal={this.handleChangeModal}
                    show={this.state.isOpen}
                />
                <div className="blog-page">
                    <i className="fas fa-bars" onClick={this.handleChangeModal} />
                    <div className="page-title-container">
                        <h1>Blog</h1>
                    </div>

                    <div className="page-content">
                        <div className="flex-column big-posts">
                            {!blogByCategory ? blogsPage.map((item, index) =>
                                <PostBig key={index}
                                         onClick={() => {
                                             this.routeChange(item.url)
                                         }}
                                         title={item.title}
                                         body={item.text}
                                         mainImg={config.BASE_URL + "/attachment/" + item.attachmentId}
                                         publishDate={item.date}
                                />
                            ) : blogByCategory.length !== 0 ? blogByCategory.map((item, index) => (
                                <PostBig key={index} onClick={() => {
                                    this.routeChange(item.url)
                                }} title={item.title} body={item.text}
                                         mainImg={config.BASE_URL + "/attachment/" + item.attachment.id}
                                         authorName={item.owner}
                                         authorImg={config.BASE_URL + "/userPhoto/" + item.userPhotoId}
                                         publishDate={item.date}/>
                            )) : <div className="title d-flex justify-content-center align-items-center">
                                <span>This category is empty</span>
                            </div>}
                            {totalElementsBlog > 0 ?
                                <div className="flex-column">
                                    <div className="align-self-center">
                                        <Pagination
                                            className="mt-2"
                                            activePage={activePageBlog + 1}
                                            itemsCountPerPage={10}
                                            totalItemsCount={totalElementsBlog}
                                            pageRangeDisplayed={5}
                                            onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                                            linkClass="page-link"
                                        />
                                    </div>

                                </div>
                                : ""}
                        </div>

                        <div className="flex-column small-posts">
                            <div className="flex-column mb-2">
                                <span className="featured-posts-title mb-1">Categories</span>
                                <div className="flex-column">
                                    {categorys.map((item, index) =>
                                        <span key={index} onClick={() => this.GetByTag(item)}
                                              className="mt-1 blog-category">{item.name}</span>
                                    )}
                                </div>
                            </div>

                            <span className="featured-posts-title">Featured posts</span>
                            {blogFeatureds.map((item, index) =>
                                <PostSmall key={index} onClick={() => {
                                    this.routeChange(item.url)
                                }} title={item.title} mainImg={config.BASE_URL + "/attachment/" + item.attachment.id}/>
                            )}
                        </div>
                    </div>
                    <Footer />
                </div>
                <Modal
                    onToggleModal={this.handleChangeModal}
                    visiblity={this.state.isOpen}
                />
            </div>

        );
    }
}


export default connect(
    ({
         app: {
             page, size, totalElements,
             embassy,
             showEmbassyModal,
             countries, showModal, currentItem, active, showDeleteModal, showStatusModal,
             activePageBlog,
             totalElementsBlog,
             blogsPage,
             blogFeatureds, categorys, blogByCategory
         },
         auth: {
             currentUser, isAdmin, modalShow,
         },
         attachment: {attachmentIdsArray}
     }) => ({
        page, size, totalElements, attachmentIdsArray,
        embassy,
        showEmbassyModal,
        countries,
        showModal,
        currentItem,
        active, showDeleteModal, showStatusModal, activePageBlog,
        totalElementsBlog,
        blogsPage, currentUser, isAdmin, blogFeatureds, categorys, blogByCategory, modalShow,
    }))(withRouter(BlogPage));
