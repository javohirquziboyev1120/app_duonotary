import React, {Component} from 'react';
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import Button from "reactstrap/es/Button";
import Input from "reactstrap/es/Input";
import {AvForm, AvField} from "availity-reactstrap-validation";
import FormGroup from "reactstrap/es/FormGroup";
import Label from "reactstrap/es/Label";
import * as authActions from "../redux/actions/AuthActions";
import * as types from "../redux/actionTypes/AuthActionTypes";
import {toast} from "react-toastify";
import {RiArrowGoBackLine} from "react-icons/ri/index";
import AlertModal from "../components/AlertModal";
import LeftMenu from "../components/LeftMenu";
import Modal from "../components/common/modal";

class Register extends Component {
    componentDidMount() {
        if (this.props.currentUser) {
            this.props.dispatch({
                type: types.AUTH_LOGOUT
            });
        }
        if (this.props.match && this.props.match.params && this.props.match.params.id) {
            this.setState({modalType: true})
            this.props.dispatch({
                type: "updateState",
                payload: {
                    sharingUserId: this.props.match.params.id,
                    alert_mes: ''
                }
            })
        }

    }

    constructor(props) {
        super(props);

        this.state = {
            modalType: false,
            firstName: '',
            lastName: '',
            email: '',
            phoneNumber: '',
            password: '',
            prePassword: '',
            agree: false,
            order: false,
            sharingUserId: null,
            isOpen : false,
        }
    }

    handleChangeModal = () => {
        this.setState({isOpen : !this.state.isOpen})
    };

    render() {
        const {dispatch, alert_mes, loading, history, currentUser, modalShow, mainMenu, sharingUserId} = this.props;
        const {agree, modalType} = this.state;
        const getInputValue = (e) => {
            e.persist();
            this.setState({
                [e.target.name]: e.target.value
            })
        }
        const signIn = (e, v) => {
            e.persist();
            e.preventDefault()
            dispatch(authActions.login({v, history}));
        }

        const register = (e) => {
            e.persist();
            e.preventDefault();
            if (sharingUserId) {
                this.setState({
                    sharingUserId: sharingUserId
                })
            }
            let userInfo = {...this.state}
            if (agree)
                if (userInfo.password.length >= 6 && userInfo.prePassword.length <= 16)
                    if (userInfo.password === userInfo.prePassword)
                        dispatch(authActions.registerUser({...this.state, history}));
                    else
                        toast.error("Password and pre password not equals!")
                else
                    toast.error("Password length must be 6 and 16 characters!")
            else {
                toast.error("Please, Sign up before please check confirm our conditions!")
                this.setState({agree: false})
            }
        }

        const doAgree = () => {
            this.setState({agree: !agree})
        }
        const changeModel = () => {
            this.setState({modalType: !modalType})
        }
        let removeModal = () => {
            dispatch({
                type: "updateState",
                payload: {
                    alert_mes: ''
                }
            })
            history.push("/register")
            this.setState({
                modalType: false,
                firstName: '',
                lastName: '',
                email: '',
                phoneNumber: '',
                password: '',
                prePassword: '',
                agree: false,
                order: false,
                sharingUserId: null
            })
        }

        return (
            <div className="home-page">
                {alert_mes && alert_mes.length > 2 ?
                    <AlertModal title={"Message"} display={removeModal} body={alert_mes}/> : ""}
                <LeftMenu
                    onToggleModal={this.handleChangeModal}
                    show={this.state.isOpen}
                />
                <div className="home-page-right bg-light">
                    <div className="row h-100 d-flex align-items-center justify-content-start">
                        <div className="col-sm-12 col-lg-7 bg-white p-5 rounded mx-auto">
                            <Link to="/">
                                <Button className="btn btn-light"><RiArrowGoBackLine/> Go home</Button>
                            </Link>
                            {modalType ?
                                <div>
                                    <h2 className="text-center py-3">Client Registration</h2>
                                    <AvForm onValidSubmit={register}>
                                        <div className="row">
                                            <div className="col-md-6 col-sm-12">
                                                <AvField
                                                    className="my-4"
                                                    onChange={getInputValue}
                                                    required={true}
                                                    name="firstName"
                                                    placeholder="First name
                                    "/>
                                            </div>
                                            <div className="col-md-6 col-sm-12">
                                                <AvField
                                                    className="my-4"
                                                    onChange={getInputValue}
                                                    required={true}
                                                    name="lastName"
                                                    placeholder="Last name
                                    "/>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6 col-sm-12">
                                                <AvField
                                                    className="my-4"
                                                    onChange={getInputValue}
                                                    required={true}
                                                    name="email"
                                                    type="email"
                                                    placeholder="Email address
                                    "/>
                                            </div>
                                            <div className="col-md-6 col-sm-12">
                                                <AvField
                                                    className="my-4"
                                                    onChange={getInputValue}
                                                    required={true}
                                                    name="phoneNumber"
                                                    type="text"
                                                    placeholder="Phone number"/>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6 col-sm-12">
                                                <AvField
                                                    className="my-4"
                                                    onChange={getInputValue}
                                                    required={true}
                                                    name="password"
                                                    type="password"
                                                    placeholder="Password"/>
                                            </div>
                                            <div className="col-md-6 col-sm-12">
                                                <AvField
                                                    className="my-4"
                                                    onChange={getInputValue}
                                                    required={true}
                                                    name="prePassword"
                                                    type="password"
                                                    placeholder="Repeat password"/>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-7 col-sm-12">
                                                <FormGroup check>
                                                    <Label check className="labelDuo pl-3">
                                                        <div className='labelBlock' style={{left : '-5%'}}>
                                                            {this.state.agree && <div className='labelBox'>
                                                                <i className="fas fa-check"/>
                                                            </div>}
                                                        </div>
                                                        <Input className='d-none' type="checkbox" onChange={doAgree}/>{' '}
                                                        Creating an account means you’re okay with our
                                                        <Link to="/termsofservices"> Terms of
                                                            Service</Link>,
                                                        <Link to="/privacypolicy"> Privacy Policy</Link>,
                                                        and our default <Link to="notificationsettings"> Notification
                                                        Settings</Link>.
                                                    </Label>
                                                </FormGroup>
                                            </div>
                                            <div className="col-md-5 col-sm-12 text-right pt-3">
                                                <p>Already have an account?
                                                    <Link to="#">
                                                        <span className="text-primary"
                                                              onClick={changeModel}> Sign in</span>
                                                    </Link>
                                                </p>
                                            </div>
                                        </div>
                                        <div className="col-6 mx-auto mt-3">
                                            <Button color="primary" disabled={!agree} block>Sign up</Button>
                                        </div>
                                    </AvForm>
                                </div> :
                                <div className="row">
                                    <AvForm onValidSubmit={signIn} className="col-10 col-md-6 col-sm-8 mx-auto">
                                        <h2 className="text-center py-3">Sign in</h2>
                                        <div className="row">
                                            <div className="col-12">
                                                <AvField
                                                    className=""
                                                    onChange={getInputValue}
                                                    required={!modalType}
                                                    name={modalType ? "e" : "username"}
                                                    type="email"
                                                    placeholder="Email"/>
                                            </div>
                                            <div className="col-12">
                                                <AvField
                                                    className="mt-2 mb-4"
                                                    onChange={getInputValue}
                                                    required={!modalType}
                                                    name={modalType ? "k" : "password"}
                                                    type="password"
                                                    placeholder="Password"/>
                                            </div>
                                        </div>
                                        <div className="">
                                            <Button color="primary" block>Sign in</Button>
                                        </div>
                                        <div className="mt-2">
                                            <p>Don't have an account?
                                                <Link to="#">
                                                    <span className="text-primary" onClick={changeModel}> Sign up</span>
                                                </Link>
                                            </p>
                                        </div>
                                    </AvForm>

                                </div>}
                        </div>
                    </div>
                </div>
                <Modal
                    onToggleModal={this.handleChangeModal}
                    visiblity={this.state.isOpen}
                />
            </div>
        );
    }
}

export default connect(({auth: {alert_mes, sharingUserId, loading, mainMenu, currentUser, modalShow}}) =>
    ({alert_mes, sharingUserId, loading, mainMenu, currentUser, modalShow}))(Register);


