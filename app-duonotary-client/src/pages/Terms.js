import React, {Component, useState} from 'react';
import LeftMenu from "../components/LeftMenu";
import TextEditor from "../components/TextEditor";
import {connect} from "react-redux";
import {userMe} from "../redux/actions/AuthActions";
import {getTerms, saveTerms} from "../redux/actions/AppAction";
import Footer from "../components/Footer";
import AdminLayout from "../components/AdminLayout";
import Modal from "../components/common/modal";

class Terms extends Component {

    constructor(props) {
        super(props);
        this.state = {editor: "Hello world", isEditing: false, isOpen : false};
    }

    type = "";
    title = "";

    unlisten = this.props.history.listen((location, action) => {
        this.updatePage();
    });

    updatePage = () =>{
        this.props.dispatch(userMe())

        let path = this.props.history.location.pathname;

        if (path === "/termsofservices") {
            this.type = "TERMSOFUSE";
            this.title = "Terms of Services"
        } else if (path === "/notificationsettings") {
            this.type = "NOTIFICATIONSETTING";
            this.title = "Notification Settings";
        } else {
            this.type = "PRIVACYPOLISY";
            this.title = "Privacy Policy";
        }
        this.props.dispatch(getTerms(this.type)).then(() => this.onEditorChange(this.props.terms.text))
    }

    componentDidMount() {
        this.updatePage();
    }

    onEditorChange = (val) => {
        this.setState({
            editor: val
        })
    }

    handleChangeModal = () => {
        this.setState({isOpen : !this.state.isOpen})
    };


    render() {
        const {isAdmin, isUser, isAgent, currentUser, showModal, dispatch, terms} = this.props
        const editTerms = () => {
            if (this.state.isEditing) {
                let v = {}
                v.id = terms.id
                v.text = this.state.editor
                v.termsEnum = this.type
                this.props.dispatch(saveTerms(v))
            }
            this.setState({isEditing: !this.state.isEditing});
        }
        return (
            <div key={this.type} className="flex-row align-items-start">
                {isAdmin ?
                    <>
                        <AdminLayout pathname={this.props.location.pathname}>
                            <div className="flex-column terms-page w-100">
                                <span className="terms-title align-self-center">{this.title}</span>
                                <div className="d-flex justify-content-between align-items-center mb-2">
                                    <span>Updated: {new Date(terms.createdAt).toLocaleDateString()}</span>
                                    {isAdmin ? <button className="publish-btn m-0"
                                                       onClick={editTerms}>{this.state.isEditing ? "Save" : "Update"}</button> : ""}
                                </div>
                                {isAdmin && this.state.isEditing ?
                                    <div className="terms-text flex-column input-text-area">
                                        <TextEditor editorHtml={this.state.editor} name="text"
                                                    handleChange={this.onEditorChange}/>
                                    </div> :

                                    <div className="terms-text flex-column"
                                         dangerouslySetInnerHTML={{__html: this.state.editor}}/>
                                }
                            </div>
                        </AdminLayout>
                    </>
                    :
                    <div className="d-flex">
                        <LeftMenu
                            onToggleModal={this.handleChangeModal}
                            show={this.state.isOpen}
                        />
                        <div className="d-flex flex-column vh-100 overflow-auto">
                            <div className="flex-column terms-page">
                                <span className="terms-title align-self-center">{this.title}</span>
                                <div className="d-flex justify-content-between align-items-center mb-2">
                                    <span>Updated: {new Date(terms.createdAt).toLocaleDateString()}</span>
                                </div>
                                <div className="terms-text flex-column"
                                     dangerouslySetInnerHTML={{__html: this.state.editor}}/>

                            </div>
                            <Footer />
                        </div>
                        <Modal
                            onToggleModal={this.handleChangeModal}
                            visiblity={this.state.isOpen}
                        />
                    </div>
                }
            </div>

        );
    }
}

export default connect(({
                            app: {
                                showModal, terms
                            },
                            auth: {
                                isAdmin,
                                isAgent,
                                isUser,
                                currentUser
                            }
                        }) => ({
    isAdmin, isUser, isAgent, currentUser, showModal, terms
}))(Terms);