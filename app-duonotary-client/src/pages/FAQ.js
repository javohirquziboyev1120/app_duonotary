import React, {Component} from 'react';
import UncontrolledCollapse from "reactstrap/es/UncontrolledCollapse";
import LeftMenu from "../components/LeftMenu";
import TabMenu from "../components/Calendar/TabMenu";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {getFaq} from "../redux/actions/AppAction";
import Modal from "../components/common/modal";

class Faq extends Component {

    constructor(props) {
        super(props);
        this.state={tabIndex: 0, isOpen : false,}
    }

    role=["CLIENT", "AGENT"];

    componentDidMount() {
        this.props.dispatch(getFaq())
    }

    handleChangeModal = () => {
        this.setState({isOpen : !this.state.isOpen})
    };

    render() {

        const {faq}=this.props;

        return (
            // <MainLayout pathname={this.props.history.location}>
                <div className="d-flex">
                    <LeftMenu
                        onToggleModal={this.handleChangeModal}
                        show={this.state.isOpen}
                    />
                    <div className="faq-page container flex-column my-2 mx-5">
                        <h1 className="page-name mx-auto my-3">FAQ</h1>
                        <div className="d-flex justify-content-center">
                            <TabMenu tabs={["Clients", "Agents"]} getTabIndex={(i)=>{
                               this.setState({tabIndex: i})}} />
                        </div>
                        {
                            faq?faq.map((item, index)=>
                                this.role[this.state.tabIndex]===item.role?
                                    <div key={item.id} className="w-100 faq-item flex-column">
                                        <div id={"id"+item.id} className="d-flex justify-content-between align-items-center">
                                            <span className="title">{item.header}</span>
                                            <div className="d-flex align-items-center">
                                                <span className="icon icon-collapse"/>
                                            </div>
                                        </div>
                                        <UncontrolledCollapse toggler={"id"+item.id}>
                                            <span className="content">{item.text}</span>
                                        </UncontrolledCollapse>
                                    </div>:""
                            ):""
                        }
                    </div>
                    <Modal
                        onToggleModal={this.handleChangeModal}
                        visiblity={this.state.isOpen}
                    />
                </div>
            // </MainLayout>
        );
    }
}


export default connect(
    ({
         app: {
             faq,
         },
     }) => ({
        faq
    }))
(withRouter(Faq));