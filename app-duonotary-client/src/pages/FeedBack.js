import React, {Component} from 'react';
import {connect} from "react-redux";
import {addAnswer, editFeedBackAction} from "../redux/actions/AppAction";
import {Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import Pagination from "react-js-pagination";

class FeedBack extends Component {


    render() {
        const {feedBacks, isAdmin, dispatch, isUser, isAgent, currentItem, showModal} = this.props;
        const editFeedback = (e, v) => {
            v.id = currentItem.id
            v.orderDto = currentItem.orderDto;
            v.agent = isAgent;
            v.operationEnum = "UPDATE";
            v.seen = false;
            dispatch(editFeedBackAction(v))
        }

        const answerFeedback = (e, v) => {
            v.id = currentItem.id
            dispatch(addAnswer(v))
        }

        const openModal = (boolean, item) => {
            dispatch({type: 'updateState', payload: {showModal: boolean, currentItem: item}})
        }

        return (
            <div>
                {feedBacks != null ?
                    <Table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Rate</th>
                            <th>Description</th>
                            <th>Agent</th>
                            <th>Client</th>
                            <th>Owner</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {feedBacks.map((feedback, i) =>
                            <tr key={feedback.id}>
                                <th>{i + 1}</th>
                                <th>{feedback.rate}</th>
                                <th>{feedback.description}</th>
                                <th>{feedback.orderDto.agent.firstName + " " + feedback.orderDto.agent.lastName}</th>
                                <th>{feedback.orderDto.client.firstName + " " + feedback.orderDto.client.lastName}</th>
                                <th>{feedback.agent ? "AGENT" : "CLIENT"}</th>
                                {isUser || isAgent ?
                                    <th><Button onClick={() => openModal(true, feedback)}>Edit</Button></th> : isAdmin ?
                                        <th><Button onClick={() => openModal(true, feedback)}>Answer send</Button>
                                        </th> : ''}
                            </tr>
                        )}
                        </tbody>
                    </Table>
                    : ''
                }
                <Col md={6}>
                    <Pagination
                        // activePage={activePage+1}
                        itemsCountPerPage={10}
                        // totalItemsCount={isZipCode?totalZipCodeElements:isCounty?totalCountyElements:isState?totalStateElements:0}
                        pageRangeDisplayed={5}
                        // onChange={this.handlePageChange.bind(this)} itemClass="page-item"
                        linkClass="page-link"
                    />
                </Col>
                {(currentItem != null && showModal && isAgent) || (currentItem != null && showModal && isUser) ?
                    <Modal isOpen={showModal} toggle={() => openModal(false)}>
                        <AvForm onValidSubmit={editFeedback}>
                            <ModalHeader>edit Feedback {currentItem.checkNumber}</ModalHeader>
                            <ModalBody>
                                <AvField type="select" value={currentItem.rate} name="rate" label="Select order rate"
                                         required>
                                    <option selected value="5">5</option>
                                    <option value="4">4</option>
                                    <option value="3">3</option>
                                    <option value="2">2</option>
                                    <option value="1">1</option>
                                </AvField>
                                <AvField name='description' value={currentItem.description}
                                         placeholder="Enter description" required/>
                            </ModalBody>
                            <ModalFooter>
                                <Button type="button" onClick={() => openModal(false)}>Cancel</Button>
                                <Button color="info">Send</Button>
                            </ModalFooter>
                        </AvForm>
                    </Modal> :
                    currentItem != null && showModal && isAdmin ?
                        <Modal isOpen={showModal} toggle={() => openModal(false)}>
                            <AvForm onValidSubmit={answerFeedback}>
                                <ModalHeader>Answer to feed back {currentItem.checkNumber}</ModalHeader>
                                <ModalBody><AvField name='description' placeholder="enter your answer"
                                                    required/></ModalBody>
                                <ModalFooter>
                                    <Button type="button" onClick={() => openModal(false)}>Cancel</Button>
                                    <Button color="info">Send</Button>
                                </ModalFooter>
                            </AvForm>
                        </Modal>
                        : ''}
            </div>
        );
    }
}

export default connect(({
                            app: {
                                feedBacks,
                                page,
                                size,
                                currentItem,
                                showModal
                            },
                            auth: {
                                isAdmin,
                                isAgent,
                                isUser,
                                currentUser
                            }
                        }) => ({
    feedBacks, page, size, isAdmin, isUser, isAgent, currentUser, currentItem, showModal
}))(FeedBack);