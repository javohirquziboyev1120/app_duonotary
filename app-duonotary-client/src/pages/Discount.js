import React, {Component} from 'react';
import {connect} from "react-redux";
import {editDiscountAction, getDiscountByUser, getDiscountList} from "../redux/actions/AppAction";
import {Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row, Table} from "reactstrap";
import {userMe} from "../redux/actions/AuthActions";
import {AvField, AvForm} from "availity-reactstrap-validation";

class Discount extends Component {
    componentDidMount() {
        this.props.dispatch(userMe()).then(()=>{
            if (this.props.isAdmin) this.props.dispatch(getDiscountList({page: this.props.page, size: this.props.size}));
            if (this.props.isUser) this.props.dispatch(getDiscountByUser(this.props.currentUser.id));
        })
    }

    render() {
        const {isAdmin, discounts,dispatch,showModal,currentItem} = this.props;
        const editDiscount=(e,v)=>{
            v.id=currentItem.id;
            v.orderDto=currentItem.orderDto
            dispatch(editDiscountAction(v))
        }
        const openModal=(bool,item)=>{
            dispatch({type:'updateState',payload:{showModal:bool,currentItem:item}})
        }
        return (
            <div>
                {discounts != null ?
                    <Table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Amount</th>
                            <th>percent</th>
                            <th>Description</th>
                            <th>Agent</th>
                            <th>Client</th>
                            {isAdmin?<th>Action</th>:''}
                        </tr>
                        </thead>
                        <tbody>
                        {discounts.map((discount, i) =>
                            <tr key={discount.id}>
                                <th>{i + 1}</th>
                                <th>{discount.amount}</th>
                                <th>{discount.percent}</th>
                                <th>{discount.description}</th>
                                <th>{discount.orderDto.agent.firstName + " " + discount.orderDto.agent.lastName}</th>
                                <th>{discount.orderDto.client.firstName + " " + discount.orderDto.client.lastName}</th>
                                {isAdmin?<th onClick={()=>openModal(true,discount)}><Button >Edit</Button></th>:''}
                            </tr>
                        )}
                        </tbody>
                    </Table>
                    : ''
                }
                {currentItem!=null  && isAdmin?<Modal isOpen={showModal} toggle={()=>openModal(false,null)}>
                    <AvForm onValidSubmit={editDiscount}>
                        <ModalHeader>Discount</ModalHeader>
                        <ModalBody>
                            <Row>
                                <Col md={12}>  <AvField   value={currentItem.percent} type="number" name="percent" placeholder="enter percent" required/></Col>
                                <Col md={12}>  <AvField value={currentItem.amount} type="number" name="amount" placeholder="enter amount" required/></Col>
                                <Col md={12}>  <AvField value={currentItem.description} name="description" placeholder="enter description" required/></Col>
                            </Row>
                        </ModalBody>
                        <ModalFooter>
                            <Button type="button" onClick={()=>openModal(false,null)}>Cancel</Button>
                            <Button color="info">Submit</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>:''}
            </div>
        );
    }
}

export default connect(({
                            app: {discounts, page, size,currentItem,showModal},
                            auth: {currentUser, isAdmin,isAgent,isUser}
                        }) => ({discounts, page, size, currentUser, isAdmin,isAgent,isUser,currentItem,showModal}))(Discount);