import React, {Component} from "react";
import {Link, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import Loader from "../components/Loader";
import LeftMenu from "../components/LeftMenu";
import AlertModal from "../components/AlertModal";
import Modal from "../components/common/modal";
import "./homepage.scss";
import {Jumbotron} from "../components/common/jumbotron";
import Footer from "../components/Footer";
import {QuesstionContent} from "../components/common/quesstionContent";
import {TeamContent} from "../components/common/team";
import Services from "../components/common/Services";
import {getPartnersForHome} from "../redux/actions/AppAction";
import {getReviews} from "../redux/actions/AppAction";

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            order: false,
            curTime: '',
            loading: false,
            isOpen : false,
        };
    }


    handleChangeModal = () => {
        this.setState({isOpen : !this.state.isOpen})
    };

    handleLogin = () => {
        this.setState({loading: !this.state.loading});
    };

    componentDidMount() {
        this.props.dispatch(getPartnersForHome());
        this.props.dispatch(getReviews());
        this.props.dispatch({
            type: "updateState",
            payload: {
                loading: true,
                alert_mes: "",
            },
        });
        setTimeout(() => {
            this.props.dispatch({
                type: "updateState",
                payload: {loading: false},
            });
        }, 2500);

        setInterval(() => {
            let time = new Date();
            this.setState({
                curTime:
                    (time.getHours() <= 9 ? "0" + time.getHours() : time.getHours()) +
                    ":" +
                    (time.getMinutes() <= 9
                        ? "0" + time.getMinutes()
                        : time.getMinutes()),
            });
        }, 1000);
    }

    render() {
        const {
            dispatch,
            alert_mes,
            history,
            modalShow,
            mainMenu,
            reviews,
            partners
        } = this.props;

        const {order, curTime, loading} = this.state;

        return loading ? (
            <Loader/>
        ) : (
            <>
                <div className='home-page'>
                    <LeftMenu
                        onToggleModal={this.handleChangeModal}
                        show={this.state.isOpen}
                    />
                    {alert_mes && alert_mes.length > 2 && (
                        <AlertModal title={"Message"} body={alert_mes}/>
                    )}

                    <div className='home-page-right' hidden={order}>
                        {mainMenu ? (
                            <>
                                <div className='sub-menu'>
                                    <Link to='/blog'>Blog</Link> <br/> <br/>
                                    <Link to='/registerAgent'>Agent registration</Link>
                                </div>
                            </>
                        ) : (
                            ""
                        )}
                        <div className='home-header-section w-100 d-flex flex-wrap'>
                            <div className='flex-column left-text justify-content-between '>
                                <span className="header">
                                    Notarize documents online or in person at your convenient time and location
                                </span>
                                <span className="sub-text mt-2">
                                    New standart for notary service
                                </span>
                            </div>

                            <div className=" flex-column new-york ">
                                    <span className=''>NEW YORK</span>
                                    <span className='clock'>{curTime}</span>
                                </div>
                        </div>

                        <div className=' link-section mt-4 align-items-end mb-4'>
                                <div className='main-links'>
                                    <div className="link-button">
                                        <Link to='/online'>
                                            Get a free quote <span className='icon icon-right'/>
                                        </Link>
                                    </div>
                                    <div className="link-button ml-3">
                                        <Link to='/havetosay' className=''>
                                            Schedule an appointment
                                            <span className='icon icon-right'/>
                                        </Link>
                                    </div>
                                </div>
                                <div className='ml-auto link-button'>
                                    <Link to='/' onClick={this.handleChangeModal}>
                                        <span>
                                            Login
                                        </span>
                                        <span className='icon icon-link'/>
                                    </Link>
                                </div>
                            </div>
                        <div className='home-part-section'>
                            <Link to='/online' className='img-half position-relative'>
                                <div className="imgBox">
                                    <img src='/assets/img/part1.png' alt='' className='imgg'/>
                                </div>
                                <h4 className='mb-0 text-uppercase text-white'>
                                    Remote Notarization
                                </h4>
                                <div className='layer'>
                                    <img
                                        src='/assets/img/linkwhite.png'
                                        className='link-white'
                                        alt=''
                                    />
                                </div>
                            </Link>
                            <Link to='/inPerson' className='img-half position-relative'>
                                <div className={"imgBox"}>
                                    <img src='/assets/img/part2.png' alt='' className='imgg'/>
                                </div>
                                <h4 className='mb-0 text-uppercase text-white'>
                                    Mobile Notarization
                                </h4>
                                <div className='layer'>
                                    <img
                                        src='/assets/img/linkwhite.png'
                                        className='link-white'
                                        alt=''
                                    />
                                </div>
                            </Link>
                        </div>
                        <Services/>
                        <Jumbotron/>
                        <QuesstionContent/>
                        <TeamContent partners={partners} reviews={reviews}/>
                        <Footer/>
                    </div>
                </div>
                <Modal
                    onToggleModal={this.handleChangeModal}
                    visiblity={this.state.isOpen}
                    onLogin={this.handleLogin}
                />
            </>
        );
    }
}

export default withRouter(
    connect(
        ({
             auth: {alert_mes, loading, mainMenu, currentUser, modalShow},
             app: {reviews,  partners },
         }) => ({
            alert_mes,
            loading,
            mainMenu,
            currentUser,
            modalShow,
            reviews,
            partners
        })
    )(HomePage)
);
