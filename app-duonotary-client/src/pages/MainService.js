import React, { Component } from "react";
import "./Universal.scss";
import {
    Button,
    CustomInput,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table,
} from "reactstrap";
import { connect } from "react-redux";
import {
    deleteMainService,
    getMainServiceList,
    saveMainService,
} from "../redux/actions/AppAction";
import { AvField, AvForm } from "availity-reactstrap-validation";
import DeleteModal from "../components/Modal/DeleteModal";
import StatusModal from "../components/Modal/StatusModal";
import OnlineModal from "../components/Modal/OnlineModal";

class MainService extends Component {
    componentDidMount() {
        this.props.dispatch(getMainServiceList());
    }

    render() {
        const {
            dispatch,
            showModal,
            mainServices,
            currentItem,
            active,
            online,
            showDeleteModal,
            showStatusModal,
            showOnlineModal,
        } = this.props;

        const openModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showModal: !showModal,
                    currentItem: item,
                    active: item.active,
                    online: item.online,
                },
            });
        };
        const openDeleteModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentItem: item,
                },
            });
        };
        const changeActive = () => {
            dispatch({
                type: "updateState",
                payload: {
                    active: !active,
                },
            });
        };

        const changeOnline = () => {
            dispatch({
                type: "updateState",
                payload: {
                    online: !online,
                },
            });
        };

        const deleteFunction = () => {
            this.props.dispatch(deleteMainService(currentItem));
        };

        const saveItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null;
            v.active = active;
            v.online = online;
            this.props.dispatch(saveMainService(v));
        };

        const changeStatusMainService = () => {
            let currentMainService = { ...currentItem };
            currentMainService.active = !currentItem.active;
            this.props.dispatch(saveMainService(currentMainService));
        };

        const changeOnlineMainService = () => {
            let currentMainServiceOnline = { ...currentItem };
            currentMainServiceOnline.online = !currentItem.online;
            this.props.dispatch(saveMainService(currentMainServiceOnline));
        };

        const openStatusModal = (item) => {
            dispatch({
                type: "updateState",
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item,
                },
            });
        };

        const openOnlineModal = (item) => {
            dispatch({
                // type: type.CHANGE_ONLINE,
                type: "updateState",
                payload: {
                    showOnlineModal: !showOnlineModal,
                    currentItem: item,
                },
            });
        };
        return (
            <div className="main-service-page">
                <h2 className="text-center">Main Service</h2>
                <div className="button-container">
                    <AddIcon
                        className="add-button"
                        onClick={() => openModal("")}
                    />
                    <span className="dark-title">Add New</span>
                </div>
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>From time</th>
                            <th>Till time</th>
                            <th>Description</th>
                            <th>InPerson Number</th>
                            <th>Status</th>
                            <th>Online</th>
                            <th colSpan="2">
                                Operation
                            </th>
                        </tr>
                    </thead>

                    {mainServices.length > 0 ? (
                        <tbody>
                            {mainServices.map((item, i) => (
                                <tr key={item.id}>
                                    <td>{i + 1}</td>
                                    <td>{item.name}</td>
                                    <td>
                                        {item.fromTime}
                                    </td>
                                    <td>
                                        {item.tillTime}
                                    </td>
                                    <td>
                                        {item.description}
                                    </td>
                                    <td>{item.orderNumber}</td>
                                    <td>
                                        <FormGroup check>
                                            <Label
                                                check
                                                for="active"
                                            >
                                                <Input
                                                    type="checkbox"
                                                    onClick={() =>
                                                        openStatusModal(item)
                                                    }
                                                    id="active"
                                                    checked={item.active}
                                                />
                                                {item.active
                                                    ? "Active"
                                                    : "Inactive"}
                                            </Label>
                                        </FormGroup>
                                    </td>
                                    <td>
                                        <FormGroup check>
                                            <Label
                                                check
                                                for="online"
                                            >
                                                <Input
                                                    type="checkbox"
                                                    onClick={() =>
                                                        openOnlineModal(item)
                                                    }
                                                    id="online"
                                                    checked={item.online}
                                                />
                                                {item.online
                                                    ? "Online"
                                                    : "Offline"}
                                            </Label>
                                        </FormGroup>
                                    </td>
                                    <td>
                                        <EditIcon
                                            onClick={() => openModal(item)}
                                        />
                                    </td>
                                    <td>
                                        <DeleteIcon
                                            onClick={() =>
                                                openDeleteModal(item)
                                            }
                                        />
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    ) : (
                        <tbody>
                            <tr>
                                <td colSpan="4">
                                    <h3 className="text-center mx-auto">
                                        {" "}
                                        No information{" "}
                                    </h3>
                                </td>
                            </tr>
                        </tbody>
                    )}
                </Table>

                <Modal
                    id="allModalStyle"
                    isOpen={showModal} toggle={openModal}>
                    <AvForm onValidSubmit={saveItem}>
                        <ModalHeader
                            toggle={openModal}
                            charCode="x"
                            className="model-head"
                        >
                            {currentItem != null
                                ? "Edit Main Service"
                                : "Add Main Service"}
                        </ModalHeader>
                        <ModalBody>
                            <AvField
                                className="modal-input"
                                name="name"
                                label="Name"
                                required
                                defaultValue={
                                    currentItem != null ? currentItem.name : ""
                                }
                                placeholder="Enter Main Service name"
                            />
                            <div className="flex-row-times margin-top-15">
                                <AvField
                                    name="fromTime"
                                    className="modal-input half-left"
                                    label="From"
                                    required
                                    defaultValue={
                                        currentItem != null
                                            ? currentItem.fromTime
                                            : ""
                                    }
                                    placeholder="Enter from time"
                                />
                                <AvField
                                    name="tillTime"
                                    className="modal-input half-right"
                                    label="Till"
                                    required
                                    defaultValue={
                                        currentItem != null
                                            ? currentItem.tillTime
                                            : ""
                                    }
                                    placeholder="Enter till time"
                                />
                            </div>
                            <AvField
                                className="modal-input"
                                name="description"
                                label="Description"
                                required
                                defaultValue={
                                    currentItem != null
                                        ? currentItem.description
                                        : ""
                                }
                                placeholder="Enter description"
                            />
                            <AvField
                                name="orderNumber"
                                label="InPerson Number"
                                className="modal-input"
                                required
                                defaultValue={
                                    currentItem != null
                                        ? currentItem.orderNumber
                                        : ""
                                }
                                placeholder="Enter order number"
                            />

                            <div className="flex-row space-around">
                                <CustomInput
                                    className="check-box-title"
                                    type="checkbox"
                                    checked={active}
                                    onChange={changeActive}
                                    label={active ? "Active" : "Inactive"}
                                    id="mainServiceActive"
                                />
                                <br />
                                <CustomInput
                                    className="check-box-title"
                                    type="checkbox"
                                    checked={online}
                                    onChange={changeOnline}
                                    label={online ? "Online" : "Offline"}
                                    id="mainServiceOnline"
                                />
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <Button
                                type="button"
                                color="secondary"
                                outline
                                onClick={openModal}
                            >
                                Cancel
                            </Button>{" "}
                            <Button color="success">Save</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>

                {showDeleteModal && (
                    <DeleteModal
                        text={currentItem != null ? currentItem.name : ""}
                        showDeleteModal={showDeleteModal}
                        confirm={deleteFunction}
                        cancel={openDeleteModal}
                    />
                )}

                {showStatusModal && (
                    <StatusModal
                        text={currentItem != null ? currentItem.name : ""}
                        showStatusModal={showStatusModal}
                        confirm={changeStatusMainService}
                        cancel={openStatusModal}
                    />
                )}

                {showOnlineModal && (
                    <OnlineModal
                        text={currentItem != null ? currentItem.name : ""}
                        showOnlineModal={showOnlineModal}
                        confirm={changeOnlineMainService}
                        cancel={openOnlineModal}
                    />
                )}
            </div>
        );
    }
}

export default connect(
    ({
        app: {
            mainServices,
            showModal,
            currentItem,
            active,
            online,
            showDeleteModal,
            showStatusModal,
            showOnlineModal,
        },
    }) => ({
        mainServices,
        showModal,
        currentItem,
        active,
        online,
        showDeleteModal,
        showStatusModal,
        showOnlineModal,
    })
)(MainService);

// Icons
function EditIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{ cursor: "pointer" }}
                width="18"
                height="18"
                viewBox="0 0 18 18"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M12.728 6.68599L11.314 5.27199L2 14.586V16H3.414L12.728 6.68599ZM14.142 5.27199L15.556 3.85799L14.142 2.44399L12.728 3.85799L14.142 5.27199ZM4.242 18H0V13.757L13.435 0.321992C13.6225 0.134521 13.8768 0.0292053 14.142 0.0292053C14.4072 0.0292053 14.6615 0.134521 14.849 0.321992L17.678 3.15099C17.8655 3.33852 17.9708 3.59283 17.9708 3.85799C17.9708 4.12316 17.8655 4.37746 17.678 4.56499L4.243 18H4.242Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}
function AddIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                width="50"
                height="50"
                viewBox="0 0 50 50"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <rect width="50" height="50" rx="25" fill="#00B238" />
                <path
                    d="M24 24V18H26V24H32V26H26V32H24V26H18V24H24Z"
                    fill="white"
                />
            </svg>
        </div>
    );
}

function DeleteIcon(props) {
    return (
        <div onClick={props.onClick} className={props.className}>
            <svg
                style={{ cursor: "pointer" }}
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <path
                    d="M2 6H18V19C18 19.2652 17.8946 19.5196 17.7071 19.7071C17.5196 19.8946 17.2652 20 17 20H3C2.73478 20 2.48043 19.8946 2.29289 19.7071C2.10536 19.5196 2 19.2652 2 19V6ZM4 8V18H16V8H4ZM7 10H9V16H7V10ZM11 10H13V16H11V10ZM5 3V1C5 0.734784 5.10536 0.48043 5.29289 0.292893C5.48043 0.105357 5.73478 0 6 0H14C14.2652 0 14.5196 0.105357 14.7071 0.292893C14.8946 0.48043 15 0.734784 15 1V3H20V5H0V3H5ZM7 2V3H13V2H7Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}
