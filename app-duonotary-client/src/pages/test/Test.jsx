import React, { Component } from "react";
import styled from "styled-components";
import img1 from "./images/1.jpeg";
import img2 from "./images/2.jpeg";
import img3 from "./images/3.jpeg";
import img4 from "./images/4.jpeg";
import img5 from "./images/5.jpeg";
import img6 from "./images/6.jpeg";
import _ from "lodash";

export function paginate(items, currentPage, pageSize) {
  // calculate array start index to begin slicing from
  const startIndex = (currentPage - 1) * pageSize;
  // convert items array to a lodash wrapper
  return (
    _(items)
      // slice items array starting from startIndex number
      .slice(startIndex)
      // grab always 4 items from the array
      .take(pageSize)
      // convert again to regular array
      .value()
  );
}

const fake = [
  {
    img: img1,
    fullName: "Genry Adam",
    company: "Dribbble",
    description:
      "“I live in Washington, D.C, and I had an issue that  could not be resolved without a notarizedsignature. Thanks to the to friendly and professional team I met at  Duo Notary, aren’t I notarized my documents in no time. D.C, and I had an issue that  could not be resolved without a documents in no time“",
  },

  {
    img: img3,
    fullName: "Genry Adam",
    company: "Spotfy",
    description:
      "“I live in Washington, D.C, and I had an issue that  could not be resolved without a notarizedsignature. Thanks to the to friendly and professional team I met at  ",
  },
  {
    img: img2,
    fullName: "Genry Adam",
    company: "Google",
    description:
      "“I live in Washington, D.C, and I had an issue that  could not be resolved without a notarizedsignature. Thanks to the to friendly and professional team I met at  Duo Notary, aren’t I notarized my documents in no time” “I live in Washington, D.C, and I had an issue that  could not be resolved without a notarizedsignature. Thanks to the to friendly and professional team I met at  Duo Notary, aren’t I notarized my documents in no time. D.C, and I had an issue that  could not be resolved without a documents in no time“  ",
  },
  {
    img: img4,
    fullName: "Genry Adam",
    company: "Spotfy",
    description:
      "“I live in Washington, D.C, and I had an issue that  could not be resolved without a notarizedsignature. Thanks to the to friendly and professional team I met at  ",
  },
  {
    img: img5,
    fullName: "Genry Adam",
    company: "Spotfy",
    description:
      "“I live in Washington, D.C, and I had an issue that  could not be resolved without a notarizedsignature. Thanks to the to friendly and professional team I met at  ",
  },
  {
    img: img6,
    fullName: "Genry Adam",
    company: "Google",
    description:
      "“I live in Washington, D.C, and I had an issue that  could not be resolved without a notarizedsignature. Thanks to the to friendly and professional team I met at  Duo Notary, aren’t I notarized my documents in no time”",
  },
];

const list = [...fake, ...fake, ...fake, ...fake];

export default class Test extends Component {
  render() {
    let wrappers = [];

    list.forEach((item, i) => {
      let wrap = [];
      for (let j = 0; j < 6; j++) {
        wrap.push(
          <Column key={Math.random()}>
            {item.map(({ img, company, fullName, description }) => (
              <ReviewWrap key={Math.random()}>
                <Description>{description}</Description>
                <Fotter>
                  <Avatar src={img} alt='img' />
                  <FullName>{fullName} </FullName> {" - "}
                  <CompanyName> {company}</CompanyName>
                </Fotter>
              </ReviewWrap>
            ))}
          </Column>
        );
      }
      if (i % 6 === 0) wrappers.push(wrap);
    });

    const data = [];
    let j = 1;
    for (let i = 0; i < fake.length; i += 2) data[i] = [fake[i], fake[j++]];
    return (
      <Wrapper>
        {data.map((item) => (
          <Column key={Math.random()}>
            {item.map(({ img, company, fullName, description }) => (
              <ReviewWrap key={Math.random()}>
                <Description>{description}</Description>
                <Fotter>
                  <Avatar src={img} alt='img' />
                  <FullName>{fullName} </FullName> {" - "}
                  <CompanyName> {company}</CompanyName>
                </Fotter>
              </ReviewWrap>
            ))}
          </Column>
        ))}
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  justify-content: center;
  grid-gap: 1rem;
  align-items: start;
  padding: 1rem;
  grid-template-rows: auto;
`;
const Column = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  row-gap: 1rem;
`;
const ReviewWrap = styled.div`
  box-sizing: border-box;
  border-radius: 10px;
  padding: 1rem;
  border: 1px solid rgba(0, 0, 0, 0.2);
`;

const Description = styled.div`
  font-size: 14px;
  line-height: 28px;
  letter-spacing: -0.01em;
  color: #384049;
  padding-bottom: 0.5rem;
  text-align: justify;
`;

const Fotter = styled.div`
  padding: 0.8rem 0 0.4rem;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  border-top: 1px solid #d8dbde;
`;

const Avatar = styled.img`
  width: 40px;
  height: 40px;
  border-radius: 50%;
`;
const FullName = styled.span`
  font-size: 16px;
  line-height: 20px;
  letter-spacing: -0.03em;
  color: #384049;
  font-weight: bold;
  margin: 0 0.5rem;
`;
const CompanyName = styled.span`
  font-size: 16px;
  line-height: 20px;
  letter-spacing: -0.03em;
  color: #384049;
  margin: 0 0.5rem;
`;
