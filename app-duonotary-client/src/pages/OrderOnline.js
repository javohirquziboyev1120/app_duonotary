import React, {Component} from 'react';
import LeftMenu from "../components/LeftMenu";
import './Universal.scss'
import {Button, Col, Container, FormGroup, Input, Label, Row} from "reactstrap";

class OrderOnline extends Component {
    render() {
        return (
            <div className="inPerson-order-page-new border">
                <div>
                    <LeftMenu/>
                </div>
                <div className="right-site">
                    <Container fluid={true}>
                        <div className="title">
                            Pricing: $19.99 for 1 notarization and $10
                            for every additional.
                        </div>
                        <Row className="all-step-inPerson">

                            <Col>
                                <div
                                    className="active-step">
                                    <span className="icon icon-orderInfo"/>
                                </div>
                                <div className="step-text">
                                    Order info
                                </div>

                            </Col>
                            <Col>
                                <div className="bordered-line"/>
                            </Col>

                            <Col>
                                <div className="steps">
                                    <span className="icon icon-timeOrder"/>
                                </div>
                                <div className="step-text">
                                    User info
                                </div>

                            </Col>
                            <Col>
                                <div className="bordered-line"/>
                            </Col>
                            <Col>
                                <div className="steps">
                                    <span className="icon icon-payment"/>
                                </div>
                                <div className="step-text">
                                    Payment
                                </div>

                            </Col>
                        </Row>

                        {/*step one*/}
                        <div className="online-step-one">

                            <div className="real-estate">
                                Real estate - $15
                                <img src="/assets/img//estate.png" alt=""/>
                            </div>

                            <div className="notarized">
                                <div className="document">
                                    Your document(s) to br notarized
                                </div>
                                <div className="file-upload">
                                    File upload .doc, docx, or .pdf
                                </div>
                                <div className="d-flex mt-4">
                                    <div className="upload">
                                        <Input type="file"/>
                                    </div>
                                    <div className="uploaded-file">
                                        <img className="mt-3" src="/assets/img/uploaded.png" alt=""/>
                                       <div className="file-name">
                                           num123.doc
                                       </div>
                                    </div>
                                    <div className="uploaded-file">
                                        <img className="mt-3" src="/assets/img/uploaded.png" alt=""/>
                                        <div className="file-name">
                                            num123.doc
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="d-flex mt-5">
                                <div>
                                    <FormGroup check>
                                        <Label check>
                                            <Input className="mt-2" type="checkbox"/>{' '}
                                            <div className="employment">
                                                I understand I need a webcam to use DuoNotary
                                            </div>
                                        </Label>
                                    </FormGroup>
                                    <FormGroup className="mt-1" check>
                                        <Label check>
                                            <Input className="mt-2" type="checkbox"/>{' '}
                                            <div className="employment">
                                                A agree with the Terms of Service?
                                            </div>
                                        </Label>
                                    </FormGroup>
                                </div>
                                <div className="next-btn">
                                    <Button>Next step</Button>
                                </div>
                            </div>
                        </div>

                        {/*step two*/}
                        <div className="step-four">
                            <Row>
                                <Col md={6}>
                                    <Input className="step-four-input" placeholder="First name"/>
                                    <Input className="step-four-input" placeholder="Phone"/>
                                    <Input className="step-four-input" placeholder="Password"/>
                                </Col>
                                <Col md={6}>
                                    <Input className="step-four-input" placeholder="Last name"/>
                                    <Input className="step-four-input" placeholder="Email"/>
                                    <Input className="step-four-input" placeholder="Re-password"/>
                                </Col>
                            </Row>
                            <div className="notification-settings">
                                <FormGroup style={{marginTop: "50px"}} check>
                                    <Label check>
                                        <Input className="mt-2" type="checkbox"/>{' '}
                                        <div className="employment">
                                            Creating an account means you’re okay with
                                            our <a href="#">
                                            Terms of Service, Privacy Policy
                                        </a>,
                                            and our default <a href="#">
                                            Notification Settings
                                        </a> .
                                        </div>
                                    </Label>
                                </FormGroup>
                                <div className="next-btn">
                                    <Button>Next step</Button>
                                </div>
                            </div>
                        </div>

                        {/*step three*/}
                        <div className="step-five">
                            <Row>

                                <Col md={4}>
                                    <div className="order-type">
                                        <div className="type">
                                            Online
                                        </div>
                                        <div className="commit">
                                            Using 3rd party payment system which will be
                                            embedded to the payment page.
                                        </div>
                                        <div className="selected-order-type">
                                            <img src="/assets/img/step.png" alt=""/>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </Container>
                </div>
            </div>
        );
    }
}


export default OrderOnline;
