import React, {Component} from 'react';
import {
    Button,
    CustomInput,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table
} from "reactstrap";
import {connect} from "react-redux";
import {
    deleteSubService,
    getSubServiceList,
    saveSubService
} from "../redux/actions/AppAction";
import {AvField, AvForm} from "availity-reactstrap-validation";
import DeleteModal from "../components/Modal/DeleteModal";
import StatusModal from "../components/Modal/StatusModal";
import DefaultInputModal from "../components/Modal/DefaultInputModal";
import * as type from "../redux/actionTypes/AppActionTypes";


class SubService extends Component {
    componentDidMount() {
        this.props.dispatch(getSubServiceList());
    }

    render() {
        const {dispatch, showModal, subServices, currentItem, active, defaultInput, showDeleteModal, showStatusModal, showDefaultInputModal} = this.props;

        const openModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showModal: !showModal,
                    currentItem: item,
                    active: item.active,
                    defaultInput: item.defaultInput
                }
            })
        };
        const openDeleteModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentItem: item
                }
            })
        };
        const changeActive = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    active: !active
                }
            })
        };

        const changeDefaultInput = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    defaultInput: !defaultInput
                }
            })
        }

        const deleteFunction = () => {
            this.props.dispatch(deleteSubService(currentItem))
        };

        const saveItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null
            v.active = active
            v.defaultInput = defaultInput
            this.props.dispatch(saveSubService(v))
        }

        const changeStatusSubService = () => {
            let currentMainService = {...currentItem};
            currentMainService.active = !currentItem.active;
            this.props.dispatch(saveSubService(currentMainService))
        }

        const changeDefaultInputSubService = () => {
            let currentSubService = {...currentItem};
            currentSubService.defaultInput = !currentItem.defaultInput;
            this.props.dispatch(saveSubService(currentSubService))
        }

        const openStatusModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item
                }
            })
        };
        const openDefaultInputModal = (item) => {
            dispatch({
                type: type.CHANGE_DEFAULT_INPUT,
                payload: {
                    showDefaultInputModal: !showDefaultInputModal,
                    currentItem: item
                }
            })
        };

        return (
            <div>
                <h2 className="text-center">Sub Service</h2>
                <Button color="success" outline className="my-2" onClick={() => openModal('')}>+ New add Sub
                    Service</Button>
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Default Input</th>
                        <th>Status</th>
                        <th colSpan="2" className="">Operation</th>
                    </tr>
                    </thead>
                    {subServices.length > 0 ?
                        <tbody>
                        {subServices.map((item, i) =>
                            <tr key={item.id}>
                                <td>{i + 1}</td>
                                <td>{item.name}</td>
                                <td>{item.description}</td>

                                <td>
                                    <FormGroup check>
                                        <Label check for="defaultInput">
                                            <Input type="checkbox" onClick={() => openDefaultInputModal(item)}
                                                   id="defaultInput"
                                                   checked={item.defaultInput}/>
                                            {item.defaultInput ? "Yes" : "No"}
                                        </Label>
                                    </FormGroup>
                                </td>
                                <td>
                                    <FormGroup check>
                                        <Label check for="active">
                                            <Input type="checkbox" onClick={() => openStatusModal(item)} id="active"
                                                   checked={item.active}/>
                                            {item.active ? "Active" : "Inactive"}
                                        </Label>
                                    </FormGroup>

                                </td>
                                <td><Button color="warning" outline onClick={() => openModal(item)}>Edit</Button></td>
                                <td><Button color="danger" outline onClick={() => openDeleteModal(item)}>Delete</Button>
                                </td>

                            </tr>
                        )}
                        </tbody>
                        :
                        <tbody>
                        <tr>
                            <td colSpan="4">
                                <h3 className="text-center mx-auto"> No information </h3>
                            </td>
                        </tr>
                        </tbody>
                    }
                </Table>

                <Modal isOpen={showModal} toggle={openModal}>
                    <AvForm onValidSubmit={saveItem}>
                        <ModalHeader toggle={openModal}
                                     charCode="x">{currentItem != null ? "Edit Main Service" : "Add Main Service"}</ModalHeader>
                        <ModalBody>
                            <AvField name="name" label="Name" required
                                     defaultValue={currentItem != null ? currentItem.name : ""}
                                     placeholder="Enter Main Service name"/>
                            <AvField name="description" label="Description" required
                                     defaultValue={currentItem != null ? currentItem.description : ""}
                                     placeholder="Enter description"/>
                            <CustomInput type="checkbox" checked={active}
                                         onChange={changeActive}
                                         label={active ? 'Active' : 'Inactive'} id="subServiceActive"/>
                            <br/>
                            <CustomInput type="checkbox" checked={showDefaultInputModal}
                                         onChange={changeDefaultInput}
                                         label={showDefaultInputModal ? 'DefaultInput' : 'DefaultInput'}
                                         id="subServiceDefaultInput"/>
                        </ModalBody>
                        <ModalFooter>
                            <Button type="button" color="secondary" outline onClick={openModal}>Cancel</Button>{' '}
                            <Button color="success">Save</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>

                {showDeleteModal && <DeleteModal text={currentItem != null ? currentItem.name : ''}
                                                 showDeleteModal={showDeleteModal}
                                                 confirm={deleteFunction}
                                                 cancel={openDeleteModal}/>}

                {showStatusModal && <StatusModal text={currentItem != null ? currentItem.name : ''}
                                                 showStatusModal={showStatusModal}
                                                 confirm={changeStatusSubService}
                                                 cancel={openStatusModal}/>}

                {showDefaultInputModal && <DefaultInputModal text={currentItem != null ? currentItem.name : ''}
                                                             showDefaultInputModal={showDefaultInputModal}
                                                             confirm={changeDefaultInputSubService}
                                                             cancel={openDefaultInputModal}/>}
            </div>
        );
    }
}

export default connect(({
                            app: {
                                subServices, showModal, currentItem, active,
                                showDeleteModal, showStatusModal, defaultInput, showDefaultInputModal
                            }
                        }) => ({
    subServices, showModal, currentItem, active, showDeleteModal, showStatusModal, defaultInput, showDefaultInputModal
}))(SubService);