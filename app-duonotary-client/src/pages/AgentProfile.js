import React, {Component} from 'react';
import {connect} from "react-redux";
import {getCountryList, getEmbassy} from "../redux/actions/AppAction";
import RealEstateComponent from "../components/RealEstateComponent";
import International from "../components/International";

class AgentProfile extends Component {
    componentDidMount() {
        this.props.dispatch(getCountryList());
        this.props.dispatch(getEmbassy());
    }

    render() {
        const realEstate=(e,v)=>{
        }
        return (
            <div>
                <RealEstateComponent
                    printDocumentCount={this.props.printDocumentCount}
                    printDocumentPrice={26}
                    servicePrice={175}
                    dispatch={this.props.dispatch}
                    submit={realEstate}
                />
                <International
                isEmbassy={true}
                isApostille={this.props.isApostille}
                dispatch={this.props.dispatch}
                countries={this.props.countries}
                documentTypes={this.props.documentTypes}
                embassyCountries={this.props.embassyCountries}
                />
            </div>
        );
    }
}
export default connect(
    ({

         app: {countries
         },
         order:{
             printDocumentCount,isApostille,documentTypes,embassyCountries
         }
     }) => ({countries,documentTypes,embassyCountries,
        printDocumentCount,isApostille,

    }))(AgentProfile);