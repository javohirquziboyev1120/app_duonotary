import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {AvField, AvForm} from 'availity-reactstrap-validation';
import * as types from '../redux/actionTypes/AppActionTypes'
import {
    Button,
    CustomInput,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Table
} from 'reactstrap'
import {connect} from "react-redux";
import {
    getAdditionalServicePriceList,
    saveAdditionalServicePrice,
    editAdditionalServicePriceActive,
    getCountyByState, getZipCodeByCounty, deleteAdditionalServicePrice, getAdditionalServicePriceDashboard,
} from "../redux/actions/AdditionalServicePriceAction";
import {
    getStateList,
    changeStatus,
    getServicePriceList,
    saveMainService,
    getAdditionalServiceList,
    deleteCountry,
    getServiceList,
    getServiceListByServicePrice,
    getServicePrices
} from "../redux/actions/AppAction";
import StatusModal from "../components/Modal/StatusModal";
import Select from "react-select";
import additionalServicePrice from "../redux/reducers/AdditionalServicePriceReducers";
import DeleteModal from "../components/Modal/DeleteModal";

class AdditionalServicePrice extends Component {
    componentDidMount() {
        this.props.dispatch(getAdditionalServicePriceList())
        this.props.dispatch(getServicePrices())
        this.props.dispatch(getAdditionalServiceList())
        this.props.dispatch(getStateList())
        this.props.dispatch(getServiceListByServicePrice())

    }

    render() {
        const {
            selectZipCodes, selectCounties, selectStates,showDeleteModal,
            dispatch, stateOptions, countyOptions, zipCodeOptions, showServiceModal,
            services, servicePrices,additionalServicePrices, additionalServices,currentAdditionalService, all, active, showStatusModal
        } = this.props;

        const saveItem = (e, v) => {
            v.id = currentAdditionalService != null ? currentAdditionalService.id : null
            v.active = active
            v.all = all
            v.statesId = selectCounties !== null ? [] : selectStates
            v.countiesId = selectZipCodes !== null ? [] : selectCounties
            v.zipCodesId = selectZipCodes !== null ? selectZipCodes : []
            this.props.dispatch(saveAdditionalServicePrice(v))
        }
        const changeStatusAdditionalServicePrice = () => {
            let currentAdditionalServicePrice = {...currentAdditionalService};
            currentAdditionalServicePrice.active = !currentAdditionalService.active;
            this.props.dispatch(editAdditionalServicePriceActive(currentAdditionalServicePrice))
        }

        const openModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showServiceModal: !showServiceModal,
                    currentAdditionalService: item,
                    active: item.active,
                    all: item.all,
                }
            })
        };
        const openStatusModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showStatusModal: !showStatusModal,
                    currentAdditionalService: item
                }
            })
        };

        const changeActive = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    active: !active
                }
            })
        };
        const changeAll = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    all: !all
                }
            })
        };

        const getCounty = (e, v) => {
            let arr = [];
            if (v.action === "remove-value") {
                e.map((item) => arr.push(item.value));
            } else {
                dispatch(getCountyByState(v))
                e.map((item) => arr.push(item.value));
            }
            dispatch({
                type: 'updateState',
                payload: {
                    selectStates: arr
                }
            })
        };
        const getZipCode = (e, v) => {
            let arr = [];
            if (v.action === "remove-value") {
                e.map((item) => arr.push(item.value))
            } else {
                dispatch(getZipCodeByCounty(v))
                e.map((item) => arr.push(item.value))
            }
            dispatch({
                type: 'updateState',
                payload: {
                    selectCounties: arr
                }
            })
        };
        const saveZipCodeForAdditionalServicePrice = (e, v) => {
            let arr = [];
            if (v.action === "remove-value") {
                e.map((item) => arr.push(item.value))
            } else {
                e.map((item) => arr.push(item.value))
            }
            dispatch({
                type: 'updateState',
                payload: {
                    selectZipCodes: arr
                }
            })
        };

        const openDeleteModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentAdditionalService: item
                }
            })
        }
        const deleteFunction = () => {
            this.props.dispatch(deleteAdditionalServicePrice(currentAdditionalService))
        };

        return (
            <div>
                <h2 className="text-center">AdditionalServicePrice list</h2>
                <Button color="success" outline className="my-2" onClick={() => openModal('')}>+ New Add</Button>
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Service Price
                            <th>Main Service</th>
                            <th>Sub Service</th>
                            <th>Zip Code</th>
                            <th>Price</th>
                        </th>
                        <th>Additional Service</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th colSpan="2" className="">Operation</th>
                    </tr>
                    </thead>
                    {additionalServicePrices.length>0 ?
                        <tbody>
                        {additionalServicePrices.map((item, i) =>
                            <tr key={item.id}>
                                <td>{i + 1}</td>
                                <td>
                                    <td>{item.servicePriceDto.serviceDto.mainServiceDto.name}</td>
                                    <td>{item.servicePriceDto.serviceDto.subServiceDto.name}</td>
                                    <td>{item.servicePriceDto.zipCodeDto.code}</td>
                                    <td>{item.servicePriceDto.price}</td>
                                </td>
                                <td>{item.additionalServiceDto.name}</td>
                                <td>{item.price}</td>
                                <td>
                                    <FormGroup check>
                                        <Label check>
                                            <Input type="checkbox" onClick={() => openStatusModal(item)}
                                                   id="countryActive"
                                                   checked={item.active}/>
                                            {item.active ? "Active" : "Inactive"}
                                        </Label>
                                    </FormGroup>
                                </td>

                                <td><Button color="warning" outline onClick={() => openModal(item)}>Edit</Button></td>
                                <td><Button color="danger" outline onClick={() => openDeleteModal(item)}>Delete</Button>
                                </td>
                            </tr>
                        )}
                        </tbody>
                        :
                        <tbody>
                        <tr>
                            <td colSpan="4">
                                <h3 className="text-center mx-auto"> No information </h3>
                            </td>
                        </tr>
                        </tbody>
                    }
                </Table>
                <Modal isOpen={showServiceModal} toggle={openModal}>
                    <AvForm onValidSubmit={saveItem}>
                        <ModalHeader toggle={openModal}
                                     charCode="x">{currentAdditionalService != null ? "Edit AdditionalServicePrice" : "Add AdditionalServicePrice"}</ModalHeader>
                        <ModalBody>
                            <AvField type="select" name="additionalServiceId"
                                     value={currentAdditionalService != null ? (currentAdditionalService.name) : "0"}
                                     required>
                                <option value="0" selected >Select additional service</option>
                                {additionalServices.map(item =>
                                    <option key={item.id} value={item.id}> {item.name}</option>
                                )}
                            </AvField>
                            <AvField type="select" name="serviceId"
                                // value={currentAdditionalService != null ? (currentAdditionalService.servicePriceDto.serviceDto != null ? currentAdditionalService.servicePriceDto.serviceDto.mainServiceDto.name+" "+currentAdditionalService.servicePriceDto.serviceDto.subServiceDto.name : currentAdditionalService.serviceId) : "0"}
                                     required>
                                <option value="0" selected >Select Service</option>
                                {services.map(item =>
                                    <option key={item.id} value={item.id}>{item.mainServiceDto.name} - {item.subServiceDto.name}</option>
                                )}
                            </AvField>
                            <AvField name="price" label="Price" required
                                     defaultValue={currentAdditionalService != null ? currentAdditionalService.price : ""}
                                     placeholder="Enter price"/>
                            <CustomInput type="checkbox" checked={all}
                                         onChange={changeAll}
                                         label={all ? 'Add all zip code' : 'No all zip code'} id="all"/>
                            <CustomInput type="checkbox" checked={active}
                                         onChange={changeActive}
                                         label={active ? 'Active' : 'Inactive'} id="active"/>

                            <Select
                                isDisabled={all}
                                defaultValue="Select State"
                                isMulti
                                name="statesId"
                                options={stateOptions}
                                onChange={getCounty}
                                className="basic-multi-select"
                                classNamePrefix="select"
                            />
                            {selectStates !== null && selectStates.length === 1 ?
                                <Select
                                    isDisabled={all}
                                    defaultValue="Select County"
                                    isMulti
                                    name="countiesId"
                                    options={countyOptions}
                                    onChange={getZipCode}
                                    className="basic-multi-select"
                                    classNamePrefix="select"
                                /> : ""}
                            {selectCounties !== null && selectCounties.length === 1 ?
                                <Select
                                    isDisabled={all}
                                    defaultValue="Select Zipcode"
                                    isMulti
                                    name="zipCodesId"
                                    options={zipCodeOptions}
                                    onChange={saveZipCodeForAdditionalServicePrice}
                                    className="basic-multi-select"
                                    classNamePrefix="select"
                                /> : ""}
                        </ModalBody>
                        <ModalFooter>
                            <Button type=" button" color="" onClick={openModal}>Cancel</Button>{' '}
                            <Button color=" success">Save</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>

                {showStatusModal && <StatusModal text={currentAdditionalService != null ? currentAdditionalService.name : ''}
                                                 showStatusModal={showStatusModal}
                                                 confirm={changeStatusAdditionalServicePrice}
                                                 cancel={openStatusModal}/>}

                {showDeleteModal && <DeleteModal text={currentAdditionalService != null ? currentAdditionalService.name : ''}
                                                 showDeleteModal={showDeleteModal}
                                                 confirm={deleteFunction}
                                                 cancel={openDeleteModal}
                />}


            </div>


        );
    }
}

AdditionalServicePrice.propTypes = {};

export default connect(
    ({
         additionalServicePrice: {
             zipCodeOptions, countyOptions, additionalServicePrices, showServiceModal, currentAdditionalService, all,
             active, selectZipCodes, selectCounties, selectStates,showStatusModal
         },
         service: {
             servicePrices
         },
         app: {
             stateOptions,
             zipCodes,
             states, counties, showDeleteModal,
             services,additionalServices
         }
     }) => ({
        zipCodeOptions, selectZipCodes, selectCounties, selectStates,
        stateOptions, countyOptions,
        additionalServicePrices,
        zipCodes,
        states,
        counties,
        showServiceModal,
        currentAdditionalService, all,
        active, showDeleteModal, showStatusModal, services, servicePrices,additionalServices
    }))(AdditionalServicePrice);
