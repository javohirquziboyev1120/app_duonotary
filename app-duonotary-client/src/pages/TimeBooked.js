import React, {Component} from 'react';
import {getTimeBooked, saveTimeBooked} from "../redux/actions/AppAction";
import {connect} from "react-redux";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

class TimeBooked extends Component {
    componentDidMount() {
        this.props.dispatch(getTimeBooked())
    }

    render() {
        const {dispatch, showModal, timeBooked, currentItem} = this.props;

        const saveItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null
            this.props.dispatch(saveTimeBooked(v))
        }
        const openModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showModal: !showModal,
                    currentItem: item,
                }
            })
        };

        return (
            <div>

                {!timeBooked.length ? <div>
                    <Button color="success" outline className="my-2" onClick={() => openModal('')}>+ Add</Button>
                </div> : ''}

                <br/>

                <h1 className="text-center">Time Booked</h1>

                <br/>

                {timeBooked.length ?
                    <div className="d-flex">
                        <h3 className="mr-lg-3">
                            {timeBooked[0].bookedDuration} min
                        </h3>
                        <Button color="warning" outline onClick={() => openModal(timeBooked[0])}>Edit</Button>
                    </div>
                    :
                    <h3>Time Booked empty</h3>
                }

                <Modal isOpen={showModal} toggle={openModal}>
                    <AvForm onValidSubmit={saveItem}>
                        <ModalHeader toggle={openModal}
                                     charCode="x">{currentItem != null ? "Edit time booked" : "Add time booked"}</ModalHeader>
                        <ModalBody>
                            <AvField name="bookedDuration" label="Booked time EX: 30"
                                     required
                                     defaultValue={currentItem != null ? currentItem.bookedDuration : ""}
                                     placeholder="Enter booked time"/>
                        </ModalBody>
                        <ModalFooter>
                            <Button type="button" color="" onClick={openModal}>Cancel</Button>{' '}
                            <Button color="success">Save</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>

            </div>
        );
    }
}


export default connect(
    ({
         app: {
             timeBooked, showModal, currentItem, active
         }
     }) => ({
        timeBooked,
        showModal,
        currentItem,
        active
    }))(TimeBooked);