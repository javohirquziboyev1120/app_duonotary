import React, {Component} from 'react';
import {
    EmailIcon,
    EmailShareButton,
    FacebookIcon,
    FacebookShareButton,
    LinkedinIcon,
    LinkedinShareButton, PinterestIcon, PinterestShareButton,
    TwitterIcon,
    TwitterShareButton, WhatsappIcon, WhatsappShareButton
} from "react-share";
import {getShareUserQrCode} from "../redux/actions/SharingAction";
import {connect} from "react-redux";
import {getUserSharingDiscount} from "../redux/actions/AuthActions";

class SharingClient extends Component {
    componentDidMount() {
        this.props.dispatch(getShareUserQrCode());
        this.props.dispatch(getUserSharingDiscount())
    }

    render() {
        const {photoUrl, currentUser, userSharingDiscount, isUser} = this.props;
        return (
            <div>
                <h1>Sharing client</h1>
                <div className="container">
                    {currentUser && currentUser.id && isUser ?
                        <div className="row">
                            <div className="col-md-4 bg-light py-4 mr-2">
                                <h5 className="text-center">I use Notary</h5>
                                <div className="row mx-auto">
                                    <p className="text-center">Share your unique invitation link and get 5% for each
                                        user</p>
                                    <div className="d-block col-md-8 mx-auto">
                                        <FacebookShareButton
                                            url={"http://duonotary.com/register?sharingUserId=" + currentUser.id}
                                            quote={"Duonotary - online & in-person notary service"}
                                            className="btn btn-primary m-2">
                                            <FacebookIcon size={36} round={true}/>
                                        </FacebookShareButton>
                                        <TwitterShareButton
                                            url={"http://duonotary.com/register?sharingUserId=" + currentUser.id}
                                            quote={"Duonotary - online & in-person notary service"}
                                            className="btn btn-primary m-2">
                                            <TwitterIcon size={36} round={true}/>
                                        </TwitterShareButton>
                                        <LinkedinShareButton
                                            url={"http://duonotary.com/register?sharingUserId=" + currentUser.id}
                                            quote={"Duonotary - online & in-person notary service"}
                                            className="btn btn-primary m-2">
                                            <LinkedinIcon size={36} round={true}/>
                                        </LinkedinShareButton>
                                    </div>
                                    <div className="d-block col-md-8 mx-auto">
                                        <EmailShareButton
                                            url={"http://duonotary.com/register?sharingUserId=" + currentUser.id}
                                            quote={"Duonotary - online & in-person notary service"}
                                            className="btn btn-primary m-2">
                                            <EmailIcon size={36} round={true}/>
                                        </EmailShareButton>
                                        <PinterestShareButton
                                            url={"http://duonotary.com/register?sharingUserId=" + currentUser.id}
                                            quote={"Duonotary - online & in-person notary service"}
                                            className="btn btn-primary m-2">
                                            <PinterestIcon size={36} round={true}/>
                                        </PinterestShareButton>
                                        <WhatsappShareButton
                                            url={"http://duonotary.com/register?sharingUserId=" + currentUser.id}
                                            quote={"Duonotary - online & in-person notary service"}
                                            className="btn btn-primary m-2">
                                            <WhatsappIcon size={36} round={true}/>
                                        </WhatsappShareButton>
                                    </div>
                                </div>
                                <img src={"data:image/png;base64," + photoUrl} alt="" className="img-fluid"/>
                            </div>
                            <div className="col-md-4 bg-light h-100 py-4 text-center">
                                <h3 className="text-center">My bonus points</h3>
                                <h5 className="text-center">Your bonus is <a href="">12%</a></h5>
                                <a href="">use now</a>
                            </div>
                        </div>
                        : <div>
                            <h1>Your don't have bonus</h1>
                        </div>}
                </div>


            </div>
        );
    }
}

export default connect(({
                            share: {photoUrl, userSharingDiscount},
                            auth: {currentUser, isUser}
                        }) => ({photoUrl, userSharingDiscount, currentUser})
)(SharingClient);