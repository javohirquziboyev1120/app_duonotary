import React, {Component} from 'react';
import './Universal.scss'
import LeftMenu from "../components/LeftMenu";
import {Button, Col, Collapse, Container, FormGroup, Input, Label, Row} from "reactstrap";
import {connect} from "react-redux";
import Modal from "../components/common/modal";

class OrderInPerson extends Component {
    constructor(props) {
        super(props);

        this.state = {
            stage: 1,
            isOpen : false,
        }
    }

    handleChangeModal = () => {
        this.setState({isOpen : !this.state.isOpen})
    };

    render() {
        const {stage, openCollapse, dispatch} = this.props
        const changeStage = (stage) => {
            this.setState({stage})
        };
        const OpenCollapse = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    openCollapse: !openCollapse
                }
            })
        };

        return (
            <div className="inPerson-order-page-new">
                <LeftMenu
                    onToggleModal={this.handleChangeModal}
                    show={this.state.isOpen}
                />
                <div className="right-site">
                    <Container fluid={true}>
                        <div className="title">
                            We provide notary services at your location,
                            whether you are located at a hospital, nursing home,
                            office, or at home.
                        </div>
                        <Row className="all-step-inPerson">
                            <Col>
                                <div className="circle-active">
                                    <div className="active-step">
                                        <span className="icon icon-address"/>
                                    </div>
                                </div>
                                <div className="step-text">
                                    Address
                                </div>

                            </Col>
                            <Col>
                                <div className="bordered-line"/>
                            </Col>
                            <Col>
                                <div className="circle">
                                    <div className="steps">
                                        <span className="icon icon-orderInfo"/>
                                    </div>
                                </div>
                                <div className="step-text">
                                    Order info
                                </div>

                            </Col>
                            <Col>
                                <div className="bordered-line"/>
                            </Col>
                            <Col>
                                <div className="circle">
                                    <div className="steps">
                                        <span className="icon icon-userInfo"/>
                                    </div>
                                </div>
                                <div className="step-text">
                                    Data / Time
                                </div>

                            </Col>
                            <Col>
                                <div className="bordered-line"/>
                            </Col>
                            <Col>
                                <div className="circle">
                                    <div className="steps">
                                        <span className="icon icon-timeOrder"/>
                                    </div>
                                </div>
                                <div className="step-text">
                                    User info
                                </div>

                            </Col>
                            <Col>
                                <div className="bordered-line"/>
                            </Col>
                            <Col>
                                <div className="circle">
                                    <div className="steps">
                                        <span className="icon icon-payment"/>
                                    </div>
                                </div>
                                <div className="step-text">
                                    Payment
                                </div>

                            </Col>
                        </Row>

                        {/*step-one*/}
                        <div className="step-one">
                            <div className="please-address">
                                Please provide an address
                            </div>
                            <Input placeholder="CA 90301, USA – 008 N La Brea Ave, Inglewood"/>
                            <div className="d-flex justify-content-end">
                                <div className="next-btn">
                                    <Button>Next step</Button>
                                </div>
                            </div>
                        </div>

                        {/*step two*/}
                        <div className="step-two">

                            <div onClick={OpenCollapse} className="collapse-employment">
                                <div className="d-flex">
                                    <div className="">
                                        <FormGroup check>
                                            <Label check>
                                                <Input className="mt-2" type="checkbox"/>{' '}
                                                <div className="employment">
                                                    Employment Verification - 39$
                                                </div>
                                            </Label>
                                        </FormGroup>
                                    </div>
                                    <div className={openCollapse ? "down-icon" : "up-icon"}>
                                        <div>
                                            <img src="/assets/img/down.png" alt=""/>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <Collapse className="open-collapse" isOpen={openCollapse}>
                                <div className="additional-service">
                                    Additional services
                                </div>
                                <Row>
                                    <Col md={4}>
                                        <FormGroup check>
                                            <Label check>
                                                <Input className="mt-2" type="checkbox"/>{' '}
                                                <div className="employment">
                                                    Print color blank - 1$
                                                </div>
                                            </Label>
                                        </FormGroup>
                                        <FormGroup check>
                                            <Label check>
                                                <Input className="mt-2" type="checkbox"/>{' '}
                                                <div className="employment">
                                                    100 psc blank - 10$
                                                </div>
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                    <Col md={4}>
                                        <FormGroup check>
                                            <Label check>
                                                <Input className="mt-2" type="checkbox"/>{' '}
                                                <div className="employment">
                                                    Fast service - 10$
                                                </div>
                                            </Label>
                                        </FormGroup>
                                        <FormGroup check>
                                            <Label check>
                                                <Input className="mt-2" type="checkbox"/>{' '}
                                                <div className="employment">
                                                    The other - 5$
                                                </div>
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                    <Col md={4}>
                                        <FormGroup check>
                                            <Label check>
                                                <Input className="mt-2" type="checkbox"/>{' '}
                                                <div className="employment">
                                                    Service 1 - 10$
                                                </div>
                                            </Label>
                                        </FormGroup>
                                        <FormGroup check>
                                            <Label check>
                                                <Input className="mt-2" type="checkbox"/>{' '}
                                                <div className="employment">
                                                    Service 2 - 3$
                                                </div>
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </Collapse>
                        </div>

                        {/*step three*/}
                        <div className="step-three">
                            <div className="select-data">
                                Select Data
                            </div>
                            <div className="date">
                                <Row>
                                    <Col md={1}>
                                        <img className="down-arrow" src="/assets/img/down.png" alt=""/>
                                    </Col>

                                    <Col md={2}>
                                        <div className="day">
                                            15 Oct, 2020
                                        </div>
                                    </Col>
                                    <Col md={2}>
                                        <div className="day">
                                            16 Oct, 2020
                                        </div>
                                    </Col>
                                    <Col md={2}>
                                        <div className="day">
                                            17 Oct, 2020
                                        </div>
                                    </Col>
                                    <Col md={2}>
                                        <div className="day">
                                            18 Oct, 2020
                                        </div>
                                    </Col>
                                    <Col md={2}>
                                        <div className="day">
                                            19 Oct, 2020
                                        </div>
                                    </Col>
                                    <Col md={1}>
                                        <img className="down-arrow2" src="/assets/img/down.png" alt=""/>
                                    </Col>
                                </Row>
                            </div>

                            <div className="select-time">
                                Select Time
                            </div>
                            <div className="times">
                                <Row>
                                    <Col md={1}>
                                        <div className="time">08:00</div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="time">08:00</div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="time">08:00</div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="time">08:00</div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="time">08:00</div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="time">08:00</div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="time">08:00</div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="time">08:00</div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="time">08:00</div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="time">08:00</div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="time">08:00</div>
                                    </Col>
                                    <Col md={1}>
                                        <div className="time">08:00</div>
                                    </Col>
                                </Row>
                            </div>

                            <div className="step-footer">
                                <div className="write-text">
                                    We are also available outside of business hours at a
                                    special rate. <span className="pl-2">
                                    Write to the Manager
                                </span>
                                </div>
                                <div className="next-step">
                                    <div className="next-btn">
                                        <Button>Next step</Button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/*step four*/}
                        <div className="step-four">
                            <Row>
                                <Col md={6}>
                                    <Input className="step-four-input" placeholder="First name"/>
                                    <Input className="step-four-input" placeholder="Phone"/>
                                    <Input className="step-four-input" placeholder="Password"/>
                                </Col>
                                <Col md={6}>
                                    <Input className="step-four-input" placeholder="Last name"/>
                                    <Input className="step-four-input" placeholder="Email"/>
                                    <Input className="step-four-input" placeholder="Re-password"/>
                                </Col>
                            </Row>
                            <div className="notification-settings">
                                <FormGroup style={{marginTop: "50px"}} check>
                                    <Label check>
                                        <Input className="mt-2" type="checkbox"/>{' '}
                                        <div className="employment">
                                            Creating an account means you’re okay with
                                            our <a href="#">
                                            Terms of Service, Privacy Policy
                                        </a>,
                                            and our default <a href="#">
                                            Notification Settings
                                        </a> .
                                        </div>
                                    </Label>
                                </FormGroup>
                                <div className="next-btn">
                                    <Button>Next step</Button>
                                </div>
                            </div>
                        </div>

                        {/*step five*/}
                        <div className="step-five">
                            <Row>
                                <Col md={4}>
                                    <div className="order-type">
                                        <div className="type">
                                            In-person
                                        </div>
                                        <div className="commit">
                                            By using 3rd party payment system which will be provided by an agent.
                                        </div>
                                        <div className="selected-order-type">
                                            <img src="/assets/img/step.png" alt=""/>
                                        </div>
                                    </div>
                                </Col>
                                <Col md={4}>
                                    <div className="order-type">
                                        <div className="type">
                                            Online
                                        </div>
                                        <div className="commit">
                                            Using 3rd party payment system which will be
                                            embedded to the payment page.
                                        </div>
                                        <div className="selected-order-type">
                                            <img src="/assets/img/step.png" alt=""/>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </Container>
                </div>
                <Modal
                    onToggleModal={this.handleChangeModal}
                    visiblity={this.state.isOpen}
                />
            </div>
        );
    }
}


export default connect(({
                            order: {
                                stage, openCollapse
                            }
                        }) => ({stage, openCollapse}))(OrderInPerson);
