import React, {Component} from 'react';
import {connect} from "react-redux";
import {verifyEmail, verifyEmailClient} from "../redux/actions/AuthActions";
import {Link} from "react-router-dom";
import Button from "reactstrap/es/Button";
import LeftMenu from "../components/LeftMenu";
import Modal from "../components/common/modal";

class VerifyEmail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen : false,
        }
    }


    componentDidMount() {
        this.props.dispatch(verifyEmailClient({path: this.props.location.search}));
    }

    handleChangeModal = () => {
        this.setState({isOpen : !this.state.isOpen})
    };

    render() {
        const {dispatch, verifyEmail} = this.props;
        const handleClick = () => {
            this.props.history.push("login")
        }

        return (
            <div className="home-page">
                <LeftMenu
                    onToggleModal={this.handleChangeModal}
                    show={this.state.isOpen}
                />
                <div className="home-page-right">
                    <div className="text-center mt-5">
                        <h1>Email verification</h1>
                        {verifyEmail ?
                            <div className="flex-column">
                                <h4 className="mt-3">
                                    Congratulations! Account successfully activated!
                                </h4>
                                <Link to="/register">
                                    <Button color="primary" type="button" className="mt-2">Sign in</Button>
                                </Link>
                            </div>
                            : ""}
                    </div>
                </div>
                <Modal
                    onToggleModal={this.handleChangeModal}
                    visiblity={this.state.isOpen}
                />
            </div>
        );
    }
}


export default connect(({auth: {verifyEmail}}) => ({verifyEmail}))(VerifyEmail);

