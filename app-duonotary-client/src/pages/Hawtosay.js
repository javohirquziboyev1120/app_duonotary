import React, {Component} from 'react';
import {Link} from "react-router-dom";
import './homepage.scss'
import LeftMenu from "../components/LeftMenu";
import Modal from "../components/common/modal";
import {connect} from "react-redux";
import Loader from "../components/Loader";

class HomePage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loader : true,
            isOpen : false,
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({loader : false})
        },2000)
    }

    handleChangeModal = () => {
        this.setState({isOpen : !this.state.isOpen})
    };

    render() {

        const {
            modalShow,
            history,
            dispatch,
        } = this.props;

        if(this.state.loader) return <Loader/>

        return (
            <>
                <div className="home-page">
                    <LeftMenu
                        onToggleModal={this.handleChangeModal}
                        show={this.state.isOpen}
                    />
                    <div className="home-page-right">
                        <div className="home-header-section">
                            <div className="d-flex justify-content-between header">
                                <div className="back-icon">
                                    <Link to="/" className="text-decoration-none"><h4 className="text-uppercase"><img
                                        src="/assets/img/arrow-left.png"
                                        alt=""/> Back to home</h4></Link>
                                </div>
                                <div>
                                    <h3 className="mb-0 text-right">NEW YORK</h3>
                                    <h3 className="mb-0 text-right">09:11AM</h3>
                                </div>
                            </div>
                            <div className="d-flex link-section align-items-end">
                                <div className="header">
                                    <h3 className="mb-0">What our customers</h3>
                                    <h3 className="mb-0">have to say</h3>
                                </div>
                                <div className="ml-auto text-right get-in-touch">
                                    <Link onClick={this.handleChangeModal}>
                                        <span className="icon icon-link"/>
                                        <p className="mb-0">Get in touch <br/>
                                            with us now</p>
                                    </Link>
                                </div>

                            </div>
                        </div>
                        <div className="home-client-section d-flex justify-content-between align-items-center">
                            <div>
                                <div className="user-image">
                                    <img src="/assets/img/user.png" alt=""/>
                                    Anette Black
                                </div>
                                <h5>Great experience</h5>
                                <p className="mb-0">I live in Washington, D.C, and I had an issue that could not be resolved without a notarized
                                    signature. Thanks to the friendly and professional team I met at Duo Notary, I notarized my
                                    documents in no time. Their service was fast, inexpensive, and thorough. I would easily
                                    recommend it.</p>
                            </div>
                            <div>
                                <div className="dots-list">
                                    <div className="dot" />
                                    <div className="dot" />
                                    <div className="dot active" />
                                    <div className="dot" />
                                    <div className="dot" />
                                    <div className="dot" />
                                    <div className="dot" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal
                    onToggleModal={this.handleChangeModal}
                    visiblity={this.state.isOpen}
                />
            </>
        );
    }
}

export default connect(({auth : {modalShow}}) => ({modalShow}))(HomePage);