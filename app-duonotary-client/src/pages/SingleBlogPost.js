import React, {Component} from "react";
import { PostSmall} from "../components/BlogPost";
import LeftMenu from "../components/LeftMenu";
import {getBlogListFeatured, getBlogsUrl} from "../redux/actions/AppAction";
import {connect} from "react-redux";
import {config} from "../utils/config";
import { withRouter } from 'react-router-dom';
import Modal from "../components/common/modal";

class SingleBlogPost extends Component {
    constructor(props){
        super(props);
        this.state={
            title: "",
            body: "",
            imgId: "",
            isOpen : false,
        }
    }
    componentDidMount() {
        this.props.dispatch(getBlogsUrl({url:this.props.match.params.title}))
        this.props.dispatch(getBlogListFeatured())
    }

    routeChange=(url)=> {
        let path ="/blog/"+url;
        this.props.history.push(path);
        window.location.reload(); //Bad use of reloading
    }
    formatDate=(date)=>{
        let date1 = new Date(date);
        return  date1.toLocaleDateString("en-US", {year: 'numeric', month: 'long', day: 'numeric' });
    }

    handleChangeModal = () => {
        this.setState({isOpen : !this.state.isOpen})
    };

    render() {
        const {dispatch,blogByUrl,blogFeatureds}=this.props
        return (
            <div className="flex-row align-items-start">
                <LeftMenu
                    onToggleModal={this.handleChangeModal}
                    show={this.state.isOpen}
                />
                <div className="full-post flex-column">
                    {blogByUrl?   <div className="flex-column">
                        <img src={config.BASE_URL+"/attachment/"+blogByUrl.attachment.id} alt="blog-img" className="main-img" />
                        <span className="title">{blogByUrl.title}</span>
                        <div className="blog-body" dangerouslySetInnerHTML={{ __html: blogByUrl.text}} />


                        {/*<div className="flex-row">*/}
                        {/*    <img src={config.BASE_URL+"/userPhoto/"+blogByUrl.userPhotoId} alt="img-avatar" className="author-avatar-big"/>*/}
                        {/*    <span className="author-text-big">{blogByUrl.owner}</span>*/}
                        {/*</div>*/}
                        <span className="publish-date-big align-self-end mt-2">{this.formatDate(blogByUrl.createdAt)}</span>

                    </div>:  <span className="title">The post you're looking for is not found.</span>
                    }
                    <span className="featured-posts-title margin-top-25">Featured posts</span>

                    <div className="featured-posts ">
                        {/*MAP*/}
                        {
                            blogFeatureds.map((item,index)=>
                                <div key={index} className="child">
                                    <PostSmall key={index}  onClick={()=>{this.routeChange(item.url)}} title={item.title} mainImg={config.BASE_URL+"/attachment/"+item.attachment.id}/>
                                </div>
                            )
                        }

                        {/*MAP end*/}
                    </div>
                </div>
                <Modal
                    onToggleModal={this.handleChangeModal}
                    visiblity={this.state.isOpen}
                />
            </div>
        );
    }
}


export default connect(
    ({
         app: {
             showModal, currentItem,  showDeleteModal, showStatusModal,
             activePageBlog,
             blogsPage,blogByUrl,blogFeatureds
         },
     }) => ({
        showModal,
        currentItem,
        showDeleteModal, showStatusModal, activePageBlog,
        blogsPage, blogByUrl,blogFeatureds
    }))(withRouter(SingleBlogPost));
