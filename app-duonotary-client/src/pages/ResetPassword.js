import React, {Component} from 'react';
import {connect} from "react-redux";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {Button, Col, Row} from "reactstrap";
import { resetPasswordAction} from "../redux/actions/AuthActions";
import {toast} from "react-toastify";

class ResetPassword extends Component {
    render() {
        const {dispatch, history} = this.props;
        const confirmCode = (e, v) => {
            if (v.password !== v.prePassword) {
                toast.error("passwords is not matches")
                return "";
            }
            if ((v.password.length < 6 && v.prePassword.length < 6) || (v.password.length > 32 && v.prePassword.length > 32)) {
                toast.error("password  length 6-32 between")
                return "";
            }
            v.emailCode = window.location.pathname.substring(15);
            dispatch(resetPasswordAction({v, history}))
        }
        return (
            <div className="forgot-password-page">
                <div className="container">
                    <Row>
                        <Col md={6} className="mx-auto">
                            <div className="info">
                                You can change your password via email or a
                                message sent to your phone number
                            </div>
                            <div className="enter-password">
                                <h3>Change password</h3>
                                <AvForm onValidSubmit={confirmCode}>
                                    <AvField name="password"
                                             placeholder="Enter  password" required/>

                                    <AvField name="prePassword"
                                             placeholder="Enter  re-password" required/>
                                    <div className="d-flex justify-content-end">
                                        <Button>Submit</Button>
                                    </div>
                                </AvForm>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}


export default connect(({app: {}}) => ({}))(ResetPassword);