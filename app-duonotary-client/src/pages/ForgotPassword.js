import React, {Component} from 'react';
import {connect} from "react-redux";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {Button, Col} from "reactstrap";
import {forgotPasswordAction} from "../redux/actions/AuthActions";

class ForgotPassword extends Component {
    constructor(props) {
        super(props);

        this.state = {
            changePassword: true
        }
    }


    render() {
        const {changePassword} = this.state
        const {history} = this.props;
        const forgotPassword = (e, v) => {
            v.isEmail = v.username.includes('@');
            v.username = v.username.trim();
            this.props.dispatch(forgotPasswordAction({v, history}));
        }

        const ChangePassword = () => {
            this.setState({
                changePassword: !changePassword
            })
        }

        return (
            <div className="forgot-password-page">


                                <Col md={6} sm={9} className="">
                                    <div className="enter-password">
                                        <h3>Did you forget your password</h3>
                                        <AvForm onValidSubmit={forgotPassword}>
                                            <AvField name="username"
                                                     placeholder="Enter  your email or phone number" required/>
                                            <div className="d-flex justify-content-end">
                                                <Button >Submit</Button>
                                            </div>
                                        </AvForm>
                                    </div>
                                </Col>
                        

            </div>
        );
    }
}

ForgotPassword.propTypes = {};

export default connect(({app: {}}) => ({}))(ForgotPassword);