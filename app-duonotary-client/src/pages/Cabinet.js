import React, {Component} from 'react';
import {connect} from "react-redux";

class Cabinet extends Component {

    render() {
        const {dispatch} = this.props;

        const handleClick = () => {
            this.props.history.push("login")
        }

        return (
            <div>
                <button type="button" onClick={handleClick}>
                    Go Home
                </button>
            </div>
        );
    }
}


export default connect(({}) => ({}))(Cabinet);

