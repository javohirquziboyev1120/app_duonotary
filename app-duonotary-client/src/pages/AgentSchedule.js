import React, {Component} from 'react';
import {
    changeScheduleDayOff,
    deleteAgentSchedule,
    getAgentScheduleList,
    getWeekDayList,
    saveAgentSchedule
} from "../redux/actions/AppAction";
import {connect} from "react-redux";
import {Button, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Table} from "reactstrap";
import StatusModal from "../components/Modal/StatusModal";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {userMe} from "../redux/actions/AuthActions";
import DeleteModal from "../components/Modal/DeleteModal";

class AgentSchedule extends Component {
    componentDidMount() {
        this.props.dispatch(userMe())
        this.props.dispatch(getAgentScheduleList())
        this.props.dispatch(getWeekDayList());
    }

    render() {
        const {dispatch, showModal, agentSchedules, weekDays, currentItem, active, showDeleteModal, showStatusModal, weekDayId, dayOff, agentHours, currentUser} = this.props;

        const saveItem = (e, v) => {
            v.id = currentItem != null ? currentItem.id : null
            v.active = active
            v.currentUser = currentUser
            this.props.dispatch(saveAgentSchedule(v))
        }

        const openModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showModal: !showModal,
                    dayOff: item.dayOff != null ? item.dayOff : false,
                    currentItem: item,
                    agentHours: item.hourList != null ? item.hourList : [{id: new Date().toString()}]
                }
            })
        };

        const changeStatusAgentScheduleDay = () => {
            let currentSchedule = {...currentItem};
            currentSchedule.dayOff = !currentItem.dayOff;
            this.props.dispatch(changeScheduleDayOff(currentSchedule))
        }

        const changeDayOff = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    dayOff: !dayOff,
                }
            })
        }

        const deleteFunction = () => {
            this.props.dispatch(deleteAgentSchedule(currentItem))
        };

        const openDeleteModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showDeleteModal: !showDeleteModal,
                    currentItem: item
                }
            })
        };

        const openStatusModal = (item) => {
            dispatch({
                type: 'updateState',
                payload: {
                    showStatusModal: !showStatusModal,
                    currentItem: item
                }
            })
        };

        const removeHour = (j) => {
            let hours = agentHours.filter(k => k !== j);
            dispatch({
                type: 'updateState',
                payload: {
                    agentHours: hours
                }
            })
        };

        const addHour = () => {
            let hour = {id: new Date().toString()}
            let hours = [...agentHours, hour]
            dispatch({
                type: 'updateState', payload: {agentHours: hours}
            })
        };
        return (
            <div className="container">
                <h2 className="text-center">Weekly schedule</h2>
                {agentSchedules !== undefined && agentSchedules.length < 7 ?
                    <Button color="success" outline className="my-2" onClick={() => openModal('')}>+ Add</Button>
                    : ''}
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Week day</th>
                        <th>Hours</th>
                        <th>Day Off</th>
                        <th colSpan="2">Operation</th>
                    </tr>
                    </thead>
                    {agentSchedules != null ?
                        <tbody>
                        {agentSchedules.map((item, i) =>
                            <tr key={i + 1}>
                                <td>{i + 1}</td>
                                <td>{item.weekDay.day}</td>
                                <td>{
                                    item.hourList.map((item, i) =>
                                        <div id={i + 1}>
                                            <h5 style={{color: 'green'}}>From {item.fromTime}</h5>
                                            <h5 style={{color: 'red'}}>To {item.tillTime}</h5>
                                        </div>
                                    )
                                }</td>
                                <td>
                                    {/*{item.dayOff ? <h5 style={{color: 'red'}}>Inactive</h5> : <h5 style={{color: 'green'}}>Active</h5>}*/}
                                    <td>
                                        <FormGroup check>
                                            <Label check>
                                                <Input type="checkbox" onClick={() => openStatusModal(item)}
                                                       checked={item.dayOff}/>
                                                {item.dayOff ? "Active" : "Inactive"}
                                            </Label>
                                        </FormGroup>
                                    </td>
                                </td>

                                <td><Button color="warning" outline onClick={() => openModal(item)}>Edit</Button></td>
                                <td><Button color="danger" outline onClick={() => openDeleteModal(item)}>Delete</Button>
                                </td>

                            </tr>
                        )}
                        </tbody> :
                        <tbody>
                        <tr>
                            <td colSpan="6">
                                <h3 className="text-center mx-auto"> No information </h3>
                            </td>
                        </tr>
                        </tbody>
                    }
                </Table>

                <Modal isOpen={showModal} toggle={openModal}>
                    <AvForm onValidSubmit={saveItem}>
                        <ModalHeader toggle={openModal}
                                     charCode="x">{currentItem != null ? "Edit schedule" : "Add schedule"}</ModalHeader>

                        <ModalBody>
                            <AvField name="weekDayId"
                                     type="select"
                                     defaultValue={currentItem != null ? (currentItem.weekDay != null ? currentItem.weekDay.id : '') : ''}
                                     label="Week day"
                                     required
                                     placeholder="Choose week day">

                                <option value="">Select week day</option>

                                {weekDays.map(item =>
                                    <option value={item.id}>{item.day}</option>
                                )}
                            </AvField>

                            <AvField name="dayOff"
                                     type="checkbox"
                                     checked={dayOff}
                                     label="Day off"
                                     onClick={() => changeDayOff(!dayOff)}/>

                            {agentHours.map((item, i) => (
                                <div key={item.id} className="row">
                                    <div className="d-flex">
                                        <AvField
                                            value={agentHours.length > 0 ? agentHours[i].fromTime : ''}
                                            label="From time"
                                            style={{paddingLeft: "20px"}}
                                            name={"hourList[" + i + "].fromTime"}
                                            placeholder="09:00:00"
                                            type="time"
                                            validate={{
                                                required: {value: true},
                                                // minLength: {value: 13},
                                                // maxLength: {value: 13}
                                            }}/>
                                        <AvField
                                            value={agentHours.length > 0 ? agentHours[i].tillTime : ''}
                                            label="Till time"
                                            style={{paddingLeft: "30px"}}
                                            name={"hourList[" + i + "].tillTime"}
                                            placeholder="10:00:00"
                                            type="time"
                                            validate={{
                                                required: {value: true},
                                                // minLength: {value: 13},
                                                // maxLength: {value: 13}
                                            }}/>
                                        {i !== 0 ?

                                            <div style={{cursor: "pointer", paddingTop: "40px", paddingLeft: "20px"}}>
                                                <button type="button" onClick={() => removeHour(item)}>
                                                    -
                                                </button>
                                            </div>

                                            :

                                            <div style={{cursor: "pointer", paddingTop: "40px", paddingLeft: "40px"}}>
                                                <button type="button" onClick={addHour}>
                                                    +
                                                </button>
                                            </div>

                                        }
                                    </div>
                                </div>
                            ))}


                        </ModalBody>
                        <ModalFooter>
                            <Button type="button" color="" onClick={openModal}>Cancel</Button>{' '}
                            <Button color="success">Save</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>

                {showDeleteModal && <DeleteModal text={currentItem != null ? currentItem.weekDay.day : ''}
                                                 showDeleteModal={showDeleteModal}
                                                 confirm={deleteFunction}
                                                 cancel={openDeleteModal}
                />}

                {showStatusModal && <StatusModal text={currentItem != null ? currentItem.weekDay.day : ''}
                                                 showStatusModal={showStatusModal}
                                                 confirm={changeStatusAgentScheduleDay}
                                                 cancel={openStatusModal}
                />}

            </div>
        );
    }
}

export default connect(
    ({
         app: {
             weekDays, agentSchedules, showModal, currentItem, active, showDeleteModal, showStatusModal, weekDayId, dayOff, agentHours
         },
         auth: {currentUser}
     }) => ({
        weekDays, agentSchedules,
        showModal,
        currentItem,
        active, showDeleteModal, showStatusModal, weekDayId, dayOff, agentHours,
        currentUser
    }))(AgentSchedule);