export const REQUEST_ADDITIONAL_SERVICE_PRICE_START = 'REQUEST_ADDITIONAL_SERVICE_PRICE_START';
export const REQUEST_ADDITIONAL_SERVICE_PRICE_SUCCESS = 'REQUEST_ADDITIONAL_SERVICE_PRICE_SUCCESS';
export const REQUEST_ADDITIONAL_SERVICE_PRICE_ERROR = 'REQUEST_ADDITIONAL_SERVICE_PRICE_ERROR';
export const REQUEST_GET_ADDITIONAL_SERVICE_PRICE_LIST_SUCCESS = 'REQUEST_GET_ADDITIONAL_SERVICE_PRICE_LIST_SUCCESS';
export const REQUEST_GET_ADDITIONAL_SERVICE_PRICE_DASHBOARD = 'REQUEST_GET_ADDITIONAL_SERVICE_PRICE_DASHBOARD';
export const REMOVE_COUNTYARR_ZIPCODEARR = 'REMOVE_COUNTYARR_ZIPCODEARR';
