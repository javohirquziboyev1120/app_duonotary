import * as app from "../../api/AppApi";
import * as types from "../actionTypes/AppActionTypes"
import * as appTypes from "../actionTypes/AppActionTypes"
import * as typesAdm from "../actionTypes/AdminActionTypes"
import {toast} from "react-toastify";
import {getAdditionalServicePriceDashboard} from "./AdditionalServicePriceAction";
import {getAllByUserId} from "./OrderAction";


export const getCustomersBySearch = (data) => (dispatch, getState) => {
    dispatch({
        api: app.getUsersBySearch,
        types: [appTypes.REQUEST_START, "", appTypes.REQUEST_ERROR],
        data: data
    }).then(res => {
        let filter = []
        Object.keys(res.data).map(item => {
            if (item === "search" || item === "zipCode" || item === 'date')
                filter.push({key: item, value: res.data[item]})
        })
        let result = []
        res.payload.map(item => {
            let customer = JSON.parse(item)
            result.push(customer)
        })
        dispatch({
            type: 'updateState',
            payload: {
                customers: result,
                loading: false,
                filters: filter,
                currentItem: '',
                search: data.search,
                totalElements: result && result.length > 0 && result[0].totalElements ? result[0].totalElements : 0,
            }
        })
    })
}

// Get sharing discount
export const getSharingDiscountList = (data) => (dispatch) => {
    dispatch({
        api: app.getSharingDiscountList,
        types: [
            types.REQUEST_GET_SHARING_DISCOUNT_LIST_START,
            types.REQUEST_GET_SHARING_DISCOUNT_LIST_SUCCESS,
            types.REQUEST_GET_SHARING_DISCOUNT_LIST_ERROR
        ],
        data: data
    })
};
//Start Audit function
export const getAudit = (data) => (dispatch) => {
    dispatch({
        api: app.getAudit,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_AUDIT_LIST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    })
};
export const getUserForAudit = (data) => (dispatch) => {
    dispatch({
        api: app.getUserForAudit,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_USER_FOR_AUDIT,
            types.REQUEST_ERROR
        ],
        data: data
    })
};
export const getOneItemAudit = (data) => (dispatch) => {
    dispatch({
        api: app.getOneItemForAudit,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_AUDIT_ONE_ITEM_LIST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    })
};
export const getItemByAuthor = (data) => (dispatch) => {
    dispatch({
        api: app.getItemByAuthor,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_AUDIT_ITEM_LIST_BY_AUTHOR_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    })
};
export const getTotalElementsCount = (data) => (dispatch) => {
    dispatch({
        type: "updateState",
        payload: {
            totalElements: 0
        }
    })
    dispatch({
        api: app.getTotalElementsCount,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_AUDIT_TOTAL_ELEMENT_COUNT,
            types.REQUEST_ERROR
        ],
        data: data
    })
};
export const getAuditForItemWithCommitId = (data) => (dispatch) => {
    dispatch({
        api: app.getAuditForItemWithCommitId,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_AUDIT_ONE_LIST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    })
};
export const getAuditTables = () => (dispatch) => {
    dispatch({
        api: app.getAuditTables,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_AUDIT_TABLE_NAME_SUCCESS,
            types.REQUEST_ERROR
        ]
    })

};
//End Audit function
// START COUNTRY FUNCTION
export const getCountryList = (payload) => (dispatch) => {
    dispatch({
        api: app.getCountries,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_COUNTRY_LIST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    })

};
export const saveCountry = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editCountry : app.addCountry,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                countries: null
            }
        })
        dispatch(getCountryList())
        toast.success("Country saved successfully!");
    }).catch(err => {
        toast.error("Error saving country!");
    })
};
export const deleteCountry = (payload) => (dispatch) => {
    dispatch({
        api: app.deleteCountry,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                countries: null
            }
        })
        dispatch(getCountryList())
        dispatch({
            type: types.REQUEST_SUCCESS
        })
        toast.success("Country deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting country!");
    })
};

export const addPartners = (payload) => (dispatch) => {
    dispatch({
        api: app.addPartner,
        types: [types.REQUEST_START,
            "",
            types.REQUEST_ERROR],
        data: payload
    }).then(res => {
        dispatch(getPartners());
    }).catch(err => toast.error('Error on adding'))

}
export const editPartners = (payload) => (dispatch) => {
    dispatch({
        api: app.editPartner,
        types: [
            types.REQUEST_START,
            "",
            types.REQUEST_ERROR],
        data: payload
    }).then(res => {
        dispatch(getPartners());
    }).catch(err => toast.error('Error on editing'))

}
export const deletePartners = (payload) => (dispatch) => {
    dispatch({
        api: app.deletePartner,
        types: [
            types.REQUEST_START,
            "",
            types.REQUEST_ERROR],
        data: payload
    }).then(res => {
        dispatch(getPartners());
    }).catch(err => toast.error('Error on deleting'))

}
export const getPartners = () => (dispatch) => {
    dispatch({type: 'updateState', payload: {partners: ''}})
    dispatch({
        api: app.getPartners,
        types: [
            types.REQUEST_START,
            "",
            types.REQUEST_ERROR
        ]
    }).then(res => {
        console.log(res);
        dispatch({type: 'updateState', payload: {partners: res.payload._embedded.list}})
    })

}
export const getPartnersForHome = () => (dispatch) => {
    dispatch({
        api: app.getPartnerForHome,
        types: [
            types.REQUEST_START,
            "",
            types.REQUEST_ERROR
        ]
    }).then(res => {
        dispatch({
            type: 'updateState', payload: {
                partners: res.payload && res.payload._embedded && res.payload._embedded.list ? res.payload._embedded.list : ""
            }
        })
    })

}

// FINISH COUNTRY FUNCTION

export const editMainService = (payload) => (dispatch) => {
    dispatch({
        api: app.editMainService,
        types: [
            types.REQUEST_EDIT_MAIN_SERVICE_START,
            types.REQUEST_EDIT_MAIN_SERVICE_SUCCESS,
            types.REQUEST_EDIT_MAIN_SERVICE_ERROR
        ],
        data: payload
    })
}

//START PUBLIC HOLIDAY FUNCTION
export const getPublicHolidays = () => (dispatch) => {
    dispatch({
        api: app.getPublicHolidays,
        types: [
            types.REQUEST_GET_PUBLIC_HOLIDAY_START,
            types.REQUEST_GET_PUBLIC_HOLIDAY_SUCCESS,
            types.REQUEST_GET_PUBLIC_HOLIDAY_ERROR
        ]
    })
}
export const getPublicHolidaysPage = () => (dispatch) => {
    dispatch({
        api: app.getPublicHolidaysPage,
        types: [
            types.REQUEST_GET_PUBLIC_HOLIDAY_START,
            types.REQUEST_GET_PUBLIC_HOLIDAY_SUCCESS,
            types.REQUEST_GET_PUBLIC_HOLIDAY_ERROR
        ]
    })
}
export const saveHolidays = (payload) => (dispatch) => {
    dispatch({
        api: app.saveHolidays,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getPublicHolidays())
            toast.success("Public holidays changed successfully!");
        } else {
            toast.error("You cannot change public holiday!")
        }
    }).catch(err => {
        toast.error("Error changing public holiday!");
    })
};
export const getCheckOrderDay = (payload) => (dispatch) => {
    dispatch({
        api: app.isOrderDay,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (!res) {
            dispatch(savePublicHolidays(payload))
        } else {
            toast.error("This day have order!")
        }
    }).catch(err => {
        toast.error("Error changing public holiday!");
    })
};
export const savePublicHolidays = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editPublicHolidays : app.addPublicHolidays,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getPublicHolidays())
            toast.success("Public holiday saved successfully!");
        } else {
            toast.error("You cannot save public holiday!")
        }
    }).catch(err => {
        toast.error("Error saving public holiday!");
    })
};
export const deletePublicHolidays = (payload) => (dispatch) => {
    dispatch({
        api: app.deletePublicHolidays,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                holidays: null
            }
        })
        dispatch(getPublicHolidays())
        dispatch({
            type: types.REQUEST_SUCCESS
        })

        toast.success("Public holiday deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting public holiday!");
    })
};

//FINISH PUBLIC HOLIDAY FUNCTION

// START STATE FUNCTION
export const getStateList = () => (dispatch) => {
    dispatch({
        api: app.getStates,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_STATE_LIST_SUCCESS,
            types.REQUEST_ERROR
        ]
    })

};
export const saveState = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editState : app.addState,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getStateList())
            toast.success("State saved successfully!");
        } else {
            toast.error("You cannot save state!")
        }
    }).catch(err => {
        toast.error("Error saving state!");
    })
};
export const deleteState = (payload) => (dispatch) => {
    dispatch({
        api: app.deleteState,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch(getStateList())
        dispatch({
            type: types.REQUEST_SUCCESS
        })
        toast.success("State deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting state!");
    })
};
// FINISH COUNTRY FUNCTION

// START ServicePrice FUNCTION
export const getServicePriceList = (payload) => (dispatch) => {
    dispatch({
        api: app.getServicePrice,
        types: [
            types.REQUEST_SERVICE_PRICE_START,
            types.REQUEST_GET_SERVICE_PRICE_LIST_SUCCESS,
            types.REQUEST_SERVICE_PRICE_ERROR
        ],
    })
};
export const getServicePrices = () => (dispatch) => {
    dispatch({
        api: app.getServicePrice,
        types: [
            types.REQUEST_SERVICE_PRICE_START,
            types.REQUEST_GET_SERVICE_PRICE_LIST_SUCCESS,
            types.REQUEST_SERVICE_PRICE_ERROR
        ],
    })
}
export const getServicePriceDashboard = () => (dispatch) => {
    dispatch({
        api: app.getServicePriceDashboard,
        types: [
            types.REQUEST_SERVICE_PRICE_START,
            types.REQUEST_GET_SERVICE_PRICE_DASHBOARD_LIST_SUCCESS,
            types.REQUEST_SERVICE_PRICE_ERROR
        ],
    })

};
export const getServiceByState = (payload) => (dispatch) => {
    dispatch({
        api: app.getServiceByState,
        types: [
            types.REQUEST_SERVICE_PRICE_START,
            types.REQUEST_GET_SERVICE_PRICE_BY_SERVICE_LIST_SUCCESS,
            types.REQUEST_SERVICE_PRICE_ERROR
        ],
        data: payload
    })

};
export const getCountyByState = (payload) => (dispatch) => {
    dispatch({
        api: app.getCountyByState,
        types: [
            types.REQUEST_SERVICE_PRICE_START,
            types.REQUEST_GET_SERVICE_PRICE_BY_STATE_LIST_SUCCESS,
            types.REQUEST_SERVICE_PRICE_ERROR
        ],
        data: payload
    })
};

export const getCountyByStateForCalendar = (payload) => (dispatch) => {
    dispatch({
        api: app.getCountyByStateForCalendar,
        types: [
            types.REQUEST_SERVICE_PRICE_START,
            types.REQUEST_GET_SERVICE_PRICE_BY_STATE_LIST_SUCCESS,
            types.REQUEST_SERVICE_PRICE_ERROR
        ],
        data: payload
    })
};
export const getZipCodeByCountyForCalendar = (payload) => (dispatch) => {
    dispatch({
        api: app.getZipCodeByCountyForCalendar,
        types: [
            types.REQUEST_SERVICE_PRICE_START,
            types.REQUEST_GET_SERVICE_PRICE_BY_COUNTY_LIST_SUCCESS,
            types.REQUEST_SERVICE_PRICE_ERROR
        ],
        data: payload
    })

};
export const getZipCodeByCounty = (payload) => (dispatch) => {
    dispatch({
        api: app.getZipCodeByCounty,
        types: [
            types.REQUEST_SERVICE_PRICE_START,
            types.REQUEST_GET_SERVICE_PRICE_BY_COUNTY_LIST_SUCCESS,
            types.REQUEST_SERVICE_PRICE_ERROR
        ],
        data: payload
    })

};
export const saveServicePrice = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editServicePrice : app.addServicePrice,
        types: [
            types.REQUEST_SERVICE_PRICE_START,
            types.REQUEST_SERVICE_PRICE_SUCCESS,
            types.REQUEST_SERVICE_PRICE_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getServicePriceDashboard())
            toast.success("ServicePrice saved successfully!");
        } else {
            toast.error("You cannot save ServicePrice!")
        }
    }).catch(err => {
        toast.error("Error saving ServicePrice!");
    })
};
export const editServicePriceActive = (payload) => (dispatch) => {
    dispatch({
        api: app.editServicePriceActive,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getServicePriceList())
            toast.success("ServicePrice edit active successfully!");
        } else {
            toast.error("You cannot edit ServicePrice!")
        }
    }).catch(err => {
        toast.error("Error edit ServicePrice!");
    })
};
// FINISH ServicePrice FUNCTION

// START Pricing FUNCTION
export const getPricingList = () => (dispatch) => {
    dispatch({
        api: app.getPricing,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_PRICING_LIST_SUCCESS,
            types.REQUEST_ERROR
        ]
    })
};
export const getDashboardPricingList = () => (dispatch) => {
    dispatch({
        api: app.getDashboardPricing,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_DASHBOARD_PRICING_LIST_SUCCESS,
            types.REQUEST_ERROR
        ]
    })
};
export const getStateForPricing = (data) => (dispatch) => {
    dispatch({
        api: app.getStateByPricing,
        types: [
            types.REQUEST_PRICING_SUCCESS,
            types.REQUEST_GET_STATE_PRICING_LIST_SUCCESS,
            types.REQUEST_PRICING_ERROR
        ],
        data: data
    })
};
export const getPricingByStates = (data) => (dispatch) => {
    dispatch({
        api: app.getPricingByState,
        types: [
            types.REQUEST_PRICING_SUCCESS,
            types.REQUEST_GET_STATE_BY_PRICING_LIST_SUCCESS,
            types.REQUEST_PRICING_ERROR
        ],
        data: data
    })
};

export const savePricing = (payload) => (dispatch) => {
    dispatch({
        api: app.addPricing,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        console.log(res);
        if (res.success) {
            dispatch(getDashboardPricingList())
            toast.success(res.payload.message);
        } else {
            toast.error(res.payload.message)
        }
    }).catch(err => {
        toast.error("Error saving pricing!");
    })
};

export const editPricing = (payload) => (dispatch) => {
    dispatch({
        api: app.editPricing,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        console.log(res);
        if (res.success) {
            dispatch(getDashboardPricingList())
            toast.success(res.payload.message);
        } else {
            toast.error(res.payload.message)
        }
    }).catch(err => {
        toast.error("Error edit pricing!");
    })
};
export const deletePricing = (payload) => (dispatch) => {
    dispatch({
        api: app.deletePricing,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getDashboardPricingList())
            toast.success(res.payload.message);
        } else {
            toast.error(res.payload.message)
        }
    }).catch(err => {
        toast.error("Error delete pricing!");
    })
};
// FINISH Pricing FUNCTION

// START COUNTY FUNCTION
export const getCountiesList = () => (dispatch) => {
    dispatch({
        api: app.getCounties,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_COUNTY_LIST_SUCCESS,
            types.REQUEST_ERROR
        ]
    })
};
export const saveCounty = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editCounty : app.addCounty,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getCountiesList())
            toast.success("County saved successfully!");
        } else {
            toast.error("You cannot save county!")
        }
    }).catch(err => {
        toast.error("Error saving county!");
    })
};
export const deleteCounty = (payload) => (dispatch) => {
    dispatch({
        api: app.deleteCounty,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch(getCountiesList())
        dispatch({
            type: types.REQUEST_SUCCESS
        })
        toast.success("County deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting county!");
    })
};
// FINISH COUNTY FUNCTION
//START SERVICE FUNCTION
export const getServiceList = () => (dispatch) => {
    dispatch({
        api: app.getServices,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_SERVICE_LIST_SUCCESS,
            types.REQUEST_ERROR
        ]
    })

};
export const getServicePage = (data) => (dispatch) => {
    dispatch({
        api: app.getServicePage,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_SERVICE_PAGE_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    })

};
export const getServiceListByServicePrice = (data) => (dispatch) => {
    dispatch({
        api: app.getServicesByServicePrice,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_SERVICE_LIST_BY_SERVICE_PRICE_SUCCESS,
            types.REQUEST_ERROR
        ],
        data
    })

};
export const saveService = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editService : app.addService,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getServicePage({page: 0, size: 10}))
            toast.success(res.payload.message);
        } else {
            toast.error(res.payload.message)
        }
    }).catch(err => {
        toast.error("Error saving Service!");
    })
};
export const saveFirstService = () => (dispatch) => {
    dispatch({
        api: app.addFirstService,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
    }).then(res => {
        dispatch(getServicePage({page: 0, size: 10}))
    });
};
export const deleteService = (data) => (dispatch) => {
    dispatch({
        api: app.deleteService,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    }).then(res => {
        dispatch(getServiceList())
        dispatch(getServicePage({page: 0, size: 10}))
        dispatch({
            type: types.REQUEST_SUCCESS
        })
        toast.success("Deleted");
    }).catch(err => {
        toast.error("Error deleting Service!");
    })
};
export const editServiceChangeActive = (payload) => (dispatch) => {
    dispatch({
        api: app.editServiceChangeActive,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch(getServicePriceDashboard())
        dispatch(getAdditionalServicePriceDashboard())
        dispatch(getDashboardPricingList())
        dispatch(getServicePage({page: 0, size: 10}))
        dispatch({
            type: types.REQUEST_SUCCESS
        })
        toast.success("Edit change status successfully!");
    }).catch(err => {
        toast.error("Error! Please try again!");
    })
};
//FINISH MAIN SERVICE FUNCTION

//START SERVICE FUNCTION

export const getMainServiceList = () => async (dispatch) => {
    await dispatch({
        api: app.getMainServices,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_MAIN_SERVICE_LIST_SUCCESS,
            types.REQUEST_ERROR
        ]
    })
};
export const saveMainService = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editMainService : app.addMainService,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getMainServiceList())
            toast.success("Main Service saved successfully!");
        } else {
            toast.error("You cannot save Main Service!")
        }
    }).catch(err => {
        toast.error("Error saving Main Service!");
    })
};
export const deleteMainService = (payload) => (dispatch) => {
    dispatch({
        api: app.deleteMainService,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                mainServices: null
            }
        })
        dispatch(getMainServiceList())
        dispatch({
            type: types.REQUEST_SUCCESS
        })
        toast.success("Main Service deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting Main Service!");
    })
};
//FINISH MAIN SERVICE FUNCTION

//START MAIN SERVICE WITH PERCENT FUNCTION
export const getMainServicesWithPercent = () => (dispatch) => {
    dispatch({
        api: app.getMainServicesWithPercent,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_MAIN_SERVICES_WITH_PERCENT_SUCCESS,
            types.REQUEST_ERROR
        ]
    })

};

export const saveMainServiceWithPercent = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editMainServiceWithPercent : app.addMainServiceWithPercent,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getMainServicesWithPercent())
            toast.success("Main Service with percent saved successfully!");
        } else {
            toast.error("You cannot save Main Service with percent!")
        }
    }).catch(err => {
        toast.error("Error saving Main Service! with percent");
    })
};
export const deleteMainServiceWithPercent = (payload) => (dispatch) => {
    dispatch({
        api: app.deleteMainServiceWithPercent,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                mainServicesWithPercent: null
            }
        })
        dispatch(getMainServicesWithPercent())
        dispatch({
            type: types.REQUEST_SUCCESS
        })
        toast.success("Main Service with percent deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting Main Service with percent!");
    })
};
//FINISH MAIN SERVICE WITH PERCENT FUNCTION

//START PAY TYPE FUNCTION
export const getPayTypes = () => (dispatch) => {
    dispatch({
        api: app.getPayType,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_PAY_TYPE_LIST_SUCCESS,
            types.REQUEST_ERROR
        ]
    })

};

export const savePayType = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editPayType : app.addPayType,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch(getPayTypes())
        dispatch({
            type: types.REQUEST_SUCCESS
        })
        toast.success("Pay Type saved successfully!");
    }).catch(err => {
        toast.error("Error saved Pay Type!");
    })

};
export const deletePayType = (payload) => (dispatch) => {
    dispatch({
        api: app.deletePayType,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch(getPayTypes())
        dispatch({
            type: types.REQUEST_SUCCESS
        })
        toast.success("Pay Type with percent deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting Pay Type with percent!");
    })
};
//FINISH PAY TYPE FUNCTION


//START SUB SERVICE FUNCTION
export const getSubServiceList = () => async (dispatch) => {
    await dispatch({
        api: app.getSubServices,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_SUB_SERVICE_LIST_SUCCESS,
            types.REQUEST_ERROR
        ]
    })

};
export const getSubServicePage = (data) => (dispatch) => {
    dispatch({
        api: app.getSubServicePage,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_SUB_SERVICE_PAGE_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    })

};
export const saveSubService = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editSubService : app.addSubService,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getSubServiceList())
            dispatch(getSubServicePage({page: 0, size: 10}))
            dispatch(getServicePage({page: 0, size: 10}))
            toast.success("Sub Service saved successfully!");
        } else {
            toast.error("You cannot save Sub Service!")
        }
    }).catch(err => {
        toast.error("Error saving Sub Service!");
    })
};
export const deleteSubService = (payload) => (dispatch) => {
    dispatch({
        api: app.deleteSubService,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch(getSubServiceList())
        dispatch(getSubServicePage({page: 0, size: 10}))

        dispatch({
            type: types.REQUEST_SUCCESS
        })
        toast.success("Sub Service deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting Sub Service!");
    })
};
//FINISH SUB SERVICE FUNCTION


//START ADDITIONAL SERVICE FUNCTION
export const getAdditionalServiceList = () => (dispatch) => {
    dispatch({
        api: app.getAdditionalServices,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_ADDITIONAL_SERVICE_LIST_SUCCESS,
            types.REQUEST_ERROR
        ]
    })

};
export const getAdditionalServicePage = (data) => (dispatch) => {
    dispatch({
        api: app.getAdditionalServicePage,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_ADDITIONAL_SERVICE_PAGE_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    })

};

export const saveAdditionalService = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editAdditionalService : app.addAdditionalService,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch(getAdditionalServiceList())
        dispatch(getAdditionalServicePage({page: 0, size: 10}))
        toast.success(payload.id ? "Additional Service edit successfully!" : "Additional Service saved successfully!");
    }).catch(err => {
        toast.error("Error saving Additional Service!");
    })
};
export const deleteAdditionalService = (payload) => (dispatch) => {
    dispatch({
        api: app.deleteAdditionalService,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch(getAdditionalServiceList())
        dispatch(getAdditionalServicePage({page: 0, size: 10}))
        dispatch({
            type: types.REQUEST_SUCCESS
        })
        toast.success("Additional Service deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting Additional Service!");
    })
};
//FINISH ADDITIONAL SERVICE FUNCTION

//START documentType FUNCTION
export const getDocumentTypeList = () => (dispatch) => {
    dispatch({
        api: app.getDocumentType,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_DOCUMENT_TYPE_LIST_SUCCESS,
            types.REQUEST_ERROR
        ]
    }).then(res => {
        dispatch({
            type: "updateState", payload: {
                documentTypes: res && res.payload && res.payload._embedded ? res.payload._embedded.list : ""
            }
        })
    })

};
export const getDocumentTypePage = (data) => (dispatch) => {
    dispatch({
        api: app.getDocumentTypePage,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_DOCUMENT_TYPE_PAGE_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    })

};
export const saveDocumentType = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editDocumentType : app.addDocumentType,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            // dispatch(getDocumentTypeList())
            dispatch(getDocumentTypePage({page: 0, size: 10}))
            toast.success("Document type saved successfully!");
        } else {
            toast.error("You cannot save Document type!")
        }
    }).catch(err => {
        toast.error("Error saving Document type!");
    })
};
export const deleteDocumentType = (payload) => (dispatch) => {
    dispatch({
        api: app.deleteDocumentType,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        // dispatch(getDocumentTypeList())
        dispatch(getDocumentTypePage({page: 0, size: 10}))
        dispatch({
            type: types.REQUEST_SUCCESS
        })
        toast.success("Document type deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting Document type!");
    })
};
//FINISH documentType FUNCTION

//----------------------------------------------------------------------------------------//

// START ZipCodes FUNCTION
export const getZipCodesList = () => (dispatch) => {
    dispatch({
        api: app.getZipCodes,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_ZIP_CODE_LIST_SUCCESS,
            types.REQUEST_ERROR
        ]
    })

};
export const saveZipCode = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editZipCode : app.addZipCode,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getZipCodesList())
            toast.success("ZipCodes saved successfully!");
        } else {
            toast.error("You cannot save ZipCodes!")
        }
    }).catch(err => {
        toast.error("Error saving ZipCodes!");
    })
};
export const deleteZipCode = (payload) => (dispatch) => {
    dispatch({
        api: app.deleteZipCode,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch(getZipCodesList())
        dispatch({
            type: types.REQUEST_SUCCESS
        })
        toast.success("ZipCodes deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting zipcode!");
    })
};
// FINISH ZipCodes FUNCTION


// START TIME_DURATION FUNCTION
export const getTimeDuration = () => (dispatch) => {
    dispatch({
        api: app.getTimeDuration,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_TIME_DURATION_SUCCESS,
            types.REQUEST_ERROR
        ]
    })

};
export const saveTimeDuration = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editTimeDuration : app.addTimeDuration,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getTimeDuration())
            toast.success("Time Duration saved successfully!");
        } else {
            toast.error("You cannot save Time Duration!")
        }
    }).catch(err => {
        toast.error("Error saving Time Duration!");
    })
};
export const deleteTimeDuration = (payload) => (dispatch) => {
    dispatch({
        api: app.deleteTimeDuration,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch(getTimeDuration())
        dispatch({
            type: types.REQUEST_SUCCESS
        })
        toast.success("TimeDuration deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting TimeDuration!");
    })
};

// FINISH TIME_DURATION FUNCTION

// START TIME_BOOKED FUNCTION
export const getTimeBooked = () => (dispatch) => {
    dispatch({
        api: app.getTimeBooked,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_TIME_BOOKED_SUCCESS,
            types.REQUEST_ERROR
        ]
    })

};
export const saveTimeBooked = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editTimeBooked : app.addTimeBooked,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getTimeBooked())
            toast.success("Time Booked saved successfully!");
        } else {
            toast.error("You cannot save Time Booked!")
        }
    }).catch(err => {
        toast.error("Error saving Time Booked!");
    })
};
export const deleteTimeBooked = (payload) => (dispatch) => {
    dispatch({
        api: app.deleteTimeBooked,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch(getTimeBooked())
        dispatch({
            type: types.REQUEST_SUCCESS
        })
        toast.success("TimeBooked deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting TimeBooked!");
    })
};

// FINISH TIME_BOOKED FUNCTION

// START DISCOUNT_PERCENT FUNCTION
export const getDiscountPercentList = () => (dispatch) => {
    dispatch({
        api: app.getDiscountPercent,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_DISCOUNT_PERCENT_LIST_SUCCESS,
            types.REQUEST_ERROR
        ]
    })

};
export const getDiscountPercentPage = (data) => (dispatch) => {
    dispatch({
        api: app.getDiscountPercentPage,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_DISCOUNT_PERCENT_PAGE_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    })


};
export const saveDiscountPercent = (data) => (dispatch) => {
    dispatch({
        api: data.id ? app.editDiscountPercent : app.addDiscountPercent,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    }).then(res => {
        if (res.success) {
            dispatch(getDiscountPercentList())
            dispatch(getDiscountPercentPage({page: 0, size: 10}))
            toast.success("DiscountPercent saved successfully!");
        } else {
            toast.error("You cannot save DiscountPercent!")
        }
    }).catch(err => {
        toast.error("Error saving DiscountPercent!");
    })
};
export const deleteDiscountPercent = (payload) => (dispatch) => {
    dispatch({
        api: app.deleteDiscountPercent,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch(getDiscountPercentList())
        dispatch(getDiscountPercentPage({page: 0, size: 10}))
        dispatch({
            type: types.REQUEST_SUCCESS
        })
        toast.success("Discount percent deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting Discount percent!");
    })
};
export const getAgentsByDiscount = (payload) => (dispatch) => {
    dispatch({
        api: app.getAgentDiscount,
        types: [
            "",
            types.REQUEST_GET_AGENT_BY_DISCOUNT,
            ''
        ],
        data: payload
    })
};
export const resetDiscount = (payload) => async (dispatch) => {
    return await dispatch({
        api: app.resetAgentDiscount,
        types: [
            "",
            types.REQUEST_SUCCESS,
            ''
        ],
        data: payload
    })
};
// FINISH DISCOUNT_PERCENT FUNCTION

// START WEEK_DAY FUNCTION
export const getWeekDayList = () => (dispatch) => {
    dispatch({
        api: app.getWeekDay,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_WEEK_DAY_LIST_SUCCESS,
            types.REQUEST_ERROR
        ]
    })

};
export const saveWeekDay = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editWeekDay : app.addWeekDay,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getWeekDayList())
            toast.success("Week day saved successfully!");
        } else {
            toast.error("You cannot save Week day!")
        }
    }).catch(err => {
        toast.error("Error saving Week day!");
    })
};
// FINISH WEEK_DAY FUNCTION

// START AGENT_SCHEDULE FUNCTION
export const getAgentScheduleList = () => (dispatch) => {
    dispatch({
        api: app.getAgentSchedule,
        types: [
            types.REQUEST_START,
            '',
            types.REQUEST_ERROR
        ],
    }).then(res => {
        let array = [];
        let today = '';
        let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
        if (!!res.payload[0]) {
            res.payload.map(v => {
                if (v.weekDay.day.toUpperCase() === days[new Date().getDay()].toUpperCase()) {
                    today = v;
                } else {
                    array.push(v)
                }
            })
        }
        dispatch({
            type: 'updateState',
            payload: {
                agentSchedules: array,
                today: today
            }
        })
    })

};
export const saveAgentSchedule = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editAgentSchedule : app.addAgentSchedule,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getAgentScheduleList(payload.currentUser.id))
            toast.success("AgentScghedule saved successfully!");
        } else {
            toast.error("You cannot save AgentScghedule!")
        }
    }).catch(err => {
        toast.error("Error saving AgentScghedule!");
    })
};
export const changeScheduleDayOff = (payload) => (dispatch) => {
    dispatch({
        api: app.changeScheduleDayOff,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getAgentScheduleList(payload.currentUser.id))
            toast.success("Changed successfully!");
        } else {
            toast.error("You can't change day OFF !")
        }
    })
};
export const changeScheduleHourOff = (payload) => (dispatch) => {
    dispatch({
        api: app.changeScheduleHourOff,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getDaylySchedule(payload.date))
            toast.success("Changed successfully !");
        } else {
            toast.error("You can't change hour off !")
        }
    }).catch(err => {
        toast.error("Error! This time is booked")
    })
};
export const getDaylySchedule = (payload) => (dispatch) => {
    dispatch({
        api: app.getDaylySchedules,
        types: [
            types.REQUEST_START,
            '',
            types.REQUEST_ERROR],
        data: payload
    }).then(res => {
        dispatch({
            type: 'updateState',
            payload: {
                daylySchedules: res.payload,
                showDaylyModal: true
            }
        })
    })
}
export const deleteAgentSchedule = (payload) => (dispatch) => {
    dispatch({
        api: app.deleteAgentSchedule,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch(getAgentScheduleList(payload.currentUser.id))
        dispatch({
            type: types.REQUEST_SUCCESS
        })
        if (res.success)
            toast.success("Successfully!");
    }).catch(err => {
        dispatch(getAgentScheduleList(payload.currentUser.id))
    })
};
// FINISH AGENT_SCHEDULE FUNCTION

//Start History function
export const getHistory = (data) => (dispatch) => {
    dispatch({
        api: app.getHistory,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_HISTORY_LIST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    })
};
export const getHistoryTables = () => (dispatch) => {
    dispatch({
        api: app.getHistoryTables,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_HISTORY_TABLE_NAME_SUCCESS,
            types.REQUEST_ERROR
        ]
    })
};
export const getAuditItemOneTable = (data) => (dispatch) => {
    dispatch({
        api: app.getAuditItemForTable,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_AUDIT_ONE_TABLE,
            types.REQUEST_ERROR
        ],
        data: data
    })
};
export const addFeedBackAction = (payload) => (dispatch) => {
    dispatch({
        api: app.addFeedBack,
        types: [types.REQUEST_START, types.REQUEST_SUCCESS, types.REQUEST_ERROR],
        data: payload
    }).then(res => {
        if (res && res.payload && res.payload.success) {
            dispatch(getAllByUserId({
                userId: "1b118d3a-a84c-46e5-a601-a98b2e91b8d1",
                param: {page: 0, size: 10}
            }))
            toast.success(res.payload.message);
            dispatch({type: "updateState", payload: {showFeedBackModal: false, currentItem: null, loading: false}})
        } else {
            toast.error(res.payload.message)
        }
    })
}
export const editFeedBackAction = (payload) => (dispatch) => {
    dispatch({
        api: app.editFeedBack,
        types: [types.REQUEST_START, "", types.REQUEST_ERROR],
        data: payload
    }).then(res => {
        if (res.payload.success) {
            toast.success(res.payload.message);
            dispatch({type: "updateState", payload: {showFeedBackModal: false, currentItem: null, loading: false}})
            dispatch(getFeedBackByUserId(payload.userId))
        } else {
            toast.error(res.payload.message)
        }
    })
}
export const addAnswer = (payload, bool) => (dispatch) => {
    dispatch({
        api: app.addAnswer,
        types: [types.REQUEST_START, "", types.REQUEST_ERROR],
        data: payload
    }).then(res => {
        if (res.payload.success) {
            // dispatch(getFeedBacksAction({page: 0, size: 20, isAgent: true}))
            dispatch(getFeedBacksAction({page: 0, size: 20, isAgent: bool}))
            dispatch({type: "updateState", payload: {showModal: false, currentItem: null, loading: false}})
            toast.success(res.payload.message);
        } else {
            toast.error(res.payload.message)
        }
    })
}

export const getFeedBacksAction = (payload) => (dispatch) => {
    dispatch({
        api: app.getFeedBacks,
        types: [types.REQUEST_START, "", types.REQUEST_ERROR],
        data: payload
    }).then(res => {
        let feedBacks = {};
        feedBacks.agents = [];
        feedBacks.clients = [];
        res.payload.object.map((feed) => {
            if (feed.agent) {
                feedBacks.agents.push(feed);
                feedBacks.clients = ""
            } else {
                feedBacks.clients.push(feed);
                feedBacks.agents = ""
            }

        })
        dispatch({
            type: "updateState",
            payload: {
                feedBacks: feedBacks,
                loading: false,
                page: res.payload.page,
                size: res.payload.size,
                totalElements: res.payload.totalElements,
                totalPages: res.payload.totalPages
            }
        })
    })
}
export const getFeedBackByUserId = (payload) => async (dispatch) => {
    await dispatch({
        api: app.getFeedBackByUser,
        types: [types.REQUEST_START, "", types.REQUEST_ERROR],
        data: payload
    }).then((res) => {
        dispatch({
            type: 'updateState', payload: {
                feedBacks: res.payload.object,
                loading: false,
                page: res.payload.page,
                size: res.payload.size,
                totalElements: res.payload.totalElements,
                totalPages: res.payload.totalPages
            }
        })
    })
}
export const getFeedBackByMaxRate = (payload) => async (dispatch) => {
    await dispatch({
        api: app.getFeedBackMaxRate,
        types: [types.REQUEST_START, "", types.REQUEST_ERROR],
        data: payload
    }).then((res) => {
        dispatch({
            type: 'updateState', payload: {
                feedBacks: res.payload.object,
                loading: false,
                page: res.payload.page,
                size: res.payload.size,
                totalElements: res.payload.totalElements,
                totalPages: res.payload.totalPages
            }
        })
    })
}
export const saveDiscountAction = (payload) => (dispatch) => {
    dispatch({
        api: app.addDiscount,
        types: [types.REQUEST_START, "", types.REQUEST_ERROR],
        data: payload
    }).then(res => {
        if (res.payload.success) toast.success(res.payload.message)
        else toast.error(res.payload.message)
        dispatch({type: 'updateState', payload: {showModal: false, currentItem: null, loading: false}})
        window.location.reload()

    })
}
export const editDiscountAction = (payload) => (dispatch) => {
    dispatch({
        api: app.editDiscount,
        types: [types.REQUEST_START, "", types.REQUEST_ERROR],
        data: payload
    }).then(res => {
        if (res.payload.success) toast.success(res.payload.message)
        else toast.error(res.payload.message)
        dispatch({type: 'updateState', payload: {showModal: false, currentItem: null, loading: false}})
        window.location.reload();
    })
}
export const getDiscountList = (payload) => (dispatch) => {
    dispatch({
            api: app.getDiscountList,
            types: [types.REQUEST_START, '', types.REQUEST_ERROR],
            data: payload
        }
    ).then(res => {
        dispatch({type: 'updateState', payload: {discounts: res.payload.object, loading: false}})
    })
}
export const getDiscountByUser = (payload) => (dispatch) => {
    dispatch({
            api: app.getDiscountList,
            types: [types.REQUEST_START, '', types.REQUEST_ERROR],
            data: payload
        }
    ).then(res => {
        dispatch({type: 'updateState', payload: {discounts: res.payload.object, loading: false}})
    })
}
export const getZipCode = (payload) => (dispatch) => {
    dispatch({
            api: app.getZipCode,
            types: [types.REQUEST_START, '', types.REQUEST_ERROR],
            data: payload
        }
    ).then(res => {
        dispatch({type: 'updateState', payload: {currentItem: res.payload, loading: false}})
    })
}
export const getUsersByZipCode = (payload) => (dispatch) => {
    dispatch({
            api: app.getUsersByZipCode,
            types: [types.REQUEST_START, '', types.REQUEST_ERROR],
            data: payload
        }
    ).then(res => {
        dispatch({type: 'updateState', payload: {agents: res.payload, loading: false}});
    })
}
export const addOrEditUserZipCode = (payload) => (dispatch) => {
    dispatch({
            api: app.addOrEditUserZipCode,
            types: [types.REQUEST_START, types.REQUEST_SUCCESS, types.REQUEST_ERROR],
            data: payload
        }
    ).then(res => {
        if (res.payload.success) {
            toast.success(res.payload.message);
            dispatch(getUsersByZipCode(payload.zipCodesId[0]))
        }
        dispatch({type: 'updateState', payload: {showModal: false}})
    })
}
export const getEmbassy = (payload) => (dispatch) => {
    dispatch({
            api: app.getEmbassy,
            types: ['', '', ""]
        }
    ).then(res => {
        console.log(res);
        dispatch({
            type: 'updateState', payload: {
                embassyCountries: res && res.payload && res.payload._embedded ? res.payload._embedded.list : "",
                loading: false
            }
        });
    })
}

export const editProfile = (payload) => (dispatch) => {
    dispatch({
            api: app.editProfile,
            data: payload,
            types: [types.REQUEST_START, '', types.REQUEST_ERROR]
        }
    ).then(res => {
        alert(res.payload.message)
    }).catch(er => {
    })
}


//End History function
//start Blog function

export const getBlogs = () => async (dispatch) => {
    await dispatch({
        api: app.getBlog,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_BLOG_SUCCESS,
            types.REQUEST_ERROR
        ]
    })
};
export const getBlogsUrl = (data) => (dispatch) => {
    dispatch({
        api: app.getBlogUrl,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_BLOG_URL_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    })
};
export const getBlogByCategory = (data) => async (dispatch) => {
    await dispatch({
        api: app.getBlogByCategory,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_BLOG_BY_CATEGORY_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    })
};
export const getBlogList = (data) => (dispatch) => {
    dispatch({
        api: app.getBlogAll,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_BLOG_LIST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    })
};
export const getBlogListFeatured = (data) => (dispatch) => {
    dispatch({
        api: app.getBlogFeatured,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_BLOG_FEATURED_LIST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    })
};
export const saveBlog = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editBlog : app.addBlog,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getBlogList({page: 0, size: 10}))
            dispatch(getDynamicList({page: 0, size: 10}))
            // dispatch(getBlogs())
            toast.success("Blog saved successfully!");
        } else {
            toast.error("You cannot save Blog!")
        }
    }).catch(err => {
        toast.error("Error saving Blog!");
    })
};
export const deleteBlog = (payload) => (dispatch) => {
    dispatch({
        api: app.deleteBlog,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                blogs: null
            }
        })
        dispatch(getBlogList({page: 0, size: 10}))
        dispatch(getDynamicList({page: 0, size: 10}))

        dispatch({
            type: types.REQUEST_SUCCESS
        })
        toast.success("Blog deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting Blog!");
    })
};


export const getDynamicList = (data) => (dispatch) => {
    dispatch({
        api: app.getDynamic,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_DYNAMIC_LIST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    })
};
export const saveDynamic = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editDynamic : app.addDynamic,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getDynamicList({page: 0, size: 10}))
            toast.success("Dynamic saved successfully!");
        } else {
            toast.error("You cannot save Dynamic!")
        }
    }).catch(err => {
        toast.error("Error saving Dynamic!");
    })
};
//finish Blog function
//start Blog function

export const getCategorys = () => async (dispatch) => {
    await dispatch({
        api: app.getCategory,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_CATEGORY_SUCCESS,
            types.REQUEST_ERROR
        ]
    })
};
export const saveCategory = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editCategory : app.addCategory,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getCategorys())

            toast.success("Category saved successfully!");
        } else {
            toast.error("You cannot save Category!")
        }
    }).catch(err => {
        toast.error("Error saving Category!");
    })
};
export const deleteCategory = (payload) => (dispatch) => {
    dispatch({
        api: app.deleteCategory,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch({
            type: "updateState",
            payload: {
                blogs: null
            }
        })
        dispatch(getCategorys())
        dispatch({
            type: types.REQUEST_SUCCESS
        })
        toast.success("Category deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting Category!");
    })
};

//finish Blog function/
// /start Terms function

export const getTerms = (data) => async (dispatch) => {
    await dispatch({
        api: app.getTerms,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_TERMS_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    })
};
export const saveTerms = (payload) => (dispatch) => {
    dispatch({
        api: app.editTerms,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getTerms())

            toast.success("Terms saved successfully!");
        } else {
            toast.error("You cannot save Terms!")
        }
    }).catch(err => {
        toast.error("Error saving Terms!");
    })
};


//finish Terms function


//Start PAYMENT for admin
export const getPayments = (payload) => (dispatch) => {
    dispatch({
        api: app.getPayment,
        types: [
            typesAdm.REQUEST_GET_ADMIN_BY_PAYMENT_START,
            typesAdm.REQUEST_GET_ADMIN_BY_PAYMENT_SUCCESS,
            typesAdm.REQUEST_GET_ADMIN_BY_PAYMENT_ERROR
        ],
        data: payload
    })
}
export const savePayment = (data) => (dispatch) => {
    dispatch({
        api: app.savePayment,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    }).then(res => {
        if (res.success) {
            dispatch(getPayments({page: 0, size: 20}))
            toast.success("Payment saved successfully!");
        } else {
            toast.error("You cannot save Payment!")
        }
    }).catch(err => {
        toast.error("Error saving Payment!");
    })
}
export const deletePayment = (data) => (dispatch) => {
    dispatch({
        api: app.deletePayment,
        types: [
            typesAdm.REQUEST_DELETE_PAYMENT_START,
            typesAdm.REQUEST_DELETE_PAYMENT_SUCCESS,
            typesAdm.REQUEST_DELETE_PAYMENT_ERROR
        ],
        data: data
    }).then(res => {
        if (res.success) {
            dispatch(getPayments({page: 0, size: 20}))

            toast.success("Payment deleted successfully!");
        }
    })
}

export const getOrderBySearch = (payload) => (dispatch) => {
    dispatch({
            api: app.getorderBySearch,
            types: [
                typesAdm.REQUEST_SEARCH_ORDER_FOR_PAYMENT_START,
                typesAdm.REQUEST_SEARCH_ORDER_FOR_PAYMENT_SUCCESS,
                typesAdm.REQUEST_SEARCH_ORDER_FOR_PAYMENT_ERROR
            ],
            data: payload
        }
    )
}
export const addUserZipcodeAction = (data) => (dispatch) => {
    dispatch({type: "updateState", payload: {userZipCodes: '', zipCodes: ''}})
    dispatch({
        api: app.addUserZipcode,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    }).then(res => {
        if (res.success) {
            dispatch(getUserZipCodeByUserAction())
            dispatch(getZipCodeByUser())

            toast.success("Zip code added");
        } else {
            toast.error("You cannot save Payment!")
        }
    }).catch(err => {
        toast.error("Error saving Payment!");
    })
}
export const addUserZipcodeActionForAdmin = (data) => (dispatch) => {
    dispatch({type: "updateState", payload: {userZipCodes: '', zipCodes: ''}})
    dispatch({
        api: app.addUserZipcodeForAdmin,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    }).then(res => {
        if (res.success) {
            dispatch(getUserZipCodeByUserForAdminAction(data.userId))
            dispatch(getZipCodeByUserForAdmin(data.userId))

            toast.success("Zip code added");
        } else {
            toast.error("You cannot save Payment!")
        }
    }).catch(err => {
        toast.error("Error saving Payment!");
    })
}
export const changeEnableUserZipcodeAction = (data) => (dispatch) => {
    dispatch({
        api: app.changeEnableUserZipcode,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    }).then(res => {
        if (res.success) {
            // dispatch(getUserZipCodeByUserForAdminAction())
            toast.success("Zip code enable changed");
        } else {
            toast.error("You cannot save Payment!")
        }
    }).catch(err => {
        toast.error("Error saving Payment!");
    })
}
export const changeActiveUserZipcodeAction = (data) => (dispatch) => {
    dispatch({
        api: app.changeActiveUserZipcode,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    }).then(res => {
        if (res.success) {
            dispatch(getUserZipCodeByUserAction())
            toast.success("Zip code active changed");
        } else {
            toast.error("You cannot save Payment!")
        }
    }).catch(err => {
        let is=err&&err.response&&err.response.data&&err.response.data.message
        toast.error(is?err.response.data.message:'Error');
        dispatch(getUserZipCodeByUserAction());
    })
}
export const getUserZipCodeByUserAction = () => (dispatch) => {
    dispatch({type: 'updateState', payload: {userZipCodes: ''}})
    dispatch({
            api: app.getUserZipcodesByUser,
            types: [
                "", "", ""
            ]
        }
    ).then(res => {
        dispatch({
            type: 'updateState',
            payload: {userZipCodes: res.payload.sort((a, b) => (a.enable) ? 1 : ((b.enable) ? -1 : 0))}
        })
    })
}

export const getUserZipCodeByUserForAdminAction = (payload) => (dispatch) => {
    dispatch({type: 'updateState', payload: {userZipCodes: ''}})
    dispatch({
            api: app.getUserZipcodesByUserForAdmin,
            types: [
                "", "", ""
            ],
            data: payload
        }
    ).then(res => {
        dispatch({
            type: 'updateState',
            payload: {userZipCodes: res.payload.sort((a, b) => (b.enable) ? 1 : ((a.enable) ? -1 : 0))}
        })
    })
}
export const getZipCodeByUser = () => (dispatch) => {
    dispatch({type: 'updateState', payload: {zipCodes: ''}})
    dispatch({
            api: app.getZipCodeByCertificate,
            types: [
                "", "", ""
            ]
        }
    ).then(res => {
        let obj = {};
        if (res && res.payload && res.payload.length > 0 && res.payload[0]) {
            obj = res.payload.map(item => {
                return {value: item.id, label: item.code}
            });
            dispatch({type: 'updateState', payload: {zipCodes: obj}})
        }
    })
}
export const getZipCodeByUserForAdmin = (id) => (dispatch) => {
    dispatch({type: 'updateState', payload: {zipCodes: ''}})
    dispatch({
            api: app.getZipCodeByCertificateForAdmin,
            types: [
                "", "", ""
            ],
        data:id
        }
    ).then(res => {
        let obj = {};
        if (res && res.payload && res.payload.length > 0 && res.payload[0]) {
            obj = res.payload.map(item => {
                return {value: item.id, label: item.code}
            });
            dispatch({type: 'updateState', payload: {zipCodes: obj}})
        }
    })
}


// Admin Dashboard
export const getTotalNumbersForDashboard = () => {
    return (dispatch) => {
        dispatch({
            api: app.getTotalNumbersForDashboard,
            types: [
                typesAdm.REQUEST_TOTAL_COUNTS_OF_ORDER_AND_USER_START,
                typesAdm.REQUEST_TOTAL_COUNTS_OF_ORDER_AND_USER_SUCCESS,
                typesAdm.REQUEST_TOTAL_COUNTS_OF_ORDER_AND_USER_ERROR
            ]
        })
    };
}
export const getOrdersCash = () => {
    return (dispatch) => {
        dispatch({
            api: app.getOrdersCash,
            types: [
                typesAdm.REQUEST_ORDER_CASH_FOR_DASHBOARD_START,
                typesAdm.REQUEST_ORDER_CASH_FOR_DASHBOARD_SUCCESS,
                typesAdm.REQUEST_ORDER_CASH_FOR_DASHBOARD_ERROR
            ]
        })
    };
}
export const getDashboard = (data) => {
    return (dispatch) => {
        dispatch({
            api: app.getDashboard,
            types: [
                typesAdm.REQUEST_ADMIN_DASHBOARD_START,
                typesAdm.REQUEST_ADMIN_DASHBOARD_SUCCESS,
                typesAdm.REQUEST_ADMIN_DASHBOARD_ERROR
            ],
            data: data
        })
    };
}

// Review home page

export const getReviews = (payload) => (dispatch) => {
    dispatch({
        api: app.getReviews,
        types: [
            types.REQUEST_GET_REVIEW_START,
            types.REQUEST_GET_REVIEW_SUCCESS,
            types.REQUEST_GET_REVIEW_ERROR
        ],
        data: payload
    })
}
export const deleteReview = (data) => {
    return (dispatch) => {
        dispatch({
            api: app.deleteReview,
            types: [
                "",
                types.REQUEST_DELETE_REVIEW_SUCCESS,
                ""
            ],
            data: data
        }).then(res => {
            dispatch({type: 'updateState', payload: {showDeleteModal: '', currentItem: ''}})
            dispatch(getReviews())
            toast.success("Deleted");
        })
    }
}
export const saveReview = (data) => {
    return (dispatch) => {
        dispatch({
            api: data.id ? app.editReview : app.addReview,
            types: [
                "",
                types.REQUEST_SAVE_REVIEW_SUCCESS,
                ""
            ],
            data: data
        }).then(res => {
            dispatch({type: 'updateState', payload: {photo: '', workicon: '', reviewModal: '', currentItem: ''}})
            dispatch(getReviews())
            toast.success("Saved");
        })
    }
}

//FAQ

export const getFaq = () => (dispatch) => {
    dispatch({
        api: app.getFaq,
        types: [
            types.REQUEST_GET_FAQ_START,
            types.REQUEST_GET_FAQ_SUCCESS,
            types.REQUEST_GET_FAQ_ERROR
        ],
    })
}
export const deleteFaq = (data) => {
    return (dispatch) => {
        dispatch({
            api: app.deleteFaq,
            types: [
                "",
                types.REQUEST_DELETE_FAQ_SUCCESS,
                ""
            ],
            data: data
        }).then(res => {
            dispatch(getFaq("AGENT"))
            dispatch(getFaq("CLIENT"))
            toast.success("Deleted");
        })
    }
}
export const saveFaq = (data) => {
    return (dispatch) => {
        dispatch({
            api: data.id ? app.editFaq : app.addFaq,
            types: [
                "",
                types.REQUEST_SAVE_FAQ_SUCCESS,
                ""
            ],
            data: data
        }).then(res => {
            dispatch(getFaq("AGENT"))
            dispatch(getFaq("CLIENT"))
            toast.success("Saved");
        })
    }
}