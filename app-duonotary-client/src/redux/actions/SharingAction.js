import * as app2 from "../../api/AppApi";
import * as types from "../actionTypes/SharingActionTypes"


export const getShareUserQrCode = () => (dispatch) => {
    dispatch({
        api: app2.getQrCodeForClient,
        types: [
            types.REQUEST_GET_USER_QR_CODE_START,
            types.REQUEST_GET_USER_QR_CODE_SUCCESS,
            types.REQUEST_GET_USER_QR_CODE_ERROR
        ]
    })
}
export const getUserSharingDiscount = () => (dispatch) => {
    dispatch({
        api: app2.getUserSharingDiscount,
        types: [
            types.REQUEST_GET_USER_SHARING_DISCOUNT_START,
            types.REQUEST_GET_USER_SHARING_DISCOUNT_SUCCESS,
            types.REQUEST_GET_USER_SHARING_DISCOUNT_ERROR
        ]
    })
}