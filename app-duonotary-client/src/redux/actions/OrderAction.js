//Start Orders function
import * as order from "../../api/OrderApi";
import * as types from "../actionTypes/OrderActionTypes";
import {CHANGE_ORDER, SHOW_MODAL} from "../actionTypes/OrderActionTypes";
import {toast} from "react-toastify";
import {uploadDoc} from "./AttachmentAction";
import {NEW_USER_FOR_ORDER} from "../../utils/constants";

export let getZipcodeByCode = (code) => (dispatch) => {
    dispatch({
        api: order.getZipCodeByCode,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_ZIP_CODE_BY_CODE,
            types.REQUEST_ERROR
        ],
        data: {code: code}
    })
};
export let getAddressInfo = (payload) => (dispatch) => {
    dispatch({
        api: order.getAddressInfoZipCode,
        types: [
            types.REQUEST_START,
            '',
            types.REQUEST_ERROR
        ],
        data: {code: payload.code}
    }).then(res => {
        if (res.success) {
            if (res.payload.success) {
                if (res.payload.object && res.payload.object.servicePrice && res.payload.object.servicePrice[0]) {
                    dispatch({
                        type: "updateStateOrder",
                        payload: {
                            servicePrices: res.payload.object.servicePrice,
                            zipCode: res.payload.object.zipCode,
                            stage: 2
                        }
                    })
                }else{
                    dispatch({
                        type: 'updateStateOrder',
                        payload: {outOfService: true}
                    })
                }
            } else {
                dispatch({
                    type: 'updateStateOrder',
                    payload: {outOfService: true}
                })
            }
        }
    }).catch(()=>toast.error("Sorry, something error"))
};
export let getServicePrices = (zipCodeId) => (dispatch) => {
    dispatch({
        api: order.getServicePriceByZipCode,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_SERVICE_PRICE_ZIP_CODE_ID,
            types.REQUEST_ERROR
        ],
        data: {zipCodeId: zipCodeId}
    })
};
export let getAdditionalServicePrices = (servicePriceId) => (dispatch) => {
    dispatch({
        api: order.getAdditionalServicePriceList,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_ADDITIONAL_SERVICE_PRICES,
            types.REQUEST_ERROR
        ],
        data: servicePriceId
    })
};

export let getOrderList = (payload) => (dispatch) => {
    dispatch({
        api: order.getOrderList,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_ORDER_LIST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch({
            type: 'updateState',
            payload: {
                page: res.payload.page,
                size: res.payload.size,
                totalElements: res.payload.totalElements,
                totalPages: res.payload.totalPages,
                loading: false
            }
        })
    })
};
export let getOrderListByStatus = (payload) => (dispatch) => {
    dispatch({
        api: order.getOrderListByStatus,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_ORDER_LIST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch({
            type: 'updateState',
            payload: {
                page: res.payload.page,
                size: res.payload.size,
                totalElements: res.payload.totalElements,
                totalPages: res.payload.totalPages,
                loading: false
            }
        })
    })
};
export let getForCustomer = (payload) => (dispatch) => {
    dispatch({
        api: order.getOrderWithCustomer,
        types: [
            types.REQUEST_START,
            "",
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        let result = []
        res.payload.map(item => {
            let customer = JSON.parse(item)
            result.push(customer)
        })
        dispatch({
            type: 'updateState',
            payload: {
                customers: result,
                loading: false,
                totalElements: (result && result[0] ? result[0].totalElements : 0)
            }
        })
    })
};

export let getTimesByDate = (data) => (dispatch) => {
    dispatch({
        api: order.getTimesByDate,
        types: [
            types.REQUEST_START,
            '',
            types.REQUEST_ERROR
        ],
        data: data
    }).then(res => {
            let times = [];
            let timesPercent = [];
            if (res.payload.hourDtoList) {
                res.payload.hourDtoList.map(v => {
                    if (v.percent === 0) {
                        times.push(v);
                    } else {
                        timesPercent.push(v);
                    }
                })
            }
            if (res.success) {
                dispatch({
                    type: types.REQUEST_GET_TIMES_BY_DATE,
                    payload: {
                        times,
                        timesPercent,
                        date: res.payload.date ? res.payload.date.substring(0, 10) : null
                    }
                })
            }
        }
    )
};
export let postTimeTable = (data) => (dispatch) => {
    dispatch({
        api: order.postTimeTable,
        types: [
            types.REQUEST_START,
            types.REQUEST_POST_TIME_TABLE,
            types.REQUEST_ERROR
        ],
        data: data
    }).then(res => {
        if (res.payload.success) {
            if (data.online)
                dispatch({
                    type: 'updateStateOrder',
                    payload: {
                        step: data.step,
                        timeTable: res.payload.object
                    }
                })
            else
                dispatch({
                    type: 'updateStateOrder',
                    payload: {
                        stage: 4,
                        timeTable: res.payload.object
                    }
                })
        } else {
            if (!data.error.error) {
                getTimesByDate(data.error)
                dispatch({
                    type: types.SET_ERROR,
                    payload: true
                })
                toast.warning("Sorry this time was taken by another user a minute ago")
            }
        }
    })
};

export let getPayTypes = () => (dispatch) => {
    dispatch({
        api: order.getPayType,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_PAY_TYPE,
            types.REQUEST_ERROR
        ]
    })
};

export let getPricingByServicePrice = (data) => (dispatch) => {
    dispatch({
        api: order.getPricingByServicePrice,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_PRICING_BY_SERVICE_PRICE,
            types.REQUEST_ERROR
        ],
        data: {servicePriceId: data}
    })
};

export let getOrderPriceAmounts = (data) => (dispatch) => {
    dispatch({
        type: "updateStateOrder",
        payload: {
            allDiscounts: []
        }
    })
    dispatch({
        api: order.getAmounts,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_ORDER_PRICE_AMOUNTS,
            types.REQUEST_ERROR
        ],
        data: data
    }).then(res => {
        if (res && res.payload && res.payload.length > 0 && res.payload[0].allAmount) {
        } else {
            dispatch({
                api: order.getAmount,
                types: [
                    types.REQUEST_START,
                    types.REQUEST_GET_ORDER_PRICE_AMOUNT,
                    types.REQUEST_ERROR
                ],
                data: data
            }).then(res => {
                console.log(res);
                if (res && res.success)
                    dispatch({
                        type: 'updateStateOrder',
                        payload: {
                            amount: res.payload
                        }

                    })
            })
        }
    })
};
export let getOrderAmount = (data) => (dispatch) => {
    dispatch({
        api: order.getAmount,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_ORDER_PRICE_AMOUNT,
            types.REQUEST_ERROR
        ],
        data: data
    })
};
export let deleteOrderDocument = (data) => (dispatch, getState) => {
    let {attachment: {attachmentIdsArray}, order: {currentOrder}, app: {page, size}} = getState();
    dispatch({
        api: order.deleteDocumentOrder,
        types: [
            "",
            "",
            ""
        ],
        data: data
    }).then((res) => {
        // dispatch(getOrderList({page: page, size: size}))
        dispatch({type: 'updateState', payload: {documents: res.payload.object.documents}})
    })
};
export let addOrderDocument = (data) => (dispatch, getState) => {
    dispatch(uploadDoc(data, "doc")).then(() => {
        let {attachment: {attachmentIdsArray}, order: {currentOrder}, app: {page, size, currentItem}} = getState();
        let attach = attachmentIdsArray.filter(item => item.key === "doc")[0].value;
        dispatch({
            api: order.addDocumentOrder,
            types: [
                '',
                "",
                ''
            ],
            data: {order: currentItem.id, attach: attach}
        }).then((res) => {
            // dispatch(getOrderList({page: page, size: size}))
            dispatch({type: 'updateState', payload: {documents: res.payload.object.documents}})
        })
    })
};

export let addPayment = (data) => (dispatch) => {
    dispatch({
        api: order.pay,
        types: [
            types.REQUEST_START,
            types.REQUEST_POST_PAYMENT,
            types.REQUEST_ERROR
        ],
        data: data
    })
}

export let addOrder = (data) => (dispatch) => {
    dispatch({
        api: order.saveOrder,
        types: [
            types.REQUEST_START,
            types.REQUEST_POST_ORDER,
            types.REQUEST_ERROR
        ],
        data: data
    }).then(res => {
        if (res.success) {
            if (data.admin)
                dispatch(getOrderList())
            dispatch({
                type: types.CHANGE_STEP,
                payload: data.step + 1
            })
            dispatch({
                type: types.REQ_ALERT,
                payload: (data.id ? "Order successfully edited" : "Your order successfully registered.")
            })
            dispatch({type:'updateState',payload:{
                attachmentIdsArray:''
                }})
        } else {
            toast.error(res.message)
        }
        localStorage.removeItem(NEW_USER_FOR_ORDER);
    }).catch(err => {
        localStorage.removeItem(NEW_USER_FOR_ORDER);
        toast.error(err.message)
    })
}

export let editOrder = (payload) => (dispatch) => {
    dispatch({
        api: order.editOrder,
        types: [
            types.REQUEST_START,
            "",
            types.REQUEST_ERROR
        ],
        data: payload.order
    }).then(res => {
        if (payload.discountType && payload.stage === 4) {
            dispatch({
                type: "updateState",
                payload: {
                    stage: 5
                }
            })
        } else if (res.success) {
            dispatch({
                type: "updateStateBack",
                payload: payload.state
            })
            dispatch(getAllByUserId({page: 0, size: 20}))
        }
        toast.success("Successfully edited!")
    })
}

export let getServicePricesOnline = () => (dispatch) => {
    dispatch({
        api: order.getServicePricesForOnline,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_SERVICE_PRICE_ONLINE,
            types.REQUEST_ERROR
        ],
    }).catch(() => {
        dispatch({
            type: types.REQUEST_ERROR_SERVICE_PRICE,
            payload: "Sorry server not working"
        })
    })
}

export const getAllByUserId = (payload) => (dispatch) => {
    dispatch({type: 'updateState', payload: {orders: ''}})
    dispatch({
        api: order.geAllUserId,
        types: ['', '', ''],
        data: payload
    }).then(res => {
        if (res.success) {
            let orders = res.payload.object;
            let monthes = {
                '01': 'Jun',
                '02': "Feb",
                '03': "Mar",
                '04': "Apr",
                '05': "May",
                '06': "Jun",
                '07': "Jul",
                '08': "Aug",
                '09': "Sep",
                '10': "Oct",
                '11': "Nov",
                '12': "Dec"
            }
            orders.map((order) => {
                let date = new Date(order.timeTableDto.fromTime);
                order.timeTableDto.day = String(date.getDate()).padStart(2, '0');
                order.timeTableDto.month = monthes[String(date.getMonth() + 1).padStart(2, '0')];
                order.timeTableDto.year = date.getFullYear()
                order.timeTableDto.hour = date.getHours()
                order.timeTableDto.min = date.getMinutes().toString().length < 2 ? '0' + date.getMinutes() : date.getMinutes();
            })
            dispatch({
                type: "updateState",
                payload: {
                    orders: orders,
                    page: res.payload.page,
                    size: res.payload.size,
                    totalElements: res.payload.totalElements,
                    totalPages: res.payload.totalPages,
                    loading: false
                }
            })
        } else window.location.reload();
    })
}
export const getCurrentFreeAgent = (payload) => (dispatch) => {
    dispatch({
        api: order.getCurrentAgent,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_CURRENT_AGENT,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.payload.success) {
            dispatch({
                type: 'updateStateOrder',
                payload: {
                    now: true,
                    timeTable: res.payload.object,
                    update: 'now',
                    step:1
                }
            })
        } else {
            toast.warning("Free agent not found but you continue schedule")
            dispatch({
                type: 'updateStateOrder',
                payload: {
                    now: false,
                    main: 'schedule'
                }
            })
        }
    })
}
export const getOrdersForClientMainPage = (payload) => async (dispatch) => {
    await dispatch({
        api: order.getOrdersForClientMainPage,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_ORDERS_FOR_CLIENT_MAIN_PAGE,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}
export const getClientForAdmin = (payload) => async (dispatch) => {
    await dispatch({
        api: order.getClientForAdmin,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_ORDERS_FOR_CLIENT_MAIN_PAGE,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}
export const getOrdersForAdminOrder = (payload) => (dispatch) => {
    dispatch({
        api: order.getOrdersForAdmin,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_ORDERS_FOR_CLIENT_MAIN_PAGE,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(
        dispatch({
            type: SHOW_MODAL,
            payload: true
        })
    )
}
export const getOrdersForAdminOrder2 = (payload) => (dispatch) => {
    dispatch({
        api: order.getOrdersForAdmin2,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_ORDERS_FOR_CLIENT_MAIN_PAGE,
            types.REQUEST_ERROR
        ]
    }).then(
        dispatch({
            type: SHOW_MODAL,
            payload: true
        })
    )
}
export const getAmountForCancelOrder = (payload) => (dispatch) => {
    dispatch({
        api: order.getAmountOfOrderToCancel,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_AMOUNT_OF_CANCEL,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}
export const orderCancel = (payload) => (dispatch) => {
    dispatch({
        api: order.orderCancel,
        types: [
            types.REQUEST_START,
            types.REQUEST_POST_CANCEL_ORDER,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success)
            toast.success("Successfully canceled")
    }).catch(err => {
        if (!err.success)
            toast.error("You can not cancel")
    })
}
export const getOrdersGroupByAgent = (payload) => (dispatch) => {
    dispatch({
        api: order.getOrderForAgent,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_ORDER_FOR_ADMIN,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(
        dispatch({
            type: CHANGE_ORDER,
            payload: payload.online
        })
    )
}
export const deleteOrder = (payload) => (dispatch) => {
    dispatch({
        api: order.deleteOrder,
        types: [
            types.REQUEST_START,
            types.REQUEST_DELETE_ORDER,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success)
            dispatch(getOrderList())
        toast.success("Successfully deleted")
    })
}
export const paymentFuture = (payload) => (dispatch) => {
    dispatch({
        api: order.paymentFuture,
        types: [
            types.REQUEST_START,
            '',
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success)
            toast.success("Successfully payed")
        setTimeout(window.location.reload(), 2000)
    })
}
export const getServiceToNow = () => (dispatch) => {
    dispatch({
        api: order.getServiceNow,
        types: [
            types.REQUEST_START,
            '',
            types.REQUEST_ERROR
        ],
    }).then(res => {
        if (res.payload.success) {
            dispatch({
                type: 'updateStateOrder',
                payload: {servicePrice: res.payload.object}
            })
            console.log(res.payload.object);
            dispatch(getPricingByServicePrice(res.payload.object.id))
        } else
            toast.error("Please add online service to database")
    })
}
export const sentToOutOfService = (payload) => (dispatch) => {
    dispatch({
        api: order.sentOutOfService,
        types: [
            types.REQUEST_START,
            '',
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            toast.success("Sent")
            dispatch({
                type: 'updateState',
                payload: {outOfService: false}
            })
        }
    }).catch(err => {
        toast.error("Sorry, Your already send")
    })
}
export const changePrintDocumentCount = (bool) => (dispatch, getState) => {
    let {order: {countDocument}} = getState();
    if (!bool && countDocument === 1) {
        return;
    }
    dispatch({
        type: "updateState", payload: {
            countDocument: bool ? ++countDocument : --countDocument
        }
    })
}


export const findAllUserByEmail = (payload) => (dispatch) => {
    dispatch({
        api: order.searchUserByEmail,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_USER_LIST_BY_EMAIL,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}
export const findAllAgent = (payload) => (dispatch) => {
    dispatch({
        api: order.searchAgent,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_AGENT_LIST,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}
export const changeCustomerNames = (payload) => (dispatch) => {
    dispatch({
        api: order.changeCustomerNames,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_AGENT_LIST,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            toast.success("Successfully changed")
        }
    })
}
export const changeStatusOrder = (payload) => (dispatch) => {
    dispatch({
        api: order.changeStatusOrder,
        types: [
            types.REQUEST_START,
            '',
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            toast.success("Successfully changed")
            dispatch(getOrdersForAdminOrder2())
        }
    })
}
export const changeStatusToComplete = (payload) => (dispatch) => {
    dispatch({
        api: order.changeStatusToComplete,
        types: [
            types.REQUEST_START,
            '',
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.payload.success) {
            dispatch(getOrdersForAdminOrder(payload.currentUserId));
            getOrdersForAdminOrder2()
            toast.success("Success changed")
        }else{
            toast.error(res.payload.message)
        }
    }).catch(()=>{
        toast.error("Sorry something error")
    })
}
export const paymentNow = (payload) => async (dispatch) => {
    await dispatch({
        api: order.payment,
        types: [
            types.REQUEST_START,
            '',
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch({
                type: 'updateState',
                payload: {
                    clientSecret: res.payload
                }
            })
        }
    })
}
export const paymentSuccess = (payload) => async (dispatch) => {
    await dispatch({
        api: order.paymentSuccess,
        types: [
            types.REQUEST_START,
            '',
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            toast.success("Successfully payed")
        }
    })
}
export const changeAgent = (payload) => async (dispatch) => {
    await dispatch({
        api: order.changeAgent,
        types: [
            types.REQUEST_START,
            '',
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getOrdersForAdminOrder(payload.toAgent))
            toast.info(res.payload.message)
        }
    })
}

export const getOrdersBySearch = (data) => (dispatch, getState) => {
    dispatch({
        api: order.getOrdersBySearch,
        types: [types.REQUEST_START, "", types.REQUEST_ERROR],
        data: data
    }).then(res => {
        let filter = []
        Object.keys(res.data).map(item => {
            if (item==="search" ||item==="zipCode" || item==='date')
                filter.push({key: item, value: res.data[item]})
        })
        dispatch({
            type: 'updateState',
            payload: {
                ordersList: res.payload.object,
                loading: false,
                filters: filter,
                currentOrder: '',
                search: data.search,
                totalElements: res.payload.totalElements,
            }
        })
    })
}


export const getOrdersBySearchForClient = (data) => (dispatch,) => {
    dispatch({
        api: order.getOrdersBySearchForClient,
        types: [types.REQUEST_START, "", types.REQUEST_ERROR],
        data: data
    }).then(res => {
        let filter = []
        Object.keys(res.data).map(item => {
            if (item==="search" ||item==="zipCode" || item==='date')
                filter.push({key: item, value: res.data[item]})
        })
        dispatch({
            type: 'updateState',
            payload: {
                orders: res.payload.object,
                loading: false,
                filters: filter,
                currentOrder: '',
                search: data.search,
                totalElements: res.payload.totalElements,
            }
        })
    })
}

export const getOrdersBySearchForAgent = (data) => (dispatch,) => {
    dispatch({
        api: order.getOrdersBySearchForAgent,
        types: [types.REQUEST_START, "", types.REQUEST_ERROR],
        data: data
    }).then(res => {
        let filter = []
        Object.keys(res.data).map(item => {
            if (item==="search" ||item==="zipCode" || item==='date')
                filter.push({key: item, value: res.data[item]})
        })
        dispatch({
            type: 'updateState',
            payload: {
                orders: res.payload.object,
                loading: false,
                filters: filter,
                currentOrder: '',
                search: data.search,
                totalElements: res.payload.totalElements,
            }
        })
    })
}
export const getOrdersByFilter = (data) => (dispatch) => {
    dispatch({
        api: order.getOrdersByFilter,
        types: [types.REQUEST_START, "", types.REQUEST_ERROR],
        data: data
    }).then(res => {
        let filter = []
        Object.keys(res.data).map(item => {
            filter.push({key: item, value: res.data[item]})
        })
        if (res.success) {
            dispatch({
                type: "updateState",
                payload: {
                    ordersList: res.payload,
                    orders: res.payload,
                    totalElements: res.payload.totalElements,
                    filters: filter
                }
            })
        }
    })
}

export const getCertificateByOrder = (data) => (dispatch) => {
    dispatch({
        api: order.getCertificateByOrder,
        types: [types.REQUEST_START, "", types.REQUEST_ERROR],
        data: data
    }).then(res => {
        dispatch({type: "updateState", payload: {certificate: res.payload}})

    })
}