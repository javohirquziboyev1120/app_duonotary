import * as zipCodeApi from "../../api/ZipCodeApi";
import * as types from "../actionTypes/ZipCodeTypes"


export const getAllZipCodes = (data) => (dispatch) => {
    dispatch({
        api: zipCodeApi.getAllZipCodes,
        types: [
            types.REQUEST_START,
            types.GET_ALL_ZIP_CODES,
            types.REQUEST_ERROR
        ],
        data: data
    })
}

export const getAllCounties = (data) => async (dispatch) => {
    await dispatch({
        api: zipCodeApi.getAllCounties,
        types: [
            types.REQUEST_START,
            types.GET_ALL_COUNTIES,
            types.REQUEST_ERROR
        ],
        data: data
    })
}

export const getAllStatesByPageable = (data) => (dispatch) => {
    dispatch({
        api: zipCodeApi.getAllStatesByPageable,
        types: [
            types.REQUEST_START,
            types.GET_ALL_STATES_BY_PAGEABLE,
            types.REQUEST_ERROR
        ],
        data: data
    })
}
export const allStates = (data) => (dispatch) => {
    dispatch({
        api: zipCodeApi.allStates,
        types: [
            types.REQUEST_START,
            types.GET_ALL_STATES,
            types.REQUEST_ERROR
        ],
        data: data
    })
}
export const allStates2 = (data) => (dispatch) => {
    dispatch({
        api: zipCodeApi.allStates2,
        types: [
            types.REQUEST_START,
            types.GET_ALL_STATES_2,
            types.REQUEST_ERROR
        ],
        data: data
    })
}
export const getTimezoneName = () => (dispatch) => {
    dispatch({
        api: zipCodeApi.getTimezoneName,
        types: [
            types.REQUEST_START,
            types.GET_ALL_TIMEZONE,
            types.REQUEST_ERROR
        ]
    })
}
export const deleteZipCode = (data) => async (dispatch) => {
    await dispatch({
        api: zipCodeApi.deleteZipCode,
        types: [
            types.REQUEST_START,
            types.DELETE_CURRENT_ZIP_CODE,
            types.DELETING_REQUEST_ERROR
        ],
        data: data
    })
}
export const deleteCounty = (data) => async (dispatch) => {
    await dispatch({
        api: zipCodeApi.deleteCounty,
        types: [
            types.REQUEST_START,
            types.DELETE_CURRENT_COUNTY,
            types.DELETING_REQUEST_ERROR
        ],
        data: data
    })
}
export const deleteState = (data) => async (dispatch) => {
    await dispatch({
        api: zipCodeApi.deleteState,
        types: [
            types.REQUEST_START,
            types.DELETE_CURRENT_STATE,
            types.DELETING_REQUEST_ERROR
        ],
        data: data
    })
}
export const deleteAgentByZipCode = (data) => async (dispatch) => {
    await dispatch({
        api: zipCodeApi.deleteAgentByZipCode,
        types: [
            types.REQUEST_START,
            types.DELETE_CURRENT_STATE,
            types.DELETING_REQUEST_ERROR
        ],
        data: data
    })
}
export const saveOrEditState = (data) => async (dispatch) => {
    await dispatch({
        api: zipCodeApi.saveOrEditState,
        types: [
            types.REQUEST_START,
            types.SAVE_OR_EDIT_STATE,
            types.REQUEST_ERROR
        ],
        data: data
    })
}
export const saveOrEditCounty = (data) => async (dispatch) => {
    await dispatch({
        api: zipCodeApi.saveOrEditCounty,
        types: [
            types.REQUEST_START,
            types.SAVE_OR_EDIT_COUNTY,
            types.REQUEST_ERROR
        ],
        data: data
    })
}
export const saveOrEditZipCode = (data) => async (dispatch) => {
    await dispatch({
        api: zipCodeApi.saveOrEditZipCode,
        types: [
            types.REQUEST_START,
            types.SAVE_OR_EDIT_COUNTY,
            types.REQUEST_ERROR
        ],
        data: data
    })
}
export const getCountyBySelectedState = (data) => async (dispatch) => {
    await dispatch({
        api: zipCodeApi.getCountyBySelectedState,
        types: [
            "",
            types.GET_COUNTY_BY_STATE_ID,
            ""
        ],
        data: data
    })
}
export const changeActiveOfZipCode = (data) => async (dispatch) => {
    await dispatch({
        api: zipCodeApi.changeActiveOfZipCode,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    })
}
export const getCurrentZipCode = (data) => async (dispatch) => {
    await dispatch({
        api: zipCodeApi.getCurrentZipCode,
        types: [
            types.REQUEST_START,
            types.GET_CURRENT_ZIP_CODE_2,
            types.REQUEST_ERROR
        ],
        data: data
    })
}
export const getAgentsByZipCode = (data) => async (dispatch) => {
    await dispatch({
        api: zipCodeApi.getAgentsByZipCode,
        types: [
            types.REQUEST_START,
            types.GET_ALL_AGENTS_BY_ZIP_CODE,
            types.REQUEST_ERROR
        ],
        data: data
    })
}
export const addAgentToZipCode = (data) => async (dispatch) => {
    await dispatch({
        api: zipCodeApi.addAgentToZipCode,
        types: [
            types.REQUEST_START,
            types.ADD_AGENT_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: data
    })
}