import {config} from "../../utils/config"
import * as app from "../../api/AppApi";
import {downloadFile} from "../../api/AppApi";
import * as attachTypes from "../actionTypes/AttachmentActionTypes";
import * as appTypes from "../actionTypes/AppActionTypes";
import {toast} from "react-toastify";
import download from "../../utils/download";

export const uploadFile = (payload, key) => async (dispatch) => {
    if (!(payload.type.substring(0, payload.type.indexOf("/")) === "image")) {
        toast.error("File must be img")
        return "";
    }
    let obj = new FormData();
    obj.append("file", payload);
    await dispatch({
        api: app.uploadFile,
        types: [
            "",
            attachTypes.REQUEST_ATTACHMENT_SUCCESS,
            ""
        ],
        data: obj
    }).then(res => {
        dispatch(attachmentArrayPush(key, res.payload))
    })
}

export const uploadDoc = (payload, key) => async (dispatch) => {
    let obj = new FormData();
    obj.append("file", payload);
    await dispatch({
        api: app.uploadFile,
        types: [
            appTypes.REQUEST_START,
            attachTypes.REQUEST_ATTACHMENT_SUCCESS,
            appTypes.REQUEST_ERROR
        ],
        data: obj
    }).then(res => {
        dispatch(attachmentArrayPush(key, res.payload))
        if (res.success)
            dispatch({
                type: 'updateStateOrder',
                payload: {
                    key: key,
                    src: config.BASE_URL + "/attachment/" + res.payload,
                    value: res.payload,
                    name: payload.name
                }
            })
    })
}

export const uploadImg = (payload) => (dispatch) => {
    let obj = new FormData();
    obj.append("file", payload);
    dispatch({
        api: app.uploadFile,
        types: [
            appTypes.REQUEST_START,
            attachTypes.REQUEST_ATTACHMENT_SUCCESS,
            appTypes.REQUEST_ERROR
        ],
        data: obj
    });
}
export const uploadImgReview = (payload) => (dispatch) => {
    let obj = new FormData();
    obj.append("file", payload.item);
    dispatch({
        api: app.uploadFile,
        types: [
            "",
            attachTypes.REQUEST_REVIEW_ATTACHMENT_SUCCESS,
            ""
        ],
        data: obj
    });
}

export const removePhoto = (payload) => (dispatch) => {
    dispatch({
        api: app.removePhoto,
        types: [
            appTypes.REQUEST_START,
            "",
            appTypes.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.payload.success) {
            toast.success(res.payload.message)
        } else toast.error(res.payload.message)
        return res.payload
    })
}

export const addPhoto = (payload, id) => (dispatch, getState) => {
    dispatch(uploadFile(payload, "12")).then(() => {
        let {attachment: {attachmentIdsArray}} = getState();
        let obj = {};
        obj.attach = attachmentIdsArray.filter(item => item.key === "12")[0].value;
        obj.userId = id
        dispatch({
            api: app.addPhoto,
            types: [
                appTypes.REQUEST_START,
                "",
                appTypes.REQUEST_ERROR
            ],
            data: obj
        }).then(res => {
            if (res.payload.success) {
                toast.success(res.payload.message)
                setTimeout(() => window.location.reload(), 500)
            } else toast.error(res.payload.message)
        })
    })


}

export const downloadFileAction = (payload) => (dispatch) => {
    dispatch({
        api: downloadFile,
        types: [appTypes.REQUEST_START, appTypes.REQUEST_SUCCESS, appTypes.REQUEST_ERROR],
        data: payload
    }).then(res => {
       if (res){
           if (res.payload){
               let link = document.createElement("a")
               link.href = config.BASE_URL + "/attachment/" + res.payload
               link.setAttribute("download", "order.pdf")
               document.body.appendChild(link)
               link.click();
           }else toast.error("Something is wrong")
       }else toast.error("Something is wrong")
    })
}

export const attachmentArrayPush = (key, id) => (dispatch) => {
    dispatch({
        type: attachTypes.ATTACHMENT_ARRAY_PUSH,
        payload: {
            key: key,
            src: config.BASE_URL + "/attachment/" + id,
            value: id
        }
    })
}