import * as app from "../../api/AppApi";
import * as agentTypes from "../actionTypes/AgentActionTypes";
import * as attachTypes from "../actionTypes/AttachmentActionTypes";
import * as appTypes from "../actionTypes/AppActionTypes";
import * as types from "../actionTypes/AppActionTypes";
import React from "react";
import {toast} from "react-toastify";

export const pushCertificateArrayForRegister = (payload) => (dispatch) => {
    dispatch({
        type: attachTypes.ATTACHMENT_ARRAY_PUSH,
        payload: {
            key: payload.id,
            src: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAMFBMVEXBx9D///+9w83Y3OHDydL19vfS1t3q7O/IzdXt7/HN0tnd4OXGy9Tl5+v4+frg4+dnyPTjAAAKUUlEQVR4nN2d28KjKgyFGUTF8/u/7dba/tWWQ0IWSve6mYuZqX5yTEiC+pdfc9cuQ9X01o7GKGNGa/umGpa2my94usr543M3VdboVcql7S+Mraa8oLkI53boNzI324lzI+2HNhdmDsJ5aoyn2QKg2jRTDko4YVdZNt2b0lYd+oWwhG2jkvFekKppoe8EJNzwRHRvSiQkirCuQHhPSFXVoDfDEE4WifeEtBPk3QCE8wBtvgOjGgCTq5iwbvLgPSEbcWcVEublgzCKCOs+Nx+AUUA4Z2+/N6NgPKYTVlfxPRirywmnC/F2pa4daYT1eGUD7tJj2nBMIry0gx4Yk7pqAmF3C96uBMuDT3jZDOpSQjNyCTtzI98mwx2NTMLhzgbcpYeMhHMGE4IvbVnrP4fwzinmLM6EwyAsoIe+pJcchJfssqnSPZxwHu+G+tBIHYxEwvpuIIeIywaNsC2ph76kafMNiXAqEXBFJJkbFMKlTEDilEogLBaQhhgnLGgZ/BZhCxclLBqQghgjLLiL7op21AhhobPoUbEZNUz4A4BRxCBh9wuAsaU/RFj/BqAKb+BChHe/N0NphPbu12bIphD26Ld4hJXswh84+u1FLyF2IdRbmMXSdnU913XXLlvABvYB3mXRR4icRrVqpu+5oJ5QkQ37Q3wTqodwBj668U/mHdK97DH6PYSoWUabmA03GRSkZ7ZxE4K223E+JKNnE+4kxAxCTT7ymzAD0j0UnYSQswndEPk2YcajoRI2iKcpXuBWC3mm66M6CBGONR3YZLg1IyY37fisDkLEk1JOayEnyxTCSv4YzrHCQYht1Pen/SIEmEw0P6ZDAINbf22evgjl5xPJgBDEMUYof0ZiF90l76hf3/eTUPoASfTSJsB0EyaUTzPsZeJD8kXj4xOfCWf4F+RL/Ab6bGSc30i8myGeeIUk3xSfdzYnQvlKIRuEu8Qj5bxinAjlrhkAIKCfnpw2x3cSN6FgJTxKvGKdGvFIKG5C6Tz6kng+PTbigVDehKhMF7F1c2zEA6F4Iv3aMCVLvHU8TKdvQvFaCBqFm+Qj8b0mvgkH4Y+CJtLna0n19kq9X6uItfAl+fb0mxA7RUsFXLj+CMUztNPRlSyxu+9v5XoRyj8aspMCuulfl1KwX8Qm8Ir3339f/EUo/L0vm0UqnB33/FPuI0Xt2F4SL/qvHdaTUO7m5vjwKYK90ZNQ3ick/ieXFvEb6SOhvJPCdt0vwV5pJ5R3CfBUCjnhaw6E4h/D7mg2IXzvb0LA9wIvFpDlYu9XD0KAG1aDARGT377oPwgBR3clEu5r9EYI6BBlEj6GzkaIiCItcRzuJtRGiDi3L5LwsV5shIjQixJXi91mVaCvVeCeRu09S6GSmsrbl6r9uytIaALcxEfl/FcPQkyUHto+hL2Vgiw8Cr8gwt5KYSaa8vw0z7eaV0JU9iQzTT4iuQf+ofW7K8ykpZDnMptQIbzLSoiJRATvakBDZ9vVKFxaBXJFRHWsdTJVmHDZTchuCsuNNysh6reQsykwF+KfAqZv0escxITL19G1An4umH0B/Oq6U8iiXahGRKZcTQo2aynYSIQmdi4KmquN2X4ji4zoQUFsp7/fQ6yJ2Ky5SqG2NLsAGxvYdmZXo8CJlPJ+Ci6E0yt0LqzU1oeOmlUWTiiMjIJXALAKXh1JtGTgKwBYha+hJ9jaZKgAYDIQpiPmKHGQqQpiWkfNVKQiC2OSBzxPmZEsvVQlOYgzlX01+Ll0F7N8Y76ikyN8PXyLszDmK7yMX/Hf0pY6p9YZq4Za9L70JFql8byVz3uwbfEhHa8Yn7syf4O1Dx0KX1OR42KMsyqsje+U1r2jtMnaessFJVFXGx/ppwk8SPWHm6u2m676TNd+fGqB+trCehQXMsYo7yVeOTQh/aUlSndIn3eJ0jXw3KJMIc+eipRBnh8WKQs8Ay5TDfAcv0wtwFiMIqVbXDxNmXrE04Cij8qUBsa1lSmLi00sVBUwvrRIPeNL/8dTzTNG+H+8b3vGeSN2NTqH5K/1itWXudO1Gvsqj/pR5gj4y7dIH4ju6rJI1YugUu1fzkzqiqgtOgXBrWSH3F/eU9qhiO7ztt5RadeBHnLXEnw12sIv0A6qS2jHQ/4h35PBvfwMIH5HO+SQ8teLaxtwF/tStGMeMHPjRr5NCivmrVqnXG6eBYVOj6GLNemf8vFZ3RRbpoUnzgbzXFOB003v6aK7GLXiP+pi0GdTeGkBnhgL24vs+Sd5LkZn4XFFtde/6tNQjy+wuT8pIk6oXzWGiNPUzX10E7GfftWJIppQuJSKdJFiKxy1vkhLYgFNSGzEd8Inr+befWv9UZQB5aq5R7GDcZURJSKctDjrJhL2NfDCCWkitIWz9iVhwSijkxK6qad+aXSSgufcpyq6PfHUoI02IrwyRKpiu2hvHeFYI8Kre6Qq1hTeWtCx/1nIRBOdagL1vGPT6aUYIYVfM1CTPfJx7jR9zwoawsG6+mHb5EcIg3cjhNv/Rwg//i3njpKfIIzeURIyMH+CMHrPTGjF+AVCwl1BgcnmFwgJ9z0FJptfIPz+t5x718onJN675t3ZlE9IvDvP+wPFE5LvP/T5ekonZNxh6bmHtHBCzj2kPj8BunJgspxvx7pL1nPGc8PZtlPuTsq7D9gzFItAnN19lHmns6/CSAHOqNrdvdj3cvucNqw7cHPIE6+QcLe61yvJTGEGy2PdBTy5AULvifKNLjefpzTw1UPeJZ8hBbzYiSlP8FfQzRn0n/nOsW4ajL6QofCZX9hD6PVp3DEYffWjIl0q4gP1Il7u4fcWXYiNmZiX11t46+Ke6r2ZPFpeLOrH9uZ6a+bt6RL5ixLEd1lxT70/nZ1WMgGgyRsITdhGEs4i/BXi9CXH3oGqGZQKeJTTloCXWI/ZozMCx6GkhZl0nhRyhGcO9w6VGKTN57QTs2AIS8bhOJnQg2ndh3gm6DZZXoi6ysIY5qNuj8mnnsGAOUKVFraWMB85LoR+rhtJedA9cnmcq3CmjKYH2DFOrmN1XrRZQJ21jSWQcLwpnLP5eMgcoiHrSPMpZgAhK/qAUHJMq0YCWQ9j/BE8w4YZX0GpSLRBJnXXbqCk/nD9fdwIko6UD6C1HXibnW4hFh0y3E0UP0aGWptL67EiJSfWbWWpCaMJNltCFBAn/2jF3ApEuUHnbhoay0mHZTdgGiE3jUw/soSN7ZumGoahqqqm6a3hp/qmuaPTIrlSywA+/ldiCjO9SCGCMGcpR59STdH0aLxM9UbdEpyXCOIN81Z0PPFJ7DNRRGVaAjKbT2ZjC2NG8zOKfQjiqNi81TkBdicg7nceMhV51GoAmGOYyOYcZUjDhU/pQsVuE6w6Fp6qUG4RYHR6K6jR8YEnsjE/hI2/3yBllBqL9w9NuKqjm0IOPFvBfeg5cijmqTFsytX6aKYcbtdcWSJzO/RU62j9d/2Q5vggKGsezNwtjX3UDfaRKWObpct6SHdFpk/dtctQrVavHY1Rxox2tYarYWk9tj9W/wHyKYDIdACaHQAAAABJRU5ErkJggg==",
            value: ''
        }
    })
    dispatch({
        type: agentTypes.PUSH_CERTIFICATE_ARRAY_FOR_REGISTER,
        payload: payload
    })
}

export const removeCertificateRow = (payload) => (dispatch) => {
    dispatch({
        type: agentTypes.REMOVE_CERTIFICATE_ROW,
        payload: payload
    })
}

export const registerAgentAction = (payload) => (dispatch) => {
    dispatch({
        api: app.registerAgent,
        types: [appTypes.REQUEST_START, agentTypes.REQUEST_REGISTER_AGENT_SUCCESS, appTypes.REQUEST_ERROR],
        data: payload
    })
}

export const editAgentAction = (payload) => (dispatch) => {
    dispatch({
        api: app.editAgent,
        types: [appTypes.REQUEST_START, "", appTypes.REQUEST_ERROR],
        data: payload
    }).then(res => {
        if (res.payload.success) {
            toast.success(res.payload.message)
            dispatch({type: "updateState", payload: {loading: false}})
        }
    })
}

export const editAgentOwnAction = (payload) => (dispatch) => {
    dispatch({
        api: app.editAgentOwn,
        types: [appTypes.REQUEST_START, "", appTypes.REQUEST_ERROR],
        data: payload
    }).then(res => {
        if (res.payload.success) {
            toast.success(res.payload.message)
            dispatch({type: "updateState", payload: {loading: false}})
        }
    })
}

export const getAgents = (payload) => async (dispatch, getState) => {
    let {app: {page, size}} = getState()
    dispatch({type: 'updateState', payload: {agents: ''}})
    return await dispatch({
        api: app.getAgents,
        types: [appTypes.REQUEST_START, '', appTypes.REQUEST_ERROR],
        data: payload == null ? {page: page, size: size} : payload
    }).then(res => {
        pageAbleAgentSet(res.payload, dispatch)
    })
}

function pageAbleAgentSet(obj, dispatch) {
    dispatch({
        type: 'updateState',
        payload: {
            agents: agentConfigList(obj.object),
            page: obj.page,
            size: obj.size,
            totalElements: obj.totalElements,
            totalPages: obj.totalPages,
            loading: false
        }
    })
}

function agentConfigList(item) {
    let agents = []
    item && item.map(agent => {
            agents.push(agentConfig(agent))
        }
    )
    agents && agents.sort((a, b) => (a.countOrder < b.countOrder) ? 1 : ((b.countOrder < a.countOrder) ? -1 : 0));
    return agents;
}

function agentConfig(agent) {
    if (agent.agentOnlineHour != null) {
        agent.agentOnlineHour = roundOut(agent.agentOnlineHour / 60) + ":" + agent.agentOnlineHour % 60
    }
    if (agent.agentLocationDto.createdAt != null) {
        agent.agentLocationDto.lastSeen = checkLastSeen(agent.agentLocationDto.createdAt);
    }
    let date = new Date(agent.updatedAt);

    if (date.getDate() >= new Date().getDate()) {
        agent.onlineHour = checkTimeLength(date.getHours()) + ":";
        agent.onlineMinute = checkTimeLength(date.getMinutes());
    } else {
        agent.onlineHour = checkTimeLength(date.getDate()) + "." + checkTimeLength(date.getMonth()) + "." + date.getFullYear()
    }
    return agent
}

function checkTimeLength(item) {
    return item.toString().length < 2 ? '0' + item : item
}

function checkLastSeen(createdAt) {
    let lastSeen = null;
    let date = (new Date() - new Date(createdAt)) / 1000 / 60;
    if (date < 60) lastSeen = {value: date < 1 ? "1" : roundOut(date), type: "minute"};
    else date = date / 60;
    if (lastSeen === null && date < 24) lastSeen = {value: date < 1 ? "1" : roundOut(date), type: "hour"};
    else date = date / 24;
    if (lastSeen === null && date < 365) lastSeen = {value: date < 1 ? "1" : roundOut(date), type: "day"};
    return lastSeen;
}

function roundOut(item) {
    let lastSeen = +item.toString().substring(0, item.toString().indexOf("."));
    if (+item < lastSeen + 0.5) return lastSeen;
    return lastSeen + 1;
}

export const getAgent = (id) => async (dispatch) => {
    dispatch({type: 'updateAgent', payload: {currentAgent: ''}})
    await dispatch({
        api: app.getAgentById,
        types: [appTypes.REQUEST_START, "", appTypes.REQUEST_ERROR],
        data: id
    }).then(res => {
        if (res.success) {
            console.log(res);
            dispatch({
                type: "updateState",
                payload: {
                    currentAgent: agentConfig(res.payload),
                    loading: false,
                    passports: res.payload.passportDtoList,
                    certificates: res.payload.certificateDtoList,
                }
            })
        } else window.location.reload()

    })
}

export const changeDocumentStatus = (payload) => (dispatch) => {
    dispatch({type: 'updateState', payload: {currentAgent: '', loading: true}})
    dispatch({
        api: app.changeStatusDocument,
        types: [appTypes.REQUEST_START, '', appTypes.REQUEST_ERROR],
        data: payload
    }).then(res => {
        if (res.payload.success) {
            dispatch(getAgent(payload.currentUserId));
            dispatch({type: "updateState", payload: {showStatusModal: false, loading: false}})
            toast.success("Successfully")
        } else {
            toast.error("error" + payload.statusEnum.toString())
            setTimeout(() => {
                window.location.reload();
            }, 1000)
        }


    })
}

export const addCertificateAction = (payload, userId) => (dispatch) => {
    dispatch({
        api: app.addCertificate,
        types: [appTypes.REQUEST_START, '', appTypes.REQUEST_ERROR],
        data: payload
    }).then(res => {
        if (res.payload.success) {
            toast.success("Successfully")
            dispatch(getCertificatesByUserId(userId))
            dispatch({type: 'updateState', payload: {loading: false, showCertificateModal: false, currentItem: null}})
        } else {
            toast.error("Sorry, Error")
            window.location.reload()
        }
    }).catch(res => {
        if (!res.response.data.success) {
            toast.error(res.response.data.message)
        }
    })
}

export const editCertificateAction = (payload) => (dispatch) => {
    dispatch({
        api: app.editCertificate,
        types: [appTypes.REQUEST_START, '', appTypes.REQUEST_ERROR],
        data: payload
    }).then(res => {
        if (res.payload.success) {
            toast.success("Successfully")
            dispatch(getCertificatesByUserId(payload.userId))
            dispatch({type: 'updateState', payload: {loading: false, showCertificateModal: false, currentItem: null}})
        } else {
            toast.error("Sorry, Error")
            window.location.reload()
        }
    })
}

export const addPassportAction = (payload) => (dispatch) => {
    dispatch({
        api: app.addPassport,
        types: [appTypes.REQUEST_START, '', appTypes.REQUEST_ERROR],
        data: payload
    }).then(res => {
        if (res.payload.success) {
            toast.success("Successfully")
            dispatch(getPassportsByUserId(payload.userId))
            dispatch({type: 'updateState', payload: {loading: false, showCertificateModal: false, currentItem: null}})
        } else {
            toast.error("Sorry, Error")
            window.location.reload()
        }
    }).catch(res => {
        if (!res.response.data.success) {
            toast.error(res.response.data.message)
            dispatch({type: "updateState", payload: {showCertificateModal: false, currentItem: null, loading: false}})
        }
    })
}

export const editPassportAction = (payload) => (dispatch) => {
    dispatch({
        api: app.editPassport,
        types: [appTypes.REQUEST_START, '', appTypes.REQUEST_ERROR],
        data: payload
    }).then(res => {
        if (res.payload.success) {
            toast.success("Successfully")
            dispatch(getPassportsByUserId(payload.userId))
            dispatch({type: 'updateState', payload: {loading: false, showCertificateModal: false, currentItem: null}})
        } else {
            toast.error("Sorry, Error")
            window.location.reload()
        }
    })
};

export const changeActiveAgentAction = (payload) => (dispatch) => {
    dispatch({
        api: app.changeActiveAgent,
        types: [types.REQUEST_START, "", types.REQUEST_ERROR],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getAgent(payload.id))
            dispatch({type: 'updateState', payload: {showModal: false, loading: false}})
            toast.success("Successfully  changed")
        } else toast.error("Error on Server")
    })
};

export const changeActiveOnlineAgentAction = (payload) => (dispatch) => {
    dispatch({
        api: app.changeActiveOnlineAgent,
        types: [types.REQUEST_START, "", types.REQUEST_ERROR],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getAgent(payload.id))
            dispatch({type: 'updateState', payload: {showModal: false, loading: false}})
            toast.success("Successfully  changed")
        } else toast.error("Error on Server")
    })
};

export const getPassportsByUserId = (id) => (dispatch, getState) => {
    let {agent: {currentAgent}} = getState();
    let agent = {...currentAgent}
    console.log(agent, id);
    dispatch({
        api: app.getPassportsByUserId,
        types: [appTypes.REQUEST_START, "", appTypes.REQUEST_ERROR],
        data: id
    }).then(res => {
        if (res.success) {
            agent.passportDtoList = res.payload;
            dispatch({
                type: 'updateState',
                payload: {
                    passports: res.payload,
                    loading: false,
                    currentAgent: agent
                }
            })
        } else window.location.reload();

    })
};

export const getCertificatesByUserId = (id) => (dispatch, getState) => {
    let {agent: {currentAgent}} = getState();
    let agent = {...currentAgent}
    dispatch({
        api: app.getCertificatesByUserId,
        types: [appTypes.REQUEST_START, "", appTypes.REQUEST_ERROR],
        data: id
    }).then(res => {
        if (res.success) {
            agent.certificateDtoList = res.payload;
            dispatch({type: 'updateState', payload: {certificates: res.payload, loading: false, currentAgent: agent}})
        } else window.location.reload();

    })
};

export const getAgentsBySearch = (data) => (dispatch, getState) => {
    dispatch({
        api: app.getAgentsBySearch,
        types: [appTypes.REQUEST_START, "", appTypes.REQUEST_ERROR],
        data: data
    }).then(res => {
        let filter = []
        Object.keys(res.data).map(item => {
            if (item === "search" || item === "zipCode" || item === 'date')
                filter.push({key: item, value: res.data[item]})
        })
        if (res.success) {
            pageAbleAgentSet(res.payload, dispatch);
            dispatch({type: 'updateState', payload: {filters: filter}})
        }
    })
};

export const getAgentsByFilter = (data) => (dispatch) => {
    dispatch({
        api: app.getAgentsByFilter,
        types: [appTypes.REQUEST_START, "", appTypes.REQUEST_ERROR],
        data: data
    }).then(res => {
        let filter = []
        Object.keys(res.data).map(item => {
            if (item === "search" || item === "zipCode" || item === 'date')
                filter.push({key: item, value: res.data[item]})
        })
        if (res.success) {
            pageAbleAgentSet(res.payload, dispatch);
            dispatch({type: 'updateState', payload: {filters: filter}})
        }
    })
};

export const checkEmailAndPhoneNumber = (data) => async (dispatch) => {
    return await dispatch({
        api: app.checkEmailAndPhone,
        types: [appTypes.REQUEST_START, "", appTypes.REQUEST_ERROR],
        data: data
    });
};
