import * as app from "../../api/PerApi";
import * as types from "../actionTypes/AdminActionTypes"
import {TOKEN} from "../../utils/constants";
import {toast} from "react-toastify";


export const getCurrentUser = (payload) => (dispatch) => {
    dispatch({
        api: app.userMe,
        types: [
            types.REQUEST_GET_USER_START,
            types.REQUEST_GET_USER_SUCCESS,
            types.REQUEST_GET_USER_ERROR
        ],
        data: payload
    })
}

export const getServices = () => (dispatch) => {
    dispatch({
        api: app.getServices,
        types: [
            types.REQUEST_GET_SERVICES_START,
            types.REQUEST_GET_SERVICES_SUCCESS,
            types.REQUEST_GET_SERVICES_ERROR,
        ]
    })
}
export const getAdminZipCode = (payload) => (dispatch) => {
    dispatch({
        api: app.getAdminZipCode,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_ADMIN_ZIP_CODE,
            types.REQUEST_ERROR,
        ],
        data: payload
    })
}

export const getAdminList = (payload) => (dispatch) => {
    dispatch({
        api: app.getAdminList,
        types: [
            types.REQUEST_GET_ADMIN_LIST_START,
            types.REQUEST_GET_ADMIN_LIST_SUCCESS,
            types.REQUEST_GET_ADMIN_LIST_ERROR
        ],
        data: payload
    })
}
export const getUserTotalCount = (payload) => (dispatch) => {
    dispatch({
        api: app.getUserTotalCount,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_USER_COUNT,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}
export const getAgentList = (payload) => (dispatch) => {
    dispatch({
        api: app.getAgentList,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_AGENT_LIST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}
export const getClientList = (payload) => (dispatch) => {
    dispatch({
        api: app.getClientList,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_CLIENT_LIST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}
export const getPermissionList = (payload) => (dispatch) => {
    dispatch({
        api: app.getPermissionList,
        types: [
            types.REQUEST_GET_PERMISSION_LIST_START,
            types.REQUEST_GET_PERMISSION_LIST_SUCCESS,
            types.REQUEST_GET_PERMISSION_LIST_ERROR
        ],
        data: payload
    })
}
export const getNewPermissionList = (payload) => (dispatch) => {
    dispatch({
        api: app.getNewPermissionList,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_NEW_PERMISSION_LIST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    })
}
export const editAdmin = (payload) => (dispatch) => {
    dispatch({
        api: app.editAdmin,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getAdminList())
            toast.success("Admin  saved successfully!");
        } else {
            toast.error("You cannot change")
        }
    }).catch(err => {
        toast.error("Error saving admin ");
    })
}
export const deleteAdmin = (payload) => (dispatch) => {
    dispatch({
        api: app.deleteAdmin,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getAdminList())
            dispatch({
                type: 'updateState',
                payload: {
                    showDeleteModal: false,
                    currentItem: null,
                }
            })
            toast.success("Admin  deleted successfully!");
        } else {
            toast.error("You cannot delete")
        }
    }).catch(err => {
        toast.error("Error delete admin ");
    })
}
export const saveAdmin = (payload) => (dispatch) => {
    dispatch({
        api: app.saveAdmin,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getAdminList())
            toast.success("Admin  saved successfully!");
        } else {
            toast.error("You cannot change")
        }
    }).catch(err => {
        toast.error("Error saving admin ");
        dispatch({
            type: types.REQUEST_SUCCESS
        })
    })
}
export const editAdminZipCode = (payload) => (dispatch) => {
    dispatch({
        api: app.editAdminZipCode,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getAdminList())
            toast.success("Admin  saved successfully!");
        } else {
            toast.error("You cannot change")
        }
    }).catch(err => {
        toast.error("Error saving admin ");
    })
}
export const changeAdminEnabled = (payload) => (dispatch) => {
    dispatch({
        api: app.editAdminEnabled,
        types: [
            types.REQUEST_GET_ADMIN_BY_ENABLED_START,
            types.REQUEST_GET_ADMIN_BY_ENABLED_SUCCESS,
            types.REQUEST_GET_ADMIN_BY_ENABLED_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getAdminList())
            toast.success("Admin enabled changed!")
        } else {
            toast.error("Error cannot change admin enabled")
        }
    }).catch(err => {
        toast.error("Error change admin enabled");
    })
}
export const editAdminPermission = (payload) => (dispatch) => {
    dispatch({
        api: app.editAdminPermission,
        types: [
            types.REQUEST_START,
            types.REQUEST_SUCCESS,
            types.REQUEST_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getAdminList())
            toast.success("Admin permission saved successfully!");
        } else {
            toast.error("You cannot change")
        }
    }).catch(err => {
        toast.error("Error saving admin permission");
    })
}


export const getStateList = () => (dispatch) => {
    dispatch({
        api: app.getStates,
        types: [
            types.REQUEST_GET_STATE_LIST_START,
            types.REQUEST_GET_STATE_LIST_SUCCESS,
            types.REQUEST_GET_STATE_LIST_ERROR
        ]
    })

};
export const getCountyByState = (payload) => (dispatch) => {
    dispatch({
        api: app.getCountyByState,
        types: [
            types.REQUEST_GET_ADMIN_BY_COUNTY_LIST_START,
            types.REQUEST_GET_ADMIN_BY_COUNTY_LIST_SUCCESS,
            types.REQUEST_GET_ADMIN_BY_COUNTY_LIST_ERROR
        ],
        data: payload
    })
};
export const getZipCodeByCounty = (payload) => (dispatch) => {
    dispatch({
        api: app.getZipCodeByCounty,
        types: [
            types.REQUEST_GET_ADMIN_BY_ZIPCODE_LIST_START,
            types.REQUEST_GET_ADMIN_BY_ZIPCODE_LIST_SUCCESS,
            types.REQUEST_GET_ADMIN_BY_ZIPCODE_LIST_ERROR
        ],
        data: payload
    })

};