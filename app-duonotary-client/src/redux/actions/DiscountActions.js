import * as discountApi from "../../api/DiscountApi";
import * as types from "../actionTypes/DiscountTypes"



export const getSharingDiscountTariff= (data) => async (dispatch) => {
   await dispatch({
        api: discountApi.getSharingDiscountTariff,
        types: [
            types.REQUEST_START,
            types.GET_SHARING_DISCOUNT_TARIFF_SUCCESS,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const getFirstOrderDiscountTariff= (data) => async (dispatch) => {
   await dispatch({
        api: discountApi.getFirstOrderDiscountTariff,
        types: [
            types.REQUEST_START,
            types.GET_FIRST_ORDER_DISCOUNT_TARIFF_SUCCESS,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const changeActiveStatusSharingDiscountTariff= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.changeActiveStatusSharingDiscountTariff,
        types: [
            types.REQUEST_START,
            types.CHANGING_SHARING_TARIFF_STATUS,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const changeActiveStatusFirstOrderDiscountTariff= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.changeActiveStatusFirstOrderDiscountTariff,
        types: [
            types.REQUEST_START,
            types.CHANGING_SHARING_TARIFF_STATUS,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const changePercentSharingDiscountTariff= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.changePercentSharingDiscountTariff,
        types: [
            types.REQUEST_START,
            types.CHANGING_SHARING_TARIFF_STATUS,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const changePercentFirstOrderDiscountTariff= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.changePercentFirstOrderDiscountTariff,
        types: [
            types.REQUEST_START,
            types.CHANGING_SHARING_TARIFF_STATUS,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const saveOrEditLoyaltyDiscountTariff= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.saveOrEditLoyaltyDiscountTariff,
        types: [
            types.REQUEST_START,
            types.CHANGING_SHARING_TARIFF_STATUS,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const saveOrEditCustomDiscountTariff= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.saveOrEditCustomDiscountTariff,
        types: [
            types.REQUEST_START,
            types.CHANGING_SHARING_TARIFF_STATUS,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const saveOrEditCustomDiscount= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.saveOrEditCustomDiscount,
        types: [
            types.REQUEST_START,
            types.CHANGING_SHARING_TARIFF_STATUS,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const getAllLoyaltyDiscountTariff= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.getAllLoyaltyDiscountTariff,
        types: [
            types.REQUEST_START,
            types.GET_LOYALTY_DISCOUNT_TARIFF_SUCCESS,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const changeLoyaltyDiscountTariffActive= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.changeLoyaltyDiscountTariffActive,
        types: [
            types.REQUEST_START,
            types.CHANGING_SHARING_TARIFF_STATUS,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const removeLoyaltiDiscountTariff= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.removeLoyaltiDiscountTariff,
        types: [
            types.REQUEST_START,
            types.CHANGING_SHARING_TARIFF_STATUS,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const removeCustomDiscountTariff= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.removeCustomDiscountTariff,
        types: [
            types.REQUEST_START,
            types.CHANGING_SHARING_TARIFF_STATUS,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const removeCustomDiscountOrder= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.removeCustomDiscountOrder,
        types: [
            types.REQUEST_START,
            types.CHANGING_SHARING_TARIFF_STATUS,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const getAllSharingDiscountGiven= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.getAllSharingDiscountGiven,
        types: [
            types.REQUEST_START,
            types.GET_ALL_SHARING_DISCOUNT_GIVEN_SUCCESS,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const getAllLoyaltyDiscountGiven= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.getAllLoyaltyDiscountGiven,
        types: [
            types.REQUEST_START,
            types.GET_ALL_LOYALTY_DISCOUNT_GIVEN_SUCCESS,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const getAllDiscountExpense= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.getAllDiscountExpense,
        types: [
            types.REQUEST_START,
            types.GET_ALL_DISCOUNT_EXPENSE_SUCCESS,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const getAllCustomerDiscount= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.getAllCustomerDiscount,
        types: [
            types.REQUEST_START,
            types.GET_ALL_CUSTOMER_DISCOUNT_SUCCESS,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const getAllCustomDiscountTariff= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.getAllCustomDiscountTariff,
        types: [
            types.REQUEST_START,
            types.GET_ALL_CUSTOM_DISCOUNT_TARIFF,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const getBySearch= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.getBySearch,
        types: [
            types.REQUEST_START,
            types.GET_ALL_CLIENTS_BY_STATE_OR_COUNTY_OR_ZIP_CODE,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
export const getZipCodeBySelectedCounty= (data) => async (dispatch) => {
    await dispatch({
        api: discountApi.getZipCodeBySelectedCounty,
        types: [
            types.REQUEST_START,
            types.GET_ALL_ZIP_CODE_CLIENTS_BY_COUNTY,
            types.REQUEST_ERROR
        ],
        data:data
    })
}
