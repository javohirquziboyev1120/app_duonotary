import * as types from "../actionTypes/AuthActionTypes";
import * as types2 from "../actionTypes/AppActionTypes";
import {getUserByUserId, loginUser, register, verifyEmail,} from "../../api/AuthApi.js";
import {NEW_USER_FOR_ORDER, TOKEN} from "../../utils/constants";
import {confirmCode, editPassword, forgotPassword, me, resetPassword,} from "../../api/AuthApi";
import {toast} from "react-toastify";
import * as app2 from "../../api/AppApi";
import jwt from "jwt-decode";

export const login = (payload) => async (dispatch) => {
    try {
        const res = await dispatch({
            api: loginUser,
            types: [types.REQUEST_AUTH_START, "", types.REQUEST_API_ERROR],
            data: payload.v,
        });

        if (res.success) {
            let parsedToken = jwt(res.payload.token);

            setTimeout(() => {
                setStateRole(parsedToken.roles, dispatch);
                pushHisPage(parsedToken.roles, payload.history);
            }, 1000);
            localStorage.setItem(
                TOKEN,
                res.payload.tokenType + " " + res.payload.token
            );
        }
        return true;
    } catch (err) {
        if (err.response) toast.error(err.response.data.message);

        return false;
    }
};

export const logout = () => (dispatch) => {
    dispatch({
        type: types.AUTH_LOGOUT,
    });
};

export const verifyEmailClient = (payload) => (dispatch) => {
    dispatch({
        api: verifyEmail,
        types: [
            types.REQUEST_AUTH_START,
            types.REQUEST_VERIFY_EMAIL_SUCCESS,
            types.REQUEST_VERIFY_EMAIL_ERROR,
        ],
        data: payload,
    })
        .then((res) => {
            if (res.success) {
                toast.success(res.payload.message);
            }
        })
        .catch((err) => {
            if (err.response) {
                toast.error(err.response.data.message);
            }
        });
};

export const getUserSharingDiscount = () => (dispatch) => {
    dispatch({
        api: app2.getUserSharingDiscount,
        types: [
            types.REQUEST_START,
            types.REQUEST_GET_USER_SHARING_DISCOUNT_SUCCESS,
            types.REQUEST_ERROR,
        ],
    });
};

export const userMe = (payload) => async (dispatch, getState) => {
    const {
        auth: {currentUser, sentUserMe},
    } = getState();
    if (sentUserMe || currentUser || !localStorage.getItem(TOKEN)) return;

    try {
        const response = await dispatch({
            api: me,
            types: [
                types.AUTH_GET_CURRENT_USER_REQUEST,
                types.AUTH_GET_USER_TOKEN_SUCCESS,
                types.AUTH_GET_CURRENT_USER_ERROR,
            ],
        });
        if (response.success) {
            dispatch({
                type: "updateState",
                payload: {
                    permissions: response.payload.permissions,
                },
            });
            if (payload) {
                dispatch({
                    type: "updateStateOrder",
                    payload: {currentUser: response.payload},
                });
            }
            dispatch({
                type: types.AUTH_GET_USER_TOKEN_SUCCESS,
                payload: response.payload,
            });
            setStateRole(response.payload.roles, dispatch);
        } else {
            dispatch({
                type: types.AUTH_LOGOUT,
            });
        }
    } catch (e) {
        dispatch({
            type: types.AUTH_LOGOUT,
        });
    }
};

export const registerUser = (payload) => (dispatch) => {
    dispatch({
        api: register,
        types: [
            types.REQUEST_AUTH_START,
            types.AUTH_REGISTER_USER_SUCCESS,
            types.REQUEST_API_ERROR,
        ],
        data: payload,
    })
        .then((res) => {
            dispatch({
                type: types.REQ_ALERT,
                payload: res.payload.message,
            });
        })
        .catch((err) => {
            if (err.response) toast.error("Please, try again!");
        });
};

export const registerOrderUser = (payload) => (dispatch) => {
    dispatch({
        api: register,
        types: [types.REQUEST_AUTH_START, "", types.REQUEST_API_ERROR],
        data: payload,
    })
        .then((res) => {
            if (res.success) {
                dispatch({
                    type: "updateStateOrder",
                    payload: {signUp: res.payload.success},
                });
                localStorage.setItem(NEW_USER_FOR_ORDER, JSON.stringify(payload));
            }
        })
        .catch((err) => {
            if (err.response) toast.error(err.response.data.message);
        });
};

export const loginForOrder = (payload) => (dispatch) => {
    dispatch({
        api: loginUser,
        types: [
            types.REQUEST_AUTH_START,
            types.AUTH_LOGIN_USER_SUCCESS,
            types.REQUEST_API_ERROR,
        ],
        data: payload.value,
    }).then((res) => {
        if (res.success) {
            localStorage.setItem(
                TOKEN,
                res.payload.tokenType + " " + res.payload.token
            );
            dispatch(userMe("order"));
            dispatch({
                type: 'updateStateOrder',
                payload: payload.online ? {step: payload.next} : {stage: payload.next}
            })
        } else {
            toast.error("Username or Password wrong");
        }
    }).catch(err => {
        toast.error("Username or Password wrong");
    });
};

export const getUserById = (id) => (dispatch) => {
    dispatch({
        api: getUserByUserId,
        types: ["", "", ""],
        data: id,
    });
};

export const forgotPasswordAction = (payload) => (dispatch, history) => {
    dispatch({
        api: forgotPassword,
        types: [types2.REQUEST_START, "", types2.REQUEST_ERROR],
        data: payload.v,
    })
        .then((res) => {
            if (res.payload.success) {
                toast.success(res.payload.message);
                dispatch({type: "updateState", payload: {loading: false}});
                payload.history.push("/");
            }
        })
        .catch((res) => {
            toast.error(res.response.data.message);
        });
};

export const confirmCodeAction = (payload) => (dispatch, history) => {
    dispatch({
        api: confirmCode,
        types: [types2.REQUEST_START, "", types2.REQUEST_ERROR],
        data: payload.v,
    }).then((res) => {
        if (res.payload.success) {
            toast.success(res.payload.message);
            payload.history.push("/resetPassword/" + res.payload.object);
            dispatch({type: "updateState", payload: {loading: false}});
        } else toast.error(res.payload.message);
    });
};

export const resetPasswordAction = (payload) => (dispatch, history) => {
    dispatch({
        api: resetPassword,
        types: [types2.REQUEST_START, "", types2.REQUEST_ERROR],
        data: payload.v,
    }).then((res) => {
        if (res.payload.success) {
            toast.success(res.payload.message);
            payload.history.push("/");
            dispatch({type: "updateState", payload: {loading: false}});
        } else toast.error(res.payload.message);
    });
};
export const editPasswordAction = (payload) => (dispatch, history) => {
    dispatch({
        api: editPassword,
        types: [types2.REQUEST_START, "", types2.REQUEST_ERROR],
        data: payload,
    })
        .then((res) => {
            if (res.payload.success) {
                toast.success(res.payload.message);
            } else toast.error(res.payload.message);
        })
        .catch((res) => {
            toast.error(res.response.data.message);
        });
};

const setStateRole = (roles, dispatch) => {
    let roleStatus = ""
    roles.forEach((item) => {
        if (item.roleName === "ROLE_SUPER_ADMIN") {
            dispatch({
                type: "updateState",
                payload: {
                    isSuperAdmin: true,
                    isAdmin: true,
                },
            });
            roleStatus = 'superAdmin'
        } else if (item.roleName === "ROLE_ADMIN") {
            dispatch({type: "updateState", payload: {isAdmin: true}});
            roleStatus = 'admin'
        } else if (item.roleName === "ROLE_AGENT") {
            dispatch({type: "updateState", payload: {isAgent: true}});
            roleStatus = 'agent'
        } else if (item.roleName === "ROLE_USER") {
            dispatch({type: "updateState", payload: {isUser: true}});
            roleStatus = 'user'
        }
        localStorage.setItem('role', roleStatus);
    });
};

const pushHisPage = (roles, history) => {
    const {push} = history;
    roles.forEach(({roleName}) => {
        if (roleName === "ROLE_SUPER_ADMIN") {
            push("/admin");
        } else if (roleName === "ROLE_ADMIN") {
            push("/admin");
        } else if (roleName === "ROLE_AGENT") {
            push("/agent");
        } else if (roleName === "ROLE_USER") {
            push("/client/main-page");
        }
    });
};
