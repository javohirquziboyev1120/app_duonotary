import * as app from "../../api/AppApi";
import * as appTypes from "../actionTypes/AppActionTypes";
import * as types from "../actionTypes/AdditionalServicePriceActionType";
import {toast} from "react-toastify";

export const getAdditionalServicePriceList = () => (dispatch) => {
    dispatch({
        api: app.getAdditionalServicePrice,
        types: [
            types.REQUEST_ADDITIONAL_SERVICE_PRICE_START,
            types.REQUEST_GET_ADDITIONAL_SERVICE_PRICE_LIST_SUCCESS,
            types.REQUEST_ADDITIONAL_SERVICE_PRICE_ERROR
        ]
    })

};
export const removeCountyArrZipCodeArr = () => (dispatch) => {
    dispatch({
        api: app.getAdditionalServicePrice,
        types: [
            types.REQUEST_ADDITIONAL_SERVICE_PRICE_START,
            types.REMOVE_COUNTYARR_ZIPCODEARR,
            types.REQUEST_ADDITIONAL_SERVICE_PRICE_ERROR
        ]
    })

};
export const getAdditionalServicePriceDashboard = () => (dispatch) => {
    dispatch({
        api: app.getAdditionalServicePriceDashboard,
        types: [
            types.REQUEST_ADDITIONAL_SERVICE_PRICE_START,
            types.REQUEST_GET_ADDITIONAL_SERVICE_PRICE_DASHBOARD,
            types.REQUEST_ADDITIONAL_SERVICE_PRICE_ERROR
        ],
    })

};
export const getCountyByState = (paload) => (dispatch) => {
    dispatch({
        api: app.getCountyByState,
        types: [
            "",
            appTypes.REQUEST_GET_SERVICE_PRICE_BY_STATE_LIST_SUCCESS,
            appTypes.REQUEST_SERVICE_PRICE_ERROR
        ],
        data: paload
    })

};
export const getZipCodeByCounty = (paload) => (dispatch) => {
    dispatch({
        api: app.getZipCodeByCounty,
        types: [
            "",
            appTypes.REQUEST_GET_SERVICE_PRICE_BY_COUNTY_LIST_SUCCESS,
            appTypes.REQUEST_SERVICE_PRICE_ERROR
        ],
        data: paload
    })

};
export const saveAdditionalServicePrice = (payload) => (dispatch) => {
    dispatch({
        api: payload.id ? app.editAdditionalServicePrice : app.addAdditionalServicePrice,
        types: [
            types.REQUEST_ADDITIONAL_SERVICE_PRICE_START,
            types.REQUEST_ADDITIONAL_SERVICE_PRICE_SUCCESS,
            types.REQUEST_ADDITIONAL_SERVICE_PRICE_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getAdditionalServicePriceDashboard())
            toast.success("AdditionalServicePrice saved successfully!");
        } else {
            toast.error("You cannot save AdditionalServicePrice!")
        }
    }).catch(err => {
        toast.error("Error saving AdditionalServicePrice!");
    })
};
export const editAdditionalServicePriceActive = (payload) => (dispatch) => {
    dispatch({
        api:app.editAdditionalServicePriceActive,
        types: [
            types.REQUEST_ADDITIONAL_SERVICE_PRICE_START,
            types.REQUEST_ADDITIONAL_SERVICE_PRICE_SUCCESS,
            types.REQUEST_ADDITIONAL_SERVICE_PRICE_ERROR
        ],
        data: payload
    }).then(res => {
        if (res.success) {
            dispatch(getAdditionalServicePriceList())
            toast.success("AdditionalServicePrice edit active successfully!");
        } else {
            toast.error("You cannot edit AdditionalServicePrice!")
        }
    }).catch(err => {
        toast.error("Error edit AdditionalServicePrice!");
    })
};
export const deleteAdditionalServicePrice = (payload) => (dispatch) => {
    dispatch({
        api: app.deleteAdditionalServicePrice,
        types: [
            types.REQUEST_ADDITIONAL_SERVICE_PRICE_START,
            types.REQUEST_ADDITIONAL_SERVICE_PRICE_SUCCESS,
            types.REQUEST_ADDITIONAL_SERVICE_PRICE_ERROR
        ],
        data: payload
    }).then(res => {
        dispatch(getAdditionalServicePriceList())
        dispatch({
            type:types.REQUEST_ADDITIONAL_SERVICE_PRICE_SUCCESS
        })
        toast.success("AdditionalServicePrice deleted successfully!");
    }).catch(err => {
        toast.error("Error deleting AdditionalServicePrice!");
    })
};
