import {createReducer} from "../../utils/StoreUtils";
import * as types from "../actionTypes/ZipCodeTypes";
import {toast} from "react-toastify";

const initState = {
    isZipCode: true,
    isCounty: false,
    isState: false,
    loading: false,
    zipCodeArray: [],
    countyArray: [],
    stateArray: [],
    stateArrayPageable: [],
    showModal: false,
    showDeleteModal: false,
    currentZipCode: '',
    currentCounty: '',
    currentState: '',
    removingType: '',
    removingItemId: '',
    zipCodeActiveState: false,
    totalStateElements: 0,
    totalCountyElements: 0,
    totalZipCodeElements: 0,
    activePage: 0,
    statesWithZipCodes: [],
    showCurrentZipCode: false,
    currentZipCode2: '',
    removingAgentId: '',
    agentList: [],
    isAddAgent: false,
    tempSelectedStateId: '',
    timezoneName: [],
    active: false
}
const reducers = {
    [types.REQUEST_START](state) {
        // state.loading = true
    },
    [types.ADD_AGENT_SUCCESS](state) {
    },
    [types.REQUEST_ERROR](state) {
        // toast.error("Error")
    },
    [types.DELETING_REQUEST_ERROR](state) {
        state.loading = true
        toast.error("Error in Deleting")
    },
    [types.OPEN_CLOSE_MODAL](state) {
        state.showModal = !state.showModal;
        state.active = false;
        state.currentZipCode = '';
        state.currentZipCode2 = '';
        state.currentCounty = '';
        state.currentState = '';
    },
    [types.OPEN_CLOSE_MODAL_CANCEL](state) {
        state.showModal = !state.showModal;
        state.currentCounty = '';
        state.active = false;
        state.currentState = '';
    },
    [types.OPEN_CLOSE_DELETE_MODAL](state) {
        state.showDeleteModal = !state.showDeleteModal;
        state.removingType = '';
        state.removingItemId = '';
    },
    [types.GET_CURRENT_ZIP_CODE](state, payload) {
        state.showModal = !state.showModal;
        state.currentZipCode = payload.payload;
        state.currentCounty = '';
        state.currentState = '';
    },
    [types.GET_CURRENT_ZIP_CODE_2](state, payload) {
        state.showCurrentZipCode = true;
        state.currentZipCode2 = payload.payload;

    },
    [types.GET_ALL_AGENTS_BY_ZIP_CODE](state, payload) {
        state.agentList = payload.payload;

    },
    [types.GET_CURRENT_COUNTY](state, payload) {
        state.showModal = !state.showModal;
        state.currentCounty = payload.payload;
        state.currentZipCode = '';
        state.currentState = '';
    },
    [types.GET_CURRENT_STATE](state, payload) {
        state.showModal = true;
        state.showDeleteModal = false;
        state.currentState = payload.payload;
        state.currentZipCode = '';
        state.currentCounty = '';
    },
    [types.CHANGE_ZIP_CODE_BOOLEAN](state) {
        state.isZipCode = true;
        state.isCounty = false;
        state.isState = false;
        state.showModal = false;
    },
    [types.CHANGE_COUNTY_BOOLEAN](state) {
        state.isZipCode = false;
        state.isCounty = true;
        state.isState = false;
        state.showModal = false;
    },
    [types.DELETE_CURRENT_ZIP_CODE](state) {
        toast.success("Successfully deleted")
        state.removingType = '';
        state.removingItemId = '';
    },
    [types.DELETE_CURRENT_COUNTY](state) {
        toast.success("Successfully deleted")
        state.removingType = '';
        state.removingItemId = '';
    },
    [types.DELETE_CURRENT_STATE](state) {
        toast.success("Successfully deleted")
        state.removingType = '';
        // state.removingItemId= '';
        state.removingAgentId = '';
    },
    [types.CHANGE_STATE_BOOLEAN](state) {
        state.isZipCode = false;
        state.isCounty = false;
        state.isState = true;
        state.showModal = false;
    },
    [types.GET_ALL_ZIP_CODES](state, payload) {
        state.zipCodeArray = payload.payload.object
        state.totalZipCodeElements = payload.payload.totalElements
        state.activePage = payload.payload.page
    },
    [types.GET_ALL_COUNTIES](state, payload) {
        state.countyArray = payload.payload.object
        state.totalCountyElements = payload.payload.totalElements
        state.activePage = payload.payload.page
    },
    [types.GET_ALL_STATES_BY_PAGEABLE](state, payload) {
        state.stateArrayPageable = payload.payload.object
        state.totalStateElements = payload.payload.totalElements
        state.activePage = payload.payload.page
    },
    [types.GET_ALL_STATES](state, payload) {
        state.stateArray = payload.payload
    },
    [types.GET_ALL_STATES_2](state, payload) {
        state.statesWithZipCodes = payload.payload
    },
    [types.GET_ALL_TIMEZONE](state, payload) {
        console.log(payload);
        state.timezoneName = payload.payload._embedded.list
    },
    [types.GET_REMOVING_TYPE_AND_ID](state, payload) {
        state.showDeleteModal = !state.showDeleteModal;
        state.removingType = payload.payload.typ;
        state.removingItemId = payload.payload.id;
        state.removingAgentId = payload.payload.agentId;
    },
    [types.SAVE_OR_EDIT_STATE](state) {
        state.showModal = !state.showModal;
        state.currentZipCode = '';
        state.currentCounty = '';
        state.currentState = '';
    },
    [types.SAVE_OR_EDIT_COUNTY](state) {
        state.currentZipCode = '';
        state.currentCounty = '';
        state.currentState = '';
        state.currentZipCode2 = '';
    },
    [types.GET_COUNTY_BY_STATE_ID](state, payload) {
        state.countyArray = payload.payload
    },
    [types.ADD_AGENT_SUCCESS](state, payload) {
    },
    updateState(state, {payload}) {
        return {
            ...state,
            ...payload
        }
    },
};
export default createReducer(initState, reducers);