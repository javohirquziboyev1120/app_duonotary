import {createReducer} from "../../utils/StoreUtils";
import * as types from "../actionTypes/SharingActionTypes";

const initState = {
    loading: false,
    photoUrl: "",
    userSharingDiscount: null
}
const reducers = {
    [types.REQUEST_GET_USER_QR_CODE_ERROR](state) {
    },
    [types.REQUEST_GET_USER_QR_CODE_START](state) {
        state.loading = true;

    },
    [types.REQUEST_GET_USER_QR_CODE_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload.object)
            state.photoUrl = payload.payload.object;
    },

    updateState(state, {payload}) {
        return {
            ...state,
            ...payload
        }
    },
};
export default createReducer(initState, reducers);

