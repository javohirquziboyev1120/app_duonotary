import * as types from "../actionTypes/AppActionTypes";
import {createReducer} from "../../utils/StoreUtils";
import {toast} from "react-toastify";

const initState = {
    loading: false,
    countries: [],
    states: '',
    itemType: "",
    isOpenUser: false,
    stateOptions: [],
    selectHolidays: [],
    showEmbassyModal: false,
    holiday: "",
    isOrderDay: false,
    isOpenPages: false,
    isOpenGeneral: false,
    countyOptions: [],
    zipCodeOptions: [],
    timeBooked: '',
    timeDuration: '',
    discountPercents: [],
    weekDays: [],
    agentSchedules: [],
    today: '',
    text: '',
    selectedDates: [],
    setSelectedDates: [],
    showModal: false,
    changeCalendar: false,
    openChangeStatusModal: true,
    currentCountry: '',
    currentItem: '',
    currentAuditItems: '',
    showDeleteModal: false,
    showEditModal: false,
    showStatusModal: false,
    showFbiModal: false,
    showDefaultInputModal: false,
    showOnlineModal: false,
    showDynamicModal: false,
    active: false,
    counties: '',
    servicePrices: [],
    pricingList: [],
    online: false,
    dynamic: false,
    defaultInput: false,
    all: false,
    mainServices: [],
    mainServicesWithPercent: [],
    subServices: [],
    services: [],
    additionalServices: [],
    additionalServicePrices: [],
    zipCodes: [],
    userSharingDiscountList: [],
    selectZipCodes: [],
    selectCounties: [],
    selectStates: [],
    selectMainServices: [],
    histories: [],
    historyTables: [],
    auditTables: [],
    showDropdown: false,
    page: 0,
    size: 10,
    totalPages: '',
    totalElements: '',
    objectId: '',
    operation: '',
    weekDayId: '',
    agentHours: [{id: new Date().toString()}],
    dayOff: false,
    historTime: false,
    isOpen: false,
    startDate: new Date(10),
    finishDate: new Date(10),
    feedBacks: '',
    showFeedBackModal: false,
    currentAuditItemUser: "",
    pricingDashboard: [],
    customers: [],
    mainServicesMultiSelect: [],
    agentActivePage: 1,
    clientActivePage: 1,
    activePageSub: 0,
    totalElementsSub: 0,
    subServicePage: [],
    activePageService: 0,
    totalElementsService: 0,
    servicePage: [],
    activePageAddService: 0,
    totalElementsAdditional: 0,
    additionalServicePage: [],
    activePageDocType: 0,
    totalElementsDocType: 0,
    docTypePage: [],
    activePageDisPer: 0,
    totalElementsDisPer: 0,
    discountPercentPage: [],
    activePageDynamic: 0,
    totalElementsDynamic: 0,
    dynamicPage: [],
    serviceEnum: ['REAL_ESTATE', 'INTERNATIONAL', 'OTHERS',],
    fbi: false,
    openCol: false,
    embassy: false,
    payType: [],
    isFilter: false,
    filters: '',
    blogs: [],
    activePageBlog: 0,
    totalElementsBlog: 0,
    blogsPage: [],
    blogFeatureds: [],
    categorys: [],
    terms: '',
    blogByCategory: '',
    blogByUrl: '',
    dynamics: [],
    MainServiceHours: [{id: new Date().toString()}],
    isMainService: true,
    isMainServicePercent: false,
    isAdditional: true,
    isAdditionalPrice: false,
    showDaylyModal: false,
    daylySchedules: [],
    currentDate: '',
    showModalDuration: false,
    showModalBooked: false,
    agentsByDiscount: [],
    partners: '',
    optionsStateByPricing: [],
    optionsCountyByPricing: [],
    optionsZipCodeByPricing: [],
    selectStateByPricing: [],
    selectCountyByPricing: [],
    selectZipCodeByPricing: [],
    pricingByStates: [],
    reviews: [],
    faq: [],
    reviewModal: false
};

const reducers = {
    [types.REQUEST_START](state) {
        state.optionsStateByPricing = []
        state.optionsCountyByPricing = []
        state.optionsZipCodeByPricing = []
        state.selectStateByPricing = null
        state.selectCountyByPricing = null
        state.selectZipCodeByPricing = null
        state.loading = true;
        state.text = '';
    },
    [types.REQUEST_ERROR](state) {
        state.optionsStateByPricing = []
        state.optionsCountyByPricing = []
        state.optionsZipCodeByPricing = []
        state.selectStateByPricing = null
        state.selectCountyByPricing = null
        state.selectZipCodeByPricing = null
        state.loading = false;
    },
    [types.REQUEST_GET_COUNTRY_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload._embedded && payload.payload._embedded.list && payload.payload._embedded.list.length > 0) {
            state.countries = (payload.payload._embedded.list.sort((a, b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0)));
            state.page = payload.payload.page.number;
            state.size = payload.payload.page.size;
            state.totalElements = payload.payload.page.totalElements;
            state.totalPages = payload.payload.page.totalPages;
        }
    },
    [types.REQUEST_GET_PAY_TYPE_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload._embedded && payload.payload._embedded.list && payload.payload._embedded.list.length > 0) {
            state.payType = payload.payload._embedded.list;
        }
    },
    [types.REQUEST_GET_AGENT_BY_DISCOUNT](state, payload) {
        if (payload.payload)
            state.agentsByDiscount = payload.payload
    },
    [types.REQUEST_GET_STATE_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload._embedded && payload.payload._embedded.list && payload.payload._embedded.list.length > 0) {
            // stateOptions=[...stateOptions,...payload.payload._embedded.list.map(item=>{return {value:item.id, label:item.name}})]
            state.stateOptions = payload.payload._embedded.list.map(item => {
                return {value: item.id, label: item.name}
            });
            state.states = payload.payload._embedded.list;
        }
    },
    [types.REQUEST_GET_COUNTY_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload._embedded && payload.payload._embedded.list && payload.payload._embedded.list.length > 0) {
            state.counties = payload.payload._embedded.list;
        }
    },
    [types.REQUEST_GET_SERVICE_LIST_SUCCESS](state, payload) {
        //     state.services = (payload.payload.object.object.sort((a, b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0)));
        // }
        state.services = payload.payload
    },
    [types.REQUEST_GET_SERVICE_PAGE_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload && payload.payload.object) {
            state.totalElementsService = payload.payload.object.totalElements
            state.servicePage = payload.payload.object.object
        }
    },
    [types.REQUEST_GET_SERVICE_LIST_BY_SERVICE_PRICE_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload && payload.payload.length > 0) {
            state.services = payload.payload;
        }
    },
    [types.REQUEST_GET_PRICING_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload.object.object && payload.payload.object.object && payload.payload.object.object.length > 0) {
            state.pricingList = payload.payload.object.object;
        }
    },

    [types.REQUEST_GET_DASHBOARD_PRICING_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload.length > 0) {
            state.pricingDashboard = payload.payload;
            state.pricingByStates = false
        }
    },

    [types.REQUEST_GET_DOCUMENT_TYPE_PAGE_SUCCESS](state, payload) {
        state.totalElementsDocType = payload.payload.totalElements;
        state.docTypePage = payload.payload.content;
    },

    [types.REQUEST_GET_MAIN_SERVICE_LIST_SUCCESS](state, data) {
        if (
            data &&
            data.payload &&
            data.payload._embedded &&
            data.payload._embedded.list &&
            data.payload._embedded.list.length > 0
        ) {
            state.mainServicesMultiSelect = data.payload._embedded.list.map((item) => {
                return {value: item.id, label: item.name, online: item.online};
            });
            state.mainServices = data.payload._embedded.list.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
        }
    },
    [types.REQUEST_GET_SUB_SERVICE_PAGE_SUCCESS](state, payload) {
        if (payload && payload.payload) {
            state.totalElementsSub = payload.payload.totalElements;
            state.subServicePage = payload.payload.content;
        }
    },
    [types.REQUEST_GET_SUB_SERVICE_LIST_SUCCESS](state, payload) {
        if (
            payload &&
            payload.payload &&
            payload.payload._embedded &&
            payload.payload._embedded.list &&
            payload.payload._embedded.list.length > 0
        ) {
            state.subServices = payload.payload._embedded.list.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
        }
    },
    [types.REQUEST_GET_ZIP_CODE_LIST_SUCCESS](state, payload) {
        if (
            payload &&
            payload.payload &&
            payload.payload.object &&
            payload.payload.object &&
            payload.payload.object.length > 0
        ) {
            state.zipCodes = payload.payload.object;
        }
    },
    [types.REQUEST_GET_TIME_BOOKED_SUCCESS](state, payload) {
        if (
            payload &&
            payload.payload &&
            payload.payload._embedded &&
            payload.payload._embedded.list &&
            payload.payload._embedded.list.length > 0
        ) {
            state.timeBooked = payload.payload._embedded.list[0];
        }
    },
    [types.REQUEST_GET_ADDITIONAL_SERVICE_LIST_SUCCESS](state, payload) {
        if (
            payload &&
            payload.payload &&
            payload.payload._embedded &&
            payload.payload._embedded.list &&
            payload.payload._embedded.list.length > 0
        ) {
            state.additionalServices = payload.payload._embedded.list;
        }
    },
    [types.REQUEST_GET_ADDITIONAL_SERVICE_PAGE_SUCCESS](state, payload) {
        if (payload && payload.payload) {
            state.totalElementsAdditional = payload.payload.totalElements;
            state.additionalServicePage = payload.payload.content;
        }
    },
    [types.REQUEST_GET_ADDITIONAL_SERVICE_PRICE_LIST_SUCCESS](state, payload) {
        if (
            payload &&
            payload.payload &&
            payload.payload.object.object &&
            payload.payload.object.object &&
            payload.payload.object.object.length > 0
        ) {
            state.additionalServicePrices = payload.payload.object.object;
            // state.timeBooked = payload.payload._embedded.list;
        }
    },
    [types.REQUEST_GET_TIME_DURATION_SUCCESS](state, payload) {
        if (
            payload &&
            payload.payload &&
            payload.payload._embedded &&
            payload.payload._embedded.list &&
            payload.payload._embedded.list.length > 0
        ) {
            state.timeDuration = payload.payload._embedded.list[0];
        }
    },
    [types.REQUEST_GET_DISCOUNT_PERCENT_LIST_SUCCESS](state, payload) {
        if (
            payload &&
            payload.payload &&
            payload.payload._embedded &&
            payload.payload._embedded.list &&
            payload.payload._embedded.list.length > 0
        ) {
            state.discountPercents = payload.payload._embedded.list;
        }
    },
    [types.REQUEST_GET_DISCOUNT_PERCENT_PAGE_SUCCESS](state, payload) {
        if (payload && payload.payload) {
            state.totalElementsDisPer = payload.payload.totalElements;
            state.discountPercentPage = payload.payload.content;
        }
    },

    [types.REQUEST_GET_WEEK_DAY_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload._embedded && payload.payload._embedded.list) {
            state.weekDays = payload.payload._embedded.list;
        }
    },
    [types.REQUEST_GET_AGENT_SCHEDULE_LIST_SUCCESS](state, payload) {
        state.agentSchedules = payload.payload;
    },
    [types.CHANGE_ACTIVE](state) {
        state.active = !state.active;
    },
    [types.CHANGE_ONLINE](state, payload) {
        state.showOnlineModal = payload.payload.showOnlineModal;
        state.currentItem = payload.payload.currentItem;
    },
    [types.CHANGE_DYNAMIC](state, payload) {
        state.showDynamicModal = payload.payload.showDynamicModal;
        state.currentItem = payload.payload.currentItem;
    },
    [types.CHANGE_DEFAULT_INPUT](state, payload) {
        state.showDefaultInputModal = payload.payload.showDefaultInputModal;
        state.currentItem = payload.payload.currentItem;
    },
    [types.REQUEST_SUCCESS](state) {
        state.showDeleteModal = false;
        state.showEditModal = false;
        state.showStatusModal = false;
        state.showDefaultInputModal = false;
        state.showOnlineModal = false;
        state.showDynamicModal = false;
        state.showEmbassyModal = false;
        state.showModal = false;
        state.online = false;
        state.currentItem = "";
        state.text = "";
    },
    //START AUDIT HISTORY
    [types.REQUEST_GET_HISTORY_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload.object && payload.payload.totalElements) {
            state.histories = payload.payload.object;
            state.page = payload.payload.page;
            state.size = payload.payload.size;
            state.totalElements = payload.payload.totalElements;
            state.totalPages = payload.payload.totalPages;
        }
    },
    [types.REQUEST_GET_AUDIT_LIST_SUCCESS](state, payload) {
        state.histories = payload.payload;
    },
    [types.REQUEST_GET_AUDIT_ONE_LIST_SUCCESS](state, payload) {
        state.currentAuditItems = payload.payload;
        state.showModal = true;
    },
    [types.REQUEST_GET_USER_FOR_AUDIT](state, payload) {
        state.currentAuditItemUser = payload.payload;
    },
    [types.REQUEST_GET_AUDIT_ONE_ITEM_LIST_SUCCESS](state, payload) {
        state.histories = payload.payload;
    },
    [types.REQUEST_GET_AUDIT_ITEM_LIST_BY_AUTHOR_SUCCESS](state, payload) {
        state.histories = payload.payload;
    },
    [types.REQUEST_GET_AUDIT_TOTAL_ELEMENT_COUNT](state, payload) {
        state.totalElements = payload.payload;
    },
    [types.REQUEST_GET_HISTORY_TABLE_NAME_SUCCESS](state, payload) {
        state.historyTables = payload.payload;
    },
    [types.REQUEST_GET_AUDIT_TABLE_NAME_SUCCESS](state, payload) {
        state.auditTables = payload.payload;
    },
    [types.REQUEST_GET_AUDIT_ONE_TABLE](state, payload) {
        state.histories = payload.payload;
    },
    [types.CHANGE_SHOW_DROPDOWN](state, payload) {
        state.showDropdown = payload.payload.showDropdown;
    },
    //FINISH AUDIT HISTORY

    //START GET MAIN SERVICES WITH PERCENT
    [types.REQUEST_GET_MAIN_SERVICES_WITH_PERCENT_START](state) {
        state.loading = true;
    },
    [types.REQUEST_GET_MAIN_SERVICES_WITH_PERCENT_SUCCESS](state, payload) {
        if (
            payload &&
            payload.payload &&
            payload.payload._embedded &&
            payload.payload._embedded.list &&
            payload.payload._embedded.list.length > 0
        ) {
            state.mainServicesWithPercent = payload.payload._embedded.list.sort(
                (a, b) => (a.id > b.id ? 1 : b.id > a.id ? -1 : 0)
            );
        }
    },
    [types.REQUEST_GET_FEED_BACKS](state, payload) {
        if (payload && payload.payload && payload.payload.object) {
            state.feedBacks = payload.payload.object;
            state.size = payload.payload.size;
            state.page = payload.payload.page;
        }
    },
    [types.REQUEST_GET_MAIN_SERVICES_WITH_PERCENT_ERROR](state) {
        toast.error("Error! Please, try again!");
    },
    //FINISH GET MAIN SERVICES WITH PERCENT

    //START GET PUBLIC HOLIDAYS
    [types.REQUEST_GET_PUBLIC_HOLIDAY_START](state) {
        state.loading = true;
    },
    [types.REQUEST_GET_PUBLIC_HOLIDAY_SUCCESS](state, payload) {
        if (
            payload &&
            payload.payload &&
            payload.payload._embedded &&
            payload.payload._embedded.list &&
            payload.payload._embedded.list.length > 0
        ) {
            state.holidays = payload.payload._embedded.list.sort((a, b) =>
                a.id > b.id ? 1 : b.id > a.id ? -1 : 0
            );
            state.page = payload.payload.page.number;
            state.size = payload.payload.page.size;
            state.totalElements = payload.payload.page.totalElements;
            state.totalPages = payload.payload.page.totalPages;
        }
    },
    [types.REQUEST_GET_PUBLIC_HOLIDAY_ERROR](state) {
        toast.error("Error! Please, try again!");
    },

    //FINISH GET PUBLIC HOLIDAYS
    //START BLOG
    [types.REQUEST_GET_BLOG_SUCCESS](state, payload) {
        if (
            payload &&
            payload.payload &&
            payload.payload.object.object &&
            payload.payload.object.object &&
            payload.payload.object.object.length > 0
        ) {
            state.blogs = payload.payload.object.object;
        }
    },
    [types.REQUEST_GET_BLOG_FEATURED_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload.length > 0) {
            state.blogFeatureds = payload.payload;
        }
    },
    [types.REQUEST_GET_BLOG_BY_CATEGORY_SUCCESS](state, payload) {
        state.blogByCategory = payload.payload;
    },
    [types.REQUEST_GET_BLOG_URL_SUCCESS](state, payload) {
        // if (payload && payload.payload.length > 0) {
        state.blogByUrl = payload.payload;
        // }
    },
    [types.REQUEST_GET_BLOG_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload.totalElements) {
            state.totalElementsBlog = payload.payload.totalElements;
            state.blogsPage = payload.payload.object;
        }

    },
    [types.REQUEST_GET_DYNAMIC_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload.object && payload.payload.totalElements) {
            state.totalElementsDynamic = payload.payload.totalElements;
            state.dynamicPage = payload.payload.object;
        }
    },

    //FINISH BLOG

    [types.REQUEST_GET_CATEGORY_SUCCESS](state, payload) {
        if (
            payload &&
            payload.payload &&
            payload.payload._embedded &&
            payload.payload._embedded.list &&
            payload.payload._embedded.list.length > 0
        ) {
            state.categorys = payload.payload._embedded.list;
        }
    },
    [types.REQUEST_GET_TERMS_SUCCESS](state, payload) {
        state.terms = payload.payload;
    },

    // START GET SHARING DISCOUNT LIST
    [types.REQUEST_GET_SHARING_DISCOUNT_LIST_SUCCESS](state, payload) {
        if (payload.payload) {
            state.userSharingDiscountList = payload.payload.object;
            state.page = payload.payload.page;
            state.size = payload.payload.size;
            state.totalElements = payload.payload.totalElements;
            state.totalPages = payload.payload.totalPages;
        }
    },
    [types.REQUEST_GET_SHARING_DISCOUNT_LIST_START](state) {
        state.loading = true;
    },
    [types.REQUEST_GET_SHARING_DISCOUNT_LIST_ERROR](state) {
    },

    // FINISH GET SHARING DISCOUNT LIST

    [types.CHANGE_ADDITIONAL_BOOLEAN](state) {
        state.isAdditional = true;
        state.isAdditionalPrice = false;
        state.showModal = false;
    },
    [types.CHANGE_ADDITIONAL_PRICE_BOOLEAN](state) {
        state.isAdditional = false;
        state.isAdditionalPrice = true;
        state.showModal = false;
    },
    [types.CHANGE_MAIN_SERVICE_BOOLEAN](state) {
        state.isMainService = true;
        state.isMainServicePercent = false;
        state.showModal = false;
    },
    [types.CHANGE_MAIN_SERVICE_PERCENT_BOOLEAN](state) {
        state.isMainService = false;
        state.isMainServicePercent = true;
        state.showModal = false;
    },

    // Review
    [types.REQUEST_GET_REVIEW_START](state, payload) {
    },

    [types.REQUEST_GET_REVIEW_ERROR](state, payload) {
    },

    [types.REQUEST_GET_REVIEW_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload._embedded && payload.payload._embedded.list) {
            state.reviews = payload.payload._embedded.list;
            state.page = payload.payload.page.number;
            state.size = payload.payload.page.size;
            state.totalElements = payload.payload.page.totalElements;
            state.totalPages = payload.payload.page.totalPages;
        }
    },

    [types.REQUEST_GET_COUNTY_PRICING_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload.length > 0) {
            state.optionsCountyByPricing = payload.payload.map(item => {
                return {value: item.id, label: item.name}
            });
        }
    },
    [types.REQUEST_GET_ZIP_CODE_PRICING_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload.length > 0) {
            state.optionsZipCodeByPricing = payload.payload.map(item => {
                return {value: item.id, label: item.code}
            });
        }
    },
    [types.REQUEST_GET_STATE_PRICING_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload.length > 0) {
            state.optionsStateByPricing = payload.payload.map(item => {
                return {value: item.id, label: item.name}
            });
        }
    },
    [types.REQUEST_GET_STATE_BY_PRICING_LIST_SUCCESS](state, payload) {
        // if (payload && payload.payload.length > 0) {
        state.pricingByStates = payload.payload
        // }
    },
    //faq
    [types.REQUEST_GET_FAQ_SUCCESS](state, payload) {
        if (payload && payload.payload.object.length > 0) {
            state.faq = payload.payload.object
        }
    },

    updateState(state, {payload}) {
        return {
            ...state,
            ...payload,
        };
    },
};
export default createReducer(initState, reducers);
