import *  as types from "../actionTypes/AppActionTypes"
import {createReducer} from "../../utils/StoreUtils";

const initState = {
    loading: false,
    stateOptions: [],
    countyOptions: [],
    zipCodeOptions: [],
    text: null,
    showServiceModal: false,
    currentService: [],
    active: false,
    servicePrices: [],
    dashboard: [],
    all: false,
    selectZipCodes: null,
    selectCounties: null,
    selectStates: null,
}
const reducers = {
    [types.REQUEST_SERVICE_PRICE_START](state) {
        state.loading = true;
        state.text = null;
    },
    [types.REQUEST_GET_SERVICE_PRICE_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload.object && payload.payload.object && payload.payload.object.length > 0) {
            state.servicePrices = payload.payload.object;
        }
    },
    [types.REQUEST_GET_SERVICE_PRICE_DASHBOARD_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload && payload.payload && payload.payload.length > 0) {
            state.dashboard = payload.payload;
        }
    },
    [types.REQUEST_GET_SERVICE_PRICE_BY_SERVICE_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload && payload.payload && payload.payload.length > 0) {
            state.byServices = payload.payload;
        }
    },
    [types.REQUEST_GET_SERVICE_PRICE_BY_STATE_LIST_SUCCESS](state, payload) {
        // state.countyOptions.push(...payload.payload.map(item => {
        //     return {value: item.id, label: item.name}
        // }));
        if (payload && payload.payload && payload.payload.length > 0) {
            state.countyOptions = payload.payload.map(item => {
                return {value: item.id, label: item.name}
            });
        }
    },
    [types.REQUEST_GET_SERVICE_PRICE_BY_COUNTY_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload.length > 0) {
            state.zipCodeOptions = payload.payload.map(item => {
                return {value: item.id, label: item.code}
            });
        }
    },

    [types.REQUEST_SERVICE_PRICE_SUCCESS](state) {
        state.selectCounties = null;
        state.selectZipCodes = null;
        state.selectStates = null;
        state.stateOptions = null;
        state.countyOptions = null;
        state.zipCodeOptions = null;
        state.showDeleteModal = false;
        state.showStatusModal = false;
        state.showServiceModal = false;
        state.currentService = null;
        state.text = null;
    },
    [types.REQUEST_SERVICE_PRICE_ERROR](state) {
        state.selectCounties = null;
        state.selectZipCodes = null;
        state.selectStates = null;
    },

    updateState(state, {payload}) {
        return {
            ...state,
            ...payload
        }
    },
};
export default createReducer(initState, reducers);
