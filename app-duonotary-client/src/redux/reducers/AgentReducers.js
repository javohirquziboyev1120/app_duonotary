import *  as types from "../actionTypes/AgentActionTypes"
import {createReducer} from "../../utils/StoreUtils";
import {toast} from "react-toastify";
import * as agentTypes from "../actionTypes/AgentActionTypes";

const initState = {
    resMessage: '',
    agents:'',
    showStatusModal:false,
    showPhotoModal:false,
    showCertificateModal:false,
    isCertificate:false,
    currentAgent:'',
    statusEnums:["PENDING","RECEIVED","REJECTED"],
    passports:'',
    certificates:'',

}
const reducers = {
    [types.REQUEST_AGENT_START](state) {
    },
    [types.REQUEST_AGENT_SUCCESS](state) {
    },
    [types.REQUEST_AGENT_ERROR](state) {
    },
    [types.REQUEST_REGISTER_AGENT_SUCCESS](state, payload) {
        state.resMessage = payload.payload
        state.showModal = true

    },
    [types.PUSH_CERTIFICATE_ARRAY_FOR_REGISTER](state, payload) {
        state.certificates.push(payload.payload);
    },
    [types.REMOVE_CERTIFICATE_ROW](state, payload) {

        let list = state.certificates;
        list.map((item, i) => {
            if (item.id === payload.payload.id)
                list.splice(i, 1)
        })
    },
    updateState(state, {payload}) {
        return {
            ...state,
            ...payload
        }
    },
};
export default createReducer(initState, reducers);

