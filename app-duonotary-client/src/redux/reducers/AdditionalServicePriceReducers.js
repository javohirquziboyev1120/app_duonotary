import *  as serviceTypes from "../actionTypes/AppActionTypes"
import *  as types from "../actionTypes/AdditionalServicePriceActionType"
import {createReducer} from "../../utils/StoreUtils";
import {toast} from "react-toastify";

const initState = {
    loading: false,
    stateOptions: [],
    countyOptions: [],
    zipCodeOptions: [],
    text: null,
    showServiceModal: false,
    currentAdditionalService: '',
    active: false,
    additionalServicePrices: [],
    additionalDashboard: [],
    all: false,
    selectZipCodes: null,
    selectCounties: null,
    selectStates: null,
    showStatusModal: false,

}
const reducers = {
    [types.REQUEST_ADDITIONAL_SERVICE_PRICE_START](state) {
        state.loading = true;
        state.text = null;
    },
    [types.REQUEST_GET_ADDITIONAL_SERVICE_PRICE_LIST_SUCCESS](state, payload) {
        state.additionalServicePrices = payload.payload.object.object;
    },
    [types.REQUEST_GET_ADDITIONAL_SERVICE_PRICE_DASHBOARD](state, payload) {
        if (payload && payload.payload && payload.payload.length > 0) {
            state.additionalDashboard = payload.payload;
        }
    },
    [types.REMOVE_COUNTYARR_ZIPCODEARR](state) {
        state.countyOptions = [];
        state.zipCodeOptions = [];
    },
    [serviceTypes.REQUEST_GET_SERVICE_PRICE_BY_STATE_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload.length > 0) {
            state.countyOptions = payload.payload.map(item => {
                return {value: item.id, label: item.name}
            })
        }
    },
    [serviceTypes.REQUEST_GET_SERVICE_PRICE_BY_COUNTY_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload.length > 0) {
            state.zipCodeOptions = payload.payload.map(item => {
                return {value: item.id, label: item.code}
            });
        }
    },

    [types.REQUEST_ADDITIONAL_SERVICE_PRICE_SUCCESS](state) {
        state.selectCounties = null;
        state.selectZipCodes = null;
        state.selectStates = null;
        state.stateOptions = null;
        state.countyOptions = null;
        state.zipCodeOptions = null;
        state.showDeleteModal = false;
        state.showStatusModal = false;
        state.showServiceModal = false;
        state.currentAdditionalService = '';
        state.text = null;
    },
    [types.REQUEST_ADDITIONAL_SERVICE_PRICE_ERROR](state) {
        state.selectCounties = null;
        state.selectZipCodes = null;
        state.selectStates = null;
    },

    updateState(state, {payload}) {
        return {
            ...state,
            ...payload
        }
    },
};
export default createReducer(initState, reducers);
