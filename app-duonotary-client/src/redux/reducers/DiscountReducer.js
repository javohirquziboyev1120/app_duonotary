import {createReducer} from "../../utils/StoreUtils";
import * as types from "../actionTypes/DiscountTypes";
import {toast} from "react-toastify";

const initState = {
    loading: false,
    isTarifTab: true,
    isGettingTab: false,
    isExpenceTab: false,
    isTarifSharing: true,
    isTarifLoyalty: false,
    isDiscountPercent: false,
    isTariffCustom: false,
    isGettingSharing: false,
    isGettingLoyalty: false,
    isGettingCustomer: false,
    currentSharingDiscountTariff: '',
    currentLoyaltyDiscountTariff: '',
    loyaltyDiscountTariffList: [],
    sharingGivenArray: [],
    totalSharingGivenElements: 0,
    sharingGivenActivePage: 0,
    loyaltyGivenArray: [],
    totalLoyaltyGivenElements: 0,
    loyaltyGivenActivePage: 0,
    discountExpenseArray: [],
    totalDiscountExpenseElements: 0,
    discountExpenseActivePage: 0,
    customerDiscountArray: [],
    totalCustomerDiscountElements: 0,
    customerDiscountActivePage: 0,
    currentCustomDiscount: '',
    isSelectAllzipCodes: false,
    isZipCodeActive: false,
    currentCustomDiscountTariff: '',
    customDiscountTariffArray: [],
    customDiscountTariffClientArray: [],
    customDiscountTariffTotalElements: 0,
    customDiscountTariffActivePage: 0,
    zipCodeArray: [],
    customTariffUnlimited: false,
    customTariffActive: false,
    isTariffFirstOrder: false,
    currentFirstOrderDiscountTariff: '',
    isOnlineOrInPerson: false
}
const reducers = {
    [types.REQUEST_START](state) {
        state.loading = true
    },
    [types.REQUEST_ERROR](state) {
        // toast.error("Error")

    },
    [types.CHANGING_SHARING_TARIFF_STATUS](state) {

    },
    [types.GET_SHARING_DISCOUNT_TARIFF_SUCCESS](state, payload) {
        state.currentSharingDiscountTariff = payload.payload.object;
    },
    [types.GET_FIRST_ORDER_DISCOUNT_TARIFF_SUCCESS](state, payload) {
        state.currentFirstOrderDiscountTariff = payload.payload.object;
    },
    [types.GET_LOYALTY_DISCOUNT_TARIFF_SUCCESS](state, payload) {
        state.loyaltyDiscountTariffList = payload.payload;
    },
    [types.GET_ALL_SHARING_DISCOUNT_GIVEN_SUCCESS](state, payload) {

        state.sharingGivenArray = payload.payload.object
        state.totalSharingGivenElements = payload.payload.totalElements
        state.sharingGivenActivePage = payload.payload.page
    },
    [types.GET_ALL_LOYALTY_DISCOUNT_GIVEN_SUCCESS](state, payload) {
        // console.log(payload,"GET_ALL_LOYALTY_DISCOUNT_GIVEN_SUCCESS")
        state.loyaltyGivenArray = payload.payload.object
        state.totalLoyaltyGivenElements = payload.payload.totalElements
        state.loyaltyGivenActivePage = payload.payload.page
    },
    [types.GET_ALL_DISCOUNT_EXPENSE_SUCCESS](state, payload) {
        state.discountExpenseArray = payload.payload.object
        state.totalDiscountExpenseElements = payload.payload.totalElements
        state.discountExpenseActivePage = payload.payload.page
    },
    [types.GET_ALL_CUSTOMER_DISCOUNT_SUCCESS](state, payload) {
        // console.log(payload,"GET_ALL_LOYALTY_DISCOUNT_GIVEN_SUCCESS")
        state.customerDiscountArray = payload.payload.object
        state.totalCustomerDiscountElements = payload.payload.totalElements
        state.customerDiscountActivePage = payload.payload.page
    },
    [types.GET_ALL_CUSTOM_DISCOUNT_TARIFF](state, payload) {
        // console.log(payload,"GET_ALL_LOYALTY_DISCOUNT_GIVEN_SUCCESS")
        state.customDiscountTariffArray = payload.payload.object
        state.customDiscountTariffTotalElements = payload.payload.totalElements
        state.customDiscountTariffActivePage = payload.payload.page
    },
    [types.GET_ALL_CLIENTS_BY_STATE_OR_COUNTY_OR_ZIP_CODE](state, payload) {
        state.customDiscountTariffClientArray = payload.payload
    },
    [types.GET_ALL_ZIP_CODE_CLIENTS_BY_COUNTY](state, payload) {
        state.zipCodeArray = payload.payload
    },
    updateState(state, {payload}) {
        return {
            ...state,
            ...payload
        }
    },
};
export default createReducer(initState, reducers);