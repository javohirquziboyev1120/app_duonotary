import * as types from "../actionTypes/OrderActionTypes";
import {createReducer} from "../../utils/StoreUtils";
import {REQUEST_ATTACHMENT_START, REQUEST_ATTACHMENT_SUCCESS} from "../actionTypes/AttachmentActionTypes";
import {
    AUTH_GET_CURRENT_USER_REQUEST,
    AUTH_GET_USER_TOKEN_SUCCESS,
    AUTH_LOGIN_USER_SUCCESS,
    AUTH_REGISTER_USER_SUCCESS,
    REQUEST_AUTH_START
} from "../actionTypes/AuthActionTypes";
import {ORDER} from "../../utils/constants";

let initState = {
    certificate: '',
    loading: false,
    step: 0,
    stage: 0,
    status: ['DRAFT', 'NEW', 'IN_PROGRESS', 'COMPLETED', 'CLOSED', 'REJECTED', 'CANCELLED'],
    error: '',
    replay: false,
    message: '',
    address: '',
    lat: '',
    lng: '',
    zipCode: '',
    code: '',
    zipCodes: [],
    servicePrice: '',
    servicePrices: [],
    additionalServicePrices: [],
    orderAdditionalServices: [],
    timeTable: '',
    payType: '',
    payTypes: [],
    username: '',
    password: '',
    countDocument: 0,
    selectedDate: '',
    dates: [],
    time: '',
    alert_mes: '',
    times: [],
    timesPercent: [],
    allAmount: '',
    discount: '',
    payment: '',
    pricingList: [],
    order: false,
    timeZone: '',
    modal: false,
    ordersList: [],
    orders: null,
    allDiscounts: null,
    ordersForClientMainPage: '',
    cancelAmount: 0,
    amount: -1,
    discountCount: [],
    discountNeed: false,
    currentOrder: '',
    checkbox1: false,
    checkbox2: false,
    checkbox3: false,
    titleDocument: '',
    now: true,
    outOfService: false,
    searchUser: [],
    searchAgent: [],
    isApostille: true,
    isEmbassy: true,
    currentService: "",
    realEstateDto: null,
    internationalDto: null,
    openCollapse: false,
    clientSecret: '',
    documents: '',
    orderState: '',
    openEditModal: false,
    currentUser: '',
    signUp: false,
    update: 'main',
    attachmentIdsArray: [],
    stripe: false,
    street_address: false,
    previousStep: ''
}

let reducers = {
    [types.REQUEST_START || AUTH_GET_CURRENT_USER_REQUEST](state) {
        state.loading = true
        state.replay = false
    },
    [AUTH_GET_USER_TOKEN_SUCCESS](state) {
        state.loading = false
    },

    [types.REQUEST_GET_ORDER_LIST_SUCCESS](state, payload) {
        state.ordersList = payload.payload.object
    },
    [types.SHOW_MODAL](state, payload) {
        state.modal = payload.payload
    },
    [types.CHANGE_STEP](state, payload) {
        state.step = payload.payload
    },
    [types.REQ_ALERT](state, payload) {
        state.alert_mes = payload.payload
    },
    [types.REQUEST_GET_ZIP_CODE_BY_CODE](state, payload) {
        state.zipCodes = payload.payload.object
    },
    [types.CHANGE_ZIP_CODES](state, payload) {
        state.zipCodes = []
        state.zipCode = payload.payload
    },
    [types.REQUEST_GET_SERVICE_PRICE_ZIP_CODE_ID](state, payload) {
        let servicePrice = []
        payload.payload.map(item => {
            item.itemActive = false
            servicePrice.push(item);
        })
        state.servicePrices = servicePrice
    },
    [types.SET_SERVICE_PRICE](state, payload) {
        state.servicePrice = payload.payload;
    },
    [types.SET_ORDER_ADD_SERVICE_PRICE](state, payload) {
        state.orderAdditionalServices = payload.payload;
    },
    [types.REQUEST_GET_ADDITIONAL_SERVICE_PRICES](state, payload) {
        state.additionalServicePrices = payload.payload.object;
    },
    [types.REQUEST_GET_CURRENT_AGENT](state, payload) {
        state.timeTable = payload.payload.object;
    },
    [types.CHANGE_DATE](state, payload) {
        state.selectedDate = payload.payload
    },
    [types.SET_TIME](state, payload) {
        state.time = payload.payload
    },
    [types.SET_ERROR](state, payload) {
        state.error = payload.payload
    },
    [types.REQUEST_POST_TIME_TABLE](state, payload) {
        state.timeTable = payload.payload.object;
    },
    [types.SET_PAY_TYPE](state, payload) {
        state.payType = payload.payload
    },
    [types.SET_COUNT_DOC](state, payload) {
        state.countDocument = payload.payload
    },
    [types.SET_ADDRESS](state, payload) {
        state.address = payload.payload
    },
    [types.SET_DISCOUNT](state, payload) {
        state.discount = payload.payload
    },
    [types.REQUEST_GET_TIMES_BY_DATE](state, payload) {
        state.times = payload.payload.times;
        state.timesPercent = payload.payload.timesPercent;
        state.date = payload.payload.date;
    },
    [types.REQUEST_GET_PAY_TYPE](state, payload) {
        state.payTypes = payload.payload._embedded.list;
    },
    [types.REQUEST_GET_PRICING_BY_SERVICE_PRICE](state, payload) {
        state.pricingList = payload.payload;
    },
    [types.REQUEST_GET_ORDER_PRICE_AMOUNTS](state, payload) {
        state.allDiscounts = [];
        if (payload.payload && payload.payload.length > 0 && payload.payload[0].allAmount) {
            state.allDiscounts = payload.payload;
            state.allAmount = payload.payload[0].allAmount;
            for (let i = 0; i < state.allDiscounts.length; i++) {
                state.discountCount = [];
                if (state.allDiscounts[i].discountName === "Discount for first order") {
                    state.discountCount.push(state.allDiscounts[i]);
                }
            }
        }
        localStorage.setItem(ORDER, JSON.stringify({
            ...JSON.parse(localStorage.getItem(ORDER)),
            allDiscounts: state.allDiscounts,
            allAmount: state.allAmount,
            discountCount: state.discountCount
        }));
    },
    [types.REQUEST_GET_ORDER_PRICE_AMOUNT](state, payload) {
        state.amount = 0;
        state.amount = payload.payload;
    },
    [types.REQUEST_GET_AMOUNT_OF_CANCEL](state, payload) {
        state.cancelAmount = payload.payload.object;
    },
    [types.REQUEST_GET_USER_LIST_BY_EMAIL](state, payload) {
        state.searchUser = payload.payload;
    },
    [types.REQUEST_POST_ORDER](state, payload) {
        state.order = payload.payload.object;
    },
    [types.REQUEST_POST_PAYMENT](state, payload) {
        state.payment = payload.payload.object;
    },
    [types.CHANGE_SHOW_DROPDOWN](state, payload) {
        state.showDropdown = payload.payload.showDropdown
    },
    [types.CHANGE_ORDER](state, payload) {
        state.order = payload.payload
    },
    [types.CHANGE_CURRENT_ORDER](state, payload) {
        state.currentOrder = payload.payload
        state.modal = !!payload.payload
    },
    [types.REQUEST_GET_SERVICE_PRICE_ONLINE](state, payload) {
        state.servicePrices = payload.payload
    },
    [types.REQUEST_GET_ORDER_FOR_ADMIN](state, payload) {
        state.orders = payload.payload.object;
        state.modal = false;
    },
    [types.REQUEST_ERROR_SERVICE_PRICE](state, payload) {
        state.loading = true;
        state.error = payload.payload
    },
    [types.REQUEST_GET_ORDERS_FOR_CLIENT_MAIN_PAGE](state, payload) {
        state.ordersForClientMainPage = payload.payload
    },
    [types.SET_LAT_LNG](state, payload) {
        state.lat = payload.payload.lat;
        state.lng = payload.payload.lng
    },
    [types.CLEAN_STATE](state, payload) {
        state.lat = payload.payload.lat;
        state.lng = payload.payload.lng
    },
    [types.REQUEST_GET_FREE_AGENT_AND_TIME_TABLE](state, payload) {
        state.timeTable = payload.payload;
        state.payload = false;
    },
    [types.REQUEST_GET_AGENT_LIST](state, payload) {
        state.searchAgent = payload.payload;
        state.payload = false;
    },
    [types.REQUEST_DELETE_ORDER](state) {
        state.modal = false;
        state.currentOrder = '';
    },
    [types.CHANGE_STAGE](state, payload) {
        state.stage = payload.payload;
    },
    [REQUEST_ATTACHMENT_START](state) {
        state.loading = true;
    },
    [REQUEST_ATTACHMENT_SUCCESS](state) {
        state.countDocument=state.countDocument+1
    },
    [REQUEST_AUTH_START](state) {
        state.loading = true;
    },
    [AUTH_LOGIN_USER_SUCCESS || AUTH_REGISTER_USER_SUCCESS](state) {
    },
    updateState(state, {payload}) {
        return {
            ...state,
            ...payload
        }
    },
    updateStateOrder(state, {payload}) {
        let array = []
        if (state.attachmentIdsArray && state.attachmentIdsArray[0]) {
            state.attachmentIdsArray.map(item => {
                array.push(item)
            })
        }
        let order;
        if (payload) {
            if ((payload.step || payload.stage) && !payload.previousStep) {
                order = {previousStep: {...state}}
            }

        }
        if (payload.src) {
            array.push(payload)
            order = {...state, attachmentIdsArray: array, ...order}
        } else {
            order = {...state, ...payload, ...order}
        }
        localStorage.setItem(ORDER, JSON.stringify(order))
        return order
    },
    updateStateBack(state, {payload}) {
        return {
            ...state,
            ...payload
        }
    },
    updateOrderState(state, payload) {
        return {
            ...state,
            orderState: state,
            ...payload.payload
        }
    },
    backOrderState(state) {
        if (state.orderState)
            return {
                ...state.orderState
            }
    },
    previousStep() {
        let order = JSON.parse(localStorage.getItem(ORDER))
        localStorage.setItem(ORDER, JSON.stringify({...order.previousStep, timeTable: order.timeTable}))
        return {
            ...order.previousStep,
            timeTable: order.timeTable
        }
    },

    resetState(){
        localStorage.removeItem(ORDER)
        return {
            certificate: '',
            loading: false,
            step: 0,
            stage: 0,
            status: ['DRAFT', 'NEW', 'IN_PROGRESS', 'COMPLETED', 'CLOSED', 'REJECTED', 'CANCELLED'],
            error: '',
            replay: false,
            message: '',
            address: '',
            lat: '',
            lng: '',
            zipCode: '',
            code: '',
            zipCodes: [],
            servicePrice: '',
            servicePrices: [],
            additionalServicePrices: [],
            orderAdditionalServices: [],
            timeTable: '',
            payType: '',
            payTypes: [],
            username: '',
            password: '',
            countDocument: 0,
            selectedDate: '',
            dates: [],
            time: '',
            times: [],
            timesPercent: [],
            allAmount: '',
            discount: '',
            payment: '',
            pricingList: [],
            order: false,
            timeZone: '',
            allDiscounts: null,
            ordersForClientMainPage: '',
            cancelAmount: 0,
            amount: -1,
            discountCount: [],
            discountNeed: false,
            currentOrder: '',
            checkbox1: false,
            checkbox2: false,
            checkbox3: false,
            titleDocument: '',
            now: true,
            outOfService: false,
            isApostille: true,
            isEmbassy: true,
            realEstateDto: null,
            internationalDto: null,
            openCollapse: false,
            clientSecret: '',
            documents: '',
            orderState: '',
            openEditModal: false,
            currentUser: '',
            signUp: false,
            update: 'main',
            attachmentIdsArray: [],
            stripe: false,
            street_address: false,
            previousStep: '',
            alert_mes: '',
        }
    }

}

export default createReducer(initState, reducers);