import {createReducer} from "../../utils/StoreUtils";
import * as types from "../actionTypes/AdminActionTypes";


const initState = {
    loading: false,
    currentUser: null,
    admins: null,
    isAdminUser: false,
    isClient: false,
    isAgent: false,
    isTable: true,
    permissions: null,
    active: false,
    currentItem: null,
    permissionsId: null,
    bigPermissionsId: null,
    showModal: false,
    stateOptions: null,
    countyOptions: [],
    zipCodeOptions: null,
    selectZipCodes: null,
    selectCounties: null,
    selectStates: null,
    adminZipCodes: '',
    all: null,
    page: 0,
    size: 0,
    totalElements: 0,
    totalPages: 0,
    showStatusModal: false,
    showDeleteModal: false,
    showEditModal: false,
    showInfoModal: false,
    showZipCodeModal: false,
    services: null,
    clients: null,
    agents: null,
    newPermissions: null,
    payments: [],
    orders: [],
    pinPayType: false,
    selectedOrder: {},
    usersAndOrdersCount: [],
    ordersCash: [],
    ordersForDashboard: [],
};

const reducers = {

    //START PERMISSION LIST

    [types.REQUEST_START](state) {
        state.loading = true;
    },
    [types.REQUEST_SUCCESS](state) {
        state.showModal = false;
        state.showPerModal = false;
        state.showEditModal = false;
        state.showZipCodeModal = false;
    },
    [types.REQUEST_ERROR](state) {
    },
    [types.REQUEST_GET_PERMISSION_LIST_SUCCESS](state, payload) {
        state.permissions = payload.payload;
    },
    [types.REQUEST_GET_NEW_PERMISSION_LIST_SUCCESS](state, payload) {
        state.newPermissions = payload.payload;
    },
    [types.REQUEST_GET_PERMISSION_LIST_ERROR](state) {
        state.loading = true;
    },
    [types.REQUEST_GET_PERMISSION_LIST_START](state) {
        state.loading = true;
    },
    //FINISH PERMISSION LIST

    [types.REQUEST_GET_CLIENT_LIST_SUCCESS](state, payload) {
        state.clients = payload.payload
    },
    [types.REQUEST_GET_AGENT_LIST_SUCCESS](state, payload) {
        state.agents = payload.payload
    },
    //START ADMIN CHANGE ENABLED
    [types.REQUEST_GET_ADMIN_BY_ENABLED_START](state) {
        state.loading = true;
    },
    [types.REQUEST_GET_ADMIN_BY_ENABLED_ERROR](state) {
    },
    [types.REQUEST_GET_ADMIN_BY_ENABLED_SUCCESS](state) {
        state.showStatusModal = false;
        state.currentItem = null
    },
    //FINISH ADMIN CHANGE ENABLED

    //START GET SERVICES FOR ADMIN
    [types.REQUEST_GET_SERVICES_START](state) {
        state.loading = true;
    },
    [types.REQUEST_GET_SERVICES_ERROR](state) {
    },
    [types.REQUEST_GET_SERVICES_SUCCESS](state, payload) {
        state.services = payload.payload.object.object;
        state.page = payload.payload.object.page;
        state.size = payload.payload.object.size;
    },
    //FINISH GET SERVICES FOR ADMIN

    [types.CHANGE_ADMIN_BOOLEAN](state) {
        state.isAdminUser = true;
        state.isClient = false;
        state.isTable = false;
        state.isAgent = false;
    },
    [types.CHANGE_AGENT_BOOLEAN](state) {
        state.isAdminUser = false;
        state.isClient = false;
        state.isTable = false;
        state.isAgent = true;
    },
    [types.CHANGE_CLIENT_BOOLEAN](state) {
        state.isAdminUser = false;
        state.isClient = true;
        state.isTable = false;
        state.isAgent = false;
    },
    [types.CHANGE_TABLE_BOOLEAN](state) {
        state.isAdminUser = false;
        state.isClient = false;
        state.isTable = true;
        state.isAgent = false;
    },
    //START ADMIN LIST
    [types.REQUEST_GET_USER_COUNT](state, payload) {
        state.totalItems = payload.payload;
    },
    [types.REQUEST_GET_ADMIN_LIST_SUCCESS](state, payload) {
        state.admins = payload.payload;
    },
    [types.REQUEST_GET_AGENT_LIST_SUCCESS](state, payload) {
        state.agents = payload.payload;
    },
    [types.REQUEST_GET_CLIENT_LIST_SUCCESS](state, payload) {
        state.clients = payload.payload;
    },
    [types.REQUEST_GET_ADMIN_LIST_ERROR](state) {
    },
    [types.REQUEST_GET_ADMIN_LIST_START](state) {
        state.loading = true;
    },
    //FINISH ADMIN LIST

    [types.REQUEST_GET_USER_ERROR](state) {
    },
    [types.REQUEST_GET_USER_START](state) {
        state.loading = true;
    },
    [types.REQUEST_GET_USER_SUCCESS](state, payload) {
        // if (payload.payload.roles[0].roleName === "ROLE_SUPER_ADMIN") {
        state.currentUser = payload;
        // } else {
        //     window.location.replace('/login')
        // }
        // getQrCodeForClient({id: state.currentUser.id})

    },


    [types.REQUEST_GET_STATE_LIST_START](state) {
        state.loading = true;
    },
    [types.REQUEST_GET_STATE_LIST_ERROR](state) {
    },
    [types.REQUEST_GET_STATE_LIST_SUCCESS](state, payload) {
        if (payload && payload.payload && payload.payload._embedded && payload.payload._embedded.list && payload.payload._embedded.list.length > 0) {
            state.stateOptions = payload.payload._embedded.list.map(item => {
                return {value: item.id, label: item.name}
            });
            state.states = payload.payload._embedded.list;
            // stateOptions=[...stateOptions,...payload.payload._embedded.list.map(item=>{return {value:item.id, label:item.name}})]
        }
    },


    // GET STATE LIST FOR ADD ADMIN
    [types.REQUEST_GET_ADMIN_BY_STATE_LIST_START](state) {
        state.loading = true;
    },
    [types.REQUEST_GET_ADMIN_BY_STATE_LIST_SUCCESS](state, payload) {
        state.stateOptions = [];
        if (payload && payload.payload && payload.payload._embedded && payload.payload._embedded.list && payload.payload._embedded.list.length > 0) {
            state.stateOptions = payload.payload._embedded.list.map(item => {
                return {value: item.id, label: item.name}
            });
        }
    },
    [types.REQUEST_GET_ADMIN_BY_STATE_LIST_ERROR](state) {
    },

    // GET COUNTY LIST FOR ADD ADMIN
    [types.REQUEST_GET_ADMIN_BY_COUNTY_LIST_START](state) {
        state.loading = true;
    },
    [types.REQUEST_GET_ADMIN_BY_COUNTY_LIST_ERROR](state) {
    },
    [types.REQUEST_GET_ADMIN_BY_COUNTY_LIST_SUCCESS](state, payload) {
        state.selectCounties = null;
        state.countyOptions = [];
        if (payload && payload.payload && payload.payload.length > 0) {
            state.countyOptions.push(...payload.payload.map(item => {
                return {value: item.id, label: item.name}
            }));
        }
    },

    // GET ZIPCODE LIST FOR ADD ADMIN
    [types.REQUEST_GET_ADMIN_BY_ZIPCODE_LIST_START](state) {
        state.loading = true;
    },
    [types.REQUEST_GET_ADMIN_BY_ZIPCODE_LIST_SUCCESS](state, payload) {
        state.zipCodeOptions = [];
        if (payload && payload.payload && payload.payload.length > 0) {
            state.zipCodeOptions.push(...payload.payload.map(item => {
                return {value: item.id, label: item.code}
            }));
        }
    },
    [types.REQUEST_GET_ADMIN_BY_ZIPCODE_LIST_ERROR](state) {
    },
    [types.REQUEST_GET_ADMIN_ZIP_CODE](state, payload) {
        if (payload && payload.payload && payload.payload.length > 0) {
            state.adminZipCodes = payload.payload.sort((a, b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0));
        }
    },
    // GET Payment for admin
    [types.REQUEST_GET_ADMIN_BY_PAYMENT_START](state) {
        state.loading = true;
    },
    [types.REQUEST_GET_ADMIN_BY_PAYMENT_SUCCESS](state, {payload}) {
        state.payments = payload.object;
        state.totalElements = payload.totalElements;
        state.totalPages = payload.totalPages;
        state.size = payload.size;
        state.page = payload.page;
    },
    [types.REQUEST_GET_ADMIN_BY_PAYMENT_ERROR]() {
    },
    [types.REQUEST_SEARCH_ORDER_FOR_PAYMENT_SUCCESS](state, payload) {
        state.orders = payload.payload.object
    },
    [types.REQUEST_TOTAL_COUNTS_OF_ORDER_AND_USER_START]() {
    },
    [types.REQUEST_TOTAL_COUNTS_OF_ORDER_AND_USER_ERROR](state, payload) {
    },
    [types.REQUEST_TOTAL_COUNTS_OF_ORDER_AND_USER_SUCCESS](state, payload) {
        state.usersAndOrdersCount = payload.payload.object
    },

    [types.REQUEST_ORDER_CASH_FOR_DASHBOARD_START]() {
    },
    [types.REQUEST_ORDER_CASH_FOR_DASHBOARD_ERROR]() {
    },
    [types.REQUEST_ORDER_CASH_FOR_DASHBOARD_SUCCESS](state, payload) {
        state.ordersCash = payload.payload.object
    },

    [types.REQUEST_ADMIN_DASHBOARD_START]() {
    },
    [types.REQUEST_ADMIN_DASHBOARD_ERROR]() {
    },
    [types.REQUEST_ADMIN_DASHBOARD_SUCCESS](state, payload) {
        state.ordersForDashboard = payload.payload.object
    },

    updateState(state, {payload}) {
        return {
            ...state,
            ...payload
        }
    }
};
export default createReducer(initState, reducers);
