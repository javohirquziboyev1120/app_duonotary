import {combineReducers} from "redux";
import auth from "./AuthReducer";
import agent from "./AgentReducers";
import attachment from "./AttachmentReducers";
import app from "./AppReducers";
import admin from "./AdminReducers";
import share from "./SharingReducers";
import {routerReducer} from "react-router-redux";
import service from "./ServicePriceReducers";
import additionalServicePrice from "./AdditionalServicePriceReducers";
import order from "./OrderReducer";
import zipCode from "./ZipCodeReducer"
import discount from "./DiscountReducer"


export const rootReducer = combineReducers({
    router:routerReducer,
    auth,
    admin,
    app,
    order,
    share,
    service,
    agent,
    attachment,
    zipCode,
    additionalServicePrice,
    discount,
})