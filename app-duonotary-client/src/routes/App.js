import React from "react";
import {Route, Switch} from "react-router-dom";
import NotFound from "../pages/NotFound";
import Cabinet from "../pages/Cabinet";
import Country from "../pages/adminPages/Country";
import State from "../pages/State";
import County from "../pages/County";
import ServicePrice from "../pages/ServicePrice";
import Service from "../pages/Service";
import MainService from "../pages/MainService";
import SubService from "../pages/SubService";
import ZipCode from "../pages/ZipCode";
import AdditionalService from "../pages/AdditionalService";
import TimeBooked from "../pages/TimeBooked";
import TimeDuratiion from "../pages/TimeDuratiion";
import DiscountPercent from "../pages/DiscountPercent";
import AgentSchedule from "../pages/AgentSchedule";
import Schedule from "../pages/agentPage/AgentSchedule";
import WeekDay from "../pages/WeekDay";
import History from "../pages/History";
import InPerson from "../pages/clientPage/InPersonOrder";
import InPersonOrder from "../pages/clientPage/InPersonOrder";
import Passport from "../pages/Passport";
import RegisterAgent from "../pages/RegisterAgent";
import SharingClient from "../pages/SharingClient";
import AgentList from "../pages/AgentList";
import AgentProfile from "../pages/AgentProfile";
import store from "../redux";
import {Provider} from "react-redux";
import PublicRoute from "../utils/PublicRoute";
import PrivateRoute from "../utils/PrivateRoute";
import Online from "../pages/clientPage/OnlineOrder";
import OnlineOrder from "../pages/clientPage/OnlineOrder";
import DownloadPdf from "../pages/order/DownloadPdf";
import Customer from "../pages/Customer";
import DashBoard from "../pages/DashBoard";
import FeedBack from "../pages/FeedBack";
import Discount from "../pages/Discount";
import MainPage from "../pages/clientPage/MainPage";
import Order from "../pages/clientPage/Order";
import Sharing from "../pages/clientPage/Sharing";
import Setting from "../pages/clientPage/Setting";
import ClientFeedback from "../pages/clientPage/Feedback";
import ForgotPassword from "../pages/ForgotPassword";
import ConfirmCode from "../pages/ConfirmCode";
import ResetPassword from "../pages/ResetPassword";
import AdminMain from "../pages/adminPages/AdminMain";
import AdminOrder from "../pages/adminPages/AdminOrder";
import Agents from "../pages/adminPages/Agents";
import Blogs from "../pages/adminPages/Blogs";
import Customers from "../pages/adminPages/Customers";
import Discounts from "../pages/adminPages/Discounts";
import Feedback from "../pages/adminPages/Feedback";
import MainServices from "../pages/adminPages/MainService";
import Services from "../pages/adminPages/Service";
import ServicePrices from "../pages/adminPages/ServicePrice";
import Pricings from "../pages/adminPages/Pricing";
import AdditionalServices from "../pages/adminPages/AdditionalService";
import ZipCodes from "../pages/adminPages/ZipCodes";
import MainSetup from "../pages/MainSetup";
import Holiday from "../pages/adminPages/Holiday";
import Audit from "../pages/Audit";
import AdminAudit from "../pages/adminPages/Audit";
import HomePage from "../pages/HomePage";
import Hawtosay from "../pages/Hawtosay";
import AuditForItem from "../pages/adminPages/AuditForItem";

import AgentMain from "../pages/agentPage/AgentMain";
import AgentOrders from "../pages/agentPage/Orders";
import AgentPassports from "../pages/agentPage/Passports";
import AgentCertificates from "../pages/agentPage/Certificates";
import AgentFeedbacks from "../pages/agentPage/Feedbacks";
import AgentSharing from "../pages/agentPage/Sharing";
import AgentSettings from "../pages/agentPage/Settings";
import DocType from "../pages/adminPages/DocumentType";
import PayType from "../pages/adminPages/PayType";
import Category from "../pages/adminPages/Category";
import Time from "../pages/adminPages/Time"
import AuditByAuthor from "../pages/adminPages/AuditByAuthor";
import Admins from "../pages/adminPages/Admin";
import VerifyEmail from "../pages/VerifyEmail";
import BlogPage from "../pages/BlogPage";
import OrderInPerson from "../pages/OrderInPerson";
import OrderOnline from "../pages/OrderOnline";
import Register from "../pages/Register";
import Terms from "../pages/Terms";
import SingleBlogPost from "../pages/SingleBlogPost";
import DynamicPage from "../pages/adminPages/DynamicPage";
import AboutPage from "../components/common/AboutPage";
import Partners from "../pages/adminPages/Partners/Partners";
import Calendar from "../pages/adminPages/Calendar";
import Payment from "../pages/adminPages/Payment";
import ZipCodeAgent from '../pages/agentPage/ZipCode'
import Review from "../pages/adminPages/Review";
// import RealEstateComponent from "../components/RealEstateComponent";
import Faq from "../pages/FAQ";
import AdminFaq from "../pages/adminPages/AdminFAQ";
import ApostilleComponent from "../components/ApostilleComponent";

const App = () => {
    return (
        <Provider store={store}>
            <Switch>
                <PublicRoute exact path="/" component={HomePage}/>
                <PublicRoute exact path="/inPerson" component={InPersonOrder}/>
                <PublicRoute exact path="/online" component={Online}/>
                <PublicRoute exact path="/forgotPassword" component={ForgotPassword}/>
                <PublicRoute exact path="/registerAgent" component={RegisterAgent}/>
                <PublicRoute exact path="/confirmCode/:username" component={ConfirmCode}/>
                <PublicRoute exact path="/resetPassword/:emailCode" component={ResetPassword}/>
                <PublicRoute exact path="/blog" component={BlogPage}/>
                <PublicRoute exact path="/havetosay" component={Hawtosay}/>
                <PublicRoute exact path="/verifyEmail" component={VerifyEmail}/>
                <PublicRoute exact path="/register/:id" component={Register}/>
                <PublicRoute exact path="/register" component={Register}/>
                <PublicRoute exact path="/termsofservices" component={Terms}/>
                <PublicRoute exact path="/notificationsettings" component={Terms}/>
                <PublicRoute exact path="/privacypolicy" component={Terms}/>
                <PublicRoute exact path="/blog/:title" component={SingleBlogPost}/>
                <PublicRoute exact path="/inPersonOrder" component={OrderInPerson}/>
                <PublicRoute exact path="/onlineOrder" component={OrderOnline}/>
                <PublicRoute exact path="/about" component={AboutPage}/>
                <PublicRoute exact path="/faq" component={Faq} />

                <PrivateRoute exact path="/admin/audit/:id" component={AuditForItem}/>
                <PrivateRoute exact path="/agent" component={AgentMain}/>
                <PrivateRoute exact path="/agent/zipcode" component={ZipCodeAgent}/>
                <PrivateRoute exact path="/agent/orders" component={AgentOrders}/>
                <PrivateRoute
                    exact
                    path="/agent/passports"
                    component={AgentPassports}
                />
                <PrivateRoute
                    exact
                    path="/agent/certificates"
                    component={AgentCertificates}
                />
                <PrivateRoute
                    exact
                    path="/agent/feedbacks"
                    component={AgentFeedbacks}
                />
                <PrivateRoute exact path="/agent/sharing" component={AgentSharing}/>
                <PrivateRoute exact path="/agent/settings" component={AgentSettings}/>
                <PrivateRoute exact path="/agent/schedule" component={Schedule}/>

                <PrivateRoute exact path="/client/online" component={OnlineOrder}/>
                <PrivateRoute exact path="/client/inPerson" component={InPersonOrder}/>
                <PrivateRoute exact path="/client/main-page" component={MainPage}/>
                <PrivateRoute exact path="/client/order" component={Order}/>
                <PrivateRoute exact path="/client/sharing" component={Sharing}/>
                <PrivateRoute exact path="/client/setting" component={Setting}/>
                <PrivateRoute
                    exact
                    path="/client/feedback"
                    component={ClientFeedback}
                />
                <PrivateRoute exact path="/admin" component={AdminMain}/>
                <PrivateRoute exact path="/admin/calendar" component={Calendar}/>
                <PrivateRoute exact path="/admin/order" component={AdminOrder}/>
                <PrivateRoute exact path="/admin/agent" component={Agents}/>
                <PrivateRoute exact path="/admin/blog" component={Blogs}/>
                <PrivateRoute exact path="/admin/customer" component={Customers}/>
                <PrivateRoute exact path="/admin/discount" component={Discounts}/>
                <PrivateRoute exact path="/admin/feedback" component={Feedback}/>
                <PrivateRoute exact path="/admin/faq" component={AdminFaq} />
                <PrivateRoute
                    exact
                    path="/admin/mainService"
                    component={MainServices}
                />
                <PrivateRoute exact path="/admin/service" component={Services}/>
                <PrivateRoute
                    exact
                    path="/admin/servicePrice"
                    component={ServicePrices}
                />
                <PrivateRoute exact path="/admin/pricing" component={Pricings}/>
                <PrivateRoute
                    exact
                    path="/admin/additionalService"
                    component={AdditionalServices}
                />
                <PrivateRoute exact path="/admin/documentType" component={DocType}/>
                <PrivateRoute exact path="/admin/time" component={Time}/>
                <PrivateRoute exact path="/admin/payType" component={PayType}/>
                <PrivateRoute exact path="/admin/blog/category" component={Category}/>
                <PrivateRoute exact path="/admin/dynamic" component={DynamicPage}/>


                <PrivateRoute exact path="/admin/zipCodes" component={ZipCodes}/>
                <PrivateRoute exact path="/orderInPerson" component={InPerson}/>
                <PrivateRoute exact path="/orderOnline" component={Online}/>
                <PrivateRoute path="/cabinet" exact component={Cabinet}/>
                <PrivateRoute exact path="/admin/country" component={Country}/>
                <PrivateRoute exact path="/state" component={State}/>
                <PrivateRoute exact path="/county" component={County}/>
                <PrivateRoute exact path="/history" component={History}/>
                <PrivateRoute exact path="/servicePrice" component={ServicePrice}/>
                <PrivateRoute exact path="/zipCode" component={ZipCode}/>
                <PrivateRoute exact path="/service" component={Service}/>
                <PrivateRoute exact path="/mainService" component={MainService}/>
                <PrivateRoute exact path="/subService" component={SubService}/>
                <PrivateRoute exact path="/additionalService" component={AdditionalService}/>
                <PrivateRoute exact path="/timeBooked" component={TimeBooked}/>
                <PrivateRoute exact path="/timeDuration" component={TimeDuratiion}/>
                <PrivateRoute exact path="/discountPercent" component={DiscountPercent}/>
                <PrivateRoute exact path="/agentSchedule" component={AgentSchedule}/>
                <PrivateRoute exact path="/weekDay" component={WeekDay}/>
                <PrivateRoute exact path="/passport" component={Passport}/>
                <PrivateRoute exact path="/discount" component={Discount}/>
                <PrivateRoute exact path="/dashBoard/:id" component={DashBoard}/>
                <PrivateRoute exact path="/feedBacks" component={FeedBack}/>
                <PrivateRoute exact path="/agentList" component={AgentList}/>
                <PrivateRoute exact path="/downloadPDF" component={DownloadPdf}/>
                <PublicRoute exact path="/agentProfile/:id" component={AgentProfile}/>
                <PrivateRoute exact path="/sharingClient" component={SharingClient}/>
                <PrivateRoute exact path="/mainSetup" component={MainSetup}/>
                <PrivateRoute exact path="/admin/holiday" component={Holiday}/>
                <PrivateRoute exact path="/audit" component={Audit}/>
                <PrivateRoute exact path="/admin/audit" component={AdminAudit}/>
                <PrivateRoute exact path="/customer" component={Customer}/>
                <PrivateRoute exact path="/admin/admins" component={Admins}/>
                <PrivateRoute exact path="/admin/partners" component={Partners}/>
                <PrivateRoute exact path="/admin/auditByAuthor/:id" component={AuditByAuthor}/>
                <PrivateRoute exact path="/admin/payment" component={Payment}/>
                <Route exact path='/path' component={ApostilleComponent} />
                <PrivateRoute exact path="/admin/review" component={Review}/>
                <Route component={NotFound}/>
            </Switch>
        </Provider>
    );
}

export default App;