import React from "react";
import App from "./routes/App";
import "bootstrap/dist/css/bootstrap.min.css";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import Test from "./pages/test/Test";

const app = (
  <>
    <ToastContainer />
    <BrowserRouter><App/></BrowserRouter>
  </>
);

ReactDOM.render(app, document.getElementById("root"));
