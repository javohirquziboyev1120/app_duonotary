import HttpClient from "../utils/HttpClient";
import {api} from './api'

export const userMe = (data = {username: null, password: null}) => {
    return HttpClient.doGet(api.userMe);
}
export const getAdminList = (data) => {
    return HttpClient.doGet(api.getAdminList + (data && data.page && data.size ? "?page=" + data.page + "&size=" + data.size : ""));
}
export const getUserTotalCount = (data) => {
    return HttpClient.doGet(api.getUserTotalCount + (data && data.roleName ? "?role=" + data.roleName : ""));
}
export const getAgentList = (data) => {
    return HttpClient.doGet(api.getAgentList + (data && data.page && data.size ? "?page=" + data.page + "&size=" + data.size : ""));
}
export const getClientList = (data) => {
    return HttpClient.doGet(api.getClientList + (data && data.page && data.size ? "?page=" + data.page + "&size=" + data.size : ""));
}
export const getAdminZipCode = (data = {}) => {
    return HttpClient.doGet(api.getAdminZipCode + (data && data.id ? data.id : ""))
}
export const getServices = () => {
    return HttpClient.doGet(api.getServicesPage)
}
export const getPermissionList = (param = {}) => {
    return HttpClient.doGet(api.getPermissionList, param);
}
export const getNewPermissionList = (param = {}) => {
    return HttpClient.doGet(api.getNewAdminPerList, param);
}
export const editAdminPermission = (param = {}) => {
    return HttpClient.doPost(api.editAdminPermission, param);
}
export const saveAdmin = (param = {}) => {
    return HttpClient.doPost(api.saveAdmin, param);
}
export const editAdmin = (param = {}) => {
    return HttpClient.doPut(api.editAdmin, param);
}
export const deleteAdmin = (data = {}) => {
    return HttpClient.doDelete(api.deleteAdmin + data.id);
}
export const editAdminZipCode = (param = {}) => {
    return HttpClient.doPost(api.editAdminZipCode, param);
}
export const getStates = (param = {}) => {
    return HttpClient.doGet(api.state, param)
}

export const getCountyByState = (param = {}) => {
    return HttpClient.doGet(api.servicePrice + "/byState" + "?stateId=" + param.option.value, param)
}
export const getZipCodeByCounty = (param = {}) => {
    return HttpClient.doGet(api.servicePrice + "/byCounty" + "?countyId=" + param.option.value, param)
}
export const editAdminEnabled = (param = {}) => {
    return HttpClient.doPost(api.editAdminEnabled + param.id, param);
}
