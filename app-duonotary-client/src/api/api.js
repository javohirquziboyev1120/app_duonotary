export const api = {
    login: '/auth/login',
    agent: '/agent/',
    registerUser: '/auth/registerUser',
    forgotPassword: '/auth/forgotPassword?',
    resetPassword: '/auth/resetPassword?',
    confirmCode: '/auth/confirmVerification?',
    getUser: '/user/',
    verifyEmail: '/auth/verifyEmail',
    registerAgent: '/auth/registerAgent',
    getAgents: '/agent/getAgents',
    getAgent: '/agent/',
    changeStatusDocument: '/agent/status',
    userMe: '/user/me',
    getAdminList: '/user/getAdminList',
    getUserTotalCount: '/user/getUserTotalCount',
    getClientList: '/user/getClientList',
    getAgentList: '/user/getAgentList',
    editAdminPermission: '/user/editAdminPermission',
    editAdminEnabled: '/user/changeEnabledAdmin/',
    saveAdmin: '/user/addAdmin',
    editAdmin: '/user/edit/',
    deleteAdmin: '/user/deleteAdmin/',
    partner: "/partner",
    editAdminZipCode: '/user/editAdminZipCode',
    getPermissionList: '/user/getPermissionsForAdmin',
    holiday: '/holiday',
    holidays: '/user/saveHolidays',
    isOrderDay: '/user/orderDay/',
    getUserSharingDiscountList: '/sharingDiscount/list?',
    user: '/user',
    getCountryList: '/country',
    getQrCodeForClient: '/qrcode',
    getUserSharingDiscount: '/sharingDiscount',
    addCountry: '/country',
    editCountry: '/country/',
    deleteCountry: '/country/',
    state: '/state/',
    county: '/county/',
    passport: '/agent/passport',
    certificates: '/agent/certificates',
    addAttachment: '/attachment/upload',
    attachment: '/attachment',
    servicePrice: '/servicePrice',
    pricing: '/pricing',
    service: '/service/',
    getServicesPage: '/service/getPage',
    mainServicesWithPercent: '/mainServiceWorkTime',
    mainService: '/mainService/',
    subService: '/subService/',
    documentType: '/documentType/',
    additionalService: '/additionalService/',
    additionalServicePrice: '/additionalServicePrice/',
    zipCode: '/zipCode/',
    timeBooked: '/timeBooked/',
    timeDuration: '/timeDuration/',
    discountPercent: '/discountPercent',
    getAgentDiscount: '/discount/getBySearchAgent',
    resetAgentDiscount: '/sharingDiscount/resetDiscount',
    weekDay: '/weekDay/',
    agentSchedule: '/agentSchedule/',
    history: '/history',
    historyTables: '/history/tableNames',
    getAuditItemForTable: '/audit/',
    zipCodeByCode: '/zipCode/byCode',
    dateAndTimes: '/timeTable/timeTableForOrder',
    timeTable: '/timeTable',
    order: '/order',
    orderAmounts: '/order/amounts',
    orderAmount: '/order/orderAmount',
    payType: '/payType/',
    payment: '/payment',
    dashboard: '/dashboard/',
    review: '/review/',
    geocode: '/geocode',
    webSocket: '/hello',
    getAllOrderByUserId: '/order/byUserId',
    feedBack: '/feedBack',
    discount: '/customDiscount',
    discounts: '/discount',
    clientMainPage: '/order/client/mainPage',
    adminOrderPage: '/order/admin/agent',
    exist: '/auth/exist/',
    getAuditForItemWithCommitId: '/audit/changes',
    getUserForAudit: '/user/getUser/',
    getOneItemAudit: '/audit/tableOneItemChanges/',
    getItemByAuthor: '/audit/getItemByAuthor/',
    getTotalElementsCount: '/audit/getTotalElementsCount',
    audit: '/audit/snapshots',
    auditTables: '/audit/getAuditTableList',
    allZipCodes: '/zipCode',
    allCounties: '/zipCode/getAllCounties',
    allStatesByPageable: '/zipCode/getAllStatesByPageable',
    allStates: '/zipCode/allStates',
    allStates2: '/zipCode/allStates2',
    timezoneName: '/timezone',
    deleteZipCode: '/zipCode/deleteZipCode',
    deleteCounty: '/zipCode/deleteCounty',
    deleteState: '/zipCode/deleteState',
    deleteAgentByZipCode: '/zipCode/deleteAgentByZipCode',
    addAgentToZipCode: '/zipCode/addAgentToZipCode',
    saveOrEditState: '/zipCode/saveOrEditState',
    saveOrEditCounty: '/zipCode/saveOrEditCounty',
    saveOrEditZipCode: '/zipCode/saveOrEditZipCode',
    getCountyBySelectedState: '/zipCode/getCountyBySelectedState',
    changeActiveOfZipCode: '/zipCode/changeActiveOfZipCode',
    getCurrentZipCode: '/zipCode/getCurrentZipCode2',
    getAgentsByZipCode: '/zipCode/getAgentsByZipCode',
    getZipCodeBySelectedCounty: '/zipCode/getZipCodeBySelectedCounty',

    sharingDiscountTariff: '/discount/sharingDiscountTariff',
    getFirstOrderDiscountTariff: '/discount/getResFirstDiscountTariff',
    changeActiveStatusSharingDiscountTariff: '/discount/changeActiveStatusSharingDiscountTariff',
    changeActiveStatusFirstOrderDiscountTariff: '/discount/changeActiveStatusFirstOrderDiscountTariff',
    changePercentSharingDiscountTariff: '/discount/saveOrEditSharingDiscountTariff',
    changePercentFirstOrderDiscountTariff: '/discount/saveOrEditFirstOrderDiscountTariff',
    saveOrEditLoyaltyDiscountTariff: '/discount/saveOrEditLoyaltyDiscountTariff',
    saveOrEditCustomDiscountTariff: '/discount/saveOrEditCustomDiscountTariff',
    getAllLoyaltyDiscountTariff: '/discount/getAllLoyaltyDiscountTariff',
    changeLoyaltyDiscountTariffActive: '/discount/changeLoyaltyDiscountTariffActive',
    removeLoyaltiDiscountTariff: '/discount/removeLoyaltiDiscountTariff',
    removeCustomDiscountTariff: '/discount/removeCustomDiscountTariff',
    removeCustomDiscountOrder: '/discount/removeCustomOrder',
    getAllSharingDiscountGiven: '/discount/getAllSharingDiscountGiven',
    getAllLoyaltyDiscountGiven: '/discount/getAllLoyaltyDiscountGiven',
    getAllDiscountExpense: '/discount/getAllDiscountExpense',
    getAllCustomerDiscount: '/discount/getAllCustomDiscount',
    getAllCustomDiscountTariff: '/discount/getCustomDiscountTariffs',
    getBySearch: '/discount/getBySearch',
    saveOrEditCustomDiscount: '/discount/saveOrEditCustomDiscount',
    outOfService: '/outOfService',
    getAdminZipCode: '/zipCode/getAdminZipCode/',
    getNewAdminPerList: '/user/getNewPermissionsForAdmin',
    blog: '/blog',
    category: "/category/",
    faq: "/blog/faq"
};