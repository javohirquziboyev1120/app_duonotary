//Start Orders api
import HttpClient from "../utils/HttpClient";
import {api} from "./api";

export let getAddressInfoZipCode = (data) => {
    return HttpClient.doGet(api.order + '/zipCode?' + makeParam(data))
}
export let getZipCodeByCode = (data) => {
    return HttpClient.doGet(api.zipCodeByCode + '?' + makeParam(data))
}
export let geAllUserId = (data) => {
    return HttpClient.doGet(api.getAllOrderByUserId + (data.userId ? "/" + data.userId : "") + "?" + makeParam(data.param))
}
export let getCurrentAgent = (data) => {
    return HttpClient.doGet(api.order + '/getOnlineAgentForNow?' + makeParam(data))
}

export let getServicePriceByZipCode = (data) => {
    return HttpClient.doGet(api.servicePrice + '/byZipCodeId?' + makeParam(data))
}

export let getAdditionalServicePriceList = (data) => {
    return HttpClient.doGet(api.additionalServicePrice + 'byServicePriceId/' + data)
}

export let getTimesByDate = (data) => {
    return HttpClient.doGet(api.dateAndTimes + '?' + makeParam(data))
}
export let postTimeTable = (data) => {
    return HttpClient.doPost(api.timeTable, data)
}

export let getPricingByServicePrice = (data) => {
    return HttpClient.doGet(api.pricing + '/byServicePrice?' + makeParam(data))
}

export let getPayType = (param = {}) => {
    return HttpClient.doGet(api.payType, param)
}
export let getAmounts = (data) => {
    return HttpClient.doPost(api.orderAmounts, data)
}
export let getAmount = (data) => {
    return HttpClient.doPost(api.orderAmount, data)
}
export let addDocumentOrder = (data) => {
    return HttpClient.doPost(api.order + "/document?" + makeParam(data))
}
export let deleteDocumentOrder = (data) => {
    return HttpClient.doDelete(api.order + "/document?" + makeParam(data))
}
export let pay = (data) => {
    return HttpClient.doPost(api.payment, data)
}

export let saveOrder = (data) => {
    return HttpClient.doPost(api.order + '?' + makeParam(data.user), data)
}
export let editOrder = (data) => {
    return HttpClient.doPut(api.order, data)
}
export let getGeocode = (data) => {
    return HttpClient.doGet(api.geocode + '/' + data.type + '?' + makeParam(data))
}
export let getServicePricesForOnline = (param = {}) => {
    return HttpClient.doGet(api.servicePrice + "/online", param)
}
export let getOrderList = (data) => {
    return HttpClient.doGet(api.order + "/getOrderList?" + makeParam(data))
}
export let getOrderListByStatus = (data) => {
    return HttpClient.doGet(api.order + "/mobile/mainPage?" + makeParam(data))
}
export let getOrderWithCustomer = (data) => {
    return HttpClient.doGet(api.order + "/customers?" + (data ? "page=" + data.page + "&size=" + data.size : ""))
}

export let getOrdersForClientMainPage = (data) => {
    return HttpClient.doGet(api.clientMainPage + '?' + makeParam(data))
}
export let getClientForAdmin = (data) => {
    return HttpClient.doGet(api.clientMainPage + "/" + data.id + (data ? "?page=0&size=5" : ""))
}
export let getOrdersForAdmin = (data) => {
    return HttpClient.doGet(api.adminOrderPage + "/" + (data ? data + "?page=0&size=5" : ""))
}
export let getOrdersForAdmin2 = (data = {}) => {
    return HttpClient.doGet(api.adminOrderPage + "?page=0&size=5")
}
export let getAmountOfOrderToCancel = (data) => {
    return HttpClient.doGet(api.order + "/cancel/" + data)
}
export let orderCancel = (data) => {
    return HttpClient.doPut(api.order + "/cancel/" + data)
}
export let getOrderForAgent = (data) => {
    return HttpClient.doGet(api.order + "/admin/order?" + makeParam(data))
}
export let deleteOrder = (data) => {
    return HttpClient.doDelete(api.order + "/" + data)
}
export let getServiceNow = () => {
    return HttpClient.doGet(api.order + '/getService')
}
export let sentOutOfService = (data) => {
    return HttpClient.doPost(api.outOfService, data)
}
export let paymentFuture = (data) => {
    return HttpClient.doGet(api.payment + "/getFutureCharge?" + makeParam(data))
}
export let searchUserByEmail = (data) => {
    return HttpClient.doGet(api.user + "/search?" + makeParam(data))
}
export let searchAgent = (data) => {
    return HttpClient.doGet(api.order + "/searchAgent?" + makeParam(data))
}
export let changeCustomerNames = (data) => {
    return HttpClient.doPost(api.user + "/customer/edit", data)
}
export let changeStatusOrder = (data) => {
    return HttpClient.doGet(api.order + "/changeStatus?" + makeParam(data))
}
export let changeStatusToComplete = (data) => {
    return HttpClient.doPut(api.order + "/complete/" + data.id)
}
export let payment = (data) => {
    return HttpClient.doGet(api.payment + "/getChargeOnTime?" + makeParam(data))
}
export let paymentSuccess = (data) => {
    return HttpClient.doGet(api.payment + "/success?" + makeParam(data))
}
export let changeAgent = (data) => {
    return HttpClient.doGet(api.order + "/changeAgent?" + makeParam(data))
}

export const getOrdersByFilter = (data) => {
    return HttpClient.doGet(api.order + "/getOrder/filter?" + makeParam(data))
}

export const getOrdersBySearch = (data) => {
    return HttpClient.doGet(api.order + "/getOrders/search?" + makeParam(data))
}
export const getCertificateByOrder = (data) => {
    return HttpClient.doGet(api.agent + "certificate/zipCode?" + makeParam(data))
}
export const getOrdersBySearchForClient = (data) => {
    return HttpClient.doGet(api.order + "/getOrders/search/byClient?"+makeParam(data))
}
export const getOrdersBySearchForAgent = (data) => {
    return HttpClient.doGet(api.order + "/getOrders/search/byAgent?"+makeParam(data))
}

function makeParam(data) {
    if (data) {
        let param = ''
        Object.entries(data).map(([key, value]) => param = param + key + '=' + value + '&&')
        return param
    }
    return ""

}
