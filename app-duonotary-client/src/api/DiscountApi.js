import HttpClient from "../utils/HttpClient";
import {api} from './api'


export const getSharingDiscountTariff = () => {
    return HttpClient.doGet(api.sharingDiscountTariff);
}
export const getFirstOrderDiscountTariff = () => {
    return HttpClient.doGet(api.getFirstOrderDiscountTariff);
}
export const changeActiveStatusSharingDiscountTariff = (data) => {
    return HttpClient.doGet(api.changeActiveStatusSharingDiscountTariff + "?active=" + data.active + "&online=" + data.online);
}
export const changeActiveStatusFirstOrderDiscountTariff = (data) => {
    return HttpClient.doGet(api.changeActiveStatusFirstOrderDiscountTariff + "?active=" + data.active + "&online=" + data.online);
}
export const changePercentSharingDiscountTariff = (data) => {
    return HttpClient.doPost(api.changePercentSharingDiscountTariff, data);
}
export const changePercentFirstOrderDiscountTariff = (data) => {
    return HttpClient.doPost(api.changePercentFirstOrderDiscountTariff, data);
}
export const editSharingDiscountTariff = (data) => {
    return HttpClient.doPatch(api.sharingDiscountTariff + "/" + data.id, data);
}

export const addLoyaltyDiscountTariff = (data) => {
    return HttpClient.doPost(api.loyaltyDiscountTariff, data);
}
export const editLoyaltyDiscountTariff = (data) => {
    return HttpClient.doPatch(api.loyaltyDiscountTariff + "/" + data.id, data);
}
export const getLoyaltyDiscountTariffs = () => {
    return HttpClient.doGet(api.loyaltyDiscountTariff);
}
export const saveOrEditLoyaltyDiscountTariff = (data) => {
    return HttpClient.doPost(api.saveOrEditLoyaltyDiscountTariff, data);
}
export const saveOrEditCustomDiscountTariff = (data) => {
    return HttpClient.doPost(api.saveOrEditCustomDiscountTariff, data);
}
export const saveOrEditCustomDiscount = (data) => {
    return HttpClient.doPost(api.saveOrEditCustomDiscount, data);
}
export const getAllLoyaltyDiscountTariff = () => {
    return HttpClient.doGet(api.getAllLoyaltyDiscountTariff);
}
export const changeLoyaltyDiscountTariffActive = (data) => {
    return HttpClient.doGet(api.changeLoyaltyDiscountTariffActive + "?id=" + data.id);
}
export const removeLoyaltiDiscountTariff = (data) => {
    return HttpClient.doGet(api.removeLoyaltiDiscountTariff + "?id=" + data.id);
}
export const removeCustomDiscountTariff = (data) => {
    return HttpClient.doGet(api.removeCustomDiscountTariff + "?id=" + data.id);
}
export const removeCustomDiscountOrder = (data) => {
    return HttpClient.doGet(api.removeCustomDiscountOrder + "?id=" + data.id);
}
export const getAllSharingDiscountGiven = (data) => {
    return HttpClient.doGet(api.getAllSharingDiscountGiven + "?page=" + data.page + "&size=" + data.size);
}
export const getAllLoyaltyDiscountGiven = (data) => {
    return HttpClient.doGet(api.getAllLoyaltyDiscountGiven + "?page=" + data.page + "&size=" + data.size);
}
export const getAllDiscountExpense = (data) => {
    return HttpClient.doGet(api.getAllDiscountExpense + "?page=" + data.page + "&size=" + data.size);
}
export const getAllCustomerDiscount = (data) => {
    return HttpClient.doGet(api.getAllCustomerDiscount + "?page=" + data.page + " &size=" + data.size);
}
export const getAllCustomDiscountTariff = (data) => {
    return HttpClient.doGet(api.getAllCustomDiscountTariff + "?page=" + data.page + "&size=" + data.size);
}
export const getZipCodeBySelectedCounty = (data) => {
    return HttpClient.doGet(api.getZipCodeBySelectedCounty + "?id=" + data.id);
}
export const getBySearch = (data) => {
    return HttpClient.doGet(api.getBySearch + "?search=" + data.search);
}
