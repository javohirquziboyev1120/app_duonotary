import HttpClient from "../utils/HttpClient";
import {api} from './api'


export const getAllZipCodes = (data) => {
    return HttpClient.doGet(api.allZipCodes + "?page=" + data.page + "&size=" + data.size);
}
export const getAllCounties = (data) => {
    return HttpClient.doGet(api.allCounties + "?page=" + data.page + "&size=" + data.size);
}
export const getAllStatesByPageable = (data) => {
    return HttpClient.doGet(api.allStatesByPageable + "?page=" + data.page + "&size=" + data.size);
}
export const allStates = () => {
    return HttpClient.doGet(api.allStates);
}
export const allStates2 = () => {
    return HttpClient.doGet(api.allStates2);
}
export const getTimezoneName = () => {
    return HttpClient.doGet(api.timezoneName);
}
export const deleteZipCode = (data) => {
    return HttpClient.doGet(api.deleteZipCode + "?id=" + data.id);
}
export const deleteCounty = (data) => {
    return HttpClient.doGet(api.deleteCounty + "?id=" + data.id);
}
export const deleteState = (data) => {
    return HttpClient.doGet(api.deleteState + "?id=" + data.id);
}
export const deleteAgentByZipCode = (data) => {
    return HttpClient.doGet(api.deleteAgentByZipCode + "?zcId=" + data.zcId+"&agId="+data.agId);
}
export const saveOrEditState = (data) => {
    return HttpClient.doPost(api.saveOrEditState,data);
}
export const saveOrEditCounty = (data) => {
    return HttpClient.doPost(api.saveOrEditCounty,data);
}
export const saveOrEditZipCode = (data) => {
    return HttpClient.doPost(api.saveOrEditZipCode,data);
}
export const getCountyBySelectedState = (data) => {
    return HttpClient.doGet(api.getCountyBySelectedState + "?id=" + data.id);
}
export const changeActiveOfZipCode = (data) => {
    return HttpClient.doGet(api.changeActiveOfZipCode + "?id=" + data.id);
}
export const getCurrentZipCode = (data) => {
    return HttpClient.doGet(api.getCurrentZipCode + "?id=" + data.id);
}
export const getAgentsByZipCode = (data) => {
    return HttpClient.doGet(api.getAgentsByZipCode + "?zcId=" + data.zcId);
}
export const addAgentToZipCode = (data) => {
    return HttpClient.doGet(api.addAgentToZipCode + "?zcId=" + data.zcId+"&agId="+data.agId);
}