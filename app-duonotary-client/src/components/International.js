import React, {Component} from 'react';
import {Col, CustomInput, Row} from "reactstrap";
import {toast} from "react-toastify";
import {InputForm} from "./InputForm";
import {connect} from "react-redux";
import Input from "reactstrap/lib/Input";
import Counter from "./Counter";

class International extends Component {
    constructor(props) {
        super(props);
        const {data} = this.props;
        console.log(data);
        this.state = {
            id: data ? data.id : '',
            embassy: data ? data.embassy : false,
            someOneElse: data ? data.someOneElse : false,
            requesterFirstName: data ? data.requesterFirstName : '',
            requesterLastName: data ? data.requesterLastName : '',
            requesterPhoneNumber: data ? data.requesterPhoneNumber : '',
            requesterEmail: data ? data.requesterEmail : '',
            country: data ? data.country && {id: data.country.id} : 0,
            countryId: data ? data.country && data.country.id : 0,
            pickUpAddress: data ? data.pickUpAddress : '',
            numberDocument: data ? data.numberDocument : 1,
            message: data ? data.message : '',
            documentType : data ? data.documentType&&{id:data.documentType.id} : 0,
            documentTypeId : data ? data.documentType&&data.documentType.id : 0
        }
    }

    handleChange = (e) => {
        const {name, value} = e.target;
        console.log(name, value);
        if (name === "check" || name === "check1") {
            this.setState({embassy: !this.state.embassy})
        } else {
            if (name === "countryId") {
                this.setState({country: {id: value}})
            }
            if(name==="documentTypeId"){
                this.setState({
                    documentType:{id:value}
                })
            }
            this.setState({
                [name]: value,
            }, () => {
                if (this.props.data) {
                    this.props.dispatch({
                        type: 'updateState',
                        payload: {internationalDto: this.state, countDocument: parseInt(this.state.numberDocument)}
                    })
                }
            })
        }
    }

    save = (e, v) => {
        e.preventDefault();
        let obj = {...this.state}
        if (!obj.country && !obj.country.id) {
            toast.error("Select country")
            return "";
        }
        if(!obj.documentType&&!obj.documentType.id){
            toast.error("Select type of document")
            return "";
        }
        this.props.dispatch({
            type: 'updateState',
            payload: {internationalDto: obj, stage: 3, countDocument: parseInt(obj.numberDocument)}
        })
    }

    render() {
        const { dispatch,countries, documentTypes, embassyCountries, data, isAdd, isClient, mobile,orderAdditionalServices,additionalServicePrices,servicePrice} = this.props;
        const {
            requesterFirstName,
            requesterLastName,
            requesterPhoneNumber,
            embassy,
            someOneElse,
            requesterEmail,
            countryId,
            pickUpAddress,
            numberDocument,documentTypeId,
        } = this.state;
        console.log(data);
        function change(v, i) {
            let oldArr = [...orderAdditionalServices];
            if (orderAdditionalServices[0]) {
                orderAdditionalServices.map((value, i) => {
                    if (value.additionalServicePriceId === v.id)
                        oldArr.splice(i, 1)
                })
            }
            oldArr.push({price: v.price * i, count: i, additionalServicePriceId: v.id})
          dispatch({
                type: 'updateState',
                payload: {
                    orderAdditionalServices: oldArr
                }
            })

        }
        return (
            <div className='embassy row pt-3'>
                <Col md={12}>
                    {isAdd ?
                        <Row> <Col md={2}>
                            <label className='labelDuo'>
                                <div className='labelBlock'>
                                    {!embassy && <div className='labelBox'>
                                        <i className="fas fa-check"/>
                                    </div>}
                                </div>
                                <CustomInput
                                    name="check"
                                    className='d-none'
                                    onChange={this.handleChange}
                                    checked={!embassy}
                                    type="checkbox"
                                    id="2asdf"
                                />
                                Apostille
                            </label>
                        </Col>
                            <Col md={2}>
                                <label className='labelDuo'>
                                    <div className='labelBlock'>
                                        {embassy && <div className='labelBox'>
                                            <i className="fas fa-check"/>
                                        </div>}
                                    </div>
                                    <CustomInput
                                        name="check1"
                                        className='d-none'
                                        onChange={this.handleChange}
                                        checked={embassy}
                                        type="checkbox"
                                        id="1sadf"
                                    />
                                    Embassy
                                </label>
                            </Col>
                        </Row>
                        : ""}
                </Col>
                <Col md={12}>
                    {isAdd && <Row>
                        <Col md={5} onClick={() => this.setState({someOneElse: !someOneElse})} style={{cursor:"pointer"}}>
                            <Input type="checkbox" checked={someOneElse}
                                   onChange={() => this.setState({someOneElse: !someOneElse})}/> Are the Reuesting this
                            serivice for someone else?
                        </Col>
                    </Row>}
                    <form className='formGroupEmbassy' onSubmit={this.save}>
                        <div className='row m-0 formInputGroup'>
                            {someOneElse&& <div className={`col-md-${mobile ? '12' : '6'}`}>
                                <InputForm
                                    required={true}
                                    title={'Requester First Name'}
                                    name="requesterFirstName"
                                    value={requesterFirstName}
                                    id="requesterFirstName"
                                    readOnly={isClient}
                                    placeholder="First name"
                                    handleChange={this.handleChange}
                                />
                            </div>
                            }
                            {someOneElse&&<div className={`col-md-${mobile ? '12' : '6'}`}>
                                <InputForm
                                    required={true}
                                    title={'Requester Last name'}
                                    name="requesterLastName"
                                    value={requesterLastName}
                                    id="lastName"
                                    readOnly={isClient}
                                    placeholder="LastName"
                                    handleChange={this.handleChange}
                                />
                            </div>}
                            {someOneElse&& <div className={`col-md-${mobile ? '12' : '6'}`}>
                                <InputForm
                                    required={true}
                                    title={'Requester Phone number'}
                                    name="requesterPhoneNumber"
                                    value={requesterPhoneNumber}
                                    id="phoneNumber"
                                    type="number"
                                    readOnly={isClient}
                                    placeholder="Phone number"
                                    handleChange={this.handleChange}
                                />
                            </div>}
                            {someOneElse&& <div className={`col-md-${mobile ? '12' : '6'}`}>
                                <InputForm
                                    required={true}
                                    title={'Requester Email'}
                                    name="requesterEmail"
                                    value={requesterEmail}
                                    id="email"
                                    readOnly={isClient}
                                    placeholder="Email"
                                    type={"email"}
                                    handleChange={this.handleChange}
                                />
                            </div>}

                            {embassy ?
                                <div className={`col-md-${mobile ? '12' : '6'}`}>
                                    <div className='form-group'>
                                        <label>Select embassy country</label>
                                        <select
                                            className='form-control h_input py-0'
                                            name="countryId"
                                            value={countryId}
                                            id="countryId"
                                            onChange={this.handleChange}
                                        >
                                            <option value="0">Select embassy country</option>
                                            {embassyCountries && embassyCountries.map(country =>
                                                <option key={country.id} value={country.id}>{country.name}</option>
                                            )}
                                        </select>
                                    </div>
                                </div> :
                                <div className={`col-md-${mobile ? '12' : '6'}`}>
                                    <div className='form-group'>
                                        <label>Select country</label>
                                        <select
                                            className='form-control h_input py-0'
                                            name="countryId"
                                            value={countryId}
                                            id="countryId"
                                            onChange={this.handleChange}
                                        >
                                            <option value="0">Select country</option>
                                            {countries && countries.map(country =>
                                                <option key={country.id} value={country.id}>{country.name}</option>
                                            )}
                                        </select>
                                    </div>
                                </div>
                            }
                            <div className={`col-md-${mobile ? '12' : '6'}`}>
                                <div className='form-group'>
                                    <label>Select Document Type</label>
                                    <select
                                        className='form-control h_input py-0'
                                        name="documentTypeId"
                                        value={documentTypeId}
                                        id="documentType"
                                        onChange={this.handleChange}
                                    >
                                        <option value="0">Select document type</option>
                                        {documentTypes && documentTypes.map(document =>
                                            <option key={document.id} value={document.id}>{document.name}</option>
                                        )}
                                    </select>
                                </div>
                            </div>

                            <div className={`col-md-${mobile ? '12' : '6'}`}>
                                <InputForm
                                    required={true}
                                    title={'Pick up Address'}
                                    name="pickUpAddress"
                                    value={pickUpAddress}
                                    id="pickUpAddress"
                                    readOnly={isClient}
                                    placeholder="Enter pick up address"
                                    handleChange={this.handleChange}
                                />
                            </div>
                            <div className={`col-md-${mobile ? '12' : '6'}`}>
                                <InputForm
                                    required={true}
                                    type="number"
                                    title={'Count document'}
                                    name="numberDocument"
                                    value={numberDocument}
                                    id="numberDocument"
                                    readOnly={isClient}
                                    placeholder="Select number document"
                                    handleChange={this.handleChange}
                                />
                            </div>
                            {isAdd&&!!servicePrice && !!additionalServicePrices[0] ? additionalServicePrices.map(v =>
                                <Col md={4} className="card" key={v.id}>
                                    <Row>
                                        <Col md={3} className="text-center">
                                            <Counter
                                                value={!JSON.stringify(orderAdditionalServices[0] === JSON.stringify([])) ? orderAdditionalServices.filter(o => o.additionalServicePriceId === v.id)[0].count : 0}
                                                getValue={i => change(v, i)}
                                            />
                                        </Col>

                                        <Col md={9} className="name">
                                            {v.additionalServiceDto.name + " - $" + v.price}
                                        </Col>
                                    </Row>
                                </Col>) : ''}


                            <div className='col-md-2 ml-auto'>
                                {isAdd ?
                                    <button type='submit' className='btn btn-primary btn-block p-2'
                                            style={{cursor: 'pointer'}}>Next</button>
                                    : ''}
                            </div>
                        </div>
                    </form>
                </Col>
            </div>
        );
    }
}

International.propTypes = {};
export default connect(({}) => ({}))(International);

// <div>
//     <div>
//         {isApostille?
//             <ApostilleComponent
//                 isClient={isClient}
//                 isAdd={isAdd}
//                 data={data}
//                 countries={countries}
//                 documentTypes={documentTypes}
//                 submit={submit}
//                 dispatch={dispatch}
//                 mobile={mobile}
//             />:
//             <EmbassyComponent
//                 isClient={isClient}
//                 isAdd={isAdd}
//                 data={data}
//                 submit={submit}
//                 dispatch={dispatch}
//                 embassyCountries={embassyCountries}
//                 mobile={mobile}
//             />}
{/*    </div>*/
}
{/*</div>*/
}

// <div className={`col-md-${mobile ? '12' : '6'}`}>
//     <InputForm
//         required={true}
//         title={'First Name'}
//         name="firstName"
//         value={firstName}
//         id="firstName"
//         readOnly={isClient}
//         placeholder="First name"
//         handleChange={this.handleChange}
//     />
// </div>
// <div className={`col-md-${mobile ? '12' : '6'}`}>
//     <InputForm
//         required={true}
//         title={'Last name'}
//         name="lastName"
//         value={lastName}
//         id="lastName"
//         readOnly={isClient}
//         placeholder="LastName"
//         handleChange={this.handleChange}
//     />
// </div>
// <div className={`col-md-${mobile ? '12' : '6'}`}>
//     <InputForm
//         required={true}
//         title={'Phone number'}
//         name="phoneNumber"
//         value={phoneNumber}
//         id="phoneNumber"
//         readOnly={isClient}
//         placeholder="Phone number"
//         handleChange={this.handleChange}
//     />
// </div>
// <div className={`col-md-${mobile ? '12' : '6'}`}>
//     <InputForm
//         required={true}
//         title={'Email'}
//         name="email"
//         value={email}
//         id="email"
//         readOnly={isClient}
//         placeholder="Email"
//         handleChange={this.handleChange}
//     />
// </div>
