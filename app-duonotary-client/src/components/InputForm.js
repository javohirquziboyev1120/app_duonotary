import React from "react";

export const InputForm = ({title, type = 'text', name, value, placeholder, handleChange,required}) => {
    return <div className='form-group'>
        <label htmlFor={name}>
            {title}
        </label>
        <input
            type={type}
            className='form-control h_input'
            name={name}
            id={name}
            value={value}
            placeholder={placeholder}
            onChange={handleChange}
            required={required}
        />
    </div>
}