import React, {Component} from "react";
import "./mainLayout.scss";
import {Link} from "react-router-dom";
import {Col, Container, Input, Row} from "reactstrap";
import {logout} from "../redux/actions/AuthActions";
import {connect} from "react-redux";
import {MenuMobile} from "./Icons";
import './agentLayout.scss';
import {getOrdersBySearchForAgent, getOrdersBySearchForClient} from "../redux/actions/OrderAction";

class AgentLayout extends Component {
    componentDidMount() {
        // if (!this.props.isAgent) window.location.replace("/")
    }

    constructor(props) {
        super(props);
        this.state = {
            menuHidden: false,
            showMobileMenu: false
        }
    }

    //Only mobile
    toggleShowMobileMenu = () => {
        this.setState({
            showMobileMenu: !this.state.showMobileMenu,
        });
    };

    toggleMenu = () => {
        this.setState({
            menuHidden: !this.state.menuHidden
        })
    }

    render() {
        const logOut = () => {
            this.props.dispatch(logout());
        };
        const {currentUser,isFilter,search, dispatch} = this.props;

        let pressEnter = (item) => {
            if (item.charCode === 13) {
                if (this.props.pathname === "/agent/orders")
                    dispatch(getOrdersBySearchForAgent({search: ""+item.target.value}));
            }
        }
        let searchByName = (item) => {
            this.setState({search: item.target.value})
            dispatch({
                type: 'updateState',
                payload: {
                    search: item.target.value
                }

            });
        }
        let ShowFilter = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    isFilter: !isFilter
                }
            })
        };
        return currentUser ? (
            <div className="main-layout">
                <div className={
                    this.state.showMobileMenu
                        ? "main-layout-left left-mobile"
                        : this.state.menuHidden ? "main-layout-left main-layout-left-hidden" : "main-layout-left"
                }>
                    <Link to="/">
                        <div className="logo-duo">
                            <img src="/assets/img/logo.png" alt=""/>
                        </div>
                    </Link>

                    <div className="main-link-div">
                        <div
                            className={
                                this.props.pathname === "/agent"
                                    ? "active-link"
                                    : "default-link"
                            }
                        >
                            <span className="icon icon-main"/>
                            <Link to="/agent" className="main-link">
                                Main page
                            </Link>
                        </div>
                        <div
                            className={
                                this.props.pathname === "/agent/orders"
                                    ? "active-link"
                                    : "default-link"
                            }
                        >
                            <span className="icon icon-order"/>
                            <Link to="/agent/orders" className="main-link">
                                Orders
                            </Link>
                        </div>
                        <div
                            className={
                                this.props.pathname === "/agent/schedule"
                                    ? "active-link"
                                    : "default-link"
                            }
                        >
                            <span className="icon icon-time"/>
                            <Link to="/agent/schedule" className="main-link">
                                Working schedule
                            </Link>
                        </div>
                        <div
                            className={
                                this.props.pathname === "/agent/zipcode"
                                    ? "active-link"
                                    : "default-link"
                            }
                        >
                            <span className="icon icon-zipCode"/>
                            <Link to="/agent/zipcode" className="main-link">
                                ZipCode
                            </Link>
                        </div>
                        <div
                            className={
                                this.props.pathname === "/agent/feedbacks"
                                    ? "active-link"
                                    : "default-link"
                            }
                        >
                            <span className="icon icon-feedback"/>
                            <Link to="/agent/feedbacks" className="main-link">
                                Feedbacks
                            </Link>
                        </div>

                        <div
                            className={
                                this.props.pathname === "/agent/sharing"
                                    ? "active-link"
                                    : "default-link"
                            }
                        >
                            <span className="icon icon-sharing"/>
                            <Link to="/agent/sharing" className="main-link">
                                Sharing
                            </Link>
                        </div>
                        <div
                            className={
                                this.props.pathname === "/agent/passports"
                                    ? "active-link"
                                    : "default-link"
                            }
                        >
                            {/*Need to change icon*/}
                            <span className="icon icon-discount"/>
                            <Link to="/agent/passports" className="main-link">
                                Photo ID
                            </Link>
                        </div>
                        <div
                            className={
                                this.props.pathname === "/agent/certificates"
                                    ? "active-link"
                                    : "default-link"
                            }
                        >
                            {/*Need to change icon*/}
                            <span className="icon icon-discount"/>
                            <Link
                                to="/agent/certificates"
                                className="main-link"
                            >
                                Certificates
                            </Link>
                        </div>
                        <div
                            className={
                                this.props.pathname === "/agent/settings"
                                    ? "active-link"
                                    : "default-link"
                            }
                        >
                            <span className="icon icon-setting"/>
                            <Link to="/agent/settings" className="main-link">
                                Settings
                            </Link>
                        </div>

                        <div className="about-user">
                            <div className="avatar">
                                <img src="/assets/img/avatar.png" alt=""/>
                            </div>
                            <div className="name-title">
                                <div className="name">
                                    {currentUser.lastName}{" "}
                                    {currentUser.firstName}
                                </div>
                                <div className="title">Agent</div>
                            </div>
                        </div>
                        <div className="close-menu mt-1 mx-auto" onClick={this.toggleShowMobileMenu}>
                            <span className="icon icon-plus"/>
                        </div>
                    </div>
                </div>
                <div className="main-layout-right">
                    <div className={this.state.menuHidden ? "menu-toggle l-0" : "menu-toggle"}
                         onClick={this.toggleMenu}>
                        {
                            this.state.menuHidden ?
                                <MenuMobile/>
                                :
                                <span className="icon icon-plus"/>
                        }
                    </div>
                    <Container className="client-navbar">

                        <Row sm={3} className={"row-client-navbar"}>
                            <Col
                                sm={1}
                                onClick={this.toggleShowMobileMenu}
                                className={"icon-menu-mobile"}
                            >
                                <MenuMobile/>
                            </Col>
                            <Col sm={4} className="page-name-mobile"
                                 md={this.props.pathname === "/agent/schedule" ? 3 : 2}>
                                {this.props.pathname === "/agent" ? (
                                    <div className="page-name">Main page</div>
                                ) : (
                                    ""
                                )}
                                {this.props.pathname === "/agent/sharing" ? (
                                    <div className="page-name">Sharing</div>
                                ) : (
                                    ""
                                )}
                                {this.props.pathname === "/agent/orders" ? (
                                    <div className="page-name">Orders</div>
                                ) : (
                                    ""
                                )}
                                {this.props.pathname === "/agent/passports" ? (
                                    <div className="page-name">Photo ID</div>
                                ) : (
                                    ""
                                )}
                                {this.props.pathname ===
                                "/agent/certificates" ? (
                                    <div className="page-name">
                                        Certificates
                                    </div>
                                ) : (
                                    ""
                                )}
                                {this.props.pathname === "/agent/feedbacks" ? (
                                    <div className="page-name">Feedbacks</div>
                                ) : (
                                    ""
                                )}

                                {this.props.pathname === "/agent/settings" ? (
                                    <div className="page-name">Settings</div>
                                ) : (
                                    ""
                                )}
                                {this.props.pathname === "/agent/schedule" ? (
                                    <div className="page-name">Working schedule</div>
                                ) : (
                                    ""
                                )}
                                {this.props.pathname === "/agent/zipcode" ? (
                                    <div className='page-name'>ZipCode</div>
                                ) : (
                                    ""
                                )}
                            </Col>

                            <Col md={4} className='mx-auto'>
                                {this.props.pathname === "/agent/orders" ?
                                    <div className={'search_block_admin'}>
                                        <Input
                                            onKeyPress={(item) => pressEnter(item)}
                                            onChange={(item) => searchByName(item)}
                                            className="search-box search-box-admin" id="searchInput"
                                            defaultValue={search} placeholder="Search ...."/>
                                        {this.props.pathname === "/agent/orders"?
                                            <img onClick={ShowFilter} className="search-icons search-icons_admin"
                                                 src="/assets/icons/search.png" alt="ccc"/> : ""}
                                    </div>
                                    : ""
                                }
                            </Col>

                            <Col md={3}
                                 className={
                                     this.props.pathname === "/agent/schedule" ? "ml-auto" : "ml-auto"
                                 }>
                                <div className="notification-exit">
                                    <div className="notification" id="notification">
                                        <img
                                            src="/assets/icons/notification.png"
                                            alt=""
                                        />
                                        <span className="notify-count" id="notify-count"/>
                                    </div>
                                    <div className="exit ml-3" onClick={logOut}>
                                        <img
                                            src="/assets/icons/exit.png"
                                            alt=""
                                        />
                                    </div>
                                </div>
                            </Col>
                        </Row>


                    </Container>
                    <div className="container">{this.props.children}</div>
                </div>
            </div>
        ) : (
            <div></div>
        );
    }
}

AgentLayout.propTypes = {};

export default connect(({auth: {currentUser, isAgent}, app:{isFilter,search}}) => ({currentUser, isAgent,isFilter,search}))(
    AgentLayout
);
