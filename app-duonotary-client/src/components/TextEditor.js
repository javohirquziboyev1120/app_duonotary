import ReactQuill from "react-quill";
import React from "react";
import 'react-quill/dist/quill.snow.css'; // ES6

class TextEditor extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            theme: 'snow'
        }
    }

    render () {
        return (
                <ReactQuill
                    theme={this.state.theme}
                    onChange={this.props.handleChange}
                    value={this.props.editorHtml}
                    style={this.props.style}
                    className={this.props.className}
                    modules={{
                        toolbar: [
                            [{ 'header': '1'}, {'header': '2'}, { 'font': [] }],
                            [{size: []}],
                            ['bold', 'italic', 'underline', 'strike', 'blockquote'],
                            [{'list': 'ordered'}, {'list': 'bullet'},
                                {'indent': '-1'}, {'indent': '+1'}],
                            ['link', 'image'],
                            ['clean']
                        ],
                        clipboard: {
                            // toggle to add extra line breaks when pasting HTML:
                            matchVisual: false,
                        }
                    }}
                    formats={[
                        'header', 'font', 'size',
                        'bold', 'italic', 'underline', 'strike', 'blockquote',
                        'list', 'bullet', 'indent',
                        'link', 'image'
                    ]}
                    bounds={'.app'}
                    placeholder={this.props.placeholder}
                />
        )
    }
}
export default TextEditor;
