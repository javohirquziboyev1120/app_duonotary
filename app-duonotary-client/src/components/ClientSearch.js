import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Select from 'react-dropdown-select';

class ClientSearch extends Component {

    render() {

        const getUsersBySearch=(e)=>{
            let search=e.target.value

        }

        return (
            <div>
                <input onChange={getUsersBySearch} list="browsers" name="browser"/>
                <datalist id="browsers">
                    <option value="Internet Explorer"></option>
                    <option value="Firefox"></option>
                    <option value="Chrome"></option>
                    <option value="Opera"></option>
                    <option value="Safari"></option>
                </datalist>
            </div>
        );
    }
}

ClientSearch.propTypes = {};

export default ClientSearch;