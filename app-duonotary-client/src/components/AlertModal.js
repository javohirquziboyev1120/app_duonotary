import React, {Component} from 'react';
import PropTypes from 'prop-types';

class AlertModal extends Component {
    constructor(props) {
        super(props);
        this.state = {off: false};
    }

    displayOff = () => {
        this.setState({off: true});
        if (this.props.action)
            this.props.action();
    }

    render() {
        return (
            <div className={this.state.off ? "alert-off" : "alert-container"}>
                <div className="alert-modal flex-column">
                    <span className="align-self-center alert-title">{this.props.title}</span>
                    <span className="align-self-center alert-body">{this.props.body}</span>
                    <button className="ok-button align-self-end"
                            onClick={this.props.display ? this.props.display : this.displayOff}>Ok
                    </button>
                </div>
            </div>
        );
    }
}

AlertModal.propTypes = {};

export default AlertModal;