import React, { Component } from "react";


export default ({item})=>{
    let months = {
        '01': 'Jun',
        '02': "Feb",
        '03': "Mar",
        '04': "Apr",
        '05': "May",
        '06': "Jun",
        '07': "Jul",
        '08': "Aug",
        '09': "Sep",
        '10': "Oct",
        '11': "Nov",
        '12': "Dec"
    }

    let date=new Date(item)

    return String(date.getDate()).padStart(2, '0')+" "+months[String(date.getMonth() + 1).padStart(2, '0')]+', '+date.getFullYear();
}