import {uploadFile} from "../redux/actions/AttachmentAction";
import React, { useState} from "react";


export default function UploadFile(props) {

    let inputRef = "";
    const [name, setName] = useState("Select your file!");
    const [isValid, setValid] = useState(true);

    return <div>
        <input type="file"
               onInvalid={()=>{setValid(false)}}
               required={true}
               name={props.name}
               ref={(input) => {
            inputRef = input;
        }}
               hidden
               onChange={(item=>{
                   props.onChange(item);
                   setName(item.target.value.substr(12));
               })}/>

        <div className="d-flex upload-file-custom" onClick={(e) => {
            e.preventDefault();
            inputRef.click();
        }}>

            <div className={isValid?"upload-file-name":"upload-file-name upload-invalid"}>
                {name===""?"Select your file!":name}
            </div>
            <div className="upload-file-button">
                Upload
            </div>
        </div>
    </div>
}