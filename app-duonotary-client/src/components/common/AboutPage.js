import React, { Component } from "react";
import Footer from "../Footer";

import {
  BoxWrapper,
  ContentWrapper,
  WrapperTitle,
  Text,
  ScrollView,
  WrapperButton,
  Description,
} from "./AboutPage.elemetns";
import LeftMenu from "../LeftMenu";
import { connect } from "react-redux";
import Modal from "./modal";

class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          text1: "our",
          text2: "story",
          src: require("./assets/images/img_1.png"),
          isReverse: false,
          content:
            "We are a full-service agency that provides notary services, both onlines and in-person. Me Whichever way you prefer it, our Duo Notary personnel will be  youur service in no time. If it requires traveling to your location, then we would do so. If you decide to optio for our meals son notary public, we willll show up at your door in no time. All you need to do it let us know the documents that your want  notarize. You also get yours choose a time and place might convenient for you. Our mob notary services equally have their perks.",
          reverseColorOfTitle: false,
        },
        {
          text1: "Mobile",
          text2: "Notary",
          src: require("./assets/images/img_2.png"),
          isReverse: true,
          content:
            "If you decide to opt for our In-person notary public, we will show up at your door in no time. All you need to do is let us know, the documents, that you want to notarize. You also get to have choose a time and place convenien for you. Our mobile notary services equally have their perks. For instance your can saver yourself from traveling costs, waiting time and any inconvenience. All you need to do is let us know, the documents that your want to notarize. ou also get to have choose a time and place me convenien for you.",
          reverseColorOfTitle: true,
        },
        {
          text1: "Online",
          text2: "Notary",
          src: require("./assets/images/img_3.png"),
          isReverse: false,
          content:
            "Our online notarization is straightforward, as you will find on our service page. We are your guarantee you that your documents are entirely legal because Docu Sign verifies the your legitimay. We encourage our regular users to try out the online process because it is much more comfortable. We are you guarantees you that your documents are entirely legal been cause Docu Sign verifies the legitimay.  Learn more! All you need to do is let us know, them documents that you want to notarize.",
          reverseColorOfTitle: true,
        },
      ],
      loader: true,
      isOpen : false,
    };
  }

  handleChangeModal = () => {
    this.setState({isOpen : !this.state.isOpen})
  };

  componentDidMount() {
    setTimeout(() => {
      this.setState({ loader: false });
    }, 2000);
  }

  render() {
    const { modalShow, history, dispatch } = this.props;

    return (
      <div className='d-flex position-relative'>
        <i
          className='fas fa-bars position-absolute d-lg-none d-md-none'
          style={{
            top: "3%",
            right: "3%",
            fontSize: "24px",
            cursor: "pointer",
          }}
          onClick={this.handleChangeModal}
        />
        <LeftMenu
          onToggleModal={this.handleChangeModal}
          show={this.state.isOpen}
        />
        <ScrollView>
          <div className='about-us'>
            <p className='title'>About Us</p>
            {this.state.data.map((item) => (
              <Box key={Math.random().toString()} {...item} />
            ))}
            <Box
              text1='get'
              text2='Notary'
              sizeTitle={45}
              src={require("./assets/images/img_4.jpeg")}
              isReverse={true}
              content='Try one of our notary services online or in-person. Get “Welcome” discount on your 1st order.'
              reverseColorOfTitle={true}
              size={22}
              InnerComponent={Button}
            />
          </div>
          <Footer />
        </ScrollView>
        <Modal
          onToggleModal={this.handleChangeModal}
          visiblity={this.state.isOpen}
        />
      </div>
    );
  }
}

const Box = ({ isReverse = false, src, ...contentProps }) => (
  <BoxWrapper isReverse={isReverse}>
    <img src={src} className='img-fluid' />
    <Content {...contentProps} />
  </BoxWrapper>
);

const Content = ({
  content,
  size = 16,
  InnerComponent = null,
  ...titleProps
}) => {
  return (
    <ContentWrapper>
      <Title {...titleProps} />
      <Description size={size}>{content}</Description>
      {InnerComponent && <InnerComponent />}
    </ContentWrapper>
  );
};

const Button = () => {
  return <WrapperButton>Get a free quote</WrapperButton>;
};

const Title = ({
  text1,
  text2,
  color1 = "#B5BDC2",
  color2 = "#313E47",
  reverseColorOfTitle = false,
  sizeTitle = 32,
}) => (
  <WrapperTitle>
    <Text sizeTitle={sizeTitle} color={reverseColorOfTitle ? color2 : color1}>
      {text1}
    </Text>
    <Text sizeTitle={sizeTitle} color={reverseColorOfTitle ? color1 : color2}>
      {` ${text2}`}
    </Text>
  </WrapperTitle>
);

export default connect(({ auth: { modalShow } }) => ({ modalShow }))(About);
