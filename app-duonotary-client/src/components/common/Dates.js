import React, {Component} from 'react';
import clock from './assets/Vector-2.png'
import geo from './assets/Vector-1.png'
import email from './assets/Vector-3.png'
import phone from './assets/Vector.png'
import './Dates.scss';
import {Link} from "react-router-dom";
import {MailIcon, PhoneIcon} from "../Icons";

class Dates extends Component {
    render() {
        return (
                    <div className="row m-0">
                            <div className="col-md-6 colMd-6 p-0 d-flex">
                                <span className="icon icon-time"/>
                                <div className="flex-column ml-4">
                                    <span className='footer-font'>
                                    Working hours
                                </span>
                                    <div>
                                        <h6 className='dates-font m-0'>Mon-Fri 09:00AM to 05:00PM</h6>
                                        <h6 className='dates-font m-0'>Mon-Fri 08:00AM to 08:00PM</h6>
                                    </div>
                                </div>
                            </div>
                <div className="col-md-6 colMd-6 p-0 d-flex">
                    <MailIcon />
                    <div className="flex-column ml-4">
                        <span className='footer-font'>
                        Email
                    </span>
                        <div className=''>
                            <Link className="dates-font" to="mailto:info@duonotary.com">
                                info@duonotary.com
                            </Link>
                        </div>
                    </div>
                </div>
                            <div className="col-md-6 colMd-6 p-0 d-flex">
                                <PhoneIcon />
                                <div className="flex-column ml-4">
                                    <span className=' footer-font '>
                                    cities in America
                                </span>
                                    <div className='dates-font'>(603) 555-0123</div>
                                </div>
                            </div>
                        <div className="col-md-6 colMd-6 p-0 d-flex">

                            <div className="flex-column ml-5">
                                    <span className=' footer-font '>
                                    Follow us
                                </span>
                                <div className="icons dates-font d-flex">
                                    <div><i className="fab fa-youtube mr-2"/></div>
                                    <div><i className="fab fa-twitter mr-2"/></div>
                                    <div><i className="fab fa-instagram mr-2"/></div>
                                    <div><i className="fab fa-facebook"/></div>
                                </div>
                            </div>
                        </div>
                        </div>

        );
    }
}

export default Dates;