import React, {Component} from "react";
import * as authActions from "../../redux/actions/AuthActions";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {FormGroup, Input, Label} from "reactstrap/es";
import {Link} from "react-router-dom";
import {toast} from "react-toastify";
import Footer from "./footer";
import Location from "./location";
import Dates from "./Dates";
import "./modal.scss";
import QRApp from "./QRApp";
import {TOKEN} from "../../utils/constants";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";

export const modal_types = {
    SIGN_IN: "signIn",
    SIGN_UP: "signup",
};

class Modal extends Component {
    state = {
        modalType: modal_types.SIGN_IN,
        firstName: "",
        lastName: "",
        email: "",
        phoneNumber: "",
        password: "",
        prePassword: "",
        agree: false,
        showPassword_1: false,
        showPassword_2: false,
        showPassword_3: false,
        status: ''
    };

    handleSignIn = async (e, v) => {
        const exisist = this.changeLoading();
        const res = await this.props.dispatch(
            authActions.login({v, history: this.props.history})
        );
        if (exisist && res) this.props.dispatch(authActions.userMe());
        if (!res) this.changeLoading();
    };

    changeLoading = () => {
        this.props.onLogin && this.props.onLogin();
        return this.props.onLogin && true;
    };

    handleToggleModalType = () => {
        const {SIGN_IN, SIGN_UP} = modal_types;
        this.setState((prev) => ({
            modalType: prev.modalType === SIGN_IN ? SIGN_UP : SIGN_IN,
        }));
    };

    handleRegister = (e, history) => {
        e.preventDefault();

        let userInfo = {...this.state};
        if (this.state.agree)
            if (userInfo.password.length >= 6 && userInfo.prePassword.length <= 16)
                if (userInfo.password === userInfo.prePassword)
                    this.props.dispatch(
                        authActions.registerUser({...this.state, history})
                    );
                else toast.error("Password and pre password not equals!");
            else toast.error("Password length must be 6 and 16 characters!");
        else {
            toast.error(
                "Please, Sign up before please check confirm our conditions!"
            );
            this.setState({agree: false});
        }
    };

    handleChange = (e) => {
        const {name, value} = e.target;
        this.setState({[name]: value});
    };

    handleEnter = () => {
        let status = localStorage.getItem('role');
        switch (status) {
            case 'superAdmin' || 'admin':
                this.props.history.push('/admin');
                break;
            case 'agent':
                this.props.history.push('/agent');
                break;
            case 'user':
                this.props.history.push('/client/main-page');
        }
    }

    render() {
        const {SIGN_IN} = modal_types;
        const {
            firstName,
            lastName,
            email,
            phoneNumber,
            agree,
            modalType,
            showPassword_1,
            showPassword_2,
            showPassword_3,
        } = this.state;

        const {onToggleModal, visiblity, history} = this.props;
        return (
            <div className={!visiblity ? 'modalBlock' : 'modalBlock modalBlockIsOpen'}>
                <div
                    className={`sign-modal-container-bg`}>
                    <div className={"bg-content"}>
                        <Footer/>
                        <Location/>
                        <div className='pl-2 mt-3'>
                            <Dates/>
                        </div>
                    </div>
                </div>
                <div className={`sign-modal-container`}>
                    <div className='sign-modal-container-content h_100'>
                        <div className='flex-row justify-content-between align-items-center'>
                            <h2>Sign {modalType === SIGN_IN ? "in" : "up"}</h2>
                        </div>
                        <div className={"close-icon"} onClick={onToggleModal}>
                            <svg
                                width='19'
                                height='19'
                                viewBox='0 0 19 19'
                                fill='none'
                                xmlns='http://www.w3.org/2000/svg'>
                                <path
                                    d='M9.50007 7.43791L16.7188 0.219162L18.7809 2.28125L11.5622 9.49999L18.7809 16.7187L16.7188 18.7808L9.50007 11.5621L2.28132 18.7808L0.219238 16.7187L7.43799 9.49999L0.219238 2.28125L2.28132 0.219162L9.50007 7.43791Z'
                                    fill='#313E47'
                                />
                            </svg>
                        </div>
                        {modalType === SIGN_IN ? (
                            <FormGroup className='formFake'>
                                <AvForm onValidSubmit={this.handleSignIn}>
                                    <AvField
                                        className='modal-input'
                                        type='text'
                                        name='username'
                                        required
                                        placeholder='Enter username'
                                    />
                                    <div className={"icon_father"}>
                                        <i
                                            className={
                                                showPassword_1
                                                    ? "fas fa-eye"
                                                    : "fas fa-eye-slash"
                                            }
                                            onClick={() => {
                                                this.setState({showPassword_1: !showPassword_1});
                                            }}
                                        />
                                        <AvField
                                            className='modal-input'
                                            type={showPassword_1 ? "text" : "password"}
                                            name='password'
                                            required
                                            errorMessage={""}
                                            placeholder='Enter password'
                                        />
                                    </div>
                                    <Link className='text-decoration-none' to='/forgotPassword'>
                                        <span className='forgot-password'>Forgot password ?</span>
                                    </Link>
                                    <div className='d-flex align-items-center'>
                                        <button type='submit'>
                                            Sign in
                                        </button>
                                        {localStorage.getItem(TOKEN) &&
                                        <div
                                            className='goAccount'
                                            onClick={this.handleEnter}
                                        >
                                            <i className="fas fa-sign-in-alt"/>
                                            Go Account
                                        </div>
                                        }
                                    </div>
                                </AvForm>
                                <div className={"validData"}>
                                    <div className={"hasAccount"}>
                                        Don't have an account?
                                        <span onClick={this.handleToggleModalType}>Sign up</span>
                                    </div>
                                </div>
                            </FormGroup>
                        ) : (
                            <form onSubmit={(e) => this.handleRegister(e, history)}>
                                <Input
                                    className={"modal-input my-3"}
                                    defaultValue={firstName}
                                    required={true}
                                    onChange={this.handleChange}
                                    name='firstName'
                                    placeholder='First name'
                                />
                                <Input
                                    className={"modal-input my-3"}
                                    defaultValue={lastName}
                                    required={true}
                                    onChange={this.handleChange}
                                    name='lastName'
                                    placeholder='Last name'
                                />
                                <Input
                                    className={"modal-input my-3"}
                                    defaultValue={email}
                                    required={true}
                                    onChange={this.handleChange}
                                    name='email'
                                    type='email'
                                    placeholder='Email address'
                                />
                                <Input
                                    className={"modal-input my-3"}
                                    defaultValue={phoneNumber}
                                    required={true}
                                    onChange={this.handleChange}
                                    name='phoneNumber'
                                    type='text'
                                    placeholder='Phone number'
                                />
                                <div className={"icon_father"}>
                                    <i
                                        className={
                                            showPassword_3
                                                ? "fas fa-eye"
                                                : "fas fa-eye-slash"
                                        }
                                        onClick={() => {
                                            this.setState({showPassword_3: !showPassword_3});
                                        }}
                                    />
                                    <Input
                                        className={"modal-input my-3"}
                                        required={true}
                                        onChange={this.handleChange}
                                        name='password'
                                        type={showPassword_3 ? "text" : "password"}
                                        placeholder='Password'
                                    />
                                </div>
                                <div className={"icon_father"}>
                                    <i
                                        className={
                                            showPassword_2
                                                ? "fas fa-eye"
                                                : "fas fa-eye-slash"
                                        }
                                        onClick={() => {
                                            this.setState({showPassword_2: !showPassword_2});
                                        }}
                                    />
                                    <Input
                                        className={"modal-input my-3"}
                                        required={true}
                                        onChange={this.handleChange}
                                        name='prePassword'
                                        type={showPassword_2 ? "text" : "password"}
                                        placeholder='Repeat password'
                                    />
                                </div>
                                <FormGroup check>
                                    <Label check className='agreement'>
                                        <Input
                                            className='agreeCheck'
                                            type='checkbox'
                                            checked={agree}
                                            onChange={() => this.setState({agree: !agree})}
                                        />
                                        Creating an account means you’re okay with our{" "}
                                        <Link to='/termsofservices' target='_blank'>
                                            Terms of Service, Privacy Policy
                                        </Link>
                                        , and our default{" "}
                                        <Link to='/notificationsettings' target='_blank'>
                                            Notification Settings
                                        </Link>
                                        .
                                    </Label>
                                </FormGroup>
                                <button type={"submit"} disabled={!agree}>
                                    Sign up
                                </button>
                                <div className={"validData"}>
                                    <div className={"hasAccount"}>
                                        Already have an account?
                                        <span onClick={this.handleToggleModalType}>Sign in</span>
                                    </div>
                                </div>
                            </form>
                        )}
                        <div className='qrBlock'>
                            <QRApp/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(connect()(Modal));
