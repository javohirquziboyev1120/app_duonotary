import React, {Component} from 'react';
import './Services.scss';
import Service from "./page/service";
import {config} from './../../utils/config';

const data = [
    {
        id: 1,
        title: 'Apostile service',
        url: "/inPerson"
    }, {
        id: 2,
        title: 'Real Estate',
        url: "/inPerson"
    }, {
        id: 3,
        title: 'Embassy Legalizations',
        url: "/inPerson"
    }, {
        id: 4,
        title: 'International',
        url: "/inPerson"
    },
]


class Services extends Component {
    render() {
        return (
            <div className='serves'>
                <div className='logos flex-column justify-content-between align-items-start'>
                    <div>
                        <img src="/assets/img/logo.png" alt="duo-notary logo"/>
                    </div>
                    <span className="sub-text">
                        Popular services
                    </span>
                </div>
                    {data.map(k => <Service key={k.id} title={k.title} url={k.url}/>)}

            </div>
        );
    }
}

export default Services;