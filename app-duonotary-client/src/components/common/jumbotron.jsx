import React from "react";
import styled from "styled-components";
import {Text} from "./text";
import {Title} from "../title";
import './jumbotron.scss';

const IconPlayMarket = ({width = '4vw', height = '5vh', color = "#384049"}) => (
    <svg
        width={width}
        height={height}
        viewBox="0 0 30 38"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
    >
        <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M22.1872 0.660522C20.3536 0.737419 18.1318 1.88187 16.8168 3.42043C15.6348 4.78681 14.6029 6.96743 14.8811 9.0583C16.9283 9.21848 19.0143 8.01929 20.2886 6.47995C21.5604 4.93785 22.4214 2.79769 22.1872 0.660522ZM21.669 37.0489C24.1534 37.0026 25.727 34.7951 27.2482 32.5738C28.9149 30.141 29.651 27.7821 29.7608 27.4301C29.7669 27.4105 29.7711 27.3971 29.7734 27.3903C29.7723 27.3898 29.7692 27.3886 29.7643 27.3866C29.5259 27.2897 24.9268 25.4219 24.8811 20.0221C24.8362 15.5626 28.3963 13.3445 28.7872 13.101C28.8007 13.0926 28.8104 13.0865 28.8161 13.0828C26.6739 9.95284 23.3383 9.52372 22.1498 9.47467C20.4137 9.29928 18.7295 9.96229 17.3534 10.504C16.4789 10.8482 15.7288 11.1435 15.1689 11.1435C14.5458 11.1435 13.7752 10.8379 12.9103 10.495C11.7784 10.0462 10.4851 9.53333 9.14982 9.55787C6.0571 9.60417 3.2031 11.3562 1.61037 14.1254C-1.60602 19.7013 0.785566 27.9638 3.91891 32.4849C5.45265 34.6962 7.2764 37.1848 9.67485 37.0952C10.7457 37.0523 11.5078 36.7281 12.296 36.3927C13.207 36.0052 14.1527 35.6028 15.6501 35.6028C17.0762 35.6028 17.9781 35.9927 18.8457 36.3678C19.6754 36.7265 20.4737 37.0716 21.669 37.0489Z"
            fill={color}
        />
    </svg>
);
const IconAppStore = ({width = '4vw', height = '5vh', color = "#384049"}) => (
    <svg
        width={width}
        height={height}
        viewBox="0 0 33 36"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
    >
        <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M0.759277 2.50606V32.6512C0.759277 33.7005 1.37445 34.6077 2.2681 35.0405L19.9582 17.5735L2.2752 0.113281C1.37776 0.544768 0.759277 1.45412 0.759277 2.50606ZM6.40613 33.9137L25.605 23.1915L21.5478 19.2668L6.40613 33.9137ZM31.3884 15.997C31.9917 16.4908 32.3809 17.2689 32.3809 18.144C32.3809 19.006 32.0035 19.7736 31.4157 20.2683L27.5626 22.6469L23.3462 18.1388L27.5598 13.6334L31.3884 15.997ZM25.605 11.9622L6.40613 1.23986L21.5478 15.8868L25.605 11.9622Z"
            fill={color}
        />
    </svg>
);

export const Jumbotron = () => {
    return (
        <div className='wrapper'>
            <div className="left_wrapper">
                <div className="header">Download<br/> mobile app</div>
                <p className="content_span">
                    Did you know that DuoNotary offers mobile applications for users, so
                    you can schedule an appointments in advance and complete all the
                    necessary process through friendly application?
                </p>
                <div className="btns">
                    <button className='buttons'>
                        <IconAppStore/>
                        <span className='ml-2'>Google play</span>
                    </button>
                    <button className='buttons'>
                        <IconPlayMarket/>
                        <span className='ml-2'>App store</span>
                    </button>
                </div>
            </div>
            <div className="right_wrapper">
                <img src="/assets/img/phone-2.png" className="imgPhone" alt="phone_img"/>
            </div>
        </div>
    );
};
