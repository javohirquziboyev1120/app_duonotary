import React, { Component } from "react";
import {
  Img,
  ImgWrap,
  ReviewWrap,
  Description,
  Fotter,
  Avatar,
  FullName,
  CompanyName,
  Column,
  Wrapper,
} from "./SliderIcons/carousel.elements";
import { UniversalSlider } from "./SliderIcons/carousel";
import "./team.scss";
import styled from "styled-components";
import { Title } from "../title";
import { config } from "../../utils/config";
import _ from "lodash";

export class TeamContent extends Component {
  getItems = (items, pageSize) => {
    const data = [];
    for (let i = 0; i < items.length; i += pageSize) {
      data.push(_(items).slice(i).take(pageSize).value());
    }
    return data;
  };

  renderReviews = (items = []) => {
    items = items.map(({ photo, companyName, fullName, description }) => (
        <ReviewWrap key={Math.random()}>
          <Description>{description}</Description>
          <Fotter>
            <Avatar
                src={
                  photo
                      ? config.BASE_URL + "/attachment/" + photo
                      : "/assets/img/avatar.png"
                }
                alt='img'
            />
            <FullName>{fullName} </FullName> {" - "}
            <CompanyName> {companyName}</CompanyName>
          </Fotter>
        </ReviewWrap>
    ));

    const columns = this.getItems(items, 2).map((item) => (
        <Column key={Math.random()}>{item}</Column>
    ));
    const wrappers = this.getItems(columns, 3).map((item) => (
        <Wrapper key={Math.random()}>{item}</Wrapper>
    ));
    return wrappers.length > 0 ?wrappers: [];
  };

  renderPartners = (items = []) => {
    const partners =
        items.length > 0
            ? items.map(({ attachmentId: id }) => (
                <ImgWrap key={Math.random().toString()}>
                  <Img
                      src={config.BASE_URL + "/attachment/" + id}
                      alt='partnior_logo'
                  />
                </ImgWrap>
            ))
            : [];
    return partners.length > 0 ? partners : [];
  };

  render() {
    const reviews = this.renderReviews(this.props.reviews);
    const partners = this.renderPartners(this.props.partners);
    console.log("reviews",reviews)
    return (
        <div className={"wrapper3"}>
          <div className='top_wrapper'>
          <span className="header">
            What our customers have to say about us
          </span>
            {reviews.length > 0 && <UniversalSlider items={reviews} />}
            <br />
            <span className="header">
            Our partners
          </span>
            {partners.length > 0 && <UniversalSlider items={partners}/>}
          </div>
        </div>
    );
  }
}
