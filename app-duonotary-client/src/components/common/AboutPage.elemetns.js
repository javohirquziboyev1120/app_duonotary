import styled from "styled-components";

export const WrapperButton = styled.div`
  background: #04a6fb;
  border-radius: 2px;
  padding: 1rem 3rem;
  margin-top: 2.5rem;
  cursor: pointer;

  font-weight: 600;
  font-size: 20px;
  line-height: 20px;
  color: #fff;
  transition: 0.25s ease-in-out all;
  text-transform: uppercase;

  &:active {
    transform: scale(0.95);
  }

  @media screen and (max-width: 960px) {
    width: 100%;
    text-align: center;
  }

  @media only screen and (max-width: 1920px) and (min-width: 1400px) {
    font-size: 30px;
    padding: 2rem 3rem;
  }
`;

export const ScrollView = styled.div`
  display: flex;
  flex-direction: column;
  overflow-y: scroll;
  height: 100vh;
  width: 100%;
  background: #f9fafb;
  .about-us {
    width: 100%;
    .title {
      font-style: normal;
      font-weight: normal;
      font-size: 55px;
      line-height: 122.52%;
      letter-spacing: -0.05em;
      text-transform: uppercase;
      color: #313e47;
      margin: 1rem 6rem;
      @media screen and (max-width: 960px) {
        text-align: center;
        margin: 0;
      }

      @media screen and (max-width: 1920px) {
        font-size: 65px;
      }
    }
  }
`;

export const BoxWrapper = styled.div`
  padding: 0 6rem;
  display: flex;
  background: #fff;
  justify-content: flex-end;
  flex-direction: ${({ isReverse }) => (isReverse ? "row-reverse" : "row")};
  margin-bottom: 3rem !important;
  img {
    width: 50%;
    padding: 0 2rem 0 0;
    object-fit: cover;
  }

  @media screen and (max-width: 960px) {
    flex-direction: column;
    margin: 3rem 0;
    padding: 0 3rem;

    img {
      margin-top: 2rem;
      width: 100%;
      padding: 0;
    }
  }

  @media screen and (max-width: 1920px && (min-width: 1440px)) {
    margin-bottom: 5rem !important;
  }
`;

export const ContentWrapper = styled.div`
  padding: 2rem 2rem 2rem 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;

  @media screen and (max-width: 960px) {
    padding: 1rem 0;
  }
`;
export const Description = styled.span`
  font-style: normal;
  font-weight: normal;
  font-size: ${({ size }) => size}px;
  line-height: 28px;
  display: flex;
  align-items: center;
  letter-spacing: -0.02em;
  color: #384049;
  text-align: justify;
  @media screen and (max-width: 960px) {
    font-size: 20px !important;
  }
  @media only screen and (min-width: 1460px) and (max-width: 1920px) {
    font-size: 25px;
  }
`;
export const Text = styled.span`
  font-weight: normal;
  font-size: ${({ sizeTitle }) => sizeTitle}px;
  line-height: 122.52%;
  letter-spacing: -0.05em;
  text-transform: uppercase;
  color: ${({ color }) => color};
  @media screen and (max-width: 960px) {
    font-size: 35px !important;
    margin: 0.5rem 0;
  }
  @media screen and (max-width: 1920px) {
    font-size: 55px !important;
    margin: 1rem 0;
  }
`;

export const WrapperTitle = styled.div`
  margin-bottom: 0.5rem;

  @media screen and (max-width: 960px) {
    text-align: center;
    width: 100%;
  }
`;
