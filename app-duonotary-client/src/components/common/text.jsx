import styled from "styled-components";

export const Text = styled.span`
   display:block;
  font-style: normal;
  font-weight: normal;
  font-size: ${({ size }) => size}vw !important;
   line-height: 8vh;
   @media screen  and (max-width: 556px){
            font-size: 6vw;
            line-height: 4.5vh;
    }
  text-transform: capitalize;
  color: ${({ color }) => color};
`;
