import React, {Component} from 'react';
import arrow from '../assets/Vector.svg';
import {Link} from "react-router-dom";

class Service extends Component {

    render() {
        return (

                    <Link to={this.props.url}  className="service-link">
                        <span>{this.props.title}</span>
                        <span className='icon icon-link'/>
                    </Link>
        );
    }
}

export default Service;