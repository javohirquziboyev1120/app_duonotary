import React, {Component} from 'react';

class Location extends Component {
    render() {
        return (
            <div className="row m-0">
                <div className="col-md-12">
                    <div style={{width: '50vw'}}>
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d17393.557328233128!2d-73.98279875762846!3d40.76305396132059!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c258f3ae5c307b%3A0x8837aff801306c08!2sSheep%20Meadow!5e0!3m2!1suz!2s!4v1605164085410!5m2!1suz!2s"
                            frameBorder="0"
                            style={{width: '100%', height: '32vh'}}
                            className='border-0'
                            allowFullScreen/>
                        {/*<iframe*/}
                        {/*    src="https://yandex.com/map-widget/v1/-/CCUAuPT6XA"*/}
                        {/*    frameBorder="0"*/}
                        {/*    style={{width: '100%',height: '219px'}}*/}
                        {/*    className='border-0'*/}
                        {/*    allowFullScreen>*/}
                        {/*</iframe>*/}
                    </div>
                </div>
            </div>

        );
    }
}

export default Location;