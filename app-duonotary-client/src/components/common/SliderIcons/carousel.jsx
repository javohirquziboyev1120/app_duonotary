import React from "react";
import AliceCarousel from "react-alice-carousel";
import "react-alice-carousel/lib/alice-carousel.css";

const responsive = {
  0: { items: 1 },
};

export const UniversalSlider = ({ items }) => (
  <AliceCarousel
    mouseTracking
    items={items}
    responsive={responsive}
    autoPlay
    infinite={false}
    animationDuration={1500}
    disableButtonsControls
    disableSlideInfo
  />
);
