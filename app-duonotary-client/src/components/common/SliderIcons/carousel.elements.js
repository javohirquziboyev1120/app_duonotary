import styled from "styled-components";

export const CarouselContainer = styled.div`
  margin: 1rem 0 0.5rem 0;
  padding: 2rem 0;
  border-top: 1px solid #d8dbde;
`;

export const ImgWrap = styled.div`
  display: flex;
  align-items: center;
`;
export const Img = styled.img`
  max-width: 90%;
  padding: 8px 6px;
  max-height: 220px;
  object-fit: cover;
`;

export const Wrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  justify-content: center;
  grid-gap: 1rem;
  align-items: start;
  padding: 1rem;
  grid-template-rows: auto;
`;
export const Column = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  row-gap: 1rem;
`;
export const ReviewWrap = styled.div`
  box-sizing: border-box;
  border-radius: 10px;
  padding: 1rem;
  border: 1px solid rgba(0, 0, 0, 0.2);
`;

export const Description = styled.div`
  font-size: 14px;
  line-height: 28px;
  letter-spacing: -0.01em;
  color: #384049;
  padding-bottom: 0.5rem;
  padding-right: 1.5em;
  text-align: justify;
`;

export const Fotter = styled.div`
  padding: 0.8rem 0 0.4rem;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  border-top: 1px solid #d8dbde;
`;

export const Avatar = styled.img`
  width: 40px;
  height: 40px;
  border-radius: 50%;
`;
export const FullName = styled.span`
  font-size: 16px;
  line-height: 20px;
  letter-spacing: -0.03em;
  color: #384049;
  font-weight: bold;
  margin: 0 0.5rem;
`;
export const CompanyName = styled.span`
  font-size: 16px;
  line-height: 20px;
  letter-spacing: -0.03em;
  color: #384049;
  margin: 0 0.5rem;
`;
