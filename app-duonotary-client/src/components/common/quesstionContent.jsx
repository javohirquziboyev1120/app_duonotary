import React, {useState} from "react";
import './questions.scss';

export const QuesstionContent = () => {
    const [active, setActive] = useState(0);
    const data = useState([
        {
            title: "Please provide an address",
            content:
                "“All our staff members are fully qualified to serve you. We have been in the industry for several decades, and we have a substantial experience to attend to all situations”.",
        },
        {
            title: "Resourceful",
            content:
                "We are into virtually anything that requires notarization. Duo Notary provides notaries for real estate and planning, financial deals, immigration, websites, contracts, power of attorneys, etc.",
        },
        {
            title: "Reliable",
            content:
                "If you are looking for a notary agency that will be available 24/7, and  always ready  to give you the best experience, Duo Notary is the name to call.",
        },
        {
            title: "Affordable quotes",
            content:
                "Despite the superiority of our services, Duo Notary quotes are in expensive, and we do not include any hidden fees. Our fees cost-effective and they cover all costs.",
        },
    ])[0];
    return (
        <div className='wrapper2 w-100'>
            <div className="left_wrapper w-50">
                <div className='header'>Why to choose duo notary</div>
                {data.map(({title, content}, index) => (
                        <div onClick={()=>{setActive(index)}} className={index===active?'boxes block-box-active': 'boxes'}>
                            <div className={index===active?"title active-title": "title"}>{title}</div>
                            <span className={index===active?"content active-content":"content"}>{content}</span>
                        </div>
                ))}
            </div>
            <div className="right_wrapper w-50">
                <img src="/assets/img/phone-1.png" alt="vector_meet"/>
            </div>
        </div>
    );
};
