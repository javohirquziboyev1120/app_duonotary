import React, {Component} from 'react';
import './footer.scss';
import {Link} from "react-router-dom";
import {connect} from "react-redux";

class Footer extends Component {
    constructor(props) {
        super(props);
    }


    handleChangeModal = () => {
        this.props.dispatch({
            type: "updateState",
            payload: {
                modalShow: false,
            },
        });
    };

    render() {
        return (
            <div className="row m-0 foot_block">
                <div className="col-md-4 d-flex align-items-start justify-content-start h_100 ">
                    <ul className="list-unstyled h_100">
                        <li className='pb-1'>
                            <h5 className="text-dark font-weight-bold">Services</h5>
                        </li>
                        <li className='pb-1'>
                            <Link onClick={this.handleChangeModal} to="/inPerson" className='footer-font text-decoration-none'>Traveling Notary </Link>
                        </li>
                        <li className='pb-1'>
                            <Link onClick={this.handleChangeModal} to="/online" className='footer-font text-decoration-none'>Online Notary</Link>
                        </li>
                        <li className='pb-1'>
                            <Link onClick={this.handleChangeModal} to="" className='footer-font text-decoration-none'>Apositille</Link>
                        </li>
                        <li className='pb-1'>
                            <Link onClick={this.handleChangeModal} to="" className='text-decoration-none footer-font'>Embassy Legalization</Link>
                        </li>
                        <li className='pb-1'>
                            <Link onClick={this.handleChangeModal} to="" className='text-decoration-none footer-font'>Real Estate/Refinance</Link>
                        </li>
                        <li className='pb-1'>
                            <Link onClick={this.handleChangeModal} to="" className='text-decoration-none footer-font'>I-9 Verification</Link>
                        </li>
                    </ul>
                </div>
                <div className="col-md-4 d-flex align-items-start justify-content-center ">
                    <ul className="list-unstyled">
                        <li className='pb-1'>
                            <h5 className="text-dark font-weight-bold">Areas we Serve</h5>
                        </li>
                        <li className='pb-1'>
                            <Link onClick={this.handleChangeModal} to="" className='footer-font text-decoration-none'>New York City</Link>
                        </li>
                        <li className='pb-1'>
                            <Link onClick={this.handleChangeModal} to="" className=' footer-font text-decoration-none'>Baltimore</Link>
                        </li>
                        <li className='pb-1'>
                            <Link onClick={this.handleChangeModal} to="" className=' footer-font text-decoration-none'>Washington DC</Link>
                        </li>
                        <li className='pb-1'>
                            <Link onClick={this.handleChangeModal} to="" className=' footer-font text-decoration-none'>Prince Month.</Link>
                        </li>
                        <li className='pb-1'>
                            <Link onClick={this.handleChangeModal} to="" className=' footer-font text-decoration-none'>N.Virginia</Link>
                        </li>
                    </ul>
                </div>
                <div className="col-md-4 d-flex align-items-start justify-content-center ">
                    <ul className="list-unstyled">
                        <li className='pb-1'>
                            <h5 className="text-dark font-weight-bold">Pages</h5>
                        </li>
                        <li className='pb-1'>
                            <Link onClick={this.handleChangeModal} to="/about" className=' footer-font text-decoration-none'>About Us</Link>
                        </li>
                        <li className='pb-1'>
                            <Link onClick={this.handleChangeModal} to="/blog" className=' footer-font text-decoration-none'>Blog</Link>
                        </li>
                        <li className='pb-1'>
                            <Link onClick={this.handleChangeModal} to="" className=' footer-font text-decoration-none'>How it works?</Link>
                        </li>
                        <li className='pb-1'>
                            <Link onClick={this.handleChangeModal} to="/termsofservices" className=' footer-font text-decoration-none'>Terms &
                                Conditions</Link>
                        </li>
                        <li className='pb-1'>
                            <Link onClick={this.handleChangeModal} to="/privacypolicy" className=' footer-font text-decoration-none'>Privacy Terms</Link>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default connect()(Footer);