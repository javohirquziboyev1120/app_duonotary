import React from 'react';
import './CloseBtn.scss'

function CloseBtn(props) {
    return (
        <div className={'closeBtn'} onClick={props.click}>
            <img src="/assets/icons/close.svg" alt="#btn"/>
        </div>
    );
}

export default CloseBtn;