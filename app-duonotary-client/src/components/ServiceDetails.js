import React from "react";

export default function ServiceDetails({name,count,price}) {



    return <div className="w-100 d-flex flex-column">
        <span style={{
            fontWeight: "500",
            fontSize: "20px",
            color: "#313E47",
        }}>{name}:</span>
        <div className="d-flex mt-2 justify-content-between">
            <span
            style={{
                fontWeight: "bold",
                fontSize: "18px",
                color: "#04A6FB",
            }}
            >Count : {count}</span>
            <span
            style={{
                fontWeight: "bold",
                fontSize: "18px",
                color: "#313E47",
            }}
            >Price : ${price}</span>
        </div>
    </div>
}