import {Col, Collapse, CustomInput, Row} from "reactstrap";
import International from "../International";
import RealEstateComponent from "../RealEstateComponent";
import React from "react";
import Counter from "../Counter";
import * as types from "../../redux/actionTypes/OrderActionTypes";
import {getAdditionalServicePrices, getPricingByServicePrice} from "../../redux/actions/OrderAction";
import {nextStage} from "./EditOrder";
import "./order.scss"
import {toast} from "react-toastify";
import warning from "react-redux/lib/utils/warning";

export default ({
                    servicePrices, servicePrice, dispatch, countDocument, countries, isApostille, documentTypes,
                    embassyCountries, orderAdditionalServices, additionalServicePrices, changeStage, pricingList
                }) => {

    function change(v, i) {
        let oldArr = [...orderAdditionalServices];
        if (orderAdditionalServices[0]) {
            orderAdditionalServices.map((value, i) => {
                if (value.additionalServicePriceId === v.id)
                    oldArr.splice(i, 1)
            })
        }
        oldArr.push({price: v.price * i, count: i, additionalServicePriceId: v.id})
        dispatch({
            type: 'updateState',
            payload: {
                orderAdditionalServices: oldArr
            }
        })

    }

    let realEstateSubmit = (v) => {
        dispatch({type: 'updateStateOrder', payload: {realEstateDto: v}})
        nextStage(3,dispatch)
    }
    let interNationalSubmit = (v) => {
        let country = {};
        let documentType = {};
        documentType.id = v.documentType;
        country.id = v.country;
        v.documentType = documentType;
        v.country = country;
        v.embassy = !isApostille;
        dispatch({type: 'updateState', payload: {internationalDto: v}})
        nextStage(3,dispatch)
    }

    let getFormOfService = (item) => {
        return (
            <div className="p-4 row-service d-flex align-items-center w-100 justify-content-between">
                <div className="justify-content-center">
                    <label className='labelDuo w-100 h-100'>
                        <div className='labelBlock' style={{left : '35%', top : '40%'}}>
                            {item.id===servicePrice.id && <div className='labelBox'>
                                <i className="fas fa-check"/>
                            </div>}
                        </div>
                        <CustomInput
                            className='d-none'
                            onChange={() => selectCurrent(servicePrice ? '' : item)}
                            checked={item.id===servicePrice.id}
                            type="checkbox"
                            id={item.id}
                        />
                    </label>
                </div>
                <div >
                    <p className="h4">{item.service.subService.name + " - " + item.price + "$"}</p>
                </div>
                <div>
                    <div className={!!servicePrice ? "down-icon" : "up-icon"}>
                        <div onClick={function () {
                            document.getElementById(item.id).click()
                        }}>
                            <img src="/assets/img/down.png" alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    let selectCurrent = (item) => {
        dispatch({
            type: types.SET_SERVICE_PRICE,
            payload: item
        })
        if (item) {
            dispatch(getPricingByServicePrice(item.id))
            dispatch(getAdditionalServicePrices(item.id))
        }
    }

    function price() {
        let one = 0;
        if (!!pricingList[0]) {
            pricingList.map(price => {
                if (price.everyCount != null && price.everyCount > 0) {
                    one = price.price;
                } else if (!price.everyCount && price.fromCount <= countDocument && price.tillCount >= countDocument) {
                    one = price.price
                } else if (!price.everyCount && price.fromCount <= countDocument && !price.tillCount) {
                    one = price.price
                }
            })
        }
        if (!!orderAdditionalServices[0])
            orderAdditionalServices.map(v => {
                one = one + v.price
            })
        one = servicePrice ? one * countDocument + servicePrice.price : '0'
        return one
    }

    return <Row className="order-step-three">
        <Col md={12}>
            <div className="d-flex flex-column justify-content-between">
                <Row>
                    {!servicePrice && servicePrices ? servicePrices.map(item =>
                        <Col key={item.id} md={12}>
                            {item.service.subService.serviceEnum === "REAL_ESTATE" ?
                                getFormOfService(item)

                                : item.service.subService.serviceEnum === "INTERNATIONAL" ?
                                    getFormOfService(item)

                                    : item.service.subService.serviceEnum === "OTHERS" ?
                                        getFormOfService(item)
                                        : ''}
                        </Col>):<Col md={12}>
                        {servicePrice?getFormOfService(servicePrice):'Services not found'}
                    </Col>}
                </Row>

                <Collapse isOpen={!!servicePrice}>
                    {servicePrice && servicePrice.service.subService.serviceEnum === "OTHERS" ?
                        <Row className="px-5 row-additional py-2 m-0">
                            <Col md={4}>Count Document:</Col>
                            <Col md={4}>
                                <Counter
                                value={countDocument}
                                getValue={i=>dispatch({
                                    type: 'updateStateOrder',
                                    payload:{countDocument:i}
                                })}
                            /></Col>
                            <Col hidden={!additionalServicePrices[0]} md={12}>Additional services:</Col>
                            {!!servicePrice && !!additionalServicePrices[0] ? additionalServicePrices.map(v =>
                                <Col md={4} className="card" key={v.id}>
                                    <Row>
                                        <Col md={3} className="text-center">
                                            <Counter
                                                value={!JSON.stringify(orderAdditionalServices[0] === JSON.stringify([])) ? orderAdditionalServices.filter(o => o.additionalServicePriceId === v.id)[0].count : 0}
                                                getValue={i => change(v, i)}
                                            />
                                        </Col>

                                        <Col md={9} className="name">
                                            {v.additionalServiceDto.name + " - $" + v.price}
                                        </Col>
                                    </Row>
                                </Col>) : ''}
                            <Col md={12}>
                                <Row className='my-3'>
                                    <Col className="col-md-2 justify-content-center">
                                        <div className="next-step-btn"
                                             onClick={() => dispatch({type: 'previousStep'})}>Previous
                                            step
                                        </div>
                                    </Col>
                                    <Col className="col-md-2 ml-auto justify-content-center">
                                        <div className="next-step-btn"
                                             onClick={() => countDocument>0?nextStage({stage:3}, dispatch):toast.warning("Please enter count document")}>Next
                                            step
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>

                        : servicePrice && servicePrice.service.subService.serviceEnum === "INTERNATIONAL" ?
                                <div>
                                    <International
                                        isAdd={true}
                                        submit={interNationalSubmit}
                                        dispatch={dispatch}
                                        embassyCountries={embassyCountries}
                                        documentTypes={documentTypes}
                                        isApostille={isApostille}
                                        countries={countries}
                                        servicePrice={servicePrice}
                                        additionalServicePrices={additionalServicePrices}
                                        orderAdditionalServices={orderAdditionalServices}
                                    />
                                </div>
                             :

                            servicePrice && servicePrice.service.subService.serviceEnum === "REAL_ESTATE" ?
                                    <RealEstateComponent
                                        servicePrice={servicePrice}
                                        additionalServicePrices={additionalServicePrices}
                                        orderAdditionalServices={orderAdditionalServices}
                                        isAdd={true}
                                        dispatch={dispatch}
                                    /> : ""}
                </Collapse>
            </div>
        </Col>


    </Row>
}


