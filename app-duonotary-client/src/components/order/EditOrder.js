import React from "react";
import {connect} from "react-redux";
import Address from "./Address";
import DateTime from "./DateTime";
import OrderInfoComponent from "./OrderInfoComponent";
import './order.scss'
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import LastStep from "./LastStep";
import {editOrder, getOrderAmount, getOrderPriceAmounts} from "../../redux/actions/OrderAction";
import FourStep from "./FourStep";
import OrderInfoOnline from "./OrderInfoOnline";

class EditOrder extends React.Component {

    getNewOrderAmount = () => {
        this.props.dispatch(getOrderAmount({
            servicePriceId: this.props.servicePrice.id,
            clientId: this.props.order.client.id,
            countDocument: this.props.countDocument,
            timeTableId: this.props.timeTable.id,
            orderAdditionalServiceDtoList: this.props.orderAdditionalServices,
            zipCodeId: this.props.zipCode ? this.props.zipCode.id : ''
        }))
    }
    confirmChange = () => {
        this.saveEditOrder();
        if (this.props.allDiscounts == null) {
            this.props.saveEditOrder()
            this.props.dispatch(getOrderPriceAmounts({
                servicePriceId: this.props.servicePrice.id,
                clientId: this.props.order.client ? this.props.order.client.id : '',
                countDocument: this.props.countDocument,
                timeTableId: this.props.timeTable.id,
                orderAdditionalServiceDtoList: this.props.orderAdditionalServices,
                zipCodeId: this.props.zipCode ? this.props.zipCode.id : ''
            }))
        }
        this.props.dispatch({
            type: "updateState",
            payload: {
                stage: 5,
                allDiscounts: null
            }
        })

    }
    saveEditOrder = () => {
        let amountDiscountSum = 0.0;
        if (this.props.discountCount && this.props.discountCount.length > 0) {
            for (let i = 0; i < this.props.discountCount.length; i++) {
                amountDiscountSum += this.props.discountCount[i].amount;
            }
            if (amountDiscountSum > this.props.discountCount[0].needAmount) {
                amountDiscountSum = this.props.discountCount[0].needAmount;
            }
        }
        this.props.dispatch(editOrder({
            discountType: !!this.props.allDiscounts,
            stage: this.props.stage,
            state: this.props.orderState, order: {
                id: this.props.order ? this.props.order.id : null,
                realEstateDto: this.props.realEstateDto,
                internationalDto: this.props.internationalDto,
                address: this.props.address,
                servicePriceId: this.props.servicePrice.id,
                agentId: this.props.timeTable.agentId,
                countDocument: this.props.countDocument,
                timeTableId: this.props.timeTable.id,
                discountAmount: amountDiscountSum,
                discountsDtos: this.props.discountCount,
                orderAdditionalServiceDtoList: this.props.orderAdditionalServices,
                titleDocument: this.props.titleDocument,
                lat: this.props.address.lat,
                lng: this.props.address.lng,
                zipCodeId: this.props.zipCode ? this.props.zipCode.id : '',
                clientId: this.props.order.client.id,
                docAttachmentId: this.props.attachmentIdsArray.filter(item => item.key === "real")[-1] != null && this.props.attachmentIdsArray.length > 0 ? [this.props.attachmentIdsArray.filter(item => item.key === "real")[0].value] : []
            }
        }))
    }

    render() {
        const {
            titleDocument, attachmentIdsArray, dispatch, checkbox1, checkbox2, pricingList, street_address, code,
            timeZone, outOfService, loading, address, stage, times, time, countDocument, selectedDate, order, timeTable,
            embassyCountries, orderState, openEditModal, amount, allDiscounts, discountCount, discountNeed, step,
            documentTypes, countries, isApostille, servicePrice, additionalServicePrices, orderAdditionalServices,
            servicePrices, timesPercent, zipCode
        } = this.props;

        const showInPersonOrderForm = () => {
            if (stage === 0) {
                nextStage({stage: 1}, dispatch)
            }
            switch (stage) {
                case 1:
                    return (
                        <Address
                            dispatch={dispatch}
                            loading={loading}
                            address={address}
                            outOfService={outOfService}
                            code={code}
                            street_address={street_address}
                        />
                    );
                case 2:
                    return (
                        <OrderInfoComponent
                            dispatch={dispatch}
                            embassyCountries={embassyCountries}
                            documentTypes={documentTypes}
                            countries={countries}
                            isApostille={isApostille}
                            servicePrice={servicePrice}
                            additionalServicePrices={additionalServicePrices}
                            countDocument={countDocument}
                            orderAdditionalServices={orderAdditionalServices}
                            pricingList={pricingList}
                            servicePrices={servicePrices}
                        />
                    );
                case 3:
                    return (
                        <DateTime
                            dispatch={dispatch}
                            loading={loading}
                            times={times}
                            timesPercent={timesPercent}
                            time={time}
                            countDocument={countDocument}
                            servicePrice={servicePrice}
                            zipCode={zipCode}
                            selectedDate={selectedDate}
                            online={false}
                            nextStep={4}
                            order={order}
                        />
                    );
                case 4:
                    if (amount === -1) {
                        this.getNewOrderAmount()
                    }
                    if (allDiscounts == null) {
                        getAllDiscounts()
                    }
                    return (
                        <FourStep
                            amount={amount}
                            isHaveDiscount={order.discountAmount && (order.amount !== amount || order.zipCode.id !== zipCode.id)}
                            dispatch={dispatch}
                            loading={loading}
                            stage={4}
                            saveEditOrder={this.saveEditOrder}
                            confirmChange={this.confirmChange}
                        />
                    );
                case 5:
                    return (
                        <LastStep
                            stage={5}
                            allDiscounts={allDiscounts}
                            discountNeed={discountNeed}
                            changeDiscount={changeDiscount}
                            saveEditOrder={this.saveEditOrder}
                            discountCount={discountCount}
                            dispatch={dispatch}
                        />
                    );
                default:
                    (console.log('This is a multi-step form built with React.'))
            }
        }

        const showOnlineOrder = () => {
            if (step===0) {
                nextStage({step: 1}, dispatch)
            }
            switch (step) {
                case 1:
                    return (
                        <OrderInfoOnline
                            dispatch={dispatch}
                            loading={loading}
                            titleDocument={titleDocument}
                            attachmentIdsArray={attachmentIdsArray}
                            checkbox1={checkbox1}
                            checkbox2={checkbox2}
                            pricingList={pricingList}
                            servicePrice={servicePrice}
                            countDocument={countDocument}
                            next={{step: 3}}
                        />
                    )
                case 3:
                    return (
                        <DateTime
                            servicePrice={servicePrice}
                            dispatch={dispatch}
                            loading={loading}
                            times={times}
                            timesPercent={timesPercent}
                            time={time}
                            order={order}
                            countDocument={countDocument}
                            selectedDate={selectedDate}
                            online={true}
                            nextStep={4}
                        />
                    );
                case 4:
                    if (amount === -1) {
                        this.getNewOrderAmount()
                    }
                    if (allDiscounts == null) {
                        getAllDiscounts()
                    }
                    return (
                        <FourStep
                            amount={amount}
                            isHaveDiscount={order.discountAmount && order.amount !== amount}
                            dispatch={dispatch}
                            loading={loading}
                            stage={4}
                            saveEditOrder={this.saveEditOrder}
                            confirmChange={this.confirmChange}
                        />
                    );
                case 5:
                    return (
                        <LastStep
                            stage={5}
                            allDiscounts={allDiscounts}
                            discountNeed={discountNeed}
                            changeDiscount={changeDiscount}
                            saveEditOrder={this.saveEditOrder}
                            discountCount={discountCount}
                        />
                    );
                default:
                    (console.log('This is a multi-step form built with React.'))

            }
        }

        let stateUpdater = (item) => {
            dispatch({
                type: 'updateStateBack',
                payload: {
                    ...item,
                    ...orderState
                }
            })
        }
        let getAllDiscounts = () => {
            if (allDiscounts == null) {
                dispatch(getOrderPriceAmounts({
                    servicePriceId: servicePrice.id,
                    clientId: order.client ? order.client.id : '',
                    countDocument: countDocument,
                    timeTableId: timeTable.id,
                    orderAdditionalServiceDtoList: orderAdditionalServices,
                    zipCodeId: zipCode ? zipCode.id : ''
                }))
            }
        }
        const changeDiscount = (item) => {
            let discountList = [];
            let isHave = false;

            for (let j = 0; j < discountCount.length; j++) {
                if (discountCount[j].discountName === item.discountName) {
                    isHave = true;
                    break;
                }
            }
            if (isHave) {
                discountList = [];
                for (let j = 0; j < discountCount.length; j++) {
                    if (discountCount[j].discountName !== item.discountName) {
                        discountList.push({...discountCount[j], number: j});
                    }
                }
            } else {
                if (!discountNeed) {
                    discountList = (discountCount ? [...discountCount] : []);
                    discountList.push({...item});
                    let discountList2 = []
                    for (let i = 0; i < discountList.length; i++) {
                        discountList2.push({...discountList[i], number: i})
                    }
                    discountList = discountList2
                }
            }
            let discountAllAmount = 0;
            discountList.map(item => discountAllAmount += item.amount);
            dispatch({
                type: "updateState",
                payload: {
                    discountCount: discountList,
                    discountNeed: (discountAllAmount >= allDiscounts[0].needAmount)
                }
            })
        }


        return <>
            <Modal className="edit-order-modal" isOpen={openEditModal}>
                <ModalHeader>
                    Edit Order
                </ModalHeader>
                <ModalBody>
                    {order.servicePrice.online ? showOnlineOrder() : showInPersonOrderForm()}
                </ModalBody>
                <ModalFooter>
                    <Button onClick={() => stateUpdater({openEditModal: false})}>Close</Button>
                </ModalFooter>
            </Modal>
        </>
    }
}

export default connect(({
                            order: {
                                titleDocument, checkbox1, checkbox2, street_address, code, attachmentIdsArray, zipCode,
                                allDiscounts, discountCount, discountNeed, amount, timesPercent, orderState, openEditModal,
                                embassyCountries, documentTypes, isApostille, internationalDto, realEstateDto, order,
                                loading, address, timeZone, servicePrices, servicePrice, pricingList, countDocument,
                                orderAdditionalServices, additionalServicePrices, step, selectedDate, error, outOfService,
                                times, time, timeTable, stage, checkbox3, modal, dispatch, payTypes, payType, allAmount, serviceArray, currentService
                            },
                            app: {countries},
                        }) => ({
    amount, timesPercent, orderState, openEditModal,
    titleDocument, code, zipCode,
    checkbox1, street_address,
    checkbox2,
    discountNeed,
    allDiscounts,
    discountCount,
    loading,
    order,
    countries,
    embassyCountries, attachmentIdsArray,
    documentTypes,
    isApostille,
    internationalDto,
    realEstateDto,
    address,
    timeZone,
    servicePrices,
    servicePrice,
    pricingList,
    countDocument,
    orderAdditionalServices,
    outOfService,
    additionalServicePrices,
    step,
    selectedDate,
    error,
    times,
    time,
    timeTable,
    stage,
    checkbox3,
    modal,
    dispatch,
    payTypes,
    payType,
    allAmount,
    serviceArray,
    currentService
}))(EditOrder);

export function nextStage(value, dispatch) {
    dispatch({
        type: 'updateStateOrder',
        payload: value
    })
}