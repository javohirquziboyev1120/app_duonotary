import React, {Component} from "react";
import {Button, Col, Input} from "reactstrap";
import './order.scss'
import {getAllByUserId} from "../../redux/actions/OrderAction";

export class LastStep extends Component {

    resetState = () => {
        this.props.dispatch({
            type: "resetState",
            payload: this.props.state
        })
        this.props.dispatch(getAllByUserId({page: 0, size: 20}))
    }

    render() {
        const {allDiscounts, saveEditOrder, discountNeed, discountCount, changeDiscount} = this.props;

        return (
            <>
                <div>
                    {allDiscounts && allDiscounts.length > 0 ?
                        <div className="discount-block bg-white p-4 rounded">
                            <h5>You have discounts! Do you want to use them? Select and click "Use".</h5>
                            {allDiscounts.map((dis, i) =>
                                <div key={i} className="one-discount my-3">
                                    <Input type="checkbox"
                                           disabled={discountNeed ? !discountCount.some(d => d.discountName === dis.discountName) : false}
                                           id={"discount" + i}
                                           name={"discount" + i}
                                           checked={discountCount && discountCount.some(d => d.discountName === dis.discountName)}
                                           onClick={() => changeDiscount(dis)}/>{' '}
                                    {dis.discountName} - {(dis.amount)}$.
                                    <br/>
                                    {((dis.allAmount / 100) * dis.discountPercent) <= dis.amount ?
                                        <small className="secondary"> (You can
                                            use {(dis.allAmount / 100) * dis.discountPercent}$)
                                        </small>
                                        : ""
                                    }
                                </div>
                            )}
                            <Col md={6} className="ml-auto">
                                <Button onClick={() => saveEditOrder()}
                                        className="btn btn-warning rounded-0 ml-auto px-4">Use</Button></Col>
                        </div>
                        :this.resetState()
                    }
                </div>
            </>
        );
    }
}

export default LastStep;

