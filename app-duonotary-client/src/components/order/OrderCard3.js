import {ModalFooter, Collapse, Container, ModalHeader, ModalBody, Modal, Button, Col, Row} from "reactstrap";
import getDate from "../Date";
import React, {useEffect, useState} from "react";
import "./order.scss"
import Stripe from "../Stripe";
import {useDispatch, useSelector} from "react-redux";
import {paymentNow} from "../../redux/actions/OrderAction";
import {useHistory} from "react-router";

function Orders3(orders) {

    const dispatch = useDispatch();
    const clientSecret = useSelector(state => state.order.clientSecret)
    const [payment, setPayment] = useState(false)
    const [id, setId] = useState('')
    const [item, setItem] = useState('')
    const [online, setOnline] = useState(true)
    const [downloadModal, setDownloadModal] = useState(false)
    const [isMobile, setIsMobile] = useState(window.innerWidth < 768);
    const [cUpcoming, setCUpcoming] = useState(false);
    const [cProgress, setCProgress] = useState(false);
    const [cCompleted, setCCompleted] = useState(false);
    const history = useHistory();
    useEffect(() => {
        window.addEventListener("resize", () => {
            setIsMobile(window.innerWidth < 768);
        })
    })

    function doPayment(order) {
        if (order.servicePrice.online)
            dispatch(paymentNow({id: order.id})).then(res => {
                setPayment(true);
                setId(order.id);
                setOnline(true)
            })
        else {
            setPayment(true);
            setId(order.id);
            setOnline(false)
        }
    }

    const openDownloadModal = (item) => {
        setDownloadModal(!downloadModal)
        setItem(item)
    }

    function getURI(v, d) {
        let uri = v + d;
        return uri

    }

    function canPay(order) {
        let created = new Date();
        let start = new Date(order.timeTableDto.fromTime);
        let result = start.getTime() - created.getTime();
        let day = Math.floor(result / 1000 / 60 / 60 / 24)
        return day < 6;
    }

    return (
        <Row className="order">
            <Col className={'mx-auto'} lg={4} md={8}>
                <div onClick={() => {
                    setCUpcoming(!cUpcoming)
                }} className="card-name-upcoming">
                    <div className="color-card"/>
                    <div className="name">
                        Upcoming
                    </div>
                    <div className="card-count">
                        {orders.orders.object.upcomingCount}
                    </div>
                </div>
                <Collapse isOpen={!isMobile || cUpcoming}>
                    {orders.orders.object.upcoming.map(item =>
                        <div className="about-upcoming" key={item.id}>
                            <div className="agent-title">
                                Agents
                            </div>
                            <div className="agent-name">
                                {item.agent.firstName + ' ' + item.agent.lastName}
                            </div>
                            <div className="line-border"/>

                            <div className="time-and-service">
                                <div className="time-order">
                                    <div className="time-title">
                                        Date, time of order
                                    </div>
                                    <div className="time">
                                                <span
                                                    className="pr-2">{getDate(item.timeTableDto.fromTime, "day")} </span>
                                        <div className="bordered"/>
                                        <span
                                            className="pl-2">{getDate(item.timeTableDto.fromTime, "time")}</span>
                                    </div>
                                </div>

                                <div className="service">
                                    <div className="time-title">
                                        Service type
                                    </div>
                                    <div className="time">
                                        <span>{item.servicePrice.serviceDto.subServiceDto.name} </span>
                                    </div>
                                </div>
                            </div>
                            <div className="time-and-service">
                                <div className="time-order">
                                    <div className="time-title">
                                        Order number
                                    </div>
                                    <div className="time">
                                        {item.serialNumber ? item.serialNumber : ""}
                                    </div>
                                </div>
                                {item.titleDocument ?
                                    <div className="service">
                                        <div className="time-title">
                                            Document title
                                        </div>
                                        <div className="time">
                                            <span>{item.titleDocument ? item.titleDocument : ""}</span>
                                        </div>
                                    </div> : ""}
                            </div>
                            <div className="location-title mt-3">
                                Location
                            </div>
                            <div className="location">
                                {item.address ? item.address : "online"}
                            </div>
                            <div className="d-flex mt-4 align-items-end justify-content-between">
                                <div>
                                    <div className="amount-title ">
                                        Amount
                                    </div>
                                    <div className="amount">
                                        ${(item.amount).toFixed(2)} {item.discountAmount ?
                                        "(-$" + item.discountAmount.toFixed(2) + ")" : ""}
                                    </div>
                                </div>
                                {item.payType.online && item.servicePrice.online === false && canPay(item) ?
                                    <div className="">
                                        {item.stripe || item.payed ?
                                            <div>
                                                <Button color="success"
                                                        className="btn  px-5">PAID</Button>
                                            </div> :
                                            <div>
                                                <Button color="primary" className="btn px-5"
                                                        onClick={() => doPayment(item)}>Pay now</Button>
                                            </div>

                                        }
                                    </div> : ''
                                }
                            </div>
                        </div>
                    )}
                </Collapse>

            </Col>
            <Col className={'mx-auto'} lg={4} md={8}>
                <div onClick={() => {
                    setCProgress(!cProgress)
                }} className="card-name-in-progress">
                    <div className="color-card"/>
                    <div className="name">
                        In-Progress
                    </div>
                    <div className="card-count">
                        {orders.orders.object.inProgressCount}
                    </div>
                </div>
                <Collapse isOpen={!isMobile || cProgress}>
                    {orders.orders.object.inProgress.map(item =>
                        <div className="about-in-progress" key={item.id}>
                            <div className="agent-title">
                                Agents
                            </div>
                            <div className="agent-name">
                                {item.agent.firstName + ' ' + item.agent.lastName}
                            </div>
                            <div className="line-border"/>

                            <div className="time-and-service">
                                <div className="time-order">
                                    <div className="time-title">
                                        Date, time of order
                                    </div>
                                    <div className="time">
                                                        <span
                                                            className="pr-2">{getDate(item.timeTableDto.fromTime, "day")} </span>
                                        <div className="bordered"/>
                                        <span
                                            className="pl-2">{getDate(item.timeTableDto.fromTime, "time")}</span>
                                    </div>
                                </div>

                                <div className="service">
                                    <div className="time-title">
                                        Service type
                                    </div>
                                    <div className="time">
                                        <span>{item.servicePrice.serviceDto.subServiceDto.name} </span>
                                    </div>
                                </div>
                            </div>
                            <div className="time-and-service">
                                <div className="time-order">
                                    <div className="time-title">
                                        Order number
                                    </div>
                                    <div className="time">
                                        {item.serialNumber ? item.serialNumber : ""}
                                    </div>
                                </div>
                                {item.titleDocument ?
                                    <div className="service">
                                        <div className="time-title">
                                            Document title
                                        </div>
                                        <div className="time">
                                            <span>{item.titleDocument ? item.titleDocument : ""}</span>
                                        </div>
                                    </div> : ""}
                            </div>
                            <div className="location-title mt-3">
                                Location
                            </div>
                            <div className="location">
                                {item.address}
                            </div>
                            <div className="location-title mt-3">
                                Order number
                            </div>
                            <div className="location">
                                {item.serialNumber ? item.serialNumber : ""}
                            </div>
                            <div className="d-flex mt-4 align-items-end justify-content-between">
                                <div>
                                    <div className="amount-title">
                                        Amount
                                    </div>
                                    <div className="amount">
                                        ${(item.amount).toFixed(2)} {item.discountAmount ?
                                        "(-$" + item.discountAmount.toFixed(2) + ")" : ""}
                                    </div>
                                </div>
                                {item.payType.online && item.servicePrice.online === false && canPay(item) ?
                                    <div className="">
                                        {item.stripe || item.payed ?
                                            <div>
                                                <Button color="success"
                                                        className="btn px-5">PAID</Button>
                                            </div> :
                                            <div>
                                                <Button color='primary' className="btn px-5"
                                                        onClick={() => doPayment(item)}>Pay now</Button>
                                            </div>
                                        }
                                    </div> : ''
                                }
                            </div>
                        </div>)}
                </Collapse>
            </Col>
            <Col className={'mx-auto'} lg={4} md={8}>
                <div onClick={() => {
                    setCCompleted(!cCompleted)
                }} className="card-name-completed">
                    <div className="color-card"/>
                    <div className="name">
                        Completed
                    </div>
                    <div className="card-count">
                        {orders.orders.object.completedCount}
                    </div>
                </div>
                <Collapse isOpen={!isMobile || cCompleted}>
                    {orders.orders.object.completed.map(item =>
                        <div className="about-completed" key={item.id}>
                            <div className="agent-title">
                                Agents
                            </div>
                            <div className="agent-name">
                                {item.agent.firstName + ' ' + item.agent.lastName}
                            </div>
                            <div className="line-border"/>

                            <div className="time-and-service">
                                <div className="time-order">
                                    <div className="time-title">
                                        Date, time of order
                                    </div>
                                    <div className="time">
                                                        <span
                                                            className="pr-2">{getDate(item.timeTableDto.fromTime, "day")} </span>
                                        <div className="bordered"/>
                                        <span
                                            className="pl-2">{getDate(item.timeTableDto.fromTime, "time")}</span>
                                    </div>
                                </div>

                                <div className="service">
                                    <div className="time-title">
                                        Service type
                                    </div>
                                    <div className="time">
                                        <span>{item.servicePrice.serviceDto.subServiceDto.name} </span>
                                    </div>
                                </div>
                            </div>
                            <div className="time-and-service">
                                <div className="time-order">
                                    <div className="time-title">
                                        Order number
                                    </div>
                                    <div className="time">
                                        {item.serialNumber ? item.serialNumber : ""}
                                    </div>
                                </div>
                                {item.titleDocument ?
                                    <div className="service">
                                        <div className="time-title">
                                            Document title
                                        </div>
                                        <div className="time">
                                            <span>{item.titleDocument ? item.titleDocument : ""}</span>
                                        </div>
                                    </div> : ""}
                            </div>
                            <div className="location-title mt-3">
                                {item.address ? "Location" : "Document Type"}
                            </div>
                            <div className="location">
                                {item.address ? item.address : item.titleDocument}
                            </div>
                            <div className="location-title mt-3">
                                Order number
                            </div>
                            <div className="location">
                                {item.serialNumber ? item.serialNumber : ""}
                            </div>
                            <div className="d-flex mt-4 justify-content-between align-items-end">
                                <div>
                                    <div className="amount-title">
                                        Amount
                                    </div>
                                    <div className="amount">
                                        ${(item.amount).toFixed(2)} {item.discountAmount ?
                                        "(-$" + item.discountAmount.toFixed(2) + ")" : ""}
                                    </div>
                                </div>
                                {item.payType.online ?
                                    item.servicePrice.online ?
                                        <div className="">
                                            {item.payed ?
                                                <div>
                                                    <Button className="btn btn-outline-primary btn-light"
                                                            onClick={() => openDownloadModal(item)}>Download</Button>
                                                </div> :
                                                <div>
                                                    <Button className="btn btn-outline-success btn-light"
                                                            onClick={() => doPayment(item)}>Pay now</Button>
                                                </div>}
                                        </div> :
                                        <div className="">
                                            {!item.payed ?
                                                <div>
                                                    <Button className="btn btn-outline-success btn-light"
                                                            onClick={() => doPayment(item)}>Pay now</Button>
                                                </div> : <Button color="success" className="btn px-5">PAID</Button>}
                                        </div> : ''
                                }
                            </div>
                        </div>)}
                </Collapse>
            </Col>
            <Modal isOpen={downloadModal}>
                <ModalHeader>Download documents</ModalHeader>
                <ModalBody>
                    <Container>
                        {item.resUploadFiles ? item.resUploadFiles.map((value, index) =>
                            <Row>
                                <Col md={4}>{"Document " + index + 1}</Col>
                                <Col md={4}>
                                    <button
                                        type='button'>
                                        <a
                                            target='_blank'
                                            href={getURI(value.fileDownloadUri, "?download=false")}
                                        >Open document</a>
                                    </button>
                                </Col>
                                <Col md={4}>
                                    <button>
                                        <a
                                            href={getURI(value.fileDownloadUri, "?download=true")}
                                            download
                                        >Download document</a>
                                    </button>
                                </Col>
                            </Row>
                        ) : ''}
                    </Container>
                </ModalBody>
                <ModalFooter>
                    <Button onClick={() => openDownloadModal('')}>Close</Button>
                </ModalFooter>
            </Modal>

            {payment ?
                <Modal isOpen={payment}>
                    <ModalBody>
                        {online ?
                            <Stripe type="now" id={id} clientSecret={clientSecret}/> :
                            <Stripe type="future" orderId={id} history={history}/>
                        }
                    </ModalBody>
                    <ModalFooter>
                        <Button className="w-100 btn btn-warning" onClick={() => setPayment(false)}>Close</Button>
                    </ModalFooter>
                </Modal>
                : ''}
        </Row>)
}

export default Orders3;