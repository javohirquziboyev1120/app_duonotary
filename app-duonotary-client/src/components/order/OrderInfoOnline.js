import React, {Component} from "react";
import {Button, Col, Form, FormGroup, Input, Label, Row} from "reactstrap";
import './order.scss'
import {toast} from "react-toastify";
import {getPricingByServicePrice, getServiceToNow} from "../../redux/actions/OrderAction";
import {uploadDoc} from "../../redux/actions/AttachmentAction";
import {nextStage} from "./EditOrder";

export class OrderInfoOnline extends Component {
    componentDidMount() {
        this.props.dispatch(getServiceToNow())
    }

    state = {
        invalid: false,
    }

    setTitleDoc = (e) => {
        this.setState({invalid: false})
        if (e.target.value != null) {
            this.props.dispatch({
                type: 'updateStateOrder',
                payload: {titleDocument: e.target.value}
            })
        }
    }

    setCheckbox = (item) => {
        this.props.dispatch({
            type: 'updateStateOrder',
            payload: item
        })
    }

    render() {
        const {
            dispatch, titleDocument, attachmentIdsArray, checkbox1, checkbox2, pricingList, servicePrice,
            countDocument, next
        } = this.props;

        function checkFile(event) {
            let file = event.target.files[0];
            if (!file) {
                return '';
            }
            if (file.size >= 2 * 1024 * 1024) {
                toast.warning("File size must be less 2mb")
                let file = document.getElementById("form-id");
                if (file != null)
                    file.reset()
                return;
            }
            if (!file.type.match('application/pdf') && !file.type.match('application/msword') && !file.type.match('application/vnd.openxmlformats-officedocument.wordprocessingml.document')) {
                toast.warning("File type should be pdf, doc or docx")
                document.getElementById("form-id").reset();
                return;
            }

            let b = 1;
            attachmentIdsArray.map(item => {
                let a = item.key.indexOf(" ");
                if (a > 0)
                    b = parseInt(item.key.substring(a + 1)) + 1;
            })
            return dispatch(uploadDoc(file, "document " + b))

        }

        function price() {
            let one = 0;
            if (pricingList.length > 0) {
                for (let i = 0; i < pricingList.length; i++) {
                    if (pricingList[i].everyCount != null && pricingList[i].everyCount > countDocument) {
                        one = pricingList[i].price;
                    } else if (!pricingList[i].everyCount && pricingList[i].fromCount <= countDocument && pricingList[i].tillCount >= countDocument) {
                        one = pricingList[i].price
                    } else if (!pricingList[i].everyCount && pricingList[i].fromCount <= countDocument && !pricingList[i].tillCount) {
                        one = pricingList[i].price
                    }
                }
            }
            one = servicePrice ? one * countDocument + servicePrice.price : '0'
            return one + '$'
        }

        return (
            <>
                <Row className="online-step-one">
                    <Col md={12} className={"step-one-form"}>
                        <Form className={"step-one-form"}>
                            <FormGroup>
                                <p>Enter title of the document</p>
                                <Input
                                    type="input"
                                    pleaceholder="Example: Real estate"
                                    id='titleDoc'
                                    onChange={(e) => this.setTitleDoc(e)} name="select"
                                    defaultValue={titleDocument} valid={!!titleDocument}
                                    invalid={this.state.invalid}>
                                </Input>
                                <div className="price text-right" style={{fontSize: '22px'}}>Price
                                    : <strong>{price()}</strong></div>
                            </FormGroup>
                            <div className="drop-files">
                                <div className="your-document">
                                    Your document(s) to br notarized
                                </div>
                                <div className="file-upload">
                                    File upload .doc, .docx or .pdf and size must not be more than 2mb
                                </div>
                                <FormGroup>
                                    <Input onChange={(e) => checkFile(e)} multiple hidden
                                           id={"documentOnline"}
                                           disabled={!titleDocument}
                                           type="file" name="file" accept=".doc, .docx, .pdf"/>
                                    <div className="d-flex">
                                        <div onClick={function () {
                                            document.getElementById("documentOnline").click()
                                        }} className="fs-15-bold form-control-file">
                                            <p>+</p>
                                        </div>
                                        {attachmentIdsArray[0] ? attachmentIdsArray.map((v, i) =>
                                            v.key.indexOf('document') === 0 ?
                                                <a href={v.src} key={i} style={{textDecoration: 'none'}}>
                                                    <div className="uploaded-file">
                                                        <img className="mt-3"
                                                             src="../../../public/assets/img/uploaded.png"
                                                             style={{filter: 'invert(1)'}}
                                                             alt=""/>
                                                        <div className="file-name pt-1">
                                                            {v.key}
                                                        </div>
                                                    </div>
                                                </a> : ''
                                        ) : ''}
                                    </div>
                                </FormGroup>
                            </div>
                            <div className="buttons d-flex mb-2">
                                <div className='checkboxs'>
                                    <FormGroup className="mt-4" check>
                                        <Input id='duo' type="checkbox" checked={checkbox1}
                                               onChange={() => {
                                                   this.setCheckbox({checkbox1: !checkbox1})
                                               }}/>
                                        <Label check htmlFor='duo'>
                                        </Label>
                                    </FormGroup>
                                    <FormGroup className="mt-2" check>
                                        <Input id='agree' type="checkbox" checked={checkbox2}
                                               onChange={() => {
                                                   this.setCheckbox({checkbox2: !checkbox2})
                                               }}/>
                                        <Label check className='label_2' htmlFor='agree'>
                                        </Label>
                                    </FormGroup>
                                </div>
                                <Button onClick={function () {
                                    if (checkbox1 && checkbox2 && !!servicePrice) {
                                        attachmentIdsArray[0] ? nextStage(next, dispatch) : toast.warning("Please fill in all information")
                                    }
                                }
                                } className="next-btn ml-auto">Next step</Button>
                            </div>
                        </Form>
                    </Col>
                </Row>
            </>
        );
    }
}

export default OrderInfoOnline;