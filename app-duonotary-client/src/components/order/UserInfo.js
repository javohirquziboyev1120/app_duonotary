import React, {Component} from "react";
import './order.scss'
import {Button, Col, Form, Input, Label, Row} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {SET_ERROR} from "../../redux/actionTypes/OrderActionTypes";
import {loginForOrder, registerOrderUser} from "../../redux/actions/AuthActions";
import '../../pages/clientPage/clientPages.scss'
import {nextStage} from "./EditOrder";
import AlertModal from "../AlertModal";
import {NEW_USER_FOR_ORDER, NEW_USER_LOG_OUT} from "../../utils/constants";

export class UserInfo extends Component {
    componentDidMount() {
        let user = localStorage.getItem(NEW_USER_FOR_ORDER);
        if (user) {
            user = JSON.parse(user);
            this.setState({
                registeredUser: user,
                newUserForOrder: true
            })
        }
    }

    state = {
        showLogin: false,
        showPassword: false,
        newUserForOrder: false,
        registeredUser: {}
    }

    render() {
        const {dispatch, checkbox3, signUp, nextValue, online, error} = this.props;

        let register = (e, v) => {
            if (!this.state.newUserForOrder) {
                if (v.password !== v.prePassword) {
                    dispatch({
                        type: SET_ERROR,
                        payload: true
                    })
                } else {
                    dispatch({
                        type: SET_ERROR,
                        payload: false
                    })
                    dispatch(registerOrderUser(v))
                    dispatch({
                        type: 'updateStateOrder',
                        payload: {currentUser: v}
                    })
                }
            } else {
                dispatch({
                    type: 'updateStateOrder',
                    payload: {currentUser: this.state.registeredUser,  signUp: true, ...nextValue}
                })
            }
        }

        let authLogin = (e, v) => {
            let user = JSON.parse(localStorage.getItem(NEW_USER_LOG_OUT));
            if (user && user.email === v.username && user.password === v.password)
                dispatch({
                    type: 'updateStateOrder',
                    payload: {currentUser: user, signUp: true, ...nextValue}
                })
            dispatch(loginForOrder({
                value: v, next: online ? nextValue.step : nextValue.stage, online: online
            }))
        }

        let logOut = () => {
            localStorage.removeItem(NEW_USER_FOR_ORDER)
            localStorage.setItem(NEW_USER_LOG_OUT, JSON.stringify(this.state.registeredUser))
            dispatch({
                type: 'updateStateOrder',
                payload: {currentUser: '', signUp: false}
            })
            this.setState({
                registeredUser: {},
                newUserForOrder: false
            })
        }

        let setCheckbox = (item) => {
            dispatch({
                type: 'updateStateOrder',
                payload: item
            })
        }
        return (
            <>
                <AvForm onValidSubmit={register}
                        model={this.state.registeredUser}
                        className={"step-two-form"}>
                    <Row className="user-info-container" hidden={this.state.showLogin}>
                        <Col md={12} className="text-right h5 py-1">
                            {!this.state.newUserForOrder ? <>
                                    <a href="#" onClick={() => this.setState({showLogin: false})}>Sign up </a>
                                    <a href="#" onClick={() => this.setState({showLogin: true})}>| Sign in</a>
                                </> :
                                <a href="#" onClick={() => logOut()}>Log out </a>
                            }
                        </Col>
                        <Col md={6}>
                            <AvField
                                disabled={this.state.newUserForOrder}
                                value={this.state.registeredUser.firstName}
                                placeholder="First Name" name="firstName" required/>
                        </Col>
                        <Col md={6}>
                            <AvField
                                disabled={this.state.newUserForOrder}
                                value={this.state.registeredUser.lastName}
                                placeholder="Last Name" name="lastName" required/>
                        </Col>
                        <Col md={6}>
                            <AvField
                                disabled={this.state.newUserForOrder}
                                value={this.state.registeredUser.phoneNumber}
                                placeholder="Phone Number" name="phoneNumber" required/>
                        </Col>
                        <Col md={6}>
                            <AvField
                                disabled={this.state.newUserForOrder}
                                value={this.state.registeredUser.email}
                                placeholder="Email" name="email" required/>
                        </Col>
                        <Col md={6} className='position-relative'>
                            <AvField
                                disabled={this.state.newUserForOrder}
                                value={this.state.registeredUser.password}
                                placeholder="Password" name="password"
                                type={this.state.showPassword ? 'text' : 'password'}
                                required/>
                            <i className={`fas ${this.state.showPassword ? 'fa-eye-slash' : 'fa-eye'} position-absolute`}
                               style={{
                                   top: '1.4rem',
                                   right: '1.5rem',
                                   fontSize: '18px',
                                   color: '#7D7D7E',
                                   cursor: 'pointer'
                               }}
                               onClick={() => this.setState({showPassword: !this.state.showPassword})}
                            />
                        </Col>
                        <Col md={6} className='position-relative'>
                            <AvField
                                disabled={this.state.newUserForOrder}
                                value={this.state.registeredUser.prePassword}
                                placeholder="Re-Password" name="prePassword"
                                type={this.state.showRePassword ? 'text' : 'password'}
                                required
                                invalid={!!error}/>
                            <i className={`fas ${this.state.showRePassword ? 'fa-eye-slash' : 'fa-eye'} position-absolute`}
                               style={{
                                   top: '1.4rem',
                                   right: '1.5rem',
                                   fontSize: '18px',
                                   color: '#7D7D7E',
                                   cursor: 'pointer'
                               }}
                               onClick={() => this.setState({showRePassword: !this.state.showRePassword})}
                            />
                        </Col>
                        <Col md={12} className="mt-3">
                            <Form id={"checkbox3"} className='formCheckControl'>
                                <Input
                                    id='create'
                                    type="checkbox"
                                    checked={checkbox3 || this.state.newUserForOrder}
                                    disabled={this.state.newUserForOrder}
                                    onChange={() => setCheckbox({checkbox3: !checkbox3})}
                                />
                                <Label className='labelCheck' check htmlFor='create'>
                                    <div className='textLabel'>Creating an account means you’re okay with
                                        our
                                        <span
                                            className="px-1 checkbox-span"> Terms of Service, Privacy Policy</span>,
                                        and our default
                                        <span className="px-1 checkbox-span"> Notification Settings.</span>
                                    </div>
                                </Label>
                            </Form>
                        </Col>
                        <Col md={12} className="mt-5 ml-auto">
                            <Row>
                                <Col md={6}>
                                    <Button onClick={() => dispatch({
                                        type: 'previousStep'
                                    })} className="next-step-btn">
                                        Previous step
                                    </Button>
                                </Col>
                                <Col md={6}>
                                    <div className='myBtn' style={{width: 'max-content'}}>
                                        <Button className="next-step-btn" disabled={!checkbox3}>
                                            Next step
                                        </Button>
                                    </div>
                                </Col>
                            </Row>

                        </Col>
                    </Row>
                </AvForm>

                <AvForm onValidSubmit={authLogin}>
                    <Row className="user-info-container" hidden={!this.state.showLogin}>
                        <Col md={12} className="text-right h5 pb-1">
                            <a href="#" onClick={() => this.setState({showLogin: false})}>Sign up </a>
                            <a href="#" onClick={() => this.setState({showLogin: true})}>| Sign in</a>
                        </Col>
                        <Col md='12'>
                            <AvField placeholder="Username" name="username" required/>
                        </Col>
                        <Col md="12" className='position-relative'>
                            <AvField placeholder="Password" name="password"
                                     type={this.state.showPassword ? 'text' : 'password'}
                                     required/>
                            <i className={`fas ${this.state.showPassword ? 'fa-eye-slash' : 'fa-eye'} position-absolute`}
                               style={{
                                   top: '1.4rem',
                                   right: '1.5rem',
                                   fontSize: '18px',
                                   color: '#7D7D7E',
                                   cursor: 'pointer'
                               }}
                               onClick={() => this.setState({showPassword: !this.state.showPassword})}
                            />
                        </Col>
                        <Col md={12} className='mt-4'>
                            <Row>
                                <Col md={6}>
                                    <Button onClick={() => dispatch({
                                        type: 'previousStep'
                                    })} className="next-step-btn">
                                        Previous step
                                    </Button>
                                </Col>
                                <Col md={6}>
                                    <div className='myBtn'>
                                        <Button className="next-step-btn">
                                            Next step
                                        </Button>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </AvForm>

                {signUp ? <AlertModal title='Create Account'
                                      body='Congratulation you successfully registered. Please verify your email after completed order.'
                                      action={() => nextStage(nextValue, dispatch)}/> : ''}
            </>
        );
    }
}