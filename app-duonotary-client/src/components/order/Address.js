import React, {Component} from "react";
import {Button, Input, Modal, ModalBody, ModalFooter, ModalHeader, Spinner} from "reactstrap";
import './order.scss'
import {getAddressInfo, sentToOutOfService} from "../../redux/actions/OrderAction";
import {toast} from "react-toastify";
import {AvField, AvForm} from "availity-reactstrap-validation";
import axios from "axios";
import {config} from "../../utils/config";
import {GOOGLE_GEOCODE_KEY} from "../../utils/constants";

export class Address extends Component {
    state = {
        alert: false,
        geocode: [],
        valid: '',
    }

    setAddress = (item) => {
        if (item != null) {
            document.getElementById("zip-code-input").value = item.formatted_address
            let code = '';
            let street_address = false;
            item.address_components.map(v => {
                if (v.types[0]) {
                    if (v.types[0] === 'postal_code') {
                        code = v.long_name;
                    }
                    if (v.types[0] === 'street_number') {
                        street_address = true;
                    }
                }
            })
            if (!code) {
                toast.warning("Location consist of zip code and address and street number")
            } else {
                this.props.dispatch({
                    type: 'updateStateOrder',
                    payload: {
                        address: item.formatted_address,
                        lat: item.geometry.location.lat,
                        lng: item.geometry.location.lng,
                        code: code,
                        street_address: street_address,
                    }
                })
                axios.get(config.TIMEZONE_URL +item.geometry.location.lat+','+item.geometry.location.lng+ config.TIMEZONE_URL_PARAM).then(res => {

                })
            }
        }
    }
    sentOutOfService = (e, v) => {
        if (v.email && v.zipCode) {
            this.props.dispatch(sentToOutOfService(v))
        }
    }

    render() {
        const {dispatch, loading, address, outOfService, code, street_address} = this.props;

        let getGeocode = (item) => {
            if (item) {
                dispatch({
                    type: 'updateStateOrder',
                    payload: {
                        address: item,
                        code: ''
                    }
                })
                let isNum = /^\d+$/.test(item);
                if ((item.length > 4 && isNum || !isNum))
                    axios.get(config.GEOCODE_URL + checkSearchTypeGeocode(item)).then(res => {
                        if (res.data.status === 'OK') {
                            this.setState({geocode: res.data.results});
                        } else {
                            this.setState({geocode: res.data.results});
                        }
                    })
            } else {
                dispatch({
                    type: 'updateStateOrder',
                    payload: {
                        address: item,
                        code: ''
                    }
                })
            }
        }

        return (
            <>
                <div className="address-container">
                    <div className="please-address">
                        Please provide an address
                    </div>
                    <div className="position-relative">
                        <Input onChange={(e) => getGeocode(e.target.value)}
                               id="zip-code-input" autoComplete="off" className="address-input"
                               placeholder="767 5th Ave, New York, NY 10153, USA" value={address}/>
                        <div className="address-input-vector">
                            <svg hidden={!code || !street_address} width="22" height="13" viewBox="0 0 22 13"
                                 fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M10.602 8.76L12.014 10.172L20.48 1.706L21.894 3.12L12.014 13L5.65 6.636L7.064 5.222L9.189 7.347L10.602 8.759V8.76ZM10.604 5.932L15.556 0.979004L16.966 2.389L12.014 7.342L10.604 5.932ZM7.777 11.587L6.364 13L0 6.636L1.414 5.222L2.827 6.635L2.826 6.636L7.777 11.587Z"
                                    fill="#00B238"/>
                            </svg>
                        </div>
                    </div>
                    <ul hidden={loading || !!code}>
                        {this.state.geocode.map((item, index) =>
                            <li className="address-li" key={index}
                                onClick={() => this.setAddress(item)}>{item.formatted_address}</li>)}
                    </ul>
                    <ul className="address-li" hidden={!loading}>
                        <Spinner animation="border" role="status"/>
                    </ul>
                    <div className="ml-auto" style={{width: 'max-content'}}>
                        <Button className="address-next-btn"
                                disabled={!address}
                                onClick={() => {
                                    if (street_address)
                                        dispatch(getAddressInfo({code: this.props.code}))
                                    else
                                        toast.warning("Please enter a more detailed address")
                                }}
                        >Next step</Button>
                    </div>
                </div>

                <Modal isOpen={outOfService}>
                    <AvForm onValidSubmit={this.sentOutOfService}>
                        <ModalHeader>
                            <p color="warning">Oops, our service doesn't exist in this location.</p>
                        </ModalHeader>
                        <ModalBody>
                            <p>We will notify you if we set up a service at this address</p>
                            <AvField name="zipCode" hidden
                                     value={code ? code : ''}/>
                            <AvField label="Location" name="location"
                                     defaultValue={address ? address : ''} disabled/>
                            <AvField label="Email" name="email" pleaceholder="Username@email.com"/>
                        </ModalBody>
                        <ModalFooter>
                            <Button onClick={() => dispatch({
                                type: 'updateStateOrder',
                                payload: {outOfService: false}
                            })} className="px-4 py-2 h3">Close</Button>
                            <Button className="px-4 py-2 h3 btn btn-info">Sent</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>
            </>
        );
    }
}

export default Address;

export let checkSearchTypeGeocode = (item) => {
    let isNum = /^\d+$/.test(item);
    if (item && isNum)
        return "components=postal_code:" + item + "|country:USA&key=" + GOOGLE_GEOCODE_KEY;
    else
        return 'address=' + item + '&key=' + GOOGLE_GEOCODE_KEY;
}