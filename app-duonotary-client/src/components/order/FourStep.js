import React, {Component} from "react";
import {Button, Col, Row} from "reactstrap";
import './order.scss'

export class FourStep extends Component {

    render() {
        const {amount, isHaveDiscount, saveEditOrder} = this.props;

        return (

            (isHaveDiscount ?
                    <Row>
                        <Col md={12}>
                            <h5>You are editing an order! Discounts are also subject to change. So do you agree to
                                reset discounts?</h5>
                            <h3 className="ml-auto">Total amount: {amount.toFixed(2)}$</h3>
                        </Col>
                        <Col md={12}>
                            <Button className="btn btn-primary rounded-0 ml-auto" onClick={() => saveEditOrder()}>Yes,
                                save</Button>
                        </Col>
                    </Row> :
                    <div>
                        <Row>
                            <Col md={12} className="ml-auto">
                                <h3>Total amount: ${amount.toFixed(2)}</h3>
                            </Col>
                            <Col md={12}>
                                <Button onClick={() => saveEditOrder()}
                                        className="btn btn-warning rounded-0 ml-auto">Save</Button></Col>
                        </Row>
                    </div>
            )
        );
    }
}

export default FourStep;

