import React, {Component} from "react";
import {getAmPm} from "../../pages/clientPage/InPersonOrder";
import * as types from "../../redux/actionTypes/OrderActionTypes";
import {Col, Button, Row} from "reactstrap";
import './order.scss'
import {getDates} from "../../pages/clientPage/OnlineOrder";
import getDate from "../Date";
import {getTimesByDate, postTimeTable} from "../../redux/actions/OrderAction";

export class DateTime extends Component {

    state = {
        currentDate: new Date(),
        time1: 0,
        time2: 4,
        time3: 0,
        time4: 4,
        alert: false,
        percent: false,
    }

    getTimes = (date) => {
        this.props.dispatch({
            type: types.CHANGE_DATE,
            payload: date
        })
        this.props.dispatch(
            getTimesByDate({
                servicePriceId: this.props.servicePrice.id,
                countDocument: this.props.countDocument,
                online: this.props.online,
                zipCodeId: this.props.online ? '' : this.props.zipCode.id,
                date: date
            })
        )
    }

    setTime = (item) => {
        this.props.dispatch({
            type: types.SET_TIME,
            payload: item
        })
    }

    saveToTimeTable = () => {
        this.props.dispatch(postTimeTable({
            id: this.props.order ? this.props.order.timeTableDto ? this.props.order.timeTableDto.id : '' : this.props.timeTable?this.props.timeTable.id:'',
            fromTime: this.props.selectedDate + ' ' + this.props.time.time + '.0',
            userDto: {id: this.props.time.agentId},
            orderDto: {
                servicePriceId: this.props.servicePrice.id,
                countDocument: this.props.countDocument,
                id: this.props.timeTable ? this.props.timeTable.id : ''
            },
            online: this.props.online,
            step: this.props.nextStep,
            error: {
                error: this.props.error,
                servicePriceId: this.props.servicePrice.id,
                countDocument: this.props.countDocument,
                online: this.props.online,
                date: this.props.selectedDate
            }
        }))
    }

    handleSize = () => {
        const size = window.innerWidth;
        if (size <= 768) {
            this.setState({
                time2: 2,
                time4: 2,
            })
        } else if (size > 768) {
            this.setState({
                time2: 4,
                time4: 4,
            })
        }
    }

    componentDidMount() {
        this.handleSize();
    }

    render() {
        const {selectedDate, time, times, timesPercent} = this.props;

        window.addEventListener('resize', this.handleSize)

        return (
            <>
                <div className="date-time-container">
                    <div className="date mt-3 mb-1">
                        <div className="date-title">Select Date</div>
                        <Row className="date-row">
                            <Col className="date-col"
                                 md={1} onClick={() => this.setState({currentDate: new Date()})}>{"<"}</Col>
                            {
                                getDates(this.state.currentDate, 5).map(item =>
                                    <Col md={2} onClick={() => this.getTimes(item)} key={item}
                                         className={selectedDate === item ? "date-col times" : "date-col-disable times"}
                                    >{getDate(item, "day")}</Col>)
                            }
                            <Col
                                onClick={() => this.setState({currentDate: new Date().setDate(new Date().getDate() + 2)})}
                                md={1} className="date-col">{">"}</Col>
                        </Row>
                    </div>
                    <div className="date mt-3 mb-1">
                        <div className="date-title">Select Time</div>
                        <div className="date-row">
                            <div className="date-col prevNext"
                                 onClick={this.state.time1 <= 0 ? () => this.setState({time1: 0}) : () => this.setState({
                                     time1: this.state.time1 - 1,
                                     time2: this.state.time2 - 1
                                 })}>
                                <div hidden={this.state.time1 <= 0}>{"<"}</div>
                            </div>
                            {
                                times.map((item, index) => {
                                    if (index >= this.state.time1 && index <= this.state.time2) return (
                                        <div key={item.time}
                                             onClick={() => this.setTime(item.enable ? item : '')}
                                             className={time.time === item.time ? 'date-col dates' : 'date-col-disable dates'}
                                        >
                                            <div>
                                                {getAmPm(item.time)}
                                            </div>
                                            <div className="time-available" hidden={!item.enable}>available
                                            </div>

                                        </div>)
                                })
                            }
                            <div onClick={() => this.setState({
                                time1: this.state.time1 + 1,
                                time2: this.state.time2 + 1
                            })}
                                 className="date-col prevNext"
                                 hidden={this.state.time2 >= times.length}>{">"}</div>
                        </div>
                    </div>
                    <Row className="date mt-3">
                        <Col md={12} className="manager align-self-center" hidden={this.state.percent}>
                            <p className="mb-0">Make an appointment for after business hours
                                <span className="text-primary"
                                      onClick={() => this.setState({percent: true})}> here</span>
                            </p>
                        </Col>
                        <Col md={12} hidden={!this.state.percent}>
                            <div className="date">
                                <div className="date-title">Select Special Time</div>
                            </div>
                        </Col>
                        <Col md={12} hidden={!this.state.percent}>
                            <Row className="date-row d-sm-flex">
                                <div className="date-col prevNext"
                                     onClick={this.state.time3 <= 0 ? () => this.setState({time1: 0}) : () => this.setState({
                                         time3: this.state.time3 - 1,
                                         time4: this.state.time4 - 1
                                     })}>
                                    <div hidden={this.state.time3 <= 0}>{"<"}</div>
                                </div>
                                {
                                    timesPercent.map((item, index) => {
                                        if (index >= this.state.time3 && index <= this.state.time4) return (
                                            <div key={item.time}
                                                 onClick={() => this.setTime(item.enable ? item : '')}
                                                 className={time.time === item.time ? 'date-col dates' : 'date-col-disable dates'}
                                            >
                                                <div>{getAmPm(item.time)}</div>
                                                <div className="time-available" hidden={!item.enable}>available
                                                </div>

                                            </div>)
                                    })
                                }
                                <div
                                    onClick={() => this.setState({
                                        time3: this.state.time3 + 1,
                                        time4: this.state.time4 + 1
                                    })}
                                    className="date-col prevNext"
                                    hidden={this.state.time4 >= timesPercent.length}>
                                    <div>{">"}</div>
                                </div>
                            </Row>
                        </Col>
                    </Row>
                    <Row className={'mt-3'}>
                        <Col className='d-flex' md={12}>
                            <div>
                                <Button
                                    className="next-step-btn"
                                    onClick={() => this.props.dispatch({
                                        type: 'previousStep'
                                    })}
                                >
                                    Previous step
                                </Button>
                            </div>
                            <div className={'ml-auto'}>
                                <Button
                                    className="next-step-btn"
                                    onClick={time ? () => this.saveToTimeTable() : () => this.setState({alert: true})}
                                >
                                    Next step
                                </Button>
                            </div>
                        </Col>
                    </Row>
                </div>
            </>
        );
    }
}

export default DateTime;