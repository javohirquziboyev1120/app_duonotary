import React, { useState} from "react";


export default function UploadFileBig(props) {

    let inputRef = "";
    const [name, setName] = useState("Select your file!");
    const [isValid, setValid] = useState(true);

    return <div>
        <input type="file"
               onInvalid={()=>{setValid(false)}}
               required={true}
               name={props.name}
               ref={(input) => {
                   inputRef = input;
               }}
               hidden
               onChange={(item=>{
                   props.onChange(item);
                   setName(item.target.value.substr(12));
               })}/>

        <div className="d-flex upload-file-custom-big" onClick={(e) => {
            e.preventDefault();
            inputRef.click();
        }}>

            <div className="upload-file-button w-100">
                <i className="fas fa-file-upload"/>
                <div>{props.title?props.title:'Upload'}</div>
            </div>
        </div>
    </div>
}