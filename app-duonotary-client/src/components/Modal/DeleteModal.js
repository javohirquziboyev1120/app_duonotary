import React, {Component} from 'react';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {connect} from "react-redux";



export default ({text, showDeleteModal, confirm, cancel}) => {
    return <Modal isOpen={showDeleteModal} toggle={cancel}>
            <ModalHeader toggle={cancel}
                         charCode="x">Delete</ModalHeader>
            <ModalBody>
                Are sure you want to delete <b className="text-danger">{text}</b> ?
            </ModalBody>
            <ModalFooter>
                <Button onClick={cancel} color="primary">No</Button>
                <Button type="button" className="btn btn-light" onClick={confirm}>Yes</Button>
            </ModalFooter>
        </Modal>
}

