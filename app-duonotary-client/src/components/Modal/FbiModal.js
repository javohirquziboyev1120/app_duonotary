import React, {Component} from 'react';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {connect} from "react-redux";


export default ({text, showFbiModal, confirm, cancel}) => {
    return <Modal  modalClassName="right" className="my-modal" isOpen={showFbiModal} toggle={cancel}>
        <ModalHeader toggle={cancel}
                     charCode="x">Change FBI</ModalHeader>
        <ModalBody>
            Are you sure change type {text} ?
        </ModalBody>
        <ModalFooter>
            <Button onClick={cancel} color="primary">No</Button>
            <Button type="button" outline onClick={confirm}
                    color="secondary">Yes</Button>
        </ModalFooter>
    </Modal>
}
