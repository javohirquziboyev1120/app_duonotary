import React from 'react'
import {Button, Modal, ModalBody, ModalFooter, ModalHeader, Row, Col} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {uploadFile} from "../../redux/actions/AttachmentAction";
import CloseBtn from "../CloseBtn";

export default ({dispatch, cancel, data, headerText, showCertificateModal, submit, isCertificate, states, isAdmin, stausEnums}) => {
    return <Modal isOpen={showCertificateModal} toggle={cancel} id="allModalStyle">
        <AvForm onValidSubmit={submit} className='bg-white'>
            <div className='closeBtnAgent'>
                <CloseBtn click={cancel} />
            </div>
            <ModalHeader
                charCode="x">{headerText}</ModalHeader>
            <ModalBody>
                <Row>
                    <Col>
                        <AvField type="file" name="passportAttachment" onChange={(item) => {
                            dispatch(uploadFile(item.target.files[0], "document"))
                        }} label="Upload your document picture:"
                                 required accept="image/*"/>
                    </Col>
                </Row>
                {/*<AvField type="date" name="issueDate" placeholder="Enter issue date"*/}
                {/*         defaultValue={data ? data.issueDate : ''} required/>*/}
                <AvField type="date" name="expireDate" label="Enter expire date:"
                         defaultValue={data ? data.expireDate : ''} required/>
                {isCertificate ?
                    <AvField type="select" name="state" label="Select state:" defaultValue={data ? data.countyId : ''}
                             required>
                        {states ? states.map((item) =>
                            <option value={item.id}>{item.name}</option>
                        ) : ''}
                    </AvField> : ''}

                {isAdmin ? <AvField type="select" name="statusEnum" defaultValue={data ? data.statusEnum : ''} required>
                    {stausEnums.map((item) =>
                        <option value={item}>{item}</option>
                    )}
                </AvField> : ''}
            </ModalBody>
            <ModalFooter>
                {/*<Button type="button" onClick={cancel} color="dark">Cancel</Button>*/}
                <Button type="submit" color={'info'} outline>Submit</Button>
            </ModalFooter>
        </AvForm>
    </Modal>
}