import React, {Component} from 'react';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

export default ({text, showModal, confirm, cancel,dispatch,path}) => {
    function pdf() {
        confirm({type:"pdf",url:path})
    }
    function excel() {
        confirm({type:"excel",url:path})
    }
    return <Modal isOpen={showModal} toggle={cancel}>
        <ModalHeader toggle={cancel} charCode="x">{text}</ModalHeader>
        <ModalFooter>
            <Button onClick={cancel} color="secondary">No</Button>
            <Button type="button" className="btn btn-danger" onClick={pdf}>PDF</Button>
            <Button type="button" className="btn btn-success" onClick={excel}>EXCEL</Button>
        </ModalFooter>
    </Modal>
}

