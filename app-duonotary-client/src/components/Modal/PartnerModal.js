import {AvForm,AvField} from "availity-reactstrap-validation"
import React, {useState} from 'react'
import {Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import {uploadFile} from "../../redux/actions/AttachmentAction";
export default ({dispatch,data,cancel,submit,showModal})=>{
    const [active, setActive] = useState(data?data.active:false);
    return <Modal isOpen={showModal} toggle={cancel}>
        <AvForm onValidSubmit={submit}>
            <ModalHeader>{data?'Edit partner':"Add partner"}</ModalHeader>
            <ModalBody>
                    <Row>
                        <Col md={12}><AvField name="a" type="file" onChange={(a)=>dispatch(uploadFile(a.target.files[0],'partner'))} label="Select picture"/></Col>
                        <Col md={12}><AvField name="name" defaultValue={data&&data.name} label="Name" placeholder="Enter name" required/></Col>
                        <Col md={12}><AvField name="description" defaultValue={data&&data.description} label="Description" placeholder="Enter description"/></Col>
                        <Col md={12} className='d-flex justify-content-center'>
                            <label className='labelDuo'>
                                <div className='labelBlock'>
                                    {active ? <div className='labelBox'>
                                        <i className="fas fa-check"/>
                                    </div> : ''}
                                </div>
                                <AvField className='d-none' name="active" onClick={()=>setActive(!active)} checked={active} type="checkbox"/>
                                Active
                            </label>
                        </Col>

                    </Row>
            </ModalBody>
            <ModalFooter>
                <Row>
                    <Button type="button" onClick={cancel} color="light" className="mr-3">cancel</Button>
                    <Button type="submit" color="primary">{data?'EDIT':'SAVE'}</Button>
                </Row>
            </ModalFooter>
        </AvForm>
    </Modal>
}