import React, {Component} from 'react';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

export default ({text, showOnlineModal, confirm, cancel}) => {
    return <Modal isOpen={showOnlineModal} toggle={cancel}>
        <ModalHeader toggle={cancel}
                     charCode="x">Change online</ModalHeader>
        <ModalBody>
            Are sure you want to change status {text} ?
        </ModalBody>
        <ModalFooter>
            <Button onClick={cancel} color="primary">No</Button>
            <Button type="button" outline onClick={confirm}
                    color="secondary">Yes</Button>
        </ModalFooter>
    </Modal>
}