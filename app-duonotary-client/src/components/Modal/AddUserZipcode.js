import React from 'react'
import  {Button,Modal,ModalFooter,ModalBody,ModalHeader} from "reactstrap";
import Select from "react-select";
export default ({zipCodes,submit,showModal,cancel})=>{
    let a='';
    function change(item){
        a=item;
    }
    function send(){
        submit(a)
        cancel();
    }
    return <Modal isOpen={showModal} toggle={cancel}>
        <ModalHeader>Add zip code</ModalHeader>
        <ModalBody>
            {!zipCodes&&!zipCodes[0]?<div className="text-danger">you do not have a zip code to add</div>:''}
            <Select
            isDisabled={!zipCodes&&!zipCodes[0]}
            defaultValue="Select Zipcode"
            isMulti
            name="zipCodesId"
            options={zipCodes}
            onChange={change}
            className="basic-multi-select modal-input"
            classNamePrefix="select"
        /></ModalBody>
        <ModalFooter>
            <Button color="light" onClick={cancel}>Cancel</Button>
            <Button color="primary" onClick={send}>Save</Button>

        </ModalFooter>
    </Modal>
}