import React from 'react';
import  {ModalFooter,ModalBody, Modal, Col, Input, Button} from "reactstrap";
import TimeStampToDate from "../TimeStampToDate";
import PhotoModal from "./PhotoModal";

export default ({certificate,states,openPhoto,dispatch})=>{
    let a=false;
    let id=''
    function aaa(item){
        id=item;
        a=false;
    }
    return <Modal isOpen={certificate}>
        <ModalBody>
            {certificate&&<Col md={12} key={certificate.id}>
              <div className="certificates-agent-page view">
                  <div className="certificate shadow-none">
                      {/*<div className="d-flex">*/}
                      {/*    <div className="name">Issue Date</div>*/}
                      {/*    <div className="ml-4">*/}
                      {/*        <TimeStampToDate item={certificate.issueDate}/>*/}
                      {/*    </div>*/}
                      {/*</div>*/}
                      <div className="d-flex ">
                          <div className="name">Expire Date</div>
                          <div className=" ml-4">
                              <TimeStampToDate item={certificate.expireDate}/>
                          </div>
                      </div>
                      <div className="d-flex mt-3">
                          <div className="name pt-1">Status</div>
                          <div className="ml-4">
                              <Input placeholder="enter status"
                                     value={certificate.statusEnum} readOnly={true}/></div>
                      </div>
                      <div className="d-flex mt-3">
                          <div className="name pt-1">State</div>
                          <div className="ml-4">
                              <Input placeholder="enter country"
                                     value={states !== null ? states.filter(item => item.id === certificate.stateDto.id)[0].name : ''}
                                     readOnly={true}/>
                          </div>
                      </div>
                      <div className="change-status">
                          <Button className="mr-3"
                                  onClick={() =>
                                      openPhoto(certificate.attachmentId)}>Document</Button>
                          {/*<Button*/}
                          {/*    onClick={() => openCertificateModal(certificate, true)}>Edit</Button>*/}
                      </div>
                  </div>
              </div>
            </Col>}
            <PhotoModal
            showModal={a}
            cancel={()=>a=false}
            id={certificate.attachmentId}
            />
        </ModalBody>
        <ModalFooter><Button onClick={()=>{dispatch({type:'updateState',payload:{certificate:''}})}}>Close</Button></ModalFooter>
    </Modal>
}