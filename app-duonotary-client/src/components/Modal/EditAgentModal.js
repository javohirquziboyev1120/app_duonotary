import React from 'react'
import {Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {removePhoto, uploadFile} from "../../redux/actions/AttachmentAction";
import {getAgent} from "../../redux/actions/AgentAction";

export default ({dispatch, cancel, data, showModal, submit}) => {
    function remove() {
        dispatch(removePhoto(data.id))
        dispatch(getAgent(data.id))
        cancel()
    }

    return <Modal isOpen={showModal} modalClassName="right" className="my-modal">

        <AvForm onValidSubmit={submit}>
            <ModalHeader
                charCode="x">Edit {data.firstName} {data.lastName} Agent</ModalHeader>
            <ModalBody>
                <Col md={12}>
                    <Row>
                        <Col md={6}>
                            <AvField type="file" name="photoId" onChange={(item) => {
                                dispatch(uploadFile(item.target.files[0], "avatar"))
                            }} label="Select avatar picture"
                                     required/>
                        </Col>
                        {data.photoId == null ? '' : <Col className="mt-4  pt-2">
                            <Button type="button" onClick={remove} className="mr-3">Remove photo</Button>
                        </Col>}
                    </Row>
                    <Row>
                        <Col md={6}><AvField defaultValue={data && data.firstName} name="firstName"
                                             placeholder='Enter first name' label="First name"/></Col>
                        <Col md={6}><AvField defaultValue={data && data.lastName} name="lastName"
                                             placeholder='Enter last name' label="Last name"/></Col>
                    </Row>
                    <Row>
                        <Col md={6}><AvField defaultValue={data && data.email} name="email"
                                             placeholder='Enter email name' label="Email"/></Col>
                        <Col md={6}><AvField defaultValue={data && data.phoneNumber} name="phoneNumber"
                                             placeholder='Enter Phone number' label="Phone number"/></Col>
                    </Row>
                    <Row>

                    </Row>
                </Col>
            </ModalBody>
            <ModalFooter>
                <Button type="button" onClick={cancel} className="mr-3">Cancel</Button>
                <Button type="submit" outline color="primary">Submit</Button>
            </ModalFooter>
        </AvForm>
    </Modal>
}