import React from 'react'
import {Button, Col, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {config} from "../../utils/config";

export default ({showModal, id, cancel}) => {
    let a = null
    return <Modal isOpen={showModal} modalClassName="confirm-modal" className="confirm-moda-content" toggle={cancel}>
        {id ? <div className="row">
            <img src={config.BASE_URL + "/attachment/" + id} className="img-fluid " alt=""/>
            <Button onClick={cancel} className="btn-block">Close</Button>
        </div> : ''}
    </Modal>
}