import React from 'react'
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import CloseBtn from "../CloseBtn";

export default ({headerText, showModal, submit, statusEnums,cancel}) => {
    return <Modal id="allModalStyle" isOpen={showModal}>
        <div className='position-absolute' style={{top : '40px', left : "-12%"}}>
            <CloseBtn click={cancel} />
        </div>
        <AvForm onValidSubmit={submit}>
            <ModalHeader
                charCode="x">{headerText}
            </ModalHeader>
            <ModalBody>
                <AvField
                    type='select'
                    name='statusEnum'
                    required
                    label='Select status'>
                    <option value='' disabled>
                        Select status
                    </option>
                    {statusEnums.map((item) => (
                        item!=='PENDING'&&<option key={item} value={item}>
                            {item}
                        </option>
                    ))}
                </AvField>
                <AvField name="description" placeholder="Enter Description" height="100px"/>
            </ModalBody>
            <ModalFooter>
               <div className="d-flex justify-content-start">
                   {/*<div> <Button type="button" onClick={cancel}>Close</Button></div>*/}
                   <div>
                       <Button
                        type="submit" outline
                        color="info"
                       >
                           Submit
                       </Button>
                   </div>
               </div>
            </ModalFooter>
        </AvForm>
    </Modal>
}