import React, {Component} from 'react';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

export default ({text, showDefaultInputModal, confirm, cancel}) => {
    return <Modal isOpen={showDefaultInputModal} toggle={cancel}>
        <ModalHeader toggle={cancel}
                     charCode="x">Change online </ModalHeader>
        <ModalBody>
            Are you sure change  {text} ?
        </ModalBody>
        <ModalFooter>
            <Button onClick={cancel} color="primary">No</Button>
            <Button type="button" outline onClick={confirm}
                    color="secondary">Yes</Button>
        </ModalFooter>
    </Modal>
}