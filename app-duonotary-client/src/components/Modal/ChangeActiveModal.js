import React from 'react'
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";

export default ({showModal, submit, cancel, headText}) => {
    function a() {
        submit();
        cancel();
    }
    return <Modal isOpen={showModal} toggle={cancel} >
        <ModalHeader>{headText} </ModalHeader>
        <ModalFooter>
            <Button onClick={cancel}>Cancel</Button>
            <Button onClick={a} color="primary">Ok</Button>
        </ModalFooter>
    </Modal>
}