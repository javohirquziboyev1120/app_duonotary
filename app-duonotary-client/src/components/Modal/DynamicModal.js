import React, {Component} from 'react';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {AvField} from "availity-reactstrap-validation";

export default ({text, showDynamicModal, confirm, cancel}) => {
    return <Modal isOpen={showDynamicModal} toggle={cancel}>
        <ModalHeader toggle={cancel}
                     charCode="x">Change online</ModalHeader>
        <ModalBody>
            Are you sure change dynamic {text} ?
        </ModalBody>
        <ModalFooter>
            <Button onClick={cancel} color="primary">No</Button>
            <Button type="button" outline onClick={confirm}
                    color="secondary">Yes</Button>
        </ModalFooter>
    </Modal>
}