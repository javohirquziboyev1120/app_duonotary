import Translate from "react-translate-component";
import React from "react";
import counterpart from "counterpart";
// import * as GenericActions from "../../store/actions/GenericActions";
import {connect} from "react-redux";

const PublicHeader = (props) => {
    const {generic: {lang}} = props;
    const onLangChange = (e) => {
        counterpart.setLocale(e.key);
        localStorage.setItem(LANG_STORAGE, e.key);
        props.dispatch(GenericActions.setLocale(e.key));
    };

    return (
        <div className="pg_landing">
            <div className="pg_landing__container">
                <figure className="pg_landing__header">
                    <div className="pg_landing__logo d-inline">
                        <img src="https://via.placeholder.com/600/771796" alt="aa" style={{cursor: 'pointer'}}/>
                    </div>
                    <div>
                    </div>
                </figure>
                <div className="pg_landing__main">
                    <div className="pg_landing__main-innerbox">
                        {props.children}
                    </div>
                </div>
            </div>
        </div>
    )
}


export default connect(({generic}) => ({
    generic
}))(PublicHeader);
