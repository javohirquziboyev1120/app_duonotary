import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {Button, Col, Input, Row} from "reactstrap";
import PhotoModal from "../Modal/PhotoModal";
import ChangeStatusModal from "../Modal/ChangeStatusModal";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {toast} from "react-toastify";
import {changeDocumentStatus} from "../../redux/actions/AgentAction";

class AgentDocumentComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            photoId: '',
            currentDocument: '',
            isCertificate: false
        };
    }

    changeStatus=(e, v)=> {
        let {currentDocument}=this.state;
        if (v.statusEnum === "PENDING")
            toast.error("You cannot select Pending")
        else if (v.statusEnum === "REJECTED" && v.description.length < 5) {
            toast.error("description is empty")
        } else {
            let bool=true;
            v.documentId = currentDocument&&currentDocument.id?currentDocument.id:bool=false;
            v.currentUserId = this.props.agent&&this.props.agent.id?this.props.agent.id:bool=false;
            if (bool)this.props.dispatch(changeDocumentStatus(v));
            else {
                toast.error("Try again");
                setTimeout(()=>window.location.reload(),1000)
            }
        }
    }

    render() {
        console.log(this.state)
        const {passports, certificates, states, statusEnums, agent} = this.props;
        const {photoId, currentDocument, isCertificate} = this.state;
        return (
            <div className="admin-agent-passport">
                <Row>
                    <Col md={6}>
                        <div className="mt-3">Agent's Photo ID</div>
                        {passports && passports.map((passport) =>
                            <div key={passport.id} className="passport">
                                {/*<div className="d-flex">*/}
                                {/*    <div className="name">Issue Date</div>*/}
                                {/*    <div className="ml-4">*/}
                                {/*        <DatePicker*/}
                                {/*            selected={new Date(passport.issueDate)}*/}
                                {/*            // onChange={selectStartDate}*/}
                                {/*        />*/}
                                {/*    </div>*/}
                                {/*</div>*/}
                                <div className="d-flex">
                                    <div className="name">Expiration Date</div>
                                    <div className=" ml-4">
                                        <DatePicker
                                            selected={new Date(passport.expireDate)}
                                            // onChange={selectFinishDate}
                                        />
                                    </div>
                                </div>
                                <div className="d-flex mt-3">
                                    <div className="name pt-1">Status</div>
                                    <div className="ml-4">
                                        <Input placeholder="enter status"
                                               onChange={() => {
                                               }}
                                               defaultValue={passport.statusEnum}
                                               readOnly={true}/>

                                    </div>
                                </div>
                                <div style={{marginTop: "52px"}}
                                     className="change-status">
                                    <Button className="mr-3"
                                            onClick={() => this.setState({photoId: passport.attachmentId})}>Document</Button>
                                    <Button
                                        onClick={() => this.setState({
                                            currentDocument: passport,
                                            isCertificate: false
                                        })}>Change
                                        Status</Button>
                                </div>
                            </div>
                        )}
                    </Col>
                    <Col md={6}>
                        <div className="mt-3">Agent's Notary certificate</div>
                        {certificates && certificates.map((certificate) =>
                            <div key={certificate.id} className="passport">
                                {/*<div className="d-flex">*/}
                                {/*    <div className="name">Issue Date</div>*/}
                                {/*    <div className="ml-4">*/}
                                {/*        <DatePicker*/}
                                {/*            selected={new Date(certificate.issueDate)}*/}
                                {/*            // onChange={selectStartDate}*/}
                                {/*        />*/}
                                {/*    </div>*/}
                                {/*</div>*/}
                                <div className="d-flex ">
                                    <div className="name">Expiration Date</div>
                                    <div className=" ml-4">
                                        <DatePicker
                                            selected={new Date(certificate.expireDate)}
                                            // onChange={selectFinishDate}
                                        />
                                    </div>
                                </div>
                                <div className="d-flex mt-3">
                                    <div className="name pt-1">State</div>
                                    <div className="ml-4">
                                        <Input placeholder="enter country"
                                               value={states && states.filter(item => item.id === certificate.stateDto.id)[0].name}/>
                                    </div>
                                </div>
                                <div className="d-flex mt-3">
                                    <div className="name pt-1">Status</div>
                                    <div className="ml-4">
                                        <Input placeholder="enter status"
                                               value={certificate.statusEnum}/></div>
                                </div>
                                <div className="change-status">
                                    <Button className="mr-3"
                                            onClick={() => this.setState({photoId: certificate.attachmentId})}>Document</Button>
                                    <Button
                                        onClick={() => this.setState({
                                            currentDocument: certificate,
                                            isCertificate: false
                                        })}>Change
                                        Status</Button>
                                </div>
                            </div>
                        )}
                    </Col>

                </Row>

                <PhotoModal
                    showModal={photoId.length>0}
                    id={photoId}
                    cancel={() => this.setState({photoId: ''})}
                />
                <ChangeStatusModal
                    submit={this.changeStatus}
                    headerText={isCertificate ? "Certificate" : "Passport"}
                    showModal={currentDocument.id}
                    statusEnums={statusEnums}
                    cancel={() => this.setState({})}
                />
            </div>
        );
    }
}

AgentDocumentComponent.propTypes = {};

export default connect(({agent: {statusEnums}}) => ({statusEnums}))(AgentDocumentComponent);