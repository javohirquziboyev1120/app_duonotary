import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import Switch from "react-switch";
import {toast} from "react-toastify";
import {
    addUserZipcodeAction, addUserZipcodeActionForAdmin,
    changeActiveUserZipcodeAction,
    changeEnableUserZipcodeAction, getZipCodeByUserForAdmin
} from "../../redux/actions/AppAction";
import ChangeActiveModal from "../Modal/ChangeActiveModal";
import AddUserZipcode from "../Modal/AddUserZipcode";
import AgentLayout from "../AgentLayout";
import {AddIcon} from "../Icons";

class AgentUserZipCodes extends Component {
    constructor(props) {
        super(props);
        this.state={
            zipcodeEnable:false,
            currentItem:"",
            modal:''
        }
    }
    componentDidMount() {
        this.props.dispatch(getZipCodeByUserForAdmin(this.props.agent.id));
    }

    checkActive(item) {
        return item ? 'Active' : 'Disactive';
    }
     changeEnable = () => {
        let {currentItem} = this.state;
        let obj = {...currentItem};
        obj.enable = !obj.enable;
        obj.active = false;
        let arr = [...this.props.userZipCodes];
        arr.map((item, i) => {
            if (item.id === obj.id) arr.splice(i, 1, obj);
        })
        this.props.dispatch(changeEnableUserZipcodeAction(currentItem.id))
        this.props.dispatch({type: 'updateState', payload: {userZipCodes: arr}})
    }
    render() {
        const {userZipCodes,agent,dispatch,zipCodes}=this.props;
        const {modal}=this.state;
        return (
            <div className='zipBlock mt-5'>
                <div className='d-flex align-items-center width-100 mb-5'>
                    <div className=''><AddIcon onClick={()=>this.setState({modal:true})}/></div>
                    <div className='font-weight-bold' style={{fontSize : '24px'}}>Add New ZipCode</div>
                </div>
                {userZipCodes&&userZipCodes[0] ?
                    userZipCodes.map(item =>
                      <div className='zipBox'>
                            <div className='zipNumber'>{item.zipCode.code} </div>
                            <div
                                className='zipNumber'>{item.zipCode.county.state.name} </div>
                            <div className='zipStatus my-3'>Status : <span
                                className={item.active ? 'text-success' : 'text-danger'}>{this.checkActive(item.active)}</span>
                            </div>
                            <div className='zipSwitch'>
                                <Switch
                                    checked={item.enable}
                                    onChange={() => {
                                        if (agent.active) {
                                            this.setState({
                                                zipcodeEnable: true,
                                                currentItem: item
                                            })
                                        } else toast.error('Activate the agent first')
                                    }}
                                />
                            </div>
                        </div>
                    )
                    : <div className="row mx-auto no">Agent does not have zipcodes</div>}

                {this.state.currentItem&&<ChangeActiveModal
                    cancel={() => this.setState({zipcodeEnable: false})}
                    showModal={this.state.zipcodeEnable}
                    headText={"Do you want to change enable this zip code : " + this.state.currentItem.zipCode.code + " ?"}
                    submit={this.changeEnable}/>}
                {modal&&<AddUserZipcode
                    submit={(item)=>dispatch(addUserZipcodeActionForAdmin({userId:agent.id,zipCodes:item.map(item=>{return item.value})}))}
                    zipCodes={zipCodes}
                    showModal={modal}
                    cancel={()=>this.setState({modal:false})}
                />}
            </div>
        );
    }
}

AgentUserZipCodes.propTypes = {};

export default connect(({app:{zipCodes}})=>({zipCodes}))(AgentUserZipCodes);