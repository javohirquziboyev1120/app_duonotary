import React from "react";
import "./Style.scss";



function PostBig(props) {
 return <div  className="flex-column post-big">
     <div className="img-container" onClick={props.onClick} ><img src={props.mainImg} alt="blog-img"/></div>
     <span className="title-big">{props.title}</span>
     <span className="preview" dangerouslySetInnerHTML={{ __html: props.body }}/>
     <div className="blog-author flex-row align-items-center justify-content-between">
         <div className="flex-row">

             <span className="publish-date">{props.publishDate}</span>
         </div>
         <div className="flex-row read-more" onClick={props.onClick} >
             <span>Read more</span>
             <RightArrow />
         </div>

     </div>
 </div>
}

function PostSmall(props) {
    return <div className="flex-column align-items-center post-small">
        <div className="img-container" onClick={props.onClick} ><img src={props.mainImg} alt="blog-img"/></div>
        <span className="title-small">{props.title}</span>
    </div>
}


export { PostBig, PostSmall};


function RightArrow(props) {
    return <div onClick={props.onClick} className={props.className}>
        <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M9.129 5.25L5.106 1.227L6.1665 0.166504L12 6L6.1665 11.8335L5.106 10.773L9.129 6.75H0V5.25H9.129Z" fill="black"/>
        </svg>

    </div>
}