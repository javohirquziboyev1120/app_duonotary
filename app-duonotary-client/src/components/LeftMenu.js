import React, {Component} from "react";
import {Link, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {LogoDuo} from "./Icons";
import './LeftMenu.scss';

class LeftMenu extends Component {
    render() {
        const {onToggleModal, show} = this.props;

        return (
            <div className="home-page-left leftMenu">
                <div className='logoLeft'>
                    <Link to="/">
                        <LogoDuo/>
                    </Link>
                </div>
                <div className='menuBlock' onClick={onToggleModal}>
              {!show ? (
                  <svg width="2.5vw" height="7vh" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                          d="M5.07245 6.33333H31.3224V9.24999H5.07245V6.33333ZM5.07245 16.5417H31.3224V19.4583H5.07245V16.5417ZM5.07245 26.75H31.3224V29.6667H5.07245V26.75Z"
                          fill="#313E47"/>
                  </svg>
              ) : (
                  <svg width="1.7vw" height="4vh" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path
                          d="M9.50007 7.43791L16.7188 0.219162L18.7809 2.28125L11.5622 9.49999L18.7809 16.7187L16.7188 18.7808L9.50007 11.5621L2.28132 18.7808L0.219238 16.7187L7.43799 9.49999L0.219238 2.28125L2.28132 0.219162L9.50007 7.43791Z"
                          fill="#313E47"/>
                  </svg>
              )}
                </div>
                <p className="textLeft">
                    © Copyright 2020 DuoNotary. <br/>
                    All Rights Reserved.
                </p>
            </div>
        );
    }
}

export default withRouter(
    connect(({auth: {loading, mainMenu}}) => ({loading, mainMenu}))(
        LeftMenu
    )
);
