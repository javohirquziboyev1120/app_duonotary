export default function getDate(item, type) {
    let monthNames = {
        '01': 'Jun',
        '02': "Feb",
        '03': "Mar",
        '04': "Apr",
        '05': "May",
        '06': "Jun",
        '07': "Jul",
        '08': "Aug",
        '09': "Sep",
        '10': "Oct",
        '11': "Nov",
        '12': "Dec"
    }
    let date = new Date(item);
    let day = String(date.getDate()).padStart(2, '0');
    let month = monthNames[String(date.getMonth() + 1).padStart(2, '0')];
    let year = date.getFullYear()
    let hour = date.getHours()
    let min = date.getMinutes().toString().length < 2 ? '0' + date.getMinutes() : date.getMinutes();
    if (type === 'day')
        return day + '-' + month + ', ' + year;
    if (type === 'time')
        return hour + ':' + min;
    return "";
}