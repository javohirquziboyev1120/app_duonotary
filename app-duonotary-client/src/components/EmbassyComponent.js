import React, {Component} from 'react';
import {changePrintDocumentCount} from "../redux/actions/OrderAction";
import './Embassy.scss'
import {InputForm} from "./InputForm";
import {connect} from "react-redux";
import {getCountiesList, getEmbassy} from "../redux/actions/AppAction";
import {toast} from "react-toastify";


class EmbassyComponent extends Component {
    constructor(props) {
        super(props);
        const {data,embassyCountries} = this.props;
        this.state = {
            id: data ? data.id : '',
            firstName: data ? data.firstName : '',
            lastName: data ? data.lastName : '',
            phoneNumber: data ? data.phoneNumber : '',
            email: data ? data.email : '',
            country: data ? data.country&&{id:data.country.id} : 0,
            countryId: data ? data.country&&data.country.id : 0,
            pickUpAddress: data ? data.pickUpAddress : '',
            numberDocument: data ? data.numberDocument : 1,
            message: data ? data.message : '',
        }
    }

    handleChange = (e) => {
        const {name, value} = e.target
        if(name==="countryId"){
           this.setState({country:{id:value}})
        }
        this.setState({
            [name]: value,
        }, () => {
            if (this.props.data) {
                this.props.dispatch({type: 'updateState', payload: {internationalDto: this.state, countDocument:parseInt(this.state.numberDocument)}})
            }
        })
    }

    save = (e, v) => {
        e.preventDefault();
        let obj = {...this.state}
        if (!obj.country&&!obj.country.id){
            toast.error("Select country")
            return "";
        }
        obj.embassy = true;
        this.props.dispatch({type: 'updateState', payload: {internationalDto: obj, stage: 3,countDocument:parseInt(obj.numberDocument)}})
    }

    render() {
        const {submit, dispatch, mobile, embassyCountries, data, isAdd,isClient} = this.props;
        const {
            firstName,
            lastName,
            phoneNumber,
            email,
            country,
            countryId,
            pickUpAddress,
            numberDocument,
            message,
        } = this.state;
        console.log(data);
        return (
            <div className='embassy pt-3'>
                <form className='formGroupEmbassy' onSubmit={this.save}>
                    <div className='row m-0 formInputGroup'>
                        <div className={`col-md-${mobile ? '12' : '6'}`}>
                            <InputForm
                                required={true}
                                title={'First Name'}
                                name="firstName"
                                value={firstName}
                                id="firstName"
                                readOnly={isClient}
                                placeholder="First name"
                                handleChange={this.handleChange}
                            />
                        </div>
                        <div className={`col-md-${mobile ? '12' : '6'}`}>
                            <InputForm
                                required={true}
                                title={'Last name'}
                                name="lastName"
                                value={lastName}
                                id="lastName"
                                readOnly={isClient}
                                placeholder="LastName"
                                handleChange={this.handleChange}
                            />
                        </div>
                        <div className={`col-md-${mobile ? '12' : '6'}`}>
                            <InputForm
                                required={true}
                                title={'Phone number'}
                                name="phoneNumber"
                                value={phoneNumber}
                                id="phoneNumber"
                                readOnly={isClient}
                                placeholder="Phone number"
                                handleChange={this.handleChange}
                            />
                        </div>
                        <div className={`col-md-${mobile ? '12' : '6'}`}>
                            <InputForm
                                required={true}
                                title={'Email'}
                                name="email"
                                value={email}
                                id="email"
                                readOnly={isClient}
                                placeholder="Email"
                                handleChange={this.handleChange}
                            />
                        </div>
                        <div className={`col-md-${mobile ? '12' : '6'}`}>
                            <div className='form-group'>
                                <label>Select embassy country</label>
                                <select
                                    className='form-control h_input py-0'
                                    name="countryId"
                                    value={countryId}
                                    id="countryId"
                                    onChange={this.handleChange}
                                >
                                    <option value="0">Select embassy country</option>
                                    {embassyCountries && embassyCountries.map(country =>
                                        <option key={country.id} value={country.id}>{country.name}</option>
                                    )}
                                </select>
                            </div>
                        </div>
                        <div className={`col-md-${mobile ? '12' : '6'}`}>
                            <InputForm
                                required={true}
                                title={'Pick up Address'}
                                name="pickUpAddress"
                                value={pickUpAddress}
                                id="pickUpAddress"
                                readOnly={isClient}
                                placeholder="Enter pick up address"
                                handleChange={this.handleChange}
                            />
                        </div>
                        <div className={`col-md-${mobile ? '12' : '6'}`}>
                            <InputForm
                                required={true}
                                type="number"
                                title={'Count document'}
                                name="numberDocument"
                                value={numberDocument}
                                id="numberDocument"
                                readOnly={isClient}
                                placeholder="Select number document"
                                handleChange={this.handleChange}
                            />
                        </div>
                        <div className={`col-md-${mobile ? '12' : '6'}`}>
                            <InputForm
                                required={true}
                                title={'Message'}
                                name="message"
                                value={message}
                                id="message"
                                readOnly={isClient}
                                placeholder="Message"
                                handleChange={this.handleChange}
                            />
                        </div>
                        <div className='col-md-2 ml-auto'>
                            {isAdd ?
                                <button type='submit' className='btn btn-primary btn-block p-2'
                                        style={{cursor: 'pointer'}}>Next</button>
                                : ''}
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

EmbassyComponent.propTypes = {};
export default connect(({}) => ({}))(EmbassyComponent);