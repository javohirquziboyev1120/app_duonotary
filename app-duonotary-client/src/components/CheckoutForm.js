import React, {useEffect, useState} from "react";
import axios from 'axios'
import {CardElement, useElements, useStripe} from '@stripe/react-stripe-js';
import {config} from "../utils/config";
import AlertModal from "./AlertModal";
import {toast} from "react-toastify";
import {getOrdersForClientMainPage} from "../redux/actions/OrderAction";
import {useDispatch} from "react-redux";

const CARD_ELEMENT_OPTIONS = {
    style: {
        base: {
            color: '#32325d',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    }
};


const CheckoutForm = (props) => {
    const [error, setError] = useState(null);
    const [clientSecret, setClientSecret] = useState('');
    const stripe = useStripe();
    const elements = useElements();
    const [succeeded, setSucceeded] = useState(false);
    const [processing, setProcessing] = useState(false);
    const [disabled, setDisabled] = useState(true);
    const [stopEffect, setStopEffect] = useState(true);
    const dispatch = useDispatch();

    useEffect(() => {
        if (stopEffect) {
            setStopEffect(false)
            axios.get(config.BASE_URL + "/payment/getTestFutureCharge?orderId=" + props.props.orderId).then(resp => {
                setClientSecret(resp.data)
            })
        }
        // dispatch(paymentNow({id:id}))
    });


    const handleChange = async (event) => {
        setDisabled(event.empty);
        setError(event.error ? event.error.message : "");
    };

    const handleSubmit = async (ev) => {
        ev.preventDefault();
        setProcessing(true);

        const payload = await stripe.confirmCardPayment(clientSecret, {
            payment_method: {
                card: elements.getElement(CardElement),
                billing_details: {
                    name: ev.target.name.value
                }
            }
        })
        if (payload.error) {
            setError(`Payment failed ${payload.error.message}`);
            setProcessing(false);
        } else {
            axios.get(config.BASE_URL + "/payment/config?orderId=" + props.props.orderId+'&secretId='+payload.paymentIntent.id).then(resp => {
                if (resp.data.success){
                    setError(null);
                    setProcessing(false);
                    setSucceeded(true);
                }else{
                    toast.error("Sorry something wrong")
                }
                dispatch(getOrdersForClientMainPage({page: 0, size: 5}))
            })

        }


    };
    return (
        succeeded ? <AlertModal title={"Payment"} body={"Congratulation. Your payment was successfully "}
                                action={() => {
                                    props.props.history.push(window.location.pathname==='/client/inPerson'|| '/client/main-page'?'/client/main-page':'/')
                                }}/> : <form
            id="payment-form"
            onSubmit={handleSubmit}>
            <CardElement id="card-element"
                         className="card-style"
                         onChange={handleChange}
                         options={{hidePostalCode: true}}
            />
            <button
                disabled={processing || disabled || succeeded}
                id="submit"
                className="payment-btn"
            >
        <span id="button-text">
          {processing ? (
              <div className="spinner" id="spinner"/>
          ) : (
              "Pay"
          )}
        </span>
            </button>
            {/* Show any error that happens when processing the payment */}
            {error && (
                <div className="card-error" role="alert">
                    {error}
                </div>
            )}
            {/* Show a success message upon completion */}
            <p className={succeeded ? "result-message" : "result-message hidden"}>
                Payment succeeded, see the result in your
                <a
                    href={`https://dashboard.stripe.com/test/payments`}
                >
                    {" "}
                    Stripe dashboard.
                </a> Refresh the page to pay again.
            </p>
        </form>
    );
}

export default (CheckoutForm)