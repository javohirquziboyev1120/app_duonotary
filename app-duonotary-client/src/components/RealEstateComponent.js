import React, {Component} from 'react';
import {Button, Col, Input, Row} from "reactstrap";
import {uploadDoc} from "../redux/actions/AttachmentAction";
import {InputForm} from "./InputForm";
import './Embassy.scss'
import Counter from "./Counter";
import {toast} from "react-toastify";

class RealEstateComponent extends Component {
    constructor(props) {
        super(props);
        const {data} = this.props;
        this.state = {
            id: data ? data.id : '',
            // requester : data ? data.requester : '',
            // requesterPhone : data ? data.requesterPhone : '',
            // requesterEmail : data ? data.requesterEmail : '',
            // requesterAddress : data ? data.requesterAddress : '',
            realEstateEnum: data ? data.realEstateEnum : null,
            file: '',
            clientName: data ? data.clientName : '',
            clientEmail: data ? data.clientEmail : '',
            clientPhone: data ? data.clientPhone : '',
            clientAddress: data ? data.clientAddress : '',
            // message : data ? data.message : '',
            selectEnum: data ? data.realEstateEnum : 'Select Service type',
            enumArray: [
                {key: "TITLE_COMPANY", value: "Title Company"},
                {key: "AGENCY", value: "Real estate Agency"},
                {key: "HOME", value: "Home buyer/seller"},
            ]
        }
    }

    handleChange = (e) => {
        const {name, value} = e.target
        console.log(name,value);
        if (name === "selectEnum") {
            this.setState({
                realEstateEnum: value,
            })
        }
        this.setState({
            [name]: value,
        }, () => {
            if (this.props.data) {
                this.props.dispatch({type: 'updateState', payload: {realEstateDto: this.state, countDocument: 1}})
            }
        })
    }

    save = (e) => {
        e.preventDefault();
        let obj = {...this.state}
        if(this.state.selectEnum==='Select Service type'){
            toast.error("Select service type")
            return ''
        }
        if(this.state.selectEnum!=='Select Service type')this.props.dispatch({type: 'updateState', payload: {realEstateDto: obj, stage: 3, countDocument: 1}})
    }

    render() {

        const {submit, printDocumentCount, printDocumentPrice, dispatch, data, isAdd, isClient,orderAdditionalServices,additionalServicePrices,servicePrice} = this.props
        const {
            requester,
            requesterPhone,
            requesterEmail,
            requesterAddress,
            file, selectEnum,
            enumArray,
            clientName,
            clientPhone,
            clientAddress,
            clientEmail,
            message,
        } = this.state;
        console.log(data);
        function change(v, i) {
            let oldArr = [...orderAdditionalServices];
            if (orderAdditionalServices[0]) {
                orderAdditionalServices.map((value, i) => {
                    if (value.additionalServicePriceId === v.id)
                        oldArr.splice(i, 1)
                })
            }
            oldArr.push({price: v.price * i, count: i, additionalServicePriceId: v.id})
            dispatch({
                type: 'updateState',
                payload: {
                    orderAdditionalServices: oldArr
                }
            })

        }


        return (
            <div className={'embassy'}>
                <form onSubmit={this.save}
                      className={this.props.class ? 'formGroupEmbassy bg-transparent' : 'formGroupEmbassy'}>
                    <Row className="m-0">
                        {/*<Col md={this.props.class ? 12 : 6}>*/}
                        {/*    <InputForm*/}
                        {/*        required={true}*/}
                        {/*        readOnly={isClient}*/}
                        {/*        title={'Requester'}*/}
                        {/*        name={'requester'}*/}
                        {/*        placeholder={'Requester'}*/}
                        {/*        value={requester}*/}
                        {/*        id="requester"*/}
                        {/*        handleChange={this.handleChange}*/}
                        {/*    />*/}
                        {/*</Col>*/}
                        {/*<Col md={this.props.class ? 12 : 6}>*/}
                        {/*    <InputForm*/}
                        {/*        required={true}*/}
                        {/*        readOnly={isClient}*/}
                        {/*        title={'Requester Phone Number'}*/}
                        {/*        name={'requesterPhone'}*/}
                        {/*        placeholder={'Requester Phone Number'}*/}
                        {/*        value={requesterPhone}*/}
                        {/*        id="requesterPhone"*/}
                        {/*        handleChange={this.handleChange}*/}
                        {/*    />*/}
                        {/*</Col>*/}
                        {/*<Col md={this.props.class ? 12 : 6}>*/}
                        {/*    <InputForm*/}
                        {/*        required={true}*/}
                        {/*        type={'email'}*/}
                        {/*        title={'Requester Email'}*/}
                        {/*        name={'requesterEmail'}*/}
                        {/*        placeholder={'Requester Email'}*/}
                        {/*        value={requesterEmail}*/}
                        {/*        id="requesterEmail"*/}
                        {/*        handleChange={this.handleChange}*/}
                        {/*        readOnly={isClient}*/}
                        {/*    />*/}
                        {/*</Col>*/}
                        {/*<Col md={this.props.class ? 12 : 6}>*/}
                        {/*    <InputForm*/}
                        {/*        required={true}*/}
                        {/*        readOnly={isClient}*/}
                        {/*        title={'Requester Address'}*/}
                        {/*        name={'requesterAddress'}*/}
                        {/*        placeholder={'Requester Address'}*/}
                        {/*        value={requesterAddress}*/}
                        {/*        id="requesterAddress"*/}
                        {/*        handleChange={this.handleChange}*/}
                        {/*    />*/}
                        {/*</Col>*/}

                        <Col md={this.props.class ? 12 : 6}>
                            <div className='form-group'>
                                <label>Select embassy country</label>
                                <select
                                    className='form-control h_input py-0'
                                    name="selectEnum"
                                    value={selectEnum}
                                    id="selectEnum"
                                    onChange={this.handleChange}
                                >
                                    <option value="0">Select service type</option>
                                    {enumArray && enumArray.map(item =>
                                        <option key={item.key} value={item.key}>{item.value}</option>
                                    )}
                                </select>
                            </div>
                        </Col>
                        {isAdd ?
                            <Col md={this.props.class ? 12 : 6}>
                                <Input
                                    type="file"
                                    name="file"
                                    id="file"
                                    placeholder="File"
                                    onChange={item => dispatch(uploadDoc(item.target.files[0], "real"))}
                                    label="File"
                                    readOnly={isClient}
                                />
                            </Col>
                            : ''}
                        {selectEnum !== 'HOME' && selectEnum !== 'Select Service type' &&
                        <div className="row justify-content-between w-100">
                            <Col md={12} className='text-center my-2'><h4>Client Info</h4></Col>
                            <Col md={this.props.class ? 12 : 6}>
                                <InputForm
                                    required={true}
                                    readOnly={isClient}
                                    title={'Client Name'}
                                    name={'clientName'}
                                    placeholder={'Client Name'}
                                    value={clientName}
                                    id="clientName"
                                    handleChange={this.handleChange}
                                />
                            </Col>
                            <Col md={this.props.class ? 12 : 6}>
                                <InputForm
                                    required={true}
                                    readOnly={isClient}
                                    title={'Client Phone'}
                                    name={'clientPhone'}
                                    placeholder={'Client Phone'}
                                    value={clientPhone}
                                    id="clientPhone"
                                    handleChange={this.handleChange}
                                />
                            </Col>
                            <Col md={this.props.class ? 12 : 6}>
                                <InputForm
                                    required={true}
                                    readOnly={isClient}
                                    title={'Client Address'}
                                    name={'clientAddress'}
                                    placeholder={'Client Address'}
                                    value={clientAddress}
                                    id="clientAddress"
                                    handleChange={this.handleChange}
                                />
                            </Col>
                            {/*<Col md={this.props.class ? 12 : 6}>*/}
                            {/*    <Label>Client Date</Label>*/}
                            {/*    <Input name="clientDate" type="date" id="clientDate" placeholder="Client Date" defaultValue={data&&data.clientDate} label="Client Date"/></Col>*/}
                            <Col md={this.props.class ? 12 : 6}>
                                <InputForm
                                    required={true}
                                    readOnly={isClient}
                                    title={'Client Email'}
                                    name={'clientEmail'}
                                    placeholder={'email'}
                                    value={clientEmail}
                                    id="clientEmail"
                                    handleChange={this.handleChange}
                                />
                            </Col>
                        </div>}
                        {isAdd&&!!servicePrice && !!additionalServicePrices[0] ? additionalServicePrices.map(v =>

                                <div className="w-50" key={v.id}>
                                    <Col md={3} className="text-center">
                                        <Counter
                                            value={!JSON.stringify(orderAdditionalServices[0] === JSON.stringify([])) ? orderAdditionalServices.filter(o => o.additionalServicePriceId === v.id)[0].count : 0}
                                            getValue={i => change(v, i)}
                                        />
                                    </Col>

                                    <Col md={9} className="name">
                                        {v.additionalServiceDto.name + " - $" + v.price}
                                    </Col>
                                </div>
                            ) : ''}

                        {/*<Col md={this.props.class ? 12 : 6}>*/}
                        {/*    <Label>Client Time</Label>*/}
                        {/*    <Input name="clientTime" type="time" id="clientTime" placeholder="Client Time" defaultValue={data&&data.clientTime}  label="Client Time"/></Col>*/}

                        {/*<Col md={this.props.class ? 12 : 6}>*/}
                        {/*    <Label>Massage</Label>*/}
                        {/*    <Input name="message" id="message" placeholder="Massage" defaultValue={data&&data.massage} label="Message"/></Col>*/}

                        {isAdd ?
                            <Col md={2} className='ml-auto'><Button type={'submit'} color="primary"
                                                                    className="btn-block">Next</Button></Col>
                            : ''}
                    </Row>
                </form>
            </div>
        )
    }
}

export default RealEstateComponent;