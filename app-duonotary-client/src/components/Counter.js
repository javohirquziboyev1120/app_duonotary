import React, {Component} from 'react';

class Counter extends Component {
    constructor(props) {
        super(props);
        this.state={
            counter: this.props.value,
        }
    }
    increase = () =>{
        this.setState({counter: this.state.counter+1}, () => {
            this.props.getValue(this.state.counter);
        });
    }
    decrease = () =>{
        if(this.state.counter>0) {
            this.setState({counter: this.state.counter-1}, ()=>{
                this.props.getValue(this.state.counter);
            });
        }
    }

    render() {
        return (
            <div className="d-flex align-items-center counter">
                <div className="inc-dec" onClick={this.decrease}><i className="fas fa-minus"/></div>
                <div className="count">{this.state.counter}</div>
                <div className="inc-dec" onClick={this.increase}><i className="fas fa-plus"/></div>
            </div>
        );
    }
}

Counter.propTypes = {};

export default Counter;