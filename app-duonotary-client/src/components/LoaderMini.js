import React from "react";
import "./LoaderMini.sass";

function LoaderMini() {
    return (
        <div className="container-loading">
            <div className="loader">
                <span className="loader--dot"></span>
                <span className="loader--dot"></span>
                <span className="loader--dot"></span>
                <span className="loader--dot"></span>
                <span className="loader--dot"></span>
                <span className="loader--text"></span>
            </div>
        </div>
    );
}

export default LoaderMini;
