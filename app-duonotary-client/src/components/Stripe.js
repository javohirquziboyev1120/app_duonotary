import React from 'react';
import {Elements} from "@stripe/react-stripe-js";
import {loadStripe} from "@stripe/stripe-js/pure";
import CheckoutForm from "./CheckoutForm";
import CardForm from "./CardForm";
import "./Stripe.scss"
import {connect} from "react-redux";

const Stripe = (type) => {

    const stripe = loadStripe("pk_test_51H7X5PAggaXTCRgQCiD5g3oi19i1D0ylWnNAYfS4rgDPRv6tXUDYDZmSaFJlf6WckjFpsv2qmhmzNhAKT08pmbFf00ezOlOnZm")

    return (
        <div>
            {type.type==="now"?
                /*Bu osha zaxoti pulini yechish uchun */
                <Elements stripe={stripe}>
                    <CardForm data={type}/>
                </Elements>:
                /*Bu future pulini yechish uchun */
                <Elements stripe={stripe}>
                    <CheckoutForm props={type}/>
                </Elements>
            }
        </div>
    );

}




export default connect(({order:{}})=>({}))(Stripe);