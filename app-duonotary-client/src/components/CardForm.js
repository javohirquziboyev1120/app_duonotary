import React, {useEffect, useState} from "react";
import {CardElement, useStripe, useElements} from "@stripe/react-stripe-js";
import axios from 'axios'
import {useDispatch} from "react-redux";
import {getOrdersForClientMainPage, payment, paymentNow, paymentSuccess} from "../redux/actions/OrderAction";
import {pay} from "../api/OrderApi";

function CardForm(data) {
    const [succeeded, setSucceeded] = useState(false);
    const [error, setError] = useState(null);
    const [processing, setProcessing] = useState(false);
    const [disabled, setDisabled] = useState(true);
    const [stopEffect, setStopEffect] = useState(false);
    const stripe = useStripe();
    const elements = useElements();
    const dispatch = useDispatch()


    useEffect(() => {
        // axios.get("http://localhost/api/payment/getChargeOnTime?id="+id).then(resp => {
        //     setClientSecret(resp.data)
        //     // setStopEffect(true)
        // })
        // dispatch(paymentNow({id:id}))
    });

    const handleChange = async (event) => {

        setDisabled(event.empty);
        setError(event.error ? event.error.message : "");
    };

    const handleSubmit = async (ev) => {
        ev.preventDefault();
        setProcessing(true);

        const payload = await stripe.confirmCardPayment(data.data.clientSecret, {
            payment_method: {
                card: elements.getElement(CardElement),
                billing_details: {
                    name: ev.target.name.value
                }
            }
        })
        if (payload.error) {
            setError(`Payment failed ${payload.error.message}`);
            setProcessing(false);
        } else {
            dispatch(paymentSuccess({id: data.data.id, checkId: payload.paymentIntent.id}))
            setError(null);
            setProcessing(false);
            setSucceeded(true);
            dispatch(getOrdersForClientMainPage({page: 0, size: 5}))
        }


    };
    return (
        <form
            id="payment-form"
            onSubmit={handleSubmit}>
            <CardElement id="card-element"
                         className="card-style"
                         onChange={handleChange}
                         options={{hidePostalCode: true}}
            />
            <button
                disabled={processing || disabled || succeeded}
                id="submit"
                className="payment-btn"
            >
        <span id="button-text">
          {processing ? (
              <div className="spinner" id="spinner"/>
          ) : (
              "Pay"
          )}
        </span>
            </button>
            {/* Show any error that happens when processing the payment */}
            {error && (
                <div className="card-error" role="alert">
                    {error}
                </div>
            )}
            {/* Show a success message upon completion */}
            <p className={succeeded ? "result-message" : "result-message hidden"}>
                Payment succeeded, see the result in your
                <a
                    href={`https://dashboard.stripe.com/test/payments`}
                >
                    {" "}
                    Stripe dashboard.
                </a> Refresh the page to pay again.
            </p>
        </form>
    );
}

export default CardForm