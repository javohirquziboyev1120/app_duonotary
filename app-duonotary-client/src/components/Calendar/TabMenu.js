import React, {useEffect, useState} from "react";

export default function TabMenu(props) {

    const [tab, setTab] = useState(0);


    return <div className="tab-menu d-flex">
        {props.tabs?props.tabs.map((item,index)=>
        <div key={index} onClick={()=>{setTab((prev)=>{
            props.getTabIndex(index);
            return index;
        })}} className={tab===index?"active":""}>
            {item}
        </div>
        ):""}
    </div>
}