import React from "react";
import {CalendarIcon} from "../Icons";

export default function FirstBlock(props) {

    const name = ["Registered Users", "Online orders", "In-person orders", "Mobile app orders"];



    return <div className="block d-flex flex-column justify-content-between">
        <div className="d-flex justify-content-between align-items-center">
            <span className="main-font">
                {props.name}
            </span>
            <CalendarIcon />
        </div>
        <div className="d-flex align-items-end mt-5">
            <span className="big-numbers mr-2" style={{color: props.color}}>{props.total}</span>
            <div className="d-flex flex-column">
                <span style={{color: props.color}}>+{props.today}</span>
                <span className="this-day">this day</span>
            </div>
        </div>
    </div>
}