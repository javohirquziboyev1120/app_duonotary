import React from "react";

export  default function Indicator(props) {

    return <div className={props.small?"indicator indicator-small":"indicator"} style={{borderLeft: "5px solid "+props.color, backgroundColor: props.color+"10"}} onClick={props.onClick}>
        {props.name}
    </div>
}