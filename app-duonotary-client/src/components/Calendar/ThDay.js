import React from "react";

export default function ThDay(props) {

    const days = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];

    return <div key={props.id} className="flex-column justify-content-center align-items-center th-day">
        <span className={props.status==="today"?"today day":props.status==="future"?"day":" day"}>{days[props.day]}</span>
        <span className={props.status==="today"?"today date":props.status==="future"?"future date":"date"}>{props.date}</span>
    </div>
}