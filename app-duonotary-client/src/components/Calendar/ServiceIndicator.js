import React from "react";
import Indicator from "./Indicator";

export default function ServiceIndicator(props) {
    return <div className="block">
        <span className="main-font">Service indicator</span>
        <div className="mt-2 d-flex flex-wrap justify-content-between">
            {props.services?props.services.map((item,index)=>
                <div key={index} className="mr-2 mb-2">
                    <Indicator name={item.name} color={props.colors[item.id]} />
                </div>
            ): ""}

        </div>
    </div>
}