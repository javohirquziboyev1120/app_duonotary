import React, {useState} from "react";
import {Collapse} from "reactstrap";
import OutsideClickHandler from "react-outside-click-handler/esm/OutsideClickHandler";

export default function ServicesBox(props) {

    const column=props.day+2+"/"+(props.day+3);
    const row=props.time+2+"/"+(props.time+3);
    const [hidden, setHidden] = useState(true);
    const overflow = React.Children.count(props.children)>2;
    const first = props.children.slice(0, 2);
    const second = props.children.slice(2);


    return <div  style={{gridColumn: column, gridRow: row}} className="" >
        {props.children?
            <OutsideClickHandler onOutsideClick={()=>!hidden?setHidden(true):""}>
                <div className={hidden?"services-box flex-column align-items-center pt-2 ":"services-box flex-column align-items-center pt-2 services-box-expanded"} >
                    {first}
                    <Collapse isOpen={!hidden}>
                        {second}
                    </Collapse>
                    {
                        overflow?<span onClick={()=>{setHidden(!hidden)}} className="see-more">{hidden?"SEE "+(props.children.length-2)+" MORE":"CLOSE"}</span>:""
                    }
                </div>
            </OutsideClickHandler>
        :<div className="services-box"/>
        }
    </div>
    // return <div style={{gridColumn: column, gridRow: row}} className="services-box flex-column align-items-center pt-2 ">
    //     {props.children}
    // </div>
}