import React, {useEffect, useRef, useState} from "react";
import Toolbar from "./Toolbar";
import ThDay from "./ThDay";
import TrTime from "./TrTime";
import ServicesBox from "./ServicesBox";
import Indicator from "./Indicator";
import InfoCard from "./InfoCard";
import OutsideClickHandler from "react-outside-click-handler/esm/OutsideClickHandler";
import {connect} from "react-redux";
import moment from "moment";

function Dashboard(props) {

    //States
    const [infoCard, setInfo] = useState({
        position: "",
        orderInfo:{},
        color: "",
    })
    const [infoHidden, setInfoHidden] = useState(true);
    let divDashboard = useRef(null);
    const Scroll=()=>{
        console.log("Working")
    }
    useEffect(()=>{
        window.addEventListener("scroll",Scroll)
        divDashboard.current.addEventListener("scroll", Scroll)
    }, [])
    const showInfo = (e, info) => {

        e.persist();
        setInfoHidden(false);
        const position = e;
        setInfo({
            position: position,
            orderInfo: info,
            color: props.colors[info.subServiceId],
        });
    }
    //States end
    const services = props.services;
    const current = props.today;
    const today = new Date();
    let date = [];
    const time = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,];

    for (let i = 0; i < 7; i++) {

        date.push(new Date(new Date(current).setDate(current.getDate()+i)));

    }




    const placeIndicators = () =>{
        let Grid =[];

        for(let i=0; i<7;i++){
            let days=[];
            for(let j=0; j<24; j++) {
                let time=[];
                days.push(time);
            }
            Grid.push(days);
        }
        services.map((item, index) => {
            const time = new Date(item.fromTime).getHours();
            const day = new Date(item.fromTime).getDate()-current.getDate();
            if(day>=0&&day<7){
                Grid[day][time].push(item);
            } else{
            }
        })

        return Grid;
    }


    return <div>
        {
            !infoHidden ? <div className="no-scroll">
                <OutsideClickHandler onOutsideClick={() => setInfoHidden(true)}><InfoCard
                    info={infoCard}/></OutsideClickHandler>
            </div> : ""
        }
        <div  ref={divDashboard} className="dashboard-grid">
            <div/>
            {
                date.map((item, index) =>
                    <ThDay key={index} id={index} day={item.getDay()} date={item.getDate()}
                           status={ item.toLocaleDateString() === today.toLocaleDateString() ? "today" : moment(item).isAfter(today) ? "future" : ""}/>
                )
            }
            {
                time.map(item =>
                    <TrTime key={item} time={item}/>
                )
            }

            {
                placeIndicators().map((day, indexDay) =>
                    day?day.map((time, indexTime) =>

                            <ServicesBox key={indexTime} time={indexTime} day={indexDay}>
                                {
                                    time.length!==0?time.map((item,index)=>
                                    <Indicator key={index} name={props.toolbarTab===0 ? item.subServiceName: item.agentFullName} small={!props.toolbarTab===0} onClick={(e)=>showInfo(e,item)} color={props.colors[item.subServiceId]} />
                                    ):""
                                }
                            </ServicesBox>

                    ):""
                )
            }



        </div>
    </div>
}
export default Dashboard;
