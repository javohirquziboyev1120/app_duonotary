import React, {useEffect, useState} from "react";
import Select from "react-select";

export default function ZipCodeFilter(props) {

    const [state, setState] = useState("");
    const [county, setCounty] = useState("");
    const [zipCode, setZipCode] = useState("");

    const mapOptions=(arr)=>{
        let result=[];
        if(arr){
            arr.map(item=>{
                result.push({
                    value: item.id, label: item.name
                })
            })
            return result;
        }
        else {
            return ""
        }
    }

    return <div className="same-height block flex-column justify-content-between">
        <div className="d-flex align-items-center justify-content-between">
            <span className="main-font">
            Zip code filter
        </span>
            <span className="today-btn" onClick={()=>{props.filter(state.value, county.value, zipCode.value)}}>Apply</span>
        </div>
        <div className="flex-column">
            <Select options={mapOptions(props.stateOptions)} value={state} onChange={(v)=>{setState(v); setCounty(""); setZipCode(""); props.getCounty(v.value)}} placeholder="State" />
            <div className="d-flex mt-2">
                <Select className="w-50 mr-2" options={state?props.countyOptions:undefined} value={county} onChange={(v)=>{setCounty(v); setZipCode(""); props.getZipCode(v.value)}} placeholder="County" />
                <Select className="w-50" options={county?props.zipCodeOptions:undefined} value={zipCode} onChange={(v)=>{setZipCode(v);}} placeholder="Zip code" />
            </div>
        </div>
    </div>
}