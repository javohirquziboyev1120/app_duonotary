import React, {useState} from "react";
import TabMenu from "./TabMenu";
import {DownloadIcon, RefreshIcon} from "../Icons";

export default function Toolbar(props) {
    const tabs=["Service", "Agents"];
    const [currentTab, setCurrentTab]=useState(0);
    const month=["January", "February", "March", "Aprel", "May", "June", "July", "August", "September", "October", "November", "December",];

    return <div className="d-flex align-items-center justify-content-between p-4">
        <div className="d-flex align-items-center">
            <span className="main-font mr-3 month-name">{month[props.today.getMonth()]}, {props.today.getFullYear()}</span>
            <span onClick={props.previousWeek} className="icon-background  rotate-180 mr-2">
                <span className="icon icon-right"/>
            </span>
            <span onClick={props.nextWeek} className="icon-background mr-3">
                <span className="icon icon-right"/>
            </span>
            <span onClick={props.currentDay} className="today-btn">Today</span>
        </div>
        <div className="d-flex align-items-center">
            <TabMenu tabs={tabs} getTabIndex={props.toggleTab} />
            <DownloadIcon className="ml-3" />
            <RefreshIcon className="ml-2"/>
        </div>
    </div>
}