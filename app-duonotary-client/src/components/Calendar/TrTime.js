import React from "react";

export default function TrTime(props) {

    return <div key={props.id} className="flex-column align-items-center justify-content-center tr-time">
        <span className="main-font">
            {props.time<=12?props.time:props.time-12}:00
        </span>
        <span className="this-day mt-1">{props.time<=12?"am":"pm"}</span>
    </div>
}