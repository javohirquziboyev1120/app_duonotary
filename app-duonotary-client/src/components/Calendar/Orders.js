import React, {useState} from "react";
import TabMenu from "./TabMenu";

export default function Orders(props) {

    const tabs = ["TODAY", "WEEK", "MONTH"];
    const [currentTab, setCurrentTab] = useState(0);

    return <div className="same-height block flex-column justify-content-between">
        <div className="d-flex justify-content-between">
            <span className="main-font">Orders</span>
            <TabMenu tabs={tabs} getTabIndex={setCurrentTab}/>
        </div>
        <div className="d-flex align-self-end align-items-end">
            <span className="big-numbers mr-2">${
                currentTab === 0 ? parseFloat(props.today).toFixed(2) :
                    currentTab === 1 ? parseFloat(props.week).toFixed(2) :
                        parseFloat(props.month).toFixed(2)
            }</span>
            <div className="flex-column">
                <span className="green-color">+${parseFloat(props.today).toFixed(2)}</span>
                <span className="this-day">this day</span>
            </div>
        </div>
    </div>
}