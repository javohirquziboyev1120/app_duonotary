import React, { useEffect, useState } from "react";
import { getAmPm } from "../../pages/clientPage/InPersonOrder";
import { Col } from "reactstrap";

export default function InfoCard(props) {
    const { orderInfo, position, color, } = props.info;
    const from = new Date(orderInfo.fromTime);
    const till = new Date(orderInfo.tillTime);

    let x = position.clientX;
    let y = position.clientY;

    if (window.innerWidth - 235 < x) {
        x = x - 235;
    }
    if (window.innerHeight - 265 < y) {
        y = y - 265;
    }
    return <div key={x} className="block info-card p-3 d-flex" style={{ top: y, left: x }}>
        <span className="circle m-0 mt-1 mr-2" style={{ backgroundColor: color }} />
        <div className="flex-column justify-content-between">
            <span className="name">{orderInfo.clientFullName}</span>
            <div className="flex-column">
                <span className="this-day">Phone</span>
                <span className=" phone mt-1">{orderInfo.clientPhoneNumber}</span>
            </div>
            <div className="flex-column">
                <span className="this-day">Agent</span>
                <span className="name mt-1">{orderInfo.agentFullName}</span>
            </div>
            <div className="d-flex justify-content-between">
                <div className="flex-column">
                    <span className="this-day">From</span>
                    <span className="phone">{from.toLocaleTimeString()}</span>
                </div>
                <div className="flex-column">
                    <span className="this-day">Till</span>
                    <span className="phone">{till.toLocaleTimeString()}</span>
                </div>
            </div>
        </div>
    </div>
}