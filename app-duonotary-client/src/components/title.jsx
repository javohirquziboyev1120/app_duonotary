import React, {useEffect, useState} from "react";
import { Text } from "./common/text";
export const Title = ({ title1, title2, colorReverse = false }) => {
    const [size, setSize] = useState(0);

    window.addEventListener('resize', () => {
        setSize(window.innerWidth);
    })
    useEffect(() => {
        setSize(window.innerWidth);
    },[])

  const color1 = "#284049",
    color2 = "#d9dbde";
  return (
    <>
      <Text size={size < 556 ? 6 : 4} color={colorReverse ? color2 : color1}>
        {title1}
      </Text>
      <Text size={size < 556 ? 6 : 4} color={colorReverse ? color1 : color2}>
        {title2}
      </Text>
    </>
  );
};
