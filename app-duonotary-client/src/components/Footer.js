import React from "react";
import './Footer.scss';
import {Link} from "react-router-dom";
import QRApp from "./common/QRApp";
import Dates from "./common/Dates";

function Footer() {
    return (
        <div className='foot'>
            <div className='collBack'>
                <div>
                    <Dates/>
                </div>
                <div>
                    <QRApp/>
                </div>
            </div>
            <div className="footer">
                <div className="w-25 align-self-center ">
                        <img src="/assets/img/logo.png" alt="logo" className="logo logo_1"/>
                </div>
                <div className="links-section justify-content-between w-100 pr-5 mr-5">
                    <div className="flex-column">
                        <Link to={''} className="links">Home</Link>
                        <Link to={'/about'} className="links">About Us</Link>
                        <Link to={'/registerAgent'} className="links">Agent registration</Link>
                        <Link to={'/blog'} className="links">Our Blog</Link>
                        <Link to={'/inPerson'} className="links">Mobile</Link>
                        <Link to={'/online'} className="links">Online</Link>
                        <Link to={'/inPerson'} className="links">Apostille</Link>
                    </div>
                    <div className="flex-column">
                        <Link to={''} className="links">Real Estate</Link>
                        <Link to={'/inPerson'} className="links">Embassy Legalization</Link>
                        <Link to={''} className="links">Refinancing</Link>
                        <Link to={''} className="links">Reverse Mortgage</Link>
                        <Link to={''} className="links">I-9 Verification</Link>
                        <Link to={' /notificationsettings'} className="links">Agreements</Link>
                        <Link to={''} className="links">Estate Planning</Link>
                    </div>
                    <div className="flex-column">
                        <Link to={''} className="links">Business Contracts</Link>
                        <Link to={''} className="links">Common Documents</Link>
                        <Link to={''} className="links">Contacts</Link>
                        <Link to={'/termsofservices'} className="links">Terms & Conditions</Link>
                        <Link to={'/privacypolicy'} className="links">Privacy Policy</Link>
                    </div>
                </div>
            </div>
            <div className='data'>
                <div className="icons mr-4">
                    <div><i className="fab fa-youtube"/></div>
                    <div><i className="fab fa-twitter"/></div>
                    <div><i className="fab fa-instagram"/></div>
                    <div><i className="fab fa-facebook"/></div>
                </div>
                <div className="footer-sub-text ml-4">
                    We at Notarize pride ourselves on providing helpful
                    resources to help demystify notarization.
                    We are not lawyers, and don’t give legal advice,
                    so always check with your own attorneys, advisors,
                    or document recipients if you have unanswered questions
                    about notarization or digitally notarized documents.
                </div>
            </div>
        </div>
    );
}

export default Footer;
