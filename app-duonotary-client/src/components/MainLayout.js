import React, { Component } from "react";
import "./mainLayout.scss";
import { Link } from "react-router-dom";
import {Button, Col, Container, Input} from "reactstrap";
import Row from "reactstrap/es/Row";
import { connect } from "react-redux";
import {getCustomersBySearch, getSharingDiscountList} from "../redux/actions/AppAction";
import { config } from "../utils/config";
import { logout } from "../redux/actions/AuthActions";
import {MenuMobile} from "./Icons";
import OutsideClickHandler from "react-outside-click-handler";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {getAgents, getAgentsByFilter, getAgentsBySearch} from "../redux/actions/AgentAction";
import {getOrderList, getOrdersByFilter, getOrdersBySearch,getOrdersBySearchForClient} from "../redux/actions/OrderAction";

class MainLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showMobileMenu: false,
            menuHidden: false
        };
    }
    toggleMenu=()=>{
        this.setState({
            menuHidden: !this.state.menuHidden
        })
    }
    //Only mobile
    toggleShowMobileMenu = () => {
        this.setState({
            showMobileMenu: !this.state.showMobileMenu,
        });
    };

    render() {
        const { page, size, historTime, dispatch, currentUser,isFilter,search } = this.props;

        const ShowHistory = () => {
            if (!historTime) {
                dispatch(getSharingDiscountList({ page, size }));
            }
            dispatch({
                type: "updateState",
                payload: {
                    historTime: !historTime,
                },
            });
        };
        const logOut = () => {
            dispatch(logout());
        };
        let pressEnter = (item) => {
            if (item.charCode === 13) {
                if (this.props.pathname === "/client/order")
                    dispatch(getOrdersBySearchForClient({search: ""+item.target.value}));
            }
        }
        let searchByName = (item) => {
            this.setState({search: item.target.value})
            dispatch({
                type: 'updateState',
                payload: {
                    search: item.target.value
                }

            });
        }
        let ShowFilter = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    isFilter: !isFilter
                }
            })
        };

        let filter = (e, v) => {
        if (this.props.pathname === "/client/order") {
         if (v.zipCode === "" && v.date === "" && v.status === "") dispatch(getOrderList({page: 0, size: this.props.size}));
         else if (v.date !== "" && v.status === "" && v.zipCode === "") dispatch(getOrdersByFilter({date: v.date.replaceAll("-", "/")}));
                else if (v.zipCode !== "" && v.status === "" && v.date === "") dispatch(getOrdersByFilter({zipCode: v.zipCode}));
                else if (v.status !== "" && v.date === "" && v.zipCode === "") dispatch(getOrdersByFilter({status: v.status}));
               else if (v.status !== "" && v.zipCode !== "" && v.date === "") dispatch(getOrdersByFilter({
                    status: v.status,
                    zipCode: v.zipCode
                }));
                else if (v.status !== "" && v.date !== "" && v.zipCode === "") dispatch(getOrdersByFilter({
                    status: v.status,
                    date: v.date.replaceAll("-", "/")
                }));
                else if (v.zipCode !== "" && v.date !== "" && v.status === "") dispatch(getOrdersByFilter({
                    zipCode: v.zipCode,
                    date: v.date.replaceAll("-", "/")
                }));
                else dispatch(getOrdersByFilter({
                        date: v.date.replaceAll("-", "/"),
                        zipCode: v.zipCode,
                        status: v.status
                    }));
                dispatch({type: 'updateState', payload: {currentOrder: ''}})
                ShowFilter()
            }
        }

        return currentUser ? (
            <div className="main-layout">
                <div
                    className={
                        this.state.showMobileMenu
                            ? "main-layout-left left-mobile"
                            : this.state.menuHidden?"main-layout-left main-layout-left-hidden":"main-layout-left"
                    }
                >
                    <div className="d-flex align-items-center justify-content-center">
                        <Link to="/">
                            <div className="logo-duo">
                                <img src="/assets/img/logo.png" alt="logo-png" />
                            </div>
                        </Link>

                    </div>

                    <div className="main-link-div">
                        <div
                            className={
                                this.props.pathname === "/client/main-page"
                                    ? "active-link"
                                    : "default-link"
                            }
                        >
                            <span className="icon icon-main" />
                            <Link to="/client/main-page" className="main-link">
                                Main page
                            </Link>
                        </div>

                        <div
                            className={
                                this.props.pathname === "/client/order"
                                    ? "active-link"
                                    : "default-link"
                            }
                        >
                            <span className="icon icon-order" />
                            <Link to="/client/order" className="main-link">
                                Order
                            </Link>
                        </div>
                        <div
                            className={
                                this.props.pathname === "/client/feedback"
                                    ? "active-link"
                                    : "default-link"
                            }
                        >
                            <span className="icon icon-feedback" />
                            <Link to="/client/feedback" className="main-link">
                                Feedback
                            </Link>
                        </div>
                        <div
                            className={
                                this.props.pathname === "/client/sharing"
                                    ? "active-link"
                                    : "default-link"
                            }
                        >
                            <span className="icon icon-sharing" />
                            <Link to="/client/sharing" className="main-link">
                                Sharing
                            </Link>
                        </div>
                        <div
                            className={
                                this.props.pathname === "/client/online"
                                    ? "active-link"
                                    : "default-link"
                            }
                        >
                            <span className="icon icon-online" />
                            <Link to="/client/online" className="main-link">
                                Online order
                            </Link>
                        </div>
                        <div
                            className={
                                this.props.pathname === "/client/inPerson"
                                    ? "active-link"
                                    : "default-link"
                            }
                        >
                            <span className="icon icon-offline" />
                            <Link to="/client/inPerson" className="main-link">
                                In-Person order
                            </Link>
                        </div>
                        <div
                            className={
                                this.props.pathname === "/client/setting"
                                    ? "active-link"
                                    : "default-link"
                            }
                        >
                            <span className="icon icon-setting" />
                            <Link to="/client/setting" className="main-link">
                                Setting
                            </Link>
                        </div>

                        <div className="about-user">
                            <div className="avatar">
                                <img
                                    src={
                                        currentUser.photoId
                                            ? config.BASE_URL +
                                              "/attachment/" +
                                              currentUser.photoId
                                            : "/assets/img/avatar.png"
                                    }
                                    alt=""
                                />
                            </div>
                            <div className="name-title">
                                <div className="name">
                                    {currentUser.lastName +
                                        " " +
                                        currentUser.firstName}
                                </div>
                                <div className="title">Client</div>
                            </div>
                        </div>
                        <div className="close-menu mt-1 mx-auto" onClick={this.toggleShowMobileMenu}>
                            <span className="icon icon-plus"/>
                        </div>
                    </div>
                </div>
                <div className={"main-layout-right"}>
                    <div className={this.state.menuHidden ? "menu-toggle l-0" : "menu-toggle"} onClick={this.toggleMenu}>
                        {
                            this.state.menuHidden?
                                <MenuMobile />
                                :
                                <span className="icon icon-plus" />
                        }
                    </div>
                    <Container className="client-navbar">
                        <Row  className="row-client-navbar d-flex align-items-center justify-content-between">
                            <Col
                                sm={1}
                                onClick={this.toggleShowMobileMenu}
                                className={"icon-menu-mobile"}
                            >
                                <MenuMobile />
                            </Col>
                            <Col md={3} sm={3} className="page-name-mobile">
                                {this.props.pathname === "/client/main-page" ? (
                                    <div className="page-name">Main page</div>
                                ) : (
                                    ""
                                )}
                                {this.props.pathname === "/client/order" ? (
                                    <div className="page-name">Order</div>
                                ) : (
                                    ""
                                )}
                                {this.props.pathname === "/client/sharing" ? (
                                    <div className="page-name">Sharing</div>
                                ) : (
                                    ""
                                )}
                                {this.props.pathname === "/client/setting" ? (
                                    <div className="page-name">Setting</div>
                                ) : (
                                    ""
                                )}

                                {this.props.pathname === "/client/online" ? (
                                    <div className="page-name">Online</div>
                                ) : (
                                    ""
                                )}
                                {this.props.pathname === "/client/inPerson" ? (
                                    <div className="page-name">In-Person</div>
                                ) : (
                                    ""
                                )}

                                {this.props.pathname === "/client/feedback" ? (
                                    <div className="page-name">Feedback</div>
                                ) : (
                                    ""
                                )}
                            </Col>

                            <Col md={4}>
                                {this.props.pathname === "/client/order" ?
                                    <div className={'search_block_admin'}>
                                        <Input
                                            onKeyPress={(item) => pressEnter(item)}
                                            onChange={(item) => searchByName(item)}
                                            className="search-box search-box-admin" id="searchInput"
                                            defaultValue={search} placeholder="Search ...."/>
                                        {this.props.pathname === "/client/order"?
                                            <img onClick={ShowFilter} className="search-icons search-icons_admin"
                                                 src="/assets/icons/search.png" alt="ccc"/> : ""}
                                        {/*{isFilter ?*/}
                                        {/*    <OutsideClickHandler onOutsideClick={ShowFilter}>*/}
                                        {/*        <div className="openFilter">*/}
                                        {/*            {this.props.pathname === "/client/order" ?*/}
                                        {/*                <AvForm onValidSubmit={filter}>*/}
                                        {/*                    <AvField name="zipCode" label="By zipcode"*/}
                                        {/*                             placeholder="Enter zipcode"*/}
                                        {/*                             type="number"/>*/}
                                        {/*                    <AvField name="status" label="By order status"*/}
                                        {/*                             placeholder="Enter status"/>*/}
                                        {/*                    <AvField name="date" label="By date"*/}
                                        {/*                             placeholder="Select date" type="date"/>*/}
                                        {/*                    <Button color="primary"*/}
                                        {/*                            className="btn-block">Filter</Button>*/}
                                        {/*                </AvForm> :*/}
                                        {/*                <AvForm onValidSubmit={filter}>*/}
                                        {/*                    <AvField name="zipCode" label="By zipcode"*/}
                                        {/*                             placeholder="Enter zipcode"*/}
                                        {/*                             type="number"/>*/}
                                        {/*                    <AvField name="date" label="By date"*/}
                                        {/*                             placeholder="Select date" type="date"/>*/}
                                        {/*                    <Button color="primary"*/}
                                        {/*                            className="btn-block">Filter</Button>*/}
                                        {/*                </AvForm>}*/}
                                        {/*        </div>*/}
                                        {/*    </OutsideClickHandler>*/}

                                        {/*    : ""}*/}
                                    </div>
                                    : ""
                                }
                            </Col>
                            <Col md={2} sm={2}
                                className="notification-mobile"
                            >
                                <div className="notification-exit">
                                    <div className="exit ml-3">
                                        <ExitIcon onClick={logOut} />
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                    {this.props.children}
                </div>
            </div>
        ) : (
            <div />
        );
    }
}

export default connect(
    ({ auth: { currentUser, isUser }, app: { historTime, page, size,isFilter,search } }) => ({
        historTime,
        currentUser,
        isUser,
        page,
        size,
        isFilter,search
    })
)(MainLayout);


function NotificationIcon(props) {
    return (
        <div className={"notification-icon"} onClick={props.onClick}>
            <svg
                width="50"
                height="50"
                viewBox="0 0 50 50"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <circle cx="25" cy="25" r="25" fill="white" />
                <path
                    d="M31 23C31 21.4087 30.3679 19.8826 29.2426 18.7574C28.1174 17.6321 26.5913 17 25 17C23.4087 17 21.8826 17.6321 20.7574 18.7574C19.6321 19.8826 19 21.4087 19 23V31H31V23ZM33 31.667L33.4 32.2C33.4557 32.2743 33.4896 32.3626 33.498 32.4551C33.5063 32.5476 33.4887 32.6406 33.4472 32.7236C33.4057 32.8067 33.3419 32.8765 33.2629 32.9253C33.1839 32.9741 33.0929 33 33 33H17C16.9071 33 16.8161 32.9741 16.7371 32.9253C16.6581 32.8765 16.5943 32.8067 16.5528 32.7236C16.5113 32.6406 16.4937 32.5476 16.502 32.4551C16.5104 32.3626 16.5443 32.2743 16.6 32.2L17 31.667V23C17 20.8783 17.8429 18.8434 19.3431 17.3431C20.8434 15.8429 22.8783 15 25 15C27.1217 15 29.1566 15.8429 30.6569 17.3431C32.1571 18.8434 33 20.8783 33 23V31.667ZM22.5 34H27.5C27.5 34.663 27.2366 35.2989 26.7678 35.7678C26.2989 36.2366 25.663 36.5 25 36.5C24.337 36.5 23.7011 36.2366 23.2322 35.7678C22.7634 35.2989 22.5 34.663 22.5 34Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}

function ExitIcon(props) {
    return (
        <div className={"notification-icon"} onClick={props.onClick}>
            <svg
                width="50"
                height="50"
                viewBox="0 0 50 50"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <circle cx="25" cy="25" r="25" fill="white" />
                <path
                    d="M25 35C19.477 35 15 30.523 15 25C15 19.477 19.477 15 25 15C26.5527 14.9988 28.0842 15.3598 29.4729 16.0541C30.8617 16.7485 32.0693 17.7572 33 19H30.29C29.1352 17.9818 27.7112 17.3184 26.1887 17.0894C24.6663 16.8604 23.1101 17.0757 21.7069 17.7092C20.3037 18.3428 19.1131 19.3678 18.2781 20.6612C17.443 21.9546 16.9989 23.4615 16.999 25.0011C16.9991 26.5407 17.4435 28.0475 18.2788 29.3408C19.1141 30.6341 20.3048 31.6589 21.7081 32.2922C23.1114 32.9255 24.6676 33.1405 26.19 32.9113C27.7125 32.6821 29.1364 32.0184 30.291 31H33.001C32.0702 32.243 30.8624 33.2517 29.4735 33.9461C28.0846 34.6405 26.5528 35.0013 25 35ZM32 29V26H24V24H32V21L37 25L32 29Z"
                    fill="#313E47"
                />
            </svg>
        </div>
    );
}
