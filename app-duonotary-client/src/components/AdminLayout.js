import React, {Component} from 'react';
import {Link} from "react-router-dom";
import './adminLayout.scss'
import '../pages/adminPages/adminOrderPages.scss'
import {Button, Col, Collapse, Container, Form, FormGroup, Input, Label, Row} from "reactstrap";
import {connect} from "react-redux";
import {logout, userMe} from "../redux/actions/AuthActions";
import {config} from "../utils/config";
import Loader from "./Loader";
import {downloadFile, downloadfile, downloadFileAction} from "../redux/actions/AttachmentAction";
import DownloadRequestModal from "./Modal/DownloadRequestModal";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {getAgents, getAgentsByFilter, getAgentsBySearch} from "../redux/actions/AgentAction";
import OutsideClickHandler from 'react-outside-click-handler';
import {getCustomersBySearch} from "../redux/actions/AppAction";
import {MenuMobile} from "./Icons";
import {getOrderList, getOrdersByFilter, getOrdersBySearch, getOrdersForAdminOrder} from "../redux/actions/OrderAction";

class AdminLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: "",
            showModal: false,
            path: '',
            search: '',
            menuHidden: false,
            loader: false,
        };
    }

    toggleMenu = () => {
        this.setState({
            menuHidden: !this.state.menuHidden
        })
    }
    openModal = (text, path) => {
        this.setState({text: text, path: path, showModal: !this.state.showModal})
    }

    render() {
        const {currentUser, isFilter, isOpenGeneral, isAdmin, isOpenUser, isOpenPages, isSuperAdmin, isOpen, dispatch, openCol, loading, filters, search} = this.props;
        let logOut = () => {
            dispatch(logout())
        };

        let ShowServices = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    isOpen: !isOpen
                }
            })
        };
        let ShowUsers = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    isOpenUser: !isOpenUser
                }
            })
        };
        let showPages = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    isOpenPages: !isOpenPages
                }
            })
        };
        let showGeneral = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    isOpenGeneral: !isOpenGeneral
                }
            })
        };
        let ShowFilter = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    isFilter: !isFilter
                }
            })
        };

        let showTime = () => {
            dispatch({
                type: 'updateState',
                payload: {
                    openCol: !openCol
                }
            })
        };
        let downloadFile = (item) => {
            dispatch(downloadFileAction(item));
            this.openModal()
        }
        let filter = (e, v) => {
            if (this.props.pathname === "/admin/agent") {
                if (v.zipCode === "" && v.date === "") dispatch(getAgents());
                else if (v.zipCode == "") dispatch(getAgentsByFilter({date: v.date.replaceAll("-", "/")}));
                else if (v.date == "") dispatch(getAgentsByFilter({zipCode: v.zipCode}));
                else dispatch(getAgentsByFilter({date: v.date.replaceAll("-", "/"), zipCode: v.zipCode}));
                dispatch({type: 'updateState', payload: {currentAgent: ''}})
                ShowFilter()
            }
            else if (this.props.pathname === "/admin/order") {
                if (v.zipCode === "" && v.date === "" && v.status === "") dispatch(getOrderList({page: 0, size: this.props.size}));
                else if (v.date !== "" && v.status === "" && v.zipCode === "") dispatch(getOrdersByFilter({date: v.date.replaceAll("-", "/")}));
                else if (v.zipCode !== "" && v.status === "" && v.date === "") dispatch(getOrdersByFilter({zipCode: v.zipCode}));
                else if (v.status !== "" && v.date === "" && v.zipCode === "") dispatch(getOrdersByFilter({status: v.status}));
                else if (v.status !== "" && v.zipCode !== "" && v.date === "") dispatch(getOrdersByFilter({
                    status: v.status,
                    zipCode: v.zipCode
                }));
                else if (v.status !== "" && v.date !== "" && v.zipCode === "") dispatch(getOrdersByFilter({
                    status: v.status,
                    date: v.date.replaceAll("-", "/")
                }));
                else if (v.zipCode !== "" && v.date !== "" && v.status === "") dispatch(getOrdersByFilter({
                    zipCode: v.zipCode,
                    date: v.date.replaceAll("-", "/")
                }));
                else dispatch(getOrdersByFilter({
                        date: v.date.replaceAll("-", "/"),
                        zipCode: v.zipCode,
                        status: v.status
                    }));
                dispatch({type: 'updateState', payload: {currentOrder: ''}})
                ShowFilter()
            }
        }

        let pressEnter = (item) => {
            if (item.charCode === 13) {
                if (this.props.pathname === "/admin/agent") dispatch(getAgentsBySearch({search: item.target.value}));
                if (this.props.pathname === "/admin/customer") dispatch(getCustomersBySearch({search: item.target.value}));
                if (this.props.pathname === "/admin/order") dispatch(getOrdersBySearch({search: ""+item.target.value}));
            }
        }
        let searchByName = (item) => {
            this.setState({search: item.target.value})
            dispatch({
                type: 'updateState',
                payload: {
                    search: item.target.value
                }

            });
        }
        return (

            (loading || this.state.loader) ?
                <Loader/>
                :
                <div className="adminLayout-page">
                    {currentUser && isAdmin ?
                        <div className="main-layout">
                            <div
                                className={this.state.menuHidden ? "main-layout-left main-layout-left-hidden" : "main-layout-left"}>
                                <Link to="/">
                                    <div className="logo-duo">
                                        <img src="/assets/img/logo.png" alt=""/>
                                    </div>
                                </Link>

                                <div className="main-link-div">
                                    <div className={
                                        this.props.pathname === "/admin/calendar" ?
                                            "active-link" : "default-link"
                                    }>
                                        <span className="icon icon-main"/>
                                        <Link to="/admin/calendar"
                                              className="main-link">
                                            Calendar
                                        </Link>
                                    </div>
                                    <div className={
                                        this.props.pathname === "/admin" ?
                                            "active-link" : "default-link"
                                    }>
                                        <span className="icon icon-main"/>
                                        <Link to="/admin"
                                              className="main-link">
                                            Main
                                        </Link>
                                    </div>
                                    <div className={
                                        this.props.pathname === "/admin/order" ?
                                            "active-link" : "default-link"
                                    }>
                                        <span className="icon icon-order"/>
                                        <Link to="/admin/order"
                                              className="main-link">
                                            Order
                                        </Link>
                                    </div>
                                    <div onClick={ShowUsers}
                                         className="default-link">
                                        <span className="icon icon-customer"
                                              style={isOpenUser ? {backgroundColor: '#04A6FB'} : {}}/>
                                        <Link to="#" className={isOpenUser ? "main-link text-duo" : "main-link"}>
                                            Users
                                        </Link>
                                        {!isOpenUser ?
                                            <span style={{marginLeft: "auto", transform: "rotate(90deg)"}}
                                                  className="fas fa-angle-up"/> :
                                            <span style={{
                                                marginLeft: "auto",
                                                transform: "rotate(180deg)",
                                                color: '#04A6FB'
                                            }}
                                                  className="fas fa-angle-up"/>}
                                    </div>
                                    <Collapse className="pl-3 collapses" isOpen={isOpenUser}>
                                        <div className={
                                            this.props.pathname === "/admin/customer" ?
                                                "active-link" : "default-link"
                                        }>
                                            <Link to="/admin/customer"
                                                  className="main-link">
                                                Clients
                                            </Link>
                                        </div>
                                        <div className={
                                            this.props.pathname === "/admin/agent" ?
                                                "active-link" : "default-link"
                                        }>
                                            <Link to="/admin/agent"
                                                  className="main-link">
                                                Agents
                                            </Link>
                                        </div>
                                        {isSuperAdmin ?
                                            <div className={
                                                this.props.pathname === "/admin/admins" ?
                                                    "active-link" : "default-link"
                                            }>
                                                <Link to="/admin/admins"
                                                      className="main-link">
                                                    Admins
                                                </Link>
                                            </div>
                                            : ""}
                                    </Collapse>
                                    <div onClick={ShowServices}
                                         className="default-link">
                                        <span className="icon icon-service"
                                              style={isOpen ? {backgroundColor: '#04A6FB'} : {}}/>
                                        <Link to="#" className={isOpen ? "main-link text-duo" : "main-link"}>
                                            Services
                                        </Link>
                                        {!isOpen ?
                                            <span style={{marginLeft: "auto", transform: "rotate(90deg)"}}
                                                  className="fas fa-angle-up"/> :
                                            <span style={{
                                                marginLeft: "auto",
                                                transform: "rotate(180deg)",
                                                color: '#04A6FB'
                                            }}
                                                  className="fas fa-angle-up"/>}
                                    </div>
                                    <Collapse className="pl-3 collapses" isOpen={isOpen}>
                                        <div className={
                                            this.props.pathname === "/admin/mainService" ?
                                                "active-link" : "default-link"
                                        }>
                                            <Link to="/admin/mainService"
                                                  className="main-link">
                                                Main Service
                                            </Link>
                                        </div>
                                        <div className={
                                            this.props.pathname === "/admin/service" ?
                                                "active-link" : "default-link"
                                        }>
                                            <Link to="/admin/service"
                                                  className="main-link">
                                                Service
                                            </Link>
                                        </div>
                                        <div className={
                                            this.props.pathname === "/admin/servicePrice" ?
                                                "active-link" : "default-link"
                                        }>
                                            <Link to="/admin/servicePrice"
                                                  className="main-link">
                                                Service price
                                            </Link>
                                        </div>
                                        <div className={
                                            this.props.pathname === "/admin/documentType" ?
                                                "active-link" : "default-link"
                                        }>
                                            <Link to="/admin/documentType"
                                                  className="main-link">
                                                Document
                                            </Link>
                                        </div>
                                        <div className={
                                            this.props.pathname === "/admin/pricing" ?
                                                "active-link" : "default-link"
                                        }>
                                            <Link to="/admin/pricing"
                                                  className="main-link">
                                                Pricing
                                            </Link>
                                        </div>
                                        <div className={
                                            this.props.pathname === "/admin/additionalService" ?
                                                "active-link" : "default-link"
                                        }>
                                            <Link to="/admin/additionalService"
                                                  className="main-link">
                                                Additional Service
                                            </Link>
                                        </div>
                                    </Collapse>
                                    <div className={
                                        this.props.pathname === "/admin/zipCodes" ?
                                            "active-link" : "default-link"
                                    }>
                                        <span className="icon icon-zipCode"/>
                                        <Link to="/admin/zipCodes"
                                              className="main-link">
                                            Zip codes
                                        </Link>
                                    </div>
                                    <div className={
                                        this.props.pathname === "/admin/feedback" ?
                                            "active-link" : "default-link"
                                    }>
                                        <span className="icon icon-feedback"/>
                                        <Link to="/admin/feedback"
                                              className="main-link">
                                            Feedback
                                        </Link>
                                    </div>
                                    <div className={
                                        this.props.pathname === "/admin/discount" ?
                                            "active-link" : "default-link"
                                    }>
                                        <span className="icon icon-discount"/>
                                        <Link to="/admin/discount"
                                              className="main-link">
                                            Discounts
                                        </Link>
                                    </div>

                                    <div onClick={showPages}
                                         className="default-link">
                                        <span className="icon icon-blog"
                                              style={isOpenPages ? {backgroundColor: '#04a6fb'} : {}}/>
                                        <Link to="#"
                                              className={isOpenPages ? "main-link text-duo" : "main-link"}>
                                            Pages
                                        </Link>
                                        {!isOpenPages ?
                                            <span style={{marginLeft: "auto", transform: "rotate(90deg)"}}
                                                  className="fas fa-angle-up"/> :
                                            <span style={{
                                                marginLeft: "auto",
                                                transform: "rotate(180deg)",
                                                color: '#04A6FB'
                                            }}
                                                  className="fas fa-angle-up"/>}
                                    </div>
                                    <Collapse className="pl-3 collapses" isOpen={isOpenPages}>
                                        <div className={
                                            this.props.pathname === "/admin/blog" ?
                                                "active-link" : "default-link"
                                        }>
                                            <Link to="/admin/blog"
                                                  className="main-link">
                                                Blog
                                            </Link>
                                        </div>
                                        <div className={

                                            this.props.pathname === "/admin/dynamic" ?
                                                "active-link" : "default-link"
                                        }>
                                            <Link to="/admin/dynamic"
                                                  className="main-link">
                                                Dynamic Pages
                                            </Link>
                                        </div>
                                        {isSuperAdmin ?
                                            <>
                                                <div className={

                                                    this.props.pathname === "/termsofservices" ?
                                                        "active-link" : "default-link"
                                                }>
                                                    <Link to="/termsofservices"
                                                          className="main-link">
                                                        Terms of services
                                                    </Link>
                                                </div>
                                                <div className={
                                                    this.props.pathname === "/notificationsettings" ?
                                                        "active-link" : "default-link"
                                                }>
                                                    <Link to="/notificationsettings"
                                                          className="main-link">
                                                        Notification settings
                                                    </Link>
                                                </div>
                                                <div className={

                                                    this.props.pathname === "/privacypolicy" ?
                                                        "active-link" : "default-link"
                                                }>
                                                    <Link to="/privacypolicy"
                                                          className="main-link">
                                                        Privacy policy
                                                    </Link>
                                                </div>
                                                <div className={

                                                    this.props.pathname === "/admin/faq" ?
                                                        "active-link" : "default-link"
                                                }>
                                                    <Link to="/admin/faq"
                                                          className="main-link">
                                                        FAQ
                                                    </Link>
                                                </div>
                                            </>
                                            : ""}
                                    </Collapse>

                                    <div onClick={showGeneral}
                                         className="default-link">
                                        <span className="icon icon-service"
                                              style={isOpenGeneral ? {backgroundColor: '#04a6fb'} : {}}/>
                                        <Link to="#"
                                              className={isOpenGeneral ? "main-link text-duo" : "main-link"}>
                                            General
                                        </Link>
                                        {!isOpenGeneral ?
                                            <span style={{marginLeft: "auto", transform: "rotate(90deg)"}}
                                                  className="fas fa-angle-up"/> :
                                            <span style={{
                                                marginLeft: "auto",
                                                transform: "rotate(180deg)",
                                                color: '#04A6FB'
                                            }}
                                                  className="fas fa-angle-up"/>}
                                    </div>
                                    <Collapse className="pl-3 collapses" isOpen={isOpenGeneral}>
                                        {isSuperAdmin ?
                                            <div onClick={showTime}
                                                 className={this.props.pathname === "/admin/time" ?
                                                     "active-link" : "default-link"}>
                                                <span className="icon icon-order"/>
                                                <Link to="/admin/time"
                                                      className="main-link">
                                                    Time
                                                </Link>
                                            </div> : ""}
                                        <div className={
                                            this.props.pathname === "/admin/payType" ?
                                                "active-link" : "default-link"
                                        }>
                                            <span className="icon icon-order"/>
                                            <Link to="/admin/payType"
                                                  className="main-link">
                                                Pay Type
                                            </Link>
                                        </div>
                                        <div className={
                                            this.props.pathname === "/admin/holiday" ?
                                                "active-link" : "default-link"
                                        }>
                                            <span className="icon icon-blog"/>
                                            <Link to="/admin/holiday"
                                                  className="main-link">
                                                Holiday
                                            </Link>
                                        </div>
                                        <div className={
                                            this.props.pathname === "/admin/country" ?
                                                "active-link" : "default-link"
                                        }>
                                            <span className="icon icon-blog"/>
                                            <Link to="/admin/country"
                                                  className="main-link">
                                                Country
                                            </Link>
                                        </div>
                                        <div className={
                                            this.props.pathname === "/admin/partners" ?
                                                "active-link" : "default-link"
                                        }>
                                            <span className="icon icon-blog"/>
                                            <Link to="/admin/partners"
                                                  className="main-link">
                                                Partners
                                            </Link>
                                        </div>
                                        <div className={
                                            this.props.pathname === "/admin/review" ?
                                                "active-link" : "default-link"
                                        }>
                                            <span className="icon icon-blog"/>
                                            <Link to="/admin/review"
                                                  className="main-link">
                                                Reviews
                                            </Link>
                                        </div>
                                    </Collapse>

                                    {isSuperAdmin ?
                                        <div className={
                                            this.props.pathname === "/admin/payment" ?
                                                "active-link" : "default-link"
                                        }>
                                            <span className="icon icon-order"/>
                                            <Link to="/admin/payment"
                                                  className="main-link">
                                                Payments
                                            </Link>
                                        </div>
                                        : ""}
                                    {isSuperAdmin ?
                                        <div className={
                                            this.props.pathname === "/admin/Audit" ?
                                                "active-link" : "default-link"
                                        }>
                                            <span className="icon icon-customer"/>
                                            <Link to="/admin/Audit"
                                                  className="main-link">
                                                History
                                            </Link>
                                        </div>
                                        : ""}


                                    <div className="about-user">
                                        <div className="avatar">
                                            <img
                                                src={currentUser.photoId ? config.BASE_URL + "/attachment/" + currentUser.photoId : "/assets/img/avatar.png"}
                                                alt=""/>
                                        </div>
                                        <div className="name-title">
                                            <div className="name">
                                                {currentUser.lastName + ' ' + currentUser.firstName}
                                            </div>
                                            <div className="title">
                                                Admin
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {/*Right side*/}
                            <div className="main-layout-right">
                                <div className={this.state.menuHidden ? "menu-toggle l-0" : "menu-toggle"}
                                     onClick={this.toggleMenu}>
                                    {
                                        this.state.menuHidden ?
                                            <MenuMobile/>
                                            :
                                            <span className="icon icon-plus"/>
                                    }
                                </div>
                                <Container className="client-navbar">

                                    <Row>
                                        <Col md={2}>
                                            {this.props.pathname === "/admin"
                                                ? <div className="page-name">
                                                    Main page
                                                </div> : ""
                                            }
                                            {
                                                this.props.pathname === "/admin/agent" ?

                                                    <div className="page-name">
                                                        Agent
                                                    </div> : ""
                                            }

                                            {/*{*/}
                                            {/*    this.props.pathname === "/admin/subService" ?*/}

                                            {/*        <div className="page-name">*/}
                                            {/*            Sub Service*/}
                                            {/*        </div> : ""*/}
                                            {/*}*/}

                                            {
                                                this.props.pathname === "/admin/mainService" ?

                                                    <div className="page-name">
                                                        Main Services
                                                    </div> : ""
                                            }
                                            {
                                                this.props.pathname === "/admin/mainServiceWithPercent" ?

                                                    <div className="page-name">
                                                        Main percent
                                                    </div> : ""
                                            }
                                            {
                                                this.props.pathname === "/admin/service" ?

                                                    <div className="page-name">
                                                        Service
                                                    </div> : ""
                                            }
                                            {
                                                this.props.pathname === "/admin/payment" ?

                                                    <div className="page-name">
                                                        Payments
                                                    </div> : ""
                                            }
                                            {
                                                this.props.pathname === "/admin/calendar" ?

                                                    <div className="page-name">
                                                        Calendar
                                                    </div> : ""
                                            }
                                            {
                                                this.props.pathname === "/admin/faq" ?

                                                    <div className="page-name">
                                                        FAQ
                                                    </div> : ""
                                            }
                                            {
                                                this.props.pathname === "/admin/servicePrice" ?

                                                    <div className="page-name">
                                                        Service price
                                                    </div> : ""
                                            }
                                            {
                                                this.props.pathname === "/admin/documentType" ?

                                                    <div className="page-name">
                                                        Document
                                                    </div> : ""
                                            }

                                            {
                                                this.props.pathname === "/admin/pricing" ?

                                                    <div className="page-name">
                                                        Pricing
                                                    </div> : ""
                                            }
                                            {/*{*/}
                                            {/*    this.props.pathname === "/admin/timeBooked" ?*/}

                                            {/*        <div className="page-name">*/}
                                            {/*            Time booked*/}
                                            {/*        </div> : ""*/}
                                            {/*}*/}
                                            {/*{*/}
                                            {/*    this.props.pathname === "/admin/timeDuration" ?*/}

                                            {/*        <div className="page-name">*/}
                                            {/*            Time duration*/}
                                            {/*        </div> : ""*/}
                                            {/*}*/}

                                            {/*{*/}
                                            {/*    this.props.pathname === "/admin/weekDay" ?*/}
                                            {/*        <div className="page-name">*/}
                                            {/*            Week days*/}
                                            {/*        </div> : ""*/}
                                            {/*}*/}

                                            {
                                                this.props.pathname === "/admin/time" ?
                                                    <div className="page-name">
                                                        Time
                                                    </div> : ""
                                            }
                                            {
                                                this.props.pathname === "/admin/payType" ?
                                                    <div className="page-name">
                                                        Pay Type
                                                    </div> : ""
                                            }
                                            {
                                                this.props.pathname === "/admin/order" ?

                                                    <div className="page-name" id="order-page-name">
                                                        Order
                                                    </div> : ""
                                            }
                                            {/*{*/}
                                            {/*    this.props.pathname === "/admin/zipCodes" ?*/}

                                            {/*        <div className="page-name">*/}
                                            {/*            ZIP codes*/}
                                            {/*        </div> : ""*/}
                                            {/*}*/}
                                            {
                                                this.props.pathname === "/admin/feedback" ?

                                                    <div className="page-name">
                                                        Feedback
                                                    </div> : ""
                                            }

                                            {
                                                this.props.pathname === "/admin/admins" ?

                                                    <div className="page-name">
                                                        Admins
                                                    </div> : ""
                                            }
                                            {
                                                this.props.pathname === "/admin/customer" ?

                                                    <div className="page-name">
                                                        Customer
                                                    </div> : ""
                                            }
                                            {
                                                this.props.pathname === "/admin/holiday" ?

                                                    <div className="page-name">
                                                        Holiday
                                                    </div> : ""
                                            }
                                            {
                                                this.props.pathname === "/admin/country" ?

                                                    <div className="page-name">
                                                        Country
                                                    </div> : ""
                                            }

                                            {
                                                this.props.pathname === "/admin/blog" ?

                                                    <div className="page-name">
                                                        Blog
                                                    </div> : ""
                                            }
                                            {
                                                this.props.pathname === "/admin/discount" ?

                                                    <div className="page-name">
                                                        Discount
                                                    </div> : ""
                                            }
                                            {
                                                this.props.pathname === "/admin/audit" ?

                                                    <div className="page-name">
                                                        History
                                                    </div> : ""
                                            }
                                            {
                                                this.props.pathname === "/admin/dynamic" ?

                                                    <div className="page-name">
                                                        Dynamic Page
                                                    </div> : ""
                                            }

                                        </Col>
                                        <Col md={4}>
                                            {this.props.pathname === "/admin/agent" ||
                                            this.props.pathname === "/admin/order" ||
                                            this.props.pathname === "/admin/customer"
                                                ?
                                                <div className={'search_block_admin'}>
                                                    <Input
                                                        onKeyPress={(item) => pressEnter(item)}
                                                        onChange={(item) => searchByName(item)}
                                                        className="search-box search-box-admin" id="searchInput"
                                                        defaultValue={search} placeholder="Search ...."/>
                                                    {this.props.pathname === "/admin/agent"  ||
                                                    this.props.pathname === "/admin/order" ?
                                                        <img onClick={ShowFilter}
                                                             className="search-icons search-icons_admin"
                                                             src="/assets/icons/search.png" alt="ccc"/> : ""}

                                                    {isFilter ?

                                                        <OutsideClickHandler onOutsideClick={ShowFilter}>
                                                            <div className="openFilter">
                                                                {/*<Form >*/}
                                                                {/*    <FormGroup>*/}
                                                                {/*        <Input type="text" name="customerName" id="customerName"*/}
                                                                {/*               placeholder="By customer name"/>*/}
                                                                {/*    </FormGroup>*/}
                                                                {/*    <FormGroup>*/}
                                                                {/*        <Input type="number" name="zipCode" id="zipCode"*/}
                                                                {/*               placeholder="By zipcode"/>*/}
                                                                {/*    </FormGroup>*/}
                                                                {/*    <FormGroup>*/}
                                                                {/*        <Input type="date" name="date" id="date"*/}
                                                                {/*               placeholder="By data"/>*/}
                                                                {/*    </FormGroup>*/}
                                                                {/*    <FormGroup>*/}
                                                                {/*        <Input type="submit" name="asdfda       " id="submit"/>*/}
                                                                {/*    </FormGroup>*/}

                                                                {/*</Form>*/}
                                                                {this.props.pathname === "/admin/order" ||this.props.pathname === "/client/order" ?
                                                                    <AvForm onValidSubmit={filter}>
                                                                        <AvField name="zipCode" label="By zipcode"
                                                                                 placeholder="Enter zipcode"
                                                                                 type="number"/>
                                                                        <AvField name="status" label="By order status"
                                                                                 placeholder="Enter status"/>
                                                                        <AvField name="date" label="By date"
                                                                                 placeholder="Select date" type="date"/>
                                                                        <Button color="primary"
                                                                                className="btn-block">Filter</Button>
                                                                    </AvForm> :
                                                                    <AvForm onValidSubmit={filter}>
                                                                        <AvField name="zipCode" label="By zipcode"
                                                                                 placeholder="Enter zipcode"
                                                                                 type="number"/>
                                                                        <AvField name="date" label="By date"
                                                                                 placeholder="Select date" type="date"/>
                                                                        <Button color="primary"
                                                                                className="btn-block">Filter</Button>
                                                                    </AvForm>}
                                                            </div>
                                                        </OutsideClickHandler>

                                                        : ""}
                                                </div>
                                                : ""
                                            }
                                        </Col>

                                        {this.props.pathname === "/admin/order" ?
                                            <Col md={4}>

                                                <div className="download-order-list"
                                                     onClick={() => this.openModal("Select file type", "/order")}>
                                                    <div className="icon icon-download"/>
                                                    <div className="download-text">Download order list</div>
                                                </div>

                                            </Col>
                                            : ""
                                        }

                                        {this.props.pathname === "/admin/customer" ?
                                            <Col md={4}>

                                                <div className="download-order-list"
                                                     onClick={() => this.openModal("Select file type", "/customer")}>
                                                    <div className="icon icon-download"/>
                                                    <div className="download-text">Download customer list</div>
                                                </div>

                                            </Col>
                                            : ""
                                        }
                                        {this.props.pathname === "/admin/agent" ?
                                            <Col md={4}>
                                                <div className="download-order-list"
                                                     onClick={() => this.openModal("Select file type", "/agent")}>
                                                    <div className="icon icon-download"/>
                                                    <div className="download-text">Download agents list</div>
                                                </div>

                                            </Col>
                                            : ""
                                        }

                                        {this.props.pathname !== "/admin/zipCodes" || this.props.pathname !== "/admin/blog" ?
                                            <Col md={2} className=
                                                {this.props.pathname === "/admin/order" ||
                                                this.props.pathname === "/admin/customer" || this.props.pathname === "/admin/agent" ? " " :
                                                    "offset-4"}>
                                                <div className="notification-exit">
                                                    <div className="notification">
                                                        <img src="/assets/icons/notification.png" alt=""/>
                                                    </div>
                                                    <div onClick={logOut} className="exit ml-3">
                                                        <img src="/assets/icons/exit.png" alt=""/>
                                                    </div>
                                                </div>
                                            </Col>
                                            : ''
                                        }

                                    </Row>
                                </Container>
                                {this.props.children}
                            </div>
                        </div>
                        : ""}
                    {this.state.showModal ?
                        <DownloadRequestModal
                            showModal={this.state.showModal}
                            cancel={this.openModal}
                            dispatch={dispatch}
                            path={this.state.path}
                            confirm={(item) => downloadFile(item)}
                            text={this.state.text}
                        /> : ""}
                </div>
        );
    }
}

AdminLayout
    .propTypes = {};

export default connect(
    ({
         app: {isOpenGeneral, isOpenUser, isOpenPages, isOpen, openCol, loading, isFilter, filters, search}
         ,
         auth: {isAdmin, isSuperAdmin, currentUser}
     }
    ) =>
        ({
            isOpenGeneral,
            isOpenPages,
            isAdmin,
            currentUser,
            isOpenUser,
            isOpen,
            isSuperAdmin,
            openCol,
            loading,
            isFilter,
            filters,
            search
        })
)
(AdminLayout);
